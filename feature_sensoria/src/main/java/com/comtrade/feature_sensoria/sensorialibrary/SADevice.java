/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: SADevice.java
 * Author: sdelic
 *
 * History:  21.2.22. 10:43 created
 */

//
// Decompiled by Procyon v0.5.36
// 

package com.comtrade.feature_sensoria.sensorialibrary;

import android.bluetooth.BluetoothDevice;
import android.os.Parcel;
import android.os.Parcelable;

public class SADevice implements Parcelable {
    public String deviceCode;
    public String deviceMac;
    public String deviceName;
    public BluetoothDevice device;
    public int rssiAtScanTime;
    public boolean isConnected;

    public static final Parcelable.Creator<SADevice> CREATOR;

    protected SADevice() {
    }

    public SADevice(final BluetoothDevice fromBluetoothDevice, final int rssi) {
        this.device = fromBluetoothDevice;
        this.deviceName = fromBluetoothDevice.getName();
        this.deviceMac = fromBluetoothDevice.getAddress();
        if (deviceName.length() > 12) {
            this.deviceCode = this.deviceName.substring(12);
        } else {
            this.deviceCode = this.deviceName;
        }
        this.isConnected = false;
        this.rssiAtScanTime = rssi;
    }

    protected SADevice(final Parcel in) {
        this.deviceCode = in.readString();
        this.deviceMac = in.readString();
        this.deviceName = in.readString();
        this.rssiAtScanTime = in.readInt();
        this.device = in.readParcelable(BluetoothDevice.class.getClassLoader());
    }

    @Override
    public String toString() {
        return this.deviceName + " / rssi:" + this.rssiAtScanTime;
    }

    @Override
    public boolean equals(final Object object) {
        if (object == null) {
            return false;
        }
        if (!SADevice.class.isAssignableFrom(object.getClass())) {
            return false;
        }
        final SADevice device = (SADevice) object;
        return device.deviceMac.compareTo(this.deviceMac) == 0 && device.deviceName.compareTo(this.deviceName) == 0;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(final Parcel dest, final int flags) {
        dest.writeString(this.deviceCode);
        dest.writeString(this.deviceMac);
        dest.writeString(this.deviceName);
        dest.writeInt(this.rssiAtScanTime);
        dest.writeParcelable(this.device, flags);
    }

    public int returnRSSI() {
        return this.rssiAtScanTime;
    }

    static {
        CREATOR = new Parcelable.Creator<SADevice>() {
            public SADevice createFromParcel(final Parcel in) {
                return new SADevice(in);
            }

            public SADevice[] newArray(final int size) {
                return new SADevice[size];
            }
        };
    }
}
