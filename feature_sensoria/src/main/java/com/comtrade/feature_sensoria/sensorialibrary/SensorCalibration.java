/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: SensorCalibration.java
 * Author: sdelic
 *
 * History:  21.2.22. 10:43 created
 */

//
// Decompiled by Procyon v0.5.36
// 

package com.comtrade.feature_sensoria.sensorialibrary;

import android.os.Parcel;
import android.os.Parcelable;

public class SensorCalibration implements Parcelable
{
    public SensorType sensorType;
    public float calibrationParameterA;
    public float calibrationParameterB;
    public float calibrationParameterC;
    public static final Parcelable.Creator<SensorCalibration> CREATOR;

    public SensorCalibration() {
        this.sensorType = SensorType.Unknown;
        this.calibrationParameterA = 0.0f;
        this.calibrationParameterB = 0.0f;
        this.calibrationParameterC = 0.0f;
    }

    public SensorCalibration(final Parcel in) {
        final int enumTmp = in.readInt();
        this.sensorType = SensorType.values()[enumTmp];
        this.calibrationParameterA = in.readFloat();
        this.calibrationParameterB = in.readFloat();
        this.calibrationParameterC = in.readFloat();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(final Parcel dest, final int flags) {
        final int enumTmp = this.sensorType.getNumVal();
        dest.writeInt(enumTmp);
        dest.writeFloat(this.calibrationParameterA);
        dest.writeFloat(this.calibrationParameterB);
        dest.writeFloat(this.calibrationParameterC);
    }

    static {
        CREATOR = (Parcelable.Creator)new Parcelable.Creator<SensorCalibration>() {
            public SensorCalibration createFromParcel(final Parcel in) {
                return new SensorCalibration(in);
            }
            
            public SensorCalibration[] newArray(final int size) {
                return new SensorCalibration[size];
            }
        };
    }
}
