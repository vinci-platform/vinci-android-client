/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: SADataPoint.java
 * Author: sdelic
 *
 * History:  15.3.22. 10:01 created
 */

//
// Decompiled by Procyon v0.5.36
// 

package com.comtrade.feature_sensoria.sensorialibrary;

import android.os.Parcel;
import android.os.Parcelable;

public class SADataPoint implements Parcelable
{
    int[] channels;
    float[] accelerometer;
    float[] gyroscope;
    float[] magnetometer;
    int tick;
    int samplingFrequency;
    int actualSamplingFrequency;
    int packetsLost;
    public static final Parcelable.Creator<SADataPoint> CREATOR;

    public SADataPoint() {
        this.channels = new int[8];
        this.accelerometer = new float[3];
        this.gyroscope = new float[3];
        this.magnetometer = new float[3];
        this.tick = 0;
        this.packetsLost = 0;
    }

    public int[] getChannels() {
        return this.channels;
    }

    public float[] getAccelerometers() {
        return this.accelerometer;
    }

    public float[] getGyroscopes() {
        return this.gyroscope;
    }

    public float[] getMagnetometers() {
        return this.magnetometer;
    }

    public int getTickCount() {
        return this.tick;
    }

    public int getNominalSamplingRate() {
        return this.samplingFrequency;
    }

    public int getActualSamplingRate() {
        return this.actualSamplingFrequency;
    }

    public int getPacketsLost() {
        return this.packetsLost;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(final Parcel dest, final int flags) {
        dest.writeIntArray(this.channels);
        dest.writeFloatArray(this.accelerometer);
        dest.writeFloatArray(this.gyroscope);
        dest.writeFloatArray(this.magnetometer);
        dest.writeInt(this.tick);
        dest.writeInt(this.packetsLost);
        dest.writeInt(this.actualSamplingFrequency);
        dest.writeInt(this.samplingFrequency);
    }

    private SADataPoint(final Parcel in) {
        this.channels = new int[8];
        this.accelerometer = new float[3];
        this.gyroscope = new float[3];
        this.magnetometer = new float[3];
        this.tick = 0;
        this.packetsLost = 0;
        in.readIntArray(this.channels);
        in.readFloatArray(this.accelerometer);
        in.readFloatArray(this.gyroscope);
        in.readFloatArray(this.magnetometer);
        this.tick = in.readInt();
        this.packetsLost = in.readInt();
        this.actualSamplingFrequency = in.readInt();
        this.samplingFrequency = in.readInt();
    }

    static {
        CREATOR = (Parcelable.Creator)new Parcelable.Creator<SADataPoint>() {
            public SADataPoint createFromParcel(final Parcel in) {
                return new SADataPoint(in);
            }
            
            public SADataPoint[] newArray(final int size) {
                return new SADataPoint[size];
            }
        };
    }
}
