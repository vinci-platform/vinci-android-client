/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: CSGenericAccessService.java
 * Author: sdelic
 *
 * History:  21.2.22. 10:43 created
 */

//
// Decompiled by Procyon v0.5.36
// 

package com.comtrade.feature_sensoria.sensorialibrary;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.os.Build;
import android.os.Handler;
import android.util.Log;
import android.util.SparseArray;

import java.io.Serializable;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class CSGenericAccessService extends SAService implements Serializable
{
    private final long UUID_GENERIC_ACCESS_SERVICE = 6144L;
    private LinkedBlockingQueue<Object> mwcQueue;
    private static SparseArray<BluetoothGattCharacteristic> characteristicMap;
    
    @Override
    protected void runOnUiThread(final Runnable runnable) {
        this.mHandler.post(runnable);
    }
    
    private CSGenericAccessService() {
        this.mwcQueue = new LinkedBlockingQueue<Object>();
    }
    
    public CSGenericAccessService(final String deviceMac) {
        super(deviceMac);
        this.mwcQueue = new LinkedBlockingQueue<Object>();
        Log.d(this.Tag, "ctor");
        this.mServiceName = "Generic Access ServiceTypes";
        CSGenericAccessService.characteristicMap = (SparseArray<BluetoothGattCharacteristic>)new SparseArray();
    }
    
    public CSGenericAccessService(final String deviceMac, final BlockingQueue<SABundleObject> saBundle) {
        super(deviceMac, saBundle);
        this.mwcQueue = new LinkedBlockingQueue<Object>();
        Log.d(this.Tag = CSGenericAccessService.class.getSimpleName(), "ctor");
        this.mServiceName = "Generic Access ServiceTypes";
        CSGenericAccessService.characteristicMap = (SparseArray<BluetoothGattCharacteristic>)new SparseArray();
    }
    
    @Override
    ServiceTypes getType() {
        return ServiceTypes.GENERIC_ACCESS_SERVICE;
    }
    
    public void start(final SAServiceInterface delegate) throws InterruptedException {
        Log.d(this.Tag, "start");
        this.mHandler = new Handler();
        try {
            super.start(delegate);
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    
    @Override
    protected void setOptionalWriter(final boolean force) {
        if (this.mWriter == null || force) {
            this.mWriter = this.getTheWriter();
            this.mThreadWriter = new Thread(this.mWriter);
        }
    }
    
    @Override
    protected void deleteOptionalWriter() {
        if (this.mServiceStatus.compare(2) && this.isWriterStarted) {
            this.mWriter.terminate();
            try {
                this.mThreadWriter.join();
                this.mWriter = null;
                this.mThreadWriter = null;
            }
            catch (InterruptedException ex) {}
        }
    }
    
    private MyRunnable getTheWriter() {
        return new MyRunnable() {
            public void terminate() {
                final StopObject stop = new StopObject();
                try {
                    CSGenericAccessService.this.mwcQueue.put(stop);
                }
                catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            
            @Override
            public void run() {
                CSGenericAccessService.this.isWriterStarted = true;
                try {
                    while (true) {
                        final Object obj = CSGenericAccessService.this.mwcQueue.take();
                        if (obj instanceof StopObject) {
                            break;
                        }
                        if (obj instanceof DeviceName) {
                            final DeviceName dn = (DeviceName)obj;
                            CSGenericAccessService.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    CSGenericAccessService.this.writeDeviceNameEx(dn.deviceName);
                                }
                            });
                        }
                        else {
                            if (!(obj instanceof AppearanceClass)) {
                                continue;
                            }
                            final AppearanceClass ac = (AppearanceClass)obj;
                            CSGenericAccessService.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    CSGenericAccessService.this.setAppearanceEx(ac.appearance);
                                }
                            });
                        }
                    }
                    CSGenericAccessService.this.isWriterStarted = false;
                }
                catch (InterruptedException e) {}
            }
        };
    }
    
    public void resume() throws InterruptedException {
        Log.d(this.Tag, "resume");
        super.resume();
    }
    
    public void pause() {
        Log.d(this.Tag, "pause");
        super.pause();
    }
    
    public void stop() {
        Log.d(this.Tag, "stop");
        super.stop();
    }
    
    public void dispose() {
    }
    
    public String readDeviceName() {
        Log.d(this.Tag, "readDeviceName");
        String response = null;
        final CSDeviceName command = new CSDeviceName();
        if (CSGenericAccessService.characteristicMap.indexOfKey((int)command.getCharacteristic()) < 0) {
            if (this.mIServiceDelegate != null) {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        CSGenericAccessService.this.mIServiceDelegate.didServiceError(CSGenericAccessService.this.mDevice, CSGenericAccessService.this.getType(), CSGenericAccessService.this.mServiceName, "readDeviceName", SAErrors.ERROR_CHARACTERISTIC_NOT_IMPLEMENTED, String.format("The mandatory characteristic 0x%02X of Generic access service is not implemented", command.getCharacteristic()));
                    }
                });
            }
            return null;
        }
        this.addListener(command);
        if (this.ReadCharacteristic(command)) {
            if ((response = command.readDeviceName()).isEmpty()) {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (CSGenericAccessService.this.mIServiceDelegate != null) {
                            CSGenericAccessService.this.mIServiceDelegate.didServiceError(CSGenericAccessService.this.mDevice, CSGenericAccessService.this.getType(), CSGenericAccessService.this.mServiceName, "readDeviceName", command.getLastError());
                        }
                    }
                });
            }
        }
        else if (this.mIServiceDelegate != null) {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    CSGenericAccessService.this.mIServiceDelegate.didServiceError(CSGenericAccessService.this.mDevice, CSGenericAccessService.this.getType(), CSGenericAccessService.this.mServiceName, "readDeviceName", SAErrors.ERROR_CHARACTERISTIC_NOT_IMPLEMENTED, String.format("Error reading Generic Access ServiceTypes characteristic 0x%02X ", command.getCharacteristic()));
                }
            });
        }
        this.removeListener(command);
        return response;
    }
    
    public boolean writeDeviceName(final String deviceName) {
        Log.d(this.Tag, "writeDeviceName");
        if (deviceName.length() > 20) {
            if (this.mIServiceDelegate != null) {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        CSGenericAccessService.this.mIServiceDelegate.didServiceError(CSGenericAccessService.this.mDevice, CSGenericAccessService.this.getType(), CSGenericAccessService.this.mServiceName, "writeDeviceName", SAErrors.ERROR_INVALID_DEVICE_NAME_LENGTH, String.format(Locale.US, "Found length %d, max permitted 20", deviceName.length()));
                    }
                });
            }
            return false;
        }
        final DeviceName dName = new DeviceName(deviceName);
        try {
            this.mwcQueue.put(dName);
        }
        catch (InterruptedException e) {
            return false;
        }
        return true;
    }
    
    private void writeDeviceNameEx(final String deviceName) {
        final CSDeviceName command = new CSDeviceName(deviceName);
        if (CSGenericAccessService.characteristicMap.indexOfKey((int)command.getCharacteristic()) < 0) {
            if (this.mIServiceDelegate != null) {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        CSGenericAccessService.this.mIServiceDelegate.didServiceError(CSGenericAccessService.this.mDevice, CSGenericAccessService.this.getType(), CSGenericAccessService.this.mServiceName, "writeDeviceNameEx", SAErrors.ERROR_CHARACTERISTIC_NOT_IMPLEMENTED, String.format("The mandatory characteristic 0x%02X of Generic access service is not implemented", command.getCharacteristic()));
                    }
                });
            }
            return;
        }
        this.addListener(command);
        if (this.WriteCharacteristic(command)) {
            if (!command.writeDeviceName()) {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (CSGenericAccessService.this.mIServiceDelegate != null) {
                            CSGenericAccessService.this.mIServiceDelegate.didServiceError(CSGenericAccessService.this.mDevice, CSGenericAccessService.this.getType(), CSGenericAccessService.this.mServiceName, "writeDeviceNameEx", command.getLastError());
                        }
                    }
                });
            }
            else {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ((CSGenericAccessServiceInterface)CSGenericAccessService.this.mIServiceDelegate).didServiceDeviceNameSet(CSGenericAccessService.this.mDevice, CSGenericAccessService.this.getType());
                    }
                });
            }
        }
        else {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (CSGenericAccessService.this.mIServiceDelegate != null) {
                        CSGenericAccessService.this.mIServiceDelegate.didServiceError(CSGenericAccessService.this.mDevice, CSGenericAccessService.this.getType(), CSGenericAccessService.this.mServiceName, "writeDeviceNameEx", SAErrors.ERROR_OPTIONAL_CHARACTERISTIC_NOT_IMPLEMENTED, String.format("Error writing Generic Access ServiceTypes optional characteristic 0x%02X ", command.getCharacteristic()));
                    }
                }
            });
        }
        this.removeListener(command);
    }
    
    public Appearance readAppearance() {
        Log.d(this.Tag, "readAppearance");
        Appearance response = null;
        final CSAppearance command = new CSAppearance();
        if (CSGenericAccessService.characteristicMap.indexOfKey((int)command.getCharacteristic()) < 0) {
            if (this.mIServiceDelegate != null) {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        CSGenericAccessService.this.mIServiceDelegate.didServiceError(CSGenericAccessService.this.mDevice, CSGenericAccessService.this.getType(), CSGenericAccessService.this.mServiceName, "readAppearance", SAErrors.ERROR_CHARACTERISTIC_NOT_IMPLEMENTED, String.format("The mandatory characteristic 0x%02X of Generic access service is not implemented", command.getCharacteristic()));
                    }
                });
            }
            return null;
        }
        this.addListener(command);
        if (this.ReadCharacteristic(command)) {
            if ((response = command.readAppearance()) == null) {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (CSGenericAccessService.this.mIServiceDelegate != null) {
                            CSGenericAccessService.this.mIServiceDelegate.didServiceError(CSGenericAccessService.this.mDevice, CSGenericAccessService.this.getType(), CSGenericAccessService.this.mServiceName, "readAppearance", command.getLastError());
                        }
                    }
                });
            }
        }
        else if (this.mIServiceDelegate != null) {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    CSGenericAccessService.this.mIServiceDelegate.didServiceError(CSGenericAccessService.this.mDevice, CSGenericAccessService.this.getType(), CSGenericAccessService.this.mServiceName, "readAppearance", SAErrors.ERROR_CHARACTERISTIC_NOT_IMPLEMENTED, String.format("Error reading characteristic 0x%02X of Generic access service", command.getCharacteristic()));
                }
            });
        }
        this.removeListener(command);
        return response;
    }
    
    public boolean setAppearance(final Appearance appearance) {
        Log.d(this.Tag, "setAppearance");
        final AppearanceClass ap = new AppearanceClass(appearance);
        try {
            this.mwcQueue.put(ap);
        }
        catch (InterruptedException e) {
            return false;
        }
        return true;
    }
    
    private void setAppearanceEx(final Appearance appearance) {
        Log.d(this.Tag, "setAppearanceEx");
        final CSAppearance command = new CSAppearance(appearance);
        if (CSGenericAccessService.characteristicMap.indexOfKey((int)command.getCharacteristic()) < 0) {
            if (this.mIServiceDelegate != null) {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        CSGenericAccessService.this.mIServiceDelegate.didServiceError(CSGenericAccessService.this.mDevice, CSGenericAccessService.this.getType(), CSGenericAccessService.this.mServiceName, "setAppearanceEx", SAErrors.ERROR_CHARACTERISTIC_NOT_IMPLEMENTED, String.format("The mandatory characteristic 0x%02X of Generic access service is not implemented", command.getCharacteristic()));
                    }
                });
            }
            return;
        }
        this.addListener(command);
        if (this.WriteCharacteristic(command)) {
            if (!command.writeAppearance()) {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (CSGenericAccessService.this.mIServiceDelegate != null) {
                            CSGenericAccessService.this.mIServiceDelegate.didServiceError(CSGenericAccessService.this.mDevice, CSGenericAccessService.this.getType(), CSGenericAccessService.this.mServiceName, "setAppearanceEx", command.getLastError());
                        }
                    }
                });
            }
            else {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ((CSGenericAccessServiceInterface)CSGenericAccessService.this.mIServiceDelegate).didServiceAppearanceSet(CSGenericAccessService.this.mDevice, CSGenericAccessService.this.getType(), appearance);
                    }
                });
            }
        }
        else {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (CSGenericAccessService.this.mIServiceDelegate != null) {
                        CSGenericAccessService.this.mIServiceDelegate.didServiceError(CSGenericAccessService.this.mDevice, CSGenericAccessService.this.getType(), CSGenericAccessService.this.mServiceName, "setAppearanceEx", SAErrors.ERROR_OPTIONAL_CHARACTERISTIC_NOT_IMPLEMENTED, String.format("Error writing Generic Access ServiceTypes characteristic 0x%02X ", command.getCharacteristic()));
                    }
                }
            });
        }
        this.removeListener(command);
    }
    
    public PeripheralPreferredConnectionParameters readPeripheralPreferredConnectionParameters() {
        Log.d(this.Tag, "readPeripheralPreferredConnectionParameters");
        PeripheralPreferredConnectionParameters response = null;
        final CSPeriferalPreferredConnectionParameters command = new CSPeriferalPreferredConnectionParameters();
        if (CSGenericAccessService.characteristicMap.indexOfKey((int)command.getCharacteristic()) < 0) {
            if (this.mIServiceDelegate != null) {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        CSGenericAccessService.this.mIServiceDelegate.didServiceError(CSGenericAccessService.this.mDevice, CSGenericAccessService.this.getType(), CSGenericAccessService.this.mServiceName, "readPeripheralPreferredConnectionParameters", SAErrors.ERROR_OPTIONAL_CHARACTERISTIC_NOT_IMPLEMENTED, String.format("The optional characteristic 0x%02X of Generic access service is not implemented", command.getCharacteristic()));
                    }
                });
            }
            return null;
        }
        this.addListener(command);
        if (this.ReadCharacteristic(command)) {
            if ((response = command.readData()) == null) {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (CSGenericAccessService.this.mIServiceDelegate != null) {
                            CSGenericAccessService.this.mIServiceDelegate.didServiceError(CSGenericAccessService.this.mDevice, CSGenericAccessService.this.getType(), CSGenericAccessService.this.mServiceName, "readPeripheralPreferredConnectionParameters", command.getLastError());
                        }
                    }
                });
            }
        }
        else if (this.mIServiceDelegate != null) {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    CSGenericAccessService.this.mIServiceDelegate.didServiceError(CSGenericAccessService.this.mDevice, CSGenericAccessService.this.getType(), CSGenericAccessService.this.mServiceName, "readPeripheralPreferredConnectionParameters", SAErrors.ERROR_OPTIONAL_CHARACTERISTIC_NOT_IMPLEMENTED, String.format("Error reading characteristic 0x%02X of Generic access service", command.getCharacteristic()));
                }
            });
        }
        this.removeListener(command);
        return response;
    }
    
    public Boolean readCentralAddressResolutionSupport() {
        Log.d(this.Tag, "readCentralAddressResolutionSupport");
        Boolean response = null;
        final CSCentralAddressResolutionSupport command = new CSCentralAddressResolutionSupport();
        if (CSGenericAccessService.characteristicMap.indexOfKey((int)command.getCharacteristic()) < 0) {
            if (this.mIServiceDelegate != null) {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        CSGenericAccessService.this.mIServiceDelegate.didServiceError(CSGenericAccessService.this.mDevice, CSGenericAccessService.this.getType(), CSGenericAccessService.this.mServiceName, "readCentralAddressResolutionSupport", SAErrors.ERROR_OPTIONAL_CHARACTERISTIC_NOT_IMPLEMENTED, String.format("The optional characteristic 0x%02X of Generic access service is not implemented", command.getCharacteristic()));
                    }
                });
            }
            return false;
        }
        this.addListener(command);
        if (this.ReadCharacteristic(command)) {
            if ((response = command.readData()) == null) {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (CSGenericAccessService.this.mIServiceDelegate != null) {
                            CSGenericAccessService.this.mIServiceDelegate.didServiceError(CSGenericAccessService.this.mDevice, CSGenericAccessService.this.getType(), CSGenericAccessService.this.mServiceName, "readCentralAddressResolutionSupport", command.getLastError());
                        }
                    }
                });
            }
        }
        else if (this.mIServiceDelegate != null) {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    CSGenericAccessService.this.mIServiceDelegate.didServiceError(CSGenericAccessService.this.mDevice, CSGenericAccessService.this.getType(), CSGenericAccessService.this.mServiceName, "readCentralAddressResolutionSupport", SAErrors.ERROR_OPTIONAL_CHARACTERISTIC_NOT_IMPLEMENTED, String.format("Error reading characteristic 0x%02X of Generic access service", command.getCharacteristic()));
                }
            });
        }
        this.removeListener(command);
        return response;
    }
    
    public boolean readResolvablePrivateAddressOnly() {
        Log.d(this.Tag, "readResolvablePrivateAddressOnly");
        Boolean response = null;
        final CSResolvablePrivateAddressOnly command = new CSResolvablePrivateAddressOnly();
        if (CSGenericAccessService.characteristicMap.indexOfKey((int)command.getCharacteristic()) < 0) {
            if (this.mIServiceDelegate != null) {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        CSGenericAccessService.this.mIServiceDelegate.didServiceError(CSGenericAccessService.this.mDevice, CSGenericAccessService.this.getType(), CSGenericAccessService.this.mServiceName, "readResolvablePrivateAddressOnly", SAErrors.ERROR_OPTIONAL_CHARACTERISTIC_NOT_IMPLEMENTED, String.format("The optional characteristic 0x%02X of Generic access service is not implemented", command.getCharacteristic()));
                    }
                });
            }
            return false;
        }
        this.addListener(command);
        this.removeListener(command);
        if (this.ReadCharacteristic(command)) {
            if ((response = command.readData()) == null) {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (CSGenericAccessService.this.mIServiceDelegate != null) {
                            CSGenericAccessService.this.mIServiceDelegate.didServiceError(CSGenericAccessService.this.mDevice, CSGenericAccessService.this.getType(), CSGenericAccessService.this.mServiceName, "readResolvablePrivateAddressOnly", command.getLastError());
                        }
                    }
                });
            }
        }
        else if (this.mIServiceDelegate != null) {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    CSGenericAccessService.this.mIServiceDelegate.didServiceError(CSGenericAccessService.this.mDevice, CSGenericAccessService.this.getType(), CSGenericAccessService.this.mServiceName, "readResolvablePrivateAddressOnly", SAErrors.ERROR_OPTIONAL_CHARACTERISTIC_NOT_IMPLEMENTED, String.format("Error reading characteristic 0x%02X of Generic access service", command.getCharacteristic()));
                }
            });
        }
        return response;
    }
    
    private boolean ReadCharacteristic(final SACommand command) {
        Log.d(this.Tag, "ReadCharacteristic: " + ToHexString(command.getCharacteristic()));
        return this.mConnectedGatt.readCharacteristic((BluetoothGattCharacteristic)CSGenericAccessService.characteristicMap.get((int)command.getCharacteristic()));
    }
    
    private boolean WriteCharacteristic(final SACommand command) {
        Log.d(this.Tag, "readControlPoint");
        Log.i(this.Tag, "Control Point Write: " + ToHexString(command.getCharacteristic()) + " value: " + command.getHex());
        final BluetoothGattCharacteristic characteristic = (BluetoothGattCharacteristic)CSGenericAccessService.characteristicMap.get((int)command.getCharacteristic());
        command.setValue(characteristic);
        return this.mConnectedGatt.writeCharacteristic(characteristic);
    }
    
    @Override
    BluetoothGattCallback getGattCallBack() {
        return new BluetoothGattCallback() {
            private String connectionState(final int status) {
                switch (status) {
                    case 2: {
                        return "Connected";
                    }
                    case 0: {
                        return "Disconnected";
                    }
                    case 1: {
                        return "Connecting";
                    }
                    case 3: {
                        return "Disconnecting";
                    }
                    default: {
                        return String.valueOf(status);
                    }
                }
            }
            
            public void onConnectionStateChange(final BluetoothGatt gatt, final int status, final int newState) {
                Log.d(CSGenericAccessService.this.Tag, "Connection State Change: " + status + " -> " + this.connectionState(newState) + " API = " + Build.VERSION.SDK_INT);
                if (status == 0 && newState == 2) {
                    if (CSGenericAccessService.this.mServiceStatus.compare(1) || CSGenericAccessService.this.mServiceStatus.compare(3)) {
                        Log.d(CSGenericAccessService.this.Tag, "Before Gatt discovery services");
                        gatt.discoverServices();
                    }
                }
                else if (status == 0 && newState == 0) {
                    if (Build.VERSION.SDK_INT >= 21 || CSGenericAccessService.this.mServiceStatus.compare(3)) {
                        if (CSGenericAccessService.this.mIBundle != null) {
                            try {
                                CSGenericAccessService.this.mIBundle.onServiceStopped(CSGenericAccessService.this.mDevice, ServiceTypes.GENERIC_ACCESS_SERVICE, CSGenericAccessService.this.mHandler);
                            }
                            catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                        if (CSGenericAccessService.this.mServiceStatus.compare(3)) {
                            CSGenericAccessService.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (CSGenericAccessService.this.mIServiceDelegate != null) {
                                        CSGenericAccessService.this.mIServiceDelegate.didServicePause(CSGenericAccessService.this.mDevice, CSGenericAccessService.this.getType());
                                    }
                                }
                            });
                        }
                    }
                    else {
                        CSGenericAccessService.this.mServiceStatus.set(1);
                        Log.d(CSGenericAccessService.this.Tag, "Try to reconnect from error:" + status);
                        CSGenericAccessService.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (CSGenericAccessService.this.isWriterStarted) {
                                    CSGenericAccessService.this.mWriter.terminate();
                                    try {
                                        CSGenericAccessService.this.mThreadWriter.join();
                                        Log.d(CSGenericAccessService.this.Tag, "Thread terminated");
                                        CSGenericAccessService.this.mWriter = CSGenericAccessService.this.getTheWriter();
                                        CSGenericAccessService.this.mThreadWriter = new Thread(CSGenericAccessService.this.mWriter);
                                    }
                                    catch (InterruptedException ex) {}
                                }
                                CSGenericAccessService.this.pause();
                                CSGenericAccessService.this.stop();
                                CSGenericAccessService.this.mServiceStatus.set(0);
                                try {
                                    CSGenericAccessService.this.start(CSGenericAccessService.this.mIServiceDelegate);
                                }
                                catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    CSGenericAccessService.this.resume();
                                }
                                catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                    if (CSGenericAccessService.this.mCountDownLatch != null) {
                        CSGenericAccessService.this.mCountDownLatch.countDown();
                    }
                }
                else if ((status == 8 || status == 133 || status == 59 || status == 34) && newState == 0) {
                    CSGenericAccessService.this.mServiceStatus.set(1);
                    Log.d(CSGenericAccessService.this.Tag, "Try to reconnect from error:" + status);
                    CSGenericAccessService.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (CSGenericAccessService.this.isWriterStarted) {
                                CSGenericAccessService.this.mWriter.terminate();
                                try {
                                    CSGenericAccessService.this.mThreadWriter.join();
                                    Log.d(CSGenericAccessService.this.Tag, "Thread terminated");
                                    CSGenericAccessService.this.mWriter = CSGenericAccessService.this.getTheWriter();
                                    CSGenericAccessService.this.mThreadWriter = new Thread(CSGenericAccessService.this.mWriter);
                                }
                                catch (InterruptedException ex) {}
                            }
                            CSGenericAccessService.this.mConnectedGatt.close();
                            try {
                                CSGenericAccessService.this.resume();
                            }
                            catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
                else if (status == 133 && newState == 2 && Build.VERSION.SDK_INT < 21) {
                    CSGenericAccessService.this.mServiceStatus.set(1);
                    Log.d(CSGenericAccessService.this.Tag, "Try to reconnect from error =" + status + " new status = " + newState + " API = " + Build.VERSION.SDK_INT);
                    CSGenericAccessService.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (CSGenericAccessService.this.isWriterStarted) {
                                CSGenericAccessService.this.mWriter.terminate();
                                try {
                                    CSGenericAccessService.this.mThreadWriter.join();
                                    Log.d(CSGenericAccessService.this.Tag, "Thread terminated");
                                    CSGenericAccessService.this.mWriter = CSGenericAccessService.this.getTheWriter();
                                    CSGenericAccessService.this.mThreadWriter = new Thread(CSGenericAccessService.this.mWriter);
                                }
                                catch (InterruptedException ex) {}
                            }
                            CSGenericAccessService.this.mConnectedGatt.close();
                            try {
                                CSGenericAccessService.this.resume();
                            }
                            catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
                else if (status != 0) {
                    try {
                        throw new SAException.UnmanagedConnectionException(status, newState);
                    }
                    catch (SAException.UnmanagedConnectionException e2) {
                        e2.printStackTrace();
                    }
                }
            }
            
            public void onServicesDiscovered(final BluetoothGatt gatt, final int status) {
                Log.d(CSGenericAccessService.this.Tag, "onServicesDiscovered");
                if (status == 0) {
                    final int p = 0;
                    CSGenericAccessService.this.uuidService = BluetoothUtils.get128BitBluetoothUUID(6144L);
                    final BluetoothGattService gattService = CSGenericAccessService.this.mConnectedGatt.getService(CSGenericAccessService.this.uuidService);
                    if (gattService != null) {
                        Log.i(CSGenericAccessService.this.Tag, "ServiceTypes characteristic UUID found: " + gattService.getUuid().toString());
                        final List<BluetoothGattCharacteristic> gattCharacteristics = (List<BluetoothGattCharacteristic>)gattService.getCharacteristics();
                        for (final BluetoothGattCharacteristic gattCharacteristic : gattCharacteristics) {
                            final Long gattKey = BluetoothUtils.get16BitBluetoothUUID(gattCharacteristic.getUuid());
                            CSGenericAccessService.characteristicMap.put(gattKey.intValue(), gattCharacteristic);
                        }
                        if (CSGenericAccessService.this.mServiceStatus.compare(1) || CSGenericAccessService.this.mServiceStatus.compare(3)) {
                            if (CSGenericAccessService.this.mIBundle != null) {
                                try {
                                    CSGenericAccessService.this.mIBundle.onServiceStarted(CSGenericAccessService.this.mDevice, ServiceTypes.GENERIC_ACCESS_SERVICE, CSGenericAccessService.this.mHandler);
                                }
                                catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                            CSGenericAccessService.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (CSGenericAccessService.this.mServiceStatus.compare(1)) {
                                        Log.d(CSGenericAccessService.this.Tag, "ServiceTypes discovered and connected");
                                        if (CSGenericAccessService.this.mIServiceDelegate != null) {
                                            CSGenericAccessService.this.mIServiceDelegate.didServiceConnect(CSGenericAccessService.this.mDevice, CSGenericAccessService.this.getType());
                                        }
                                    }
                                    else {
                                        if (CSGenericAccessService.this.mIServiceDelegate != null) {
                                            CSGenericAccessService.this.mIServiceDelegate.didServiceResume(CSGenericAccessService.this.mDevice, CSGenericAccessService.this.getType());
                                        }
                                        Log.d(CSGenericAccessService.this.Tag, "ServiceTypes resumed");
                                    }
                                }
                            });
                            CSGenericAccessService.this.mThreadWriter.start();
                            Log.d(CSGenericAccessService.this.Tag, "THREAD is started");
                            CSGenericAccessService.this.mServiceStatus.set(2);
                        }
                    }
                    else {
                        Log.i(CSGenericAccessService.this.Tag, "ServiceTypes not found for UUID: " + CSGenericAccessService.this.uuidService.toString());
                        if (CSGenericAccessService.this.mIBundle != null) {
                            try {
                                CSGenericAccessService.this.mIBundle.onServiceError(CSGenericAccessService.this.mDevice, CSGenericAccessService.this.getType(), SAErrors.ERROR_SERVICE_NOT_FOUND);
                            }
                            catch (InterruptedException e2) {
                                e2.printStackTrace();
                            }
                        }
                        CSGenericAccessService.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (CSGenericAccessService.this.mIServiceDelegate != null) {
                                    CSGenericAccessService.this.mIServiceDelegate.didServiceError(CSGenericAccessService.this.mDevice, CSGenericAccessService.this.getType(), CSGenericAccessService.this.mServiceName, "onServicesDiscovered", SAErrors.ERROR_SERVICE_NOT_FOUND);
                                }
                            }
                        });
                    }
                    if (CSGenericAccessService.this.firstInitialization && CSGenericAccessService.this.mServiceStatus.compare(2)) {
                        CSGenericAccessService.this.mThreadStart.start();
                    }
                }
            }
            
            public void onCharacteristicWrite(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic, final int status) {
                Log.d(CSGenericAccessService.this.Tag, "onCharacteristicWrite: " + ToHexString(characteristic.getValue()));
                synchronized (CSGenericAccessService.this.subscribers) {
                    for (final SACommand command : CSGenericAccessService.this.subscribers) {
                        command.setCharacteristic(new SensoriaGattCharacteristic(characteristic, status));
                    }
                }
            }
            
            public void onCharacteristicRead(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic, final int status) {
                Log.d(CSGenericAccessService.this.Tag, "onCharacteristicRead:" + ToHexString(characteristic.getValue()));
                synchronized (CSGenericAccessService.this.subscribers) {
                    for (final SACommand command : CSGenericAccessService.this.subscribers) {
                        command.setCharacteristic(new SensoriaGattCharacteristic(characteristic, status));
                    }
                }
            }
            
            public void onCharacteristicChanged(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {
                Log.d(CSGenericAccessService.this.Tag, "onCharacteristicChanged: " + ToHexString(characteristic.getValue()));
                synchronized (CSGenericAccessService.this.subscribers) {
                    for (final SACommand command : CSGenericAccessService.this.subscribers) {
                        command.setCharacteristic(new SensoriaGattCharacteristic(characteristic));
                    }
                }
            }
        };
    }
    
    private class StopObject
    {
    }
    
    private class DeviceName
    {
        String deviceName;
        
        DeviceName(final String deviceName) {
            this.deviceName = deviceName;
        }
    }
    
    private class AppearanceClass
    {
        Appearance appearance;
        
        AppearanceClass(final Appearance appearance) {
            this.appearance = appearance;
        }
    }
}
