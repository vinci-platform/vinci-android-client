/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: SASensoriaControlPointService.java
 * Author: sdelic
 *
 * History:  21.2.22. 10:43 created
 */

//
// Decompiled by Procyon v0.5.36
// 

package com.comtrade.feature_sensoria.sensorialibrary;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.os.Build;
import android.os.Handler;
import android.util.Log;

import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class SASensoriaControlPointService extends SAService implements Serializable
{
    private final UUID UUID_SENSORIA_CORE_CONTROL_POINT_SERVICE;
    private final UUID UUID_SENSORIA_CORE_CONTROL_POINT_RX_CHARACTERISTIC;
    private final UUID UUID_SENSORIA_CORE_CONTROL_POINT_TX_CHARACTERISTIC;
    private final UUID UUID_NORDIC_DFU_SERVICE;
    private int mSDVersion;
    private BluetoothGattCharacteristic mCharacteristicTX;
    private BluetoothGattCharacteristic mCharacteristicRX;
    private LinkedBlockingQueue<Object> mwcQueue;
    
    @Override
    protected void runOnUiThread(final Runnable runnable) {
        this.mHandler.post(runnable);
    }
    
    private SASensoriaControlPointService() {
        this.UUID_SENSORIA_CORE_CONTROL_POINT_SERVICE = UUID.fromString("1cac0001-656e-696c-4b5f-5f6e6167654e");
        this.UUID_SENSORIA_CORE_CONTROL_POINT_RX_CHARACTERISTIC = UUID.fromString("1CAC0002-656E-696C-4B5F-5F6E6167654E");
        this.UUID_SENSORIA_CORE_CONTROL_POINT_TX_CHARACTERISTIC = UUID.fromString("1CAC0003-656E-696C-4B5F-5F6E6167654E");
        this.UUID_NORDIC_DFU_SERVICE = UUID.fromString("0000fe59-0000-1000-8000-00805f9b34fb");
        this.mSDVersion = 0;
        this.mCharacteristicTX = null;
        this.mCharacteristicRX = null;
        this.mwcQueue = new LinkedBlockingQueue<Object>();
    }
    
    public SASensoriaControlPointService(final String deviceMac) {
        super(deviceMac);
        this.UUID_SENSORIA_CORE_CONTROL_POINT_SERVICE = UUID.fromString("1cac0001-656e-696c-4b5f-5f6e6167654e");
        this.UUID_SENSORIA_CORE_CONTROL_POINT_RX_CHARACTERISTIC = UUID.fromString("1CAC0002-656E-696C-4B5F-5F6E6167654E");
        this.UUID_SENSORIA_CORE_CONTROL_POINT_TX_CHARACTERISTIC = UUID.fromString("1CAC0003-656E-696C-4B5F-5F6E6167654E");
        this.UUID_NORDIC_DFU_SERVICE = UUID.fromString("0000fe59-0000-1000-8000-00805f9b34fb");
        this.mSDVersion = 0;
        this.mCharacteristicTX = null;
        this.mCharacteristicRX = null;
        this.mwcQueue = new LinkedBlockingQueue<Object>();
        Log.d(this.Tag, "SASensoriaControlPointService:ctor");
        this.mServiceName = "Sensoria Core Control Point ServiceTypes";
        this.uuidService = this.UUID_SENSORIA_CORE_CONTROL_POINT_SERVICE;
    }
    
    public SASensoriaControlPointService(final String deviceMac, final BlockingQueue<SABundleObject> saBundle) {
        super(deviceMac, saBundle);
        this.UUID_SENSORIA_CORE_CONTROL_POINT_SERVICE = UUID.fromString("1cac0001-656e-696c-4b5f-5f6e6167654e");
        this.UUID_SENSORIA_CORE_CONTROL_POINT_RX_CHARACTERISTIC = UUID.fromString("1CAC0002-656E-696C-4B5F-5F6E6167654E");
        this.UUID_SENSORIA_CORE_CONTROL_POINT_TX_CHARACTERISTIC = UUID.fromString("1CAC0003-656E-696C-4B5F-5F6E6167654E");
        this.UUID_NORDIC_DFU_SERVICE = UUID.fromString("0000fe59-0000-1000-8000-00805f9b34fb");
        this.mSDVersion = 0;
        this.mCharacteristicTX = null;
        this.mCharacteristicRX = null;
        this.mwcQueue = new LinkedBlockingQueue<Object>();
        Log.d(this.Tag, "SASensoriaControlPointService:ctor");
        this.mServiceName = "Sensoria Core Control Point ServiceTypes";
        this.uuidService = this.UUID_SENSORIA_CORE_CONTROL_POINT_SERVICE;
    }
    
    public ServiceTypes getType() {
        return ServiceTypes.SENSORIA_CONTROL_POINT_SERVICE;
    }
    
    public void start(final SAServiceInterface delegate) throws InterruptedException {
        Log.d(this.Tag, "SASensoriaControlPointService:start");
        this.mHandler = new Handler();
        try {
            super.start(delegate);
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    
    @Override
    protected void setOptionalWriter(final boolean force) {
        if (this.mWriter == null || force) {
            this.mWriter = this.getTheWriter();
            this.mThreadWriter = new Thread(this.mWriter);
        }
    }
    
    @Override
    protected void deleteOptionalWriter() {
        if (this.mServiceStatus.compare(2) && this.isWriterStarted) {
            this.mWriter.terminate();
            try {
                this.mThreadWriter.join();
                this.mWriter = null;
                this.mThreadWriter = null;
            }
            catch (InterruptedException ex) {}
        }
    }
    
    public void resume() throws InterruptedException {
        Log.d(this.Tag, "SASensoriaControlPointService:resume");
        super.resume();
    }
    
    public void pause() {
        Log.d(this.Tag, "SASensoriaControlPointService:pause");
        super.pause();
    }
    
    public void stop() {
        Log.d(this.Tag, "SASensoriaControlPointService:stop");
        super.stop();
    }
    
    public void dispose() {
    }
    
    public Integer readProtocolVersion() {
        Log.d(this.Tag, "readProtocolVersion");
        Integer response = null;
        final SAReadProtocolVersion command = new SAReadProtocolVersion();
        this.addListener(command);
        if (this.WriteControlPoint(command.getCommand())) {
            response = command.readInteger();
            if (response == null) {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                            SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "readProtocolVersion", command.getLastError());
                        }
                    }
                });
            }
        }
        else {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                        SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "readProtocolVersion", SAErrors.ERROR_CANNOT_WRITE_CONTROL_POINT);
                    }
                }
            });
        }
        this.removeListener(command);
        return response;
    }
    
    public SystemStatus readSystemStatus() {
        Log.d(this.Tag, "readSystemStatus");
        final ByteBuffer payload = ByteBuffer.allocate(4);
        byte[] response = null;
        SystemStatus systemStatus = null;
        final SAReadSystemStatus command = new SAReadSystemStatus();
        this.addListener(command);
        if (this.WriteControlPoint(command.getCommand())) {
            if ((response = command.readData()) == null) {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                            SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "readSystemStatus", command.getLastError());
                        }
                    }
                });
            }
            else {
                payload.position(0);
                payload.put(response, 0, 4);
                Log.i(this.Tag, "Payload=" + ToHexString(payload.array()));
                systemStatus = new SystemStatus(payload.array());
            }
        }
        else {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                        SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "readSystemStatus", SAErrors.ERROR_CANNOT_WRITE_CONTROL_POINT);
                    }
                }
            });
        }
        this.removeListener(command);
        return systemStatus;
    }
    
    public SAChargingState getChargingState() {
        Log.d(this.Tag, "getChargingState");
        byte[] response = null;
        final SAReadSystemStatus command = new SAReadSystemStatus();
        this.addListener(command);
        if (this.WriteControlPoint(command.getCommand())) {
            if ((response = command.readData()) == null) {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                            SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "isChargingState", command.getLastError());
                        }
                    }
                });
            }
        }
        else {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                        SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "isChargingState", SAErrors.ERROR_CANNOT_WRITE_CONTROL_POINT);
                    }
                }
            });
        }
        this.removeListener(command);
        final byte isCharging = (byte)(response[0] & 0x1);
        final byte isChargingCompleted = (byte)((response[0] & 0x4) >> 1);
        final byte chargingState = (byte)(isChargingCompleted | isCharging);
        switch (chargingState) {
            case 0: {
                return SAChargingState.NOT_CHARGING;
            }
            case 1: {
                return SAChargingState.CHARGING;
            }
            case 2:
            case 3: {
                return SAChargingState.CHARGING_COMPLETED;
            }
            default: {
                return SAChargingState.ERROR;
            }
        }
    }
    
    public boolean loadSystemWideDefaultSettings() {
        boolean returnCode = true;
        Log.d(this.Tag, "loadSystemWideDefaultSettings");
        final SALoadDefaultSettings command = new SALoadDefaultSettings();
        this.addListener(command);
        if (this.WriteControlPoint(command.getCommand())) {
            if (!command.upload()) {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                            SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "loadSystemWideDefaultSettings", command.getLastError());
                        }
                    }
                });
                returnCode = false;
            }
        }
        else {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                        SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "loadSystemWideDefaultSettings", SAErrors.ERROR_CANNOT_WRITE_CONTROL_POINT);
                    }
                }
            });
            returnCode = false;
        }
        this.removeListener(command);
        return returnCode;
    }
    
    public boolean resetPowerCycleSystem() {
        Log.d(this.Tag, "resetPowerCycleSystem");
        boolean retCode = true;
        final SAResetPowerCycleSystem command = new SAResetPowerCycleSystem();
        this.addListener(command);
        if (!this.WriteControlPoint(command.getCommand())) {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                        SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "resetPowerCycleSystem", SAErrors.ERROR_CANNOT_WRITE_CONTROL_POINT);
                    }
                }
            });
            retCode = false;
        }
        else {
            this.pause();
            this.stop();
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                        SASensoriaControlPointService.this.mIServiceDelegate.didServiceReset(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType());
                    }
                }
            });
        }
        this.removeListener(command);
        return retCode;
    }
    
    public Short readFastAdvertisingTimeout() {
        Log.d(this.Tag, "SASensoriaControlPointService:readFastAdvertisingTimeout");
        Short retval = null;
        final SAReadAdvertisingTimeout command = new SAReadAdvertisingTimeout();
        this.addListener(command);
        if (this.WriteControlPoint(command.getCommand())) {
            if ((retval = command.readFastTimeout()) == null) {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                            SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "readFastAdvertisingTimeout", command.getLastError());
                        }
                    }
                });
            }
        }
        else {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                        SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "readFastAdvertisingTimeout", SAErrors.ERROR_CANNOT_WRITE_CONTROL_POINT);
                    }
                }
            });
        }
        this.removeListener(command);
        return retval;
    }
    
    public Short readSlowAdvertisingTimeout() {
        Log.d(this.Tag, "SASensoriaControlPointService:readSlowAdvertisingTimeout");
        Short retval = null;
        final SAReadAdvertisingTimeout command = new SAReadAdvertisingTimeout();
        this.addListener(command);
        if (this.WriteControlPoint(command.getCommand())) {
            if ((retval = command.readSlowTimeout()) == null) {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                            SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "readSlowAdvertisingTimeout", command.getLastError());
                        }
                    }
                });
            }
        }
        else {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                        SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "readSlowAdvertisingTimeout", SAErrors.ERROR_CANNOT_WRITE_CONTROL_POINT);
                    }
                }
            });
        }
        this.removeListener(command);
        return retval;
    }
    
    public boolean writeAdvertisingTimeout(short fastTimeout, short slowTimeout) {
        Log.d(this.Tag, "SASensoriaControlPointService:writeAdvertisingTimeout");
        if (fastTimeout < 10 && fastTimeout != 0) {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "writeAdvertisingTimeout", SAErrors.ERROR_INVALID_TIMEOUT_RANGE, "Timeout < 10 sec. Assuming value of 10 sec.");
                }
            });
            fastTimeout = 10;
        }
        if (fastTimeout > 1800) {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "writeAdvertisingTimeout", SAErrors.ERROR_INVALID_TIMEOUT_RANGE, "Timeout > 1800 sec. Assuming value of 1800 sec.");
                }
            });
            fastTimeout = 1800;
        }
        if (slowTimeout < 10 && slowTimeout != 0) {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "writeAdvertisingTimeout", SAErrors.ERROR_INVALID_TIMEOUT_RANGE, "Timeout < 10 sec. Assuming value of 10 sec.");
                }
            });
            slowTimeout = 10;
        }
        if (slowTimeout > 1800) {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "writeAdvertisingTimeout", SAErrors.ERROR_INVALID_TIMEOUT_RANGE, "Timeout > 1800 sec. Assuming value of 1800 sec.");
                }
            });
            slowTimeout = 1800;
        }
        final AdvertisingTimeout advertisingTimeout = new AdvertisingTimeout(fastTimeout, slowTimeout);
        try {
            this.mwcQueue.put(advertisingTimeout);
        }
        catch (InterruptedException e) {
            return false;
        }
        return true;
    }
    
    private void writeAdvertisingTimeoutEx(final short fastTimeout, final short slowTimeout) {
        Log.d(this.Tag, "writeAdvertisingTimeoutEx");
        final ByteBuffer payload = ByteBuffer.allocate(4).order(ByteOrder.BIG_ENDIAN);
        payload.put(0, (byte)(fastTimeout >> 8 & 0xFF));
        payload.put(1, (byte)(fastTimeout & 0xFF));
        payload.put(2, (byte)(slowTimeout >> 8 & 0xFF));
        payload.put(3, (byte)(slowTimeout & 0xFF));
        Log.i(this.Tag, ToHexString(payload.array()));
        final SAWriteAdvertisingTimeout command = new SAWriteAdvertisingTimeout(payload.array());
        this.addListener(command);
        if (this.WriteControlPoint(command.getCommand())) {
            if (!command.writeTimeout()) {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                            SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "writeAdvertisingTimeoutEx", command.getLastError());
                        }
                    }
                });
            }
            else {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                            ((SAServiceControlPointInterface)SASensoriaControlPointService.this.mIServiceDelegate).didServiceTimeoutSet(fastTimeout, slowTimeout);
                        }
                    }
                });
            }
        }
        else {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                        SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "writeAdvertisingTimeoutEx", SAErrors.ERROR_CANNOT_WRITE_CONTROL_POINT);
                    }
                }
            });
        }
        this.removeListener(command);
    }
    
    public boolean writeWakeUpDrivingSources(final byte tap, final byte shake, final byte tilt) {
        Log.d(this.Tag, "SASensoriaControlPointService:writeWakeUpDrivingSources");
        if (tap < 0 || tap > 31) {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "writeWakeUpDrivingSources", SAErrors.ERROR_SENSITIVITY_OUT_OF_RANGE);
                }
            });
            return false;
        }
        if (shake < 0 || shake > 31) {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "writeWakeUpDrivingSources", SAErrors.ERROR_SENSITIVITY_OUT_OF_RANGE);
                }
            });
            return false;
        }
        if (tilt < 0 || tilt > 31) {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "writeWakeUpDrivingSources", SAErrors.ERROR_SENSITIVITY_OUT_OF_RANGE);
                }
            });
            return false;
        }
        final WakeUpDrivingSources wakeUpDrivingSources = new WakeUpDrivingSources(tap, shake, tilt);
        try {
            this.mwcQueue.put(wakeUpDrivingSources);
        }
        catch (InterruptedException e) {
            return false;
        }
        return true;
    }
    
    private void writeWakeUpDrivingSourcesEx(final byte tap, final byte shake, final byte tilt) {
        Log.d(this.Tag, "writeWakeUpDrivingSourcesEx");
        final ByteBuffer payload = ByteBuffer.allocate(5).order(ByteOrder.BIG_ENDIAN);
        payload.put(0, (byte)4);
        payload.put(1, tap);
        payload.put(2, shake);
        payload.put(3, tilt);
        payload.put(4, (byte)0);
        Log.i(this.Tag, ToHexString(payload.array()));
        final SAAutoWakeUpMode command = new SAAutoWakeUpMode(payload.array());
        this.addListener(command);
        if (this.WriteControlPoint(command.getCommand())) {
            if (!command.enableWakeUpDrivingSources()) {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                            SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "writeWakeUpDrivingSourcesEx", command.getLastError());
                        }
                    }
                });
            }
            else {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                            ((SAServiceControlPointInterface)SASensoriaControlPointService.this.mIServiceDelegate).didServiceAutoWakeUpModeSourcesSet(tap, shake, tilt);
                        }
                    }
                });
            }
        }
        else {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                        SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "initializeHawriteWakeUpDrivingSourcesExpticDriverEx", SAErrors.ERROR_CANNOT_WRITE_CONTROL_POINT);
                    }
                }
            });
        }
        this.removeListener(command);
    }
    
    public boolean pauseLED() {
        Log.d(this.Tag, "pauseLED");
        boolean retCode = true;
        final SAPauseLED command = new SAPauseLED();
        this.addListener(command);
        if (this.WriteControlPoint(command.getCommand())) {
            if (!command.pauseLED()) {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                            SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "pauseLED", command.getLastError());
                        }
                    }
                });
                retCode = false;
            }
            else {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                            ((SAServiceControlPointInterface)SASensoriaControlPointService.this.mIServiceDelegate).didPauseLED(true);
                        }
                    }
                });
            }
        }
        else {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                        SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "pauseLED", SAErrors.ERROR_CANNOT_WRITE_CONTROL_POINT);
                    }
                }
            });
            retCode = false;
        }
        this.removeListener(command);
        return retCode;
    }
    
    public boolean resumeLED() {
        Log.d(this.Tag, "resumeLED");
        boolean retCode = true;
        final SAResumeLED command = new SAResumeLED();
        this.addListener(command);
        if (this.WriteControlPoint(command.getCommand())) {
            if (!command.resumeLED()) {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                            SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "resumeLED", command.getLastError());
                        }
                    }
                });
                retCode = false;
            }
            else {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                            ((SAServiceControlPointInterface)SASensoriaControlPointService.this.mIServiceDelegate).didPauseLED(false);
                        }
                    }
                });
            }
        }
        else {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                        SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "resumeLED", SAErrors.ERROR_CANNOT_WRITE_CONTROL_POINT);
                    }
                }
            });
            retCode = false;
        }
        this.removeListener(command);
        return retCode;
    }
    
    public boolean driveLED(final String color, final byte blinkCount, final byte onTime, final byte offTime, final byte reset) {
        Log.d(this.Tag, "driveLED");
        byte colorToBlink = 0;
        switch (color) {
            case "red": {
                colorToBlink = 1;
                break;
            }
            case "green": {
                colorToBlink = 2;
                break;
            }
            case "yellow": {
                colorToBlink = 3;
                break;
            }
            case "blue": {
                colorToBlink = 4;
                break;
            }
            case "purple": {
                colorToBlink = 5;
                break;
            }
            case "teal": {
                colorToBlink = 6;
                break;
            }
            case "white": {
                colorToBlink = 7;
                break;
            }
            default: {
                colorToBlink = 7;
                break;
            }
        }
        boolean retCode = true;
        final SADriveLED command = new SADriveLED(new byte[] { colorToBlink, blinkCount, onTime, offTime, reset });
        this.addListener(command);
        if (this.WriteControlPoint(command.getCommand())) {
            if (!command.driveLED()) {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                            SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "driveLED", command.getLastError());
                        }
                    }
                });
                retCode = false;
            }
            else {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                            ((SAServiceControlPointInterface)SASensoriaControlPointService.this.mIServiceDelegate).didPauseLED(true);
                        }
                    }
                });
            }
        }
        else {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                        SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "driveLED", SAErrors.ERROR_CANNOT_WRITE_CONTROL_POINT);
                    }
                }
            });
            retCode = false;
        }
        this.removeListener(command);
        return retCode;
    }
    
    public Short readAccelerometerSamplingRate() {
        Log.d(this.Tag, "readAccelerometerSamplingRate");
        Short retval = null;
        final SAReadAccelerometerSamplingRate command = new SAReadAccelerometerSamplingRate();
        this.addListener(command);
        if (this.WriteControlPoint(command.getCommand())) {
            if ((retval = command.readSamplingRate()) == null) {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                            SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "readAccelerometerSamplingRate", command.getLastError());
                        }
                    }
                });
            }
        }
        else {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                        SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "readAccelerometerSamplingRate", SAErrors.ERROR_CANNOT_WRITE_CONTROL_POINT);
                    }
                }
            });
        }
        this.removeListener(command);
        return retval;
    }
    
    public boolean writeAccelerometerSamplingRate(final short samplingRate) {
        Log.d(this.Tag, "writeAccelorometerSamplingRate");
        if (samplingRate > 1660 || samplingRate < 13) {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                        SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "writeAccelerometerSamplingRate", SAErrors.ERROR_INVALID_SAMPLING_RATE_VALUES, "Possible values for Accelerometer are: 13 Hz, 26 Hz, 52 Hz, 104 Hz, 208 Hz, 416 Hz, 833 Hz, 1660 Hz");
                    }
                }
            });
            return false;
        }
        final AccelerometerSamplingRate accelorometerSamplingRate = new AccelerometerSamplingRate(samplingRate);
        try {
            this.mwcQueue.put(accelorometerSamplingRate);
        }
        catch (InterruptedException e) {
            return false;
        }
        return true;
    }
    
    private void writeAccelerometerSamplingRateEx(final short samplingRate) {
        Log.d(this.Tag, "writeAccelerometerSamplingRateEx");
        final ByteBuffer payload = ByteBuffer.allocate(2).order(ByteOrder.BIG_ENDIAN);
        payload.putShort(0, samplingRate);
        Log.i(this.Tag, ToHexString(payload.array()));
        final SAWriteAccelerometerSamplingRate command = new SAWriteAccelerometerSamplingRate(payload.array());
        this.addListener(command);
        if (this.WriteControlPoint(command.getCommand())) {
            if (!command.writeSamplingRate()) {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                            SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "writeAccelerometerSamplingRateEx", command.getLastError());
                        }
                    }
                });
            }
            else {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                            ((SAServiceControlPointInterface)SASensoriaControlPointService.this.mIServiceDelegate).didServiceAccelerometerSamplingRateSet(samplingRate);
                        }
                    }
                });
            }
        }
        else {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                        SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "writeAccelerometerSamplingRateEx", SAErrors.ERROR_CANNOT_WRITE_CONTROL_POINT);
                    }
                }
            });
        }
        this.removeListener(command);
    }
    
    public Short readAccelerometerSamplingRange() {
        Log.d(this.Tag, "readAccelerometerSamplingRange");
        Short retval = null;
        final SAReadAccelerometerSamplingRange command = new SAReadAccelerometerSamplingRange();
        this.addListener(command);
        if (this.WriteControlPoint(command.getCommand())) {
            if ((retval = command.readSamplingRange()) == null) {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                            SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "readAccelerometerSamplingRange", command.getLastError());
                        }
                    }
                });
            }
        }
        else {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                        SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "readAccelerometerSamplingRange", SAErrors.ERROR_CANNOT_WRITE_CONTROL_POINT);
                    }
                }
            });
        }
        this.removeListener(command);
        return retval;
    }
    
    public boolean writeAccelerometerSamplingRange(final short samplingRange) {
        Log.d(this.Tag, "SASensoriaControlPointService:writeAccelerometerSamplingRange");
        if (samplingRange > 16 || samplingRange < 2) {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                        SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "writeAccelerometerSamplingRange", SAErrors.ERROR_INVALID_SAMPLING_RANGE_VALUES, "Possible values for Accelerometer are: 2 g, 4 g, 8 g, 16 g");
                    }
                }
            });
            return false;
        }
        final AccelerometerSamplingRange acceloremeterSamplingRange = new AccelerometerSamplingRange(samplingRange);
        try {
            this.mwcQueue.put(acceloremeterSamplingRange);
        }
        catch (InterruptedException e) {
            return false;
        }
        return true;
    }
    
    private void writeAccelerometerSamplingRangeEx(final short samplingRange) {
        Log.d(this.Tag, "SASensoriaControlPointService:writeAccelerometerSamplingRangeEx");
        final ByteBuffer payload = ByteBuffer.allocate(2).order(ByteOrder.BIG_ENDIAN);
        payload.putShort(0, samplingRange);
        Log.i(this.Tag, ToHexString(payload.array()));
        final SAWriteAccelerometerSamplingRange command = new SAWriteAccelerometerSamplingRange(payload.array());
        this.addListener(command);
        if (this.WriteControlPoint(command.getCommand())) {
            if (!command.writeSamplingRange()) {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                            SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "writeAccelerometerSamplingRangeEx", command.getLastError());
                        }
                    }
                });
            }
        }
        else {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                        SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "writeAccelerometerSamplingRangeEx", SAErrors.ERROR_CANNOT_WRITE_CONTROL_POINT);
                    }
                }
            });
        }
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                    ((SAServiceControlPointInterface)SASensoriaControlPointService.this.mIServiceDelegate).didServiceAccelerometerSamplingRangeSet(samplingRange);
                }
            }
        });
        this.removeListener(command);
    }
    
    public boolean restoreAccelerometerDefaults() {
        Log.d(this.Tag, "restoreAccelerometerDefaults");
        boolean retCode = true;
        final SARestoreAccelerometerDefaults command = new SARestoreAccelerometerDefaults();
        this.addListener(command);
        if (this.WriteControlPoint(command.getCommand())) {
            if (!command.upload()) {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                            SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "restoreAccelerometerDefaults", command.getLastError());
                        }
                    }
                });
                retCode = false;
            }
        }
        else {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                        SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "restoreAccelerometerDefaults", SAErrors.ERROR_CANNOT_WRITE_CONTROL_POINT);
                    }
                }
            });
            retCode = false;
        }
        this.removeListener(command);
        return retCode;
    }
    
    public Short readGyroscopeSamplingRate() {
        Log.d(this.Tag, "readGyroscopeSamplingRate");
        Short retval = null;
        final SAReadGyroscopeSamplingRate command = new SAReadGyroscopeSamplingRate();
        this.addListener(command);
        if (this.WriteControlPoint(command.getCommand())) {
            if ((retval = command.readSamplingRate()) == null) {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                            SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "readGyroscopeSamplingRate", command.getLastError());
                        }
                    }
                });
            }
        }
        else {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                        SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "readGyroscopeSamplingRate", SAErrors.ERROR_CANNOT_WRITE_CONTROL_POINT);
                    }
                }
            });
        }
        this.removeListener(command);
        return retval;
    }
    
    public boolean writeGyroscopeSamplingRate(final short samplingRate) {
        Log.d(this.Tag, "SASensoriaControlPointService:writeGyroscopeSamplingRate");
        if (samplingRate > 1660 || samplingRate < 13) {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                        SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "writeGyroscopeSamplingRate", SAErrors.ERROR_INVALID_SAMPLING_RATE_VALUES, "Possible values for Gyroscope are: 13 Hz, 26 Hz, 52 Hz, 104 Hz, 208 Hz, 416 Hz, 833 Hz, 1660 Hz");
                    }
                }
            });
            return false;
        }
        final GyroscopeSamplingRate gyroscopeSamplingRate = new GyroscopeSamplingRate(samplingRate);
        try {
            this.mwcQueue.put(gyroscopeSamplingRate);
        }
        catch (InterruptedException e) {
            return false;
        }
        return true;
    }
    
    private void writeGyroscopeSamplingRateEx(final short samplingRate) {
        Log.d(this.Tag, "writeGyroscopeSamplingRateEx");
        final ByteBuffer payload = ByteBuffer.allocate(2).order(ByteOrder.BIG_ENDIAN);
        payload.putShort(0, samplingRate);
        Log.i(this.Tag, ToHexString(payload.array()));
        final SAWriteGyroscopeSamplingRate command = new SAWriteGyroscopeSamplingRate(payload.array());
        this.addListener(command);
        if (this.WriteControlPoint(command.getCommand())) {
            if (!command.writeSamplingRate()) {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                            SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "writeGyroscopeSamplingRateEx", command.getLastError());
                        }
                    }
                });
            }
            else {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                            ((SAServiceControlPointInterface)SASensoriaControlPointService.this.mIServiceDelegate).didServiceGyroscopeSamplingRateSet(samplingRate);
                        }
                    }
                });
            }
        }
        else {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                        SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "writeGyroscopeSamplingRateEx", SAErrors.ERROR_CANNOT_WRITE_CONTROL_POINT);
                    }
                }
            });
        }
        this.removeListener(command);
    }
    
    public Short readGyroscopeSamplingRange() {
        Log.d(this.Tag, "readGyroscopeSamplingRange");
        Short retval = null;
        final SAReadGyroscopeSamplingRange command = new SAReadGyroscopeSamplingRange();
        this.addListener(command);
        if (this.WriteControlPoint(command.getCommand())) {
            if ((retval = command.readSamplingRange()) == null) {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                            SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "readGyroscopeSamplingRange", command.getLastError());
                        }
                    }
                });
            }
        }
        else {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                        SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "readGyroscopeSamplingRange", SAErrors.ERROR_CANNOT_WRITE_CONTROL_POINT);
                    }
                }
            });
        }
        this.removeListener(command);
        return retval;
    }
    
    public boolean writeGyroscopeSamplingRange(final short samplingRange) {
        Log.d(this.Tag, "writeGyroscopeSamplingRange");
        if (samplingRange > 2000 || samplingRange < 245) {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                        SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "writeGyroscopeSamplingRange", SAErrors.ERROR_INVALID_SAMPLING_RANGE_VALUES, "Possible values for Gyroscope are: 245 DPS, 500 DPS, 1000 DPS, 2000 DPS");
                    }
                }
            });
            return false;
        }
        final GyroscopeSamplingRange gyroscopeSamplingRange = new GyroscopeSamplingRange(samplingRange);
        try {
            this.mwcQueue.put(gyroscopeSamplingRange);
        }
        catch (InterruptedException e) {
            return false;
        }
        return true;
    }
    
    private void writeGyroscopeSamplingRangeEx(final short samplingRange) {
        Log.d(this.Tag, "writeGyroscopeSamplingRateEx");
        final ByteBuffer payload = ByteBuffer.allocate(2).order(ByteOrder.BIG_ENDIAN);
        payload.putShort(0, samplingRange);
        Log.i(this.Tag, ToHexString(payload.array()));
        final SAWriteGyroscopeSamplingRange command = new SAWriteGyroscopeSamplingRange(payload.array());
        this.addListener(command);
        if (this.WriteControlPoint(command.getCommand())) {
            if (!command.writeSamplingRange()) {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                            SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "writeGyroscopeSamplingRangeEx", command.getLastError());
                        }
                    }
                });
            }
            else {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                            ((SAServiceControlPointInterface)SASensoriaControlPointService.this.mIServiceDelegate).didServiceGyroscopeSamplingRangeSet(samplingRange);
                        }
                    }
                });
            }
        }
        else {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                        SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "writeGyroscopeSamplingRangeEx", SAErrors.ERROR_CANNOT_WRITE_CONTROL_POINT);
                    }
                }
            });
        }
        this.removeListener(command);
    }
    
    public boolean restoreGyroscopeDefaults() {
        Log.d(this.Tag, "restoreGyroscopeDefaults");
        boolean retCode = true;
        final SARestoreGyroscopeDefaults command = new SARestoreGyroscopeDefaults();
        this.addListener(command);
        if (this.WriteControlPoint(command.getCommand())) {
            if (!command.upload()) {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                            SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "restoreGyroscopeDefaults", command.getLastError());
                        }
                    }
                });
                retCode = false;
            }
        }
        else {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                        SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "restoreGyroscopeDefaults", SAErrors.ERROR_CANNOT_WRITE_CONTROL_POINT);
                    }
                }
            });
            retCode = false;
        }
        this.removeListener(command);
        return retCode;
    }
    
    public Short readMagnetometerSamplingRate() {
        Log.d(this.Tag, "readMagnetometerSamplingRate");
        Short retval = null;
        final SAReadMagnetometerSamplingRate command = new SAReadMagnetometerSamplingRate();
        this.addListener(command);
        if (this.WriteControlPoint(command.getCommand())) {
            if ((retval = command.readSamplingRate()) == null) {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                            SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "readMagnetometerSamplingRate", command.getLastError());
                        }
                    }
                });
            }
        }
        else {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                        SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "readMagnetometerSamplingRate", SAErrors.ERROR_CANNOT_WRITE_CONTROL_POINT);
                    }
                }
            });
        }
        this.removeListener(command);
        return retval;
    }
    
    public boolean writeMagnetometerSamplingRate(final short samplingRate) {
        Log.d(this.Tag, "writeMagnetometerSamplingRate");
        if (samplingRate > 80 || samplingRate < 4) {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                        SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "writeMagnetometerSamplingRate", SAErrors.ERROR_INVALID_SAMPLING_RANGE_VALUES, "Possible values for Magnetometer are: 5 Hz, 10 Hz, 20 Hz, 40 Hz, 80 Hz.");
                    }
                }
            });
            return false;
        }
        final MagnetometerSamplingRate magnetometerSamplingRate = new MagnetometerSamplingRate(samplingRate);
        try {
            this.mwcQueue.put(magnetometerSamplingRate);
        }
        catch (InterruptedException e) {
            return false;
        }
        return true;
    }
    
    private void writeMagnetometerSamplingRateEx(final short samplingRate) {
        Log.d(this.Tag, "writeMagnetometerSamplingRateEx");
        final ByteBuffer payload = ByteBuffer.allocate(2).order(ByteOrder.BIG_ENDIAN);
        payload.putShort(0, samplingRate);
        Log.i(this.Tag, ToHexString(payload.array()));
        final SAWriteMagnetometerSamplingRate command = new SAWriteMagnetometerSamplingRate(payload.array());
        this.addListener(command);
        if (this.WriteControlPoint(command.getCommand())) {
            if (!command.writeSamplingRate()) {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                            SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "writeMagnetometerSamplingRateEx", command.getLastError());
                        }
                    }
                });
            }
            else {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                            ((SAServiceControlPointInterface)SASensoriaControlPointService.this.mIServiceDelegate).didServiceMagnetometerSamplingRateSet(samplingRate);
                        }
                    }
                });
            }
        }
        else {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                        SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "writeMagnetometerSamplingRateEx", SAErrors.ERROR_CANNOT_WRITE_CONTROL_POINT);
                    }
                }
            });
        }
        this.removeListener(command);
    }
    
    public Short readMagnetometerSamplingRange() {
        Log.d(this.Tag, "readMagnetometerSamplingRange");
        Short retval = null;
        final SAReadMagnetometerSamplingRange command = new SAReadMagnetometerSamplingRange();
        this.addListener(command);
        if (this.WriteControlPoint(command.getCommand())) {
            if ((retval = command.readSamplingRange()) == null) {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                            SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "readMagnetometerSamplingRange", command.getLastError());
                        }
                    }
                });
            }
        }
        else {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                        SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "readMagnetometerSamplingRange", SAErrors.ERROR_CANNOT_WRITE_CONTROL_POINT);
                    }
                }
            });
        }
        this.removeListener(command);
        return retval;
    }
    
    public boolean writeMagnetometerSamplingRange(final short samplingRange) {
        Log.d(this.Tag, "writeMagnetometerSamplingRange");
        if (samplingRange > 16 || samplingRange < 4) {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                        SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "writeMagnetometerSamplingRange", SAErrors.ERROR_INVALID_SAMPLING_RANGE_VALUES, "Possible values for Magnetometer are: 4 Gs, 6Gs, 12 Gs, 16 Gs.");
                    }
                }
            });
            return false;
        }
        final MagnetometerSamplingRange magnetometerSamplingRange = new MagnetometerSamplingRange(samplingRange);
        try {
            this.mwcQueue.put(magnetometerSamplingRange);
        }
        catch (InterruptedException e) {
            return false;
        }
        return true;
    }
    
    private void writeMagnetometerSamplingRangeEx(final short samplingRange) {
        Log.d(this.Tag, "writeMagnetometerSamplingRangeEx");
        final ByteBuffer payload = ByteBuffer.allocate(2).order(ByteOrder.BIG_ENDIAN);
        payload.putShort(0, samplingRange);
        Log.i(this.Tag, ToHexString(payload.array()));
        final SAWriteMagnetometerSamplingRange command = new SAWriteMagnetometerSamplingRange(payload.array());
        this.addListener(command);
        if (this.WriteControlPoint(command.getCommand())) {
            if (!command.writeSamplingRange()) {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                            SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "writeMagnetometerSamplingRangeEx", command.getLastError());
                        }
                    }
                });
            }
            else {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                            ((SAServiceControlPointInterface)SASensoriaControlPointService.this.mIServiceDelegate).didServiceMagnetometerSamplingRangeSet(samplingRange);
                        }
                    }
                });
            }
        }
        else {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                        SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "writeMagnetometerSamplingRangeEx", SAErrors.ERROR_CANNOT_WRITE_CONTROL_POINT);
                    }
                }
            });
        }
        this.removeListener(command);
    }
    
    public boolean restoreMagnetometerDefaults() {
        Log.d(this.Tag, "restoreMagnetometerDefaults");
        boolean retCode = true;
        final SARestoreMagnetometerDefaults command = new SARestoreMagnetometerDefaults();
        this.addListener(command);
        if (this.WriteControlPoint(command.getCommand())) {
            if (!command.upload()) {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                            SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "restoreMagnetometerDefaults", command.getLastError());
                        }
                    }
                });
                retCode = false;
            }
        }
        else {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                        SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "restoreMagnetometerDefaults", SAErrors.ERROR_CANNOT_WRITE_CONTROL_POINT);
                    }
                }
            });
            retCode = false;
        }
        this.removeListener(command);
        return retCode;
    }
    
    public Short[] readMagnetometerCalibrationValues() {
        Log.d(this.Tag, "readMagnetometerCalibrationValues");
        Short[] retval = null;
        final SAReadMagnetometerCalibrationValues command = new SAReadMagnetometerCalibrationValues();
        this.addListener(command);
        if (this.WriteControlPoint(command.getCommand())) {
            if ((retval = command.readCalibrationValues()) == null) {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                            SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "readMagnetometerCalibrationValues", command.getLastError());
                        }
                    }
                });
            }
        }
        else {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                        SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "readMagnetometerCalibrationValues", SAErrors.ERROR_CANNOT_WRITE_CONTROL_POINT);
                    }
                }
            });
        }
        this.removeListener(command);
        return retval;
    }
    
    public boolean writeMagnetometerCalibrationValues(final short[] calibrationValues) {
        Log.d(this.Tag, "writeMagnetometerCalibrationValues");
        final MagnetometerCalibrationValues magnetometerCalibrationValues = new MagnetometerCalibrationValues(calibrationValues);
        try {
            this.mwcQueue.put(magnetometerCalibrationValues);
        }
        catch (InterruptedException e) {
            return false;
        }
        return true;
    }
    
    private void writeMagnetometerCalibrationValuesEx(final short[] calibrationValues) {
        Log.d(this.Tag, "writeMagnetometerCalibrationValuesEx");
        final ByteBuffer payload = ByteBuffer.allocate(6).order(ByteOrder.BIG_ENDIAN);
        payload.putShort(0, calibrationValues[0]);
        payload.putShort(2, calibrationValues[1]);
        payload.putShort(4, calibrationValues[2]);
        Log.i(this.Tag, ToHexString(payload.array()));
        final SAWriteMagnetometerCalibrationValues command = new SAWriteMagnetometerCalibrationValues(payload.array());
        this.addListener(command);
        if (this.WriteControlPoint(command.getCommand())) {
            if (!command.writeCalibrationValues()) {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                            SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "writeMagnetometerCalibrationValuesEx", command.getLastError());
                        }
                    }
                });
            }
        }
        else {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                        SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "writeMagnetometerCalibrationValuesEx", SAErrors.ERROR_CANNOT_WRITE_CONTROL_POINT);
                    }
                }
            });
        }
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                    ((SAServiceControlPointInterface)SASensoriaControlPointService.this.mIServiceDelegate).didServiceMagnetometerCalibrationValuesSet(calibrationValues);
                }
            }
        });
        this.removeListener(command);
    }
    
    public boolean triggerMagnetometerCalibrationValues() {
        Log.d(this.Tag, "triggerMagnetometerCalibrationValues");
        boolean retCode = true;
        final SATriggerCalibrationValues command = new SATriggerCalibrationValues();
        this.addListener(command);
        if (this.WriteControlPoint(command.getCommand())) {
            if (!command.triggerCalibration()) {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                            SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "triggerMagnetometerCalibrationValues", command.getLastError());
                        }
                    }
                });
                retCode = false;
            }
            else {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                            ((SAServiceControlPointInterface)SASensoriaControlPointService.this.mIServiceDelegate).didServiceMagnetometerCalibrated(true);
                        }
                    }
                });
            }
        }
        else {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                        SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "triggerMagnetometerCalibrationValues", SAErrors.ERROR_CANNOT_WRITE_CONTROL_POINT);
                    }
                }
            });
            retCode = false;
        }
        this.removeListener(command);
        return retCode;
    }
    
    public ChannelSettings readChannelSettings(final short channelNumber) {
        Log.d(this.Tag, "readChannelSettings");
        ChannelSettings channelSettings = null;
        final ByteBuffer payload = ByteBuffer.allocate(3);
        final SAReadChannelSettings command = new SAReadChannelSettings(channelNumber);
        this.addListener(command);
        if (this.WriteControlPoint(command.getCommand())) {
            final byte[] response;
            if ((response = command.readData()) == null) {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                            SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "readChannelSettings", command.getLastError());
                        }
                    }
                });
                Log.d(this.Tag, "Cannot read channel Settings");
            }
            else {
                payload.position(0);
                payload.put(response, 0, 3);
                Log.i(this.Tag, "Payload=" + ToHexString(payload.array()));
                channelSettings = new ChannelSettings(payload.array());
                Log.d(this.Tag, String.format("Channel Settings/Sampling:%d, Protocol:%s", channelSettings.getSamplingPeriod(), channelSettings.getProtocol()));
            }
        }
        else {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                        SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "readChannelSettings", SAErrors.ERROR_CANNOT_WRITE_CONTROL_POINT);
                    }
                }
            });
            Log.d(this.Tag, "Cannot read channel Settings");
        }
        this.removeListener(command);
        return channelSettings;
    }
    
    public boolean writeChannelSettings(final ChannelSettings channelSettings) {
        Log.d(this.Tag, "writeChannelSettings");
        if (channelSettings.getSamplingPeriod() > 200 || channelSettings.getSamplingPeriod() < 8) {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                        SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "writeChannelSettings", SAErrors.ERROR_INVALID_SAMPLING_PERIOD_RANGE, "Sampling Period in milliseconds. Valid values: 8 \u2013 200");
                    }
                }
            });
            return false;
        }
        try {
            this.mwcQueue.put(channelSettings);
        }
        catch (InterruptedException e) {
            return false;
        }
        return true;
    }
    
    private void writeChannelSettingsEx(final ChannelSettings channelSettings) {
        Log.d(this.Tag, "SASensoriaControlPointService:writeChannelSettingsEx");
        final ByteBuffer payload = ByteBuffer.allocate(3).order(ByteOrder.BIG_ENDIAN);
        payload.put((byte)channelSettings.getSamplingPeriod());
        payload.put(1, channelSettings.getProtocol().getByteVal());
        payload.put(2, channelSettings.channelBooleans2Byte());
        Log.i(this.Tag, ToHexString(payload.array()));
        final SAWriteChannelSettings command = new SAWriteChannelSettings(channelSettings.getChannel(), payload.array());
        this.addListener(command);
        if (this.WriteControlPoint(command.getCommand())) {
            if (!command.writeData()) {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                            SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "writeChannelSettingsEx", command.getLastError());
                        }
                    }
                });
            }
            else {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                            ((SAServiceControlPointInterface)SASensoriaControlPointService.this.mIServiceDelegate).didServiceWriteChannelSettings(channelSettings.getChannel());
                        }
                    }
                });
            }
        }
        else {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                        SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "writeChannelSettingsEx", SAErrors.ERROR_CANNOT_WRITE_CONTROL_POINT);
                    }
                }
            });
        }
        this.removeListener(command);
    }
    
    public boolean restoreDefaultChannelSettings(final short channel) {
        Log.d(this.Tag, "restoreDefaultChannelSettings");
        boolean retCode = true;
        final SARestoreDefaultChannelSettings command = new SARestoreDefaultChannelSettings(channel);
        this.addListener(command);
        if (this.WriteControlPoint(command.getCommand())) {
            if (!command.upload()) {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                            SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "restoreDefaultChannelSettings", command.getLastError());
                        }
                    }
                });
                retCode = false;
            }
        }
        else {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                        SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "restoreDefaultChannelSettings", SAErrors.ERROR_CANNOT_WRITE_CONTROL_POINT);
                    }
                }
            });
            retCode = false;
        }
        this.removeListener(command);
        return retCode;
    }
    
    public boolean setSamplingPeriod(final short channel, final short period) {
        Log.d(this.Tag, "setSamplingPeriod");
        if (period > 200 || period < 8) {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                        SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "setSamplingPeriod", SAErrors.ERROR_INVALID_SAMPLING_PERIOD_RANGE, "Sampling Period in milliseconds. Valid values: 8 \u2013 200");
                    }
                }
            });
            return false;
        }
        final SamplingPeriod samplingPeriod = new SamplingPeriod(channel, period);
        try {
            this.mwcQueue.put(samplingPeriod);
        }
        catch (InterruptedException e) {
            return false;
        }
        return true;
    }
    
    private void setSamplingPeriodEx(final short channel, final Short period) {
        Log.d(this.Tag, "setSamplingPeriodEx");
        final ByteBuffer payload = ByteBuffer.allocate(1).order(ByteOrder.BIG_ENDIAN);
        payload.put(period.byteValue());
        Log.i(this.Tag, ToHexString(payload.array()));
        final SASetSamplingPeriod command = new SASetSamplingPeriod(channel, payload.array());
        this.addListener(command);
        if (this.WriteControlPoint(command.getCommand())) {
            if (!command.putPeriod()) {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                            SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "setSamplingPeriodEx", command.getLastError());
                        }
                    }
                });
            }
            else {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                            ((SAServiceControlPointInterface)SASensoriaControlPointService.this.mIServiceDelegate).didServiceSamplingPeriodSet(channel);
                        }
                    }
                });
            }
        }
        else {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                        SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "setSamplingPeriodEx", SAErrors.ERROR_CANNOT_WRITE_CONTROL_POINT);
                    }
                }
            });
        }
        this.removeListener(command);
    }
    
    public boolean setChannelProtocolType(final short channel, final ProtocolType protocolType) {
        Log.d(this.Tag, "setChannelProtocolType");
        final ChannelProtocolType channelProtocolType = new ChannelProtocolType(channel, protocolType);
        try {
            this.mwcQueue.put(channelProtocolType);
        }
        catch (InterruptedException e) {
            return false;
        }
        return true;
    }
    
    private void setChannelProtocolTypeEx(final short channel, final ProtocolType protocolType) {
        Log.d(this.Tag, "SASensoriaControlPointService:setSamplingPeriodEx");
        final ByteBuffer payload = ByteBuffer.allocate(1).order(ByteOrder.BIG_ENDIAN);
        payload.put(protocolType.getByteVal());
        Log.i(this.Tag, ToHexString(payload.array()));
        final SASetChannelProtocolType command = new SASetChannelProtocolType(channel, payload.array());
        this.addListener(command);
        if (this.WriteControlPoint(command.getCommand())) {
            if (!command.putProtocolType()) {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                            SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "setChannelProtocolTypeEx", command.getLastError());
                        }
                    }
                });
            }
            else {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                            ((SAServiceControlPointInterface)SASensoriaControlPointService.this.mIServiceDelegate).didServiceChannelProtocolTypeSet(channel);
                        }
                    }
                });
            }
        }
        else {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                        SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "setChannelProtocolTypeEx", SAErrors.ERROR_CANNOT_WRITE_CONTROL_POINT);
                    }
                }
            });
        }
        this.removeListener(command);
    }
    
    public boolean setEnableChannel(final short channel, final boolean value) {
        Log.d(this.Tag, "setEnableChannel");
        final ChannelBits channelBits = new ChannelBits(channel, ChannelBitsEnum.Enable, value);
        try {
            this.mwcQueue.put(channelBits);
        }
        catch (InterruptedException e) {
            return false;
        }
        return true;
    }
    
    public boolean setEnableTransmitViaBle(final short channel, final boolean value) {
        Log.d(this.Tag, "setEnableTransmitViaBle");
        final ChannelBits channelBits = new ChannelBits(channel, ChannelBitsEnum.Send, value);
        try {
            this.mwcQueue.put(channelBits);
        }
        catch (InterruptedException e) {
            return false;
        }
        return true;
    }
    
    public boolean setEnableLogToLocalMemory(final short channel, final boolean value) {
        Log.d(this.Tag, "setEnableLogToLocalMemory");
        final ChannelBits channelBits = new ChannelBits(channel, ChannelBitsEnum.Store, value);
        try {
            this.mwcQueue.put(channelBits);
        }
        catch (InterruptedException e) {
            return false;
        }
        return true;
    }
    
    private void setChannelBitsEx(final short channel, final ChannelBitsEnum channelBitsEnum, final boolean value) {
        Log.d(this.Tag, "setChannelBitsEx");
        final ByteBuffer payload = ByteBuffer.allocate(2).order(ByteOrder.BIG_ENDIAN);
        payload.put(channelBitsEnum.getByteVal());
        payload.put(1, (byte)(value ? 1 : 0));
        Log.i(this.Tag, ToHexString(payload.array()));
        final SASetChannelBits command = new SASetChannelBits(channel, payload.array());
        this.addListener(command);
        if (this.WriteControlPoint(command.getCommand())) {
            if (!command.putProtocolType()) {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                            SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "setChannelProtocolTypeEx", command.getLastError());
                        }
                    }
                });
            }
            else {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                            ((SAServiceControlPointInterface)SASensoriaControlPointService.this.mIServiceDelegate).didServiceChannelPropertySet(channel, channelBitsEnum);
                        }
                    }
                });
            }
        }
        else {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                        SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "setChannelProtocolTypeEx", SAErrors.ERROR_CANNOT_WRITE_CONTROL_POINT);
                    }
                }
            });
        }
        this.removeListener(command);
    }
    
    public boolean acquireEeprom() {
        Log.d(this.Tag, "acquireEeprom");
        boolean retCode = true;
        final SAAcquireEeprom command = new SAAcquireEeprom();
        this.addListener(command);
        if (this.WriteControlPoint(command.getCommand())) {
            if (!command.isAcquired()) {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                            SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "acquireEeprom", command.getLastError());
                        }
                    }
                });
                retCode = false;
            }
        }
        else {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                        SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "acquireEeprom", SAErrors.ERROR_CANNOT_WRITE_CONTROL_POINT);
                    }
                }
            });
            retCode = false;
        }
        this.removeListener(command);
        return retCode;
    }
    
    public boolean releaseEeprom() {
        Log.d(this.Tag, "releaseEeprom");
        boolean retCode = true;
        final SAReleaseEeprom command = new SAReleaseEeprom();
        this.addListener(command);
        if (this.WriteControlPoint(command.getCommand())) {
            if (!command.isReleased()) {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                            SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "releaseEeprom", command.getLastError());
                        }
                    }
                });
                retCode = false;
            }
        }
        else {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                        SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "releaseEeprom", SAErrors.ERROR_CANNOT_WRITE_CONTROL_POINT);
                    }
                }
            });
            retCode = false;
        }
        this.removeListener(command);
        return retCode;
    }
    
    public BaseConfigurationStorage readConfigurationStorage() {
        Log.d(this.Tag, "readConfigurationStorage");
        if (!this.acquireEeprom()) {
            return null;
        }
        final ByteBuffer payload = ByteBuffer.allocate(256);
        int chunkLength;
        for (int offset = 0; offset < payload.capacity(); offset += chunkLength) {
            byte[] response = null;
            final short basechunkLength = 16;
            chunkLength = ((offset + basechunkLength > payload.capacity()) ? (payload.capacity() - offset) : basechunkLength);
            final SAReadDataEeprom command = new SAReadDataEeprom((byte)offset, (byte)chunkLength);
            this.addListener(command);
            if (!this.WriteControlPoint(command.getCommand())) {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                            SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "readConfigurationStorage", SAErrors.ERROR_CANNOT_WRITE_CONTROL_POINT);
                        }
                    }
                });
                this.releaseEeprom();
                return null;
            }
            if ((response = command.readData()) == null) {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                            SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "readConfigurationStorage", command.getLastError());
                        }
                    }
                });
                this.releaseEeprom();
                return null;
            }
            payload.position(offset);
            payload.put(response, 0, chunkLength);
            Log.i(this.Tag, "Payload=" + ToHexString(payload.array()));
            this.removeListener(command);
        }
        if (!this.releaseEeprom()) {
            return null;
        }
        try {
            return ConfigurationStorageFactory.Instance.Create(payload.array());
        }
        catch (final SAException.InvalidStorageTypeException e) {
            Log.e(this.Tag, e.getMessage());
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                        SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "readConfigurationStorage", SAErrors.ERROR_INVALID_STORAGE_TYPE, e.getMessage());
                    }
                }
            });
        }
        catch (final SAException.InvalidPayloadException e2) {
            Log.e(this.Tag, e2.getMessage());
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                        SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "readConfigurationStorage", SAErrors.ERROR_INVALID_PAYLOAD, e2.getMessage());
                    }
                }
            });
        }
        catch (final SAException.UnformattedStorageException e3) {
            Log.e(this.Tag, e3.getMessage());
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                        SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "readConfigurationStorage", SAErrors.ERROR_UNFORMATTED_STORAGE, e3.getMessage());
                    }
                }
            });
        }
        catch (final SAException.CrcFailedException e4) {
            Log.e(this.Tag, e4.getMessage());
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                        SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "readConfigurationStorage", SAErrors.ERROR_CRC_FAILED, e4.getMessage());
                    }
                }
            });
        }
        catch (final InstantiationException e5) {
            Log.e(this.Tag, e5.getMessage());
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                        SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "readConfigurationStorage", SAErrors.ERROR_INSTANTIATION, e5.getMessage());
                    }
                }
            });
        }
        catch (final IllegalAccessException e6) {
            Log.e(this.Tag, e6.getMessage());
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                        SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "readConfigurationStorage", SAErrors.ERROR_ILLEGAL_ACCESS, e6.getMessage());
                    }
                }
            });
        }
        catch (final SAException.InvalidGarmentTypeException e7) {
            Log.e(this.Tag, e7.getMessage());
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                        SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "readConfigurationStorage", SAErrors.ERROR_INVALID_GARMENT_TYPE, e7.getMessage());
                    }
                }
            });
        }
        catch (final SAException.InvalidBodyLocationException e8) {
            Log.e(this.Tag, e8.getMessage());
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                        SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "readConfigurationStorage", SAErrors.ERROR_INVALID_BODY_LOCATION, e8.getMessage());
                    }
                }
            });
        }
        catch (final SAException.InvalidSensorTypeException e9) {
            Log.e(this.Tag, e9.getMessage());
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                        SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "readConfigurationStorage", SAErrors.ERROR_INVALID_SENSOR_TYPE, e9.getMessage());
                    }
                }
            });
        }
        catch (final Exception e10) {
            Log.e(this.Tag, e10.getMessage());
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                        SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "readConfigurationStorage", SAErrors.ERROR_FAILURE, e10.getMessage());
                    }
                }
            });
        }
        return null;
    }
    
    public boolean acquireHapticDriver() {
        Log.d(this.Tag, "acquireHapticDriver");
        boolean retCode = true;
        final SAAcquireHapticDriver command = new SAAcquireHapticDriver();
        this.addListener(command);
        if (this.WriteControlPoint(command.getCommand())) {
            if (!command.isAcquired()) {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                            SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "acquireHapticDriver", command.getLastError());
                        }
                    }
                });
                retCode = false;
            }
            else {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                            ((SAServiceControlPointInterface)SASensoriaControlPointService.this.mIServiceDelegate).didServiceHapticDriverMotorAcquired(true);
                        }
                    }
                });
            }
        }
        else {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                        SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "acquireHapticDriver", SAErrors.ERROR_CANNOT_WRITE_CONTROL_POINT);
                    }
                }
            });
            retCode = false;
        }
        this.removeListener(command);
        return retCode;
    }
    
    public boolean releaseHapticDriver() {
        Log.d(this.Tag, "releaseHapticDriver");
        boolean retCode = true;
        final SAReleaseHapticDriver command = new SAReleaseHapticDriver();
        this.addListener(command);
        if (this.WriteControlPoint(command.getCommand())) {
            if (!command.isReleased()) {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                            SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "releaseHapticDriver", command.getLastError());
                        }
                    }
                });
                retCode = false;
            }
            else {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                            ((SAServiceControlPointInterface)SASensoriaControlPointService.this.mIServiceDelegate).didServiceHapticDriverMotorReleased(true);
                        }
                    }
                });
            }
        }
        else {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                        SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "releaseHapticDriver", SAErrors.ERROR_CANNOT_WRITE_CONTROL_POINT);
                    }
                }
            });
            retCode = false;
        }
        this.removeListener(command);
        return retCode;
    }
    
    public boolean initializeHapticDriver(final byte motorID) {
        Log.d(this.Tag, "initializeHapticDriver");
        if (motorID > 2 || motorID < 1) {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                        SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "initializeHapticDriver", SAErrors.ERROR_INVALID_SAMPLING_RATE_VALUES, "Haptic Driver Motor ID options are 1 for ERM and 1 for LRA.");
                    }
                }
            });
            return false;
        }
        final HapticDriverMotorID hapticDriverMotorID = new HapticDriverMotorID(motorID);
        try {
            this.mwcQueue.put(hapticDriverMotorID);
        }
        catch (InterruptedException e) {
            return false;
        }
        return true;
    }
    
    private void initializeHapticDriverEx(final byte motorID) {
        Log.d(this.Tag, "initializeHapticDriverEx");
        final ByteBuffer payload = ByteBuffer.allocate(1).order(ByteOrder.BIG_ENDIAN);
        payload.put(0, motorID);
        Log.i(this.Tag, ToHexString(payload.array()));
        final SAInitializeHapticDriver command = new SAInitializeHapticDriver(payload.array());
        this.addListener(command);
        if (this.WriteControlPoint(command.getCommand())) {
            if (!command.initializeAutoCalibration()) {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                            SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "initializeHapticDriverEx", command.getLastError());
                        }
                    }
                });
            }
            else {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                            ((SAServiceControlPointInterface)SASensoriaControlPointService.this.mIServiceDelegate).didServiceHapticDriverMotorInitialized(motorID);
                        }
                    }
                });
            }
        }
        else {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                        SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "initializeHapticDriverEx", SAErrors.ERROR_CANNOT_WRITE_CONTROL_POINT);
                    }
                }
            });
        }
        this.removeListener(command);
    }
    
    public boolean turnOnHapticDriver(final byte strength, final byte onTime, final byte offTime, final byte repeatCount) {
        Log.d(this.Tag, "turnOnHapticDriver");
        final HapticDriverMotorPattern hapticDriverMotorPattern = new HapticDriverMotorPattern(strength, onTime, offTime, repeatCount);
        try {
            this.mwcQueue.put(hapticDriverMotorPattern);
        }
        catch (InterruptedException e) {
            return false;
        }
        return true;
    }
    
    private void turnOnHapticDriverEx(final byte strength, final byte onTime, final byte offTime, final byte repeatCount) {
        Log.d(this.Tag, "turnOnHapticDriverEx");
        if (strength < 0 || strength > 127) {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                        SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "turnOnHapticDriverEx", SAErrors.ERROR_OUT_OF_BOUNDS_STRENGTH);
                    }
                }
            });
        }
        else {
            final ByteBuffer payload = ByteBuffer.allocate(4).order(ByteOrder.BIG_ENDIAN);
            payload.put(0, strength);
            payload.put(1, onTime);
            payload.put(2, offTime);
            payload.put(3, repeatCount);
            Log.i(this.Tag, ToHexString(payload.array()));
            final SATurnOnHapticDriver command = new SATurnOnHapticDriver(payload.array());
            this.addListener(command);
            if (this.WriteControlPoint(command.getCommand())) {
                if (!command.turnMotorOn()) {
                    this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                                SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "turnOnHapticDriverEx", command.getLastError());
                            }
                        }
                    });
                }
                else {
                    this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                                ((SAServiceControlPointInterface)SASensoriaControlPointService.this.mIServiceDelegate).didServiceHapticDriverMotorOn(strength, onTime, offTime, repeatCount);
                            }
                        }
                    });
                }
            }
            else {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                            SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "turnOnHapticDriverEx", SAErrors.ERROR_CANNOT_WRITE_CONTROL_POINT);
                        }
                    }
                });
            }
            this.removeListener(command);
        }
    }
    
    public boolean turnOffHapticDriver() {
        Log.d(this.Tag, "turnOffHapticDriver");
        boolean retCode = true;
        final SATurnOffHapticDriver command = new SATurnOffHapticDriver();
        this.addListener(command);
        if (this.WriteControlPoint(command.getCommand())) {
            if (!command.turnMotorOff()) {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                            SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "turnOffHapticDriver", command.getLastError());
                        }
                    }
                });
                retCode = false;
            }
            else {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                            ((SAServiceControlPointInterface)SASensoriaControlPointService.this.mIServiceDelegate).didServiceHapticDriverMotorOff(true);
                        }
                    }
                });
            }
        }
        else {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                        SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "turnOffHapticDriver", SAErrors.ERROR_CANNOT_WRITE_CONTROL_POINT);
                    }
                }
            });
            retCode = false;
        }
        this.removeListener(command);
        return retCode;
    }
    
    private MyRunnable getTheWriter() {
        return new MyRunnable() {
            public void terminate() {
                final BaseConfigurationStorage stop = new BaseConfigurationStorage();
                stop.HeaderVersion = 0;
                try {
                    SASensoriaControlPointService.this.mwcQueue.put(stop);
                }
                catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void run() {
                SASensoriaControlPointService.this.isWriterStarted = true;
                try {
                    while (true) {
                        final Object obj = SASensoriaControlPointService.this.mwcQueue.take();
                        if (obj instanceof BaseConfigurationStorage) {
                            final BaseConfigurationStorage bc = (BaseConfigurationStorage)obj;
                            if (bc.HeaderVersion == 0) {
                                break;
                            }
                            SASensoriaControlPointService.this.writeConfigurationStorageEx(bc);
                        }
                        else if (obj instanceof AdvertisingTimeout) {
                            final AdvertisingTimeout at = (AdvertisingTimeout)obj;
                            SASensoriaControlPointService.this.writeAdvertisingTimeoutEx(at.fastTimeout, at.slowTimeout);
                        }
                        else if (obj instanceof WakeUpDrivingSources) {
                            final WakeUpDrivingSources wuds = (WakeUpDrivingSources)obj;
                            SASensoriaControlPointService.this.writeWakeUpDrivingSourcesEx(wuds.tap, wuds.shake, wuds.tilt);
                        }
                        else if (obj instanceof AccelerometerSamplingRate) {
                            final AccelerometerSamplingRate asr = (AccelerometerSamplingRate)obj;
                            SASensoriaControlPointService.this.writeAccelerometerSamplingRateEx(asr.samplingRate);
                        }
                        else if (obj instanceof AccelerometerSamplingRange) {
                            final AccelerometerSamplingRange asrg = (AccelerometerSamplingRange)obj;
                            SASensoriaControlPointService.this.writeAccelerometerSamplingRangeEx(asrg.samplingRange);
                        }
                        else if (obj instanceof GyroscopeSamplingRate) {
                            final GyroscopeSamplingRate gsr = (GyroscopeSamplingRate)obj;
                            SASensoriaControlPointService.this.writeGyroscopeSamplingRateEx(gsr.samplingRate);
                        }
                        else if (obj instanceof GyroscopeSamplingRange) {
                            final GyroscopeSamplingRange gsrg = (GyroscopeSamplingRange)obj;
                            SASensoriaControlPointService.this.writeGyroscopeSamplingRangeEx(gsrg.samplingRange);
                        }
                        else if (obj instanceof MagnetometerSamplingRate) {
                            final MagnetometerSamplingRate msr = (MagnetometerSamplingRate)obj;
                            SASensoriaControlPointService.this.writeMagnetometerSamplingRateEx(msr.samplingRate);
                        }
                        else if (obj instanceof MagnetometerSamplingRange) {
                            final MagnetometerSamplingRange msrg = (MagnetometerSamplingRange)obj;
                            SASensoriaControlPointService.this.writeMagnetometerSamplingRangeEx(msrg.samplingRange);
                        }
                        else if (obj instanceof MagnetometerCalibrationValues) {
                            final MagnetometerCalibrationValues mcv = (MagnetometerCalibrationValues)obj;
                            SASensoriaControlPointService.this.writeMagnetometerCalibrationValuesEx(mcv.calibrationValues);
                        }
                        else if (obj instanceof ChannelSettings) {
                            final ChannelSettings cs = (ChannelSettings)obj;
                            SASensoriaControlPointService.this.writeChannelSettingsEx(cs);
                        }
                        else if (obj instanceof SamplingPeriod) {
                            final SamplingPeriod sp = (SamplingPeriod)obj;
                            SASensoriaControlPointService.this.setSamplingPeriodEx(sp.channel, sp.period);
                        }
                        else if (obj instanceof ChannelProtocolType) {
                            final ChannelProtocolType cpt = (ChannelProtocolType)obj;
                            SASensoriaControlPointService.this.setChannelProtocolTypeEx(cpt.channel, cpt.protocolType);
                        }
                        else if (obj instanceof ChannelBits) {
                            final ChannelBits cb = (ChannelBits)obj;
                            SASensoriaControlPointService.this.setChannelBitsEx(cb.channel, cb.channelBitsEnum, cb.value);
                        }
                        else if (obj instanceof HapticDriverMotorID) {
                            final HapticDriverMotorID mid = (HapticDriverMotorID)obj;
                            SASensoriaControlPointService.this.initializeHapticDriverEx(mid.motorID);
                        }
                        else {
                            if (!(obj instanceof HapticDriverMotorPattern)) {
                                continue;
                            }
                            final HapticDriverMotorPattern mp = (HapticDriverMotorPattern)obj;
                            SASensoriaControlPointService.this.turnOnHapticDriverEx(mp.strength, mp.onTime, mp.offTime, mp.repeatCount);
                        }
                    }
                    SASensoriaControlPointService.this.isWriterStarted = false;
                }
                catch (InterruptedException e) {}
            }
        };
    }
    
    public boolean writeConfigurationStorage(final BaseConfigurationStorage configuration) {
        Log.d(this.Tag, "writeConfigurationStorage");
        try {
            this.mwcQueue.put(configuration);
        }
        catch (InterruptedException e) {
            return false;
        }
        return true;
    }
    
    private void writeConfigurationStorageEx(final BaseConfigurationStorage configuration) {
        Log.d(this.Tag, "SASensoriaControlPointService:writeConfigurationStorageEx");
        final byte[] payload = configuration.Serialize();
        if (payload != null) {
            if (!this.acquireEeprom()) {
                this.releaseEeprom();
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                            SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "writeConfigurationStorageEx", SAErrors.ERROR_NO_EEPROM);
                        }
                    }
                });
                return;
            }
            Log.i(this.Tag, ToHexString(payload));
            int chunkLength;
            for (int offset = 0; offset < payload.length; offset += chunkLength) {
                final int basechunkLength = 16;
                chunkLength = ((offset + basechunkLength > payload.length) ? (payload.length - offset) : basechunkLength);
                final byte[] chunk = new byte[chunkLength];
                System.arraycopy(payload, offset, chunk, 0, chunkLength);
                Log.i(this.Tag, ToHexString(chunk));
                final SAWriteDataEeprom command = new SAWriteDataEeprom((byte)offset, (byte)chunkLength, chunk);
                this.addListener(command);
                if (!this.WriteControlPoint(command.getCommand())) {
                    this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                                SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "writeConfigurationStorageEx", SAErrors.ERROR_CANNOT_WRITE_CONTROL_POINT);
                            }
                        }
                    });
                    this.releaseEeprom();
                    return;
                }
                if (!command.isSuccess()) {
                    this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                                SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "writeConfigurationStorageEx", command.getLastError());
                            }
                        }
                    });
                    this.releaseEeprom();
                    return;
                }
            }
            this.releaseEeprom();
        }
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                    ((SAServiceControlPointInterface)SASensoriaControlPointService.this.mIServiceDelegate).didServiceConfigurationStorageSet(configuration);
                }
            }
        });
    }
    
    private Boolean WriteControlPoint(final byte[] data) {
        Log.d(this.Tag, "SASensoriaControlPointService:WriteControlPoint");
        Log.i(this.Tag, "Control Point WRITE: " + ToHexString(data));
        this.mCharacteristicTX.setValue(data);
        if (!this.mConnectedGatt.writeCharacteristic(this.mCharacteristicTX)) {
            return false;
        }
        return true;
    }
    
    public int getSDVersion() {
        return this.mSDVersion;
    }
    
    @Override
    BluetoothGattCallback getGattCallBack() {
        return new BluetoothGattCallback() {
            private String connectionState(final int status) {
                switch (status) {
                    case 2: {
                        return "Connected";
                    }
                    case 0: {
                        return "Disconnected";
                    }
                    case 1: {
                        return "Connecting";
                    }
                    case 3: {
                        return "Disconnecting";
                    }
                    default: {
                        return String.valueOf(status);
                    }
                }
            }
            
            private boolean setCharacteristicNotification(final BluetoothGattCharacteristic characteristic, final boolean enabled) throws InterruptedException {
                Log.d(SASensoriaControlPointService.this.Tag, "SASensoriaControlPointService:setCharacteristicNotification");
                if (SASensoriaControlPointService.this.mConnectedGatt == null) {
                    if (SASensoriaControlPointService.this.mIBundle != null) {
                        SASensoriaControlPointService.this.mIBundle.onServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SAErrors.ERROR_GATT_NOT_INITIALIZED);
                    }
                    SASensoriaControlPointService.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                                SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "setCharacteristicNotification", SAErrors.ERROR_GATT_NOT_INITIALIZED);
                            }
                        }
                    });
                    return false;
                }
                final int charaProp = characteristic.getProperties();
                if ((charaProp | 0x10) <= 0) {
                    return false;
                }
                SASensoriaControlPointService.this.mConnectedGatt.setCharacteristicNotification(characteristic, enabled);
                final byte[] enableNotification = enabled ? BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE : BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE;
                final UUID uuidCharacteristic = characteristic.getUuid();
                Log.d(SASensoriaControlPointService.this.Tag, "setCharacteristicNotification: UUID: " + uuidCharacteristic.toString());
                final List<BluetoothGattDescriptor> bluetoothGattDescriptors = (List<BluetoothGattDescriptor>)characteristic.getDescriptors();
                if (bluetoothGattDescriptors == null || bluetoothGattDescriptors.size() == 0) {
                    return false;
                }
                final BluetoothGattDescriptor descriptor = bluetoothGattDescriptors.get(0);
                descriptor.setValue(enableNotification);
                SASensoriaControlPointService.this.mConnectedGatt.writeDescriptor(descriptor);
                return true;
            }
            
            public void onConnectionStateChange(final BluetoothGatt gatt, final int status, final int newState) {
                Log.d(SASensoriaControlPointService.this.Tag, "SASensoriaControlPointService:onConnectionStateChange: " + status + " -> " + this.connectionState(newState) + " API = " + Build.VERSION.SDK_INT);
                if (status == 0 && newState == 2) {
                    if (SASensoriaControlPointService.this.mServiceStatus.compare(1) || SASensoriaControlPointService.this.mServiceStatus.compare(3)) {
                        Log.d(SASensoriaControlPointService.this.Tag, "Before Gatt discovery services");
                        gatt.discoverServices();
                    }
                }
                else if (status == 0 && newState == 0) {
                    if (Build.VERSION.SDK_INT >= 21 || SASensoriaControlPointService.this.mServiceStatus.compare(3)) {
                        if (SASensoriaControlPointService.this.mIBundle != null) {
                            try {
                                SASensoriaControlPointService.this.mIBundle.onServiceStopped(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mHandler);
                            }
                            catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                        if (SASensoriaControlPointService.this.mServiceStatus.compare(3)) {
                            SASensoriaControlPointService.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                                        SASensoriaControlPointService.this.mIServiceDelegate.didServicePause(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType());
                                    }
                                }
                            });
                        }
                    }
                    else {
                        Log.d(SASensoriaControlPointService.this.Tag, "SASensoriaControlPointService:onConnectionStateChange: " + status + " -> new state =" + this.connectionState(newState) + " API = " + Build.VERSION.SDK_INT);
                        SASensoriaControlPointService.this.mServiceStatus.set(1);
                        Log.d(SASensoriaControlPointService.this.Tag, "Try to reconnect from error:" + status);
                        SASensoriaControlPointService.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (SASensoriaControlPointService.this.isWriterStarted) {
                                    SASensoriaControlPointService.this.mWriter.terminate();
                                    try {
                                        SASensoriaControlPointService.this.mThreadWriter.join();
                                        Log.d(SASensoriaControlPointService.this.Tag, "Thread terminated");
                                        SASensoriaControlPointService.this.mWriter = SASensoriaControlPointService.this.getTheWriter();
                                        SASensoriaControlPointService.this.mThreadWriter = new Thread(SASensoriaControlPointService.this.mWriter);
                                    }
                                    catch (InterruptedException ex) {}
                                }
                                SASensoriaControlPointService.this.pause();
                                SASensoriaControlPointService.this.stop();
                                SASensoriaControlPointService.this.mServiceStatus.set(0);
                                try {
                                    SASensoriaControlPointService.this.start(SASensoriaControlPointService.this.mIServiceDelegate);
                                }
                                catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    SASensoriaControlPointService.this.resume();
                                }
                                catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                    if (SASensoriaControlPointService.this.mCountDownLatch != null) {
                        SASensoriaControlPointService.this.mCountDownLatch.countDown();
                    }
                }
                else if ((status == 8 || status == 133 || status == 34 || status == 59) && newState == 0) {
                    SASensoriaControlPointService.this.mServiceStatus.set(1);
                    Log.d(SASensoriaControlPointService.this.Tag, "Try to reconnect from error:" + status);
                    SASensoriaControlPointService.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (SASensoriaControlPointService.this.isWriterStarted) {
                                SASensoriaControlPointService.this.mWriter.terminate();
                                try {
                                    SASensoriaControlPointService.this.mThreadWriter.join();
                                    Log.d(SASensoriaControlPointService.this.Tag, "Thread terminated");
                                    SASensoriaControlPointService.this.mWriter = SASensoriaControlPointService.this.getTheWriter();
                                    SASensoriaControlPointService.this.mThreadWriter = new Thread(SASensoriaControlPointService.this.mWriter);
                                }
                                catch (InterruptedException ex) {}
                            }
                            SASensoriaControlPointService.this.mConnectedGatt.close();
                            try {
                                SASensoriaControlPointService.this.resume();
                            }
                            catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
                else if (status == 133 && newState == 2 && Build.VERSION.SDK_INT < 21) {
                    SASensoriaControlPointService.this.mServiceStatus.set(1);
                    Log.d(SASensoriaControlPointService.this.Tag, "Try to reconnect from error =" + status + " new status = " + newState + " API = " + Build.VERSION.SDK_INT);
                    SASensoriaControlPointService.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (SASensoriaControlPointService.this.isWriterStarted) {
                                SASensoriaControlPointService.this.mWriter.terminate();
                                try {
                                    SASensoriaControlPointService.this.mThreadWriter.join();
                                    Log.d(SASensoriaControlPointService.this.Tag, "Thread terminated");
                                    SASensoriaControlPointService.this.mWriter = SASensoriaControlPointService.this.getTheWriter();
                                    SASensoriaControlPointService.this.mThreadWriter = new Thread(SASensoriaControlPointService.this.mWriter);
                                }
                                catch (InterruptedException ex) {}
                            }
                            SASensoriaControlPointService.this.mConnectedGatt.close();
                            try {
                                SASensoriaControlPointService.this.resume();
                            }
                            catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
                else if (status != 0) {
                    try {
                        throw new SAException.UnmanagedConnectionException(status, newState);
                    }
                    catch (SAException.UnmanagedConnectionException e2) {
                        e2.printStackTrace();
                    }
                }
            }
            
            public void onServicesDiscovered(final BluetoothGatt gatt, final int status) {
                Log.d(SASensoriaControlPointService.this.Tag, "SASensoriaControlPointService:onServicesDiscovered");
                if (status == 0) {
                    SASensoriaControlPointService.this.mSDVersion = ((SASensoriaControlPointService.this.mConnectedGatt.getService(SASensoriaControlPointService.this.UUID_NORDIC_DFU_SERVICE) != null) ? 5 : 3);
                    final BluetoothGattService gattService = SASensoriaControlPointService.this.mConnectedGatt.getService(SASensoriaControlPointService.this.UUID_SENSORIA_CORE_CONTROL_POINT_SERVICE);
                    if (gattService != null) {
                        Log.i(SASensoriaControlPointService.this.Tag, "ServiceTypes characteristic UUID found: " + gattService.getUuid().toString());
                        final List<BluetoothGattCharacteristic> gattCharacteristics = (List<BluetoothGattCharacteristic>)gattService.getCharacteristics();
                        for (final BluetoothGattCharacteristic gattCharacteristic : gattCharacteristics) {
                            if (gattCharacteristic.getUuid().compareTo(SASensoriaControlPointService.this.UUID_SENSORIA_CORE_CONTROL_POINT_RX_CHARACTERISTIC) == 0) {
                                SASensoriaControlPointService.this.mCharacteristicRX = gattCharacteristic;
                                if ((SASensoriaControlPointService.this.mCharacteristicRX.getProperties() & 0x10) <= 0) {
                                    continue;
                                }
                                try {
                                    this.setCharacteristicNotification(SASensoriaControlPointService.this.mCharacteristicRX, true);
                                }
                                catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                            else {
                                if (gattCharacteristic.getUuid().compareTo(SASensoriaControlPointService.this.UUID_SENSORIA_CORE_CONTROL_POINT_TX_CHARACTERISTIC) != 0) {
                                    continue;
                                }
                                SASensoriaControlPointService.this.mCharacteristicTX = gattCharacteristic;
                            }
                        }
                        if (SASensoriaControlPointService.this.mCharacteristicRX == null || SASensoriaControlPointService.this.mCharacteristicTX == null) {
                            if (SASensoriaControlPointService.this.mIBundle != null) {
                                try {
                                    SASensoriaControlPointService.this.mIBundle.onServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SAErrors.ERROR_CHARACTERISTIC_NOT_FOUND);
                                }
                                catch (InterruptedException e2) {
                                    e2.printStackTrace();
                                }
                            }
                            SASensoriaControlPointService.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                                        SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "onServicesDiscovered", SAErrors.ERROR_CHARACTERISTIC_NOT_FOUND);
                                    }
                                }
                            });
                        }
                        else if (SASensoriaControlPointService.this.mServiceStatus.compare(1) || SASensoriaControlPointService.this.mServiceStatus.compare(3)) {
                            if (SASensoriaControlPointService.this.mIBundle != null) {
                                try {
                                    SASensoriaControlPointService.this.mIBundle.onServiceStarted(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mHandler);
                                }
                                catch (InterruptedException e2) {
                                    e2.printStackTrace();
                                }
                            }
                            SASensoriaControlPointService.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (SASensoriaControlPointService.this.mServiceStatus.compare(1)) {
                                        Log.d(SASensoriaControlPointService.this.Tag, "ServiceTypes discovered and connected");
                                        if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                                            SASensoriaControlPointService.this.mIServiceDelegate.didServiceConnect(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType());
                                        }
                                    }
                                    else {
                                        if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                                            SASensoriaControlPointService.this.mIServiceDelegate.didServiceResume(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType());
                                        }
                                        Log.d(SASensoriaControlPointService.this.Tag, "ServiceTypes resumed");
                                    }
                                }
                            });
                            SASensoriaControlPointService.this.mThreadWriter.start();
                            Log.d(SASensoriaControlPointService.this.Tag, "THREAD is started");
                            SASensoriaControlPointService.this.mServiceStatus.set(2);
                        }
                    }
                    else {
                        Log.i(SASensoriaControlPointService.this.Tag, "ServiceTypes not found for UUID: " + SASensoriaControlPointService.this.UUID_SENSORIA_CORE_CONTROL_POINT_SERVICE.toString());
                        if (SASensoriaControlPointService.this.mIBundle != null) {
                            try {
                                SASensoriaControlPointService.this.mIBundle.onServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SAErrors.ERROR_SERVICE_NOT_FOUND);
                            }
                            catch (InterruptedException e3) {
                                e3.printStackTrace();
                            }
                        }
                        SASensoriaControlPointService.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (SASensoriaControlPointService.this.mIServiceDelegate != null) {
                                    SASensoriaControlPointService.this.mIServiceDelegate.didServiceError(SASensoriaControlPointService.this.mDevice, SASensoriaControlPointService.this.getType(), SASensoriaControlPointService.this.mServiceName, "onServicesDiscovered", SAErrors.ERROR_SERVICE_NOT_FOUND);
                                }
                            }
                        });
                    }
                    if (SASensoriaControlPointService.this.firstInitialization && SASensoriaControlPointService.this.mServiceStatus.compare(2)) {
                        SASensoriaControlPointService.this.mThreadStart.start();
                    }
                }
            }
            
            public void onCharacteristicWrite(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic, final int status) {
                Log.i(SASensoriaControlPointService.this.Tag, "onCharacteristicWrite: " + ToHexString(characteristic.getValue()));
                synchronized (SASensoriaControlPointService.this.subscribers) {
                    for (final SACommand command : SASensoriaControlPointService.this.subscribers) {
                        command.setCharacteristic(new SensoriaGattCharacteristic(characteristic, status));
                    }
                }
            }
            
            public void onCharacteristicChanged(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {
                Log.i(SASensoriaControlPointService.this.Tag, "onCharacteristicChanged: " + ToHexString(characteristic.getValue()));
                synchronized (SASensoriaControlPointService.this.subscribers) {
                    for (final SACommand command : SASensoriaControlPointService.this.subscribers) {
                        command.setCharacteristic(new SensoriaGattCharacteristic(characteristic));
                    }
                }
            }
            
            public void onDescriptorWrite(final BluetoothGatt gatt, final BluetoothGattDescriptor descriptor, final int status) {
            }
        };
    }
    
    private class AdvertisingTimeout
    {
        short fastTimeout;
        short slowTimeout;
        
        AdvertisingTimeout(final short fastTimeout, final short slowTimeout) {
            this.fastTimeout = fastTimeout;
            this.slowTimeout = slowTimeout;
        }
    }
    
    private class WakeUpDrivingSources
    {
        byte tap;
        byte shake;
        byte tilt;
        
        WakeUpDrivingSources(final byte tap, final byte shake, final byte tilt) {
            this.tap = tap;
            this.shake = shake;
            this.tilt = tilt;
        }
    }
    
    private class AccelerometerSamplingRate
    {
        short samplingRate;
        
        AccelerometerSamplingRate(final short samplingRate) {
            this.samplingRate = samplingRate;
        }
    }
    
    private class AccelerometerSamplingRange
    {
        short samplingRange;
        
        AccelerometerSamplingRange(final short samplingRange) {
            this.samplingRange = samplingRange;
        }
    }
    
    private class GyroscopeSamplingRate
    {
        short samplingRate;
        
        GyroscopeSamplingRate(final short samplingRate) {
            this.samplingRate = samplingRate;
        }
    }
    
    private class GyroscopeSamplingRange
    {
        short samplingRange;
        
        GyroscopeSamplingRange(final short samplingRange) {
            this.samplingRange = samplingRange;
        }
    }
    
    private class MagnetometerSamplingRate
    {
        short samplingRate;
        
        MagnetometerSamplingRate(final short samplingRate) {
            this.samplingRate = samplingRate;
        }
    }
    
    private class MagnetometerSamplingRange
    {
        short samplingRange;
        
        MagnetometerSamplingRange(final short samplingRange) {
            this.samplingRange = samplingRange;
        }
    }
    
    private class MagnetometerCalibrationValues
    {
        short[] calibrationValues;
        
        MagnetometerCalibrationValues(final short[] calibrationValues) {
            this.calibrationValues = calibrationValues;
        }
    }
    
    private class SamplingPeriod
    {
        short period;
        short channel;
        
        SamplingPeriod(final short channel, final short period) {
            this.period = period;
            this.channel = channel;
        }
    }
    
    private class ChannelProtocolType
    {
        ProtocolType protocolType;
        short channel;
        
        ChannelProtocolType(final short channel, final ProtocolType protocolType) {
            this.protocolType = protocolType;
            this.channel = channel;
        }
    }
    
    private class ChannelBits
    {
        ChannelBitsEnum channelBitsEnum;
        short channel;
        boolean value;
        
        ChannelBits(final short channel, final ChannelBitsEnum channelBitsEnum, final boolean value) {
            this.channelBitsEnum = channelBitsEnum;
            this.channel = channel;
            this.value = value;
        }
    }
    
    private class HapticDriverMotorID
    {
        byte motorID;
        
        HapticDriverMotorID(final byte motorID) {
            this.motorID = motorID;
        }
    }
    
    private class HapticDriverMotorPattern
    {
        byte strength;
        byte onTime;
        byte offTime;
        byte repeatCount;
        
        HapticDriverMotorPattern(final byte strength, final byte onTime, final byte offTime, final byte repeatCount) {
            this.strength = strength;
            this.onTime = onTime;
            this.offTime = offTime;
            this.repeatCount = repeatCount;
        }
    }
}
