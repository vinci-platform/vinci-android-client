/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: SASensoriaStreamingProtocols.java
 * Author: sdelic
 *
 * History:  21.2.22. 10:43 created
 */

//
// Decompiled by Procyon v0.5.36
// 

package com.comtrade.feature_sensoria.sensorialibrary;

import android.bluetooth.BluetoothGattCharacteristic;

import java.util.Locale;

abstract class SASensoriaStreamingProtocols extends SACommand
{
    protected SADataPoint dataPoint;
    protected int gRangeIndex;
    protected int dpsRangeIndex;
    protected int gaussRangeIndex;
    protected String TAG;
    private int actualSamplingRate;
    private int oneSecondPacketCount;
    private long oneSecondStartMillis;
    private int nominalSamplingRate;
    private int lastTick;
    protected final short myCharacteristic = 3;
    
    abstract void analyze(final byte[] p0);
    
    abstract void setCommandLog(final String p0);
    
    SASensoriaStreamingProtocols(final DataPointEvent event, final short samplingPeriod) {
        super(Command.GeneralStreamingProtocol.getNumVal(), event);
        this.dataPoint = null;
        this.gRangeIndex = 0;
        this.dpsRangeIndex = 0;
        this.gaussRangeIndex = 0;
        this.actualSamplingRate = 0;
        this.oneSecondPacketCount = 0;
        this.oneSecondStartMillis = 0L;
        this.nominalSamplingRate = 0;
        this.lastTick = 0;
        this.nominalSamplingRate = 1000 / samplingPeriod;
    }
    
    void readDataPoint() {
        BluetoothGattCharacteristic characteristic = null;
        this.terminated = false;
        while (true) {
            characteristic = this.waitForCharacteristic(false);
            if (characteristic != null) {
                if (!this.isInternalError()) {
                    final byte[] data = characteristic.getValue();
                    if (data.length != 20) {
                        ((DataPointEvent)this.value).onReceiveDataPoint(null, SAErrors.ERROR_UNEXPECTED_PROTOCOL_LENGTH, data.length);
                    }
                    else {
                        this.analyze(data);
                    }
                }
                else {
                    if (this.getLastError() == SAErrors.ERROR_TIMEOUT) {
                        continue;
                    }
                    ((DataPointEvent)this.value).onReceiveDataPoint(null, this.getLastError());
                }
            }
            else {
                if (this.getLastError() == SAErrors.ERROR_STOP) {
                    break;
                }
                continue;
            }
        }
        this.stopped();
    }
    
    protected void setSamplingFrequency(final byte[] data) {
        this.dataPoint.samplingFrequency = this.nominalSamplingRate;
    }
    
    protected int getAccelerometerRangeIndex(final byte[] data) {
        return data[1] >>> 2 & 0x3;
    }
    
    protected int getGyroscopeRangeIndex(final byte[] data) {
        return data[1] >>> 4 & 0x3;
    }
    
    protected int getMagnetometerRangeIndex(final byte[] data) {
        return data[1] >>> 6 & 0x3;
    }
    
    protected void setTicketLost() {
        if (this.lastTick != 0) {
            final int deltaTick = this.dataPoint.tick - this.lastTick - 1;
            if (deltaTick > 0) {
                this.setCommandLog(String.format(Locale.US, "Packets lost: %d", deltaTick));
            }
            final SADataPoint dataPoint = this.dataPoint;
            dataPoint.packetsLost += deltaTick;
        }
        this.lastTick = this.dataPoint.tick;
        final long timestamp = System.currentTimeMillis();
        if (timestamp - this.oneSecondStartMillis > 1000L) {
            this.oneSecondStartMillis = timestamp;
            this.actualSamplingRate = this.oneSecondPacketCount;
            this.oneSecondPacketCount = 0;
        }
        this.dataPoint.actualSamplingFrequency = this.actualSamplingRate;
        ++this.oneSecondPacketCount;
    }
    
    protected float accelToFloat(final int rangeIndex, final short rawValue) {
        final float[] lookup = { 0.003906253f, 0.00781248f, 0.015625024f, 0.031249983f };
        final float conversion = lookup[rangeIndex];
        return conversion * ((rawValue < 512) ? rawValue : (-(1024 - rawValue)));
    }
    
    protected float gyroToFloat(final int rangeIndex, final short rawValue) {
        final float[] lookup = { 0.56f, 1.12f, 2.24f, 4.48f };
        final float conversion = lookup[rangeIndex];
        return conversion * ((rawValue < 512) ? rawValue : (-(1024 - rawValue)));
    }
    
    protected float gyroToFloat2(final int rangeIndex, final short rawValue) {
        final float[] lookup = { 0.48828125f, 0.9765625f, 1.953125f, 3.90625f };
        final float conversion = lookup[rangeIndex];
        return conversion * ((rawValue < 512) ? rawValue : (-(1024 - rawValue)));
    }
    
    protected float magnetToFloat(final int rangeIndex, final short rawValue) {
        final float[] lookup = { 0.009344f, 0.018688f, 0.028032f, 0.037376f };
        final float conversion = lookup[rangeIndex];
        return conversion * ((rawValue < 512) ? rawValue : (-(1024 - rawValue)));
    }
    
    protected float magnetToFloat2(final short rawValue) {
        final float conversion = 38.4f;
        return conversion * ((rawValue < 512) ? rawValue : (-(1024 - rawValue))) * 0.01f;
    }
    
    protected void didUpdateData() {
        ((DataPointEvent)this.value).onReceiveDataPoint(this.dataPoint);
    }
    
    @Override
    protected boolean isMyCharacteristic(final SensoriaGattCharacteristic characteristic) {
        final Long uuid16 = BluetoothUtils.get16BitBluetoothUUID(characteristic.getUuid());
        return 3 == uuid16.shortValue();
    }
    
    interface DataPointEvent
    {
        void onReceiveDataPoint(final SADataPoint p0);
        
        void onReceiveDataPoint(final SADataPoint p0, final SAErrors p1);
        
        void onReceiveDataPoint(final SADataPoint p0, final SAErrors p1, final Object p2);
    }
}
