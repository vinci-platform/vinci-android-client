/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: UInt16.java
 * Author: sdelic
 *
 * History:  15.3.22. 10:01 created
 */

//
// Decompiled by Procyon v0.5.36
// 

package com.comtrade.feature_sensoria.sensorialibrary;

import android.util.Log;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

class UInt16 extends Number
{
    private static final String TAG;
    static final int MAX_VALUE = 65535;
    static final int MIN_VALUE = 0;
    private int value;
    
    UInt16(final int value) {
        if (value < 0) {
            this.value = 32767 + Math.abs(value);
        }
        else {
            this.value = value;
        }
        if (this.value < 0 || this.value > 65535) {
            throw new NumberFormatException("Invalid UInt16 value");
        }
    }
    
    UInt16(final String value) {
        this(Integer.parseInt(value));
    }
    
    @Override
    public byte byteValue() {
        return (byte)this.value;
    }
    
    int compareTo(final UInt16 other) {
        return this.value - other.value;
    }
    
    @Override
    public double doubleValue() {
        return this.value;
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof UInt16 && ((UInt16)o).value == this.value;
    }
    
    @Override
    public float floatValue() {
        return (float)this.value;
    }
    
    @Override
    public int hashCode() {
        return this.value;
    }
    
    @Override
    public int intValue() {
        return this.value;
    }
    
    @Override
    public long longValue() {
        return this.value;
    }
    
    @Override
    public short shortValue() {
        return (short)this.value;
    }
    
    @Override
    public String toString() {
        return "" + this.value;
    }
    
    public byte[] GetCommand() {
        final ByteBuffer returnByte = ByteBuffer.allocate(2);
        final ByteBuffer buffer = ByteBuffer.allocate(4).order(ByteOrder.BIG_ENDIAN);
        buffer.putInt(this.value);
        returnByte.put(buffer.array(), 2, 2);
        Log.i(UInt16.TAG, "Bytes value is:" + this.ToHexString(returnByte.array()));
        return returnByte.array();
    }
    
    private String ToHexString(final byte[] in) {
        final StringBuilder builder = new StringBuilder();
        for (final byte b : in) {
            builder.append(String.format("%02x", b));
        }
        return builder.toString();
    }
    
    static {
        TAG = UInt16.class.getSimpleName();
    }
}
