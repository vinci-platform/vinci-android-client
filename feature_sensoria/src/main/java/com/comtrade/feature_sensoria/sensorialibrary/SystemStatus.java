/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: SystemStatus.java
 * Author: sdelic
 *
 * History:  21.2.22. 10:43 created
 */

//
// Decompiled by Procyon v0.5.36
// 

package com.comtrade.feature_sensoria.sensorialibrary;

import android.os.Parcel;
import android.os.Parcelable;

public class SystemStatus implements Parcelable
{
    public boolean batteryReadingActive;
    public boolean hasNandMemoryModule;
    public boolean SPIM2Comfigured;
    public boolean SPIM1Configured;
    public boolean SPIS0Configured;
    public boolean dataLoggerActive;
    public boolean batteryLevelUnderThreshold;
    public boolean coreInChangingMode;
    public boolean samplingReady;
    public boolean fileTableBuilt;
    public boolean listeningToEventChannel;
    public boolean listeningToChannel2;
    public boolean listeningToChannel1;
    public boolean listeningToChannel0;
    public boolean coreConnectedViaBle;
    public boolean AD7Enabled;
    public boolean AD6Enabled;
    public boolean AD5Enabled;
    public boolean AD4Enabled;
    public boolean AD3Enabled;
    public boolean AD2Enabled;
    public boolean AD1Enabled;
    public boolean AD0Enabled;
    public static final Parcelable.Creator<SystemStatus> CREATOR;

    public SystemStatus() {
        this.batteryReadingActive = false;
        this.hasNandMemoryModule = false;
        this.SPIM2Comfigured = false;
        this.SPIM1Configured = false;
        this.SPIS0Configured = false;
        this.dataLoggerActive = false;
        this.batteryLevelUnderThreshold = false;
        this.coreInChangingMode = false;
        this.samplingReady = false;
        this.fileTableBuilt = false;
        this.listeningToEventChannel = false;
        this.listeningToChannel2 = false;
        this.listeningToChannel1 = false;
        this.listeningToChannel0 = false;
        this.coreConnectedViaBle = false;
        this.AD7Enabled = false;
        this.AD6Enabled = false;
        this.AD5Enabled = false;
        this.AD4Enabled = false;
        this.AD3Enabled = false;
        this.AD2Enabled = false;
        this.AD1Enabled = false;
        this.AD0Enabled = false;
    }

    public SystemStatus(final byte[] array) {
        this.batteryReadingActive = false;
        this.hasNandMemoryModule = false;
        this.SPIM2Comfigured = false;
        this.SPIM1Configured = false;
        this.SPIS0Configured = false;
        this.dataLoggerActive = false;
        this.batteryLevelUnderThreshold = false;
        this.coreInChangingMode = false;
        this.samplingReady = false;
        this.fileTableBuilt = false;
        this.listeningToEventChannel = false;
        this.listeningToChannel2 = false;
        this.listeningToChannel1 = false;
        this.listeningToChannel0 = false;
        this.coreConnectedViaBle = false;
        this.AD7Enabled = false;
        this.AD6Enabled = false;
        this.AD5Enabled = false;
        this.AD4Enabled = false;
        this.AD3Enabled = false;
        this.AD2Enabled = false;
        this.AD1Enabled = false;
        this.AD0Enabled = false;
        this.coreInChangingMode = ((array[0] & 0x1) == 0x1);
        this.batteryLevelUnderThreshold = ((array[0] >> 1 & 0x1) == 0x1);
        this.dataLoggerActive = ((array[0] >> 2 & 0x1) == 0x1);
        this.SPIS0Configured = ((array[0] >> 3 & 0x1) == 0x1);
        this.SPIM1Configured = ((array[0] >> 4 & 0x1) == 0x1);
        this.SPIM2Comfigured = ((array[0] >> 5 & 0x1) == 0x1);
        this.hasNandMemoryModule = ((array[0] >> 6 & 0x1) == 0x1);
        this.batteryReadingActive = ((array[0] >> 7 & 0x1) == 0x1);
        this.samplingReady = ((array[1] & 0x1) == 0x1);
        this.fileTableBuilt = ((array[1] >> 1 & 0x1) == 0x1);
        this.coreConnectedViaBle = ((array[2] & 0x1) == 0x1);
        this.listeningToChannel0 = ((array[2] >> 1 & 0x1) == 0x1);
        this.listeningToChannel1 = ((array[2] >> 2 & 0x1) == 0x1);
        this.listeningToChannel2 = ((array[2] >> 3 & 0x1) == 0x1);
        this.listeningToEventChannel = ((array[2] >> 4 & 0x1) == 0x1);
        this.AD0Enabled = ((array[3] & 0x1) == 0x1);
        this.AD1Enabled = ((array[3] >> 1 & 0x1) == 0x1);
        this.AD2Enabled = ((array[3] >> 2 & 0x1) == 0x1);
        this.AD3Enabled = ((array[3] >> 3 & 0x1) == 0x1);
        this.AD4Enabled = ((array[3] >> 4 & 0x1) == 0x1);
        this.AD5Enabled = ((array[3] >> 5 & 0x1) == 0x1);
        this.AD6Enabled = ((array[3] >> 6 & 0x1) == 0x1);
        this.AD7Enabled = ((array[3] >> 7 & 0x1) == 0x1);
    }

    public SystemStatus(final Parcel in) {
        this.batteryReadingActive = false;
        this.hasNandMemoryModule = false;
        this.SPIM2Comfigured = false;
        this.SPIM1Configured = false;
        this.SPIS0Configured = false;
        this.dataLoggerActive = false;
        this.batteryLevelUnderThreshold = false;
        this.coreInChangingMode = false;
        this.samplingReady = false;
        this.fileTableBuilt = false;
        this.listeningToEventChannel = false;
        this.listeningToChannel2 = false;
        this.listeningToChannel1 = false;
        this.listeningToChannel0 = false;
        this.coreConnectedViaBle = false;
        this.AD7Enabled = false;
        this.AD6Enabled = false;
        this.AD5Enabled = false;
        this.AD4Enabled = false;
        this.AD3Enabled = false;
        this.AD2Enabled = false;
        this.AD1Enabled = false;
        this.AD0Enabled = false;
        this.batteryReadingActive = (in.readByte() != 0);
        this.hasNandMemoryModule = (in.readByte() != 0);
        this.SPIM2Comfigured = (in.readByte() != 0);
        this.SPIM1Configured = (in.readByte() != 0);
        this.SPIS0Configured = (in.readByte() != 0);
        this.dataLoggerActive = (in.readByte() != 0);
        this.batteryLevelUnderThreshold = (in.readByte() != 0);
        this.coreInChangingMode = (in.readByte() != 0);
        this.samplingReady = (in.readByte() != 0);
        this.fileTableBuilt = (in.readByte() != 0);
        this.listeningToEventChannel = (in.readByte() != 0);
        this.listeningToChannel2 = (in.readByte() != 0);
        this.listeningToChannel1 = (in.readByte() != 0);
        this.listeningToChannel0 = (in.readByte() != 0);
        this.coreConnectedViaBle = (in.readByte() != 0);
        this.AD7Enabled = (in.readByte() != 0);
        this.AD6Enabled = (in.readByte() != 0);
        this.AD5Enabled = (in.readByte() != 0);
        this.AD4Enabled = (in.readByte() != 0);
        this.AD3Enabled = (in.readByte() != 0);
        this.AD2Enabled = (in.readByte() != 0);
        this.AD1Enabled = (in.readByte() != 0);
        this.AD0Enabled = (in.readByte() != 0);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(final Parcel dest, final int flags) {
        dest.writeByte((byte)(byte)(this.batteryReadingActive ? 1 : 0));
        dest.writeByte((byte)(byte)(this.hasNandMemoryModule ? 1 : 0));
        dest.writeByte((byte)(byte)(this.SPIM2Comfigured ? 1 : 0));
        dest.writeByte((byte)(byte)(this.SPIM1Configured ? 1 : 0));
        dest.writeByte((byte)(byte)(this.SPIS0Configured ? 1 : 0));
        dest.writeByte((byte)(byte)(this.dataLoggerActive ? 1 : 0));
        dest.writeByte((byte)(byte)(this.batteryLevelUnderThreshold ? 1 : 0));
        dest.writeByte((byte)(byte)(this.coreInChangingMode ? 1 : 0));
        dest.writeByte((byte)(byte)(this.samplingReady ? 1 : 0));
        dest.writeByte((byte)(byte)(this.fileTableBuilt ? 1 : 0));
        dest.writeByte((byte)(byte)(this.listeningToEventChannel ? 1 : 0));
        dest.writeByte((byte)(byte)(this.listeningToChannel2 ? 1 : 0));
        dest.writeByte((byte)(byte)(this.listeningToChannel1 ? 1 : 0));
        dest.writeByte((byte)(byte)(this.listeningToChannel0 ? 1 : 0));
        dest.writeByte((byte)(byte)(this.coreConnectedViaBle ? 1 : 0));
        dest.writeByte((byte)(byte)(this.AD7Enabled ? 1 : 0));
        dest.writeByte((byte)(byte)(this.AD6Enabled ? 1 : 0));
        dest.writeByte((byte)(byte)(this.AD5Enabled ? 1 : 0));
        dest.writeByte((byte)(byte)(this.AD4Enabled ? 1 : 0));
        dest.writeByte((byte)(byte)(this.AD3Enabled ? 1 : 0));
        dest.writeByte((byte)(byte)(this.AD2Enabled ? 1 : 0));
        dest.writeByte((byte)(byte)(this.AD1Enabled ? 1 : 0));
        dest.writeByte((byte)(byte)(this.AD0Enabled ? 1 : 0));
    }

    static {
        CREATOR = (Parcelable.Creator)new Parcelable.Creator<SystemStatus>() {
            public SystemStatus createFromParcel(final Parcel in) {
                return new SystemStatus(in);
            }
            
            public SystemStatus[] newArray(final int size) {
                return new SystemStatus[size];
            }
        };
    }
}
