/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: SACore.java
 * Author: sdelic
 *
 * History:  21.2.22. 10:43 created
 */

//
// Decompiled by Procyon v0.5.36
// 

package com.comtrade.feature_sensoria.sensorialibrary;

import android.Manifest;
import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.ParcelUuid;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresPermission;
import androidx.core.content.ContextCompat;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;


public class SACore implements SABundleInterface, Serializable {
    private static final String TAG;
    private String Tag;
    private static final int STATE_NOT_INITIALIZED = 0;
    private static final int STATE_INITIALIZED = 1;
    private static final int STATE_DISCONNECTED = 2;
    private static final int STATE_SCANNING = 3;
    private static final int STATE_CONNECTING = 4;
    private static final int STATE_CONNECTED = 5;
    private static final int STATE_ABORTED = 6;
    private int mConnectionState;
    private int mOldConnectionState;
    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothLeScanner mLEScanner;
    private ScanSettings settings;
    private List<ScanFilter> filters;
    private final Map<String, BluetoothGatt> connectedDeviceMap;
    private SADevice mDevice;
    private ArrayList<SADevice> mDeviceList;
    private final SADeviceInterface mIDevice;
    private final Context context;
    private ArrayList<SADevice> deviceDiscoveredList;
    private final ArrayList<SASensoriaStreamingService> serviceDiscoveredList;
    private final UUID UUID_SENSORIA_CORE_STREAMING_SERVICE;
    private final String SensoriaServiceUuidString;
    private final String BalanceServiceUuidString;
    private final String UUID_MASK;
    private final String UUID_MASK_NULL;
    private static BlockingQueue<SABundleObject> mSABundleQueue;
    private EnumMap<SAService.ServiceTypes, Handler> handlers;
    private EnumMap<SAService.ServiceTypes, UUID> serviceUUID;
    private final Handler mHandler;
    private static final long SCAN_PERIOD = 5000L;
    private ScanCallback mScanCallback;
    private BluetoothManager mBluetoothManager;
    private final Object mLock;
    private final BroadcastReceiver mBroadCastReceiver;
    private final BluetoothGattCallback mGattCallback;

    private void runOnUiThread(final Runnable runnable) {
        this.mHandler.post(runnable);
    }

    private void runOnServiceHandler(final Handler sHandler, final Runnable runnable) {
        sHandler.post(runnable);
    }

    public SACore(final SADeviceInterface delegate, final Context ctx) {
        this.mConnectionState = STATE_NOT_INITIALIZED;
        this.mOldConnectionState = 0;
        this.deviceDiscoveredList = new ArrayList<>();
        this.serviceDiscoveredList = new ArrayList<>();
        this.mScanCallback = null;
        this.mLock = new Object();

        this.UUID_MASK = "FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF";
        this.UUID_MASK_NULL = "00000000-0000-0000-0000-000000000000";
        this.SensoriaServiceUuidString = "1cac0001-656e-696c-4b5f-6e6572726157";
        this.BalanceServiceUuidString = "6e400001-b5a3-f393-e0a9-e50e24dcca9e";

        UUID UUID_GENERIC_ACCESS_SERVICE = UUID.fromString("00001800-0000-1000-8000-00805f9b34fb");
        UUID UUID_GENERIC_ATTRIBUTE_SERVICE = UUID.fromString("00001801-0000-1000-8000-00805f9b34fb");
        UUID UUID_DEVICE_INFORMATION_SERVICE = UUID.fromString("0000180a-0000-1000-8000-00805f9b34fb");
        UUID UUID_BATTERY_SERVICE = UUID.fromString("0000180f-0000-1000-8000-00805f9b34fb");
        this.UUID_SENSORIA_CORE_STREAMING_SERVICE = UUID.fromString(this.SensoriaServiceUuidString);
        UUID UUID_SENSORIA_CORE_CONTROL_POINT_SERVICE = UUID.fromString("1cac0001-656e-696c-4b5f-5f6e6167654e");
        UUID UUID_BALANCE_STREAMING_SERVICE = UUID.fromString(this.BalanceServiceUuidString);
        UUID UUID_NORDIC_DFU_SERVICE = UUID.fromString("0000fe59-0000-1000-8000-00805f9b34fb");

        this.handlers = new EnumMap<>(SAService.ServiceTypes.class);
        this.serviceUUID = new EnumMap<>(SAService.ServiceTypes.class);
        this.serviceUUID.put(SAService.ServiceTypes.GENERIC_ACCESS_SERVICE, UUID_GENERIC_ACCESS_SERVICE);
        this.serviceUUID.put(SAService.ServiceTypes.GENERIC_ATTRIBUTE_SERVICE, UUID_GENERIC_ATTRIBUTE_SERVICE);
        this.serviceUUID.put(SAService.ServiceTypes.DEVICE_INFORMATION_SERVICE, UUID_DEVICE_INFORMATION_SERVICE);
        this.serviceUUID.put(SAService.ServiceTypes.BATTERY_SERVICE, UUID_BATTERY_SERVICE);
        this.serviceUUID.put(SAService.ServiceTypes.SENSORIA_STREAMING_SERVICE, this.UUID_SENSORIA_CORE_STREAMING_SERVICE);
        this.serviceUUID.put(SAService.ServiceTypes.SENSORIA_CONTROL_POINT_SERVICE, UUID_SENSORIA_CORE_CONTROL_POINT_SERVICE);
        this.serviceUUID.put(SAService.ServiceTypes.BALANCE_STREAMING_SERVICE, UUID_BALANCE_STREAMING_SERVICE);

        connectedDeviceMap = new HashMap<>();

        this.mBroadCastReceiver = new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                final String action = intent.getAction();
                Log.d(Tag + "/BroadcastActions", "Action " + action + "received");
                int n = -1;
                if (action.hashCode() == -1530327060) {
                    if (action.equals("android.bluetooth.adapter.action.STATE_CHANGED")) {
                        n = 0;
                    }
                }
                Label_0435:
                {
                    if (n == 0) {
                        final int state = intent.getIntExtra("android.bluetooth.adapter.extra.STATE", -1);
                        switch (state) {
                            case 10: {
                                Log.d(Tag + "/BroadcastActions", "Bluetooth is off");
                                pushState(STATE_NOT_INITIALIZED);
                                mIDevice.didUninitialized();
                                mIDevice.didDeviceError(null, SAErrors.ERROR_BLUETOOTH_DISABLED);
                                break Label_0435;
                            }
                            case 13:
                            case 11: {
                                Log.d(SACore.this.Tag + "/BroadcastActions", "Bluetooth is turning off");
                                break Label_0435;
                            }
                            case 12: {
                                Log.d(SACore.this.Tag + "/BroadcastActions", "Bluetooth is on");
                                SACore.this.popState();
                                switch (SACore.this.mConnectionState) {
                                    case 0:
                                    case 1: {
                                        validateBluetoothConnection();
                                        SACore.this.start();
                                        break Label_0435;
                                    }
                                    case 2:
                                    case 3:
                                    case 4:
                                    case 6: {
                                        //SACore.this.mIDevice.didDisconnect(SACore.this.mDevice);
                                        break Label_0435;
                                    }
                                    case 5: {
                                        //SACore.this.mIDevice.didConnect(SACore.this.mDevice);
                                        break Label_0435;
                                    }
                                }
                                break;
                            }
                        }
                    }
                }
            }
        };

        this.mGattCallback = new BluetoothGattCallback() {
            private String connectionState(final int status) {
                switch (status) {
                    case 0: {
                        return "Disconnected";
                    }
                    case 1: {
                        return "Connecting";
                    }
                    case 2: {
                        return "Connected";
                    }
                    case 3: {
                        return "Disconnecting";
                    }
                    default: {
                        return String.valueOf(status);
                    }
                }
            }

            public void onConnectionStateChange(final BluetoothGatt gatt, final int status, final int newState) {
                Log.d(SACore.this.Tag, "SACore:onConnectionStateChange: " + status + " -> " + this.connectionState(newState));

                BluetoothDevice btDevice = gatt.getDevice();
                final String address = btDevice.getAddress();

                if (status == BluetoothGatt.GATT_SUCCESS && newState == BluetoothProfile.STATE_CONNECTED) {
                    Log.i(TAG, "Connected to GATT server.");

                    if (!connectedDeviceMap.containsKey(address)) {
                        connectedDeviceMap.put(address, gatt);
                        Log.i(TAG, "Attempting to start service discovery:" +
                                gatt.discoverServices());
                    }
                    // Broadcast if needed
                } else if (status == BluetoothGatt.GATT_SUCCESS && newState == BluetoothProfile.STATE_DISCONNECTED) {
                    if (SACore.this.mConnectionState != STATE_CONNECTED) {
                        SACore.this.pushState(STATE_ABORTED);
                    } else {
                        SACore.this.pushState(STATE_DISCONNECTED);
                    }
                    SACore.this.runOnUiThread(() -> {
                        Log.i(TAG, "Disconnected from GATT server.");
                        disconnectFromGATServer(address);
                    });
                } else if ((status == 8 || status == 133 || status == 34 || status == 22 || status == 257) && newState == BluetoothProfile.STATE_DISCONNECTED) {
                    SACore.this.pushState(STATE_DISCONNECTED);
                    SACore.this.runOnUiThread(() -> {
                        Log.i(TAG, "Disconnected from GATT server.");
                        disconnectFromGATServer(address);

                        SACore.this.mIDevice.didSignalLost(address);
                    });
                } else if (status != BluetoothGatt.GATT_SUCCESS) {
                    try {
                        throw new SAException.UnmanagedConnectionException(status, newState);
                    } catch (SAException.UnmanagedConnectionException e) {
                        e.printStackTrace();
                    }
                }
            }

            public void onServicesDiscovered(final BluetoothGatt gatt, final int status) {
                Log.d(SACore.this.Tag, "SaCore:OnServicesDiscovered, status:" + status);
                BluetoothDevice device = gatt.getDevice();
                final String address = device.getAddress();

                if (status == BluetoothGatt.GATT_SUCCESS) {
                    for (final BluetoothGattService gattService : gatt.getServices()) {
                        Log.d(SACore.this.Tag, "ServiceTypes Discovered: " + gattService.getUuid().toString());

                        if (gattService.getUuid().compareTo(SACore.this.UUID_SENSORIA_CORE_STREAMING_SERVICE) == 0) {
                            for (final SADevice saDevice : SACore.this.deviceDiscoveredList) {
                                if (saDevice.deviceMac.equals(address)) {
                                    final SASensoriaStreamingService service = new SASensoriaStreamingService(address, saDevice, SACore.mSABundleQueue);
                                    SACore.this.serviceDiscoveredList.add(service);

                                    break;
                                }
                            }
                            SACore.this.runOnUiThread(() -> {
                                SACore.this.pushState(STATE_CONNECTED);
                                SACore.this.mIDevice.didConnect(address);
                            });
                            break;
                        }
                    }
                }
            }
        };

        if (!SensoriaSdk.isInitialized()) {
            Log.e(this.Tag, "SDK NOT initialized - call SensoriaSdk.initialize() first");
            throw new IllegalStateException("SDK NOT initialized - call SensoriaSdk.initialize() first");
        }
        this.mIDevice = delegate;
        this.mHandler = new Handler(Looper.getMainLooper());
        this.mDevice = null;
        this.mDeviceList = new ArrayList<>();
        this.Tag = SACore.TAG;
        this.context = ctx;
        final IntentFilter filter1 = new IntentFilter("android.bluetooth.adapter.action.STATE_CHANGED");
        this.context.registerReceiver(this.mBroadCastReceiver, filter1);
        SACore.mSABundleQueue = new LinkedBlockingDeque<>();
        new Thread(() -> {
            try {
                while (true) {
                    final SABundleObject bundle = SACore.mSABundleQueue.take();
                    switch (bundle.getOperation()) {
                        case SERVICE_ERROR: {
                            SACore.this.onServiceError(bundle.getDevice(), bundle.getService(), bundle.getError());
                            continue;
                        }
                        case SERVICE_STARTED: {
                            SACore.this.onServiceStarted(bundle.getDevice(), bundle.getService(), bundle.getHandler());
                            continue;
                        }
                        case SERVICE_STARTING: {
                            SACore.this.onServiceStarting(bundle.getDevice(), bundle.getService());
                            continue;
                        }
                        case SERVICE_STOPPED: {
                            SACore.this.onServiceStopped(bundle.getDevice(), bundle.getService(), bundle.getHandler());
                        }
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
                Log.d(SACore.this.Tag, "FINISH THREAD!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            }
        }).start();

        validateBluetoothConnection();

        this.start();
    }

    public SACore(final SADeviceInterface delegate) {
        this(delegate, (Context) delegate);
    }

    public void dispose() {
        this.context.unregisterReceiver(this.mBroadCastReceiver);
        switch (this.mConnectionState) {
            case 5: {
                this.disconnectAll();
                break;
            }
            case 3: {
                this.stopScan();
                break;
            }
        }
    }

    /**
     * Validate the Bluetooth adapter state and dispatch the global state objects based on that state.
     */
    private void validateBluetoothConnection() {
        mBluetoothManager = (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = mBluetoothManager.getAdapter();
        if (mBluetoothAdapter == null) {
            mIDevice.didDeviceError(null, SAErrors.ERROR_BLUETOOTH_NOT_SUPPORTED);
        } else {
            if (!mBluetoothAdapter.isEnabled()) {
                mIDevice.didDeviceError(null, SAErrors.ERROR_BLUETOOTH_DISABLED);
            }
            if (mBluetoothAdapter.isEnabled()) {
                if (!context.getPackageManager().hasSystemFeature("android.hardware.bluetooth_le")) {
                    mIDevice.didDeviceError(null, SAErrors.ERROR_BLUETOOTH_LE_NOT_SUPPORTED);
                } else {
                    pushState(STATE_INITIALIZED);
                }
            }
        }
    }

    private ScanCallback getScanCallBack() {
        return new ScanCallback() {
            public void onScanResult(final int callbackType, final ScanResult result) {
                Log.i(SACore.this.Tag + "/onScanResult", "CALLBACK TYPE " + callbackType);
                Log.i(SACore.this.Tag + "/onScanResult", result.toString());
                SACore.this.onLeScan(result.getDevice(), result.getRssi());
            }

            public void onBatchScanResults(final List<ScanResult> results) {
                Log.i(SACore.this.Tag + "/onBatchScanResults", "CALLBACK");
                SACore.this.onBatchLeScan(results);
            }

            public void onScanFailed(final int errorCode) {
                Log.e(SACore.this.Tag + "/Scan Failed", "Error Code: " + errorCode);
                switch (errorCode) {
                    case 1: {
                        SACore.this.runOnUiThread(() -> SACore.this.mIDevice.didDeviceError(null, SAErrors.ERROR_SCAN_ALREADY_STARTED));
                        break;
                    }
                    case 2: {
                        SACore.this.runOnUiThread(() -> SACore.this.mIDevice.didDeviceError(null, SAErrors.ERROR_SCAN_APPLICATION_REGISTRATION_FAILED));
                        break;
                    }
                    case 4: {
                        SACore.this.runOnUiThread(() -> SACore.this.mIDevice.didDeviceError(null, SAErrors.ERROR_FEATURE_NOT_IMPLEMENTED));
                        break;
                    }
                    case 3: {
                        SACore.this.runOnUiThread(() -> SACore.this.mIDevice.didDeviceError(null, SAErrors.ERROR_SCAN_INTERNAL_ERROR));
                        break;
                    }
                    default: {
                        SACore.this.runOnUiThread(() -> SACore.this.mIDevice.didDeviceError(null, SAErrors.ERROR_FAILURE));
                        break;
                    }
                }

            }
        };
    }

    // TODO: 17.5.2022. Refactor these two methods
    private void onLeScan(final BluetoothDevice device, final int rssi) {
        final String deviceName = device.getName();
        if (deviceName != null && !deviceName.isEmpty()) {
            String foundDeviceCode;
            if (deviceName.length() > 12) {
                foundDeviceCode = deviceName.substring(12);
            } else {
                foundDeviceCode = deviceName;
            }
            final SADevice deviceDiscovered = new SADevice(this.mBluetoothAdapter.getRemoteDevice(device.getAddress()), rssi);
            if (!this.foundDeviceCodeInArray(foundDeviceCode)) {
                this.deviceDiscoveredList.add(deviceDiscovered);
                this.runOnUiThread(() -> SACore.this.mIDevice.didDeviceDiscovered(deviceDiscovered));
            } else {
                this.updateDeviceInArray(foundDeviceCode, rssi);
                this.runOnUiThread(() -> SACore.this.mIDevice.didDeviceDiscoveredUpdated(deviceDiscovered, false));
            }
        }
    }

    private void onBatchLeScan(final List<ScanResult> results) {
        final ArrayList<SADevice> updatedDeviceList = new ArrayList<>();

        for (final ScanResult result : results) {
            Log.i(this.Tag + "/onBatchScanResults", result.toString());
            final BluetoothDevice device = result.getDevice();
            final String deviceName = device.getName();
            final int rssi = result.getRssi();
            if (deviceName != null && !deviceName.isEmpty()) {
                final SADevice deviceDiscovered = new SADevice(this.mBluetoothAdapter.getRemoteDevice(device.getAddress()), rssi);
                updatedDeviceList.add(deviceDiscovered);
            }
        }

        for (Map.Entry<String, BluetoothGatt> connectedDevice : this.connectedDeviceMap.entrySet()) {
            for (SADevice device : this.deviceDiscoveredList) {
                if (device.deviceMac.equals(connectedDevice.getKey())) {
                    device.isConnected = true;
                    updatedDeviceList.add(device);
                }
            }
        }

        this.deviceDiscoveredList = updatedDeviceList;
        this.runOnUiThread(() -> SACore.this.mIDevice.didDeviceScanCompleted(SACore.this.deviceDiscoveredList));

        /*
        for (final SADevice device2 : updatedDeviceList) {
            if (!oldDeviceList.contains(device2)) {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        SACore.this.mIDevice.didDeviceDiscovered(device2);
                    }
                });
            }
            else {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        SACore.this.mIDevice.didDeviceDiscoveredUpdated(device2, false);
                    }
                });
            }
        }

        for (final SADevice device2 : oldDeviceList) {
            if (!updatedDeviceList.contains(device2)) {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        SACore.this.mIDevice.didDeviceDiscoveredUpdated(device2, true);
                    }
                });
            }
        }*/
    }

    private void start() {
        if (this.mConnectionState == STATE_INITIALIZED) {
            final Runnable runnable = () -> {
                while (ContextCompat.checkSelfPermission(SACore.this.context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    try {
                        Thread.sleep(1000L);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                SACore.this.pushState(STATE_DISCONNECTED);
                SACore.this.mLEScanner = mBluetoothAdapter.getBluetoothLeScanner();
                SACore.this.settings = new ScanSettings.Builder().setScanMode(ScanSettings.SCAN_MODE_BALANCED).build();
                SACore.this.filters = new ArrayList<>();
                SACore.this.filters.add(new ScanFilter.Builder().setServiceUuid(ParcelUuid.fromString(SACore.this.SensoriaServiceUuidString), ParcelUuid.fromString(SACore.this.UUID_MASK)).build());
                SACore.this.filters.add(new ScanFilter.Builder().setServiceUuid(ParcelUuid.fromString(SACore.this.BalanceServiceUuidString), ParcelUuid.fromString(SACore.this.UUID_MASK)).build());
                final Runnable runnable1 = SACore.this.mIDevice::didInitialized;
                SACore.this.runOnUiThread(runnable1);
            };
            new Thread(runnable).start();
        }
    }

    @RequiresPermission(allOf = {Manifest.permission.BLUETOOTH_ADMIN, Manifest.permission.BLUETOOTH})
    public void startScan(final long scanPeriod) {
        if (this.mConnectionState == STATE_DISCONNECTED) {
            this.mIDevice.didDeviceScanning();
            this.pushState(STATE_SCANNING);
            this.mScanCallback = this.getScanCallBack();
            AsyncTask.execute(() -> SACore.this.mLEScanner.startScan(filters, settings, SACore.this.mScanCallback));
            if (scanPeriod > 0L) {
                this.mHandler.postDelayed(SACore.this::stopScan, scanPeriod);
            }
        } else if (mConnectionState == STATE_NOT_INITIALIZED) {
            mIDevice.didUninitialized();
        } else {
            this.mIDevice.didDeviceError(null, SAErrors.ERROR_NOT_IN_STATE_DISCONNECTED);
        }
    }

    private boolean foundDeviceCodeInArray(final String foundDeviceCode) {
        for (final SADevice storedDeviceDiscovered : this.deviceDiscoveredList) {
            if (storedDeviceDiscovered.deviceCode.equals(foundDeviceCode)) {
                return true;
            }
        }
        return false;
    }

    private void updateDeviceInArray(final String foundDeviceCode, final int rssi) {
        for (final SADevice storedDeviceDiscovered : this.deviceDiscoveredList) {
            if (storedDeviceDiscovered.deviceCode.equals(foundDeviceCode)) {
                storedDeviceDiscovered.rssiAtScanTime = rssi;
                break;
            }
        }
    }

    public void stopScan() {
        if (this.mConnectionState == STATE_SCANNING) {
            this.pushState(STATE_DISCONNECTED);
            try {
                this.mLEScanner.stopScan(this.mScanCallback);
            } catch (Exception e) {
                e.printStackTrace();
            }
            this.mIDevice.didDeviceScanCompleted(this.deviceDiscoveredList);
        }
    }

    public ArrayList<SADevice> getDeviceDiscoveredList() {
        return this.deviceDiscoveredList;
    }

    public ArrayList<SASensoriaStreamingService> getServiceDiscoveredList() {
        return this.serviceDiscoveredList;
    }

    public void connect(final ArrayList<SADevice> listOfDevices) {
        long waitConnection;
        switch (Build.VERSION.SDK_INT) {
            case 19: {
                waitConnection = 600L;
                break;
            }
            case 20: {
                waitConnection = 500L;
                break;
            }
            case 21: {
                waitConnection = 400L;
                break;
            }
            case 22: {
                waitConnection = 300L;
                break;
            }
            case 23: {
                waitConnection = 200L;
                break;
            }
            case 24: {
                waitConnection = 100L;
                break;
            }
            default: {
                waitConnection = 0L;
                break;
            }
        }

        try {
            Log.d(this.Tag, "waiting " + waitConnection + " milliseconds before connecting");
            Thread.sleep(waitConnection);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }

        if (listOfDevices == null) {
            this.mIDevice.didDeviceError(null, SAErrors.ERROR_NO_DEVICE_FOUND);
        } else {
            this.mDeviceList = listOfDevices;
            try {
                this.connectOnTheMainThread(this.context);
            } catch (IllegalArgumentException e) {
                this.mIDevice.didDeviceError(this.mDevice, SAErrors.ERROR_INVALID_MAC_ADDRESS);
                this.pushState(STATE_DISCONNECTED);
            }
        }
    }

    @SuppressLint("MissingPermission")
//todo remove annotation when proper permission handling implemented
    private void connectOnTheMainThread(final Context context) {
        this.runOnUiThread(() -> {
            synchronized (SACore.this.mLock) {
                try {
                    for (final SADevice mDevice : mDeviceList) {
                        BluetoothDevice btDevice = mBluetoothAdapter.getRemoteDevice(mDevice.device.getAddress());
                        int connectionState = mBluetoothManager.getConnectionState(btDevice, BluetoothProfile.GATT);
                        SACore.this.mDevice = mDevice;
                        if (connectionState == BluetoothProfile.STATE_DISCONNECTED) {
                            // connect your device
                            if (Build.VERSION.SDK_INT < 23) {
                                btDevice.connectGatt(context, false, SACore.this.mGattCallback);
                            } else {
                                btDevice.connectGatt(context, false, SACore.this.mGattCallback, 2);
                            }
                        } else if (connectionState == BluetoothProfile.STATE_CONNECTED) {
                            // already connected . send Broadcast if needed
                        }
                    }

                    SACore.this.mIDevice.didConnecting(SACore.this.mDevice);
                    SACore.this.pushState(STATE_CONNECTING);
                } catch (IllegalArgumentException e) {
                    SACore.this.mIDevice.didDeviceError(SACore.this.mDevice, SAErrors.ERROR_INVALID_OR_NULL_CALLBACK);
                    SACore.this.mDevice = null;
                    SACore.this.Tag = SACore.TAG;
                }
            }
        });
    }

    public boolean isConnected() {
        return !this.connectedDeviceMap.isEmpty();
    }

    public void disconnect(final List<String> deviceMacAddresses) {
        synchronized (this.mLock) {
            for (String deviceMac : deviceMacAddresses) {
                disconnectFromGATServer(deviceMac);
            }
        }
    }

    private void disconnectFromGATServer(final String devicesAddress) {
        if (this.connectedDeviceMap.containsKey(devicesAddress)) {
            BluetoothGatt deviceGatt = connectedDeviceMap.get(devicesAddress);
            disconnectGattDevice(devicesAddress, deviceGatt);
        }
        this.pushState(STATE_DISCONNECTED);
    }


    public void disconnectAll() {
        synchronized (this.mLock) {
            for (Map.Entry<String, BluetoothGatt> connectedDevice : connectedDeviceMap.entrySet()) {
                disconnectGattDevice(connectedDevice.getKey(), connectedDevice.getValue());
            }

            this.pushState(STATE_DISCONNECTED);
        }
    }

    /**
     * Disconnects and removes any connected devices from the streaming services.
     *
     * @param key              The stored device mac address.
     * @param bluetoothProfile The device BluetoothGatt profile.
     */
    private void disconnectGattDevice(String key, @Nullable BluetoothGatt bluetoothProfile) {
        connectedDeviceMap.remove(key);

        ArrayList<SASensoriaStreamingService> servicesToRemove = new ArrayList<>();
        for (SASensoriaStreamingService service : serviceDiscoveredList) {
            if (service.mDeviceMac.equals(key)) {
                service.stop();
                servicesToRemove.add(service);
            }
        }

        //Close Gatt connection
        if (bluetoothProfile != null) {
            bluetoothProfile.close();
        }

        serviceDiscoveredList.removeAll(servicesToRemove);
        mIDevice.didDisconnect(key);
    }

    private void pushState(final int newState) {
        this.mOldConnectionState = this.mConnectionState;
        this.mConnectionState = newState;
    }

    private void popState() {
        this.mConnectionState = this.mOldConnectionState;
    }

    @Override
    public void onServiceStarted(final SADevice device, final SAService.ServiceTypes serviceTypes, final Handler mHandler) {
        Log.d(this.Tag, "SACore:OnServiceStarted");
        SAService.ServiceStatus.put(serviceTypes, true);
        this.pushState(STATE_DISCONNECTED);
        this.handlers.put(serviceTypes, mHandler);
        if (device != this.mDevice) {
            Log.e(this.Tag, String.format("BUG!!! onServiceStarted called with mismatching device, device = %s, mDevice = %s", device.deviceName, this.mDevice.deviceName));
        }
        this.runOnUiThread(() -> SACore.this.mIDevice.didServiceStatusChange(device, SAService.ServiceStatus));
    }

    @Override
    public void onServiceStopped(final SADevice device, final SAService.ServiceTypes serviceTypes, final Handler mHandler) {
        Log.d(this.Tag, "SACore:OnServiceStopped");
        SAService.ServiceStatus.put(serviceTypes, false);
        if (!SAService.ServiceStatus.containsValue(true)) {
            this.pushState(STATE_CONNECTED);
        }
        this.handlers.remove(serviceTypes);
        if (device != this.mDevice) {
            Log.e(this.Tag, String.format("BUG!!! onServiceStopped called with mismatching device, device = %s, mDevice = %s", device.deviceName, this.mDevice.deviceName));
        }
        this.runOnUiThread(() -> SACore.this.mIDevice.didServiceStatusChange(device, SAService.ServiceStatus));
    }

    @Override
    public void onServiceError(final SADevice device, final SAService.ServiceTypes serviceTypes, final SAErrors error) {
        Log.d(this.Tag, "SACore:OnServiceError");
        if (device != this.mDevice) {
            Log.e(this.Tag, String.format("BUG!!! onServiceStarted called with mismatching device, device = %s, mDevice = %s", device.deviceName, this.mDevice.deviceName));
        }
        this.runOnUiThread(() -> SACore.this.mIDevice.didDeviceError(device, error));
    }

    @Override
    public void onServiceStarting(final SADevice device, final SAService.ServiceTypes serviceTypes) {
        Log.d(this.Tag, String.format("SACore:OnServiceStarting: %s", serviceTypes));
    }

    static {
        TAG = SACore.class.getSimpleName();
    }
}
