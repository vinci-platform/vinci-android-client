/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: Appearance.java
 * Author: sdelic
 *
 * History:  21.2.22. 10:43 created
 */

//
// Decompiled by Procyon v0.5.36
// 

package com.comtrade.feature_sensoria.sensorialibrary;

public enum Appearance
{
    Unknown((short)0), 
    Phone((short)64), 
    Computer((short)128), 
    Watch((short)192), 
    SportsWatch((short)193), 
    Clock((short)256), 
    Display((short)320), 
    RemoteControl((short)384), 
    EyeGlasses((short)448), 
    Tag((short)512), 
    Keyring((short)576), 
    MediaPlayer((short)640), 
    BarcodeScanner((short)704), 
    Thermometer((short)768), 
    EarThermometer((short)769), 
    HeartRateSensor((short)832), 
    HeartRateBeltHeartRateSensor((short)833), 
    BloodPressure((short)896), 
    ArmBloodPressure((short)897), 
    WristBloodPressure((short)898), 
    HumanInterfaceDevice((short)960), 
    HidKeyboard((short)961), 
    HidMouse((short)962), 
    HidJoystick((short)963), 
    HidGamePad((short)964), 
    HidDigitizerTablet((short)965), 
    HidCardReader((short)966), 
    HidDigitalPen((short)967), 
    HidBarcodeScanner((short)968), 
    GlucoseMeter((short)1024), 
    RunningWalkingSensor((short)1088), 
    InShoeRunningWalkingSensor((short)1089), 
    OnShoeRunningWalkingSensor((short)1090), 
    OnHipRunningWalkingSensor((short)1091), 
    Cycling((short)1152), 
    CyclingComputer((short)1153), 
    CyclingSpeedSensor((short)1154), 
    CyclingCadenceSensor((short)1155), 
    CyclingPowerSensor((short)1156), 
    CyclingSpeedAndCadenceSensor((short)1157), 
    ControlDevice((short)1216), 
    ControlDeviceSwitch((short)1217), 
    ControlDeviceMultiSwitch((short)1218), 
    ControlDeviceButton((short)1219), 
    ControlDeviceSlider((short)1220), 
    ControlDeviceRotary((short)1221), 
    ControlDeviceTouchPanel((short)1121), 
    NetworkDevice((short)1280), 
    NetworkAccessPoint((short)1281), 
    Sensor((short)1344), 
    MotionSensor((short)1345), 
    AirQualitySensor((short)1346), 
    TemperatureSensor((short)1347), 
    HumiditySensor((short)1348), 
    LeakSensor((short)1349), 
    SmokeSensor((short)1350), 
    OccupancySensor((short)1351), 
    ContactSensor((short)1352), 
    CarbonMonoxide((short)1353), 
    CarbonDioxideSensor((short)1354), 
    AmbientLight((short)1355), 
    EnergySensor((short)1356), 
    ColorLightSensor((short)1357), 
    RainSensor((short)1358), 
    FireSensor((short)1359), 
    WindSensor((short)1360), 
    ProximitySensor((short)1361), 
    MultiSensor((short)1362), 
    LightFixtures((short)1408), 
    WallLightFixture((short)1409), 
    CeilingLightFixture((short)1410), 
    FloorLightFixture((short)1411), 
    CabinetLightFixture((short)1412), 
    DeskLightFixture((short)1413), 
    TrofferLightFixture((short)1414), 
    PendantLightFixture((short)1415), 
    IngroundLightFixture((short)1416), 
    FloodLightFixture((short)1417), 
    UnderwaterLightFixture((short)1418), 
    BollardLightFixture((short)1419), 
    PathwayLightFixture((short)1420), 
    GardenLightFixture((short)1421), 
    PoletopLightFixture((short)1422), 
    SpotlightLightFixture((short)1423), 
    LinearLightFixture((short)1424), 
    StreetLightFixture((short)1425), 
    ShelvesLightFixture((short)1426), 
    HighbayLowbayLightFixture((short)1427), 
    EmergencyExitLightFixture((short)1428), 
    Fan((short)1472), 
    CeilingFan((short)1473), 
    AxialFan((short)1474), 
    ExhaustFan((short)1475), 
    PedestalFan((short)1476), 
    DeskFan((short)1477), 
    WallFan((short)1478), 
    HVAC((short)1536), 
    ThermostatHVAC((short)1537), 
    AirConditioning((short)1600), 
    Humidifier((short)1664), 
    Heater((short)1728), 
    RadiatorHeater((short)1729), 
    BoilerHeater((short)1730), 
    HeatPumpHeater((short)1731), 
    InfraredHeater((short)1732), 
    RadiantPanelHeater((short)1733), 
    FanHeater((short)1734), 
    AirCurtain((short)1735), 
    AccessControl((short)1792), 
    AccessDoorAccess((short)1793), 
    GarageDoorAccess((short)1794), 
    EmergencyExitDoorAccess((short)1795), 
    AccessLockAccess((short)1796), 
    ElevatorAccess((short)1797), 
    WindowAccess((short)1798), 
    EntranceGateAccess((short)1799), 
    MotorizedDevice((short)1856), 
    MotorizedGate((short)1857), 
    AwningMotorized((short)1858), 
    BlindsOrShadesMotorized((short)1859), 
    CurtainsMotorized((short)1860), 
    ScreenMotorized((short)1861), 
    PowerDevice((short)1920), 
    PowerOutlet((short)1921), 
    PowerStrip((short)1922), 
    PlugPower((short)1923), 
    PowerSupply((short)1924), 
    LEDDriverPower((short)1925), 
    FluorescentLampGearPower((short)1926), 
    HIDLampGearPower((short)1927), 
    LightSource((short)1984), 
    IncandescentLightBulb((short)1985), 
    LEDBulbLight((short)1986), 
    HIDLampLight((short)1987), 
    FluorescentLampLight((short)1988), 
    LEDArrayLight((short)1989), 
    MultiColorLEDArrayLight((short)1990), 
    PulseOximeter((short)3136), 
    FingertipPulseOximeter((short)3137), 
    WristWornPulseOximeter((short)3138), 
    WeightScale((short)3200), 
    OutdoorSportsActivity((short)3200), 
    LocationDisplayDeviceOutdoorSportsActivity((short)5184), 
    LocationAndNavigationDisplayDeviceOutdoorSportsActivity((short)5185), 
    LocationPodOutdoorSportsActivity((short)5186), 
    LocationAndNavigationPodOutdoorSportsActivity((short)5187), 
    NotSupported((short)(-1));
    
    private short shortValue;
    
    private Appearance(final short shortValue) {
        this.shortValue = shortValue;
    }
    
    short getShortVal() {
        return this.shortValue;
    }
    
    static Appearance get(final short value) {
        for (final Appearance ap : values()) {
            if (ap.getShortVal() == value) {
                return ap;
            }
        }
        return null;
    }
}
