/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: GarmentConfigurationStorage.java
 * Author: sdelic
 *
 * History:  15.3.22. 10:01 created
 */

//
// Decompiled by Procyon v0.5.36
// 

package com.comtrade.feature_sensoria.sensorialibrary;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class GarmentConfigurationStorage extends BaseConfigurationStorage
{
    public GarmentType garmentType;
    public SensorCalibration[] calibrationData;
    protected final String iTAG;
    public static final Parcelable.Creator CREATOR;

    public GarmentConfigurationStorage() {
        this.iTAG = GarmentConfigurationStorage.class.getSimpleName();
        this.storageType = StorageTypes.GarmentConfigurationStorage;
        this.garmentType = GarmentType.Unknown;
        this.InitializeCalibrationData();
    }

    private void InitializeCalibrationData() {
        this.calibrationData = new SensorCalibration[8];
        for (int i = 0; i < 8; ++i) {
            this.calibrationData[i] = new SensorCalibration();
        }
    }

    @Override
    void Deserialize(final byte[] payload) throws SAException.InvalidGarmentTypeException, SAException.InvalidBodyLocationException, SAException.InvalidSensorTypeException {
        super.Deserialize(payload);
        this.garmentType = GarmentType.get(new UInt32(payload, 32).intValue());
        if (this.garmentType == null) {
            throw new SAException.InvalidGarmentTypeException(new UInt32(payload, 32).intValue());
        }
        if (this.calibrationData == null) {
            this.InitializeCalibrationData();
        }
        int offset = 36;
        for (final SensorCalibration sensorData : this.calibrationData) {
            sensorData.sensorType = SensorType.get(new UInt32(payload, offset).intValue());
            if (sensorData.sensorType == null) {
                throw new SAException.InvalidSensorTypeException(new UInt32(payload, offset).intValue());
            }
            sensorData.calibrationParameterA = new UInt32(payload, offset + 4).floatValue();
            sensorData.calibrationParameterB = new UInt32(payload, offset + 8).floatValue();
            sensorData.calibrationParameterC = new UInt32(payload, offset + 12).floatValue();
            offset += 16;
        }
    }

    @Override
    protected void FillDataForSerialization(final byte[] payload) {
        super.FillDataForSerialization(payload);
        int offset = 32;
        final ByteBuffer tmpPayload = ByteBuffer.allocate(4).order(ByteOrder.BIG_ENDIAN);
        tmpPayload.putInt(0, this.garmentType.getNumVal());
        System.arraycopy(tmpPayload.array(), 0, payload, offset, 4);
        Log.i(this.iTAG, SAService.ToHexString(payload));
        offset = 36;
        for (final SensorCalibration sensorData : this.calibrationData) {
            tmpPayload.putInt(0, sensorData.sensorType.getNumVal());
            System.arraycopy(tmpPayload.array(), 0, payload, offset, 4);
            tmpPayload.putFloat(0, sensorData.calibrationParameterA);
            System.arraycopy(tmpPayload.array(), 0, payload, offset + 4, 4);
            tmpPayload.putFloat(0, sensorData.calibrationParameterB);
            System.arraycopy(tmpPayload.array(), 0, payload, offset + 8, 4);
            tmpPayload.putFloat(0, sensorData.calibrationParameterC);
            System.arraycopy(tmpPayload.array(), 0, payload, offset + 12, 4);
            offset += 16;
        }
        Log.i(this.iTAG, SAService.ToHexString(payload));
    }

    public GarmentConfigurationStorage(final Parcel in) {
        super(in);
        this.iTAG = GarmentConfigurationStorage.class.getSimpleName();
        final int enumTmp = in.readInt();
        this.garmentType = GarmentType.get(enumTmp);
        this.calibrationData = (SensorCalibration[])in.createTypedArray((Parcelable.Creator)SensorCalibration.CREATOR);
    }

    @Override
    public void writeToParcel(final Parcel dest, final int flags) {
        super.writeToParcel(dest, flags);
        final int enumTmp = this.garmentType.getNumVal();
        dest.writeInt(enumTmp);
        dest.writeTypedArray((Parcelable[])this.calibrationData, 0);
    }

    static {
        CREATOR = (Parcelable.Creator)new Parcelable.Creator() {
            public GarmentConfigurationStorage createFromParcel(final Parcel in) {
                return new GarmentConfigurationStorage(in);
            }
            
            public GarmentConfigurationStorage[] newArray(final int size) {
                return new GarmentConfigurationStorage[size];
            }
        };
    }
}
