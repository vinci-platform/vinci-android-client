/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: CSDeviceName.java
 * Author: sdelic
 *
 * History:  21.2.22. 10:43 created
 */

//
// Decompiled by Procyon v0.5.36
// 

package com.comtrade.feature_sensoria.sensorialibrary;

import android.bluetooth.BluetoothGattCharacteristic;

class CSDeviceName extends SACommand
{
    private final short myCharacteristic = 10752;
    
    CSDeviceName() {
        super(Command.DeviceName.getNumVal());
    }
    
    CSDeviceName(final String value) {
        super(Command.DeviceName.getNumVal(), value);
    }
    
    String readDeviceName() {
        final SensoriaGattCharacteristic characteristic = this.waitForCharacteristic(false);
        if (characteristic != null && !this.isInternalError()) {
            final String retval = characteristic.getStringValue(0);
            return retval;
        }
        return null;
    }
    
    boolean writeDeviceName() {
        final SensoriaGattCharacteristic characteristic = this.waitForCharacteristic(false);
        return characteristic != null && !this.isInternalError();
    }
    
    @Override
    String getHex() {
        final StringBuilder builder = new StringBuilder();
        for (final byte b : ((String)this.value).getBytes()) {
            builder.append(String.format("%02X", b));
        }
        return builder.toString();
    }
    
    @Override
    void setValue(final BluetoothGattCharacteristic characteristic) {
        characteristic.setValue((String)this.value);
    }
    
    @Override
    protected boolean isMyCharacteristic(final SensoriaGattCharacteristic characteristic) {
        final Long uuid16 = BluetoothUtils.get16BitBluetoothUUID(characteristic.getUuid());
        return 10752 == uuid16.shortValue();
    }
}
