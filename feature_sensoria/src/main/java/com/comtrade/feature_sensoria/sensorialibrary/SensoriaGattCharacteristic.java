/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: SensoriaGattCharacteristic.java
 * Author: sdelic
 *
 * History:  15.3.22. 10:01 created
 */

//
// Decompiled by Procyon v0.5.36
// 

package com.comtrade.feature_sensoria.sensorialibrary;

import android.bluetooth.BluetoothGattCharacteristic;

import java.util.UUID;

class SensoriaGattCharacteristic extends BluetoothGattCharacteristic implements QueueableComplexObject
{
    private byte[] value;
    private int status;
    
    SensoriaGattCharacteristic(final UUID uuid, final int properties, final int permissions) {
        super(uuid, properties, permissions);
    }
    
    SensoriaGattCharacteristic(final BluetoothGattCharacteristic characteristic, final int status) {
        super(characteristic.getUuid(), characteristic.getProperties(), characteristic.getPermissions());
        this.value = characteristic.getValue().clone();
        this.status = status;
    }
    
    SensoriaGattCharacteristic(final BluetoothGattCharacteristic characteristic) {
        super(characteristic.getUuid(), characteristic.getProperties(), characteristic.getPermissions());
        this.value = characteristic.getValue().clone();
        this.status = 0;
    }
    
    int getStatus() {
        return this.status;
    }
    
    private int getTypeLen(final int formatType) {
        return formatType & 0xF;
    }
    
    private int unsignedByteToInt(final byte b) {
        return b & 0xFF;
    }
    
    private int unsignedBytesToInt(final byte b0, final byte b1, final byte b2, final byte b3) {
        return this.unsignedByteToInt(b0) + (this.unsignedByteToInt(b1) << 8) + (this.unsignedByteToInt(b2) << 16) + (this.unsignedByteToInt(b3) << 24);
    }
    
    private int unsignedBytesToInt(final byte b0, final byte b1) {
        return this.unsignedByteToInt(b0) + (this.unsignedByteToInt(b1) << 8);
    }
    
    private int unsignedToSigned(int unsigned, final int size) {
        if ((unsigned & 1 << size - 1) != 0x0) {
            unsigned = -1 * ((1 << size - 1) - (unsigned & (1 << size - 1) - 1));
        }
        return unsigned;
    }
    
    private float bytesToFloat(final byte b0, final byte b1) {
        final int mantissa = this.unsignedToSigned(this.unsignedByteToInt(b0) + ((this.unsignedByteToInt(b1) & 0xF) << 8), 12);
        final int exponent = this.unsignedToSigned(this.unsignedByteToInt(b1) >> 4, 4);
        return (float)(mantissa * Math.pow(10.0, exponent));
    }
    
    private float bytesToFloat(final byte b0, final byte b1, final byte b2, final byte b3) {
        final int mantissa = this.unsignedToSigned(this.unsignedByteToInt(b0) + (this.unsignedByteToInt(b1) << 8) + (this.unsignedByteToInt(b2) << 16), 24);
        return (float)(mantissa * Math.pow(10.0, b3));
    }
    
    public byte[] getValue() {
        return this.value;
    }
    
    public Integer getIntValue(final int formatType, final int offset) {
        if (offset + this.getTypeLen(formatType) > this.value.length) {
            return null;
        }
        switch (formatType) {
            case 17: {
                return this.unsignedByteToInt(this.value[offset]);
            }
            case 18: {
                return this.unsignedBytesToInt(this.value[offset], this.value[offset + 1]);
            }
            case 20: {
                return this.unsignedBytesToInt(this.value[offset], this.value[offset + 1], this.value[offset + 2], this.value[offset + 3]);
            }
            case 33: {
                return this.unsignedToSigned(this.unsignedByteToInt(this.value[offset]), 8);
            }
            case 34: {
                return this.unsignedToSigned(this.unsignedBytesToInt(this.value[offset], this.value[offset + 1]), 16);
            }
            case 36: {
                return this.unsignedToSigned(this.unsignedBytesToInt(this.value[offset], this.value[offset + 1], this.value[offset + 2], this.value[offset + 3]), 32);
            }
            default: {
                return null;
            }
        }
    }
    
    public Float getFloatValue(final int formatType, final int offset) {
        if (offset + this.getTypeLen(formatType) > this.value.length) {
            return null;
        }
        switch (formatType) {
            case 50: {
                return this.bytesToFloat(this.value[offset], this.value[offset + 1]);
            }
            case 52: {
                return this.bytesToFloat(this.value[offset], this.value[offset + 1], this.value[offset + 2], this.value[offset + 3]);
            }
            default: {
                return null;
            }
        }
    }
    
    public String getStringValue(final int offset) {
        if (this.value == null || offset > this.value.length) {
            return null;
        }
        final byte[] strBytes = new byte[this.value.length - offset];
        for (int i = 0; i != this.value.length - offset; ++i) {
            strBytes[i] = this.value[offset + i];
        }
        return new String(strBytes);
    }
    
    public SensoriaGattCharacteristic asComplexObject() {
        return this;
    }
}
