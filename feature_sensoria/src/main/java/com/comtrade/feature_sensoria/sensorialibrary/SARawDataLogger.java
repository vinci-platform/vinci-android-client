/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: SARawDataLogger.java
 * Author: sdelic
 *
 * History:  21.2.22. 10:43 created
 */

//
// Decompiled by Procyon v0.5.36
// 

package com.comtrade.feature_sensoria.sensorialibrary;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

class SARawDataLogger
{
    private static final String tag = "SDK/RawDataLogger";
    private static boolean rawDataLoggersEnabled;
    private boolean mRawDataLoggerEnabled;
    private SADevice mDevice;
    private File mRootFile;
    private File mLogFile;
    
    public static void enableDataLoggers(final boolean enabled) {
//        SARawDataLogger.rawDataLoggersEnabled = enabled;
    }
    
    public SARawDataLogger(final SADevice device, final String logName) {
//        this.mRawDataLoggerEnabled = false;
//        if (logName == null || device == null) {
//            Log.e("SDK/RawDataLogger", "Invalid parameters");
//            return;
//        }
//        if (!SARawDataLogger.rawDataLoggersEnabled) {
//            Log.e("SDK/RawDataLogger", "Raw data logs are not enabled or allowed by user, no log will be created for " + logName);
//            return;
//        }
//        try {
//            final String state = Environment.getExternalStorageState();
//            if (!"mounted".equals(state)) {
//                Log.e("SDK/RawDataLogger", "No external storage - unable to log data");
//                return;
//            }
//            final File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), "com.sensoria.sensorialibrary");
//            if (!file.exists()) {
//                if (!file.mkdirs()) {
//                    Log.e("SDK/RawDataLogger", "Error creating folder - permission not asked/granted by user");
//                    return;
//                }
//            }
//            else if (!file.canWrite()) {
//                Log.e("SDK/RawDataLogger", "Unable to access folder - permission not asked/granted by user");
//                return;
//            }
//            this.mRootFile = file;
//            this.mDevice = device;
//            this.mLogFile = new File(this.mRootFile, String.format(Locale.US, "%s-%s.csv", getTimeStampStringShort(), logName));
//            if (this.mLogFile.exists()) {
//                Log.e("SDK/RawDataLogger", "Log already exists " + logName);
//                return;
//            }
//            this.mLogFile.createNewFile();
//            this.writeHeaders();
//            this.mRawDataLoggerEnabled = true;
//        }
//        catch (IOException e) {
//            e.printStackTrace();
//            Log.e("SDK/RawDataLogger", "Error creating/writing to log file " + this.mLogFile.getName());
//        }
    }
    
    private void writeHeaders() throws IOException {
//        final BufferedWriter buf = new BufferedWriter(new FileWriter(this.mLogFile, true));
//        buf.append((CharSequence)"Tag,AutoTag,Tick,T,S0,S1,S2,Ax,Ay,Az,Gx,Gy,Gz,Mx,My,Mz,HRM,S3,S4,S5,S6,S7,Yaw,Pitch,Roll,RSSI,Timestamp");
//        buf.newLine();
//        buf.append((CharSequence)"SensoriaRawDataFormat: 3.0");
//        buf.newLine();
//        buf.append((CharSequence)"FirmwareRevision: N/A");
//        buf.newLine();
//        buf.append((CharSequence)("DeviceName: " + this.mDevice.deviceName));
//        buf.newLine();
//        buf.append((CharSequence)"SerialNumber: N/A");
//        buf.newLine();
//        buf.append((CharSequence)"Location: N/A");
//        buf.newLine();
//        buf.append((CharSequence)("Start: " + getTimeStampString()));
//        buf.newLine();
//        buf.append((CharSequence)"SamplingFrequency: N/A");
//        buf.newLine();
//        buf.append((CharSequence)"Tag,AutoTag,Tick,T,S0,S1,S2,Ax,Ay,Az,Gx,Gy,Gz,Mx,My,Mz,HRM,S3,S4,S5,S6,S7,Yaw,Pitch,Roll,RSSI,Timestamp");
//        buf.newLine();
//        buf.close();
    }
    
    public void logDataPoint(final SADataPoint dataPoint) {
//        try {
//            final String rawData = String.format(Locale.US, ",,%d,%d,%d,%d,%d,%f,%f,%f,%f,%f,%f,%f,%f,%f,,%d,%d,%d,%d,%d,,,,,%s", dataPoint.getTickCount(), dataPoint.getTickCount() * 20, dataPoint.getChannels()[0], dataPoint.getChannels()[1], dataPoint.getChannels()[2], dataPoint.getAccelerometers()[0], dataPoint.getAccelerometers()[1], dataPoint.getAccelerometers()[2], dataPoint.getGyroscopes()[0], dataPoint.getGyroscopes()[1], dataPoint.getGyroscopes()[2], dataPoint.getMagnetometers()[0], dataPoint.getMagnetometers()[1], dataPoint.getMagnetometers()[2], dataPoint.getChannels()[3], dataPoint.getChannels()[4], dataPoint.getChannels()[5], dataPoint.getChannels()[6], dataPoint.getChannels()[7], getTimeStampString());
//            Log.v("SDK/RawDataLogger", "Raw Data: " + rawData);
//            if (this.mRawDataLoggerEnabled) {
//                final BufferedWriter buf = new BufferedWriter(new FileWriter(this.mLogFile, true));
//                buf.append((CharSequence)rawData);
//                buf.newLine();
//                buf.flush();
//                buf.close();
//            }
//        }
//        catch (IOException e) {
//            e.printStackTrace();
//        }
    }
    
    private static String getTimeStampStringShort() {
        final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HHmmss", Locale.US);
        final Date dateTime = new Date(System.currentTimeMillis());
        return dateFormat.format(dateTime);
    }
    
    private static String getTimeStampString() {
        final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.US);
        final Date dateTime = new Date(System.currentTimeMillis());
        return dateFormat.format(dateTime);
    }
    
    static {
        SARawDataLogger.rawDataLoggersEnabled = false;
    }
}
