/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: SAPermissionUtils.java
 * Author: sdelic
 *
 * History:  21.2.22. 10:43 created
 */

//
// Decompiled by Procyon v0.5.36
// 

package com.comtrade.feature_sensoria.sensorialibrary;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.util.Log;

import androidx.annotation.ChecksSdkIntAtLeast;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

class SAPermissionUtils {
    private static final String TAG = "SensoriaSDK/Init";
    private static final String SENSORIA_SDK_PERMISSIONS_FILE = "sensorialibrary_permission";
    private static final String FIRST_TIME_KEY = "firstTime.";

    private static boolean getApplicationLaunchedFirstTime(final Context context, final String permission) {
        final SharedPreferences sharedPreferences = context.getSharedPreferences("sensorialibrary_permission", 0);
        final boolean isFirstTime = sharedPreferences.getBoolean("firstTime." + permission, true);
        return isFirstTime;
    }

    private static void setApplicationLaunchedFirstTime(final Context context, final String permission) {
        final SharedPreferences sharedPreferences = context.getSharedPreferences("sensorialibrary_permission", 0);
        final SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("firstTime." + permission, false);
        editor.apply();
    }

    @ChecksSdkIntAtLeast(api = Build.VERSION_CODES.M)
    private static boolean isRuntimePermissionRequired() {
        return Build.VERSION.SDK_INT >= 23;
    }

    static PermissionStatus checkPermission(final Context context, final String permission) {
        Log.d("SensoriaSDK/Init", "check permission: " + permission);
        if (!isRuntimePermissionRequired()) {
            Log.d("SensoriaSDK/Init", "Runtime permission not required, API < 23");
            return PermissionStatus.permissionGranted;
        }
        if (ContextCompat.checkSelfPermission(context, permission) == 0) {
            Log.d("SensoriaSDK/Init", "Permission already granted");
            return PermissionStatus.permissionGranted;
        }
        if (context instanceof Activity) {
            if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, permission)) {
                return PermissionStatus.permissionOptionalRationaleRequested;
            }
            if (getApplicationLaunchedFirstTime(context, permission)) {
                Log.d("SensoriaSDK/Init", "ApplicationLaunchedFirstTime");
                setApplicationLaunchedFirstTime(context, permission);
                return PermissionStatus.permissionToBeRequested;
            }
            Log.d("SensoriaSDK/Init", "onPermissionDisabled");
            return PermissionStatus.permissionDisabled;
        } else {
            Log.d("SensoriaSDK/Init", "ServiceTypes: Permission Requested");
            if (getApplicationLaunchedFirstTime(context, permission)) {
                Log.d("SensoriaSDK/Init", "ApplicationLaunchedFirstTime");
                setApplicationLaunchedFirstTime(context, permission);
                return PermissionStatus.permissionToBeRequested;
            }
            Log.d("SensoriaSDK/Init", "onPermissionDisabled");
            return PermissionStatus.permissionDisabled;
        }
    }

    public enum PermissionStatus {
        permissionGranted,
        permissionToBeRequested,
        permissionOptionalRationaleRequested,
        permissionDisabled;
    }
}
