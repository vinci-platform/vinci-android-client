/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: SAException.java
 * Author: sdelic
 *
 * History:  15.3.22. 10:01 created
 */

//
// Decompiled by Procyon v0.5.36
// 

package com.comtrade.feature_sensoria.sensorialibrary;

import java.io.IOException;

public final class SAException
{
    public static final class InvalidStorageTypeException extends Exception
    {
        InvalidStorageTypeException(final String message) {
            super(message);
        }
        
        InvalidStorageTypeException(final String message, final StorageTypes storageType) {
            super(String.format(message, storageType.toString()));
        }
    }
    
    static final class InvalidPayloadException extends Exception
    {
        InvalidPayloadException(final String message) {
            super(message);
        }
        
        InvalidPayloadException(final byte[] payload) {
            super(String.format("Payload is invalid and could not be deserialized: %s", SAService.ToHexString(payload)));
        }
        
        InvalidPayloadException(final String message, final byte[] payload) {
            super(String.format(message, SAService.ToHexString(payload)));
        }
    }
    
    static final class UnformattedStorageException extends Exception
    {
    }
    
    static final class CrcFailedException extends Exception
    {
        CrcFailedException(final byte[] payload) {
            super(String.format("Payload failed CRC validation: %s", SAService.ToHexString(payload)));
        }
    }
    
    public static final class InvalidSerialNumberException extends Exception
    {
        InvalidSerialNumberException(final String message) {
            super(message);
        }
    }
    
    static final class InvalidGarmentTypeException extends Exception
    {
        InvalidGarmentTypeException(final int garmentType) {
            super(String.format("Invalid garment type %d", garmentType));
        }
    }
    
    static final class UnmanagedConnectionException extends Exception
    {
        UnmanagedConnectionException(final int status, final int newState) {
            super("Unknown status:" + status + " or newState:" + newState);
        }
    }
    
    static final class InvalidBodyLocationException extends Throwable
    {
        InvalidBodyLocationException(final int bodyLocation) {
            super(String.format("Invalid body location %d", bodyLocation));
        }
    }
    
    static final class InvalidSensorTypeException extends Throwable
    {
        InvalidSensorTypeException(final int sensorType) {
            super(String.format("Invalid sensor type %d", sensorType));
        }
    }
    
    static final class NullSAServiceInterfaceException extends Throwable
    {
        NullSAServiceInterfaceException(final SAService service) {
            super(String.format("The service %s has null or not implemented SAServiceInterface", service.toString()));
        }
    }
    
    static class SizeValidationException extends IOException
    {
        private static final long serialVersionUID = -6467104024030837875L;
        
        public SizeValidationException(final String message) {
            super(message);
        }
    }
    
    static class UploadAbortedException extends Exception
    {
        private static final long serialVersionUID = -6901728550661937942L;
        
        public UploadAbortedException() {
        }
    }
    
    static class DeviceDisconnectedException extends Exception
    {
        private static final long serialVersionUID = -6901728550661937942L;
        
        public DeviceDisconnectedException(final String message) {
            super(message);
        }
    }
    
    static class DfuException extends Exception
    {
        private static final long serialVersionUID = -6901728550661937942L;
        private final int mError;
        
        public DfuException(final String message, final int state) {
            super(message);
            this.mError = state;
        }
        
        public int getErrorNumber() {
            return this.mError;
        }
        
        @Override
        public String getMessage() {
            return super.getMessage() + " (error " + (this.mError & 0xFFFFBFFF) + ")";
        }
    }
    
    static class HexFileValidationException extends IOException
    {
        private static final long serialVersionUID = -6467104024030837875L;
        
        public HexFileValidationException(final String message) {
            super(message);
        }
    }
    
    static class RemoteDfuException extends Exception
    {
        private static final long serialVersionUID = -6901728550661937942L;
        private final int mState;
        
        public RemoteDfuException(final String message, final int state) {
            super(message);
            this.mState = state;
        }
        
        public int getErrorNumber() {
            return this.mState;
        }
        
        @Override
        public String getMessage() {
            return super.getMessage() + " (error " + this.mState + ")";
        }
    }
    
    static class UnknownResponseException extends Exception
    {
        private static final long serialVersionUID = -8716125467309979289L;
        private static final char[] HEX_ARRAY;
        private final byte[] mResponse;
        private final int mExpectedReturnCode;
        private final int mExpectedOpCode;
        
        public UnknownResponseException(final String message, final byte[] response, final int expectedReturnCode, final int expectedOpCode) {
            super(message);
            this.mResponse = ((response != null) ? response : new byte[0]);
            this.mExpectedReturnCode = expectedReturnCode;
            this.mExpectedOpCode = expectedOpCode;
        }
        
        @Override
        public String getMessage() {
            return String.format("%s (response: %s, expected: 0x%02X%02X..)", super.getMessage(), bytesToHex(this.mResponse, 0, this.mResponse.length), this.mExpectedReturnCode, this.mExpectedOpCode);
        }
        
        public static String bytesToHex(final byte[] bytes, final int start, final int length) {
            if (bytes == null || bytes.length <= start || length <= 0) {
                return "";
            }
            final int maxLength = Math.min(length, bytes.length - start);
            final char[] hexChars = new char[maxLength * 2];
            for (int j = 0; j < maxLength; ++j) {
                final int v = bytes[start + j] & 0xFF;
                hexChars[j * 2] = UnknownResponseException.HEX_ARRAY[v >>> 4];
                hexChars[j * 2 + 1] = UnknownResponseException.HEX_ARRAY[v & 0xF];
            }
            return "0x" + new String(hexChars);
        }
        
        static {
            HEX_ARRAY = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
        }
    }
    
    static class RemoteDfuExtendedErrorException extends RemoteDfuException
    {
        private static final long serialVersionUID = -6901728550661937942L;
        private final int mError;
        
        public RemoteDfuExtendedErrorException(final String message, final int extendedError) {
            super(message, 11);
            this.mError = extendedError;
        }
        
        public int getExtendedErrorNumber() {
            return this.mError;
        }
        
        @Override
        public String getMessage() {
            return super.getMessage() + " (error 11." + this.mError + ")";
        }
    }
}
