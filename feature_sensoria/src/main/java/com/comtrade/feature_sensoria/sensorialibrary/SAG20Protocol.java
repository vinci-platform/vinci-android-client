/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: SAG20Protocol.java
 * Author: sdelic
 *
 * History:  15.3.22. 10:01 created
 */

//
// Decompiled by Procyon v0.5.36
// 

package com.comtrade.feature_sensoria.sensorialibrary;

import android.util.Log;

class SAG20Protocol extends SASensoriaStreamingProtocols
{
    SAG20Protocol(final DataPointEvent event, final short samplingPeriod) {
        super(event, samplingPeriod);
        this.TAG = SAG20Protocol.class.getSimpleName();
    }
    
    @Override
    void analyze(final byte[] data) {
        this.dataPoint = new SADataPoint();
        this.setSamplingFrequency(data);
        this.dataPoint.channels[0] = (short)(0x3FF & ((data[5] & 0xFF) << 2 | (data[6] & 0xFF) >>> 6));
        this.dataPoint.channels[1] = (short)(0x3FF & ((data[6] & 0xFF) << 4 | (data[7] & 0xFF) >>> 4));
        this.dataPoint.channels[2] = (short)(0x3FF & ((data[7] & 0xFF) << 6 | (data[8] & 0xFF) >>> 2));
        this.dataPoint.channels[3] = (short)(0x3FF & ((data[8] & 0xFF) << 8 | (data[9] & 0xFF)));
        this.dataPoint.channels[4] = (short)(0x3FF & ((data[10] & 0xFF) << 2 | (data[11] & 0xFF) >>> 6));
        this.dataPoint.channels[5] = (short)(0x3FF & ((data[11] & 0xFF) << 4 | (data[12] & 0xFF) >>> 4));
        this.dataPoint.channels[6] = (short)(0x3FF & ((data[0] & 0xFF) << 6 | (data[1] & 0xC0) >>> 2 | (data[4] & 0xF)));
        this.dataPoint.channels[7] = 0;
        this.gRangeIndex = this.getAccelerometerRangeIndex(data);
        this.dataPoint.accelerometer[0] = this.accelToFloat(this.gRangeIndex, (short)(0x3FF & ((data[12] & 0xFF) << 6 | (data[13] & 0xFF) >>> 2)));
        this.dataPoint.accelerometer[1] = this.accelToFloat(this.gRangeIndex, (short)(0x3FF & ((data[13] & 0xFF) << 8 | (data[14] & 0xFF))));
        this.dataPoint.accelerometer[2] = this.accelToFloat(this.gRangeIndex, (short)(0x3FF & ((data[15] & 0xFF) << 2 | (data[16] & 0xFF) >>> 6)));
        this.dpsRangeIndex = this.getGyroscopeRangeIndex(data);
        this.dataPoint.gyroscope[0] = this.gyroToFloat(this.dpsRangeIndex, (short)(0x3FF & ((data[16] & 0xFF) << 4 | (data[17] & 0xFF) >>> 4)));
        this.dataPoint.gyroscope[1] = this.gyroToFloat(this.dpsRangeIndex, (short)(0x3FF & ((data[17] & 0xFF) << 6 | (data[18] & 0xFF) >>> 2)));
        this.dataPoint.gyroscope[2] = this.gyroToFloat(this.dpsRangeIndex, (short)(0x3FF & ((data[18] & 0xFF) << 8 | (data[19] & 0xFF))));
        this.dataPoint.magnetometer[0] = 0.0f;
        this.dataPoint.magnetometer[1] = 0.0f;
        this.dataPoint.magnetometer[2] = 0.0f;
        this.dataPoint.tick = ((data[2] & 0xFF) | (data[3] & 0xFF) << 8 | (data[4] & 0xF0) << 12);
        this.setTicketLost();
        this.didUpdateData();
    }
    
    @Override
    void setCommandLog(final String logString) {
        Log.d(this.TAG, logString);
    }
    
    @Override
    protected boolean isMyCharacteristic(final SensoriaGattCharacteristic characteristic) {
        return (characteristic.getValue()[0] >>> 4 & 0xF) == 0x1 && super.isMyCharacteristic(characteristic);
    }
}
