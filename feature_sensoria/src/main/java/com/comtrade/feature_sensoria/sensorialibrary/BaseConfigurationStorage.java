/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: BaseConfigurationStorage.java
 * Author: sdelic
 *
 * History:  21.2.22. 10:43 created
 */

//
// Decompiled by Procyon v0.5.36
// 

package com.comtrade.feature_sensoria.sensorialibrary;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.CRC32;

public class BaseConfigurationStorage implements Parcelable
{
    protected final String iTAG;
    private String serialNumber;
    public byte HeaderVersion;
    public StorageTypes storageType;
    public BodyLocation bodyLocation;
    public static final Parcelable.Creator CREATOR;

    public String getSerialNumber() {
        return this.serialNumber;
    }

    public void setSerialNumber(final String value) throws SAException.InvalidSerialNumberException {
        if (value == null || value.isEmpty() || value.length() > 24) {
            throw new SAException.InvalidSerialNumberException(value);
        }
        final Pattern pattern = Pattern.compile("^[\\u0020-\\u007E]*$");
        final Matcher matcher = pattern.matcher(value);
        if (!matcher.find()) {
            throw new SAException.InvalidSerialNumberException(String.format(Locale.US, "Serial Number contains not supported characters: %s", value));
        }
        this.serialNumber = value;
    }

    public BaseConfigurationStorage() {
        this.iTAG = BaseConfigurationStorage.class.getSimpleName();
        this.HeaderVersion = 1;
        this.storageType = StorageTypes.BaseConfigurationStorage;
        this.serialNumber = "000000000000000000000000";
        this.bodyLocation = BodyLocation.NotOnBody;
    }

    void Deserialize(final byte[] payload) throws SAException.InvalidGarmentTypeException, SAException.InvalidBodyLocationException, SAException.InvalidSensorTypeException {
        this.bodyLocation = BodyLocation.get(new UInt32(payload, 4).intValue());
        if (this.bodyLocation == null) {
            throw new SAException.InvalidBodyLocationException(new UInt32(payload, 4).intValue());
        }
        final StringBuilder serialBuilder = new StringBuilder();
        for (int i = 0; i < 24 && payload[8 + i] != 0; ++i) {
            serialBuilder.append((char)payload[8 + i]);
        }
        this.serialNumber = serialBuilder.toString();
    }

    public byte[] Serialize() {
        final byte[] payload = new byte[256];
        this.FillHeaderForSerialization(payload);
        this.FillDataForSerialization(payload);
        this.FillCrc32ForSerialization(payload);
        return payload;
    }

    protected void FillHeaderForSerialization(final byte[] payload) {
        payload[0] = this.HeaderVersion;
        Log.i(this.iTAG, SAService.ToHexString(payload));
        payload[1] = (byte)this.storageType.getNumVal();
        Log.i(this.iTAG, SAService.ToHexString(payload));
        final ByteBuffer tmpPayload = ByteBuffer.allocate(4).order(ByteOrder.BIG_ENDIAN);
        tmpPayload.putInt(0, this.bodyLocation.getNumVal());
        System.arraycopy(tmpPayload.array(), 0, payload, 4, 4);
        Log.i(this.iTAG, SAService.ToHexString(payload));
        int counter = 0;
        for (final byte c : this.serialNumber.getBytes()) {
            payload[8 + counter++] = c;
        }
        Log.i(this.iTAG, SAService.ToHexString(payload));
    }

    protected void FillDataForSerialization(final byte[] payload) {
    }

    protected void FillCrc32ForSerialization(final byte[] payload) {
        final CRC32 Crc32 = new CRC32();
        Crc32.update(payload, 0, 252);
        final ByteBuffer tmpPayload = ByteBuffer.allocate(8).order(ByteOrder.BIG_ENDIAN);
        tmpPayload.putLong(Crc32.getValue());
        System.arraycopy(tmpPayload.array(), 4, payload, 252, 4);
        Log.i(this.iTAG, SAService.ToHexString(payload));
    }

    public BaseConfigurationStorage(final Parcel in) {
        this.iTAG = BaseConfigurationStorage.class.getSimpleName();
        this.HeaderVersion = 1;
        this.serialNumber = in.readString();
        this.HeaderVersion = in.readByte();
        int enumTmp = in.readInt();
        this.storageType = StorageTypes.get(enumTmp);
        enumTmp = in.readInt();
        this.bodyLocation = BodyLocation.get(enumTmp);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(final Parcel dest, final int flags) {
        dest.writeString(this.serialNumber);
        dest.writeByte(this.HeaderVersion);
        int enumTmp = this.storageType.getNumVal();
        dest.writeInt(enumTmp);
        enumTmp = this.bodyLocation.getNumVal();
        dest.writeInt(enumTmp);
    }

    static {
        CREATOR = (Parcelable.Creator)new Parcelable.Creator() {
            public BaseConfigurationStorage createFromParcel(final Parcel in) {
                return new BaseConfigurationStorage(in);
            }
            
            public BaseConfigurationStorage[] newArray(final int size) {
                return new BaseConfigurationStorage[size];
            }
        };
    }
}
