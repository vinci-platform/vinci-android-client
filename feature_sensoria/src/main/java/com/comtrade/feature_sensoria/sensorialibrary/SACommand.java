/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: SACommand.java
 * Author: sdelic
 *
 * History:  21.2.22. 10:43 created
 */

//
// Decompiled by Procyon v0.5.36
// 

package com.comtrade.feature_sensoria.sensorialibrary;

import android.bluetooth.BluetoothGattCharacteristic;
import android.util.Log;
import android.util.SparseArray;

import java.nio.ByteBuffer;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;

abstract class SACommand
{
    private volatile SAErrors error;
    private final String iTAG;
    private Integer characteristic;
    protected boolean terminated;
    private static final SparseArray<SAErrors> GattToSAErrors;
    private ByteBuffer commandBuffer;
    private BlockingQueue<QueueableComplexObject> queue;
    protected Object value;
    
    private SACommand() {
        this.error = SAErrors.ERROR_SUCCESS;
        this.iTAG = SACommand.class.getSimpleName();
        this.terminated = false;
        this.queue = new LinkedBlockingDeque<QueueableComplexObject>();
    }
    
    SACommand(final Object value) {
        this.error = SAErrors.ERROR_SUCCESS;
        this.iTAG = SACommand.class.getSimpleName();
        this.terminated = false;
        this.queue = new LinkedBlockingDeque<QueueableComplexObject>();
    }
    
    SACommand(final Integer characteristic, final Object value) {
        this.error = SAErrors.ERROR_SUCCESS;
        this.iTAG = SACommand.class.getSimpleName();
        this.terminated = false;
        this.queue = new LinkedBlockingDeque<QueueableComplexObject>();
        this.characteristic = characteristic;
        this.value = value;
    }
    
    SACommand(final Integer characteristic) {
        this(characteristic, null);
    }
    
    SACommand(final Command command) {
        this.error = SAErrors.ERROR_SUCCESS;
        this.iTAG = SACommand.class.getSimpleName();
        this.terminated = false;
        this.queue = new LinkedBlockingDeque<QueueableComplexObject>();
        this.BuildCommand(command, null);
    }
    
    SACommand(final Command command, final byte[] payload) {
        this.error = SAErrors.ERROR_SUCCESS;
        this.iTAG = SACommand.class.getSimpleName();
        this.terminated = false;
        this.queue = new LinkedBlockingDeque<QueueableComplexObject>();
        this.BuildCommand(command, payload);
    }
    
    SACommand(final Command command, final byte offset, final byte chunkLength) {
        this.error = SAErrors.ERROR_SUCCESS;
        this.iTAG = SACommand.class.getSimpleName();
        this.terminated = false;
        this.queue = new LinkedBlockingDeque<QueueableComplexObject>();
        final byte[] payload = { offset, chunkLength };
        this.BuildCommand(command, payload);
    }
    
    SACommand(final Command command, final byte function) {
        this.error = SAErrors.ERROR_SUCCESS;
        this.iTAG = SACommand.class.getSimpleName();
        this.terminated = false;
        this.queue = new LinkedBlockingDeque<QueueableComplexObject>();
        final byte[] payload = { function };
        this.BuildCommand(command, payload);
    }
    
    SACommand(final Command command, final byte function, final byte[] chunk) {
        this.error = SAErrors.ERROR_SUCCESS;
        this.iTAG = SACommand.class.getSimpleName();
        this.terminated = false;
        this.queue = new LinkedBlockingDeque<QueueableComplexObject>();
        final byte[] payload = new byte[chunk.length + 1];
        payload[0] = function;
        System.arraycopy(chunk, 0, payload, 1, chunk.length);
        this.BuildCommand(command, payload);
    }
    
    SACommand(final Command command, final byte offset, final byte chunklength, final byte[] chunk) {
        this.error = SAErrors.ERROR_SUCCESS;
        this.iTAG = SACommand.class.getSimpleName();
        this.terminated = false;
        this.queue = new LinkedBlockingDeque<QueueableComplexObject>();
        final byte[] payload = new byte[18];
        payload[0] = offset;
        payload[1] = chunklength;
        System.arraycopy(chunk, 0, payload, 2, chunk.length);
        Log.i(this.iTAG, SAService.ToHexString(payload));
        this.BuildCommand(command, payload);
    }
    
    String getHex() {
        return null;
    }
    
    void setValue(final BluetoothGattCharacteristic characteristic) {
    }
    
    void setError(final SAErrors error) {
        this.error = error;
    }
    
    SAErrors getLastError() {
        final SAErrors err = this.error;
        this.error = SAErrors.ERROR_SUCCESS;
        return err;
    }
    
    boolean isNack(final SensoriaGattCharacteristic characteristic) {
        final byte s = characteristic.getValue()[2];
        try {
            if (s == 21) {
                final byte e = characteristic.getValue()[3];
                switch (e) {
                    case -15: {
                        this.setError(SAErrors.ERROR_UNKNOWN_BLOCK);
                        break;
                    }
                    case -14: {
                        this.setError(SAErrors.ERROR_UNKNOWN_PROPERTY);
                        break;
                    }
                    case -13: {
                        this.setError(SAErrors.ERROR_UNKNOWN_FUNCTION);
                        break;
                    }
                    case -2: {
                        this.setError(SAErrors.ERROR_FEATURE_NOT_IMPLEMENTED);
                        break;
                    }
                    default: {
                        this.setError(SAErrors.ERROR_FAILURE);
                        break;
                    }
                }
                return true;
            }
        }
        catch (Exception e2) {
            this.setError(SAErrors.ERROR_FAILURE);
        }
        return false;
    }
    
    protected boolean isInternalError() {
        return this.error != SAErrors.ERROR_SUCCESS;
    }
    
    protected boolean isError(final SensoriaGattCharacteristic characteristic) {
        final byte b = characteristic.getValue()[2];
        return b == 9 || this.error != SAErrors.ERROR_SUCCESS;
    }
    
    SensoriaGattCharacteristic waitForCharacteristic() {
        return this.waitForCharacteristic(true);
    }
    
    SensoriaGattCharacteristic waitForCharacteristic(final boolean hasWrite) {
        SensoriaGattCharacteristic s = null;
        SAErrors internalError = SAErrors.ERROR_SUCCESS;
        Label_0105: {
            if (hasWrite) {
                try {
                    QueueableComplexObject w;
                    do {
                        w = this.queue.poll(30L, TimeUnit.SECONDS).asComplexObject();
                        if (w != null) {
                            if (!(w instanceof StopObject)) {
                                s = (SensoriaGattCharacteristic)w;
                                continue;
                            }
                            internalError = SAErrors.ERROR_STOP;
                        }
                        else {
                            internalError = SAErrors.ERROR_TIMEOUT;
                        }
                        break Label_0105;
                    } while (!this.isMyCharacteristic((SensoriaGattCharacteristic)w));
                    internalError = this.convertGattStatusToSAErrors(s.getStatus());
                }
                catch (InterruptedException e) {
                    this.setError(SAErrors.ERROR_INTERRUPTED_EXCEPTION);
                    return null;
                }
            }
        }
        Label_0199: {
            if (internalError == SAErrors.ERROR_SUCCESS) {
                try {
                    do {
                        final QueueableComplexObject w = this.queue.poll(30L, TimeUnit.SECONDS);
                        if (w != null) {
                            if (!(w instanceof StopObject)) {
                                s = (SensoriaGattCharacteristic)w;
                                continue;
                            }
                            internalError = SAErrors.ERROR_STOP;
                        }
                        else {
                            internalError = SAErrors.ERROR_TIMEOUT;
                        }
                        break Label_0199;
                    } while (!this.isMyCharacteristic(s));
                    internalError = this.convertGattStatusToSAErrors(s.getStatus());
                }
                catch (InterruptedException e) {
                    this.setError(SAErrors.ERROR_INTERRUPTED_EXCEPTION);
                    return null;
                }
            }
        }
        if (internalError != SAErrors.ERROR_SUCCESS) {
            this.setError(internalError);
            return null;
        }
        return s;
    }
    
    protected abstract boolean isMyCharacteristic(final SensoriaGattCharacteristic p0);
    
    private SAErrors convertGattStatusToSAErrors(final int status) {
        return (SAErrors)SACommand.GattToSAErrors.get(status, SAErrors.ERROR_FAILURE);
    }
    
    private void BuildCommand(final Command command, final byte[] payload) {
        (this.commandBuffer = ByteBuffer.allocate(20)).put(new UInt16(command.getNumVal()).GetCommand(), 0, 2);
        final int payloadLength = (payload == null) ? 0 : Math.min(payload.length, 18);
        if (payloadLength > 0) {
            this.commandBuffer.position(2);
            this.commandBuffer.put(payload, 0, payloadLength);
        }
        Log.i(this.iTAG, SAService.ToHexString(this.commandBuffer.array()));
    }
    
    byte[] getCommand() {
        return this.commandBuffer.array();
    }
    
    short getCharacteristic() {
        return this.characteristic.shortValue();
    }
    
    void setCharacteristic(final QueueableComplexObject characteristic) {
        try {
            this.queue.put(characteristic);
        }
        catch (InterruptedException e) {
            this.setError(SAErrors.ERROR_INTERRUPTED_EXCEPTION);
        }
        catch (NullPointerException e2) {
            this.setError(SAErrors.ERROR_NULL_POINTER_CHARACTERISTIC);
        }
    }
    
    void stopRead() {
        final StopObject sgc = new StopObject();
        this.setCharacteristic(sgc);
        while (!this.terminated) {
            try {
                Thread.sleep(1L);
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    
    void stopped() {
        this.terminated = true;
    }
    
    static {
        (GattToSAErrors = new SparseArray<SAErrors>()).put(0, SAErrors.ERROR_SUCCESS);
        SACommand.GattToSAErrors.put(257, SAErrors.ERROR_FAILURE);
        SACommand.GattToSAErrors.put(143, SAErrors.ERROR_CONNECTION_CONGESTED);
        SACommand.GattToSAErrors.put(5, SAErrors.ERROR_INSUFFICIENT_AUTHENTICATION);
        SACommand.GattToSAErrors.put(15, SAErrors.ERROR_INSUFFICIENT_ENCRYPTION);
        SACommand.GattToSAErrors.put(13, SAErrors.ERROR_INVALID_ATTRIBUTE_LENGTH);
        SACommand.GattToSAErrors.put(7, SAErrors.ERROR_INVALID_OFFSET);
        SACommand.GattToSAErrors.put(2, SAErrors.ERROR_READ_NOT_PERMITTED);
        SACommand.GattToSAErrors.put(6, SAErrors.ERROR_REQUEST_NOT_SUPPORTED);
        SACommand.GattToSAErrors.put(3, SAErrors.ERROR_WRITE_NOT_PERMITTED);
    }
    
    enum Command
    {
        ReadProtocolVersion(257), 
        ReadSystemStatus(258), 
        LoadSystemWideDefaultSettings(259), 
        PrepareSystemToEnterDFUMode(260), 
        ResetPowerCycleSystem(261), 
        SetAdvertisingSleepTimeout(262), 
        AutoWakeUpMode(263), 
        DisableLed(513), 
        EnableLed(514), 
        DriveLed(515), 
        Accelerometer(769), 
        Gyroscope(770), 
        Magnetometer(771), 
        ADC(772), 
        Channel0(1025), 
        Channel1(1026), 
        Channel2(1027), 
        Events(1028), 
        ReadData(1281), 
        WriteData(1282), 
        DetectEEPROM12C(1283), 
        ReleaseEEPROM12C(1284), 
        HapticDriverInitialize(1537), 
        HapticDriverDetectI2C(1538), 
        HapticDriverReleaseI2C(1539), 
        HapticDriverTurnMotorOn(1540), 
        HapticDriverTurnMotorOff(1541), 
        DeviceName(10752), 
        Appearance(10753), 
        PeriferalPreferredConnectionParameters(10756), 
        CentralAddressResolutionSupport(10918), 
        ResolvablePrivateAddressOnly(10953), 
        GeneralStreamingProtocol(3), 
        BatteryLevel(10777);
        
        private int numVal;
        
        private Command(final int numVal) {
            this.numVal = numVal;
        }
        
        public int getNumVal() {
            return this.numVal;
        }
    }
}
