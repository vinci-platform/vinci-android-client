/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: SABalanceStreamingService.java
 * Author: sdelic
 *
 * History:  21.2.22. 10:43 created
 */

//
// Decompiled by Procyon v0.5.36
// 

package com.comtrade.feature_sensoria.sensorialibrary;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.util.Log;

import java.io.Serializable;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import java.util.concurrent.BlockingQueue;

public class SABalanceStreamingService extends SAStreamingService implements Serializable
{
    private final UUID UUID_BALANCE_STREAMING_SERVICE;
    private final UUID UUID_BALANCE_STREAMING_CHARACTERISTIC;
    private final UUID UUID_BALANCE_CONTROL_POINT_CHARACTERISTIC;
    private BluetoothGattCharacteristic mCharacteristicRX;
    private BluetoothGattCharacteristic mCharacteristicTX;
    private SASensoriaStreamingProtocols command;
    private boolean resume;
    private Thread receiveThread;
    private MyWorkerThread mWorkerThread;
    
    @Override
    protected void runOnUiThread(final Runnable runnable) {
        this.mHandler.post(runnable);
    }
    
    private SABalanceStreamingService() {
        this.UUID_BALANCE_STREAMING_SERVICE = UUID.fromString("6e400001-b5a3-f393-e0a9-e50e24dcca9e");
        this.UUID_BALANCE_STREAMING_CHARACTERISTIC = UUID.fromString("6e400003-b5a3-f393-e0a9-e50e24dcca9e");
        this.UUID_BALANCE_CONTROL_POINT_CHARACTERISTIC = UUID.fromString("6e400002-b5a3-f393-e0a9-e50e24dcca9e");
        this.mCharacteristicRX = null;
        this.mCharacteristicTX = null;
        this.resume = true;
        this.receiveThread = null;
    }
    
    public SABalanceStreamingService(final String deviceMac) {
        super(deviceMac);
        this.UUID_BALANCE_STREAMING_SERVICE = UUID.fromString("6e400001-b5a3-f393-e0a9-e50e24dcca9e");
        this.UUID_BALANCE_STREAMING_CHARACTERISTIC = UUID.fromString("6e400003-b5a3-f393-e0a9-e50e24dcca9e");
        this.UUID_BALANCE_CONTROL_POINT_CHARACTERISTIC = UUID.fromString("6e400002-b5a3-f393-e0a9-e50e24dcca9e");
        this.mCharacteristicRX = null;
        this.mCharacteristicTX = null;
        this.resume = true;
        this.receiveThread = null;
        Log.d(this.Tag = SABalanceStreamingService.class.getSimpleName(), "ctor");
        this.mServiceName = "Balance Streaming ServiceTypes";
        this.uuidService = this.UUID_BALANCE_STREAMING_SERVICE;
    }
    
    SABalanceStreamingService(final String deviceMac, final BlockingQueue<SABundleObject> saBundle) {
        super(deviceMac, saBundle);
        this.UUID_BALANCE_STREAMING_SERVICE = UUID.fromString("6e400001-b5a3-f393-e0a9-e50e24dcca9e");
        this.UUID_BALANCE_STREAMING_CHARACTERISTIC = UUID.fromString("6e400003-b5a3-f393-e0a9-e50e24dcca9e");
        this.UUID_BALANCE_CONTROL_POINT_CHARACTERISTIC = UUID.fromString("6e400002-b5a3-f393-e0a9-e50e24dcca9e");
        this.mCharacteristicRX = null;
        this.mCharacteristicTX = null;
        this.resume = true;
        this.receiveThread = null;
        Log.d(this.Tag = SABalanceStreamingService.class.getSimpleName(), "ctor");
        this.mServiceName = "Balance Streaming ServiceTypes";
        this.uuidService = this.UUID_BALANCE_STREAMING_SERVICE;
    }
    
    @Override
    ServiceTypes getType() {
        return ServiceTypes.BALANCE_STREAMING_SERVICE;
    }
    
    @Override
    public void start(final SAServiceInterface delegate) throws InterruptedException {
        Log.d(this.Tag, "SABalanceStreamingService:start");
        this.mHandler = new Handler(Looper.getMainLooper());
        try {
            super.start(delegate);
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    
    @Override
    protected void setOptionalWriter(final boolean force) {
    }
    
    @Override
    protected void deleteOptionalWriter() {
    }
    
    @Override
    public void resume() throws InterruptedException {
        Log.d(this.Tag, "resume");
        if (this.resume) {
            if (this.mServiceStatus.compare(1) || this.mServiceStatus.compare(3)) {
                super.resume();
                if (this.receiveThread == null) {
                    this.startReceiving();
                }
            }
            else if (this.mIServiceDelegate != null) {
                this.mIServiceDelegate.didServiceResume(this.mDevice, this.getType());
            }
        }
    }
    
    private void startReceiving() {
        Log.d(this.Tag, "startReceiving");
        (this.receiveThread = new Thread(new Runnable() {
            @Override
            public void run() {
                if (SABalanceStreamingService.this.mWorkerThread != null) {
                    SABalanceStreamingService.this.mWorkerThread.quit();
                    SABalanceStreamingService.this.mWorkerThread = null;
                }
                while (!SABalanceStreamingService.this.mServiceReady) {
                    try {
                        Thread.sleep(10L);
                    }
                    catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                SABalanceStreamingService.this.command.readDataPoint();
            }
        })).start();
    }
    
    @Override
    public void pause() {
        Log.d(this.Tag, "pause");
        super.pause();
        this.receiveThread = null;
    }
    
    private void receiveDataPoint(final SADataPoint dataPoint, final SAErrors error, final Object more) {
        if (error == SAErrors.ERROR_SUCCESS) {
            this.mDataLogger.logDataPoint(dataPoint);
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (SABalanceStreamingService.this.mIServiceDelegate != null) {
                        ((SAServiceStreamingServiceInterface)SABalanceStreamingService.this.mIServiceDelegate).didUpdateData(SABalanceStreamingService.this.mDevice, SABalanceStreamingService.this.getType(), dataPoint);
                    }
                }
            });
        }
        else if (error == SAErrors.ERROR_UNEXPECTED_PROTOCOL_LENGTH) {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (SABalanceStreamingService.this.mIServiceDelegate != null) {
                        SABalanceStreamingService.this.mIServiceDelegate.didServiceError(SABalanceStreamingService.this.mDevice, SABalanceStreamingService.this.getType(), SABalanceStreamingService.this.mServiceName, "receiveDataPoint", error, String.format(Locale.US, "Expected length=20 received length=%d", (Integer)more));
                    }
                }
            });
        }
        else {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (SABalanceStreamingService.this.mIServiceDelegate != null) {
                        SABalanceStreamingService.this.mIServiceDelegate.didServiceError(SABalanceStreamingService.this.mDevice, SABalanceStreamingService.this.getType(), SABalanceStreamingService.this.mServiceName, "receiveDatapoint", error);
                    }
                }
            });
        }
    }
    
    @Override
    public void stop() {
        Log.d(this.Tag, "stop");
        this.removeListener(this.command);
        super.stop();
    }
    
    @Override
    public void dispose() {
    }
    
    private boolean initializeCommand(final short samplingPeriod) {
        final boolean retCode = true;
        this.addListener(this.command = new SAH20BalanceProtocol(new SASensoriaStreamingProtocols.DataPointEvent() {
            @Override
            public void onReceiveDataPoint(final SADataPoint datapoint) {
                SABalanceStreamingService.this.receiveDataPoint(datapoint, SAErrors.ERROR_SUCCESS, null);
            }
            
            @Override
            public void onReceiveDataPoint(final SADataPoint datapoint, final SAErrors error) {
                SABalanceStreamingService.this.receiveDataPoint(datapoint, error, null);
            }
            
            @Override
            public void onReceiveDataPoint(final SADataPoint datapoint, final SAErrors error, final Object moreInformation) {
                SABalanceStreamingService.this.receiveDataPoint(datapoint, error, moreInformation);
            }
        }, samplingPeriod));
        return retCode;
    }
    
    @Override
    BluetoothGattCallback getGattCallBack() {
        return new BluetoothGattCallback() {
            private String connectionState(final int status) {
                switch (status) {
                    case 2: {
                        return "Connected";
                    }
                    case 0: {
                        return "Disconnected";
                    }
                    case 1: {
                        return "Connecting";
                    }
                    case 3: {
                        return "Disconnecting";
                    }
                    default: {
                        return String.valueOf(status);
                    }
                }
            }
            
            private boolean setCharacteristicNotification(final BluetoothGattCharacteristic characteristic, final boolean enabled) throws InterruptedException {
                Log.d(SABalanceStreamingService.this.Tag, "SASensoriaBalanceService:setCharacteristicNotification");
                if (SABalanceStreamingService.this.mConnectedGatt == null) {
                    if (SABalanceStreamingService.this.mIBundle != null) {
                        SABalanceStreamingService.this.mIBundle.onServiceError(SABalanceStreamingService.this.mDevice, SABalanceStreamingService.this.getType(), SAErrors.ERROR_GATT_NOT_INITIALIZED);
                    }
                    SABalanceStreamingService.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (SABalanceStreamingService.this.mIServiceDelegate != null) {
                                SABalanceStreamingService.this.mIServiceDelegate.didServiceError(SABalanceStreamingService.this.mDevice, SABalanceStreamingService.this.getType(), SABalanceStreamingService.this.mServiceName, "setCharacteristicNotification", SAErrors.ERROR_GATT_NOT_INITIALIZED);
                            }
                        }
                    });
                    return false;
                }
                SABalanceStreamingService.this.mConnectedGatt.setCharacteristicNotification(characteristic, enabled);
                final byte[] enableNotification = enabled ? BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE : BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE;
                final UUID uuidCharacteristic = characteristic.getUuid();
                Log.d(SABalanceStreamingService.this.Tag, "setCharacteristicNotification: UUID: " + uuidCharacteristic.toString());
                final List<BluetoothGattDescriptor> bluetoothGattDescriptors = (List<BluetoothGattDescriptor>)characteristic.getDescriptors();
                if (bluetoothGattDescriptors == null || bluetoothGattDescriptors.size() == 0) {
                    return false;
                }
                final BluetoothGattDescriptor descriptor = bluetoothGattDescriptors.get(0);
                descriptor.setValue(enableNotification);
                SABalanceStreamingService.this.mConnectedGatt.writeDescriptor(descriptor);
                return true;
            }
            
            public void onConnectionStateChange(final BluetoothGatt gatt, final int status, final int newState) {
                Log.d(SABalanceStreamingService.this.Tag, "SABalanceStreamingService:onConnectionStateChange: " + status + " -> " + this.connectionState(newState) + " API = " + Build.VERSION.SDK_INT);
                if (status == 0 && newState == 2) {
                    if (SABalanceStreamingService.this.mServiceStatus.compare(1) || SABalanceStreamingService.this.mServiceStatus.compare(3)) {
                        Log.d(SABalanceStreamingService.this.Tag, "Before Gatt discovery services");
                        gatt.discoverServices();
                    }
                }
                else if (status == 0 && newState == 0) {
                    if (Build.VERSION.SDK_INT >= 21 || SABalanceStreamingService.this.mServiceStatus.compare(3)) {
                        SABalanceStreamingService.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (SABalanceStreamingService.this.mIBundle != null) {
                                    try {
                                        SABalanceStreamingService.this.mIBundle.onServiceStopped(SABalanceStreamingService.this.mDevice, SABalanceStreamingService.this.getType(), SABalanceStreamingService.this.mHandler);
                                    }
                                    catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        });
                        if (SABalanceStreamingService.this.mServiceStatus.compare(3)) {
                            SABalanceStreamingService.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (SABalanceStreamingService.this.mIServiceDelegate != null) {
                                        SABalanceStreamingService.this.mIServiceDelegate.didServicePause(SABalanceStreamingService.this.mDevice, SABalanceStreamingService.this.getType());
                                    }
                                }
                            });
                        }
                    }
                    else {
                        SABalanceStreamingService.this.mServiceStatus.set(1);
                        Log.d(SABalanceStreamingService.this.Tag, "Try to reconnect from error:" + status);
                        SABalanceStreamingService.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                SABalanceStreamingService.this.pause();
                                SABalanceStreamingService.this.stop();
                                SABalanceStreamingService.this.mServiceStatus.set(0);
                                try {
                                    SABalanceStreamingService.this.start(SABalanceStreamingService.this.mIServiceDelegate);
                                }
                                catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    SABalanceStreamingService.this.resume();
                                }
                                catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                    if (SABalanceStreamingService.this.mCountDownLatch != null) {
                        SABalanceStreamingService.this.mCountDownLatch.countDown();
                    }
                }
                else if ((status == 8 || status == 133 || status == 34 || status == 22) && newState == 0) {
                    SABalanceStreamingService.this.mServiceStatus.set(1);
                    Log.d(SABalanceStreamingService.this.Tag, "Try to reconnect from error:" + status);
                    SABalanceStreamingService.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            SABalanceStreamingService.this.mConnectedGatt.close();
                            try {
                                SABalanceStreamingService.this.resume();
                            }
                            catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
                else if (status == 133 && newState == 2 && Build.VERSION.SDK_INT < 21) {
                    SABalanceStreamingService.this.mServiceStatus.set(1);
                    Log.d(SABalanceStreamingService.this.Tag, "Try to reconnect from error =" + status + " new status = " + newState + " API = " + Build.VERSION.SDK_INT);
                    SABalanceStreamingService.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            SABalanceStreamingService.this.mConnectedGatt.close();
                            try {
                                SABalanceStreamingService.this.resume();
                            }
                            catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
                else if (status != 0) {
                    try {
                        throw new SAException.UnmanagedConnectionException(status, newState);
                    }
                    catch (SAException.UnmanagedConnectionException e) {
                        e.printStackTrace();
                    }
                }
            }
            
            public void onServicesDiscovered(final BluetoothGatt gatt, final int status) {
                Log.d(SABalanceStreamingService.this.Tag, "SASensoriaStreamingService:onServicesDiscovered");
                if (status == 0) {
                    final BluetoothGattService gattService = SABalanceStreamingService.this.mConnectedGatt.getService(SABalanceStreamingService.this.UUID_BALANCE_STREAMING_SERVICE);
                    if (gattService != null) {
                        Log.i(SABalanceStreamingService.this.Tag, "ServiceTypes UUID found: " + gattService.getUuid().toString());
                        final List<BluetoothGattCharacteristic> gattCharacteristics = (List<BluetoothGattCharacteristic>)gattService.getCharacteristics();
                        for (final BluetoothGattCharacteristic gattCharacteristic : gattCharacteristics) {
                            Log.d(SABalanceStreamingService.this.Tag, "Characteristic UUID found: " + gattCharacteristic.getUuid().toString());
                            if (gattCharacteristic.getUuid().compareTo(SABalanceStreamingService.this.UUID_BALANCE_STREAMING_CHARACTERISTIC) == 0) {
                                Log.d(SABalanceStreamingService.this.Tag, "UUID_BALANCE_STREAMING_CHARACTERISTIC found");
                                SABalanceStreamingService.this.mCharacteristicRX = gattCharacteristic;
                            }
                            if (gattCharacteristic.getUuid().compareTo(SABalanceStreamingService.this.UUID_BALANCE_CONTROL_POINT_CHARACTERISTIC) == 0) {
                                Log.d(SABalanceStreamingService.this.Tag, "UUID_BALANCE_CONTROL_POINT_CHARACTERISTIC found");
                                SABalanceStreamingService.this.mCharacteristicTX = gattCharacteristic;
                            }
                        }
                        if (SABalanceStreamingService.this.mCharacteristicRX == null || SABalanceStreamingService.this.mCharacteristicTX == null) {
                            if (SABalanceStreamingService.this.mIBundle != null) {
                                try {
                                    SABalanceStreamingService.this.mIBundle.onServiceError(SABalanceStreamingService.this.mDevice, SABalanceStreamingService.this.getType(), SAErrors.ERROR_CHARACTERISTIC_NOT_FOUND);
                                }
                                catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                            SABalanceStreamingService.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (SABalanceStreamingService.this.mIServiceDelegate != null) {
                                        SABalanceStreamingService.this.mIServiceDelegate.didServiceError(SABalanceStreamingService.this.mDevice, SABalanceStreamingService.this.getType(), SABalanceStreamingService.this.mServiceName, "onServicesDiscovered", SAErrors.ERROR_CHARACTERISTIC_NOT_FOUND);
                                    }
                                }
                            });
                        }
                        else if (SABalanceStreamingService.this.mServiceStatus.compare(1) || SABalanceStreamingService.this.mServiceStatus.compare(3)) {
                            if (SABalanceStreamingService.this.mIBundle != null) {
                                try {
                                    SABalanceStreamingService.this.mIBundle.onServiceStarted(SABalanceStreamingService.this.mDevice, SABalanceStreamingService.this.getType(), SABalanceStreamingService.this.mHandler);
                                }
                                catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                            SABalanceStreamingService.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (SABalanceStreamingService.this.mServiceStatus.compare(1)) {
                                        Log.d(SABalanceStreamingService.this.Tag, "ServiceTypes discovered and connected");
                                        if (SABalanceStreamingService.this.mIServiceDelegate != null) {
                                            SABalanceStreamingService.this.mIServiceDelegate.didServiceConnect(SABalanceStreamingService.this.mDevice, SABalanceStreamingService.this.getType());
                                        }
                                    }
                                    else {
                                        if (SABalanceStreamingService.this.mIServiceDelegate != null) {
                                            SABalanceStreamingService.this.mIServiceDelegate.didServiceResume(SABalanceStreamingService.this.mDevice, SABalanceStreamingService.this.getType());
                                        }
                                        Log.d(SABalanceStreamingService.this.Tag, "ServiceTypes resumed");
                                    }
                                }
                            });
                            SABalanceStreamingService.this.mServiceStatus.set(2);
                        }
                    }
                    else {
                        Log.i(SABalanceStreamingService.this.Tag, "ServiceTypes not found for UUID: " + SABalanceStreamingService.this.UUID_BALANCE_STREAMING_SERVICE.toString());
                        if (SABalanceStreamingService.this.mIBundle != null) {
                            try {
                                SABalanceStreamingService.this.mIBundle.onServiceError(SABalanceStreamingService.this.mDevice, SABalanceStreamingService.this.getType(), SAErrors.ERROR_SERVICE_NOT_FOUND);
                            }
                            catch (InterruptedException e2) {
                                e2.printStackTrace();
                            }
                        }
                        SABalanceStreamingService.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (SABalanceStreamingService.this.mIServiceDelegate != null) {
                                    SABalanceStreamingService.this.mIServiceDelegate.didServiceError(SABalanceStreamingService.this.mDevice, SABalanceStreamingService.this.getType(), SABalanceStreamingService.this.mServiceName, "onServicesDiscovered", SAErrors.ERROR_SERVICE_NOT_FOUND);
                                }
                            }
                        });
                    }
                    if (SABalanceStreamingService.this.mServiceStatus.compare(2)) {
                        if (SABalanceStreamingService.this.firstInitialization) {
                            final Runnable task = new Runnable() {
                                @Override
                                public void run() {
                                    if (!SABalanceStreamingService.this.setAllSignalsOn()) {
                                        SABalanceStreamingService.this.runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                if (SABalanceStreamingService.this.mIServiceDelegate != null) {
                                                    SABalanceStreamingService.this.mIServiceDelegate.didServiceError(SABalanceStreamingService.this.mDevice, SABalanceStreamingService.this.getType(), SABalanceStreamingService.this.mServiceName, "start", SAErrors.ERROR_CANNOT_WRITE_CONTROL_POINT, String.format(Locale.US, "error during initialization: enabling sensors failed", new Object[0]));
                                                }
                                            }
                                        });
                                    }
                                    else {
                                        try {
                                            Thread.sleep(100L);
                                        }
                                        catch (InterruptedException e) {
                                            e.printStackTrace();
                                        }
                                        if (!SABalanceStreamingService.this.setFiftyHertz()) {
                                            SABalanceStreamingService.this.runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    if (SABalanceStreamingService.this.mIServiceDelegate != null) {
                                                        SABalanceStreamingService.this.mIServiceDelegate.didServiceError(SABalanceStreamingService.this.mDevice, SABalanceStreamingService.this.getType(), SABalanceStreamingService.this.mServiceName, "start", SAErrors.ERROR_CANNOT_WRITE_CONTROL_POINT, String.format(Locale.US, "error during initialization: setting sampling period", new Object[0]));
                                                    }
                                                }
                                            });
                                        }
                                        else {
                                            try {
                                                Thread.sleep(100L);
                                            }
                                            catch (InterruptedException e) {
                                                e.printStackTrace();
                                            }
                                            if (!SABalanceStreamingService.this.setH20Protocol()) {
                                                SABalanceStreamingService.this.runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        if (SABalanceStreamingService.this.mIServiceDelegate != null) {
                                                            SABalanceStreamingService.this.mIServiceDelegate.didServiceError(SABalanceStreamingService.this.mDevice, SABalanceStreamingService.this.getType(), SABalanceStreamingService.this.mServiceName, "start", SAErrors.ERROR_CANNOT_WRITE_CONTROL_POINT, String.format(Locale.US, "error during initialization: setting protocol", new Object[0]));
                                                        }
                                                    }
                                                });
                                            }
                                            else if ((SABalanceStreamingService.this.mCharacteristicRX.getProperties() & 0x10) > 0) {
                                               /* try {
                                                    if (SABalanceStreamingService.this.setCharacteristicNotification(SABalanceStreamingService.this.mCharacteristicRX, true)) {
                                                        SABalanceStreamingService.this.initializeCommand((short)20);
                                                        SABalanceStreamingService.this.mThreadStart.start();
                                                    }
                                                }
                                                catch (InterruptedException e) {
                                                    e.printStackTrace();
                                                }*/
                                            }
                                        }
                                    }
                                }
                            };
                            SABalanceStreamingService.this.mWorkerThread = new MyWorkerThread("internalControlPoint");
                            SABalanceStreamingService.this.mWorkerThread.start();
                            SABalanceStreamingService.this.mWorkerThread.prepareHandler();
                            SABalanceStreamingService.this.mWorkerThread.postTask(task);
                        }
                        else if ((SABalanceStreamingService.this.mCharacteristicRX.getProperties() & 0x10) > 0) {
                            try {
                                if (!this.setCharacteristicNotification(SABalanceStreamingService.this.mCharacteristicRX, true)) {
                                    SABalanceStreamingService.this.mIServiceDelegate.didServiceError(SABalanceStreamingService.this.mDevice, SABalanceStreamingService.this.getType(), SABalanceStreamingService.this.mServiceName, "resume", SAErrors.ERROR_FAILURE, String.format(Locale.US, "error during initialization: re-enable notification failed", new Object[0]));
                                }
                            }
                            catch (InterruptedException e2) {
                                e2.printStackTrace();
                            }
                        }
                    }
                }
            }
            
            public void onReadRemoteRssi(final BluetoothGatt gatt, final int rssi, final int status) {
                Log.d(SABalanceStreamingService.this.Tag, "Remote RSSI: " + rssi);
            }
            
            public void onCharacteristicChanged(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {
                Log.i(SABalanceStreamingService.this.Tag, "onCharacteristicChanged: " + ToHexString(characteristic.getValue()));
                SABalanceStreamingService.this.command.setCharacteristic(new SensoriaGattCharacteristic(characteristic));
            }
        };
    }
    
    private boolean setAllSignalsOn() {
        final byte[] command = { 65, 79 };
        return this.WriteControlPoint(command);
    }
    
    private boolean setFiftyHertz() {
        final byte[] command = { 70, 48, 53 };
        return this.WriteControlPoint(command);
    }
    
    private boolean setH20Protocol() {
        final byte[] command = { 80, 50 };
        return this.WriteControlPoint(command);
    }
    
    private boolean sleepMode() {
        final byte[] command = { 112, 115 };
        return this.WriteControlPoint(command);
    }
    
    private boolean wakeUp() {
        final byte[] command = { 112, 119 };
        return this.WriteControlPoint(command);
    }
    
    public boolean vibrateMotor(final int intensity) {
        if (intensity < 0 || intensity > 100) {
            return false;
        }
        final byte[] command = { 115, 0, 0, 0 };
        byte quotient = (byte)intensity;
        for (int i = 3; i > 0; --i) {
            command[i] = (byte)(48 + quotient % 10);
            quotient /= 10;
        }
        return this.WriteControlPoint(command);
    }
    
    private Boolean WriteControlPoint(final byte[] data) {
        Log.d(this.Tag, "SABalanceControlPointService:WriteControlPoint");
        Log.i(this.Tag, "Control Point WRITE: " + ToHexString(data));
        this.mCharacteristicTX.setValue(data);
        if (!this.mConnectedGatt.writeCharacteristic(this.mCharacteristicTX)) {
            return false;
        }
        return true;
    }
    
    private class MyWorkerThread extends HandlerThread
    {
        private Handler mWorkerHandler;
        
        MyWorkerThread(final String name) {
            super(name);
        }
        
        void postTask(final Runnable task) {
            this.mWorkerHandler.post(task);
        }
        
        void prepareHandler() {
            this.mWorkerHandler = new Handler(this.getLooper());
        }
    }
}
