/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: CSBatteryService.java
 * Author: sdelic
 *
 * History:  21.2.22. 10:43 created
 */

//
// Decompiled by Procyon v0.5.36
// 

package com.comtrade.feature_sensoria.sensorialibrary;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.os.Build;
import android.os.Handler;
import android.util.Log;
import android.util.SparseArray;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class CSBatteryService extends SAService implements Serializable
{
    private final UUID UUID_BATTERY_SERVICE;
    private LinkedBlockingQueue<Object> mwcQueue;
    private static SparseArray<BluetoothGattCharacteristic> characteristicMap;
    
    @Override
    protected void runOnUiThread(final Runnable runnable) {
        this.mHandler.post(runnable);
    }
    
    private CSBatteryService() {
        this.UUID_BATTERY_SERVICE = UUID.fromString("0000180f-0000-1000-8000-00805f9b34fb");
        this.mwcQueue = new LinkedBlockingQueue<Object>();
    }
    
    public CSBatteryService(final String deviceMac) {
        super(deviceMac);
        this.UUID_BATTERY_SERVICE = UUID.fromString("0000180f-0000-1000-8000-00805f9b34fb");
        this.mwcQueue = new LinkedBlockingQueue<Object>();
        Log.d(this.Tag, "ctor");
        this.mServiceName = "Battery ServiceTypes";
        CSBatteryService.characteristicMap = new SparseArray<BluetoothGattCharacteristic>();
        this.uuidService = this.UUID_BATTERY_SERVICE;
    }
    
    public CSBatteryService(final String deviceMac, final BlockingQueue<SABundleObject> saBundle) {
        super(deviceMac, saBundle);
        this.UUID_BATTERY_SERVICE = UUID.fromString("0000180f-0000-1000-8000-00805f9b34fb");
        this.mwcQueue = new LinkedBlockingQueue<Object>();
        Log.d(this.Tag = CSBatteryService.class.getSimpleName(), "ctor");
        this.mServiceName = "Battery ServiceTypes";
        CSBatteryService.characteristicMap = new SparseArray<BluetoothGattCharacteristic>();
        this.uuidService = this.UUID_BATTERY_SERVICE;
    }
    
    @Override
    ServiceTypes getType() {
        return ServiceTypes.BATTERY_SERVICE;
    }
    
    public void start(final SAServiceInterface delegate) throws InterruptedException {
        Log.d(this.Tag, "start");
        this.mHandler = new Handler();
        try {
            super.start(delegate);
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    
    @Override
    protected void setOptionalWriter(final boolean force) {
    }
    
    @Override
    protected void deleteOptionalWriter() {
    }
    
    public void resume() throws InterruptedException {
        Log.d(this.Tag, "resume");
        super.resume();
    }
    
    public void pause() {
        Log.d(this.Tag, "pause");
        super.pause();
    }
    
    public void stop() {
        Log.d(this.Tag, "stop");
        super.stop();
    }
    
    public void dispose() {
    }
    
    public int readBatteryLevel() {
        Log.d(this.Tag, "readBatteryLevel");
        int response = -1;
        final CSBatteryLevel command = new CSBatteryLevel();
        if (CSBatteryService.characteristicMap.indexOfKey((int)command.getCharacteristic()) < 0) {
            if (this.mIServiceDelegate != null) {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        CSBatteryService.this.mIServiceDelegate.didServiceError(CSBatteryService.this.mDevice, CSBatteryService.this.getType(), CSBatteryService.this.mServiceName, "readBatteryLevel", SAErrors.ERROR_CHARACTERISTIC_NOT_IMPLEMENTED, String.format("The mandatory characteristic 2A19 of Battery service is not implemented", command.getCharacteristic()));
                    }
                });
            }
            return -1;
        }
        this.addListener(command);
        if (this.ReadCharacteristic(command)) {
            if ((response = command.readBatteryLevel()) == -1) {
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (CSBatteryService.this.mIServiceDelegate != null) {
                            CSBatteryService.this.mIServiceDelegate.didServiceError(CSBatteryService.this.mDevice, CSBatteryService.this.getType(), CSBatteryService.this.mServiceName, "readBatteryLevel", command.getLastError());
                        }
                    }
                });
            }
        }
        else if (this.mIServiceDelegate != null) {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    CSBatteryService.this.mIServiceDelegate.didServiceError(CSBatteryService.this.mDevice, CSBatteryService.this.getType(), CSBatteryService.this.mServiceName, "readBatteryLevel", SAErrors.ERROR_CHARACTERISTIC_NOT_IMPLEMENTED, String.format("Error reading Battery ServiceTypes characteristic 0x2A19", command.getCharacteristic()));
                }
            });
        }
        this.removeListener(command);
        return response;
    }
    
    private boolean ReadCharacteristic(final SACommand command) {
        Log.d(this.Tag, "ReadCharacteristic: " + ToHexString(command.getCharacteristic()));
        return this.mConnectedGatt.readCharacteristic((BluetoothGattCharacteristic)CSBatteryService.characteristicMap.get((int)command.getCharacteristic()));
    }
    
    @Override
    BluetoothGattCallback getGattCallBack() {
        return new BluetoothGattCallback() {
            private String connectionState(final int status) {
                switch (status) {
                    case 2: {
                        return "Connected";
                    }
                    case 0: {
                        return "Disconnected";
                    }
                    case 1: {
                        return "Connecting";
                    }
                    case 3: {
                        return "Disconnecting";
                    }
                    default: {
                        return String.valueOf(status);
                    }
                }
            }
            
            public void onConnectionStateChange(final BluetoothGatt gatt, final int status, final int newState) {
                Log.d(CSBatteryService.this.Tag, "Connection State Change: " + status + " -> " + this.connectionState(newState) + " API = " + Build.VERSION.SDK_INT);
                if (status == 0 && newState == 2) {
                    if (CSBatteryService.this.mServiceStatus.compare(1) || CSBatteryService.this.mServiceStatus.compare(3)) {
                        Log.d(CSBatteryService.this.Tag, "Before Gatt discovery services");
                        gatt.discoverServices();
                    }
                }
                else if (status == 0 && newState == 0) {
                    if (Build.VERSION.SDK_INT >= 21 || CSBatteryService.this.mServiceStatus.compare(3)) {
                        if (CSBatteryService.this.mIBundle != null) {
                            try {
                                CSBatteryService.this.mIBundle.onServiceStopped(CSBatteryService.this.mDevice, ServiceTypes.BATTERY_SERVICE, CSBatteryService.this.mHandler);
                            }
                            catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                        if (CSBatteryService.this.mServiceStatus.compare(3)) {
                            CSBatteryService.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (CSBatteryService.this.mIServiceDelegate != null) {
                                        CSBatteryService.this.mIServiceDelegate.didServicePause(CSBatteryService.this.mDevice, CSBatteryService.this.getType());
                                    }
                                }
                            });
                        }
                    }
                    else {
                        CSBatteryService.this.mServiceStatus.set(1);
                        Log.d(CSBatteryService.this.Tag, "Try to reconnect from error:" + status);
                        CSBatteryService.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                CSBatteryService.this.pause();
                                CSBatteryService.this.stop();
                                CSBatteryService.this.mServiceStatus.set(0);
                                try {
                                    CSBatteryService.this.start(CSBatteryService.this.mIServiceDelegate);
                                }
                                catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    CSBatteryService.this.resume();
                                }
                                catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                    if (CSBatteryService.this.mCountDownLatch != null) {
                        CSBatteryService.this.mCountDownLatch.countDown();
                    }
                }
                else if ((status == 8 || status == 133 || status == 59 || status == 34) && newState == 0) {
                    CSBatteryService.this.mServiceStatus.set(1);
                    Log.d(CSBatteryService.this.Tag, "Try to reconnect from error:" + status);
                    CSBatteryService.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            CSBatteryService.this.mConnectedGatt.close();
                            try {
                                CSBatteryService.this.resume();
                            }
                            catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
                else if (status == 133 && newState == 2 && Build.VERSION.SDK_INT < 21) {
                    CSBatteryService.this.mServiceStatus.set(1);
                    Log.d(CSBatteryService.this.Tag, "Try to reconnect from error =" + status + " new status = " + newState + " API = " + Build.VERSION.SDK_INT);
                    CSBatteryService.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            CSBatteryService.this.mConnectedGatt.close();
                            try {
                                CSBatteryService.this.resume();
                            }
                            catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
                else if (status != 0) {
                    try {
                        throw new SAException.UnmanagedConnectionException(status, newState);
                    }
                    catch (SAException.UnmanagedConnectionException e2) {
                        e2.printStackTrace();
                    }
                }
            }
            
            public void onServicesDiscovered(final BluetoothGatt gatt, final int status) {
                Log.d(CSBatteryService.this.Tag, "onServicesDiscovered");
                if (status == 0) {
                    final BluetoothGattService gattService = CSBatteryService.this.mConnectedGatt.getService(CSBatteryService.this.UUID_BATTERY_SERVICE);
                    if (gattService != null) {
                        Log.i(CSBatteryService.this.Tag, "ServiceTypes characteristic UUID found: " + gattService.getUuid().toString());
                        final List<BluetoothGattCharacteristic> gattCharacteristics = (List<BluetoothGattCharacteristic>)gattService.getCharacteristics();
                        for (final BluetoothGattCharacteristic gattCharacteristic : gattCharacteristics) {
                            final Long gattKey = BluetoothUtils.get16BitBluetoothUUID(gattCharacteristic.getUuid());
                            CSBatteryService.characteristicMap.put(gattKey.intValue(), gattCharacteristic);
                        }
                        if (CSBatteryService.this.mServiceStatus.compare(1) || CSBatteryService.this.mServiceStatus.compare(3)) {
                            if (CSBatteryService.this.mIBundle != null) {
                                try {
                                    CSBatteryService.this.mIBundle.onServiceStarted(CSBatteryService.this.mDevice, ServiceTypes.BATTERY_SERVICE, CSBatteryService.this.mHandler);
                                }
                                catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                            CSBatteryService.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (CSBatteryService.this.mServiceStatus.compare(1)) {
                                        Log.d(CSBatteryService.this.Tag, "ServiceTypes discovered and connected");
                                        if (CSBatteryService.this.mIServiceDelegate != null) {
                                            CSBatteryService.this.mIServiceDelegate.didServiceConnect(CSBatteryService.this.mDevice, CSBatteryService.this.getType());
                                        }
                                    }
                                    else {
                                        if (CSBatteryService.this.mIServiceDelegate != null) {
                                            CSBatteryService.this.mIServiceDelegate.didServiceResume(CSBatteryService.this.mDevice, CSBatteryService.this.getType());
                                        }
                                        Log.d(CSBatteryService.this.Tag, "ServiceTypes resumed");
                                    }
                                }
                            });
                            Log.d(CSBatteryService.this.Tag, "THREAD is started");
                            CSBatteryService.this.mServiceStatus.set(2);
                        }
                    }
                    else {
                        Log.i(CSBatteryService.this.Tag, "ServiceTypes not found for UUID: " + CSBatteryService.this.uuidService.toString());
                        if (CSBatteryService.this.mIBundle != null) {
                            try {
                                CSBatteryService.this.mIBundle.onServiceError(CSBatteryService.this.mDevice, CSBatteryService.this.getType(), SAErrors.ERROR_SERVICE_NOT_FOUND);
                            }
                            catch (InterruptedException e2) {
                                e2.printStackTrace();
                            }
                        }
                        CSBatteryService.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (CSBatteryService.this.mIServiceDelegate != null) {
                                    CSBatteryService.this.mIServiceDelegate.didServiceError(CSBatteryService.this.mDevice, CSBatteryService.this.getType(), CSBatteryService.this.mServiceName, "onServicesDiscovered", SAErrors.ERROR_SERVICE_NOT_FOUND);
                                }
                            }
                        });
                    }
                    if (CSBatteryService.this.firstInitialization && CSBatteryService.this.mServiceStatus.compare(2)) {
                        CSBatteryService.this.mThreadStart.start();
                    }
                }
            }
            
            public void onCharacteristicWrite(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic, final int status) {
                Log.d(CSBatteryService.this.Tag, "onCharacteristicWrite: " + ToHexString(characteristic.getValue()));
                synchronized (CSBatteryService.this.subscribers) {
                    for (final SACommand command : CSBatteryService.this.subscribers) {
                        command.setCharacteristic(new SensoriaGattCharacteristic(characteristic, status));
                    }
                }
            }
            
            public void onCharacteristicRead(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic, final int status) {
                Log.d(CSBatteryService.this.Tag, "onCharacteristicRead:" + ToHexString(characteristic.getValue()));
                synchronized (CSBatteryService.this.subscribers) {
                    for (final SACommand command : CSBatteryService.this.subscribers) {
                        command.setCharacteristic(new SensoriaGattCharacteristic(characteristic, status));
                    }
                }
            }
            
            public void onCharacteristicChanged(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {
                Log.d(CSBatteryService.this.Tag, "onCharacteristicChanged: " + ToHexString(characteristic.getValue()));
                synchronized (CSBatteryService.this.subscribers) {
                    for (final SACommand command : CSBatteryService.this.subscribers) {
                        command.setCharacteristic(new SensoriaGattCharacteristic(characteristic));
                    }
                }
            }
        };
    }
}
