/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: ConfigurationStorageFactory.java
 * Author: sdelic
 *
 * History:  15.3.22. 10:01 created
 */

//
// Decompiled by Procyon v0.5.36
// 

package com.comtrade.feature_sensoria.sensorialibrary;

import java.util.EnumMap;
import java.util.Locale;
import java.util.zip.CRC32;

class ConfigurationStorageFactory
{
    static final ConfigurationStorageFactory Instance;
    private EnumMap<StorageTypes, Class> storageMap;
    
    ConfigurationStorageFactory() {
        (this.storageMap = new EnumMap<StorageTypes, Class>(StorageTypes.class)).put(StorageTypes.Invalid, null);
        this.storageMap.put(StorageTypes.BaseConfigurationStorage, BaseConfigurationStorage.class);
        this.storageMap.put(StorageTypes.GarmentConfigurationStorage, GarmentConfigurationStorage.class);
    }
    
    BaseConfigurationStorage Create(final byte[] payload) throws SAException.InvalidStorageTypeException, SAException.InvalidPayloadException, SAException.UnformattedStorageException, SAException.CrcFailedException, InstantiationException, IllegalAccessException, SAException.InvalidGarmentTypeException, SAException.InvalidBodyLocationException, SAException.InvalidSensorTypeException {
        final StorageTypes storageType = this.GetStorageType(payload);
        Class<? extends BaseConfigurationStorage> type = null;
        switch (storageType) {
            case BaseConfigurationStorage: {
                type = BaseConfigurationStorage.class;
                break;
            }
            case GarmentConfigurationStorage: {
                type = GarmentConfigurationStorage.class;
                break;
            }
            default: {
                throw new SAException.InvalidStorageTypeException(String.format(Locale.US, "Storage type %d found in the payload is not supported", storageType.getNumVal()));
            }
        }
        return this.Create(payload, type);
    }
    
    private StorageTypes GetStorageType(final byte[] payload) throws SAException.InvalidPayloadException, SAException.InvalidStorageTypeException {
        if (payload == null || payload.length < 256) {
            throw new SAException.InvalidPayloadException(payload);
        }
        final StorageTypes storageType = StorageTypes.get(payload[1]);
        if (storageType == null) {
            throw new SAException.InvalidStorageTypeException("Storage Type is not recognized.");
        }
        if (storageType == StorageTypes.Invalid) {
            throw new SAException.InvalidStorageTypeException("Storage Type not allowed: %d", storageType);
        }
        return storageType;
    }
    
    private <T extends BaseConfigurationStorage> T instanceClass(final byte[] payload, final Class<T> type) throws SAException.InvalidStorageTypeException, SAException.InvalidPayloadException, SAException.UnformattedStorageException, SAException.CrcFailedException, IllegalAccessException, InstantiationException, SAException.InvalidGarmentTypeException, SAException.InvalidBodyLocationException, SAException.InvalidSensorTypeException {
        T createD = null;
        this.Validate(payload);
        final StorageTypes storageType = this.GetStorageType(payload);
        createD = type.newInstance();
        final StorageTypes internalStorageType = createD.storageType;
        if (internalStorageType.compareTo(storageType) != 0) {
            throw new SAException.InvalidStorageTypeException(String.format("Requested storage type %s does not match type found in the payload %s", createD.getClass().toString(), storageType.toString()));
        }
        createD.Deserialize(payload);
        return createD;
    }
    
    private BaseConfigurationStorage Create(final byte[] payload, final Class<? extends BaseConfigurationStorage> type) throws SAException.InvalidStorageTypeException, SAException.InvalidPayloadException, SAException.UnformattedStorageException, SAException.CrcFailedException, IllegalAccessException, InstantiationException, SAException.InvalidGarmentTypeException, SAException.InvalidBodyLocationException, SAException.InvalidSensorTypeException {
        return this.instanceClass(payload, type);
    }
    
    private void Validate(final byte[] payload) throws SAException.UnformattedStorageException, SAException.InvalidPayloadException, SAException.CrcFailedException {
        if (payload == null) {
            throw new SAException.InvalidPayloadException("Payload cannot null");
        }
        if (payload.length < 256) {
            throw new SAException.InvalidPayloadException("Payload must be 256 byte long", payload);
        }
        if (payload[0] == 255) {
            throw new SAException.UnformattedStorageException();
        }
        if (payload[0] != 1) {
            throw new SAException.InvalidPayloadException("Invalid header", payload);
        }
        try {
            this.GetStorageType(payload);
        }
        catch (Exception exc) {
            throw new SAException.InvalidPayloadException("Invalid storage type", payload);
        }
        final CRC32 Crc32 = new CRC32();
        Crc32.update(payload, 0, 252);
        final long checkCrc32 = Crc32.getValue();
        final long crc32 = new UInt32(payload, 252).longValue();
        if (checkCrc32 != crc32) {
            throw new SAException.CrcFailedException(payload);
        }
    }
    
    static {
        Instance = new ConfigurationStorageFactory();
    }
}
