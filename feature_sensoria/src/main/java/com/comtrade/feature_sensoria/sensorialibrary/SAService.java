/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: SAService.java
 * Author: sdelic
 *
 * History:  21.2.22. 10:43 created
 */

//
// Decompiled by Procyon v0.5.36
// 

package com.comtrade.feature_sensoria.sensorialibrary;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.util.Log;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;

public abstract class SAService implements Serializable
{
    public String mServiceName;
    protected final int SERVICE_STATUS_UNINITIALIZED = 0;
    protected final int SERVICE_STATUS_DISCONNECTED = 1;
    protected final int SERVICE_STATUS_CONNECTED = 2;
    protected final int SERVICE_STATUS_PAUSED = 3;
    protected Handler mHandler;
    protected SARawDataLogger mDataLogger;
    static Map<ServiceTypes, Boolean> ServiceStatus;
    UUID uuidService;
    public String mDeviceMac;
    public String mDeviceName;
    public String mDeviceCode;
    protected SADevice mDevice;
    BluetoothGatt mConnectedGatt;
    BluetoothAdapter mBluetoothAdapter;
    protected boolean mServiceReady;
    SABundleInterface mIBundle;
    SAServiceInterface mIServiceDelegate;
    Status mServiceStatus;
    private static BlockingQueue<SABundleObject> mSABundleQueue;
    CountDownLatch mCountDownLatch;
    private Context mContext;
    Thread mThreadStart;
    boolean firstInitialization;
    Thread mThreadWriter;
    MyRunnable mWriter;
    volatile boolean isWriterStarted;
    final List<SACommand> subscribers;
    private boolean bundle;
    protected final String TAG;
    protected String Tag;

    abstract ServiceTypes getType();
    
    abstract BluetoothGattCallback getGattCallBack();
    
    @Override
    public String toString() {
        return this.mServiceName + " (" + this.uuidService + ")";
    }
    
    public boolean IsServiceReady() {
        return this.mServiceReady;
    }
    
    protected SAService() {
        this.mDataLogger = null;
        this.mServiceReady = false;
        this.mServiceStatus = new Status();
        this.firstInitialization = true;
        this.isWriterStarted = false;
        this.subscribers = Collections.synchronizedList(new ArrayList<SACommand>());
        this.TAG = this.getClass().getSimpleName();
    }
    
    protected SAService(final String deviceMac) {
        this.mDataLogger = null;
        this.mServiceReady = false;
        this.mServiceStatus = new Status();
        this.firstInitialization = true;
        this.isWriterStarted = false;
        this.subscribers = Collections.synchronizedList(new ArrayList<SACommand>());
        this.TAG = this.getClass().getSimpleName();
        if (!SensoriaSdk.isInitialized()) {
            Log.e(this.Tag, "SDK NOT initialized - call SensoriaSdk.initialize() first");
            throw new IllegalStateException("SDK NOT initialized - call SensoriaSdk.initialize() first");
        }
        this.mDeviceMac = deviceMac;
        this.mDevice = null;
        this.Tag = this.TAG;
        this.bundle = false;
    }
    
    protected SAService(final String deviceMac, final BlockingQueue<SABundleObject> saBundle) {
        this.mDataLogger = null;
        this.mServiceReady = false;
        this.mServiceStatus = new Status();
        this.firstInitialization = true;
        this.isWriterStarted = false;
        this.subscribers = Collections.synchronizedList(new ArrayList<SACommand>());
        this.TAG = this.getClass().getSimpleName();
        if (!SensoriaSdk.isInitialized()) {
            Log.e(this.Tag, "SDK NOT initialized - call SensoriaSdk.initialize() first");
            throw new IllegalStateException("SDK NOT initialized - call SensoriaSdk.initialize() first");
        }
        this.mDeviceMac = deviceMac;
        this.mDevice = null;
        this.Tag = this.TAG;
        SAService.mSABundleQueue = saBundle;
        this.bundle = true;
    }
    
    public void start(final SAServiceInterface delegate, final Context context) throws InterruptedException {
        this.mContext = context;
        this.start(delegate);
    }
    
    protected abstract void runOnUiThread(final Runnable p0);
    
    protected void serviceReady() {
        Log.d(this.Tag, "ServiceTypes is Ready");
        this.mServiceReady = true;
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (SAService.this.mIServiceDelegate != null) {
                    SAService.this.mIServiceDelegate.didServiceReady(SAService.this.mDevice, SAService.this.getType());
                }
            }
        });
    }
    
    protected void start(final SAServiceInterface delegate) throws InterruptedException {
        this.mIServiceDelegate = delegate;
        if (this.mContext == null) {
            this.mContext = (Context)delegate;
        }
        this.mThreadStart = new Thread(new Runnable() {
            @Override
            public void run() {
                if (Build.VERSION.SDK_INT >= 23) {
                    try {
                        Thread.sleep(256L);
                    }
                    catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                else if (Build.VERSION.SDK_INT == 19) {
                    try {
                        Thread.sleep(256L);
                    }
                    catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                else {
                    try {
                        Thread.sleep(64L);
                    }
                    catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                SAService.this.firstInitialization = false;
                SAService.this.serviceReady();
            }
        });
        if (this.mBluetoothAdapter == null) {
            this.mBluetoothAdapter = ((BluetoothManager)this.mContext.getSystemService( Context.BLUETOOTH_SERVICE)).getAdapter();
            try {
                this.mDevice = new SADevice(this.mBluetoothAdapter.getRemoteDevice(this.mDeviceMac), 0);
                this.Tag = String.format("%s/%s", this.TAG, this.mDevice.deviceName);
                if (this.mDevice == null) {
                    if (SAService.mSABundleQueue != null && this.bundle) {
                        this.mIBundle = new SABundle(SAService.mSABundleQueue);
                        try {
                            this.mIBundle.onServiceStarting(this.mDevice, this.getType());
                        }
                        catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    if (this.mIBundle != null) {
                        this.mIBundle.onServiceError(this.mDevice, this.getType(), SAErrors.ERROR_DEVICE_NOT_STARTED);
                    }
                    this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (SAService.this.mIServiceDelegate != null) {
                                SAService.this.mIServiceDelegate.didServiceError(SAService.this.mDevice, SAService.this.getType(), SAService.this.mServiceName, "start", SAErrors.ERROR_DEVICE_NOT_STARTED);
                            }
                            SAService.this.stop();
                        }
                    });
                }
                else {
                    this.mServiceStatus.set(1);
                    this.setOptionalWriter(true);
                }
            }
            catch (IllegalArgumentException e2) {
                if (this.mIBundle != null) {
                    this.mIBundle.onServiceError(this.mDevice, this.getType(), SAErrors.ERROR_INVALID_MAC_ADDRESS);
                }
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (SAService.this.mIServiceDelegate != null) {
                            SAService.this.mIServiceDelegate.didServiceError(SAService.this.mDevice, SAService.this.getType(), SAService.this.mServiceName, "start", SAErrors.ERROR_INVALID_MAC_ADDRESS);
                        }
                        SAService.this.stop();
                    }
                });
            }
        }
        else {
            this.mServiceStatus.set(1);
            this.setOptionalWriter(true);
        }
    }
    
    void start() throws SAException.NullSAServiceInterfaceException, InterruptedException {
        if (this.mIServiceDelegate != null) {
            this.start(this.mIServiceDelegate);
            return;
        }
        throw new SAException.NullSAServiceInterfaceException(this);
    }
    
    protected abstract void setOptionalWriter(final boolean p0);
    
    protected abstract void deleteOptionalWriter();
    
    protected void resume() throws InterruptedException {
        if (this.mServiceStatus.compare(0)) {
            if (this.mIServiceDelegate != null) {
                this.mIServiceDelegate.didServiceError(this.mDevice, this.getType(), this.mServiceName, "resume", SAErrors.ERROR_GATT_NOT_INITIALIZED);
            }
            return;
        }
        this.setOptionalWriter(false);
        if (this.mServiceStatus.compare(3)) {
            this.mConnectedGatt.connect();
        }
        else if (this.mServiceStatus.compare(1)) {
            try {
                if (Build.VERSION.SDK_INT < 23) {
                    this.mConnectedGatt = this.mDevice.device.connectGatt(this.mContext, false, this.getGattCallBack());
                }
                else {
                    this.mConnectedGatt = this.mDevice.device.connectGatt(this.mContext, false, this.getGattCallBack(), 2);
                }
            }
            catch (IllegalArgumentException e) {
                if (this.mIBundle != null) {
                    this.mIBundle.onServiceError(this.mDevice, this.getType(), SAErrors.ERROR_INVALID_OR_NULL_CALLBACK);
                }
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (SAService.this.mIServiceDelegate != null) {
                            SAService.this.mIServiceDelegate.didServiceError(SAService.this.mDevice, SAService.this.getType(), SAService.this.mServiceName, "resume", SAErrors.ERROR_INVALID_OR_NULL_CALLBACK);
                        }
                    }
                });
            }
        }
    }
    
    protected void pause() {
        this.deleteOptionalWriter();
        if (this.mServiceStatus.get() != 3) {
            this.mServiceStatus.set(3);
            if (this.mIBundle != null) {
                this.mCountDownLatch = new CountDownLatch(1);
            }
            this.mConnectedGatt.disconnect();
            try {
                if (this.mCountDownLatch != null) {
                    this.mCountDownLatch.await();
                }
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    
    protected void stop() {
        if (this.mServiceStatus.compare(3)) {
            if (this.mConnectedGatt != null) {
                this.mConnectedGatt.close();
                this.mConnectedGatt = null;
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (SAService.this.mIServiceDelegate != null) {
                            SAService.this.mIServiceDelegate.didServiceDisconnect(SAService.this.mDevice, SAService.this.getType());
                        }
                    }
                });
            }
        }
        else if (this.mIServiceDelegate != null) {
            this.mIServiceDelegate.didServiceError(this.mDevice, this.getType(), this.mServiceName, "stop", SAErrors.ERROR_GATT_NOT_PAUSED);
        }
    }
    
    protected void dispose() {
    }
    
    static String ToHexString(final byte[] in) {
        final StringBuilder builder = new StringBuilder();
        for (final byte b : in) {
            builder.append(String.format("%02X", b));
        }
        return builder.toString();
    }
    
    static String ToHexString(final short in) {
        return String.format("%02X", in);
    }
    
    protected void addListener(final SACommand command) {
        Log.d(this.Tag, "addListener");
        synchronized (this.subscribers) {
            this.subscribers.add(command);
        }
    }
    
    protected void removeListener(final SACommand command) {
        Log.d(this.Tag, "removeListener");
        synchronized (this.subscribers) {
            this.subscribers.remove(command);
        }
    }
    
    static {
        (SAService.ServiceStatus = Collections.synchronizedMap(new EnumMap<ServiceTypes, Boolean>(ServiceTypes.class))).put(ServiceTypes.GENERIC_ACCESS_SERVICE, false);
        SAService.ServiceStatus.put(ServiceTypes.GENERIC_ATTRIBUTE_SERVICE, false);
        SAService.ServiceStatus.put(ServiceTypes.DEVICE_INFORMATION_SERVICE, false);
        SAService.ServiceStatus.put(ServiceTypes.BATTERY_SERVICE, false);
        SAService.ServiceStatus.put(ServiceTypes.SENSORIA_STREAMING_SERVICE, false);
        SAService.ServiceStatus.put(ServiceTypes.SENSORIA_CONTROL_POINT_SERVICE, false);
        SAService.ServiceStatus.put(ServiceTypes.SENSORIA_CUSTOM_CONFIGURATION_SERVICE, false);
        SAService.ServiceStatus.put(ServiceTypes.BALANCE_STREAMING_SERVICE, false);
    }
    
    public enum ServiceTypes
    {
        GENERIC_ACCESS_SERVICE, 
        GENERIC_ATTRIBUTE_SERVICE, 
        DEVICE_INFORMATION_SERVICE, 
        BATTERY_SERVICE, 
        SENSORIA_STREAMING_SERVICE, 
        SENSORIA_CONTROL_POINT_SERVICE, 
        BALANCE_STREAMING_SERVICE, 
        SENSORIA_CUSTOM_CONFIGURATION_SERVICE, 
        SENSORIA_SMOKE_TEST_SERVICE;
    }
    
    protected class Status implements Serializable
    {
        private int _status;
        
        private Status() {
            this._status = 0;
        }
        
        void set(final int status) {
            this._status = status;
        }
        
        boolean compare(final int status) {
            return this._status == status;
        }
        
        public int get() {
            return this._status;
        }
    }
    
    abstract class MyRunnable implements Runnable
    {
        abstract void terminate();
    }
}
