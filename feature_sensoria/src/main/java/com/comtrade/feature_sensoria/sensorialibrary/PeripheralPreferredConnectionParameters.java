/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: PeripheralPreferredConnectionParameters.java
 * Author: sdelic
 *
 * History:  21.2.22. 10:43 created
 */

//
// Decompiled by Procyon v0.5.36
// 

package com.comtrade.feature_sensoria.sensorialibrary;

import android.os.Parcel;
import android.os.Parcelable;

public class PeripheralPreferredConnectionParameters implements Parcelable
{
    private short minimumConnectionInterval;
    private short maximumConnectionInterval;
    private short slaveLatency;
    private short connectionSupervisionTimeoutMultiplier;
    public static final Parcelable.Creator<PeripheralPreferredConnectionParameters> CREATOR;

    PeripheralPreferredConnectionParameters(final short minimumConnectionInterval, final short maximumConnectionInterval, final short slaveLatency, final short connectionSupervisionTimeoutMultiplier) {
        this.minimumConnectionInterval = minimumConnectionInterval;
        this.maximumConnectionInterval = maximumConnectionInterval;
        this.slaveLatency = slaveLatency;
        this.connectionSupervisionTimeoutMultiplier = connectionSupervisionTimeoutMultiplier;
    }

    protected PeripheralPreferredConnectionParameters(final Parcel in) {
    }

    public void writeToParcel(final Parcel dest, final int flags) {
    }

    public int describeContents() {
        return 0;
    }

    public float getMaximumConnectionInterval() {
        return this.maximumConnectionInterval * 1.25f;
    }

    public float getMinimumConnectionInterval() {
        return this.minimumConnectionInterval * 1.25f;
    }

    public short getSlaveLatency() {
        return this.slaveLatency;
    }

    public int getConnectionSupervisionTimeoutMultiplier() {
        return this.connectionSupervisionTimeoutMultiplier * 10;
    }

    static {
        CREATOR = (Parcelable.Creator)new Parcelable.Creator<PeripheralPreferredConnectionParameters>() {
            public PeripheralPreferredConnectionParameters createFromParcel(final Parcel in) {
                return new PeripheralPreferredConnectionParameters(in);
            }
            
            public PeripheralPreferredConnectionParameters[] newArray(final int size) {
                return new PeripheralPreferredConnectionParameters[size];
            }
        };
    }
}
