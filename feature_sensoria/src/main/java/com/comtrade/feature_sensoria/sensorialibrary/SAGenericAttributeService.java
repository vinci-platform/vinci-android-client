/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: SAGenericAttributeService.java
 * Author: sdelic
 *
 * History:  21.2.22. 10:43 created
 */

//
// Decompiled by Procyon v0.5.36
// 

package com.comtrade.feature_sensoria.sensorialibrary;

import android.bluetooth.BluetoothGattCallback;
import android.os.Handler;

import java.io.Serializable;
import java.util.UUID;

public class SAGenericAttributeService extends SAService implements Serializable
{
    private final UUID UUID_GENERIC_ATTRIBUTE_SERVICE;
    private static Handler mSAHandler;
    
    public SAGenericAttributeService(final String deviceMac, final Handler handler, final SABundleInterface delegate) {
        this.UUID_GENERIC_ATTRIBUTE_SERVICE = UUID.fromString("00001801-0000-1000-8000-00805f9b34fb");
        this.mDeviceMac = deviceMac;
        this.mServiceName = "Generic Attribute ServiceTypes";
        SAGenericAttributeService.mSAHandler = handler;
        this.uuidService = this.UUID_GENERIC_ATTRIBUTE_SERVICE;
    }
    
    @Override
    protected void runOnUiThread(final Runnable runnable) {
    }
    
    @Override
    protected void setOptionalWriter(final boolean force) {
    }
    
    @Override
    protected void deleteOptionalWriter() {
    }
    
    public void stop() {
    }
    
    public ServiceTypes getType() {
        return ServiceTypes.GENERIC_ATTRIBUTE_SERVICE;
    }
    
    @Override
    BluetoothGattCallback getGattCallBack() {
        return null;
    }
}
