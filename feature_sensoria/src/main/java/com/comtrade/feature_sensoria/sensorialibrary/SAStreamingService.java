/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: SAStreamingService.java
 * Author: sdelic
 *
 * History:  21.2.22. 10:43 created
 */

//
// Decompiled by Procyon v0.5.36
// 

package com.comtrade.feature_sensoria.sensorialibrary;

import android.content.Context;
import android.util.Log;

import java.io.Serializable;
import java.util.concurrent.BlockingQueue;

public abstract class SAStreamingService extends SAService implements Serializable
{
    protected SAStreamingService() {
    }
    
    protected SAStreamingService(final String deviceMac) {
        super(deviceMac);
    }
    
    protected SAStreamingService(final String deviceMac, final BlockingQueue<SABundleObject> saBundle) {
        super(deviceMac, saBundle);
    }
    
    @Override
    protected void serviceReady() {
        Log.d(this.Tag, "instantiating data logger for streaming service");
        this.mDataLogger = new SARawDataLogger(this.mDevice, this.mDevice.deviceName.replaceAll("[^A-Za-z0-9\\-]", ""));
        super.serviceReady();
    }

    public void start(final SAServiceInterface delegate, final Context context) throws InterruptedException {
        super.start(delegate, context);
    }

    public void start(final SAServiceInterface delegate) throws InterruptedException {
        super.start(delegate);
    }
    
    public void resume() throws InterruptedException {
        super.resume();
    }
    
    public void pause() {
        super.pause();
    }
    
    public void stop() {
        super.stop();
    }
    
    public void dispose() {
    }
}
