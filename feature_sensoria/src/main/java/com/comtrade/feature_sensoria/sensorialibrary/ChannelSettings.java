/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: ChannelSettings.java
 * Author: sdelic
 *
 * History:  21.2.22. 10:43 created
 */

//
// Decompiled by Procyon v0.5.36
// 

package com.comtrade.feature_sensoria.sensorialibrary;

import android.os.Parcel;
import android.os.Parcelable;

public class ChannelSettings implements Parcelable
{
    private ProtocolType protocol;
    private short samplingPeriod;
    private boolean enabled;
    private boolean transmitViaBle;
    private boolean logToLocalMemory;
    private short channel;
    public static final Parcelable.Creator<ChannelSettings> CREATOR;

    public ChannelSettings(final short channel, final short samplingPeriod, final ProtocolType protocolType, final boolean enabled, final boolean transmitViaBle, final boolean logToLocalMemory) {
        this.protocol = ProtocolType.H20;
        this.enabled = true;
        this.transmitViaBle = true;
        this.logToLocalMemory = false;
        this.channel = channel;
        this.samplingPeriod = samplingPeriod;
        this.protocol = protocolType;
        this.enabled = enabled;
        this.transmitViaBle = transmitViaBle;
        this.logToLocalMemory = logToLocalMemory;
    }

    ChannelSettings(final byte[] array) {
        this.protocol = ProtocolType.H20;
        this.enabled = true;
        this.transmitViaBle = true;
        this.logToLocalMemory = false;
        this.protocol = ProtocolType.get(array[1]);
        this.samplingPeriod = (short)(array[0] & 0xFF);
        this.enabled = ((array[2] & 0x1) == 0x1);
        this.transmitViaBle = ((array[2] >> 3 & 0x1) == 0x1);
        this.logToLocalMemory = ((array[2] >> 2 & 0x1) == 0x1);
    }

    private ChannelSettings(final Parcel in) {
        this.protocol = ProtocolType.H20;
        this.enabled = true;
        this.transmitViaBle = true;
        this.logToLocalMemory = false;
        this.enabled = (in.readByte() != 0);
        this.transmitViaBle = (in.readByte() != 0);
        this.logToLocalMemory = (in.readByte() != 0);
    }

    public void writeToParcel(final Parcel dest, final int flags) {
        dest.writeByte((byte)(byte)(this.enabled ? 1 : 0));
        dest.writeByte((byte)(byte)(this.transmitViaBle ? 1 : 0));
        dest.writeByte((byte)(byte)(this.logToLocalMemory ? 1 : 0));
    }

    public int describeContents() {
        return 0;
    }

    public void setProtocol(final ProtocolType protocol) {
        this.protocol = protocol;
    }

    public void setSamplingPeriod(final short samplingPeriod) {
        this.samplingPeriod = samplingPeriod;
    }

    public void setEnabled(final boolean enabled) {
        this.enabled = enabled;
    }

    public void setTransmitViaBle(final boolean transmitViaBle) {
        this.transmitViaBle = transmitViaBle;
    }

    public void setLogToLocalMemory(final boolean logToLocalMemory) {
        this.logToLocalMemory = logToLocalMemory;
    }

    public ProtocolType getProtocol() {
        return this.protocol;
    }

    public short getSamplingPeriod() {
        return this.samplingPeriod;
    }

    public Boolean getEnabled() {
        return this.enabled;
    }

    public Boolean getTransmitViaBle() {
        return this.transmitViaBle;
    }

    public Boolean getLogToLocalMemory() {
        return this.logToLocalMemory;
    }

    byte channelBooleans2Byte() {
        return (byte)(8 * (this.transmitViaBle ? 1 : 0) + 4 * (this.logToLocalMemory ? 1 : 0) + (this.enabled ? 1 : 0));
    }

    public short getChannel() {
        return this.channel;
    }

    static {
        CREATOR = (Parcelable.Creator)new Parcelable.Creator<ChannelSettings>() {
            public ChannelSettings createFromParcel(final Parcel in) {
                return new ChannelSettings(in);
            }
            
            public ChannelSettings[] newArray(final int size) {
                return new ChannelSettings[size];
            }
        };
    }
}
