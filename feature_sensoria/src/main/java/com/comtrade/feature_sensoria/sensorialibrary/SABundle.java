/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: SABundle.java
 * Author: sdelic
 *
 * History:  21.2.22. 10:43 created
 */

//
// Decompiled by Procyon v0.5.36
// 

package com.comtrade.feature_sensoria.sensorialibrary;

import android.os.Handler;

import java.util.concurrent.BlockingQueue;

class SABundle implements SABundleInterface
{
    BlockingQueue<SABundleObject> mSABundleQueue;
    
    SABundle(final BlockingQueue<SABundleObject> saBundleQueue) {
        this.mSABundleQueue = saBundleQueue;
    }
    
    @Override
    public void onServiceStarted(final SADevice device, final SAService.ServiceTypes serviceTypes, final Handler handler) throws InterruptedException {
        final SABundleObject saBundleObject = new SABundleObject(SABundleObject.ServiceOperation.SERVICE_STARTED, device, serviceTypes, handler);
        this.mSABundleQueue.put(saBundleObject);
    }
    
    @Override
    public void onServiceStopped(final SADevice device, final SAService.ServiceTypes serviceTypes, final Handler handler) throws InterruptedException {
        final SABundleObject saBundleObject = new SABundleObject(SABundleObject.ServiceOperation.SERVICE_STOPPED, device, serviceTypes, handler);
        this.mSABundleQueue.put(saBundleObject);
    }
    
    @Override
    public void onServiceError(final SADevice device, final SAService.ServiceTypes serviceTypes, final SAErrors error) throws InterruptedException {
        final SABundleObject saBundleObject = new SABundleObject(SABundleObject.ServiceOperation.SERVICE_ERROR, device, serviceTypes, error);
        this.mSABundleQueue.put(saBundleObject);
    }
    
    @Override
    public void onServiceStarting(final SADevice device, final SAService.ServiceTypes serviceTypes) throws InterruptedException {
        final SABundleObject saBundleObject = new SABundleObject(SABundleObject.ServiceOperation.SERVICE_STARTING, device, serviceTypes);
        this.mSABundleQueue.put(saBundleObject);
    }
}
