/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: UInt32.java
 * Author: sdelic
 *
 * History:  21.2.22. 10:43 created
 */

//
// Decompiled by Procyon v0.5.36
// 

package com.comtrade.feature_sensoria.sensorialibrary;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class UInt32 extends Number implements Comparable<UInt32>
{
    public static final long MAX_VALUE = 4294967295L;
    public static final long MIN_VALUE = 0L;
    private long value;
    private byte[] localBuffer;
    private boolean isBigEndian;
    
    public UInt32(final long value) {
        this.localBuffer = null;
        this.isBigEndian = true;
        if (value < 0L) {
            this.value = 2147483647L + Math.abs(value);
        }
        else {
            this.value = value;
        }
        if (this.value < 0L || this.value > 4294967295L) {
            throw new NumberFormatException("Invalid UInt16 value");
        }
    }
    
    public UInt32(final String value) {
        this(Long.parseLong(value));
    }
    
    public UInt32(final byte[] buffer) {
        this(buffer, 0);
    }
    
    public UInt32(final byte[] buffer, final int offset) {
        this(buffer, offset, true);
    }
    
    public UInt32(final byte[] buffer, final int offset, final boolean isBigEndian) {
        this(buffer, offset, isBigEndian, 4);
    }
    
    public UInt32(final byte[] buffer, final int offset, final boolean isBigEndian, final int length) {
        this.localBuffer = null;
        this.isBigEndian = true;
        ByteBuffer localBytes = null;
        if (isBigEndian) {
            localBytes = ByteBuffer.allocate(length).order(ByteOrder.BIG_ENDIAN);
        }
        else {
            localBytes = ByteBuffer.allocate(length).order(ByteOrder.LITTLE_ENDIAN);
        }
        localBytes.put(buffer, offset, length);
        this.localBuffer = localBytes.array();
        this.isBigEndian = isBigEndian;
    }
    
    @Override
    public byte byteValue() {
        return (byte)this.value;
    }
    
    @Override
    public int compareTo(final UInt32 other) {
        return (int)(this.value - other.value);
    }
    
    @Override
    public double doubleValue() {
        return (double)this.value;
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof UInt32 && ((UInt32)o).value == this.value;
    }
    
    @Override
    public float floatValue() {
        return ByteBuffer.wrap(this.localBuffer).getFloat();
    }
    
    @Override
    public int hashCode() {
        return (int)this.value;
    }
    
    @Override
    public int intValue() {
        int retVal = 0;
        if (this.isBigEndian) {
            for (int i = 0; i < this.localBuffer.length && i < 4; ++i) {
                retVal = (retVal << 8) + (this.localBuffer[i] & 0xFF);
            }
        }
        else {
            for (int i = 0; i < this.localBuffer.length; ++i) {
                retVal += (int)(((long)this.localBuffer[i] & 0xFFL) << 8 * i);
            }
        }
        return retVal;
    }
    
    @Override
    public long longValue() {
        long retVal = 0L;
        if (this.isBigEndian) {
            for (int i = 0; i < this.localBuffer.length && i < 8; ++i) {
                retVal = (retVal << 8) + (this.localBuffer[i] & 0xFF);
            }
        }
        else {
            for (int i = 0; i < this.localBuffer.length; ++i) {
                retVal += ((long)this.localBuffer[i] & 0xFFL) << 8 * i;
            }
        }
        return retVal;
    }
    
    @Override
    public short shortValue() {
        int retVal = 0;
        if (this.isBigEndian) {
            for (int i = 0; i < this.localBuffer.length && i < 2; ++i) {
                retVal = (retVal << 8) + (this.localBuffer[i] & 0xFF);
            }
        }
        else {
            for (int i = 0; i < this.localBuffer.length; ++i) {
                retVal += (int)(((long)this.localBuffer[i] & 0xFFL) << 8 * i);
            }
        }
        return (short)retVal;
    }
    
    @Override
    public String toString() {
        return "" + this.value;
    }
}
