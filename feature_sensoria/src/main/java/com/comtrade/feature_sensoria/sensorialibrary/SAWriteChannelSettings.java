/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: SAWriteChannelSettings.java
 * Author: sdelic
 *
 * History:  21.2.22. 10:43 created
 */

//
// Decompiled by Procyon v0.5.36
// 

package com.comtrade.feature_sensoria.sensorialibrary;

import android.util.Log;

import java.nio.ByteBuffer;
import java.util.Arrays;

class SAWriteChannelSettings extends SACommand
{
    private final byte[] myCommand;
    
    SAWriteChannelSettings(final short channel, final byte[] payload) {
        super((channel == 0) ? Command.Channel0 : ((channel == 1) ? Command.Channel1 : Command.Channel2), (byte)2, payload);
        (this.myCommand = new byte[] { 4, 0 })[1] = (byte)(channel + 1);
    }
    
    public boolean writeData() {
        final SensoriaGattCharacteristic characteristic = this.waitForCharacteristic();
        return characteristic != null && !this.isNack(characteristic) && !this.isError(characteristic);
    }
    
    @Override
    protected boolean isError(final SensoriaGattCharacteristic characteristic) {
        return super.isError(characteristic);
    }
    
    @Override
    protected boolean isMyCharacteristic(final SensoriaGattCharacteristic characteristic) {
        Log.d("isMyCharacteristic", SAService.ToHexString(characteristic.getValue()));
        Log.d("myCommand", SAService.ToHexString(this.myCommand));
        final ByteBuffer aCopy = ByteBuffer.allocate(2);
        aCopy.put(characteristic.getValue(), 0, 2);
        if (characteristic.getValue()[2] == 2) {
            return Arrays.equals(this.myCommand, aCopy.array());
        }
        if (characteristic.getValue()[2] == 8) {
            return Arrays.equals(this.myCommand, aCopy.array()) && characteristic.getValue().length == 3;
        }
        return characteristic.getValue()[2] == 21 && Arrays.equals(this.myCommand, aCopy.array()) && characteristic.getValue().length == 4;
    }
}
