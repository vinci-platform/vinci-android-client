/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: SASensoriaStreamingService.java
 * Author: sdelic
 *
 * History:  21.2.22. 10:43 created
 */

//
// Decompiled by Procyon v0.5.36
// 

package com.comtrade.feature_sensoria.sensorialibrary;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.util.Log;

import java.io.Serializable;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import java.util.concurrent.BlockingQueue;

public class SASensoriaStreamingService extends SAStreamingService implements Serializable
{
    private UUID UUID_SENSORIA_CORE_STREAMING_SERVICE;
    private UUID UUID_SENSORIA_CORE_STREAMING_CHARACTERISTIC;
    private BluetoothGattCharacteristic mCharacteristicRX;
    private SASensoriaControlPointService sensoriaControlPointService;
    private SASensoriaStreamingProtocols command;
    private boolean initialized;
    private boolean resume;
    private ProtocolType protocolType;
    private Thread receiveThread;
    private boolean initializationFailed;
    private MyWorkerThread mWorkerThread;

    @Override
    protected void runOnUiThread(final Runnable runnable) {
        this.mHandler.post(runnable);
    }
    
    private SASensoriaStreamingService() {
        this.UUID_SENSORIA_CORE_STREAMING_SERVICE = UUID.fromString("1cac0001-656e-696c-4b5f-6e6572726157");
        this.UUID_SENSORIA_CORE_STREAMING_CHARACTERISTIC = UUID.fromString("1CAC0003-656E-696C-4B5F-6E6572726157");
        this.mCharacteristicRX = null;
        this.initialized = false;
        this.resume = true;
        this.protocolType = null;
        this.receiveThread = null;
        this.initializationFailed = false;
    }
    
    public SASensoriaStreamingService(final String deviceMac) {
        super(deviceMac);
        this.UUID_SENSORIA_CORE_STREAMING_SERVICE = UUID.fromString("1cac0001-656e-696c-4b5f-6e6572726157");
        this.UUID_SENSORIA_CORE_STREAMING_CHARACTERISTIC = UUID.fromString("1CAC0003-656E-696C-4B5F-6E6572726157");
        this.mCharacteristicRX = null;
        this.initialized = false;
        this.resume = true;
        this.protocolType = null;
        this.receiveThread = null;
        this.initializationFailed = false;
        Log.d(this.Tag, "ctor");
        this.mServiceName = "Sensoria Core Streaming ServiceTypes";
        this.uuidService = this.UUID_SENSORIA_CORE_STREAMING_SERVICE;
    }

    SASensoriaStreamingService(final String deviceMac, final BlockingQueue<SABundleObject> saBundle) {
        super(deviceMac, saBundle);
        this.UUID_SENSORIA_CORE_STREAMING_SERVICE = UUID.fromString("1cac0001-656e-696c-4b5f-6e6572726157");
        this.UUID_SENSORIA_CORE_STREAMING_CHARACTERISTIC = UUID.fromString("1CAC0003-656E-696C-4B5F-6E6572726157");
        this.mCharacteristicRX = null;
        this.initialized = false;
        this.resume = true;
        this.protocolType = null;
        this.receiveThread = null;
        this.initializationFailed = false;
        Log.d(this.Tag, "ctor");
        this.mServiceName = "Sensoria Core Streaming ServiceTypes";
        this.uuidService = this.UUID_SENSORIA_CORE_STREAMING_SERVICE;
    }

    SASensoriaStreamingService(final String deviceMac, SADevice device, final BlockingQueue<SABundleObject> saBundle) {
        this(deviceMac, saBundle);
        this.mDeviceCode = device.deviceCode;
        this.mDeviceName = device.deviceName;
    }
    
    @Override
    ServiceTypes getType() {
        return ServiceTypes.SENSORIA_STREAMING_SERVICE;
    }
    
    @Override
    public void start(final SAServiceInterface delegate, final Context context) throws InterruptedException {
        Log.d(this.Tag, "SASensoriaStreamingService:start");
        this.mHandler = new Handler(Looper.getMainLooper());
        super.start(delegate, context);
        if (this.firstInitialization && this.protocolType == null) {
            final Runnable task = new Runnable() {
                volatile short tr = 0;
                
                @Override
                public void run() {
                    SASensoriaStreamingService.this.sensoriaControlPointService = new SASensoriaControlPointService(SASensoriaStreamingService.this.mDeviceMac);
                    try {
                        SASensoriaStreamingService.this.sensoriaControlPointService.start(new SAServiceInterface() {
                            @Override
                            public void didServiceError(final SADevice device, final ServiceTypes service, final String serviceName, final String functionName, final SAErrors errorCode) {
                                if (tr < 100) {
                                    return;
                                }
                                SASensoriaStreamingService.this.resume = false;
                                SASensoriaStreamingService.this.initialized = true;
                                SASensoriaStreamingService.this.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (SASensoriaStreamingService.this.mIServiceDelegate != null) {
                                            SASensoriaStreamingService.this.mIServiceDelegate.didServiceError(device, service, serviceName, functionName, errorCode);
                                        }
                                    }
                                });
                            }
                            
                            @Override
                            public void didServiceError(final SADevice device, final ServiceTypes service, final String serviceName, final String functionName, final SAErrors errorCode, final String innerErrorCode) {
                                if (tr < 100) {
                                    return;
                                }
                                SASensoriaStreamingService.this.resume = false;
                                SASensoriaStreamingService.this.initialized = true;
                                SASensoriaStreamingService.this.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (SASensoriaStreamingService.this.mIServiceDelegate != null) {
                                            SASensoriaStreamingService.this.mIServiceDelegate.didServiceError(device, service, serviceName, functionName, errorCode, innerErrorCode);
                                        }
                                    }
                                });
                            }
                            
                            @Override
                            public void didServiceConnect(final SADevice device, final ServiceTypes service) {
                            }
                            
                            @Override
                            public void didServiceDisconnect(final SADevice device, final ServiceTypes service) {
                                SASensoriaStreamingService.this.initialized = true;
                            }
                            
                            @Override
                            public void didServicePause(final SADevice device, final ServiceTypes service) {
                                SASensoriaStreamingService.this.sensoriaControlPointService.stop();
                            }
                            
                            @Override
                            public void didServiceResume(final SADevice device, final ServiceTypes service) {
                            }
                            
                            @Override
                            public void didServiceReady(final SADevice device, final ServiceTypes service) {
                                ChannelSettings cs = null;
                                while (cs == null) {
                                    cs = SASensoriaStreamingService.this.sensoriaControlPointService.readChannelSettings((short)0);
                                    if (++tr == 100) {
                                        break;
                                    }
                                }
                                if (tr < 100) {
                                    SASensoriaStreamingService.this.protocolType = cs.getProtocol();
                                    SASensoriaStreamingService.this.initializationFailed = !SASensoriaStreamingService.this.initializeCommand(SASensoriaStreamingService.this.protocolType, cs.getSamplingPeriod());
                                    SASensoriaStreamingService.this.sensoriaControlPointService.pause();
                                }
                            }
                            
                            @Override
                            public void didServiceReset(final SADevice device, final ServiceTypes service) {
                                SASensoriaStreamingService.this.sensoriaControlPointService.pause();
                                SASensoriaStreamingService.this.sensoriaControlPointService.stop();
                            }
                        }, context);
                    }
                    catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    try {
                        SASensoriaStreamingService.this.sensoriaControlPointService.resume();
                    }
                    catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            };
            (this.mWorkerThread = new MyWorkerThread("controlPoint")).start();
            this.mWorkerThread.prepareHandler();
            this.mWorkerThread.postTask(task);
        }
    }
    
    @Override
    protected void setOptionalWriter(final boolean force) {
    }
    
    @Override
    protected void deleteOptionalWriter() {
    }
    
    @Override
    public void resume() throws InterruptedException {
        Log.d(this.Tag, "resume");
        while (!this.initialized) {
            try {
                Thread.sleep(10L);
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        if (this.initializationFailed) {
            Log.d(this.Tag, "Initialization failed: protocol type " + this.protocolType + " not implemented");
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (SASensoriaStreamingService.this.mIServiceDelegate != null) {
                        SASensoriaStreamingService.this.mIServiceDelegate.didServiceError(SASensoriaStreamingService.this.mDevice, SASensoriaStreamingService.this.getType(), SASensoriaStreamingService.this.mServiceName, "initializeCommand", SAErrors.ERROR_INVALID_CHANNEL_PROTOCOL, "The protocol " + SASensoriaStreamingService.this.protocolType + " is not supported in this release of Sensoria Streaming ServiceTypes SDK");
                    }
                }
            });
        }
        else {
            Log.d(this.Tag, "initialized");
            if (this.resume) {
                if (this.mServiceStatus.compare(1) || this.mServiceStatus.compare(3)) {
                    super.resume();
                    if (this.receiveThread == null) {
                        this.startReceiving();
                    }
                }
                else if (this.mIServiceDelegate != null) {
                    this.mIServiceDelegate.didServiceResume(this.mDevice, this.getType());
                }
            }
        }
    }
    
    private void startReceiving() {
        Log.d(this.Tag, "startReceiving");
        (this.receiveThread = new Thread(new Runnable() {
            @Override
            public void run() {
                if (SASensoriaStreamingService.this.mWorkerThread != null) {
                    SASensoriaStreamingService.this.mWorkerThread.quit();
                    SASensoriaStreamingService.this.mWorkerThread = null;
                }
                while (!SASensoriaStreamingService.this.mServiceReady) {
                    try {
                        Thread.sleep(10L);
                    }
                    catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                SASensoriaStreamingService.this.command.readDataPoint();
            }
        })).start();
    }
    
    @Override
    public void pause() {
        Log.d(this.Tag, "pause");
        if (!this.initializationFailed) {
            this.command.stopRead();
            super.pause();
        }
        else {
            this.deleteOptionalWriter();
            this.mServiceStatus.set(3);
        }
        this.receiveThread = null;
    }
    
    private void receiveDataPoint(final SADataPoint dataPoint, final SAErrors error, final Object more) {
        if (error == SAErrors.ERROR_SUCCESS) {
            this.mDataLogger.logDataPoint(dataPoint);
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (SASensoriaStreamingService.this.mIServiceDelegate != null) {
                        ((SAServiceStreamingServiceInterface)SASensoriaStreamingService.this.mIServiceDelegate)
                                .didUpdateData(SASensoriaStreamingService.this.mDevice, SASensoriaStreamingService.this.getType(), dataPoint);
                    }
                }
            });
        }
        else if (error == SAErrors.ERROR_UNEXPECTED_PROTOCOL_LENGTH) {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (SASensoriaStreamingService.this.mIServiceDelegate != null) {
                        SASensoriaStreamingService.this.mIServiceDelegate.didServiceError(SASensoriaStreamingService.this.mDevice, SASensoriaStreamingService.this.getType(), SASensoriaStreamingService.this.mServiceName, "receiveDataPoint", error, String.format(Locale.US, "Expected length=20 received length=%d", (Integer)more));
                    }
                }
            });
        }
        else {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (SASensoriaStreamingService.this.mIServiceDelegate != null) {
                        SASensoriaStreamingService.this.mIServiceDelegate.didServiceError(SASensoriaStreamingService.this.mDevice, SASensoriaStreamingService.this.getType(), SASensoriaStreamingService.this.mServiceName, "receiveDatapoint", error);
                    }
                }
            });
        }
    }
    
    @Override
    public void stop() {
        Log.d(this.Tag, "stop");
        this.removeListener(this.command);
        super.stop();
    }
    
    @Override
    public void dispose() {
    }
    
    private boolean initializeCommand(final ProtocolType protocolType, final short samplingPeriod) {
        boolean retCode = true;
        switch (protocolType) {
            case G20: {
                this.command = new SAG20Protocol(new SASensoriaStreamingProtocols.DataPointEvent() {
                    @Override
                    public void onReceiveDataPoint(final SADataPoint datapoint) {
                        SASensoriaStreamingService.this.receiveDataPoint(datapoint, SAErrors.ERROR_SUCCESS, null);
                    }
                    
                    @Override
                    public void onReceiveDataPoint(final SADataPoint datapoint, final SAErrors error) {
                        SASensoriaStreamingService.this.receiveDataPoint(datapoint, error, null);
                    }
                    
                    @Override
                    public void onReceiveDataPoint(final SADataPoint datapoint, final SAErrors error, final Object moreInformation) {
                        SASensoriaStreamingService.this.receiveDataPoint(datapoint, error, moreInformation);
                    }
                }, samplingPeriod);
                break;
            }
            case H20: {
                this.command = new SAH20Protocol(new SASensoriaStreamingProtocols.DataPointEvent() {
                    @Override
                    public void onReceiveDataPoint(final SADataPoint datapoint) {
                        SASensoriaStreamingService.this.receiveDataPoint(datapoint, SAErrors.ERROR_SUCCESS, null);
                    }
                    
                    @Override
                    public void onReceiveDataPoint(final SADataPoint datapoint, final SAErrors error) {
                        SASensoriaStreamingService.this.receiveDataPoint(datapoint, error, null);
                    }
                    
                    @Override
                    public void onReceiveDataPoint(final SADataPoint datapoint, final SAErrors error, final Object moreInformation) {
                        SASensoriaStreamingService.this.receiveDataPoint(datapoint, error, moreInformation);
                    }
                }, samplingPeriod);
                break;
            }
            case I20: {
                this.command = new SAI20Protocol(new SASensoriaStreamingProtocols.DataPointEvent() {
                    @Override
                    public void onReceiveDataPoint(final SADataPoint datapoint) {
                        SASensoriaStreamingService.this.receiveDataPoint(datapoint, SAErrors.ERROR_SUCCESS, null);
                    }
                    
                    @Override
                    public void onReceiveDataPoint(final SADataPoint datapoint, final SAErrors error) {
                        SASensoriaStreamingService.this.receiveDataPoint(datapoint, error, null);
                    }
                    
                    @Override
                    public void onReceiveDataPoint(final SADataPoint datapoint, final SAErrors error, final Object moreInformation) {
                        SASensoriaStreamingService.this.receiveDataPoint(datapoint, error, moreInformation);
                    }
                }, samplingPeriod);
                break;
            }
            case J20: {
                this.command = new SAJ20Protocol(new SASensoriaStreamingProtocols.DataPointEvent() {
                    @Override
                    public void onReceiveDataPoint(final SADataPoint datapoint) {
                        SASensoriaStreamingService.this.receiveDataPoint(datapoint, SAErrors.ERROR_SUCCESS, null);
                    }
                    
                    @Override
                    public void onReceiveDataPoint(final SADataPoint datapoint, final SAErrors error) {
                        SASensoriaStreamingService.this.receiveDataPoint(datapoint, error, null);
                    }
                    
                    @Override
                    public void onReceiveDataPoint(final SADataPoint datapoint, final SAErrors error, final Object moreInformation) {
                        SASensoriaStreamingService.this.receiveDataPoint(datapoint, error, moreInformation);
                    }
                }, samplingPeriod);
                break;
            }
            case D20: {
                this.command = new SAD20Protocol(new SASensoriaStreamingProtocols.DataPointEvent() {
                    @Override
                    public void onReceiveDataPoint(final SADataPoint datapoint) {
                        SASensoriaStreamingService.this.receiveDataPoint(datapoint, SAErrors.ERROR_SUCCESS, null);
                    }
                    
                    @Override
                    public void onReceiveDataPoint(final SADataPoint datapoint, final SAErrors error) {
                        SASensoriaStreamingService.this.receiveDataPoint(datapoint, error, null);
                    }
                    
                    @Override
                    public void onReceiveDataPoint(final SADataPoint datapoint, final SAErrors error, final Object moreInformation) {
                        SASensoriaStreamingService.this.receiveDataPoint(datapoint, error, moreInformation);
                    }
                }, samplingPeriod);
                break;
            }
            case E20: {
                this.command = new SAE20Protocol(new SASensoriaStreamingProtocols.DataPointEvent() {
                    @Override
                    public void onReceiveDataPoint(final SADataPoint datapoint) {
                        SASensoriaStreamingService.this.receiveDataPoint(datapoint, SAErrors.ERROR_SUCCESS, null);
                    }
                    
                    @Override
                    public void onReceiveDataPoint(final SADataPoint datapoint, final SAErrors error) {
                        SASensoriaStreamingService.this.receiveDataPoint(datapoint, error, null);
                    }
                    
                    @Override
                    public void onReceiveDataPoint(final SADataPoint datapoint, final SAErrors error, final Object moreInformation) {
                        SASensoriaStreamingService.this.receiveDataPoint(datapoint, error, moreInformation);
                    }
                }, samplingPeriod);
                break;
            }
            case F20: {
                this.command = new SAF20Protocol(new SASensoriaStreamingProtocols.DataPointEvent() {
                    @Override
                    public void onReceiveDataPoint(final SADataPoint datapoint) {
                        SASensoriaStreamingService.this.receiveDataPoint(datapoint, SAErrors.ERROR_SUCCESS, null);
                    }
                    
                    @Override
                    public void onReceiveDataPoint(final SADataPoint datapoint, final SAErrors error) {
                        SASensoriaStreamingService.this.receiveDataPoint(datapoint, error, null);
                    }
                    
                    @Override
                    public void onReceiveDataPoint(final SADataPoint datapoint, final SAErrors error, final Object moreInformation) {
                        SASensoriaStreamingService.this.receiveDataPoint(datapoint, error, moreInformation);
                    }
                }, samplingPeriod);
                break;
            }
            default: {
                retCode = false;
                break;
            }
        }
        this.addListener(this.command);
        return retCode;
    }
    
    @Override
    BluetoothGattCallback getGattCallBack() {
        return new BluetoothGattCallback() {
            private String connectionState(final int status) {
                switch (status) {
                    case 2: {
                        return "Connected";
                    }
                    case 0: {
                        return "Disconnected";
                    }
                    case 1: {
                        return "Connecting";
                    }
                    case 3: {
                        return "Disconnecting";
                    }
                    default: {
                        return String.valueOf(status);
                    }
                }
            }
            
            private boolean setCharacteristicNotification(final BluetoothGattCharacteristic characteristic, final boolean enabled) throws InterruptedException {
                Log.d(SASensoriaStreamingService.this.Tag, "SASensoriaStreamingService:setCharacteristicNotification");
                if (SASensoriaStreamingService.this.mConnectedGatt == null) {
                    if (SASensoriaStreamingService.this.mIBundle != null) {
                        SASensoriaStreamingService.this.mIBundle.onServiceError(SASensoriaStreamingService.this.mDevice, SASensoriaStreamingService.this.getType(), SAErrors.ERROR_GATT_NOT_INITIALIZED);
                    }
                    SASensoriaStreamingService.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (SASensoriaStreamingService.this.mIServiceDelegate != null) {
                                SASensoriaStreamingService.this.mIServiceDelegate.didServiceError(SASensoriaStreamingService.this.mDevice, SASensoriaStreamingService.this.getType(), SASensoriaStreamingService.this.mServiceName, "setCharacteristicNotification", SAErrors.ERROR_GATT_NOT_INITIALIZED);
                            }
                        }
                    });
                    return false;
                }
                SASensoriaStreamingService.this.mConnectedGatt.setCharacteristicNotification(characteristic, enabled);
                final byte[] enableNotification = enabled ? BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE : BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE;
                final UUID uuidCharacteristic = characteristic.getUuid();
                Log.d(SASensoriaStreamingService.this.Tag, "setCharacteristicNotification: UUID: " + uuidCharacteristic.toString());
                final List<BluetoothGattDescriptor> bluetoothGattDescriptors = (List<BluetoothGattDescriptor>)characteristic.getDescriptors();
                if (bluetoothGattDescriptors == null || bluetoothGattDescriptors.size() == 0) {
                    return false;
                }
                final BluetoothGattDescriptor descriptor = bluetoothGattDescriptors.get(0);
                descriptor.setValue(enableNotification);
                SASensoriaStreamingService.this.mConnectedGatt.writeDescriptor(descriptor);
                return true;
            }
            
            public void onConnectionStateChange(final BluetoothGatt gatt, final int status, final int newState) {
                Log.d(SASensoriaStreamingService.this.Tag, "SASensoriaStreamingService Connection State Change: " + status + " -> " + this.connectionState(newState) + " API = " + Build.VERSION.SDK_INT);
                if (status == 0 && newState == 2) {
                    if (SASensoriaStreamingService.this.mServiceStatus.compare(1) || SASensoriaStreamingService.this.mServiceStatus.compare(3)) {
                        Log.d(SASensoriaStreamingService.this.Tag, "Before Gatt discovery services");
                        gatt.discoverServices();
                    }
                }
                else if (status == 0 && newState == 0) {
                    if (Build.VERSION.SDK_INT >= 21 || SASensoriaStreamingService.this.mServiceStatus.compare(3)) {
                        SASensoriaStreamingService.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (SASensoriaStreamingService.this.mIBundle != null) {
                                    try {
                                        SASensoriaStreamingService.this.mIBundle.onServiceStopped(SASensoriaStreamingService.this.mDevice, SASensoriaStreamingService.this.getType(), SASensoriaStreamingService.this.mHandler);
                                    }
                                    catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        });
                        if (SASensoriaStreamingService.this.mServiceStatus.compare(3)) {
                            SASensoriaStreamingService.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (SASensoriaStreamingService.this.mIServiceDelegate != null) {
                                        SASensoriaStreamingService.this.mIServiceDelegate.didServicePause(SASensoriaStreamingService.this.mDevice, SASensoriaStreamingService.this.getType());
                                    }
                                }
                            });
                        }
                    }
                    else {
                        SASensoriaStreamingService.this.mServiceStatus.set(1);
                        Log.d(SASensoriaStreamingService.this.Tag, "Try to reconnect from error:" + status);
                        SASensoriaStreamingService.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                SASensoriaStreamingService.this.pause();
                                SASensoriaStreamingService.this.stop();
                                SASensoriaStreamingService.this.mServiceStatus.set(0);
                                try {
                                    SASensoriaStreamingService.this.start(SASensoriaStreamingService.this.mIServiceDelegate);
                                }
                                catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    SASensoriaStreamingService.this.resume();
                                }
                                catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                    if (SASensoriaStreamingService.this.mCountDownLatch != null) {
                        SASensoriaStreamingService.this.mCountDownLatch.countDown();
                    }
                }
                else if ((status == 8 || status == 133 || status == 34 || status == 22) && newState == 0) {
                    SASensoriaStreamingService.this.mServiceStatus.set(1);
                    Log.d(SASensoriaStreamingService.this.Tag, "Try to reconnect from error:" + status);
                    SASensoriaStreamingService.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            SASensoriaStreamingService.this.mConnectedGatt.close();
                            try {
                                SASensoriaStreamingService.this.resume();
                            }
                            catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
                else if (status == 133 && newState == 2 && Build.VERSION.SDK_INT < 21) {
                    SASensoriaStreamingService.this.mServiceStatus.set(1);
                    Log.d(SASensoriaStreamingService.this.Tag, "Try to reconnect from error =" + status + " new status = " + newState + " API = " + Build.VERSION.SDK_INT);
                    SASensoriaStreamingService.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            SASensoriaStreamingService.this.mConnectedGatt.close();
                            try {
                                SASensoriaStreamingService.this.resume();
                            }
                            catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
                else if (status != 0) {
                    try {
                        throw new SAException.UnmanagedConnectionException(status, newState);
                    }
                    catch (SAException.UnmanagedConnectionException e) {
                        e.printStackTrace();
                    }
                }
            }
            
            public void onServicesDiscovered(final BluetoothGatt gatt, final int status) {
                Log.d(SASensoriaStreamingService.this.Tag, "SASensoriaStreamingService:onServicesDiscovered");
                if (status == 0) {
                    final BluetoothGattService gattService = SASensoriaStreamingService.this.mConnectedGatt.getService(SASensoriaStreamingService.this.UUID_SENSORIA_CORE_STREAMING_SERVICE);
                    if (gattService != null) {
                        Log.i(SASensoriaStreamingService.this.Tag, "ServiceTypes UUID found: " + gattService.getUuid().toString());
                        final List<BluetoothGattCharacteristic> gattCharacteristics = (List<BluetoothGattCharacteristic>)gattService.getCharacteristics();
                        for (final BluetoothGattCharacteristic gattCharacteristic : gattCharacteristics) {
                            Log.d(SASensoriaStreamingService.this.Tag, "Characteristic UUID found: " + gattCharacteristic.getUuid().toString());
                            if (gattCharacteristic.getUuid().compareTo(SASensoriaStreamingService.this.UUID_SENSORIA_CORE_STREAMING_CHARACTERISTIC) == 0) {
                                Log.i(SASensoriaStreamingService.this.Tag, "UUID_SENSORIA_CORE_STREAMING_CHARACTERISTIC found");
                                SASensoriaStreamingService.this.mCharacteristicRX = gattCharacteristic;
                                if ((SASensoriaStreamingService.this.mCharacteristicRX.getProperties() & 0x10) > 0) {
                                    try {
                                        this.setCharacteristicNotification(SASensoriaStreamingService.this.mCharacteristicRX, true);
                                    }
                                    catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                    break;
                                }
                                continue;
                            }
                        }
                        if (SASensoriaStreamingService.this.mCharacteristicRX == null) {
                            if (SASensoriaStreamingService.this.mIBundle != null) {
                                try {
                                    SASensoriaStreamingService.this.mIBundle.onServiceError(SASensoriaStreamingService.this.mDevice, SASensoriaStreamingService.this.getType(), SAErrors.ERROR_CHARACTERISTIC_NOT_FOUND);
                                }
                                catch (InterruptedException e2) {
                                    e2.printStackTrace();
                                }
                            }
                            SASensoriaStreamingService.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (SASensoriaStreamingService.this.mIServiceDelegate != null) {
                                        SASensoriaStreamingService.this.mIServiceDelegate.didServiceError(SASensoriaStreamingService.this.mDevice, SASensoriaStreamingService.this.getType(), SASensoriaStreamingService.this.mServiceName, "onServicesDiscovered", SAErrors.ERROR_CHARACTERISTIC_NOT_FOUND);
                                    }
                                }
                            });
                        }
                        else if (SASensoriaStreamingService.this.mServiceStatus.compare(1) || SASensoriaStreamingService.this.mServiceStatus.compare(3)) {
                            if (SASensoriaStreamingService.this.mIBundle != null) {
                                try {
                                    SASensoriaStreamingService.this.mIBundle.onServiceStarted(SASensoriaStreamingService.this.mDevice, SASensoriaStreamingService.this.getType(), SASensoriaStreamingService.this.mHandler);
                                }
                                catch (InterruptedException e2) {
                                    e2.printStackTrace();
                                }
                            }
                            SASensoriaStreamingService.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (SASensoriaStreamingService.this.mServiceStatus.compare(1)) {
                                        Log.d(SASensoriaStreamingService.this.Tag, "ServiceTypes discovered and connected");
                                        if (SASensoriaStreamingService.this.mIServiceDelegate != null) {
                                            SASensoriaStreamingService.this.mIServiceDelegate.didServiceConnect(SASensoriaStreamingService.this.mDevice, SASensoriaStreamingService.this.getType());
                                        }
                                    }
                                    else {
                                        if (SASensoriaStreamingService.this.mIServiceDelegate != null) {
                                            SASensoriaStreamingService.this.mIServiceDelegate.didServiceResume(SASensoriaStreamingService.this.mDevice, SASensoriaStreamingService.this.getType());
                                        }
                                        Log.d(SASensoriaStreamingService.this.Tag, "ServiceTypes resumed");
                                    }
                                }
                            });
                            SASensoriaStreamingService.this.mServiceStatus.set(2);
                        }
                    }
                    else {
                        Log.i(SASensoriaStreamingService.this.Tag, "ServiceTypes not found for UUID: " + SASensoriaStreamingService.this.UUID_SENSORIA_CORE_STREAMING_SERVICE.toString());
                        if (SASensoriaStreamingService.this.mIBundle != null) {
                            try {
                                SASensoriaStreamingService.this.mIBundle.onServiceError(SASensoriaStreamingService.this.mDevice, SASensoriaStreamingService.this.getType(), SAErrors.ERROR_SERVICE_NOT_FOUND);
                            }
                            catch (InterruptedException e3) {
                                e3.printStackTrace();
                            }
                        }
                        SASensoriaStreamingService.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (SASensoriaStreamingService.this.mIServiceDelegate != null) {
                                    SASensoriaStreamingService.this.mIServiceDelegate.didServiceError(SASensoriaStreamingService.this.mDevice, SASensoriaStreamingService.this.getType(), SASensoriaStreamingService.this.mServiceName, "onServicesDiscovered", SAErrors.ERROR_SERVICE_NOT_FOUND);
                                }
                            }
                        });
                    }
                    if (SASensoriaStreamingService.this.firstInitialization && SASensoriaStreamingService.this.mServiceStatus.compare(2)) {
                        SASensoriaStreamingService.this.mThreadStart.start();
                    }
                }
            }
            
            public void onReadRemoteRssi(final BluetoothGatt gatt, final int rssi, final int status) {
                Log.d(SASensoriaStreamingService.this.Tag, "Remote RSSI: " + rssi);
            }
            
            public void onCharacteristicChanged(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {
//                Log.v(SASensoriaStreamingService.this.Tag, "onCharacteristicChanged: " + SAService.ToHexString(characteristic.getValue()));
                SASensoriaStreamingService.this.command.setCharacteristic(new SensoriaGattCharacteristic(characteristic));
            }
        };
    }


    private class MyWorkerThread extends HandlerThread
    {
        private Handler mWorkerHandler;
        
        MyWorkerThread(final String name) {
            super(name);
        }
        
        void postTask(final Runnable task) {
            this.mWorkerHandler.post(task);
        }
        
        void prepareHandler() {
            this.mWorkerHandler = new Handler(this.getLooper());
        }
    }
}
