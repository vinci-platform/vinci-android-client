# vINCI App

This repository contains vINCI Mobile Application

# Commit message enforcement

The vInci Android client project has commit message formatting enforcement guidelines. Much like shared code style rules, client-side (local development machines) enforcement of a standardized GIT commit message format provides a way for a better and cleaner GIT project history. Server-side commit message rules are planned.

**1. Enable commit message hook**

In your .git folder navigate to the 'hooks' folder. In the 'hooks' folder look for the 'commit-msg.sample' file.
Rename the 'commit-msg.sample' file to 'commit-msg' (remove the 'sample' extension).

This step enables GIT to check the commit content before the actual commit has been created.

**2. Add the vInci rules**

Open the new 'commit-msg' file in your editor of choice.
Replace any un-commented code with the lines below:

```

REGEX_PREFIX="^((BUILD|FIX|API|DESIGN|TEST|Merge|Update)|(VIN-[0-9]+)): (START|PROGRESS|FINISH)+"
PREFIX_IN_COMMIT=$(echo $(cat "$1") | grep -o -E "$REGEX_PREFIX")

if [[-z "$PREFIX_IN_COMMIT"]]; then
echo "[commit-msg-hook] Your commit message is illegal. Please edit your commint to comply with the following regex: $REGEX_PREFIX"
exit 1
fi

```

**3. Test vInci rules**

Test the new commit message rules by trying to make create a new commit with a message not in the new format, eg.: "test commit message new rules"

In your console you should see the new commit message validation error displayed.

Let's change the commit message to comply with the new validation rules : "TEST: FINISH added new commit message rules"

This time a new commit should be created.

In case of any issues contact your team lead or anyone in your team that can provide more input for the issue.

# Work machine recommendations

Start with empty Ubuntu machine install

**1. Update server**

```
sudo apt update
sudo apt upgrade
sudo reboot
```

**_2. Install prerequisites_**

```
sudo apt install libc6-dev-i386 lib32z1 openjdk-8-jdk unzip git
```

**_3. Download latest Android studio command line tools from https://developer.android.com/studio_**

```
mkdir android-sdk
cd android-sdk
wget https://dl.google.com/android/repository/commandlinetools-linux-7583922_latest.zip
unzip commandlinetools-linux-7583922_latest.zip
cmdline-tools/bin/sdkmanager --sdk_root=cmdline-tools --update
cmdline-tools/bin/sdkmanager --sdk_root=cmdline-tools --licenses
```

**_4. Add Android SDK to path variable into file $HOME/.bash_profile_**

```
# Android
export ANDROID_SDK_ROOT=$HOME/android-sdk
export PATH=$PATH:$ANDROID_SDK_ROOT/cmdline-tools/bin
```

**_5. Install Gradle - check for latest version on https://gradle.org/install/_**

```
VERSION=7.3.3
wget https://services.gradle.org/distributions/gradle-$VERSION-bin.zip -P /tmp
sudo unzip -d /opt/gradle /tmp/gradle-$VERSION-bin.zip
sudo ln -s /opt/gradle/gradle-$VERSION /opt/gradle/latest
```

**_6. Add Gradle profile file /etc/profile.d/gradle.sh_**

```
# Gradle paths
export GRADLE_HOME=/opt/gradle/latest
export PATH=$GRADLE_HOME/bin:$PATH
```

**_7. Run commands_**

```
chmod +x /etc/profile.d/gradle.sh
source /etc/profile.d/gradle.sh
```

**_8. Download Source code form gitlab.com_**

```
git clone this-repo-name
```

**_9. Provide testing.properties file_**

```
#For loading sensitive testing information, provide the testing.properties #file with the testing login credentials.

#Create testing.properties file in the following format:

USERNAME:"replaceWithTestUsername"
PASSWORD:"replaceWithTestPassword"

#Move the newly created testing.properties file into the ROOT application directory.
```

**_10. Build project_**

```
gradle clean
gradle build -x lint -x test
```
