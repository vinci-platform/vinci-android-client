/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: GetUserEventRecordsImplTest.kt
 * Author: sdelic
 *
 * History:  15.4.22. 17:56 created
 */

package com.comtrade.feature_events.use_case

import com.comtrade.domain.contracts.repository.IAppRepository
import com.comtrade.domain.entities.event.EventEntity
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.anyLong
import org.mockito.Mockito.*

/**
 * Test scenario for the User obtain health records use case.
 */
internal class GetUserEventRecordsImplTest {

    private lateinit var appRepository: IAppRepository
    private lateinit var getUserEventRecordsImpl: GetUserEventRecordsImpl

    @Before
    fun setUp() {
        appRepository = mock(IAppRepository::class.java)
        getUserEventRecordsImpl = GetUserEventRecordsImpl(appRepository)

    }

    @Test
    fun `given a valid user id return an empty list of health records`() {
        val testId = 1L
        val testTimeStamp = 1L
        runBlocking {
            `when`(
                appRepository.getUserEventRecords(
                    anyLong(),
                    anyLong()
                )
            ).thenReturn(getTestUserEventRecords())

            //Make call.
            val data = getUserEventRecordsImpl.getUserEventRecords(testId, testTimeStamp)

            verify(appRepository, times(1)).getUserEventRecords(testId, testTimeStamp)
            assertNotNull(data)
            assertEquals(0, data.size)
        }
    }

    @Test
    fun `given an invalid user id return an empty list of health records`() {
        val testId = 1L
        val testTimeStamp = 1L
        runBlocking {
            `when`(
                appRepository.getUserEventRecords(
                    anyLong(),
                    anyLong()
                )
            ).thenReturn(null)

            //Make call.
            val data = getUserEventRecordsImpl.getUserEventRecords(testId, testTimeStamp)

            verify(appRepository, times(1)).getUserEventRecords(testId, testTimeStamp)
            assertNotNull(data)
            assertEquals(0, data.size)
        }
    }

    @Test
    fun `given a valid user id return a list of health records`() {
        val testCount = 1_000
        val testId = 1L
        val testTimeStamp = 1L
        runBlocking {
            `when`(
                appRepository.getUserEventRecords(
                    anyLong(),
                    anyLong()
                )
            ).thenReturn(getTestUserEventRecords(testCount))

            //Make call.
            val data = getUserEventRecordsImpl.getUserEventRecords(testId, testTimeStamp)

            verify(appRepository, times(1)).getUserEventRecords(testId, testTimeStamp)
            assertNotNull(data)
            assertEquals(testCount, data.size)
        }
    }
}


/**
 * Provide User test health data.
 */
internal fun getTestUserEventRecords(itemCount: Int = 0): List<EventEntity> {
    if (itemCount <= 0) {
        return emptyList()
    }

    val testData = mutableListOf<EventEntity>()

    for (i in 0 until itemCount) {
        testData.add(
            EventEntity(
                id = i,
                userId = 1L,
                timestamp = 1L,
                created = System.currentTimeMillis(),
                notify = 1L,
                repeat = "",
                title = "Test Health Record $i",
                type = "",
                text = "Health record information",
                data = "{}",
                syncedWithServer = false,
                deleted = false
            )
        )
    }

    return testData
}