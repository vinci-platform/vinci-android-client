/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: SaveUserEventRecordUseCaseImplTest.kt
 * Author: sdelic
 *
 * History:  20.4.22. 17:11 created
 */

package com.comtrade.feature_events.use_case

import com.comtrade.domain.contracts.repository.IAppRepository
import com.comtrade.domain.contracts.usecases.events.IPersistUserEventRecordUseCase
import com.comtrade.domain.entities.event.EventEntity
import kotlinx.coroutines.runBlocking
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.`when`

/**
 * Test scenarios for the save User event use case implementation detail.
 */
internal class SaveUserEventRecordUseCaseImplTest {
    private lateinit var appRepository: IAppRepository
    private lateinit var saveUserEventRecordUseCaseImpl: IPersistUserEventRecordUseCase

    @Before
    fun setUp() {
        appRepository = Mockito.mock(IAppRepository::class.java)
        saveUserEventRecordUseCaseImpl = SaveUserEventRecordUseCaseImpl(appRepository)
    }

    @Test
    fun `given valid input data when insert event then return new Id`() {
        runBlocking {
            val testEventEntity = getTestEventEntity()
            `when`(appRepository.insertUserEventRecord(testEventEntity)).thenReturn(1)
            val result = saveUserEventRecordUseCaseImpl.saveUserEventRecord(testEventEntity)

            Mockito.verify(appRepository, Mockito.times(1))
                .insertUserEventRecord(testEventEntity)

            assertNotNull(result)
            assertNotEquals(0, result)
            assertNotEquals(-1, result)
        }
    }

    @Test
    fun `given valid input data when insert event then return 0`() {
        runBlocking {
            val testEventEntity = getTestEventEntity()
            `when`(appRepository.insertUserEventRecord(testEventEntity)).thenReturn(0)
            val result = saveUserEventRecordUseCaseImpl.saveUserEventRecord(testEventEntity)

            Mockito.verify(appRepository, Mockito.times(1))
                .insertUserEventRecord(testEventEntity)

            assertNotNull(result)
            assertEquals(0, result)
        }
    }

    @Test
    fun `given an invalid input data when insert event then return -1`() {
        runBlocking {
            val testEventEntity = getTestEventEntity().copy(created = -1)
            `when`(appRepository.insertUserEventRecord(testEventEntity)).thenReturn(-1)
            val result = saveUserEventRecordUseCaseImpl.saveUserEventRecord(testEventEntity)

            Mockito.verify(appRepository, Mockito.times(1))
                .insertUserEventRecord(testEventEntity)

            assertNotNull(result)
            assertEquals(-1, result)
        }
    }

    @Test(expected = Exception::class)
    fun `given an invalid userId provided throw exception`() {
        runBlocking {
            val testEventEntity = getTestEventEntity().copy(userId = -1)
            `when`(appRepository.insertUserEventRecord(testEventEntity)).thenReturn(-1)
            val result = saveUserEventRecordUseCaseImpl.saveUserEventRecord(testEventEntity)

            Mockito.verify(appRepository, Mockito.times(1))
                .insertUserEventRecord(testEventEntity)

            assertNotNull(result)
            assertEquals(-1, result)
        }
    }
}

internal fun getTestEventEntity(): EventEntity = EventEntity(
    id = 0,
    userId = 1L,
    timestamp = 1L,
    created = System.currentTimeMillis(),
    notify = 1L,
    repeat = "",
    title = "Test Health Record",
    type = "",
    text = "Health record information",
    data = "{}",
    syncedWithServer = false,
    deleted = false
)