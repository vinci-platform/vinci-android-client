/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: UpdateUserEventUseCaseImplTest.kt
 * Author: sdelic
 *
 * History:  20.4.22. 17:11 created
 */

package com.comtrade.feature_events.use_case

import com.comtrade.domain.contracts.repository.IAppRepository
import com.comtrade.domain.contracts.usecases.events.IUpdateUserEventUseCase
import kotlinx.coroutines.runBlocking
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito

/**
 * Test scenarios for the update User events use case implementation detail.
 */
internal class UpdateUserEventUseCaseImplTest {
    private lateinit var appRepository: IAppRepository
    private lateinit var updateUserEventUseCase: IUpdateUserEventUseCase

    @Before
    fun setUp() {
        appRepository = Mockito.mock(IAppRepository::class.java)
        updateUserEventUseCase = UpdateUserEventUseCaseImpl(appRepository)
    }

    @Test
    fun `given valid input data when update event then return true`() {
        runBlocking {
            val testEventEntity = getTestEventEntity()
            Mockito.`when`(appRepository.updateUserEventRecord(testEventEntity)).thenReturn(true)
            val result = updateUserEventUseCase.updateUserEventRecord(testEventEntity)
            Mockito.verify(appRepository, Mockito.times(1))
                .updateUserEventRecord(testEventEntity)
            assertNotNull(result)
            assertTrue(result)
        }
    }

    @Test
    fun `given valid input data when update event then return false`() {
        runBlocking {
            val testEventEntity = getTestEventEntity()
            Mockito.`when`(appRepository.updateUserEventRecord(testEventEntity)).thenReturn(false)
            val result = updateUserEventUseCase.updateUserEventRecord(testEventEntity)
            Mockito.verify(appRepository, Mockito.times(1))
                .updateUserEventRecord(testEventEntity)
            assertNotNull(result)
            assertFalse(result)
        }
    }

    @Test(expected = Exception::class)
    fun `given an invalid userId provided throw exception`() {
        runBlocking {

            val testEventEntity = getTestEventEntity().copy(userId = -1)
            Mockito.`when`(appRepository.updateUserEventRecord(testEventEntity)).thenReturn(false)
            val result = updateUserEventUseCase.updateUserEventRecord(testEventEntity)
            Mockito.verify(appRepository, Mockito.times(1))
                .updateUserEventRecord(testEventEntity)
            assertNotNull(result)
            assertFalse(result)
        }
    }
}