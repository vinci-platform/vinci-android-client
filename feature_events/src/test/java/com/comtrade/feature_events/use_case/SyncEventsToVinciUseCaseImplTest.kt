/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: SyncEventsToVinciUseCaseImplTest.kt
 * Author: sdelic
 *
 * History:  12.5.22. 11:41 created
 */

package com.comtrade.feature_events.use_case

import com.comtrade.domain.contracts.repository.IAppRepository
import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.contracts.repository.user.UserServiceRepository
import com.comtrade.domain.contracts.usecases.events.ISyncEventsToVinciUseCase
import com.comtrade.domain.entities.event.EventEntity
import com.comtrade.domain.entities.event.PostEventRecordRequest
import kotlinx.coroutines.runBlocking
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.anyLong
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mockito

/**
 * Test scenario for the User events synchronization with the Vinci backend use case implementation detail.
 */
internal class SyncEventsToVinciUseCaseImplTest {
    private val testUserId = 1L
    private val testToken = "testToken"
    private val testDeletionEventsCount = 100 //Performance problem with larger numbers.

    private lateinit var prefs: IPrefs
    private lateinit var appRepository: IAppRepository
    private lateinit var userServiceRepository: UserServiceRepository
    private lateinit var syncEventsToVinciUseCase: ISyncEventsToVinciUseCase

    @Before
    fun setUp() {
        prefs = Mockito.mock(IPrefs::class.java)
        appRepository = Mockito.mock(IAppRepository::class.java)
        userServiceRepository = Mockito.mock(UserServiceRepository::class.java)
        syncEventsToVinciUseCase =
            SyncEventsToVinciUseCaseImpl(prefs, appRepository, userServiceRepository)
    }

    @Test
    fun `given valid input when called return true`() {
        runBlocking {
            val testEventData = getTestUserEventRecords(testDeletionEventsCount)
            Mockito.`when`(prefs.userId).thenReturn(testUserId)
            Mockito.`when`(prefs.token).thenReturn(testToken)

            Mockito.`when`(appRepository.getEventRecordsForDelete(testUserId))
                .thenReturn(testEventData)
            Mockito.`when`(
                userServiceRepository.getEventRecord(
                    anyString(), anyLong(), anyLong(),
                    anyLong()
                )
            ).thenReturn(testEventData)

            Mockito.`when`(
                userServiceRepository.deleteEventRecord(
                    anyString(), anyLong()
                )
            ).thenReturn(true)

            Mockito.`when`(
                appRepository.deleteEvent(
                    any(EventEntity::class.java)
                )
            ).thenReturn(1)

            Mockito.`when`(appRepository.getEventRecordsForSync(testUserId)).thenReturn(
                getTestUserEventRecords(testDeletionEventsCount)
            )

            Mockito.`when`(
                userServiceRepository.postEventRecord(
                    anyString(), any(PostEventRecordRequest::class.java)
                )
            ).thenReturn(true)

            Mockito.`when`(appRepository.updateUserEventRecord(any(EventEntity::class.java)))
                .thenReturn(
                    true
                )

            val result = syncEventsToVinciUseCase.syncEventsToVinci()

            Mockito.verify(prefs, Mockito.times(1)).userId
            Mockito.verify(prefs, Mockito.times(1)).token
            Mockito.verify(appRepository, Mockito.times(1)).getEventRecordsForDelete(testUserId)
            Mockito.verify(userServiceRepository, Mockito.times(testDeletionEventsCount))
                .getEventRecord(
                    anyString(), anyLong(), anyLong(),
                    anyLong()
                )

            assertNotNull(result)
            assertTrue(result)
        }
    }

    @Test
    fun `given valid input when called return false for delete event`() {
        runBlocking {
            val testEventData = getTestUserEventRecords(testDeletionEventsCount)
            Mockito.`when`(prefs.userId).thenReturn(testUserId)
            Mockito.`when`(prefs.token).thenReturn(testToken)

            Mockito.`when`(appRepository.getEventRecordsForDelete(testUserId))
                .thenReturn(testEventData)
            Mockito.`when`(
                userServiceRepository.getEventRecord(
                    anyString(), anyLong(), anyLong(),
                    anyLong()
                )
            ).thenReturn(testEventData)

            Mockito.`when`(
                userServiceRepository.deleteEventRecord(
                    anyString(), anyLong()
                )
            ).thenReturn(false)

            Mockito.`when`(
                appRepository.deleteEvent(
                    any(EventEntity::class.java)
                )
            ).thenReturn(1)

            Mockito.`when`(appRepository.getEventRecordsForSync(testUserId)).thenReturn(
                getTestUserEventRecords(testDeletionEventsCount)
            )

            Mockito.`when`(
                userServiceRepository.postEventRecord(
                    anyString(), any(PostEventRecordRequest::class.java)
                )
            ).thenReturn(true)

            Mockito.`when`(appRepository.updateUserEventRecord(any(EventEntity::class.java)))
                .thenReturn(
                    true
                )

            val result = syncEventsToVinciUseCase.syncEventsToVinci()

            Mockito.verify(prefs, Mockito.times(1)).userId
            Mockito.verify(prefs, Mockito.times(1)).token
            Mockito.verify(appRepository, Mockito.times(1)).getEventRecordsForDelete(testUserId)
            Mockito.verify(userServiceRepository, Mockito.times(1))
                .getEventRecord(
                    anyString(), anyLong(), anyLong(),
                    anyLong()
                )

            assertNotNull(result)
            assertFalse(result)
        }
    }

    @Test
    fun `given valid input when called return false for local db delete event`() {
        runBlocking {
            val testEventData = getTestUserEventRecords(testDeletionEventsCount)
            Mockito.`when`(prefs.userId).thenReturn(testUserId)
            Mockito.`when`(prefs.token).thenReturn(testToken)

            Mockito.`when`(appRepository.getEventRecordsForDelete(testUserId))
                .thenReturn(testEventData)
            Mockito.`when`(
                userServiceRepository.getEventRecord(
                    anyString(), anyLong(), anyLong(),
                    anyLong()
                )
            ).thenReturn(testEventData)

            Mockito.`when`(
                userServiceRepository.deleteEventRecord(
                    anyString(), anyLong()
                )
            ).thenReturn(true)

            Mockito.`when`(
                appRepository.deleteEvent(
                    any(EventEntity::class.java)
                )
            ).thenReturn(-1)

            Mockito.`when`(appRepository.getEventRecordsForSync(testUserId)).thenReturn(
                getTestUserEventRecords(testDeletionEventsCount)
            )

            Mockito.`when`(
                userServiceRepository.postEventRecord(
                    anyString(), any(PostEventRecordRequest::class.java)
                )
            ).thenReturn(true)

            Mockito.`when`(appRepository.updateUserEventRecord(any(EventEntity::class.java)))
                .thenReturn(
                    true
                )

            val result = syncEventsToVinciUseCase.syncEventsToVinci()

            Mockito.verify(prefs, Mockito.times(1)).userId
            Mockito.verify(prefs, Mockito.times(1)).token
            Mockito.verify(appRepository, Mockito.times(1)).getEventRecordsForDelete(testUserId)
            Mockito.verify(userServiceRepository, Mockito.times(1))
                .getEventRecord(
                    anyString(), anyLong(), anyLong(),
                    anyLong()
                )

            assertNotNull(result)
            assertFalse(result)
        }
    }

    @Test
    fun `given valid input when called return false for create event api call`() {
        runBlocking {
            val testEventData = getTestUserEventRecords(testDeletionEventsCount)
            Mockito.`when`(prefs.userId).thenReturn(testUserId)
            Mockito.`when`(prefs.token).thenReturn(testToken)

            Mockito.`when`(appRepository.getEventRecordsForDelete(testUserId))
                .thenReturn(testEventData)
            Mockito.`when`(
                userServiceRepository.getEventRecord(
                    anyString(), anyLong(), anyLong(),
                    anyLong()
                )
            ).thenReturn(testEventData)

            Mockito.`when`(
                userServiceRepository.deleteEventRecord(
                    anyString(), anyLong()
                )
            ).thenReturn(true)

            Mockito.`when`(
                appRepository.deleteEvent(
                    any(EventEntity::class.java)
                )
            ).thenReturn(1)

            Mockito.`when`(appRepository.getEventRecordsForSync(testUserId)).thenReturn(
                getTestUserEventRecords(testDeletionEventsCount)
            )

            Mockito.`when`(
                userServiceRepository.postEventRecord(
                    anyString(), any(PostEventRecordRequest::class.java)
                )
            ).thenReturn(false)

            Mockito.`when`(appRepository.updateUserEventRecord(any(EventEntity::class.java)))
                .thenReturn(
                    true
                )

            val result = syncEventsToVinciUseCase.syncEventsToVinci()

            Mockito.verify(prefs, Mockito.times(1)).userId
            Mockito.verify(prefs, Mockito.times(1)).token
            Mockito.verify(appRepository, Mockito.times(1)).getEventRecordsForDelete(testUserId)
            Mockito.verify(userServiceRepository, Mockito.times(testDeletionEventsCount))
                .getEventRecord(
                    anyString(), anyLong(), anyLong(),
                    anyLong()
                )

            assertNotNull(result)
            assertFalse(result)
        }
    }

    @Test
    fun `given valid input when called return false for update local db event`() {
        runBlocking {
            val testEventData = getTestUserEventRecords(testDeletionEventsCount)
            Mockito.`when`(prefs.userId).thenReturn(testUserId)
            Mockito.`when`(prefs.token).thenReturn(testToken)

            Mockito.`when`(appRepository.getEventRecordsForDelete(testUserId))
                .thenReturn(testEventData)
            Mockito.`when`(
                userServiceRepository.getEventRecord(
                    anyString(), anyLong(), anyLong(),
                    anyLong()
                )
            ).thenReturn(testEventData)

            Mockito.`when`(
                userServiceRepository.deleteEventRecord(
                    anyString(), anyLong()
                )
            ).thenReturn(true)

            Mockito.`when`(
                appRepository.deleteEvent(
                    any(EventEntity::class.java)
                )
            ).thenReturn(1)

            Mockito.`when`(appRepository.getEventRecordsForSync(testUserId)).thenReturn(
                getTestUserEventRecords(testDeletionEventsCount)
            )

            Mockito.`when`(
                userServiceRepository.postEventRecord(
                    anyString(), any(PostEventRecordRequest::class.java)
                )
            ).thenReturn(true)

            Mockito.`when`(appRepository.updateUserEventRecord(any(EventEntity::class.java)))
                .thenReturn(
                    false
                )

            val result = syncEventsToVinciUseCase.syncEventsToVinci()

            Mockito.verify(prefs, Mockito.times(1)).userId
            Mockito.verify(prefs, Mockito.times(1)).token
            Mockito.verify(appRepository, Mockito.times(1)).getEventRecordsForDelete(testUserId)
            Mockito.verify(userServiceRepository, Mockito.times(testDeletionEventsCount))
                .getEventRecord(
                    anyString(), anyLong(), anyLong(),
                    anyLong()
                )

            assertNotNull(result)
            assertFalse(result)
        }
    }

    @Test(expected = Exception::class)
    fun `given an invalid token input when called throw exception`() {
        runBlocking {
            runBlocking {
                val testEventData = getTestUserEventRecords(testDeletionEventsCount)
                Mockito.`when`(prefs.userId).thenReturn(testUserId)
                Mockito.`when`(prefs.token).thenReturn(null)
                Mockito.`when`(appRepository.getEventRecordsForDelete(testUserId))
                    .thenReturn(testEventData)
                Mockito.`when`(
                    userServiceRepository.getEventRecord(
                        anyString(), anyLong(), anyLong(),
                        anyLong()
                    )
                ).thenReturn(testEventData)

                Mockito.`when`(
                    userServiceRepository.deleteEventRecord(
                        anyString(), anyLong()
                    )
                ).thenReturn(true)

                Mockito.`when`(
                    appRepository.deleteEvent(
                        any(EventEntity::class.java)
                    )
                ).thenReturn(1)
                val result = syncEventsToVinciUseCase.syncEventsToVinci()
                Mockito.verify(prefs, Mockito.times(1)).userId
                Mockito.verify(prefs, Mockito.times(1)).token
                Mockito.verify(appRepository, Mockito.times(1)).getEventRecordsForDelete(testUserId)
                Mockito.verify(userServiceRepository, Mockito.times(testDeletionEventsCount))
                    .getEventRecord(
                        anyString(), anyLong(), anyLong(),
                        anyLong()
                    )

                assertNotNull(result)
                assertTrue(result)
            }
        }
    }

    @Test(expected = Exception::class)
    fun `given an invalid userId input when called throw exception`() {
        runBlocking {
            runBlocking {
                val testEventData = getTestUserEventRecords(testDeletionEventsCount)
                Mockito.`when`(prefs.userId).thenReturn(-1L)
                Mockito.`when`(prefs.token).thenReturn(testToken)
                Mockito.`when`(appRepository.getEventRecordsForDelete(testUserId))
                    .thenReturn(testEventData)
                Mockito.`when`(
                    userServiceRepository.getEventRecord(
                        anyString(), anyLong(), anyLong(),
                        anyLong()
                    )
                ).thenReturn(testEventData)

                Mockito.`when`(
                    userServiceRepository.deleteEventRecord(
                        anyString(), anyLong()
                    )
                ).thenReturn(true)

                Mockito.`when`(
                    appRepository.deleteEvent(
                        any(EventEntity::class.java)
                    )
                ).thenReturn(1)
                val result = syncEventsToVinciUseCase.syncEventsToVinci()
                Mockito.verify(prefs, Mockito.times(1)).userId
                Mockito.verify(prefs, Mockito.times(1)).token
                Mockito.verify(appRepository, Mockito.times(1)).getEventRecordsForDelete(testUserId)
                Mockito.verify(userServiceRepository, Mockito.times(testDeletionEventsCount))
                    .getEventRecord(
                        anyString(), anyLong(), anyLong(),
                        anyLong()
                    )

                assertNotNull(result)
                assertTrue(result)
            }
        }
    }

    /**
     * Kotlin null type handling for Mockito.
     */
    private fun <T> any(type: Class<T>): T = Mockito.any(type)
}