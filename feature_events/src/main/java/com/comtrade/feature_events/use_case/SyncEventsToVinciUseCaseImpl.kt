/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: SyncEventsToVinciUseCaseImpl.kt
 * Author: sdelic
 *
 * History:  12.5.22. 11:39 created
 */

package com.comtrade.feature_events.use_case

import com.comtrade.domain.contracts.repository.IAppRepository
import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.contracts.repository.user.UserServiceRepository
import com.comtrade.domain.contracts.usecases.events.ISyncEventsToVinciUseCase
import com.comtrade.domain.entities.event.PostEventRecordRequest
import javax.inject.Inject

/**
 * Implementation detail for synchronizing User events with the Vinci backend use case.
 */
internal class SyncEventsToVinciUseCaseImpl @Inject constructor(
    private val prefs: IPrefs,
    private val appRepository: IAppRepository,
    private val userServiceRepository: UserServiceRepository
) : ISyncEventsToVinciUseCase {
    override suspend fun syncEventsToVinci(): Boolean {

        val userId = prefs.userId
        if (userId < 1) {
            throw Exception("Provided invalid User Id: $userId")
        }
        val token = prefs.token
        if (token.isBlank()) {
            throw Exception("User token not found.")
        }

        return deleteUserEventsMarkedForRemoval(userId, token) && syncEventDataToTheVinciServer(
            userId,
            token
        )
    }


    /**
     * Obtain all of the events marked as deleted and execute delete query API calls.
     * @param userId The User Id associated with the event data.
     * @param token The authentication token for the delete API request.
     */
    private suspend fun deleteUserEventsMarkedForRemoval(
        userId: Long,
        token: String
    ): Boolean {
        val eventRecords =
            appRepository.getEventRecordsForDelete(userId)
        for (eventRecord in eventRecords) {
            val result = userServiceRepository.getEventRecord(
                token,
                eventRecord.id.toLong(),
                eventRecord.created,
                eventRecord.timestamp
            )
            if (result.isNotEmpty()) {
                for (event in result) {
                    val eventDeleteResult =
                        userServiceRepository.deleteEventRecord(
                            token,
                            event.id.toLong()
                        )
                    if (eventDeleteResult) {
                        prefs.addLog("Deleted event: $event")
                        val deleteRowResult =
                            appRepository.deleteEvent(eventRecord)
                        if (deleteRowResult != 1) {
                            return false
                        }
                    } else {
                        return false
                    }
                }
            }
        }
        return true
    }

    /**
     * Obtain all of the events marked as not synced with server and execute create event query API calls.
     * Upon successful API response, update the sync state of the local database events.
     * @param userId The User Id associated with the event data.
     * @param token The authentication token for the create event API request.
     */
    private suspend fun syncEventDataToTheVinciServer(
        userId: Long,
        token: String
    ): Boolean {
        val eventEntities = appRepository.getEventRecordsForSync(userId)
        for (eventEntity in eventEntities) {
            val request = PostEventRecordRequest(
                eventEntity.id.toLong(),
                userId,
                eventEntity.timestamp,
                eventEntity.created,
                eventEntity.notify,
                eventEntity.repeat,
                eventEntity.type,
                eventEntity.title,
                eventEntity.text,
                eventEntity.data
            )

            val createEventResult =
                userServiceRepository.postEventRecord(
                    token,
                    request
                )

            if (createEventResult) {
                prefs.addLog("Send event record successful: $request")
                val updateUserEventResult =
                    appRepository.updateUserEventRecord(eventEntity.copy(syncedWithServer = true))
                if (!updateUserEventResult) {
                    return false
                }
            } else {
                return false
            }
        }
        return true
    }

}