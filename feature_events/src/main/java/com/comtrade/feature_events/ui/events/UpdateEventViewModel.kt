/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: UpdateEventViewModel.kt
 * Author: sdelic
 *
 * History:  19.4.22. 13:01 created
 */

package com.comtrade.feature_events.ui.events

import androidx.lifecycle.*
import com.comtrade.domain.contracts.usecases.events.IUpdateUserEventUseCase
import com.comtrade.domain.entities.event.EventEntity
import com.comtrade.domain_ui.state.RequestUIState
import com.comtrade.feature_events.ui.events.UpdateEventViewModel.UpdateEventRequest.DefaultState
import com.comtrade.feature_events.ui.events.UpdateEventViewModel.UpdateEventRequest.UpdateEventEntity
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

/**
 * Android ViewModel component for updating existing User event.
 */
class UpdateEventViewModel @Inject constructor(private val updateUserEventUseCase: IUpdateUserEventUseCase) :
    ViewModel() {

    private val updateEventEntityRequest = MutableLiveData<UpdateEventRequest>(DefaultState)
    val updateEventEntity: LiveData<RequestUIState<Boolean>> =
        Transformations.switchMap(updateEventEntityRequest) {
            liveData(context = viewModelScope.coroutineContext + Dispatchers.IO) {
                when (it) {
                    DefaultState -> emit(RequestUIState.DefaultState)
                    is UpdateEventEntity -> {
                        emit(RequestUIState.OnLoading)
                        try {
                            val result =
                                updateUserEventUseCase.updateUserEventRecord(it.payload)
                            if (result) {
                                emit(RequestUIState.OnSuccess(true))
                            } else {
                                emit(RequestUIState.OnError(null))
                            }

                        } catch (e: Exception) {
                            e.printStackTrace()
                            emit(RequestUIState.OnError(e.message))
                        }
                    }
                }
            }
        }

    fun updateEventEntity(eventEntity: EventEntity) {
        updateEventEntityRequest.value =
            UpdateEventEntity(eventEntity)
    }

    fun resetState() {
        updateEventEntityRequest.value = DefaultState
    }

    /**
     * Update event record data request states.
     * @property UpdateEventEntity the request state with the request payload.
     * @property DefaultState the reset/default state for an request. Dispatching the event to be handled by some other observing component.
     */
    private sealed class UpdateEventRequest {
        data class UpdateEventEntity(val payload: EventEntity) : UpdateEventRequest()
        object DefaultState : UpdateEventRequest()
    }
}