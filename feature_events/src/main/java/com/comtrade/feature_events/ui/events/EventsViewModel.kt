/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: EventsViewModel.kt
 * Author: sdelic
 *
 * History:  5.4.22. 09:30 created
 */

package com.comtrade.feature_events.ui.events

import androidx.lifecycle.*
import com.comtrade.domain.contracts.usecases.events.IGetUserEventRecordsUseCase
import com.comtrade.feature_events.contracts.events.IEventsViewModel
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

/**
 * Android ViewModel component for Event-related use cases.
 */
class EventsViewModel @Inject constructor(userEventRecordsUseCase: IGetUserEventRecordsUseCase) :
    ViewModel(), IEventsViewModel {

    private val userEventRecordsRequest = MutableLiveData<UserEventRecordsRequest>()
    val eventRecords: LiveData<UserEventRecordsUIState> =
        Transformations.switchMap(userEventRecordsRequest) {

            liveData(context = viewModelScope.coroutineContext + Dispatchers.IO) {
                emit(UserEventRecordsUIState.OnLoading)

                try {
                    val data = userEventRecordsUseCase.getUserEventRecords(it.userId, it.timestamp)
                    if (data.isEmpty()) {
                        emit(UserEventRecordsUIState.OnEmptyDataSet)
                    } else {
                        emit(UserEventRecordsUIState.OnSuccess(data))
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                    emit(UserEventRecordsUIState.OnError(e.message))
                }
            }

        }

    /**
     * Obtain User event records from the database.
     */
    fun getUserEventRecords(userId: Long, timestamp: Long) {
        userEventRecordsRequest.value = UserEventRecordsRequest(userId, timestamp)
    }
}

/**
 * The immutable data holder for the User-related event records request.
 */
internal data class UserEventRecordsRequest(val userId: Long, val timestamp: Long)