/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: SaveEventViewModel.kt
 * Author: sdelic
 *
 * History:  15.4.22. 15:55 created
 */

package com.comtrade.feature_events.ui.events

import androidx.lifecycle.*
import com.comtrade.domain.contracts.usecases.events.IPersistUserEventRecordUseCase
import com.comtrade.domain.entities.event.EventEntity
import com.comtrade.domain_ui.state.RequestUIState
import com.comtrade.feature_events.ui.events.SaveEventViewModel.SaveEventRequest.DefaultState
import com.comtrade.feature_events.ui.events.SaveEventViewModel.SaveEventRequest.SaveEventEntity
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

/**
 * Android ViewModel component for persisting new User event records in the database.
 */
class SaveEventViewModel @Inject constructor(persistUserEventRecordUseCase: IPersistUserEventRecordUseCase) :
    ViewModel() {

    private val storeEventEntityRequest = MutableLiveData<SaveEventRequest>(DefaultState)
    val storeEventEntity: LiveData<RequestUIState<EventEntity>> =
        Transformations.switchMap(storeEventEntityRequest) {
            liveData(context = viewModelScope.coroutineContext + Dispatchers.IO) {
                when (it) {
                    DefaultState -> emit(RequestUIState.DefaultState)
                    is SaveEventEntity -> {
                        emit(RequestUIState.OnLoading)
                        try {
                            val result =
                                persistUserEventRecordUseCase.saveUserEventRecord(it.payload)
                            if (result > 0) {
                                emit(RequestUIState.OnSuccess(it.payload.copy(id = result.toInt())))
                            } else {
                                emit(RequestUIState.OnError(null))
                            }

                        } catch (e: Exception) {
                            e.printStackTrace()
                            emit(RequestUIState.OnError(e.message))
                        }
                    }
                }
            }
        }

    fun storeEventEntity(eventEntity: EventEntity) {
        storeEventEntityRequest.value = SaveEventEntity(eventEntity)
    }

    fun resetState() {
        storeEventEntityRequest.value = DefaultState
    }

    /**
     * Save event record data request states.
     * @property SaveEventEntity the request state with the request payload.
     * @property DefaultState the reset/default state for an request. Dispatching the event to be handled by some other observing component.
     */
    private sealed class SaveEventRequest {
        data class SaveEventEntity(val payload: EventEntity) : SaveEventRequest()
        object DefaultState : SaveEventRequest()
    }
}