/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: EventsModule.kt
 * Author: sdelic
 *
 * History:  4.4.22. 12:55 created
 */

package com.comtrade.feature_events.di

import com.comtrade.domain.contracts.repository.IAppRepository
import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.contracts.repository.user.UserServiceRepository
import com.comtrade.domain.contracts.usecases.events.IGetUserEventRecordsUseCase
import com.comtrade.domain.contracts.usecases.events.IPersistUserEventRecordUseCase
import com.comtrade.domain.contracts.usecases.events.ISyncEventsToVinciUseCase
import com.comtrade.domain.contracts.usecases.events.IUpdateUserEventUseCase
import com.comtrade.feature_events.use_case.GetUserEventRecordsImpl
import com.comtrade.feature_events.use_case.SaveUserEventRecordUseCaseImpl
import com.comtrade.feature_events.use_case.SyncEventsToVinciUseCaseImpl
import com.comtrade.feature_events.use_case.UpdateUserEventUseCaseImpl
import dagger.Module
import dagger.Provides

/**
 * The Dagger DI User events module. Provides implementations for the event-related use cases.
 */
@Module
class EventsModule {

    @Provides
    internal fun provideGetUserEventRecords(appRepository: IAppRepository): IGetUserEventRecordsUseCase =
        GetUserEventRecordsImpl(appRepository)

    @Provides
    internal fun provideSaveUserEventRecords(appRepository: IAppRepository): IPersistUserEventRecordUseCase =
        SaveUserEventRecordUseCaseImpl(appRepository)

    @Provides
    internal fun provideUpdateUserEventRecords(appRepository: IAppRepository): IUpdateUserEventUseCase =
        UpdateUserEventUseCaseImpl(appRepository)

    @Provides
    internal fun provideSyncUserEventsToVinciUseCase(
        prefs: IPrefs,
        appRepository: IAppRepository,
        userServiceRepository: UserServiceRepository
    ): ISyncEventsToVinciUseCase =
        SyncEventsToVinciUseCaseImpl(prefs, appRepository, userServiceRepository)
}