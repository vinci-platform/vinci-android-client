/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: IAccountRepository.kt
 * Author: sdelic
 *
 * History:  7.4.22. 15:43 created
 */

package com.comtrade.domain.contracts.repository.user

import com.comtrade.domain.entities.auth.UserAuthenticateResponseEntity
import com.comtrade.domain.entities.user.account.*
import com.comtrade.domain.entities.user.data.UpdateUserRequest

/**
 * The account repository domain contract.
 */
interface IAccountRepository {

    suspend fun authenticateUser(
        username: String,
        password: String,
        rememberMe: Boolean
    ): UserAuthenticateResponseEntity

    suspend fun resetPassword(token: String, email: String): Boolean

    suspend fun resetPasswordFinish(resetPasswordFinishRequest: ResetPasswordFinishRequest): Boolean

    suspend fun changePassword(
        token: String,
        changePasswordRequestEntity: ChangePasswordRequestEntity
    ): Boolean

    suspend fun updateAccount(token: String, updateUserRequest: UpdateUserRequest): Boolean

    suspend fun getUser(token: String): GetUserEntity

    suspend fun getUserExtra(token: String, id: Long): GetUserExtraEntity

    suspend fun createUser(login: String, email: String, password: String, role: String): Boolean

    suspend fun getMobileAppSettings(token: String, userId: Long = 0): List<MobileAppSettingsEntity>

}