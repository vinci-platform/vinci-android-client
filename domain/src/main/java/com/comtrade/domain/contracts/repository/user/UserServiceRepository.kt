/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: UserServiceRepository.kt
 * Author: sdelic
 *
 * History:  22.3.22. 14:56 created
 */

package com.comtrade.domain.contracts.repository.user

import com.comtrade.domain.entities.devices.data.*
import com.comtrade.domain.entities.devices.fitbit.GetFitBitUrlEntity
import com.comtrade.domain.entities.event.EventEntity
import com.comtrade.domain.entities.event.PostEventRecordRequest
import com.comtrade.domain.entities.survey.PostSurveyDataRequest
import com.comtrade.domain.entities.survey.SurveyDataEntity
import com.comtrade.domain.entities.user.account.GetUserEntity
import com.comtrade.domain.entities.user.account.GetUserExtraEntity
import com.comtrade.domain.entities.user.data.GetAllUserPhonesEntity
import com.comtrade.domain.entities.user.data.GetUserDevicesEntity
import com.comtrade.domain.entities.user.data.PostHealthDataRequest
import java.io.File

interface UserServiceRepository {

    suspend fun getUser(token: String): GetUserEntity

    suspend fun getUserExtra(token: String, id: Long): GetUserExtraEntity

    suspend fun getAllUserPhones(token: String): List<GetAllUserPhonesEntity>

    suspend fun updateUserImage(token: String, account: String, imageId: Long, file: File): Boolean

    suspend fun addDevice(token: String, addDeviceRequest: AddDeviceRequest): AddDeviceResponse?

    suspend fun updateDevice(token: String, updateDeviceRequest: UpdateDeviceRequest): Boolean

    suspend fun getUserDevices(token: String, login: String): List<GetUserDevicesEntity>

    suspend fun updateShoeData(token: String, updateShoeDataRequest: UpdateShoeDataRequest): Boolean

    suspend fun getSurveyData(
        token: String,
        deviceId: Long,
        fromCreatedTime: String
    ): List<SurveyDataEntity>

    suspend fun postSurveyData(
        token: String,
        userId: Long,
        postSurveyDataRequest: PostSurveyDataRequest
    ): Boolean

    suspend fun getCmdWatchData(
        token: String,
        getCmdWatchDataRequest: GetCmdWatchDataRequest
    ): GetCmdWatchDataEntity

    suspend fun postHealthData(token: String, postHealthDataRequest: PostHealthDataRequest): Boolean

    suspend fun getEventRecord(
        token: String,
        appId: Long,
        created: Long,
        timestamp: Long
    ): List<EventEntity>

    suspend fun postEventRecord(
        token: String,
        postEventRecordRequest: PostEventRecordRequest
    ): Boolean

    suspend fun deleteEventRecord(token: String, id: Long): Boolean

    suspend fun getFitbitURL(token: String, userExtraId: Long): GetFitBitUrlEntity
}