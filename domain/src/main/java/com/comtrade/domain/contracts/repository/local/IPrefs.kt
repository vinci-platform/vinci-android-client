package com.comtrade.domain.contracts.repository.local

/**
 * Local storage contract.
 */
interface IPrefs : IAuthPrefs, IUserAccountPrefs, IEmergencyNumberPrefs, INetworkPrefs,
    IDevicePrefs, INotificationPrefs {

    var isFirstRun: Boolean

    var showHints: Boolean

    var isQuestionnaireStarted: Boolean

    var isNotificationSet: Boolean

}