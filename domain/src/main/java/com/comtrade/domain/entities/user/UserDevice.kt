package com.comtrade.domain.entities.user

import com.comtrade.domain.entities.user.data.GetUserDevicesEntity

open class UserDevice(device: GetUserDevicesEntity?) {
    var deviceName: String = ""
    var deviceUUID: String = ""
    var description: String = ""
    var deviceId: Long = -1
    var active: Boolean = false

    init {
        if (device != null) {
            deviceName = device.name ?: ""
            deviceUUID = device.uuid ?: ""
            description = device.description ?: ""
            deviceId = device.deviceId ?: -1
            active = device.active
        }
    }
}