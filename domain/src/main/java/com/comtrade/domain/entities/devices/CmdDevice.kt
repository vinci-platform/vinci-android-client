package com.comtrade.domain.entities.devices

import com.comtrade.domain.entities.user.UserDevice
import com.comtrade.domain.entities.user.data.GetUserDevicesEntity

class CmdDevice(device: GetUserDevicesEntity) : UserDevice(device)