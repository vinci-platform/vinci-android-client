/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: DependencyLibraryRepoImplTest.kt
 * Author: sdelic
 *
 * History:  30.3.22. 16:05 created
 */

package com.comtrade.feature_about_dependencies.data

import com.comtrade.domain.entities.legal.DependencyInfo
import com.comtrade.domain.entities.legal.LicenseInfo
import com.comtrade.feature_about_dependencies.contracts.IDataParser
import com.comtrade.feature_about_dependencies.di.AboutDependenciesTestComponent
import com.comtrade.feature_about_dependencies.di.DaggerAboutDependenciesTestComponent
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito

/**
 * Test scenario for the obtain dependency licenses repository implementation.
 * Asserting that the correct response is returned.
 */
internal class DependencyLibraryRepoImplTest {

    private lateinit var dependencyLibraryRepoImpl: DependencyLibraryRepoImpl
    private lateinit var mockedDataParser: IDataParser

    @Before
    fun setUp() {

        val aboutDependenciesTestComponent: AboutDependenciesTestComponent =
            DaggerAboutDependenciesTestComponent.builder().build()
        mockedDataParser = aboutDependenciesTestComponent.provideDataParser()
        dependencyLibraryRepoImpl =
            DependencyLibraryRepoImpl(mockedDataParser)
    }

    @Test
    fun `when invoked then return list is not null`() {
        //Null check
        assertNotNull(dependencyLibraryRepoImpl)

        runBlocking {
            val actualResponse = dependencyLibraryRepoImpl.getDependencyInfo()
            assertNotNull(actualResponse)
        }
    }

    @Test
    fun `when invoked then return mock list of items`() {

        //Null check
        assertNotNull(dependencyLibraryRepoImpl)

        runBlocking {
            Mockito.`when`(mockedDataParser.getDependencyInfoList())
                .thenReturn(getTestDependencyInfoData())

            val actualResponse = dependencyLibraryRepoImpl.getDependencyInfo()
            assertNotNull(actualResponse)
            Assert.assertNotEquals(0, actualResponse.size)
            Assert.assertEquals(100, actualResponse.size)
        }
    }

    /**
     * Fake test data.
     */
    private fun getTestDependencyInfoData(): List<DependencyInfo> {
        val testData = mutableListOf<DependencyInfo>()
        for (i in 0 until 100) {
            testData.add(
                DependencyInfo(
                    "Test name $i",
                    "Test url $i",
                    "Test description $i",
                    listOf(
                        LicenseInfo(
                            "Test license information $i",
                            "Test license description $i",
                            "$i",
                            "Test license url $i",
                        )
                    )
                )
            )
        }

        return testData
    }
}