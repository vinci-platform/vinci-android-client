/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: AboutDependenciesViewModel.kt
 * Author: sdelic
 *
 * History:  31.3.22. 14:53 created
 */

package com.comtrade.feature_about_dependencies.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.comtrade.domain.contracts.usecases.legal.IDependencyLicenses
import com.comtrade.feature_about_dependencies.contracts.IAboutDependenciesViewModel
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

/**
 * The ViewModel component implementation. Exposes an observable Livedata component for the UI component to consume.
 */
class AboutDependenciesViewModel @Inject constructor(private val dependencyLicenses: IDependencyLicenses) :
    ViewModel(), IAboutDependenciesViewModel {

    val dependencyInformation: LiveData<AboutDependenciesUIState> =
        liveData(context = viewModelScope.coroutineContext + Dispatchers.IO) {
            emit(AboutDependenciesUIState.OnLoading)

            try {
                val data = dependencyLicenses()
                if (data.isEmpty()) {
                    emit(AboutDependenciesUIState.OnEmptyDataSet)
                } else {
                    emit(AboutDependenciesUIState.OnSuccess(data))
                }
            } catch (e: Exception) {
                e.printStackTrace()
                emit(AboutDependenciesUIState.OnError(e.message))
            }
        }
}