/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: AboutDependenciesModule.kt
 * Author: sdelic
 *
 * History:  30.3.22. 16:08 created
 */

package com.comtrade.feature_about_dependencies.di

import android.app.Application
import com.comtrade.domain.contracts.usecases.legal.IDependencyLicenses
import com.comtrade.feature_about_dependencies.R
import com.comtrade.feature_about_dependencies.contracts.IDataParser
import com.comtrade.feature_about_dependencies.contracts.IDependencyLibraryRepo
import com.comtrade.feature_about_dependencies.data.DependencyLibraryRepoImpl
import com.comtrade.feature_about_dependencies.framework.AboutLibrariesDataParserImpl
import com.comtrade.feature_about_dependencies.usecase.DependencyLicensesUseCaseImpl
import dagger.Module
import dagger.Provides

/**
 * Dagger module for providing the actual implementations of the required feature dependencies.
 */
@Module
class AboutDependenciesModule(private val application: Application) {

    @Provides
    internal fun provideDataParser(): IDataParser = AboutLibrariesDataParserImpl(
        application.resources.openRawResource(
            R.raw.aboutlibraries
        )
    )

    @Provides
    internal fun provideDependencyRepo(dataParser: IDataParser): IDependencyLibraryRepo =
        DependencyLibraryRepoImpl(dataParser)

    @Provides
    internal fun provideDependencyLicensesUseCase(dependencyLibraryRepo: IDependencyLibraryRepo): IDependencyLicenses =
        DependencyLicensesUseCaseImpl(dependencyLibraryRepo)

}