/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: JSONFileReader.kt
 * Author: sdelic
 *
 * History:  31.3.22. 11:10 created
 */

package com.comtrade.feature_about_dependencies.framework.util

import java.io.InputStream
import java.io.InputStreamReader

/**
 * Helper class for reading Json files.
 */
internal class JsonFileReader {

    /**
     * Creates a new input stream read from the resource directory and returns the file content
     * or empty String if file not found.
     * @param fileInputStream The resource file path
     */
    fun readFileContentFromPath(fileInputStream: InputStream?): String {
        return try {
            val inputStreamReader =
                InputStreamReader(fileInputStream)
            inputStreamReader.readText()
        } catch (exception: Exception) {
            exception.printStackTrace()
            ""
        }
    }

    /**
     * Creates a new input stream read from the resource directory and returns the file content
     * or empty String if file not found.
     * @param filePath The resource file path
     */
    fun readFileFromResource(filePath: String): String {
        return try {
            val inputStreamReader =
                InputStreamReader(this.javaClass.classLoader?.getResourceAsStream(filePath))
            inputStreamReader.readText()
        } catch (exception: Exception) {
            exception.printStackTrace()
            ""
        }
    }
}