/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: AboutLibrariesDataParser.kt
 * Author: sdelic
 *
 * History:  31.3.22. 13:54 created
 */

package com.comtrade.feature_about_dependencies.framework

import com.comtrade.domain.entities.legal.DependencyInfo
import com.comtrade.domain.entities.legal.LicenseInfo
import com.comtrade.feature_about_dependencies.contracts.IDataParser
import com.comtrade.feature_about_dependencies.framework.util.JsonFileReader
import com.mikepenz.aboutlibraries.Libs
import com.mikepenz.aboutlibraries.entity.License
import java.io.InputStream

/**
 * Implementation detail for the about libraries plugin data parsing.
 * @param rawFileInputStream The json raw file input stream obtained by the UI client component.
 */
internal class AboutLibrariesDataParserImpl(private val rawFileInputStream: InputStream?) :
    IDataParser {

    override suspend fun getDependencyInfoList(): List<DependencyInfo> {
        val jsonFileReader = JsonFileReader()
        val jsonString = jsonFileReader.readFileContentFromPath(rawFileInputStream)
        return if (jsonString.isNotBlank()) {
            readAboutData(jsonString)
        } else {
            emptyList()
        }
    }

    /**
     * Maps the resolved Json data String for the dependency information
     * into a list of consumable data.
     */
    private fun readAboutData(jsonData: String): List<DependencyInfo> {
        val libs = Libs.Builder().withJson(jsonData).build()
        return libs.libraries.map {
            DependencyInfo(
                it.name,
                it.website ?: "",
                it.description ?: "",
                it.licenses.map { license ->
                    license.toLicenseInfo()
                }
            )
        }
    }
}

private fun License.toLicenseInfo(): LicenseInfo =
    LicenseInfo(
        this.name,
        this.licenseContent ?: "",
        this.year ?: "",
        this.url ?: ""
    )
