package com.comtrade.util

import java.io.InputStreamReader

/**
 * Helper class for reading the fake API response files for testing.
 */
internal class MockAPIResponseFileReader {
    /**
     * Creates a new input stream read from the resource directory and returns the file content
     * or empty String if file not found.
     * @param filePath The resource file path
     */
    internal fun readFileFromResource(filePath: String): String {
        return try {
            val inputStreamReader =
                InputStreamReader(this.javaClass.classLoader?.getResourceAsStream(filePath))
            inputStreamReader.readText()
        } catch (exception: Exception) {
            exception.printStackTrace()
            ""
        }
    }
}
