/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: APITestWebServerImpl.kt
 * Author: sdelic
 *
 * History:  23.3.22. 16:41 created
 */

package com.comtrade.data.repository.api.core

import com.comtrade.data.repository.api.contract.IAPITestWebServer
import com.comtrade.domain.contracts.repository.user.IAccountRepository
import com.comtrade.domain.contracts.repository.user.IDeviceRepository
import com.comtrade.domain.contracts.repository.user.UserServiceRepository
import com.google.gson.Gson
import okhttp3.mockwebserver.MockWebServer
import javax.inject.Inject

/**
 * The implementation for the test web server component.
 * Provides the needed components for each of the API tests in this module.
 */
internal class APITestWebServerImpl @Inject constructor(
    private val rootFilesDir: String,
    private val mockWebServer: MockWebServer,
    private val userServiceRepository: UserServiceRepository,
    private val accountRepository: IAccountRepository,
    private val deviceRepository: IDeviceRepository,
    private val gson: Gson
) : IAPITestWebServer {
    override fun gson(): Gson = gson

    override fun mockWebServer(): MockWebServer = mockWebServer

    override fun rootFilesDir(): String = rootFilesDir

    override fun userServiceRepository(): UserServiceRepository = userServiceRepository
    override fun accountRepository(): IAccountRepository = accountRepository
    override fun deviceRepository(): IDeviceRepository = deviceRepository
}