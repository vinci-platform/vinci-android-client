/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: APIGetUserExtrasByIdTest.kt
 * Author: sdelic
 *
 * History:  23.3.22. 16:41 created
 */

package com.comtrade.data.repository.api.user

import com.comtrade.data.network.models.user.GetUserExtraResponse
import com.comtrade.data.network.models.user.toDomainEntity
import com.comtrade.data.repository.api.core.APIBaseTest
import com.comtrade.util.MockAPIResponseFileReader
import com.google.gson.Gson
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockResponse
import org.junit.Assert.*
import org.junit.Test
import retrofit2.HttpException
import java.io.EOFException
import java.net.HttpURLConnection

/**
 * API test for the @see api/user-extras/by-userId/{id} endpoint.
 * Web server mocked using MockWebServer.
 */
@ExperimentalCoroutinesApi
internal class APIGetUserExtrasByIdTest : APIBaseTest() {

    private val apiEndpoint = "/api/user-extras/by-userId/"

    @Test
    fun `get user extras by id 401 unauthorized due to missing authorization header`() {
        mockWebServer.enqueue(MockResponse().setResponseCode(HttpURLConnection.HTTP_UNAUTHORIZED))
        val userIdParam: Long = -100 // Some non-existing User Id

        runBlocking {
            try {
                userServiceRepository.getUserExtra("", userIdParam)
            } catch (e: HttpException) {
                assertEquals(HttpURLConnection.HTTP_UNAUTHORIZED, e.code())
            }

            // Assert correct api call has been made
            val request = mockWebServer.takeRequest()
            assertEquals("$apiEndpoint$userIdParam", request.path)

            //Assert authorization header is sent
            assertNotNull(request.getHeader("Authorization"))
        }
    }

    @Test
    fun `get user extras by passing a non-existing id value returns 400 bad request`() {
        mockWebServer.enqueue(MockResponse().setResponseCode(HttpURLConnection.HTTP_BAD_REQUEST))
        val userIdParam: Long = -100

        runBlocking {
            try {
                userServiceRepository.getUserExtra("", userIdParam)
            } catch (e: HttpException) {
                assertEquals(HttpURLConnection.HTTP_BAD_REQUEST, e.code())
            }

            // Assert correct api call has been made
            val request = mockWebServer.takeRequest()
            assertEquals("$apiEndpoint$userIdParam", request.path)

            // Assert authorization header is sent
            assertNotNull(request.getHeader("Authorization"))
        }
    }

    /**
     * NOTE: This is not to recommended way to handle the User state,
     * but the time of writing this test, that was the server error handling state.
     */
    @Test
    fun `get user extras by passing a non-existing id value returns 200 OK empty response`() {
        mockWebServer.enqueue(MockResponse().setResponseCode(HttpURLConnection.HTTP_OK))
        val userIdParam: Long = -100

        assertThrows(EOFException::class.java) {
            runBlocking {
                userServiceRepository.getUserExtra("", userIdParam)

                // Assert correct api call has been made
                val request = mockWebServer.takeRequest()
                assertEquals("$apiEndpoint$userIdParam", request.path)

                // Assert authorization header is sent
                assertNotNull(request.getHeader("Authorization"))
            }
        }
    }

    @Test
    fun `get user extras returns 200 OK`() {
        val rawResponseData = MockAPIResponseFileReader()
            .readFileFromResource("$rootTestFilesDir/account_extras_response.json")
        val userIdParam: Long = 15503

        mockWebServer.enqueue(
            MockResponse()
                .setBody(rawResponseData)
                .setResponseCode(HttpURLConnection.HTTP_OK)
        )

        runBlocking {
            val actualResponse = userServiceRepository.getUserExtra("", userIdParam)

            // Assert correct api call has been made
            val request = mockWebServer.takeRequest()
            assertEquals("$apiEndpoint$userIdParam", request.path)

            // Assert authorization header is sent
            assertNotNull(request.getHeader("Authorization"))

            val expectedResponse = Gson().fromJson(
                rawResponseData,
                GetUserExtraResponse::class.java
            ).toDomainEntity()
            assertEquals(expectedResponse, actualResponse)
        }

    }
}