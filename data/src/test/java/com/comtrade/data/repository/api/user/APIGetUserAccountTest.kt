/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: APIGetUserAccountTest.kt
 * Author: sdelic
 *
 * History:  23.3.22. 16:41 created
 */

package com.comtrade.data.repository.api.user

import com.comtrade.data.network.models.user.GetUserResponse
import com.comtrade.data.network.models.user.toDomainEntity
import com.comtrade.data.repository.api.core.APIBaseTest
import com.comtrade.util.MockAPIResponseFileReader
import com.google.gson.Gson
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockResponse
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Test
import retrofit2.HttpException
import java.net.HttpURLConnection

/**
 * API test for the @see api/account endpoint.
 * Web server mocked using MockWebServer.
 */
@ExperimentalCoroutinesApi
internal class APIGetUserAccountTest : APIBaseTest() {

    @Test
    fun `get account returns 401 unauthorized due to missing authorization header`() {
        mockWebServer.enqueue(MockResponse().setResponseCode(HttpURLConnection.HTTP_UNAUTHORIZED))

        runBlocking {
            try {
                userServiceRepository.getUser("")
            } catch (e: HttpException) {
                assertEquals(HttpURLConnection.HTTP_UNAUTHORIZED, e.code())
            }

            // Assert correct api call has been made
            val request = mockWebServer.takeRequest()
            assertEquals("/api/account", request.path)

            //Assert authorization header is sent
            assertNotNull(request.getHeader("Authorization"))
        }
    }

    @Test
    fun `get user account returns 200`() {
        val rawResponseData = MockAPIResponseFileReader()
            .readFileFromResource("$rootTestFilesDir/account_response.json")

        mockWebServer.enqueue(
            MockResponse()
                .setBody(
                    rawResponseData
                ).setResponseCode(HttpURLConnection.HTTP_OK)
        )

        runBlocking {
            val actualResponse = userServiceRepository.getUser("")

            // Assert correct api call has been made
            val request = mockWebServer.takeRequest()
            assertEquals("/api/account", request.path)

            // Assert authorization header is sent
            assertNotNull(request.getHeader("Authorization"))
            val expectedResponse =
                Gson().fromJson(rawResponseData, GetUserResponse::class.java).toDomainEntity()
            assertNotNull(actualResponse)
            assertEquals(expectedResponse, actualResponse)
        }
    }
}