/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: APIUpdateUserAccount.kt
 * Author: sdelic
 *
 * History:  23.3.22. 16:41 created
 */

package com.comtrade.data.repository.api.user

import com.comtrade.data.BuildConfig
import com.comtrade.data.network.models.user.GetUserResponse
import com.comtrade.data.network.models.user.toDomainEntity
import com.comtrade.data.repository.api.core.APIBaseTest
import com.comtrade.domain.entities.user.data.UpdateUserRequest
import com.comtrade.util.MockAPIResponseFileReader
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockResponse
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Test
import java.net.HttpURLConnection

/**
 * API test for the @see POST api/account endpoint.
 * Web server mocked using MockWebServer.
 */
@ExperimentalCoroutinesApi
internal class APIUpdateUserAccount : APIBaseTest() {

    private val apiEndpoint = "/api/account"

    @Test
    fun `update user profile returns 400 bad request for non-existing id`() {
        mockWebServer.enqueue(MockResponse().setResponseCode(HttpURLConnection.HTTP_BAD_REQUEST))
        val rawRequestData =
            MockAPIResponseFileReader().readFileFromResource("$rootTestFilesDir/account_update_valid_request.json")
        val updateUserRequest =
            gson.fromJson(rawRequestData, UpdateUserRequest::class.java)
                .copy(id = null)

        updateUserRequest(updateUserRequest)

    }

    @Test
    fun `update user profile returns 400 bad request for missing fields`() {
        mockWebServer.enqueue(MockResponse().setResponseCode(HttpURLConnection.HTTP_BAD_REQUEST))
        val rawRequestData =
            MockAPIResponseFileReader().readFileFromResource("$rootTestFilesDir/account_update_valid_request.json")
        val updateUserRequest =
            gson.fromJson(rawRequestData, UpdateUserRequest::class.java)
                .copy(
                    address = null,
                    login = null,
                    firstName = null,
                    role = null,
                    email = null,
                    phone = null,
                    friends = null
                )

        updateUserRequest(updateUserRequest)
    }

    @Suppress("DEPRECATION")
    @Test
    fun `update user profile returns 500 server error when problem occurs `() {
        // The server supports the deprecated general 500 server error
        mockWebServer.enqueue(MockResponse().setResponseCode(HttpURLConnection.HTTP_SERVER_ERROR))
        val rawRequestData =
            MockAPIResponseFileReader().readFileFromResource("$rootTestFilesDir/account_update_valid_request.json")
        val updateUserRequest =
            gson.fromJson(rawRequestData, UpdateUserRequest::class.java)
                .copy(
                    address = "SomeAddress",
                    login = BuildConfig.AUTH_TEST_USERNAME,
                    firstName = "UserNameUpdate",
                    lastName = "UserLastNameUpdate",
                    gender = "Male",
                    phone = "123456789",
                )

        updateUserRequest(updateUserRequest)
    }


    @Test
    fun `update user profile returns 200 Ok and user data updated`() {
        mockWebServer.enqueue(MockResponse().setResponseCode(HttpURLConnection.HTTP_OK))
        val rawRequestData =
            MockAPIResponseFileReader().readFileFromResource("$rootTestFilesDir/account_update_valid_request.json")
        val updateUserRequest =
            gson.fromJson(rawRequestData, UpdateUserRequest::class.java)
                .copy(
                    address = "SomeAddress",
                    login = BuildConfig.AUTH_TEST_USERNAME,
                    firstName = "UserNameUpdate",
                    lastName = "UserLastNameUpdate",
                    gender = "Male",
                    phone = "123456789",
                )

        // Send update request
        updateUserRequest(updateUserRequest)

        // Send get user data request
        getUpdatedUserData(updateUserRequest)

    }

    /**
     * Assert the common state for sending bad user update request data.
     */
    private fun updateUserRequest(updateUserRequest: UpdateUserRequest) =
        runBlocking {
            val response = accountRepository.updateAccount("", updateUserRequest)

            val request = mockWebServer.takeRequest()

            // Assert correct api call has been made
            assertEquals(
                "POST $apiEndpoint HTTP/1.1",
                request.requestLine
            )

            // Assert API method call and header
            assertEquals("POST", request.method)
            assertNotNull(request.getHeader("Authorization"))

            // Assert response
            assertNotNull(response)
        }

    /**
     * Assert updated User data server obtain.
     */
    private fun getUpdatedUserData(updateUserRequest: UpdateUserRequest) {
        //Retrieve the updated User account
        val updatedUserResponse = GetUserResponse(
            userId = updateUserRequest.id,
            login = updateUserRequest.login,
            firstName = updateUserRequest.firstName,
            lastName = updateUserRequest.lastName,
            gender = updateUserRequest.gender,
            email = updateUserRequest.email,
            address = updateUserRequest.address,
            phone = updateUserRequest.phone,
            role = updateUserRequest.role,
            devices = emptyList()
        )
        val rawResponse = gson.toJson(
            updatedUserResponse
        )
        mockWebServer.enqueue(
            MockResponse().setBody(rawResponse).setResponseCode(HttpURLConnection.HTTP_OK)
        )

        runBlocking {
            val actualResponse = userServiceRepository.getUser("")

            // Assert correct api call has been made
            val request = mockWebServer.takeRequest()
            assertEquals(apiEndpoint, request.path)

            val expectedResult = updatedUserResponse.toDomainEntity()
            assertNotNull(expectedResult)
            // Assert authorization header is sent
            assertNotNull(request.getHeader("Authorization"))
            assertEquals(expectedResult, actualResponse)
        }
    }

}