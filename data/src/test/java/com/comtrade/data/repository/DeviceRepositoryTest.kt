/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: DeviceRepositoryTest.kt
 * Author: sdelic
 *
 * History:  29.4.22. 14:01 created
 */

package com.comtrade.data.repository

import com.comtrade.data.network.api.UserService
import com.comtrade.domain.contracts.repository.user.IDeviceRepository
import kotlinx.coroutines.runBlocking
import okhttp3.ResponseBody.Companion.toResponseBody
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mockito
import retrofit2.Response
import java.net.HttpURLConnection

/**
 * Device repository test suite.
 */
internal class DeviceRepositoryTest {
    private val testDeviceId = 1L
    private val testToken = "testToken"


    private lateinit var userService: UserService
    private lateinit var deviceRepository: IDeviceRepository

    @Before
    fun setUp() {
        userService = Mockito.mock(UserService::class.java)
        deviceRepository = DeviceRepository(userService)
    }

    @Test
    fun `given valid device Id when delete user device return true`() {
        runBlocking {
            Mockito.`when`(userService.deleteUserDevice(anyString(), anyString())).thenReturn(
                Response.success(null)
            )
            val result = deviceRepository.deleteUserDevice(testToken, testDeviceId)
            Mockito.verify(userService, Mockito.times(1))
                .deleteUserDevice("Bearer $testToken", testDeviceId.toString())
            assertNotNull(result)
            assertTrue(result)
        }
    }

    @Test
    fun `given invalid token when delete user device return false`() {
        runBlocking {
            Mockito.`when`(userService.deleteUserDevice(anyString(), anyString())).thenReturn(
                Response.error(
                    HttpURLConnection.HTTP_UNAUTHORIZED,
                    "response data".toResponseBody()
                )
            )
            val result = deviceRepository.deleteUserDevice("", testDeviceId)
            Mockito.verify(userService, Mockito.times(1))
                .deleteUserDevice("Bearer ", testDeviceId.toString())
            assertNotNull(result)
            assertFalse(result)
        }
    }

    @Test
    fun `given valid device Id when delete user device return false`() {
        runBlocking {
            Mockito.`when`(userService.deleteUserDevice(anyString(), anyString())).thenReturn(
                Response.error(
                    HttpURLConnection.HTTP_NOT_FOUND,
                    "response data".toResponseBody()
                )
            )
            val result = deviceRepository.deleteUserDevice(testToken, testDeviceId)
            Mockito.verify(userService, Mockito.times(1))
                .deleteUserDevice("Bearer $testToken", testDeviceId.toString())
            assertNotNull(result)
            assertFalse(result)
        }
    }
}