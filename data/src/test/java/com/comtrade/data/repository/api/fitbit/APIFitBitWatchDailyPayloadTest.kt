/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: APIFitBitWatchDailyPayloadTest.kt
 * Author: sdelic
 *
 * History:  26.5.22. 13:12 created
 */

package com.comtrade.data.repository.api.fitbit

import com.comtrade.data.network.models.fitbit.FitBitDailyPayloadDataResponse
import com.comtrade.data.network.models.fitbit.toDomainEntity
import com.comtrade.data.repository.api.core.APIBaseTest
import com.comtrade.util.MockAPIResponseFileReader
import com.google.gson.Gson
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockResponse
import org.junit.Assert
import org.junit.Test
import retrofit2.HttpException
import java.net.HttpURLConnection

/**
 * API test for the { @see ioserver/api/fitbit-watch-intraday-data/payload endpoint} .
 * Web server mocked using MockWebServer.
 */
@ExperimentalCoroutinesApi
internal class APIFitBitWatchDailyPayloadTest : APIBaseTest() {

    private val apiEndpoint = "/ioserver/api/fitbit-watch-intraday-data/payload"
    private val testToken = "some auth token"
    private val testDeviceId = 1L
    private val testTimeStamp = 0L

    @Test
    fun `given an wrong input token when invoked returns 401 unauthorized due to missing authorization header`() {
        mockWebServer.enqueue(MockResponse().setResponseCode(HttpURLConnection.HTTP_UNAUTHORIZED))

        runBlocking {
            try {
                deviceRepository.getFitbitWatchDailyData(
                    "",
                    testDeviceId,
                    testTimeStamp
                )
            } catch (e: HttpException) {
                Assert.assertEquals(HttpURLConnection.HTTP_UNAUTHORIZED, e.code())
            }

            // Assert correct api call has been made
            val request = mockWebServer.takeRequest()
            Assert.assertEquals(
                "$apiEndpoint?deviceId.equals=$testDeviceId&timestamp.greaterOrEqualThan=$testTimeStamp",
                request.path
            )

            //Assert authorization header is sent
            Assert.assertNotNull(request.getHeader(AUTHORIZATION_HEADER))
        }
    }

    @Test
    fun `given an invalid input device Id when invoked returns 200 ok with empty response`() {
        mockWebServer.enqueue(MockResponse().setResponseCode(HttpURLConnection.HTTP_OK))

        runBlocking {
            val invalidTestDeviceId = -1L
            val actualResponse = deviceRepository.getFitbitWatchDailyData(
                testToken,
                invalidTestDeviceId,
                testTimeStamp
            )

            // Assert correct api call has been made
            val request = mockWebServer.takeRequest()
            Assert.assertEquals(
                "$apiEndpoint?deviceId.equals=$invalidTestDeviceId&timestamp.greaterOrEqualThan=$testTimeStamp",
                request.path
            )

            //Assert authorization header is sent
            Assert.assertNotNull(request.getHeader(AUTHORIZATION_HEADER))

            Assert.assertNotNull(actualResponse)
        }
    }

    @Test
    fun `given an invalid timestamp when invoked returns returns the complete list of FitBit data`() {

        val rawResponseData = MockAPIResponseFileReader()
            .readFileFromResource("$rootTestFilesDir/fitbit_watch_daily_payload_response.json")

        mockWebServer.enqueue(
            MockResponse().setBody(
                rawResponseData
            ).setResponseCode(HttpURLConnection.HTTP_OK)
        )

        runBlocking {

            val invalidTimeStamp = -1L
            val actualResponse = deviceRepository.getFitbitWatchDailyData(
                testToken,
                testDeviceId,
                invalidTimeStamp
            )

            // Assert correct api call has been made
            val request = mockWebServer.takeRequest()
            Assert.assertEquals(
                "$apiEndpoint?deviceId.equals=$testDeviceId&timestamp.greaterOrEqualThan=$invalidTimeStamp",
                request.path
            )

            //Assert authorization header is sent
            Assert.assertNotNull(request.getHeader(AUTHORIZATION_HEADER))

            Assert.assertNotNull(actualResponse)

            val expectedResponseRaw = Gson().fromJson(
                rawResponseData,
                Array<FitBitDailyPayloadDataResponse>::class.java
            )

            val expectedResponseMapped = expectedResponseRaw.map { it.toDomainEntity() }

            Assert.assertEquals(expectedResponseMapped, actualResponse)
        }
    }

    @Test
    fun `given a valid input data when invoked returns returns the complete list of FitBit data`() {

        val rawResponseData = MockAPIResponseFileReader()
            .readFileFromResource("$rootTestFilesDir/fitbit_watch_daily_payload_response.json")

        mockWebServer.enqueue(
            MockResponse().setBody(
                rawResponseData
            ).setResponseCode(HttpURLConnection.HTTP_OK)
        )

        runBlocking {

            val actualResponse = deviceRepository.getFitbitWatchDailyData(
                testToken,
                testDeviceId,
                testTimeStamp
            )

            // Assert correct api call has been made
            val request = mockWebServer.takeRequest()
            Assert.assertEquals(
                "$apiEndpoint?deviceId.equals=$testDeviceId&timestamp.greaterOrEqualThan=$testTimeStamp",
                request.path
            )

            //Assert authorization header is sent
            Assert.assertNotNull(request.getHeader(AUTHORIZATION_HEADER))

            Assert.assertNotNull(actualResponse)

            val expectedResponseRaw = Gson().fromJson(
                rawResponseData,
                Array<FitBitDailyPayloadDataResponse>::class.java
            )

            val expectedResponseMapped = expectedResponseRaw.map { it.toDomainEntity() }

            Assert.assertEquals(expectedResponseMapped, actualResponse)
        }
    }
}