/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: APIRegisterTest.kt
 * Author: sdelic
 *
 * History:  23.3.22. 16:41 created
 */

package com.comtrade.data.repository.api.auth

import com.comtrade.data.repository.api.core.APIBaseTest
import com.comtrade.domain.entities.user.UserRoles
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockResponse
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Test
import java.net.HttpURLConnection

/**
 * API test for the @see api/register endpoint.
 * Web server mocked using MockWebServer.
 */
@ExperimentalCoroutinesApi
internal class APIRegisterTest : APIBaseTest() {
    //Register new User params
    private val username = "testUsername"
    private val password = "testPassword"
    private val email = "test@test.com"
    private val role = UserRoles.ROLE_PACIENT.name

    @Test
    fun `register new user has no valid password returns 400`() {
        mockWebServer.enqueue(MockResponse().setResponseCode(HttpURLConnection.HTTP_BAD_REQUEST))

        runBlocking {
            //Not passing the required password parameter.
            val actualResponse = accountRepository.createUser(
                username, email, "", role
            )

            // Assert correct api call has been made
            val request = mockWebServer.takeRequest()
            assertEquals("/api/register", request.path)

            // Assert correct method called
            assertEquals("POST", request.method)

            assertNotNull(actualResponse)
        }
    }

    @Test
    fun `register new user has no valid role returns 500`() {
        mockWebServer.enqueue(MockResponse().setResponseCode(500))

        runBlocking {
            // Not passing the required role parameter.
            val actualResponse = accountRepository.createUser(
                username, email, password, role.plus("_test")
            )

            // Assert correct api call has been made
            val request = mockWebServer.takeRequest()
            assertEquals("/api/register", request.path)

            // Assert correct method called
            assertEquals("POST", request.method)

            assertNotNull(actualResponse)
        }
    }

    @Test
    fun `register new user with valid parameters returns 201 created`() {
        mockWebServer.enqueue(MockResponse().setResponseCode(HttpURLConnection.HTTP_CREATED))

        runBlocking {
            // Passing the required role parameter.
            val actualResponse = accountRepository.createUser(
                username, email, password, role
            )

            // Assert correct api call has been made
            val request = mockWebServer.takeRequest()
            assertEquals("/api/register", request.path)

            // Assert correct method called
            assertEquals("POST", request.method)


            assertNotNull(actualResponse)
        }
    }

}