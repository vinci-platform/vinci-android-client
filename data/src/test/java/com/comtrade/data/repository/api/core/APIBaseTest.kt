/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: APIBaseTest.kt
 * Author: sdelic
 *
 * History:  23.3.22. 16:41 created
 */

package com.comtrade.data.repository.api.core

import com.comtrade.data.repository.api.contract.IAPITestWebServer
import com.comtrade.data.repository.api.di.APITestComponent
import com.comtrade.data.repository.api.di.DaggerAPITestComponent
import com.comtrade.domain.contracts.repository.user.IAccountRepository
import com.comtrade.domain.contracts.repository.user.IDeviceRepository
import com.comtrade.domain.contracts.repository.user.UserServiceRepository
import com.google.gson.Gson
import kotlinx.coroutines.ExperimentalCoroutinesApi
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import javax.inject.Inject

/**
 * The API testing base class.
 * Injects the core dependency for API testing.
 * Abstracts test initialization boilerplate code.
 */
@ExperimentalCoroutinesApi
internal open class APIBaseTest {

    @Inject
    lateinit var apiTestWebServer: IAPITestWebServer
    protected lateinit var mockWebServer: MockWebServer
    protected lateinit var rootTestFilesDir: String
    protected lateinit var userServiceRepository: UserServiceRepository
    protected lateinit var accountRepository: IAccountRepository
    protected lateinit var deviceRepository: IDeviceRepository
    protected lateinit var gson: Gson

    companion object {
        internal const val AUTHORIZATION_HEADER = "Authorization"
    }

    @Before
    fun setUp() {
        val apiTestComponent: APITestComponent = DaggerAPITestComponent.create()
        apiTestComponent.inject(this)

        mockWebServer = apiTestWebServer.mockWebServer()
        rootTestFilesDir = apiTestWebServer.rootFilesDir()
        userServiceRepository = apiTestWebServer.userServiceRepository()
        accountRepository = apiTestWebServer.accountRepository()
        deviceRepository = apiTestWebServer.deviceRepository()
        gson = apiTestWebServer.gson()
    }

    @After
    fun tearDown() {
        mockWebServer.shutdown()
    }
}