/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: GetCmdWatchDataResponse.kt
 * Author: sdelic
 *
 * History:  28.4.22. 16:39 created
 */

package com.comtrade.data.network.models.devices

import com.comtrade.domain.entities.devices.data.CmdWatchEntity
import com.comtrade.domain.entities.devices.data.GetCmdWatchDataEntity
import com.google.gson.annotations.SerializedName

/**
 * Cmd watch data holder.
 */
class CmdWatchRecord {
    @SerializedName("id")
    var id: Long? = 0

    @SerializedName("coordinates")
    var coordinates: String? = null

    @SerializedName("steps")
    var steps: String? = null

    @SerializedName("timestamp")
    var timestamp: Long = 0

    @SerializedName("deviceId")
    var deviceId: Long = 0

}

/**
 * Obtain Cmd watch data API response data holder.
 */
class GetCmdWatchDataResponse {
    @SerializedName("H02")
    var h02: List<CmdWatchRecord>? = listOf()
}

fun CmdWatchRecord.toDomainEntity(): CmdWatchEntity = CmdWatchEntity(
    id, coordinates, steps, timestamp, deviceId
)

fun GetCmdWatchDataResponse.toDomainEntity(): GetCmdWatchDataEntity =
    GetCmdWatchDataEntity(
        h02 = h02?.map { it.toDomainEntity() }
    )
