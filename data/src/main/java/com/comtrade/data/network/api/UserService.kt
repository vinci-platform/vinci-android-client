/*
 *  Copyright (c) 2020, Comtrade
 *  All rights reserved.
 *
 *  Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of the <organization> nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *   DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  Package: com.comtrade.vinciapp.data.api
 *  File: UserService.kt
 *  Author: Milenko Bojanic <milenko.bojanic@comtrade.com>
 *
 *  Description: API call methods for Retrofit
 *
 *  History: 8/11/20 Milenko  Initial code
 *
 *
 */

package com.comtrade.data.network.api

import com.comtrade.data.network.models.auth.UserAuthenticateResponse
import com.comtrade.data.network.models.devices.GetCmdWatchDataResponse
import com.comtrade.data.network.models.devices.GetMobileAppSettingsResponse
import com.comtrade.data.network.models.devices.GetShoeDataResponse
import com.comtrade.data.network.models.event.GetEventRecordResponse
import com.comtrade.data.network.models.fitbit.FitBitDailyPayloadDataResponse
import com.comtrade.data.network.models.fitbit.FitBitPayloadDataResponse
import com.comtrade.data.network.models.fitbit.GetFitbitUrlResponse
import com.comtrade.data.network.models.survey.GetSurveyDataResponse
import com.comtrade.data.network.models.user.GetAllUserPhonesResponse
import com.comtrade.data.network.models.user.GetUserDevicesResponse
import com.comtrade.data.network.models.user.GetUserExtraResponse
import com.comtrade.data.network.models.user.GetUserResponse
import com.comtrade.domain.entities.devices.data.*
import com.comtrade.domain.entities.event.PostEventRecordRequest
import com.comtrade.domain.entities.survey.PostSurveyDataRequest
import com.comtrade.domain.entities.user.account.ChangePasswordRequestEntity
import com.comtrade.domain.entities.user.account.ResetPasswordFinishRequest
import com.comtrade.domain.entities.user.data.CreateUserRequest
import com.comtrade.domain.entities.user.data.PostHealthDataRequest
import com.comtrade.domain.entities.user.data.UserAuthenticateRequest
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.http.*

interface UserService {
    @POST("api/authenticate")
    suspend fun authenticateUser(@Body userAuthenticateRequest: UserAuthenticateRequest): UserAuthenticateResponse

    @POST("api/register")
    suspend fun createUser(@Body createUserRequest: CreateUserRequest): Response<Void>

    @GET("api/account")
    suspend fun getUser(@Header("Authorization") authHeader: String): GetUserResponse

    @GET("api/user-extras/by-userId/{id}")
    suspend fun getUserExtra(
        @Header("Authorization") authHeader: String,
        @Path("id") id: String
    ): GetUserExtraResponse

    @Multipart
    @POST("api/account")
    suspend fun updateUser(
        @Header("Authorization") authHeader: String,
        @Part("account") account: RequestBody
    ): Response<Void>

    @Multipart
    @POST("api/account")
    suspend fun updateUserImage(
        @Header("Authorization") authHeader: String,
        @Query("account") account: String,
        @Query("image1Id") image1: String,
        @Part filePart: MultipartBody.Part
    ): Response<Void>

    @POST("api/account/reset-password/init")
    suspend fun resetPassword(
        @Header("Authorization") authHeader: String,
        @Body mail: RequestBody
    ): Response<Void>

    @POST("api/account/reset-password/finish")
    suspend fun resetPasswordFinish(
        @Body resetPasswordFinishRequest: ResetPasswordFinishRequest
    ): Response<Void>

    @POST("api/account/change-password")
    suspend fun changePassword(
        @Header("Authorization") authHeader: String,
        @Body changePasswordRequestEntity: ChangePasswordRequestEntity
    ): Response<Void>

    @GET("api/users/getPhones")
    suspend fun getAllUserPhones(@Header("Authorization") authHeader: String): List<GetAllUserPhonesResponse>

    @POST("api/devices")
    suspend fun addDevice(
        @Header("Authorization") authHeader: String,
        @Body addDeviceRequest: AddDeviceRequest
    ): AddDeviceResponse?

    @PUT("api/devices")
    suspend fun updateDevice(
        @Header("Authorization") authHeader: String,
        @Body updateDeviceRequest: UpdateDeviceRequest
    ): Response<Void>

    @DELETE("api/devices/{id}")
    suspend fun deleteUserDevice(
        @Header("Authorization") authHeader: String,
        @Path("id") id: String
    ): Response<Void>

    @GET("api/user-devices/{login}")
    suspend fun getUserDevices(
        @Header("Authorization") authHeader: String,
        @Path("login") login: String
    ): List<GetUserDevicesResponse>

    @POST("ioserver/api/shoe-data")
    suspend fun postShoeData(
        @Header("Authorization") authHeader: String,
        @Body postShoeDataRequest: PostShoeDataRequest
    ): Response<Void>

    @GET("ioserver/api/shoe-data")
    suspend fun getShoeData(
        @Header("Authorization") authHeader: String,
        @Query("deviceId.equals") deviceId: Long,
        @Query("timestamp.greaterOrEqualThan") fromTimestamp: Long,
        @Query("timestamp.lessOrEqualThan") toTimestamp: Long
    ): List<GetShoeDataResponse>

    @PUT("ioserver/api/shoe-data")
    suspend fun updateShoeData(
        @Header("Authorization") authHeader: String,
        @Body updateShoeDataRequest: UpdateShoeDataRequest
    ): Response<Boolean>

    @POST("survey/api/survey-data")
    suspend fun postSurveyData(
        @Header("Authorization") authHeader: String,
        @Query("userIdentifier") userExtraId: Long,
        @Body postSurveyDataRequest: PostSurveyDataRequest
    ): Response<Void>

    @GET("ioserver/api/survey-data")
    suspend fun getSurveyData(
        @Header("Authorization") authHeader: String,
        @Query("deviceId.equals") deviceId: Long,
        @Query("createdTime.greaterThan") fromCreatedTime: String
    ): List<GetSurveyDataResponse>

    @GET("ioserver/api/fitbit-watch-data/payload")
    suspend fun getFitbitWatchDataPayload(
        @Header("Authorization") authHeader: String,
        @Query("deviceId.equals") deviceId: Long,
        @Query("timestamp.greaterOrEqualThan") timestamp: Long
    ): List<FitBitPayloadDataResponse>

    @GET("ioserver/api/fitbit-watch-intraday-data/payload")
    suspend fun getFitbitWatchDailyDataPayload(
        @Header("Authorization") authHeader: String,
        @Query("deviceId.equals") deviceId: Long,
        @Query("timestamp.greaterOrEqualThan") timestamp: Long
    ): List<FitBitDailyPayloadDataResponse>

    @GET("api/mobile-app-settings")
    suspend fun getMobileAppSettings(
        @Header("Authorization") authHeader: String, @Query("userId.equals") userId: Long
    ): List<GetMobileAppSettingsResponse>

    @POST("ioserver/api/watch-data/orderedByTypeStepsAndCoordinates")
    suspend fun getCmdWatchData(
        @Header("Authorization") authHeader: String,
        @Body getCmdWatchDataRequest: GetCmdWatchDataRequest
    ): GetCmdWatchDataResponse

    @POST("api/health-records")
    suspend fun postHealthData(
        @Header("Authorization") authHeader: String,
        @Body postHealthDataRequest: PostHealthDataRequest
    ): Response<Void>

    @GET("api/event-records")
    suspend fun getEventRecord(
        @Header("Authorization") authHeader: String,
        @Query("appId.equals") appId: Long,
        @Query("created.equals") created: Long,
        @Query("timestamp.equals") timestamp: Long
    ): List<GetEventRecordResponse>

    @POST("api/event-records")
    suspend fun postEventRecord(
        @Header("Authorization") authHeader: String,
        @Body postEventRecordRequest: PostEventRecordRequest
    ): Response<Void>

    @DELETE("api/event-records/{id}")
    suspend fun deleteEventRecord(
        @Header("Authorization") authHeader: String,
        @Path("id") id: Long
    ): Response<Void>

    @GET("api/fitbit-auth/authorize-mobile")
    suspend fun getFitbitURL(
        @Header("Authorization") authHeader: String,
        @Query("userExtraId") userExtraId: Long
    ): GetFitbitUrlResponse

}