/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: FitBitDataPayloadResponse.kt
 * Author: sdelic
 *
 * History:  26.5.22. 10:48 created
 */

package com.comtrade.data.network.models.fitbit

import com.comtrade.domain.entities.devices.fitbit.*
import com.google.gson.annotations.SerializedName

///Sleep data
data class SleepDataSummaryResponse(
    @SerializedName("totalMinutesAsleep") val totalMinutesAsleep: Int,
    @SerializedName("totalTimeInBed") val totalTimeInBed: Int
)

fun SleepDataSummaryResponse.toDomainEntity(): SleepDataSummaryEntity =
    SleepDataSummaryEntity(
        totalMinutesAsleep = totalMinutesAsleep,
        totalTimeInBed = totalTimeInBed
    )

data class SleepDataResponse(@SerializedName("summary") val summary: SleepDataSummaryResponse)

fun SleepDataResponse.toDomainEntity(): SleepDataEntity =
    SleepDataEntity(
        summary = summary.toDomainEntity()
    )
///

///Activity data
data class HeartRateZonesResponse(
    @SerializedName("caloriesOut")
    val caloriesOut: Int,

    @SerializedName("max")
    val max: Int,

    @SerializedName("min")
    val min: Int,

    @SerializedName("minutes")
    val minutes: Int,

    @SerializedName("name")
    val name: String
)

fun HeartRateZonesResponse.toDomainEntity(): HeartRateZonesEntity =
    HeartRateZonesEntity(
        caloriesOut = caloriesOut,
        max = max,
        min = min,
        minutes = minutes,
        name = name
    )

data class DistanceDataResponse(
    @SerializedName("activity") val activity: String,
    @SerializedName("distance") val distance: Double
)

fun DistanceDataResponse.toDomainEntity(): DistanceDataEntity = DistanceDataEntity(
    activity = activity,
    distance = distance,
)

data class ActivityDataResponse(
    @SerializedName("activityCalories")
    val activityCalories: Int,

    @SerializedName("caloriesBMR")
    val caloriesBMR: Int,

    @SerializedName("caloriesOut")
    val caloriesOut: Int,

    @SerializedName("elevation")
    val elevation: Int,

    @SerializedName("fairlyActiveMinutes")
    val fairlyActiveMinutes: Int,

    @SerializedName("floors")
    val floors: Int,

    @SerializedName("lightlyActiveMinutes")
    val lightlyActiveMinutes: Int,

    @SerializedName("marginalCalories")
    val marginalCalories: Int,

    @SerializedName("restingHeartRate")
    val restingHeartRate: Int?,

    @SerializedName("sedentaryMinutes")
    val sedentaryMinutes: Int,

    @SerializedName("steps")
    val steps: Int,

    @SerializedName("veryActiveMinutes")
    val veryActiveMinutes: Int,

    @SerializedName("distances")
    val distances: List<DistanceDataResponse>,

    @SerializedName("heartRateZones")
    val heartRateZones: List<HeartRateZonesResponse>?
)

fun ActivityDataResponse.toDomainEntity(): ActivityDataEntity =
    ActivityDataEntity(
        activityCalories = activityCalories,
        caloriesBMR = caloriesBMR,
        caloriesOut = caloriesOut,
        elevation = elevation,
        fairlyActiveMinutes = fairlyActiveMinutes,
        floors = floors,
        lightlyActiveMinutes = lightlyActiveMinutes,
        marginalCalories = marginalCalories,
        restingHeartRate = restingHeartRate,
        sedentaryMinutes = sedentaryMinutes,
        steps = steps,
        veryActiveMinutes = veryActiveMinutes,
        distances = distances.map { it.toDomainEntity() },
        heartRateZones = heartRateZones?.map { it.toDomainEntity() }
    )
///

///Device data
data class FitBitDeviceResponse(
    @SerializedName("id")
    val id: Long,

    @SerializedName("name")
    val name: String,

    @SerializedName("description")
    val description: String,

    @SerializedName("uuid")
    val uuid: String,

    @SerializedName("deviceType")
    val deviceType: String,

    @SerializedName("active")
    val active: Boolean,

    @SerializedName("startTimestamp")
    val startTimestamp: String,

    @SerializedName("userExtraId")
    val userExtraId: Long
)

fun FitBitDeviceResponse.toDomainEntity(): FitBitDeviceEntity =
    FitBitDeviceEntity(
        id = id,
        name = name,
        description = description,
        uuid = uuid,
        deviceType = deviceType,
        active = active,
        startTimestamp = startTimestamp,
        userExtraId = userExtraId
    )
///

/**
 * FitBit watch payload data holder to which the API response data will me mapped on.
 */
data class FitBitPayloadDataResponse(
    @SerializedName("id")
    val id: Long,

    @SerializedName("timestamp")
    val timestamp: Long,

    @SerializedName("sleepData")
    val sleepData: SleepDataResponse,

    @SerializedName("activityData")
    val activityData: ActivityDataResponse,

    @SerializedName("device")
    val device: FitBitDeviceResponse
)

fun FitBitPayloadDataResponse.toDomainEntity(): FitBitDataPayloadEntity =
    FitBitDataPayloadEntity(
        id = id,
        timestamp = timestamp,
        sleepData = sleepData.toDomainEntity(),
        activityData = activityData.toDomainEntity(),
        device = device.toDomainEntity()
    )