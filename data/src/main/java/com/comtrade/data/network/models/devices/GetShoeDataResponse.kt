/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: GetShoeDataResponse.kt
 * Author: sdelic
 *
 * History:  28.4.22. 16:52 created
 */

package com.comtrade.data.network.models.devices

import com.comtrade.domain.entities.devices.data.GetShoeDataEntity
import com.comtrade.domain.entities.devices.data.ShoeDataEntity
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName

/**
 * Shoe data holder.
 */
class ShoeData {
    @SerializedName("step_counter")
    var step_counter: Int? = 0

    @SerializedName("step_activity")
    var step_activity: Int? = 0
}

/**
 * Obtain shoe data API response data holder.
 */
class GetShoeDataResponse {
    @SerializedName("id")
    var id: Long? = 0

    @SerializedName("data")
    var data: String? = null

    @SerializedName("timestamp")
    var timestamp: Long = 0

    fun getShoeData(): ShoeData {
        return Gson().fromJson(data, ShoeData::class.java)
    }
}

fun ShoeData.toDomainEntity(): ShoeDataEntity = ShoeDataEntity(
    step_counter, step_activity
)

fun GetShoeDataResponse.toDomainEntity(): GetShoeDataEntity = GetShoeDataEntity(
    id, getShoeData().toDomainEntity(), timestamp
)