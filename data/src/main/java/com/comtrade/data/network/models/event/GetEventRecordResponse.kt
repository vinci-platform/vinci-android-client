/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: GetEventRecordResponse.kt
 * Author: sdelic
 *
 * History:  4.5.22. 12:55 created
 */

package com.comtrade.data.network.models.event

import com.comtrade.domain.entities.event.EventEntity
import com.google.gson.annotations.SerializedName

/**
 * Obtain event records API response data holder.
 */
class GetEventRecordResponse {
    @SerializedName("id")
    var id: Long? = 0

    @SerializedName("appId")
    var appId: Long? = 0

    @SerializedName("userId")
    var userId: Long? = 0

    @SerializedName("timestamp")
    var timestamp: Long = 0

    @SerializedName("created")
    var created: Long = 0

    @SerializedName("notify")
    var notify: Long = 0

    @SerializedName("repeat")
    var repeat: String = ""

    @SerializedName("title")
    var title: String = ""

    @SerializedName("text")
    var text: String = ""

    @SerializedName("type")
    var type: String = ""

    @SerializedName("data")
    var data: String = ""
}

fun GetEventRecordResponse.toDomainEntity(): EventEntity = EventEntity(
    id = id?.toInt() ?: 0,
    userId = userId ?: 0,
    timestamp,
    created,
    notify,
    repeat,
    title,
    type,
    text,
    data
)
