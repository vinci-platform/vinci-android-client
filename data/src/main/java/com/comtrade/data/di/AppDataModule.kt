/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: AppDataModule.kt
 * Author: sdelic
 *
 * History:  22.3.22. 12:22 created
 */

package com.comtrade.data.di

import android.app.Application
import androidx.room.Room
import com.comtrade.data.local.Prefs
import com.comtrade.data.network.api.RetrofitBuilder
import com.comtrade.data.network.api.UserService
import com.comtrade.data.repository.AccountRepositoryImpl
import com.comtrade.data.repository.AppRepositoryImpl
import com.comtrade.data.repository.DeviceRepository
import com.comtrade.data.repository.UserServiceRepositoryImpl
import com.comtrade.data.stepDatabase.*
import com.comtrade.domain.contracts.repository.IAppRepository
import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.contracts.repository.user.IAccountRepository
import com.comtrade.domain.contracts.repository.user.IDeviceRepository
import com.comtrade.domain.contracts.repository.user.UserServiceRepository
import com.comtrade.domain.di.AppQualifiers
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import javax.inject.Named

/**
 * Dagger DI data module.
 * Provides the dependency implementations for the app database, SharedPreferences and StepManager component.
 */
@Module
class AppDataModule(private val application: Application) {

    @Provides
    fun provideGson(): Gson = Gson().newBuilder().create()

    @Provides
    fun provideAppPrefs(gson: Gson): IPrefs = Prefs(application, gson)

    @Provides
    internal fun provideStepDatabase(): StepDatabase = Room.databaseBuilder(
        application,
        StepDatabase::class.java,
        StepDatabase.DATABASE_NAME
    ).fallbackToDestructiveMigration().build()

    @Provides
    fun provideSurveyDao(stepDatabase: StepDatabase): SurveyDao = stepDatabase.surveyDao()

    @Provides
    fun provideEventsDao(stepDatabase: StepDatabase): EventDao = stepDatabase.eventDao()

    @Provides
    fun provideHealthDao(stepDatabase: StepDatabase): HealthDao = stepDatabase.healthDao()

    @Provides
    internal fun provideStepDao(stepDatabase: StepDatabase): StepDao = stepDatabase.stepDao()

    @Provides
    internal fun provideCmdDao(stepDatabase: StepDatabase): CmdDao = stepDatabase.cmdDao()

    @Provides
    internal fun provideFriendsDao(stepDatabase: StepDatabase): FriendsDao =
        stepDatabase.friendsDao()

    @Provides
    fun provideUserService(@Named(AppQualifiers.APP_BASE_URL) baseUrl: String): UserService =
        RetrofitBuilder.getUserService(baseUrl)

    @Provides
    fun provideAppRepository(
        surveyDao: SurveyDao,
        eventDao: EventDao,
        healthDao: HealthDao,
        stepDao: StepDao,
        cmdDao: CmdDao,
        friendsDao: FriendsDao,
    ): IAppRepository =
        AppRepositoryImpl(surveyDao, eventDao, healthDao, stepDao, cmdDao, friendsDao)

    @Provides
    fun provideAccountRepository(gson: Gson, userService: UserService): IAccountRepository =
        AccountRepositoryImpl(gson, userService)

    @Provides
    fun provideDeviceRepository(userService: UserService): IDeviceRepository =
        DeviceRepository(userService)

    @Provides
    fun provideUserServiceRepository(
        userService: UserService
    ): UserServiceRepository =
        UserServiceRepositoryImpl(userService)
}