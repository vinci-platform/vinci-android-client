/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: Prefs.kt
 * Author: sdelic
 *
 * History:  17.3.22. 10:06 created
 */

package com.comtrade.data.local

import android.content.Context
import android.content.SharedPreferences
import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.entities.devices.CmdDevice
import com.comtrade.domain.entities.devices.FitBitDevice
import com.comtrade.domain.entities.devices.ShoeDevice
import com.comtrade.domain.entities.devices.SurveyDevice
import com.comtrade.domain.entities.user.Contact
import com.comtrade.domain.entities.user.account.GetUserEntity
import com.comtrade.domain.entities.user.account.GetUserExtraEntity
import com.comtrade.domain.settings.SupportedDeviceTypes
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class Prefs @Inject constructor(context: Context, private val gson: Gson) : IPrefs {

    private var prefs = context.getSharedPreferences("shared_data", Context.MODE_PRIVATE)

    private inline fun SharedPreferences.edit(operation: (SharedPreferences.Editor) -> Unit) {
        val editor = edit()
        operation(editor)
        editor.apply()
    }

    override var token: String
        get() = prefs.getString("token", "") ?: ""
        set(value) = prefs.edit { it.putString("token", value) }

    override var userId: Long
        get() = prefs.getLong("user_id", -1)
        set(value) = prefs.edit { it.putLong("user_id", value) }

    override var userFirstName: String
        get() = prefs.getString("user_firstname", "") ?: ""
        set(value) = prefs.edit { it.putString("user_firstname", value) }

    override var userLastName: String
        get() = prefs.getString("user_lastname", "") ?: ""
        set(value) = prefs.edit { it.putString("user_lastname", value) }

    override var userEmail: String
        get() = prefs.getString("user_email", "") ?: ""
        set(value) = prefs.edit { it.putString("user_email", value) }

    override var userLogin: String
        get() = prefs.getString("user_login", "") ?: ""
        set(value) = prefs.edit { it.putString("user_login", value) }

    override var userPassword: String
        get() = prefs.getString("user_password", "") ?: ""
        set(value) = prefs.edit { it.putString("user_password", value) }

    override var userAutologin: Boolean
        get() = prefs.getBoolean("user_autologin", false)
        set(value) = prefs.edit { it.putBoolean("user_autologin", value) }

    override var userAddress: String
        get() = prefs.getString("user_address", "") ?: ""
        set(value) = prefs.edit { it.putString("user_address", value) }

    override var userPhone: String
        get() = prefs.getString("user_telephone", "") ?: ""
        set(value) = prefs.edit { it.putString("user_telephone", value) }

    override var userGender: String
        get() = prefs.getString("user_gender", "") ?: ""
        set(value) = prefs.edit { it.putString("user_gender", value) }

    override var userProfilePicture: String
        get() = prefs.getString("user_profile_picture", "") ?: ""
        set(value) = prefs.edit { it.putString("user_profile_picture", value) }

    override var userProfilePictureId: Long
        get() = prefs.getLong("user_profile_picture_id", -1)
        set(value) = prefs.edit { it.putLong("user_profile_picture_id", value) }

    override var userExtraId: Long
        get() = prefs.getLong("user_extra_id", -1)
        set(value) = prefs.edit { it.putLong("user_extra_id", value) }

    override var isTermsAccepted: Boolean
        get() = prefs.getBoolean("terms_and_conditions", false)
        set(value) = prefs.edit { it.putBoolean("terms_and_conditions", value) }

    override var isFirstRun: Boolean
        get() = prefs.getBoolean("first_run", true)
        set(value) = prefs.edit { it.putBoolean("first_run", value) }

    override var isDeviceAdded: Boolean
        get() = prefs.getBoolean("device_added", false)
        set(value) = prefs.edit { it.putBoolean("device_added", value) }

    override var isQuestionnaireStarted: Boolean
        get() = prefs.getBoolean("questionnaire_started", false)
        set(value) = prefs.edit { it.putBoolean("questionnaire_started", value) }

    override var emergencyNumber: String
        get() = prefs.getString("emergency_number", "") ?: ""
        set(value) = prefs.edit { it.putString("emergency_number", value) }

    override var emergencyNumberName: String
        get() = prefs.getString("emergency_number_name", "") ?: ""
        set(value) = prefs.edit { it.putString("emergency_number_name", value) }

    override var isEmergencyNumberSet: Boolean
        get() = prefs.getBoolean("emergency_number_set", false)
        set(value) = prefs.edit { it.putBoolean("emergency_number_set", value) }

    override var isNowSetPhoneNumber: Boolean
        get() = prefs.getBoolean("now_set_phone_number", false)
        set(value) = prefs.edit { it.putBoolean("now_set_phone_number", value) }

    override var notificationId: Int
        get() = prefs.getInt("notification_id", 0)
        set(value) = prefs.edit { it.putInt("notification_id", value) }

    override var notificationRequestCode: Int
        get() = prefs.getInt("notification_request_code", 0)
        set(value) = prefs.edit { it.putInt("notification_request_code", value) }

    var alarmRequestCode: Int
        get() = prefs.getInt("alarm_request_code", 0)
        set(value) = prefs.edit { it.putInt("alarm_request_code", value) }

    override var showHints: Boolean
        get() = prefs.getBoolean("show_hints", true)
        set(value) = prefs.edit { it.putBoolean("show_hints", value) }

    var showStepCounts: Boolean
        get() = prefs.getBoolean("show_step_counts", false)
        set(value) = prefs.edit { it.putBoolean("show_step_counts", value) }

    var isNearbyInsole: Boolean
        get() = prefs.getBoolean("nearby_insole", false)
        set(value) = prefs.edit { it.putBoolean("nearby_insole", value) }

    var isNearbyFitBit: Boolean
        get() = prefs.getBoolean("nearby_fitbit", false)
        set(value) = prefs.edit { it.putBoolean("nearby_fitbit", value) }

    override var isNotificationSet: Boolean
        get() = prefs.getBoolean("notification_set", true)
        set(value) = prefs.edit { it.putBoolean("notification_set", value) }

    override var logFile: String
        get() = prefs.getString("log_file", "") ?: "Empty"
        set(value) = prefs.edit { it.putString("log_file", value) }

    override var connectionStatus: String
        get() = prefs.getString("connection_status", "") ?: ""
        set(value) = prefs.edit { it.putString("connection_status", value) }

    override var connectionError: String
        get() = prefs.getString("connection_error", "") ?: ""
        set(value) = prefs.edit { it.putString("connection_error", value) }

    override var lastSuccessfulSync: String
        get() = prefs.getString("last_successful_sync", "") ?: ""
        set(value) = prefs.edit { it.putString("last_successful_sync", value) }

    override var isLogEnabled: Boolean
        get() = prefs.getBoolean("log_enabled", true)
        set(value) = prefs.edit { it.putBoolean("log_enabled", value) }

    override var shoeDevice: ShoeDevice?
        get() = if (prefs.getString("shoe_device", "")!!.isNotEmpty()) {
            val sType = object : TypeToken<ShoeDevice>() {}.type
            gson.fromJson<ShoeDevice>(
                prefs.getString("shoe_device", ""),
                sType
            )
        } else {
            null
        }
        set(value) = prefs.edit {
            it.putString("shoe_device", gson.toJson(value))
        }

    override var fitBitDevice: FitBitDevice?
        get() = if (prefs.getString("fitbit_device", "")!!.isNotEmpty()) {
            val sType = object : TypeToken<FitBitDevice>() {}.type
            gson.fromJson<FitBitDevice>(
                prefs.getString("fitbit_device", ""),
                sType
            )
        } else {
            null
        }
        set(value) = prefs.edit {
            it.putString("fitbit_device", gson.toJson(value))
        }

    override var cmdDevice: CmdDevice?
        get() = if (prefs.getString("cmd_device", "")!!.isNotEmpty()) {
            val sType = object : TypeToken<CmdDevice>() {}.type
            gson.fromJson<CmdDevice>(
                prefs.getString("cmd_device", ""),
                sType
            )
        } else {
            null
        }
        set(value) = prefs.edit {
            it.putString("cmd_device", gson.toJson(value))
        }

    override var surveyDevice: SurveyDevice?
        get() = if (prefs.getString("survey_device", "")!!.isNotEmpty()) {
            val sType = object : TypeToken<SurveyDevice>() {}.type
            gson.fromJson<SurveyDevice>(
                prefs.getString("survey_device", ""),
                sType
            )
        } else {
            null
        }
        set(value) = prefs.edit {
            it.putString("survey_device", gson.toJson(value))
        }

    override var userFriends: List<Contact>
        get() = if (prefs.getString("user_friends", "")!!.isNotEmpty()) {
            val sType = object : TypeToken<List<Contact>>() {}.type
            gson.fromJson(
                prefs.getString("user_friends", "").toString(),
                sType
            )
        } else {
            listOf()
        }
        set(value) = prefs.edit { it.putString("user_friends", gson.toJson(value)) }

    override fun addLog(text: String, isError: Boolean) {
        if (isLogEnabled || isError) {
            val formatter = SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss z", Locale.getDefault())
            val time = formatter.format(Date(System.currentTimeMillis()))

            logFile = "$logFile\n$time: $text"
            if (logFile.length > 10000) {
                logFile = logFile.substring(1000)
            }
        }
    }

    override fun setUserData(getUserResponse: GetUserEntity?) {
        if (getUserResponse == null) {
            userId = -1
            userFirstName = ""
            userLastName = ""
            userEmail = ""
            userExtraId = -1
            userProfilePicture = ""
            userAddress = ""
            userPhone = ""
        } else {
            userId = getUserResponse.userId!!.toLong()
            userFirstName = if (getUserResponse.firstName != null)
                getUserResponse.firstName!!
            else
                ""
            userLastName = if (getUserResponse.lastName != null)
                getUserResponse.lastName!!
            else
                ""

            userEmail = if (getUserResponse.email != null)
                getUserResponse.email!!
            else
                ""
            userAddress = if (getUserResponse.address != null)
                getUserResponse.address!!
            else
                ""
            userPhone = if (getUserResponse.phone != null)
                getUserResponse.phone!!
            else
                ""
        }
    }

    // TODO: check for internationalization issues
    override fun setUserExtraData(getUserExtraResponse: GetUserExtraEntity?) {
        if (getUserExtraResponse == null) {
            userGender = "Male"
            shoeDevice = null
            fitBitDevice = null
            cmdDevice = null
            userProfilePicture = ""
            userFriends = listOf()
        } else {
            userExtraId = getUserExtraResponse.id!!
            userGender = if (getUserExtraResponse.gender != null)
                getUserExtraResponse.gender!!
            else
                "Male"
            userFriends = if (getUserExtraResponse.friends == null) {
                listOf()
            } else {
                try {
                    val sType = object : TypeToken<List<Contact>>() {}.type
                    gson.fromJson(
                        getUserExtraResponse.friends,
                        sType
                    )
                } catch (e: Exception) {
                    listOf()
                }
            }
            if (!getUserExtraResponse.devices.isNullOrEmpty()) {
                for (device in getUserExtraResponse.devices!!) {
                    when {
                        device.deviceType == SupportedDeviceTypes.SHOE.name -> {
                            try {
                                val shoeDevice =
                                    gson.fromJson(device.description!!, ShoeDevice::class.java)
                                shoeDevice.deviceId = device.deviceId!!
                                shoeDevice.deviceUUID =
                                    device.uuid ?: shoeDevice.deviceMac.toString()
                                shoeDevice.active = device.active
                                this.shoeDevice = shoeDevice
                            } catch (e: Exception) {
                                addLog("Error parsing shoe device: $e", true)
                                this.shoeDevice = null
                            }
                        }
                        device.deviceType!! == SupportedDeviceTypes.FITBIT_WATCH.name -> {
                            this.fitBitDevice = FitBitDevice(device)
                        }
                        device.deviceType!! == SupportedDeviceTypes.WATCH.name -> {
                            this.cmdDevice = CmdDevice(device)
                        }
                        device.deviceType == SupportedDeviceTypes.SURVEY.name -> {
                            this.surveyDevice = SurveyDevice(device)
                        }
                    }
                }
            }
        }
    }
}