/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: AppRepositoryImpl.kt
 * Author: sdelic
 *
 * History:  24.3.22. 13:23 created
 */

package com.comtrade.data.repository

import com.comtrade.data.stepDatabase.*
import com.comtrade.domain.contracts.repository.IAppRepository
import com.comtrade.domain.entities.devices.data.CmdWatchEntity
import com.comtrade.domain.entities.event.EventEntity
import com.comtrade.domain.entities.friends.FriendsEntity
import com.comtrade.domain.entities.health.HealthEntity
import com.comtrade.domain.entities.step.StepDailyEntity
import com.comtrade.domain.entities.step.StepEntity
import com.comtrade.domain.entities.survey.SurveyDataModel
import javax.inject.Inject

/**
 * The application repository implementation.
 */
internal class AppRepositoryImpl @Inject constructor(
    private val surveyDao: SurveyDao,
    private val eventDao: EventDao,
    private val healthDao: HealthDao,
    private val stepDao: StepDao,
    private val cmdDao: CmdDao,
    private val friendsDao: FriendsDao,
) :
    IAppRepository {
    override suspend fun insertNewSurveyData(surveyDataModel: SurveyDataModel): Long =
        surveyDao.insertAll(
            SurveyData.fromDomainModel(surveyDataModel)
        )

    override suspend fun getUserEventRecords(userId: Long, timestamp: Long): List<EventEntity> =
        eventDao.getUserEventRecords(userId, timestamp).map {
            it.toDomainEntity()
        }

    override suspend fun insertUserEventRecord(eventEntity: EventEntity): Long =
        eventDao.insertAll(EventRecord.fromDomainEntity(eventEntity))[0]

    override suspend fun updateUserEventRecord(eventEntity: EventEntity): Boolean {
        val updatedRows = eventDao.update(EventRecord.fromDomainEntity(eventEntity))
        return updatedRows == 1
    }

    override suspend fun getEventRecordsForDelete(userId: Long): List<EventEntity> {
        return eventDao.getEventRecordsForDelete(userId).map { it.toDomainEntity() }
    }

    override suspend fun deleteEvent(eventEntity: EventEntity): Int {
        return eventDao.delete(EventRecord.fromDomainEntity(eventEntity))
    }

    override suspend fun getEventRecordsForSync(userId: Long): List<EventEntity> {
        return eventDao.getEventRecordsForSync(userId).map { it.toDomainEntity() }
    }

    override suspend fun getLastHealthRecord(userId: Long): HealthEntity? =
        healthDao.getLastHealthRecord(userId)?.toDomainEntity()

    override suspend fun insertHealthEntity(healthEntity: HealthEntity): Long =
        healthDao.insertAll(HealthRecord.fromDomainEntity(healthEntity))[0]

    override suspend fun updateHealthEntity(healthEntity: HealthEntity): Int =
        healthDao.updateHealthRecord(HealthRecord.fromDomainEntity(healthEntity))

    override suspend fun getHealthRecord(userId: Long, timestamp: Long): HealthEntity? {
        return healthDao.getHealthRecords(userId, timestamp)?.toDomainEntity()
    }

    override suspend fun getHealthRecordsForSync(userId: Long): List<HealthEntity> {
        return healthDao.getRecordsForSync(userId).map { it.toDomainEntity() }
    }

    override suspend fun getDailySteps(deviceId: Long, timestamp: Long): StepDailyEntity? =
        stepDao.getDailySteps(deviceId, timestamp)?.toDomainEntity()

    override suspend fun getStepsForPeriod(deviceId: Long, start: Long, end: Long): Int =
        stepDao.getStepsForPeriod(deviceId, start, end)

    override suspend fun getDailyStepsForPeriod(
        deviceId: Long,
        start: Long
    ): List<StepDailyEntity> {
        return stepDao.getDailyStepsForPeriod(deviceId, start).map { it.toDomainEntity() }
    }

    override suspend fun getMaxSyncedTimestamp(deviceId: Long): Long {
        return stepDao.getMaxSyncedTimestamp(deviceId)
    }

    override suspend fun getMaxDailyTimestamp(deviceId: Long): Long =
        stepDao.getMaxDailyTimestamp(deviceId)

    override suspend fun insertDaily(stepsDaily: StepDailyEntity): Long {
        return stepDao.insertDaily(StepDaily.fromDomainEntity(stepsDaily))
    }

    override suspend fun updateDaily(stepsDaily: StepDailyEntity): Int {
        return stepDao.updateDaily(StepDaily.fromDomainEntity(stepsDaily))
    }

    override suspend fun getStepEntity(deviceId: Long, timestamp: Long): StepEntity? {
        return stepDao.getStepRecord(deviceId, timestamp)?.toDomainEntity()
    }

    override suspend fun addSteps(steps: StepEntity): Long {
        return stepDao.insertAll(StepRecord.fromDomainEntity(steps))[0]
    }

    override suspend fun updateStepData(steps: StepEntity): Int {
        return stepDao.updateStepRecord(StepRecord.fromDomainEntity(steps))
    }

    override suspend fun getStepsForSync(deviceId: Long): List<StepEntity> {
        return stepDao.getStepsForSync(deviceId).map { it.toDomainEntity() }
    }

    override suspend fun getCmdStepsForPeriod(deviceId: Long, start: Long, end: Long): Int =
        cmdDao.getCmdStepsForPeriod(deviceId, start, end)

    override suspend fun getMaxTimestamp(deviceId: Long): Long =
        cmdDao.getMaxTimestamp(deviceId)

    override suspend fun insertCMDEntity(cmdDevice: CmdWatchEntity): Long {
        return cmdDao.insertAll(CmdRecord.fromDomainEntity(cmdDevice))[0]
    }

    override suspend fun getSurveyRecords(
        userId: Long,
        surveyType: String,
        startTime: Long,
        endTime: Long
    ): List<SurveyDataModel> =
        surveyDao.getSurveyRecords(userId, surveyType, startTime, endTime).map {
            it.toDomainEntity()
        }

    override suspend fun getRecordsForSync(userSurveyExtraId: Long): List<SurveyDataModel> =
        surveyDao.getRecordsForSync(userSurveyExtraId).map { it.toDomainEntity() }

    override suspend fun updateSurveyRecord(surveyDataModel: SurveyDataModel): Int =
        surveyDao.updateSurveyRecord(SurveyData.fromDomainModel(surveyDataModel))

    override suspend fun getMaxSynced(): Long = surveyDao.getMaxSynced()

    //Friends
    override suspend fun getByPhone(userId: Long, phone: String): FriendsEntity? =
        friendsDao.getByPhone(userId, phone)?.toDomainEntity()

    override suspend fun deleteFriend(friendsEntity: FriendsEntity): Int =
        friendsDao.delete(FriendsRecord.fromDomainEntity(friendsEntity))

    override suspend fun addFriend(friendsEntity: FriendsEntity): Long =
        friendsDao.insertAll(FriendsRecord.fromDomainEntity(friendsEntity))

}