/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: UserServiceRepositoryImpl.kt
 * Author: sdelic
 *
 * History:  22.3.22. 15:14 created
 */

package com.comtrade.data.repository

import com.comtrade.data.network.api.UserService
import com.comtrade.data.network.models.devices.toDomainEntity
import com.comtrade.data.network.models.event.toDomainEntity
import com.comtrade.data.network.models.fitbit.toDomainEntity
import com.comtrade.data.network.models.survey.toDomainEntity
import com.comtrade.data.network.models.user.toDomainEntity
import com.comtrade.domain.contracts.repository.user.UserServiceRepository
import com.comtrade.domain.entities.devices.data.*
import com.comtrade.domain.entities.event.PostEventRecordRequest
import com.comtrade.domain.entities.survey.PostSurveyDataRequest
import com.comtrade.domain.entities.survey.SurveyDataEntity
import com.comtrade.domain.entities.user.account.GetUserEntity
import com.comtrade.domain.entities.user.account.GetUserExtraEntity
import com.comtrade.domain.entities.user.data.GetAllUserPhonesEntity
import com.comtrade.domain.entities.user.data.GetUserDevicesEntity
import com.comtrade.domain.entities.user.data.PostHealthDataRequest
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import retrofit2.HttpException
import java.io.File

class UserServiceRepositoryImpl(
    private val userService: UserService
) : UserServiceRepository {

    override suspend fun getUser(
        token: String
    ): GetUserEntity = userService.getUser(
        "Bearer $token"
    ).toDomainEntity()


    override suspend fun getUserExtra(token: String, id: Long): GetUserExtraEntity =
        userService.getUserExtra("Bearer $token", id.toString()).toDomainEntity()

    override suspend fun getAllUserPhones(
        token: String
    ): List<GetAllUserPhonesEntity> =
        userService.getAllUserPhones(
            "Bearer $token"
        ).map { it.toDomainEntity() }


    override suspend fun updateUserImage(
        token: String,
        account: String,
        imageId: Long,
        file: File
    ): Boolean {
        val response = userService.updateUserImage(
            "Bearer $token",
            account,
            imageId.toString(),
            MultipartBody.Part.createFormData(
                "image1", file.name,
                file.asRequestBody("image/*".toMediaTypeOrNull())
            )
        )

        return try {
            response.isSuccessful
        } catch (e: HttpException) {
            e.printStackTrace()
            false
        }
    }

    override suspend fun addDevice(
        token: String,
        addDeviceRequest: AddDeviceRequest
    ): AddDeviceResponse? =
        userService.addDevice(
            "Bearer $token",
            addDeviceRequest
        )


    override suspend fun updateDevice(
        token: String,
        updateDeviceRequest: UpdateDeviceRequest
    ): Boolean {

        val response = userService.updateDevice(
            "Bearer $token",
            updateDeviceRequest
        )
        return try {
            response.isSuccessful
        } catch (e: HttpException) {
            e.printStackTrace()
            false
        }

    }

    override suspend fun getUserDevices(
        token: String,
        login: String
    ): List<GetUserDevicesEntity> =
        userService.getUserDevices(
            "Bearer $token",
            login
        ).map { it.toDomainEntity() }


    override suspend fun updateShoeData(
        token: String,
        updateShoeDataRequest: UpdateShoeDataRequest
    ): Boolean {
        val response = userService.updateShoeData(
            "Bearer $token",
            updateShoeDataRequest
        )
        return try {
            response.isSuccessful
        } catch (e: HttpException) {
            e.printStackTrace()
            false
        }
    }


    override suspend fun postSurveyData(
        token: String,
        userId: Long,
        postSurveyDataRequest: PostSurveyDataRequest
    ): Boolean {
        val response = userService.postSurveyData(
            "Bearer $token",
            userId,
            postSurveyDataRequest
        )

        return try {
            response.isSuccessful
        } catch (e: HttpException) {
            e.printStackTrace()
            false
        }
    }

    override suspend fun getSurveyData(
        token: String,
        deviceId: Long,
        fromCreatedTime: String
    ): List<SurveyDataEntity> {
        return try {
            userService.getSurveyData(
                "Bearer $token",
                deviceId,
                fromCreatedTime
            ).map { it.toDomainEntity() }
        } catch (e: Exception) {
            emptyList()
        }
    }


    override suspend fun getCmdWatchData(
        token: String,
        getCmdWatchDataRequest: GetCmdWatchDataRequest
    ): GetCmdWatchDataEntity {
        return try {
            userService.getCmdWatchData(
                "Bearer $token",
                getCmdWatchDataRequest
            ).toDomainEntity()
        } catch (e: Exception) {
            GetCmdWatchDataEntity()
        }
    }

    override suspend fun postHealthData(
        token: String,
        postHealthDataRequest: PostHealthDataRequest
    ): Boolean {
        val response = userService.postHealthData(
            "Bearer $token",
            postHealthDataRequest
        )

        return try {
            response.isSuccessful
        } catch (e: HttpException) {
            e.printStackTrace()
            false
        }
    }

    override suspend fun getEventRecord(
        token: String,
        appId: Long,
        created: Long,
        timestamp: Long
    ) = userService.getEventRecord(
        "Bearer $token",
        appId,
        created,
        timestamp
    ).map { it.toDomainEntity() }


    override suspend fun postEventRecord(
        token: String,
        postEventRecordRequest: PostEventRecordRequest
    ): Boolean {
        val response = userService.postEventRecord(
            "Bearer $token",
            postEventRecordRequest
        )

        return try {
            response.isSuccessful
        } catch (e: HttpException) {
            e.printStackTrace()
            false
        }
    }

    override suspend fun deleteEventRecord(
        token: String,
        id: Long
    ): Boolean {

        val response = userService.deleteEventRecord(
            "Bearer $token",
            id
        )
        return try {
            response.isSuccessful
        } catch (e: HttpException) {
            e.printStackTrace()
            false
        }
    }

    override suspend fun getFitbitURL(token: String, userExtraId: Long) =
        userService.getFitbitURL(
            "Bearer $token",
            userExtraId
        ).toDomainEntity()
}