/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: DeviceRepository.kt
 * Author: sdelic
 *
 * History:  29.4.22. 13:56 created
 */

package com.comtrade.data.repository

import com.comtrade.data.network.api.UserService
import com.comtrade.data.network.models.devices.toDomainEntity
import com.comtrade.data.network.models.fitbit.toDomainEntity
import com.comtrade.domain.contracts.repository.user.IDeviceRepository
import com.comtrade.domain.entities.devices.data.GetShoeDataEntity
import com.comtrade.domain.entities.devices.data.PostShoeDataRequest
import com.comtrade.domain.entities.devices.fitbit.FitBitDailyPayloadEntity
import com.comtrade.domain.entities.devices.fitbit.FitBitDataPayloadEntity
import retrofit2.HttpException
import javax.inject.Inject

/**
 * Device repository implementation detail.
 * Coordinates the devices repository-related actions and CRUD operations.
 */
internal class DeviceRepository @Inject constructor(
    private val userService: UserService
) : IDeviceRepository {

    override suspend fun deleteUserDevice(
        token: String,
        deviceId: Long
    ): Boolean {
        val response = userService.deleteUserDevice(
            "Bearer $token",
            deviceId.toString()
        )

        return try {
            response.isSuccessful
        } catch (e: HttpException) {
            e.printStackTrace()
            false
        }
    }

    override suspend fun getShoeData(
        token: String,
        deviceId: Long,
        fromTimestamp: Long,
        toTimestamp: Long
    ): List<GetShoeDataEntity> =
        userService.getShoeData(
            "Bearer $token",
            deviceId, fromTimestamp, toTimestamp
        ).map { it.toDomainEntity() }

    override suspend fun postShoeData(
        token: String,
        postShoeDataRequest: PostShoeDataRequest
    ): Boolean {
        val response = userService.postShoeData(
            "Bearer $token",
            postShoeDataRequest
        )

        return try {
            response.isSuccessful
        } catch (e: HttpException) {
            e.printStackTrace()
            false
        }
    }

    override suspend fun getFitbitWatchData(
        token: String,
        deviceId: Long,
        timestamp: Long
    ): List<FitBitDataPayloadEntity> {
        return try {
            userService.getFitbitWatchDataPayload(
                "Bearer $token",
                deviceId, timestamp
            ).map { it.toDomainEntity() }
        } catch (e: Exception) {
            listOf()
        }
    }

    override suspend fun getFitbitWatchDailyData(
        token: String,
        deviceId: Long,
        timestamp: Long
    ): List<FitBitDailyPayloadEntity> {
        return try {
            userService.getFitbitWatchDailyDataPayload(
                "Bearer $token",
                deviceId, timestamp
            ).map { it.toDomainEntity() }
        } catch (e: Exception) {
            listOf()
        }
    }
}