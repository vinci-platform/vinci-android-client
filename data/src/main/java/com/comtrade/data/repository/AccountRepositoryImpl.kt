/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: AccountRepositoryImpl.kt
 * Author: sdelic
 *
 * History:  29.4.22. 17:28 created
 */

package com.comtrade.data.repository

import com.comtrade.data.network.api.UserService
import com.comtrade.data.network.models.devices.toDomainEntity
import com.comtrade.data.network.models.user.toDomainEntity
import com.comtrade.domain.contracts.repository.user.IAccountRepository
import com.comtrade.domain.entities.auth.UserAuthenticateResponseEntity
import com.comtrade.domain.entities.user.account.*
import com.comtrade.domain.entities.user.data.CreateUserRequest
import com.comtrade.domain.entities.user.data.UpdateUserRequest
import com.comtrade.domain.entities.user.data.UserAuthenticateRequest
import com.comtrade.domain.locale.SupportedLanguages
import com.google.gson.Gson
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.HttpException
import javax.inject.Inject

/**
 * The account repository implementation detail.
 */
internal class AccountRepositoryImpl @Inject constructor(
    private val gson: Gson,
    private val userService: UserService
) :
    IAccountRepository {
    override suspend fun authenticateUser(
        username: String,
        password: String,
        rememberMe: Boolean
    ): UserAuthenticateResponseEntity {
        val result =
            userService.authenticateUser(
                UserAuthenticateRequest(
                    username,
                    password,
                    rememberMe
                )
            )
        return UserAuthenticateResponseEntity(result.tokenId)
    }

    override suspend fun resetPassword(token: String, email: String): Boolean {
        //Creating a request body specifically for sending plain/text formatted request payload.
        val rawStringRequestBody: RequestBody =
            email.toRequestBody("text/plain".toMediaTypeOrNull())
        val response = userService.resetPassword(
            "Bearer $token",
            rawStringRequestBody
        )

        return try {
            response.isSuccessful
        } catch (e: HttpException) {
            e.printStackTrace()
            false
        }
    }

    override suspend fun resetPasswordFinish(resetPasswordFinishRequest: ResetPasswordFinishRequest): Boolean {
        val response = userService.resetPasswordFinish(resetPasswordFinishRequest)

        return try {
            response.isSuccessful
        } catch (e: HttpException) {
            e.printStackTrace()
            false
        }
    }

    override suspend fun changePassword(
        token: String,
        changePasswordRequestEntity: ChangePasswordRequestEntity
    ): Boolean {
        val response = userService.changePassword("Bearer $token", changePasswordRequestEntity)

        return try {
            response.isSuccessful
        } catch (e: HttpException) {
            e.printStackTrace()
            false
        }
    }

    override suspend fun updateAccount(
        token: String,
        updateUserRequest: UpdateUserRequest
    ): Boolean {
        val requestData = gson.toJson(updateUserRequest)
        //Creating a request body specifically for sending plain/text formatted request payload.
        val rawStringRequestBody: RequestBody =
            requestData.toRequestBody("text/plain".toMediaTypeOrNull())
        val response = userService.updateUser("Bearer $token", rawStringRequestBody)

        return try {
            response.isSuccessful
        } catch (e: HttpException) {
            e.printStackTrace()
            false
        }
    }

    override suspend fun getUser(token: String): GetUserEntity {
        return userService.getUser(
            "Bearer $token"
        ).toDomainEntity()
    }

    override suspend fun getUserExtra(token: String, id: Long): GetUserExtraEntity {
        return userService.getUserExtra("Bearer $token", id.toString()).toDomainEntity()
    }

    override suspend fun createUser(
        login: String,
        email: String,
        password: String,
        role: String
    ): Boolean {
        val response = userService.createUser(
            CreateUserRequest(
                login,
                email,
                password,
                SupportedLanguages.ENGLISH_LANGUAGE_KEY,
                role
            )
        )

        return try {
            response.isSuccessful
        } catch (e: HttpException) {
            e.printStackTrace()
            false
        }
    }

    @Suppress("UselessCallOnNotNull")
    override suspend fun getMobileAppSettings(
        token: String,
        userId: Long
    ): List<MobileAppSettingsEntity> {
        val response = userService.getMobileAppSettings(
            "Bearer $token",
            userId
        )

        return if (!response.isNullOrEmpty()) {
            response.map { it.toDomainEntity() }
        } else {
            emptyList()
        }
    }
}