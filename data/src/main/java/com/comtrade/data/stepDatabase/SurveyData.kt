/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: SurveyData.kt
 * Author: sdelic
 *
 * History:  22.3.22. 12:15 created
 */

package com.comtrade.data.stepDatabase

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.comtrade.domain.entities.survey.PostSurveyDataRequest
import com.comtrade.domain.entities.survey.SurveyDataModel
import java.time.Instant

@Entity
data class SurveyData(
    @PrimaryKey(autoGenerate = true)
    val id: Int,

    @ColumnInfo(name = "user_extra_id") var userExtraId: Long,
    @ColumnInfo(name = "scoring_result") var scoringResult: Long,
    @ColumnInfo(name = "survey_type") var surveyType: String,
    @ColumnInfo(name = "identifier") var identifier: String,
    @ColumnInfo(name = "created_time") var createdTime: Long,
    @ColumnInfo(name = "end_time") var endTime: Long,
    @ColumnInfo(name = "assessment_data") var assessmentData: String,
    @ColumnInfo(name = "additional_info") var additionalInfo: String,
    @ColumnInfo(name = "synced_with_server") var syncedWithServer: Boolean = false
) {

    companion object {
        fun fromDomainModel(surveyDataModel: SurveyDataModel): SurveyData = SurveyData(
            surveyDataModel.id,
            surveyDataModel.userExtraId,
            surveyDataModel.scoringResult,
            surveyDataModel.surveyType,
            surveyDataModel.identifier,
            surveyDataModel.createdTime,
            surveyDataModel.endTime,
            surveyDataModel.assessmentData,
            surveyDataModel.additionalInfo,
            surveyDataModel.syncedWithServer
        )
    }

    fun getRequest(): PostSurveyDataRequest {
        return PostSurveyDataRequest(
            userExtraId,
            scoringResult,
            surveyType,
            identifier,
            Instant.ofEpochMilli(createdTime).toString(),
            Instant.ofEpochMilli(endTime).toString(),
            assessmentData,
            additionalInfo
        )
    }
}

internal fun SurveyData.toDomainEntity(): SurveyDataModel = SurveyDataModel(
    id = id,
    userExtraId = userExtraId,
    scoringResult = scoringResult,
    surveyType = surveyType,
    identifier = identifier,
    createdTime = createdTime,
    endTime = endTime,
    assessmentData = assessmentData,
    additionalInfo = additionalInfo,
    syncedWithServer = syncedWithServer
)