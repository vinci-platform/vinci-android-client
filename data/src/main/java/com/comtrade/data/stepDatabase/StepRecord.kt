package com.comtrade.data.stepDatabase

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.comtrade.domain.entities.step.StepEntity

@Entity
data class StepRecord(
    @PrimaryKey(autoGenerate = true)
    val id: Int,

    @ColumnInfo(name = "device_id") val deviceId: Long,
    @ColumnInfo(name = "steps") var steps: Int,
    @ColumnInfo(name = "timestamp") val timestamp: Long,
    @ColumnInfo(name = "synced_with_server") var syncedWithServer: Boolean
) {
    companion object {
        fun fromDomainEntity(steps: StepEntity): StepRecord = StepRecord(
            id = steps.id,
            deviceId = steps.deviceId,
            steps = steps.steps,
            timestamp = steps.timestamp,
            syncedWithServer = steps.syncedWithServer
        )
    }
}

fun StepRecord.toDomainEntity(): StepEntity = StepEntity(
    id, deviceId, steps, timestamp, syncedWithServer
)

