package com.comtrade.data.stepDatabase

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.comtrade.domain.entities.friends.FriendsEntity

@Entity
data class FriendsRecord(
    @PrimaryKey(autoGenerate = true)
    val id: Int,

    @ColumnInfo(name = "user_id") val userId: Long,
    @ColumnInfo(name = "name") var name: String,
    @ColumnInfo(name = "phone") val phone: String,
    @ColumnInfo(name = "synced_with_server") val syncedWithServer: Boolean = false
) {
    companion object {
        fun fromDomainEntity(friendsEntity: FriendsEntity): FriendsRecord = FriendsRecord(
            id = friendsEntity.id,
            userId = friendsEntity.userId,
            name = friendsEntity.name,
            phone = friendsEntity.phone,
            syncedWithServer = friendsEntity.syncedWithServer
        )
    }
}

fun FriendsRecord.toDomainEntity(): FriendsEntity = FriendsEntity(
    id = id,
    userId = userId,
    name = name,
    phone = phone,
    syncedWithServer = syncedWithServer
)

