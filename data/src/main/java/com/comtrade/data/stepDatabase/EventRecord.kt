/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: EventRecord.kt
 * Author: sdelic
 *
 * History:  22.3.22. 12:15 created
 */

package com.comtrade.data.stepDatabase

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.comtrade.domain.entities.event.EventEntity

@Entity
data class EventRecord(
    @PrimaryKey(autoGenerate = true)
    var id: Int,

    @ColumnInfo(name = "user_id") val userId: Long,
    @ColumnInfo(name = "timestamp") var timestamp: Long,
    @ColumnInfo(name = "created") val created: Long,
    @ColumnInfo(name = "notify") val notify: Long,
    @ColumnInfo(name = "repeat") val repeat: String,
    @ColumnInfo(name = "title") val title: String,
    @ColumnInfo(name = "type") val type: String,
    @ColumnInfo(name = "text") val text: String,
    @ColumnInfo(name = "data") val data: String = "{}",
    @ColumnInfo(name = "synced_with_server") var syncedWithServer: Boolean = false,
    @ColumnInfo(name = "deleted") var deleted: Boolean = false
) {
    companion object {
        fun fromDomainEntity(eventEntity: EventEntity): EventRecord = EventRecord(
            eventEntity.id,
            eventEntity.userId,
            eventEntity.timestamp,
            eventEntity.created,
            eventEntity.notify,
            eventEntity.repeat,
            eventEntity.title,
            eventEntity.type,
            eventEntity.text,
            eventEntity.data,
            eventEntity.syncedWithServer,
            eventEntity.deleted
        )
    }
}

fun EventRecord.toDomainEntity(): EventEntity =
    EventEntity(
        this.id,
        this.userId,
        this.timestamp,
        this.created,
        this.notify,
        this.repeat,
        this.title,
        this.type,
        this.text,
        this.data,
        this.syncedWithServer,
        this.deleted
    )

