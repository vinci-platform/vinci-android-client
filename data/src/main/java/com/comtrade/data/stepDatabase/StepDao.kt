/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: StepDao.kt
 * Author: sdelic
 *
 * History:  21.2.22. 10:43 created
 */

package com.comtrade.data.stepDatabase

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update

@Dao
interface StepDao {

    @Query("SELECT * FROM StepRecord WHERE device_id=:deviceId AND timestamp=:timestamp")
    fun getStepRecord(deviceId: Long, timestamp: Long): StepRecord?

    @Update
    fun updateStepRecord(vararg stepRecord: StepRecord): Int

    @Query("SELECT * FROM StepRecord WHERE device_id=:deviceId AND synced_with_server=0")
    fun getStepsForSync(deviceId: Long): List<StepRecord>

    @Query("SELECT SUM(steps) FROM StepRecord WHERE device_id=:deviceId AND timestamp BETWEEN :start AND :end")
    fun getStepsForPeriod(deviceId: Long, start: Long, end: Long): Int

    @Insert
    fun insertAll(vararg users: StepRecord): List<Long>

    @Query("SELECT MAX(timestamp) FROM StepRecord WHERE device_id=:deviceId AND synced_with_server=1")
    fun getMaxSyncedTimestamp(deviceId: Long): Long

    @Query("SELECT MAX(timestamp) FROM StepDaily WHERE device_id=:deviceId")
    fun getMaxDailyTimestamp(deviceId: Long): Long

    @Query("SELECT * FROM StepDaily WHERE device_id=:deviceId AND timestamp >= :start")
    fun getDailyStepsForPeriod(deviceId: Long, start: Long): List<StepDaily>

    @Query("SELECT * FROM StepDaily WHERE device_id=:deviceId AND timestamp = :timestamp")
    fun getDailySteps(deviceId: Long, timestamp: Long): StepDaily?

    @Insert
    fun insertDaily(stepsDaily: StepDaily): Long

    @Update
    fun updateDaily(vararg stepsDaily: StepDaily): Int
}
