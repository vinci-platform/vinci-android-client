/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: StepDaily.kt
 * Author: sdelic
 *
 * History:  22.3.22. 12:15 created
 */

package com.comtrade.data.stepDatabase

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.comtrade.domain.entities.step.StepDailyEntity

@Entity
data class StepDaily(
    @PrimaryKey(autoGenerate = true)
    val id: Int,

    @ColumnInfo(name = "device_id") val deviceId: Long,
    @ColumnInfo(name = "steps") var steps: Int,
    @ColumnInfo(name = "activity_calories") var activityCalories: Int,
    @ColumnInfo(name = "calories_out") var calories_out: Int,
    @ColumnInfo(name = "heart_rate_min") var heartRateMin: Double,
    @ColumnInfo(name = "heart_rate_avg") var heartRateAvg: Double,
    @ColumnInfo(name = "heart_rate_max") var heartRateMax: Double,
    @ColumnInfo(name = "resting_heart_rate") var restingHeartRate: Double,
    @ColumnInfo(name = "minutes_asleep") var minutesAsleep: Int,
    @ColumnInfo(name = "minutes_in_bed") var minutesInBed: Int,
    @ColumnInfo(name = "timestamp") val timestamp: Long
) {
    companion object {
        fun fromDomainEntity(stepDailyEntity: StepDailyEntity): StepDaily = StepDaily(
            id = stepDailyEntity.id,
            deviceId = stepDailyEntity.deviceId,
            steps = stepDailyEntity.steps,
            activityCalories = stepDailyEntity.activityCalories,
            calories_out = stepDailyEntity.calories_out,
            heartRateMin = stepDailyEntity.heartRateMin,
            heartRateAvg = stepDailyEntity.heartRateAvg,
            heartRateMax = stepDailyEntity.heartRateMax,
            restingHeartRate = stepDailyEntity.restingHeartRate,
            minutesAsleep = stepDailyEntity.minutesAsleep,
            minutesInBed = stepDailyEntity.minutesInBed,
            timestamp = stepDailyEntity.timestamp
        )
    }
}

fun StepDaily.toDomainEntity(): StepDailyEntity = StepDailyEntity(
    id = id,
    deviceId = deviceId,
    steps = steps,
    activityCalories = activityCalories,
    calories_out = calories_out,
    heartRateMin = heartRateMin,
    heartRateAvg = heartRateAvg,
    heartRateMax = heartRateMax,
    restingHeartRate = restingHeartRate,
    minutesAsleep = minutesAsleep,
    minutesInBed = minutesInBed,
    timestamp = timestamp
)

