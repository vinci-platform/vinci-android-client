package com.comtrade.data.stepDatabase

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.comtrade.domain.entities.devices.data.CmdWatchEntity

@Entity
data class CmdRecord(
    @PrimaryKey(autoGenerate = true)
    val id: Int,

    @ColumnInfo(name = "device_id") val deviceId: Long,
    @ColumnInfo(name = "steps") var steps: Int,
    @ColumnInfo(name = "timestamp") val timestamp: Long
) {
    companion object {
        fun fromDomainEntity(cmdWatchEntity: CmdWatchEntity): CmdRecord = CmdRecord(
            id = cmdWatchEntity.id?.toInt() ?: 0,
            deviceId = cmdWatchEntity.deviceId,
            steps = cmdWatchEntity.steps?.toInt() ?: 0,
            timestamp = cmdWatchEntity.timestamp
        )
    }
}

