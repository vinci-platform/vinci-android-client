package com.comtrade.data.stepDatabase

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.comtrade.domain.entities.health.HealthEntity

@Entity
class HealthRecord(
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
    @ColumnInfo(name = "user_id") var userId: Long = 0,
    @ColumnInfo(name = "timestamp") var timestamp: Long = 0,
    @ColumnInfo(name = "score") var score: Double = 0.0,
    @ColumnInfo(name = "available_score") var available_score: Double = 0.0,
    @ColumnInfo(name = "steps_score") var stepsScore: Double = 0.0,
    @ColumnInfo(name = "ipaq_score") var ipaqScore: Double = 0.0,
    @ColumnInfo(name = "whoqol_score") var whoqolScore: Double = 0.0,
    @ColumnInfo(name = "feelings_score") var feelingsScore: Double = 0.0,
    @ColumnInfo(name = "synced_with_server") var syncedWithServer: Boolean = false
) {
    companion object {
        fun fromDomainEntity(healthEntity: HealthEntity): HealthRecord = HealthRecord(
            id = healthEntity.id,
            userId = healthEntity.userId,
            timestamp = healthEntity.timestamp,
            score = healthEntity.score,
            available_score = healthEntity.available_score,
            stepsScore = healthEntity.stepsScore,
            ipaqScore = healthEntity.ipaqScore,
            whoqolScore = healthEntity.whoqolScore,
            feelingsScore = healthEntity.feelingsScore,
            syncedWithServer = healthEntity.syncedWithServer
        )
    }
}

internal fun HealthRecord.toDomainEntity(): HealthEntity = HealthEntity(
    id = this.id,
    userId = this.userId,
    timestamp = this.timestamp,
    score = this.score,
    available_score = this.available_score,
    stepsScore = this.stepsScore,
    ipaqScore = this.ipaqScore,
    whoqolScore = this.whoqolScore,
    feelingsScore = this.feelingsScore,
    syncedWithServer = this.syncedWithServer
)