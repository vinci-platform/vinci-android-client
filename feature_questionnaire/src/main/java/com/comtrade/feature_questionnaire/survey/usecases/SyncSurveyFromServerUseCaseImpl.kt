/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: SyncSurveyFromServerUseCaseImpl.kt
 * Author: sdelic
 *
 * History:  2.6.22. 16:33 created
 */

package com.comtrade.feature_questionnaire.survey.usecases

import com.comtrade.domain.contracts.repository.IAppRepository
import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.contracts.repository.user.UserServiceRepository
import com.comtrade.domain.contracts.usecases.survey.ISaveSurveyDataUseCase
import com.comtrade.domain.contracts.usecases.survey.ISyncSurveyFromServerUseCase
import com.comtrade.domain.entities.survey.SurveyDataModel
import java.time.Instant
import javax.inject.Inject

/**
 * Implementation detail for the synchronization of the survey data from the backend use case
 */
internal class SyncSurveyFromServerUseCaseImpl @Inject constructor(
    private val prefs: IPrefs,
    private val appRepository: IAppRepository,
    private val saveSurveyDataUseCase: ISaveSurveyDataUseCase,
    private val userServiceRepository: UserServiceRepository,
) :
    ISyncSurveyFromServerUseCase {

    override suspend fun syncSurveyFromServer(): Boolean {
        val maxDatabaseDate = Instant.ofEpochMilli(appRepository.getMaxSynced())

        val token = prefs.token

        if (token.isBlank()) {
            throw Exception("No valid User token provided: $token")
        }

        val surveyDevice = prefs.surveyDevice
            ?: throw Exception("No survey device found currently attached to the User profile.")

        val result =
            userServiceRepository.getSurveyData(
                token,
                surveyDevice.deviceId,
                maxDatabaseDate.toString()
            )
        if (result.isNotEmpty()) {
            prefs.addLog("Received survey data from server: $result")
            for (data in result) {
                if (Instant.parse(data.created_time!!) > maxDatabaseDate) {
                    val insertionResult = saveSurveyDataUseCase(
                        SurveyDataModel(
                            data.id.toInt(),
                            prefs.userExtraId,
                            data.scoring_result!!,
                            data.survey_type!!,
                            data.identifier!!,
                            Instant.parse(data.created_time!!).toEpochMilli(),
                            Instant.parse(data.end_time!!).toEpochMilli(),
                            data.assessment_data!!,
                            data.additional_info!!,
                            true
                        )
                    )

                    if (insertionResult < 1) {
                        return false
                    }
                }
            }
        }

        return true
    }
}