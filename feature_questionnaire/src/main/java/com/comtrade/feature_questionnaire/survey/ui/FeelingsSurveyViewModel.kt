/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: SurveyViewModel.kt
 * Author: sdelic
 *
 * History:  14.4.22. 12:53 created
 */

package com.comtrade.feature_questionnaire.survey.ui

import androidx.lifecycle.*
import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.contracts.usecases.survey.ISaveSurveyDataUseCase
import com.comtrade.domain.entities.survey.SurveyDataModel
import com.comtrade.domain.survey.SurveyTypes
import com.comtrade.feature_questionnaire.survey.ui.FeelingsSurveyViewModel.SaveSurveyRequest.DefaultState
import com.comtrade.feature_questionnaire.survey.ui.FeelingsSurveyViewModel.SaveSurveyRequest.SaveSurvey
import com.comtrade.feature_questionnaire.survey.ui.state.SaveSurveyUIState
import kotlinx.coroutines.Dispatchers
import java.time.Instant
import javax.inject.Inject

/**
 * Android ViewModel component for handling the various survey use cases.
 */
class FeelingsSurveyViewModel @Inject constructor(
    private val prefs: IPrefs,
    private val saveSurveyDataUseCase: ISaveSurveyDataUseCase
) : ViewModel() {

    private val storeSurveyDataRequest =
        MutableLiveData<SaveSurveyRequest>(DefaultState)
    val storePointsResult: LiveData<SaveSurveyUIState> =
        Transformations.switchMap(storeSurveyDataRequest) {
            liveData(context = viewModelScope.coroutineContext + Dispatchers.IO) {

                when (it) {
                    DefaultState -> emit(SaveSurveyUIState.DefaultState)
                    is SaveSurvey -> {
                        emit(SaveSurveyUIState.OnLoading)
                        try {
                            val result = saveSurveyDataUseCase(it.payload)
                            if (result > 0) {
                                emit(SaveSurveyUIState.OnSuccess(true))
                            } else {
                                emit(SaveSurveyUIState.OnSuccess(true))
                            }

                        } catch (e: Exception) {
                            e.printStackTrace()
                            emit(SaveSurveyUIState.OnError(e.message))
                        }
                    }
                }
            }
        }


    fun storeFeelingPoints(points: Int) {
        saveSurvey(
            SurveyDataModel(
                0,
                prefs.userId,
                points.toLong(),
                SurveyTypes.FEELINGS,
                "",
                Instant.now().toEpochMilli(),
                Instant.now().toEpochMilli(),
                "",
                ""
            )
        )
    }

    fun resetState() {
        storeSurveyDataRequest.value = DefaultState
    }

    private fun saveSurvey(surveyDataModel: SurveyDataModel) {
        storeSurveyDataRequest.value = SaveSurvey(surveyDataModel)

    }

    /**
     * Save health points request states.
     * @property SaveSurvey the request state with the request payload.
     * @property DefaultState the reset/default state for an request. Dispatching the event to be handled by some other observing component.
     */
    private sealed class SaveSurveyRequest {
        data class SaveSurvey(val payload: SurveyDataModel) : SaveSurveyRequest()
        object DefaultState : SaveSurveyRequest()
    }
}