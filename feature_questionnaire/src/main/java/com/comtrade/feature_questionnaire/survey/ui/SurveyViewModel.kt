/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: SurveyViewModel.kt
 * Author: sdelic
 *
 * History:  16.6.22. 15:17 created
 */

package com.comtrade.feature_questionnaire.survey.ui

import androidx.lifecycle.*
import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.contracts.usecases.survey.ISaveSurveyDataUseCase
import com.comtrade.domain.entities.survey.SurveyDataModel
import com.comtrade.domain.survey.SurveyTypes
import com.comtrade.domain_ui.state.RequestUIState
import com.comtrade.feature_questionnaire.survey.ui.SurveyViewModel.SaveSurveyRequest.DefaultState
import com.comtrade.feature_questionnaire.survey.ui.SurveyViewModel.SaveSurveyRequest.SaveSurvey
import com.google.gson.Gson
import kotlinx.coroutines.Dispatchers
import java.time.Instant
import javax.inject.Inject

/**
 * Android ViewModel component for starting/stopping and persisting the survey process.
 */
class SurveyViewModel @Inject constructor(
    private val prefs: IPrefs,
    private val saveSurveyDataUseCase: ISaveSurveyDataUseCase,
    private val gson: Gson
) : ViewModel() {

    private var startTime: Instant? = null
    private var endTime: Instant = Instant.MIN
    var assessmentData: MutableMap<Int, String>? = null
    var questionnaireQolSurveyAnswers: MutableMap<String, String>? = null
    var questionnaireUserNeedsAnswers: MutableMap<String, String>? = null
    private var scoringResult: Long = 0

    private var activeSurveyType: String? = null

    private val saveSurveyRequest =
        MutableLiveData<SaveSurveyRequest>(DefaultState)
    val saveSurveyObserve: LiveData<RequestUIState<Boolean>> =
        Transformations.switchMap(saveSurveyRequest) {
            liveData(context = viewModelScope.coroutineContext + Dispatchers.IO) {
                when (it) {
                    DefaultState -> emit(RequestUIState.DefaultState)
                    is SaveSurvey -> {
                        try {
                            val result =
                                saveSurveyDataUseCase(it.surveyDataModel)
                            if (result > 0) {
                                emit(RequestUIState.OnSuccess(true))
                            } else {
                                emit(RequestUIState.OnError(null))
                            }

                        } catch (e: Exception) {
                            e.printStackTrace()
                            emit(RequestUIState.OnError(e.message))
                        }
                    }
                }
            }
        }

    /**
     *  Initialize the survey process.
     */
    fun startSurvey(surveyType: String) {
        //stop any active survey
        resetValues()

        activeSurveyType = surveyType
        startTime = Instant.now()

        if (surveyType in setOf(
                SurveyTypes.IPAQ,
                SurveyTypes.ELDERLY
            )
        ) {
            assessmentData = mutableMapOf()
        }

    }

    fun isSurveyActive(): Boolean = activeSurveyType != null

    fun setScore(newScoreValue: Long) {
        this.scoringResult = newScoreValue
    }

    fun getScore(): Long = this.scoringResult

    /**
     * Stops any active surveys and persists the data to the local storage for later synchronization.
     */
    fun stopAndSaveActiveSurvey() {
        activeSurveyType?.let { surveyType ->
            endTime = Instant.now()
            startTime?.let { start ->
                val assessmentString =
                    when {
                        assessmentData != null -> gson.toJson(assessmentData)
                        questionnaireQolSurveyAnswers != null -> gson.toJson(
                            questionnaireQolSurveyAnswers
                        )
                        else -> gson.toJson(questionnaireUserNeedsAnswers)
                    }
                val saveData = SurveyDataModel(
                    0,
                    prefs.userId,
                    scoringResult,
                    surveyType,
                    "",
                    start.toEpochMilli(),
                    endTime.toEpochMilli(),
                    assessmentString,
                    ""
                )
                saveSurveyRequest.value = SaveSurvey(saveData)
            }

        }

        activeSurveyType = null//reset

    }

    /**
     * Reset the survey state.
     */
    fun resetState() {
        resetValues()
        saveSurveyRequest.value = DefaultState
    }

    /**
     * Set default values for properties.
     */
    private fun resetValues() {
        activeSurveyType = null
        startTime = null
        endTime = Instant.MIN
        assessmentData = null
        questionnaireQolSurveyAnswers = null
        questionnaireUserNeedsAnswers = null
        scoringResult = 0
    }

    /**
     * Save survey data to the local database request states.
     * @property SaveSurvey the request state.
     * @property DefaultState the reset/default state for an request. Dispatching the event to be handled by some other observing component.
     */
    private sealed class SaveSurveyRequest {
        data class SaveSurvey(val surveyDataModel: SurveyDataModel) : SaveSurveyRequest()
        object DefaultState : SaveSurveyRequest()
    }
}