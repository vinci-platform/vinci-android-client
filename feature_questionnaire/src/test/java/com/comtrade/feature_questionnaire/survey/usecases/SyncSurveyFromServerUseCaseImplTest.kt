/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: SyncSurveyFromServerUseCaseImplTest.kt
 * Author: sdelic
 *
 * History:  2.6.22. 16:39 created
 */

package com.comtrade.feature_questionnaire.survey.usecases

import com.comtrade.domain.contracts.repository.IAppRepository
import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.contracts.repository.user.UserServiceRepository
import com.comtrade.domain.contracts.usecases.survey.ISaveSurveyDataUseCase
import com.comtrade.domain.contracts.usecases.survey.ISyncSurveyFromServerUseCase
import com.comtrade.domain.entities.devices.SurveyDevice
import com.comtrade.domain.entities.survey.SurveyDataEntity
import com.comtrade.domain.entities.survey.SurveyDataModel
import com.comtrade.domain.entities.user.data.GetUserDevicesEntity
import com.comtrade.domain.settings.SupportedDeviceTypes
import com.comtrade.domain.survey.SurveyTypes
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import java.time.Instant
import java.util.*

/**
 * Test suite for the synchronization of the survey data from the backend use case implementation detail.
 */
internal class SyncSurveyFromServerUseCaseImplTest {

    private val testMaxSync = 10L
    private val testDeviceId = 1L
    private val testUserToken = "testUserToken"
    private val testSurveyDataSize = 1_000

    private lateinit var prefs: IPrefs
    private lateinit var appRepository: IAppRepository
    private lateinit var saveSurveyDataUseCase: ISaveSurveyDataUseCase
    private lateinit var userServiceRepository: UserServiceRepository
    private lateinit var syncSurveyFromServerUseCase: ISyncSurveyFromServerUseCase

    @Before
    fun setUp() {
        prefs = Mockito.mock(IPrefs::class.java)
        appRepository = Mockito.mock(IAppRepository::class.java)
        saveSurveyDataUseCase = Mockito.mock(ISaveSurveyDataUseCase::class.java)
        userServiceRepository = Mockito.mock(UserServiceRepository::class.java)
        syncSurveyFromServerUseCase =
            SyncSurveyFromServerUseCaseImpl(
                prefs,
                appRepository,
                saveSurveyDataUseCase,
                userServiceRepository
            )
    }

    @Test
    fun `given valid input data when called return true`() {
        runBlocking {
            val testSurveyDevice = provideTestSurveyDevice()
            val testSurveyData = provideTestSurveyData()
            val convertedTime = Instant.ofEpochMilli(testMaxSync)
            Mockito.`when`(appRepository.getMaxSynced()).thenReturn(testMaxSync)
            Mockito.`when`(prefs.token).thenReturn(testUserToken)
            Mockito.`when`(prefs.surveyDevice).thenReturn(testSurveyDevice)
            Mockito.`when`(
                userServiceRepository.getSurveyData(
                    testUserToken,
                    testDeviceId,
                    convertedTime.toString()
                )
            ).thenReturn(testSurveyData)
            Mockito.`when`(saveSurveyDataUseCase(any(SurveyDataModel::class.java))).thenReturn(1)

            //execute call
            val result = syncSurveyFromServerUseCase.syncSurveyFromServer()

            Mockito.verify(appRepository, Mockito.times(1)).getMaxSynced()
            Mockito.verify(prefs, Mockito.times(1)).token
            Mockito.verify(prefs, Mockito.times(1)).surveyDevice
            Mockito.verify(userServiceRepository, Mockito.times(1)).getSurveyData(
                testUserToken,
                testDeviceId,
                convertedTime.toString()
            )
            Mockito.verify(saveSurveyDataUseCase, Mockito.times(testSurveyDataSize))
                .invoke(any(SurveyDataModel::class.java))
            assertTrue(result)
        }
    }

    @Test(expected = Exception::class)
    fun `given an invalid user token valid input data when called throw exception`() {
        runBlocking {
            val testSurveyDevice = provideTestSurveyDevice()
            val testSurveyData = provideTestSurveyData()
            val convertedTime = Instant.ofEpochMilli(testMaxSync)
            Mockito.`when`(appRepository.getMaxSynced()).thenReturn(testMaxSync)
            Mockito.`when`(prefs.token).thenReturn("")
            Mockito.`when`(prefs.surveyDevice).thenReturn(testSurveyDevice)
            Mockito.`when`(
                userServiceRepository.getSurveyData(
                    testUserToken,
                    testDeviceId,
                    convertedTime.toString()
                )
            ).thenReturn(testSurveyData)
            Mockito.`when`(saveSurveyDataUseCase(any(SurveyDataModel::class.java))).thenReturn(1)

            //execute call
            val result = syncSurveyFromServerUseCase.syncSurveyFromServer()

            Mockito.verify(appRepository, Mockito.times(1)).getMaxSynced()
            Mockito.verify(prefs, Mockito.times(1)).token

            assertFalse(result)
        }
    }

    @Test(expected = Exception::class)
    fun `given no survey device when called throw exception`() {
        runBlocking {
            val testSurveyData = provideTestSurveyData()
            val convertedTime = Instant.ofEpochMilli(testMaxSync)
            Mockito.`when`(appRepository.getMaxSynced()).thenReturn(testMaxSync)
            Mockito.`when`(prefs.token).thenReturn(testUserToken)
            Mockito.`when`(prefs.surveyDevice).thenReturn(null)
            Mockito.`when`(
                userServiceRepository.getSurveyData(
                    testUserToken,
                    testDeviceId,
                    convertedTime.toString()
                )
            ).thenReturn(testSurveyData)
            Mockito.`when`(saveSurveyDataUseCase(any(SurveyDataModel::class.java))).thenReturn(1)

            //execute call
            val result = syncSurveyFromServerUseCase.syncSurveyFromServer()

            Mockito.verify(appRepository, Mockito.times(1)).getMaxSynced()
            Mockito.verify(prefs, Mockito.times(1)).token
            Mockito.verify(prefs, Mockito.times(1)).surveyDevice

            assertFalse(result)
        }
    }

    @Test
    fun `given valid input data when called return false for data insertion`() {
        runBlocking {
            val testSurveyDevice = provideTestSurveyDevice()
            val testSurveyData = provideTestSurveyData()
            val convertedTime = Instant.ofEpochMilli(testMaxSync)
            Mockito.`when`(appRepository.getMaxSynced()).thenReturn(testMaxSync)
            Mockito.`when`(prefs.token).thenReturn(testUserToken)
            Mockito.`when`(prefs.surveyDevice).thenReturn(testSurveyDevice)
            Mockito.`when`(
                userServiceRepository.getSurveyData(
                    testUserToken,
                    testDeviceId,
                    convertedTime.toString()
                )
            ).thenReturn(testSurveyData)
            Mockito.`when`(saveSurveyDataUseCase(any(SurveyDataModel::class.java))).thenReturn(-1)

            //execute call
            val result = syncSurveyFromServerUseCase.syncSurveyFromServer()

            Mockito.verify(appRepository, Mockito.times(1)).getMaxSynced()
            Mockito.verify(prefs, Mockito.times(1)).token
            Mockito.verify(prefs, Mockito.times(1)).surveyDevice
            Mockito.verify(userServiceRepository, Mockito.times(1)).getSurveyData(
                testUserToken,
                testDeviceId,
                convertedTime.toString()
            )
            Mockito.verify(saveSurveyDataUseCase, Mockito.times(1))
                .invoke(any(SurveyDataModel::class.java))
            assertFalse(result)
        }
    }

    @Test
    fun `given valid input data when called return true for empty survey data`() {
        runBlocking {
            val testSurveyDevice = provideTestSurveyDevice()
            val testSurveyData = emptyList<SurveyDataEntity>()
            val convertedTime = Instant.ofEpochMilli(testMaxSync)
            Mockito.`when`(appRepository.getMaxSynced()).thenReturn(testMaxSync)
            Mockito.`when`(prefs.token).thenReturn(testUserToken)
            Mockito.`when`(prefs.surveyDevice).thenReturn(testSurveyDevice)
            Mockito.`when`(
                userServiceRepository.getSurveyData(
                    testUserToken,
                    testDeviceId,
                    convertedTime.toString()
                )
            ).thenReturn(testSurveyData)

            //execute call
            val result = syncSurveyFromServerUseCase.syncSurveyFromServer()

            Mockito.verify(appRepository, Mockito.times(1)).getMaxSynced()
            Mockito.verify(prefs, Mockito.times(1)).token
            Mockito.verify(prefs, Mockito.times(1)).surveyDevice
            Mockito.verify(userServiceRepository, Mockito.times(1)).getSurveyData(
                testUserToken,
                testDeviceId,
                convertedTime.toString()
            )
            assertTrue(result)
        }
    }

    private fun provideTestSurveyDevice(): SurveyDevice = SurveyDevice(
        GetUserDevicesEntity(
            deviceId = testDeviceId,
            name = "Survey Test device",
            description = "",
            uuid = UUID.randomUUID().toString(),
            deviceType = SupportedDeviceTypes.SURVEY.name,
            active = true,
        )
    )

    private fun provideTestSurveyData(): List<SurveyDataEntity> {
        val testData = mutableListOf<SurveyDataEntity>()

        for (i in 0 until testSurveyDataSize) {
            testData.add(
                SurveyDataEntity(
                    id = i.toLong(),
                    user_extra_id = -1,
                    scoring_result = 100,
                    survey_type = SurveyTypes.IPAQ,
                    identifier = "1",
                    created_time = Instant.now().toString(),
                    end_time = Instant.now().toString(),
                    assessment_data = "",
                    additional_info = ""
                )
            )
        }

        return testData
    }
}