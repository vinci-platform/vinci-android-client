/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: SaveSurveyUseCaseImplTest.kt
 * Author: sdelic
 *
 * History:  5.5.22. 09:59 created
 */

package com.comtrade.feature_questionnaire.survey.usecases

import com.comtrade.domain.contracts.repository.IAppRepository
import com.comtrade.domain.contracts.usecases.survey.ISaveSurveyDataUseCase
import com.comtrade.domain.entities.survey.SurveyDataModel
import com.comtrade.domain.survey.SurveyTypes
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.times

/**
 * Test scenario for the survey data persistence use case.
 */
internal class SaveSurveyUseCaseImplTest {
    private val testUserExtraId = 1L
    private lateinit var appRepository: IAppRepository
    private lateinit var saveSurveyDataUseCase: ISaveSurveyDataUseCase

    @Before
    fun setUp() {
        appRepository = Mockito.mock(IAppRepository::class.java)
        saveSurveyDataUseCase = SaveSurveyUseCaseImpl(appRepository)
    }

    @Test
    fun `given a valid input when save survey return 1`() {
        runBlocking {
            val testData = getSurveyData()
            Mockito.`when`(appRepository.insertNewSurveyData(testData)).thenReturn(1)
            val result = saveSurveyDataUseCase(testData)
            Mockito.verify(appRepository, times(1)).insertNewSurveyData(testData)

            assertNotNull(result)
            assertEquals(1, result)
        }
    }

    @Test
    fun `given a valid input when save survey return -1`() {
        runBlocking {
            val testData = getSurveyData()
            Mockito.`when`(appRepository.insertNewSurveyData(testData)).thenReturn(-1)
            val result = saveSurveyDataUseCase(testData)
            Mockito.verify(appRepository, times(1)).insertNewSurveyData(testData)

            assertNotNull(result)
            assertEquals(-1, result)
        }
    }

    @Test
    fun `given an invalid input when save survey return -1`() {
        runBlocking {
            val testData = getSurveyData().copy(userExtraId = -1)
            Mockito.`when`(appRepository.insertNewSurveyData(testData)).thenReturn(-1)
            val result = saveSurveyDataUseCase(testData)
            Mockito.verify(appRepository, times(1)).insertNewSurveyData(testData)

            assertNotNull(result)
            assertEquals(-1, result)
        }
    }


    private fun getSurveyData(): SurveyDataModel = SurveyDataModel(
        id = 1,
        userExtraId = testUserExtraId,
        scoringResult = 100,
        surveyType = SurveyTypes.WHOQOL_BREF,
        identifier = "",
        createdTime = 1_000L,
        endTime = 2_000L,
        assessmentData = "",
        additionalInfo = "",
        syncedWithServer = false
    )
}