/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: SyncSurveyToVinciUseCaseImplTest.kt
 * Author: sdelic
 *
 * History:  16.5.22. 16:05 created
 */

package com.comtrade.feature_questionnaire.survey.usecases

import com.comtrade.domain.contracts.repository.IAppRepository
import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.contracts.repository.user.UserServiceRepository
import com.comtrade.domain.contracts.usecases.survey.ISyncSurveyToVinciUseCase
import com.comtrade.domain.entities.survey.PostSurveyDataRequest
import com.comtrade.domain.entities.survey.SurveyDataModel
import com.comtrade.domain.survey.SurveyTypes
import kotlinx.coroutines.runBlocking
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.anyLong
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mockito
import java.time.Instant

/**
 * Test scenario for the synchronizing of the survey state to the Vinci server use case implementation detail.
 */
internal class SyncSurveyToVinciUseCaseImplTest {
    private val testUserId = 1L
    private val testToken = "testToken"
    private val numberOfSurveyDataModels = 1_000

    private lateinit var prefs: IPrefs
    private lateinit var appRepository: IAppRepository
    private lateinit var userServiceRepository: UserServiceRepository
    private lateinit var syncSurveyToVinciUseCase: ISyncSurveyToVinciUseCase

    @Before
    fun setUp() {
        prefs = Mockito.mock(IPrefs::class.java)
        appRepository = Mockito.mock(IAppRepository::class.java)
        userServiceRepository = Mockito.mock(UserServiceRepository::class.java)
        syncSurveyToVinciUseCase =
            SyncSurveyToVinciUseCaseImpl(prefs, userServiceRepository, appRepository)
    }

    @Test
    fun `given valid input data when called return true`() {
        runBlocking {
            val testSurveyData = provideTestSurveyRecords()
            Mockito.`when`(prefs.userId).thenReturn(testUserId)
            Mockito.`when`(prefs.token).thenReturn(testToken)
            Mockito.`when`(appRepository.getRecordsForSync(testUserId)).thenReturn(testSurveyData)
            Mockito.`when`(
                userServiceRepository.postSurveyData(
                    anyString(), anyLong(), any(
                        PostSurveyDataRequest::class.java
                    )
                )
            ).thenReturn(true)
            Mockito.`when`(appRepository.updateSurveyRecord(any(SurveyDataModel::class.java)))
                .thenReturn(1)

            //Make call
            val result = syncSurveyToVinciUseCase.syncSurveyToVinci()

            Mockito.verify(prefs, Mockito.times(1)).userId
            Mockito.verify(prefs, Mockito.times(1)).token
            Mockito.verify(appRepository, Mockito.times(1)).getRecordsForSync(testUserId)
            Mockito.verify(userServiceRepository, Mockito.times(numberOfSurveyDataModels))
                .postSurveyData(
                    anyString(), anyLong(), any(
                        PostSurveyDataRequest::class.java
                    )
                )
            Mockito.verify(appRepository, Mockito.times(numberOfSurveyDataModels))
                .updateSurveyRecord(
                    any(
                        SurveyDataModel::class.java
                    )
                )

            assertNotNull(result)
            assertTrue(result)
        }
    }

    @Test
    fun `given valid input data when called return false for post survey`() {
        runBlocking {
            val testSurveyData = provideTestSurveyRecords()
            Mockito.`when`(prefs.userId).thenReturn(testUserId)
            Mockito.`when`(prefs.token).thenReturn(testToken)
            Mockito.`when`(appRepository.getRecordsForSync(testUserId)).thenReturn(testSurveyData)
            Mockito.`when`(
                userServiceRepository.postSurveyData(
                    anyString(), anyLong(), any(
                        PostSurveyDataRequest::class.java
                    )
                )
            ).thenReturn(false)
            Mockito.`when`(appRepository.updateSurveyRecord(any(SurveyDataModel::class.java)))
                .thenReturn(1)

            //Make call
            val result = syncSurveyToVinciUseCase.syncSurveyToVinci()

            Mockito.verify(prefs, Mockito.times(1)).userId
            Mockito.verify(prefs, Mockito.times(1)).token
            Mockito.verify(appRepository, Mockito.times(1)).getRecordsForSync(testUserId)
            Mockito.verify(userServiceRepository, Mockito.times(1))
                .postSurveyData(
                    anyString(), anyLong(), any(
                        PostSurveyDataRequest::class.java
                    )
                )
            Mockito.verify(appRepository, Mockito.times(0))
                .updateSurveyRecord(
                    any(
                        SurveyDataModel::class.java
                    )
                )

            assertNotNull(result)
            assertFalse(result)
        }
    }

    @Test
    fun `given valid input data when called return false for database update`() {
        runBlocking {
            val testSurveyData = provideTestSurveyRecords()
            Mockito.`when`(prefs.userId).thenReturn(testUserId)
            Mockito.`when`(prefs.token).thenReturn(testToken)
            Mockito.`when`(appRepository.getRecordsForSync(testUserId)).thenReturn(testSurveyData)
            Mockito.`when`(
                userServiceRepository.postSurveyData(
                    anyString(), anyLong(), any(
                        PostSurveyDataRequest::class.java
                    )
                )
            ).thenReturn(true)
            Mockito.`when`(appRepository.updateSurveyRecord(any(SurveyDataModel::class.java)))
                .thenReturn(0)

            //Make call
            val result = syncSurveyToVinciUseCase.syncSurveyToVinci()

            Mockito.verify(prefs, Mockito.times(1)).userId
            Mockito.verify(prefs, Mockito.times(1)).token
            Mockito.verify(appRepository, Mockito.times(1)).getRecordsForSync(testUserId)
            Mockito.verify(userServiceRepository, Mockito.times(1))
                .postSurveyData(
                    anyString(), anyLong(), any(
                        PostSurveyDataRequest::class.java
                    )
                )
            Mockito.verify(appRepository, Mockito.times(1))
                .updateSurveyRecord(
                    any(
                        SurveyDataModel::class.java
                    )
                )

            assertNotNull(result)
            assertFalse(result)
        }
    }

    @Test(expected = Exception::class)
    fun `given an invalid user Id when called thrown exception`() {
        runBlocking {
            val testSurveyData = provideTestSurveyRecords()
            Mockito.`when`(prefs.userId).thenReturn(null)
            Mockito.`when`(prefs.token).thenReturn(testToken)
            Mockito.`when`(appRepository.getRecordsForSync(testUserId)).thenReturn(testSurveyData)
            Mockito.`when`(
                userServiceRepository.postSurveyData(
                    anyString(), anyLong(), any(
                        PostSurveyDataRequest::class.java
                    )
                )
            ).thenReturn(true)
            Mockito.`when`(appRepository.updateSurveyRecord(any(SurveyDataModel::class.java)))
                .thenReturn(0)

            //Make call
            val result = syncSurveyToVinciUseCase.syncSurveyToVinci()

            Mockito.verify(prefs, Mockito.times(1)).userId
            Mockito.verify(prefs, Mockito.times(1)).token
            Mockito.verify(appRepository, Mockito.times(1)).getRecordsForSync(testUserId)
            Mockito.verify(userServiceRepository, Mockito.times(1))
                .postSurveyData(
                    anyString(), anyLong(), any(
                        PostSurveyDataRequest::class.java
                    )
                )
            Mockito.verify(appRepository, Mockito.times(1))
                .updateSurveyRecord(
                    any(
                        SurveyDataModel::class.java
                    )
                )

            assertNotNull(result)
            assertFalse(result)
        }
    }

    @Test(expected = Exception::class)
    fun `given an invalid token when called thrown exception`() {
        runBlocking {
            val testSurveyData = provideTestSurveyRecords()
            Mockito.`when`(prefs.userId).thenReturn(testUserId)
            Mockito.`when`(prefs.token).thenReturn(null)
            Mockito.`when`(appRepository.getRecordsForSync(testUserId)).thenReturn(testSurveyData)
            Mockito.`when`(
                userServiceRepository.postSurveyData(
                    anyString(), anyLong(), any(
                        PostSurveyDataRequest::class.java
                    )
                )
            ).thenReturn(true)
            Mockito.`when`(appRepository.updateSurveyRecord(any(SurveyDataModel::class.java)))
                .thenReturn(0)

            //Make call
            val result = syncSurveyToVinciUseCase.syncSurveyToVinci()

            Mockito.verify(prefs, Mockito.times(1)).userId
            Mockito.verify(prefs, Mockito.times(1)).token
            Mockito.verify(appRepository, Mockito.times(1)).getRecordsForSync(testUserId)
            Mockito.verify(userServiceRepository, Mockito.times(1))
                .postSurveyData(
                    anyString(), anyLong(), any(
                        PostSurveyDataRequest::class.java
                    )
                )
            Mockito.verify(appRepository, Mockito.times(1))
                .updateSurveyRecord(
                    any(
                        SurveyDataModel::class.java
                    )
                )

            assertNotNull(result)
            assertFalse(result)
        }
    }

    private fun provideTestSurveyRecords(): List<SurveyDataModel> {

        val testData = mutableListOf<SurveyDataModel>()

        val surveyTypes = arrayOf(
            SurveyTypes.FEELINGS,
            SurveyTypes.IPAQ,
            SurveyTypes.WHOQOL_BREF
        )

        for (i in 0 until numberOfSurveyDataModels) {
            testData.add(
                SurveyDataModel(
                    id = i,
                    userExtraId = 1L,
                    scoringResult = 10,
                    surveyType = surveyTypes.random(),
                    identifier = "",
                    createdTime = Instant.now().toEpochMilli(),
                    endTime = Instant.now().toEpochMilli(),
                    assessmentData = "",
                    additionalInfo = "",
                    syncedWithServer = false
                )
            )
        }

        return testData
    }
}

/**
 * Kotlin null type handling for Mockito.
 */
internal fun <T> any(type: Class<T>): T = Mockito.any(type)