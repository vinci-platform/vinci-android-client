/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: GetFriendByPhoneViewModel.kt
 * Author: sdelic
 *
 * History:  6.6.22. 12:40 created
 */

package com.comtrade.feature_friends.ui

import androidx.lifecycle.*
import com.comtrade.domain.contracts.usecases.friends.IAddFriendUseCase
import com.comtrade.domain.contracts.usecases.friends.IDeleteFriendUseCase
import com.comtrade.domain.contracts.usecases.friends.IGetFriendByPhoneUseCase
import com.comtrade.domain_ui.state.RequestUIState
import com.comtrade.feature_friends.ui.GetFriendByPhoneViewModel.GetContactRequest.DefaultState
import com.comtrade.feature_friends.ui.GetFriendByPhoneViewModel.GetContactRequest.GetContactByPhone
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

/**
 * Android ViewModel component for obtaining contact information and handling of the UI state.
 */
class GetFriendByPhoneViewModel @Inject constructor(
    private val getFriendByPhoneUseCase: IGetFriendByPhoneUseCase,
    private val deleteFriendUseCase: IDeleteFriendUseCase,
    private val addFriendUseCase: IAddFriendUseCase
) :
    ViewModel() {

    private val getContactRequest =
        MutableLiveData<GetContactRequest>(DefaultState)

    val getContactByPhoneObserve: LiveData<RequestUIState<Boolean>> =
        Transformations.switchMap(getContactRequest) {
            liveData(context = viewModelScope.coroutineContext + Dispatchers.IO) {
                when (it) {
                    DefaultState -> emit(RequestUIState.DefaultState)
                    is GetContactByPhone -> {
                        emit(RequestUIState.OnLoading)
                        try {
                            val getFriendResult =
                                getFriendByPhoneUseCase.getByPhone(it.phone)
                            val result = getFriendResult?.let { existingFriend ->
                                deleteFriendUseCase.delete(friendsEntity = existingFriend)
                            }
                                ?: addFriendUseCase.addFriend(
                                    it.phone,
                                    it.contactName
                                )

                            emit(RequestUIState.OnSuccess(result))

                        } catch (e: Exception) {
                            e.printStackTrace()
                            emit(RequestUIState.OnError(e.message))
                        }
                    }
                }
            }
        }


    /**
     *  Get contact data from device storage.
     *  @param contactName Resolved contact name from Cursor component.
     */
    fun getContactByNumber(phone: String, contactName: String) {
        getContactRequest.value = GetContactByPhone(phone, contactName)
    }

    fun resetState() {
        getContactRequest.value = DefaultState
    }

    /**
     * Get contact information request states.
     * @property GetContactByPhone the request state.
     * @property DefaultState the reset/default state for an request. Dispatching the event to be handled by some other observing component.
     */
    private sealed class GetContactRequest {
        data class GetContactByPhone(val phone: String, val contactName: String) :
            GetContactRequest()

        object DefaultState : GetContactRequest()
    }
}