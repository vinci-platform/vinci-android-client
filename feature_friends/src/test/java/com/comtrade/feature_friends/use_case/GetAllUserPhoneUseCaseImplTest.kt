/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: GetAllUserPhoneUseCaseImplTest.kt
 * Author: sdelic
 *
 * History:  28.6.22. 11:04 created
 */

package com.comtrade.feature_friends.use_case

import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.contracts.repository.user.UserServiceRepository
import com.comtrade.domain.contracts.usecases.friends.IGetAllUserPhonesUseCase
import com.comtrade.domain.entities.user.data.GetAllUserPhonesEntity
import kotlinx.coroutines.runBlocking
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito

/**
 * Test scenario for for obtaining all of User's phone numbers use case implementation detail.
 */
internal class GetAllUserPhoneUseCaseImplTest {
    private val testToken = "testToken"
    private val testDataCount = 1_000
    private lateinit var prefs: IPrefs
    private lateinit var userServiceRepository: UserServiceRepository
    private lateinit var getAllUserPhonesUseCase: IGetAllUserPhonesUseCase

    @Before
    fun setUp() {
        prefs = Mockito.mock(IPrefs::class.java)
        userServiceRepository = Mockito.mock(UserServiceRepository::class.java)
        getAllUserPhonesUseCase = GetAllUserPhoneUseCaseImpl(prefs, userServiceRepository)
    }

    @Test
    fun `given a valid input when called return a list of contacts`() = runBlocking {
        val testData = provideTestContactData()
        Mockito.`when`(prefs.token).thenReturn(testToken)
        Mockito.`when`(userServiceRepository.getAllUserPhones(testToken))
            .thenReturn(testData)

        //Make call
        val result = getAllUserPhonesUseCase.getAllUserPhones()

        Mockito.verify(prefs, Mockito.times(1)).token
        Mockito.verify(userServiceRepository, Mockito.times(1))
            .getAllUserPhones(testToken)

        assertNotNull(result)
        assertNotEquals(0, result.size)
    }

    @Test
    fun `given a valid input when called return an empty list of contacts`() = runBlocking {
        val testData = listOf<GetAllUserPhonesEntity>()
        Mockito.`when`(prefs.token).thenReturn(testToken)
        Mockito.`when`(userServiceRepository.getAllUserPhones(testToken))
            .thenReturn(testData)

        //Make call
        val result = getAllUserPhonesUseCase.getAllUserPhones()

        Mockito.verify(prefs, Mockito.times(1)).token
        Mockito.verify(userServiceRepository, Mockito.times(1))
            .getAllUserPhones(testToken)

        assertNotNull(result)
        assertEquals(0, result.size)
    }


    @Test(expected = Exception::class)
    fun `given an invalid token request when called throw exception`() = runBlocking {
        val testData = provideTestContactData()
        Mockito.`when`(prefs.token).thenReturn(null)
        Mockito.`when`(userServiceRepository.getAllUserPhones(testToken))
            .thenReturn(testData)

        //Make call
        val result = getAllUserPhonesUseCase.getAllUserPhones()

        Mockito.verify(prefs, Mockito.times(1)).token
        Mockito.verify(userServiceRepository, Mockito.times(0))
            .getAllUserPhones(testToken)

        assertNotNull(result)
        assertNotEquals(0, result.size)
    }

    private fun provideTestContactData(): List<GetAllUserPhonesEntity> {
        val testData = mutableListOf<GetAllUserPhonesEntity>()

        for (i in 0 until testDataCount) {
            testData.add(
                GetAllUserPhonesEntity(
                    userId = i.toLong(),
                    firstName = "first name $i",
                    lastName = "last name $i",
                    phone = "$i"
                )
            )
        }

        return testData
    }
}