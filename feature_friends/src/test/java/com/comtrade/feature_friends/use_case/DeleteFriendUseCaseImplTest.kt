/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: DeleteFriendUseCaseImplTest.kt
 * Author: sdelic
 *
 * History:  6.6.22. 14:31 created
 */

package com.comtrade.feature_friends.use_case

import com.comtrade.domain.contracts.repository.IAppRepository
import com.comtrade.domain.contracts.usecases.friends.IDeleteFriendUseCase
import com.comtrade.domain.entities.friends.FriendsEntity
import kotlinx.coroutines.runBlocking
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito

/**
 * Test scenario for deleting an existing friend/contact entity use case implementation detail.
 */
internal class DeleteFriendUseCaseImplTest {
    private val testPhoneNumber = "0123456789"

    private lateinit var appRepository: IAppRepository
    private lateinit var deleteFriendUseCase: IDeleteFriendUseCase

    @Before
    fun setUp() {
        appRepository = Mockito.mock(IAppRepository::class.java)
        deleteFriendUseCase = DeleteFriendUseCaseImpl(appRepository)
    }

    @Test
    fun `given valid input data when called return true`() {
        runBlocking {
            val testData = provideTestData()
            Mockito.`when`(appRepository.deleteFriend(testData)).thenReturn(1)
            //Execute call
            val result = deleteFriendUseCase.delete(testData)

            Mockito.verify(appRepository, Mockito.times(1)).deleteFriend(testData)

            assertNotNull(result)
            assertTrue(result)
        }
    }

    @Test
    fun `given valid input data when called return false for database delete query`() {
        runBlocking {
            val testData = provideTestData()
            Mockito.`when`(appRepository.deleteFriend(testData)).thenReturn(0)
            //Execute call
            val result = deleteFriendUseCase.delete(testData)

            Mockito.verify(appRepository, Mockito.times(1)).deleteFriend(testData)

            assertNotNull(result)
            assertFalse(result)
        }
    }

    @Test(expected = Exception::class)
    fun `given an invalid input data when called throw exception`() {
        runBlocking {
            val testData = provideTestData().copy(id = -1)
            Mockito.`when`(appRepository.deleteFriend(testData)).thenReturn(0)
            //Execute call
            val result = deleteFriendUseCase.delete(testData)

            Mockito.verify(appRepository, Mockito.times(1)).deleteFriend(testData)

            assertNotNull(result)
            assertFalse(result)
        }
    }

    private fun provideTestData(): FriendsEntity = FriendsEntity(
        id = 12,
        userId = 10,
        name = "TestFriendName",
        phone = testPhoneNumber,
        syncedWithServer = false
    )
}