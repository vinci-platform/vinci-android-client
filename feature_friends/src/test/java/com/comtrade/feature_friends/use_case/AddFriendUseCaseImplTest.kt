/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: AddFriendUseCaseImplTest.kt
 * Author: sdelic
 *
 * History:  6.6.22. 16:10 created
 */

package com.comtrade.feature_friends.use_case

import com.comtrade.domain.contracts.repository.IAppRepository
import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.contracts.usecases.friends.IAddFriendUseCase
import com.comtrade.domain.entities.friends.FriendsEntity
import kotlinx.coroutines.runBlocking
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito

/**
 * Test scenario for the adding of new friend/contact data use case implementation detail.
 */
internal class AddFriendUseCaseImplTest {
    private val testUserId = 1L
    private val testContactName = "testContactName"
    private val testPhoneNumber = "0123456789"

    private lateinit var prefs: IPrefs
    private lateinit var appRepository: IAppRepository
    private lateinit var addFriendUseCase: IAddFriendUseCase

    @Before
    fun setUp() {
        prefs = Mockito.mock(IPrefs::class.java)
        appRepository = Mockito.mock(IAppRepository::class.java)
        addFriendUseCase = AddFriendUseCaseImpl(prefs, appRepository)
    }

    @Test
    fun `given valid input data when called return true`() {
        runBlocking {
            val testData = provideTestData()
            Mockito.`when`(prefs.userId).thenReturn(testUserId)
            Mockito.`when`(appRepository.addFriend(testData)).thenReturn(1L)

            //Execute call
            val result = addFriendUseCase.addFriend(testPhoneNumber, testContactName)
            Mockito.verify(prefs, Mockito.times(1)).userId
            Mockito.verify(appRepository, Mockito.times(1)).addFriend(testData)

            assertNotNull(result)
            assertTrue(result)
        }
    }

    @Test
    fun `given valid input data when called return false`() {
        runBlocking {
            val testData = provideTestData()
            Mockito.`when`(prefs.userId).thenReturn(testUserId)
            Mockito.`when`(appRepository.addFriend(testData)).thenReturn(0L)

            //Execute call
            val result = addFriendUseCase.addFriend(testPhoneNumber, testContactName)
            Mockito.verify(prefs, Mockito.times(1)).userId
            Mockito.verify(appRepository, Mockito.times(1)).addFriend(testData)

            assertNotNull(result)
            assertFalse(result)
        }
    }


    @Test(expected = Exception::class)
    fun `given invalid friend userid input data when called throw exception`() {
        runBlocking {
            val testData = provideTestData().copy(userId = -1)
            Mockito.`when`(prefs.userId).thenReturn(-1)
            Mockito.`when`(appRepository.addFriend(testData)).thenReturn(1L)

            //Execute call
            val result = addFriendUseCase.addFriend(testPhoneNumber, testContactName)
            Mockito.verify(prefs, Mockito.times(1)).userId
            Mockito.verify(appRepository, Mockito.times(1)).addFriend(testData)

            assertNotNull(result)
            assertTrue(result)
        }
    }

    @Test(expected = Exception::class)
    fun `given invalid friend name input data when called throw exception`() {
        runBlocking {
            val testData = provideTestData().copy(name = "")
            Mockito.`when`(prefs.userId).thenReturn(testUserId)
            Mockito.`when`(appRepository.addFriend(testData)).thenReturn(1L)

            //Execute call
            val result = addFriendUseCase.addFriend(testPhoneNumber, "")
            Mockito.verify(prefs, Mockito.times(1)).userId
            Mockito.verify(appRepository, Mockito.times(1)).addFriend(testData)

            assertNotNull(result)
            assertTrue(result)
        }
    }

    @Test(expected = Exception::class)
    fun `given invalid friend phone input data when called throw exception`() {
        runBlocking {
            val testData = provideTestData().copy(phone = "")
            Mockito.`when`(prefs.userId).thenReturn(testUserId)
            Mockito.`when`(appRepository.addFriend(testData)).thenReturn(1L)

            //Execute call
            val result = addFriendUseCase.addFriend("", testContactName)
            Mockito.verify(prefs, Mockito.times(1)).userId
            Mockito.verify(appRepository, Mockito.times(1)).addFriend(testData)

            assertNotNull(result)
            assertTrue(result)
        }
    }

    private fun provideTestData(): FriendsEntity = FriendsEntity(
        id = 0,
        userId = testUserId,
        name = testContactName,
        phone = testPhoneNumber,
        syncedWithServer = false
    )
}