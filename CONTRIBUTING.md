# Contributing to vInci

## Thank you

Thank you for taking the time to contribute to this project! We realize that every new beginning can be challenging so we are providing a set of general contribution guidelines.

The guidelines are by no means strict rules and we encourage you to use your best judgement and propose changes to this and other related documents for this platform. New propositions can be made by simply submitting a new pull request.

#### Code of Conduct

In the interest of hosting an open and welcoming environment for everyone involved, we as fellow contributors and maintainers will put an active effort in preventing any form of mistreating, harassment or any kind of harmful behavior aimed at fellow contributors. That includes any discriminatory language regarding age, body-size, nationality, level of experience, sexuality, gender identity.

Anyone in breach of the said conduct will be permanently banned from further participating in the continuous maintenance for this platform.

Please be patient and respectful of fellow contributors.

[Styleguides](#styleguides)

- [Git Commit Messages](#git-commit-messages)
- [Java Styleguide](#java-styleguide)
- [Kotlin Styleguide](#kotlin-styleguide)
- [TypeScript Styleguide](#typescript-styleguide)

## How Can I Contribute?

- ### Updating the documentation

Every project needs a good documentation to back it up. Please feel free to submit and update the project's documentation files.

- ### Reporting Bugs

Before creating new bug reports, please make sure to check for already listed bug reports in the project's issues category. Any duplicated bug reports will be auto closed. Make sure to use descriptive and clear titles in bug reports. Other than clear and descriptive titles, reports should include detailed steps for reproducing the said bug, software versions used and if its a crash-related bug, submit the relevant debug log data. Be careful with the information you are sharing and remove any sensitive information such as API keys and passwords relating to your specific setup.

- ### Suggesting Features or Enhancements

Much of the same is recommended for submitting feature requests as with submitting bug reports(- ### Re)

### Pull Requests

The process described here has several goals:

- Maintain Atom's quality
- Fix problems that are important to users
- Engage the community in working toward the best possible Atom
- Enable a sustainable system for Atom's maintainers to review contributions

Please follow these steps to have your contribution considered by the maintainers:

## Styleguides

### Git Commit Messages

- Use the present tense ("Add feature" not "Added feature")
- Limit the first line to 72 characters or less
- Reference issues and pull requests literally after the first line

### Java Styleguide

For Java code styles we are leaning towards the JetBrains styleguide outlined in this [link](https://www.jetbrains.com/help/idea/code-style-java.html#javadoc).

### Kotlin Styleguide

Same as for our Java styleguide, we also rely on the JetBrains official Kotlin [styles](https://developer.android.com/kotlin/style-guide?utm_source=source.android.com&utm_medium=referral).

### TypeScript Styleguide

All TypeScript code is linted with [Prettier](https://prettier.io/). We are using the Google recommended TypeScript [styles](https://google.github.io/styleguide/tsguide.html).
