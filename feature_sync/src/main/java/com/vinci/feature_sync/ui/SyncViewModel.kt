/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: SyncViewModel.kt
 * Author: sdelic
 *
 * History:  22.6.22. 14:07 created
 */

package com.vinci.feature_sync.ui

import androidx.lifecycle.*
import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain_ui.state.RequestUIState
import com.vinci.feature_sync.ui.SyncViewModel.SyncServicesRequest.DefaultState
import com.vinci.feature_sync.ui.SyncViewModel.SyncServicesRequest.SyncServices
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import javax.inject.Inject
import kotlin.system.measureTimeMillis

class SyncViewModel @Inject constructor(
    private val prefs: IPrefs,
    private val syncsServiceContainer: ISyncServiceContainer
) :
    ViewModel() {

    private val uxDelayForActionThreshold =
        1500L // If an action takes less than 1.5s delay further execution for improved UX
    private val uxDelayForActionInterval = 2000L // In case that the


    private val syncServicesDataRequest =
        MutableLiveData<SyncServicesRequest>(DefaultState)
    val syncServicesObserve: LiveData<RequestUIState<Int>> =
        Transformations.switchMap(syncServicesDataRequest) {
            liveData(context = viewModelScope.coroutineContext + Dispatchers.IO) {
                when (it) {
                    DefaultState -> emit(RequestUIState.DefaultState)
                    SyncServices -> {
                        try {
                            emit(RequestUIState.OnLoading)
                            val accuResultData = mutableListOf<Boolean>()

                            val duration = measureTimeMillis {
                                accuResultData.add(syncsServiceContainer.syncSurveyFromVinci())
                                accuResultData.add(syncsServiceContainer.syncSurveyToVinci())
                                accuResultData.add(syncsServiceContainer.syncEventsToVinci())

                                val fitBitDevice = prefs.fitBitDevice
                                if (fitBitDevice != null && fitBitDevice.active) {
                                    accuResultData.add(syncsServiceContainer.syncFitBitFromServer())
                                }

                                val cmdDevice = prefs.cmdDevice
                                if (cmdDevice != null && cmdDevice.active) {
                                    accuResultData.add(syncsServiceContainer.syncCmdFromServer())
                                }

                            }

                            val successes = accuResultData.filter { it }.count().toFloat()
                            val resultValue = successes / accuResultData.size

                            if (duration < uxDelayForActionThreshold) {
                                delay(uxDelayForActionInterval)
                            }
                            emit(RequestUIState.OnSuccess((resultValue * 100).toInt()))

                        } catch (e: Exception) {
                            e.printStackTrace()
                            emit(RequestUIState.OnError(e.message))
                        }
                    }
                }
            }
        }

    /**
     *  Synchronize services.
     */
    fun startSyncService() {
        syncServicesDataRequest.value = SyncServices
    }

    fun resetState() {
        syncServicesDataRequest.value = DefaultState
    }

    /**
     * Synchronize services data from and to server request states.
     * @property SyncServices the request state.
     * @property DefaultState the reset/default state for an request. Dispatching the event to be handled by some other observing component.
     */
    private sealed class SyncServicesRequest {
        object SyncServices : SyncServicesRequest()
        object DefaultState : SyncServicesRequest()
    }
}