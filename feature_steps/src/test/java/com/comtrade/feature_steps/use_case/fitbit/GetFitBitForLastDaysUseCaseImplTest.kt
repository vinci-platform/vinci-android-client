/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: GetFitBitForLastDaysUseCaseImplTest.kt
 * Author: sdelic
 *
 * History:  10.5.22. 12:32 created
 */

package com.comtrade.feature_steps.use_case.fitbit

import com.comtrade.domain.contracts.repository.IAppRepository
import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.contracts.usecases.steps.fitbit.IGetFitBitForLastDaysUseCase
import com.comtrade.domain.entities.devices.ShoeDevice
import com.comtrade.domain.entities.step.FitbitDataType
import com.comtrade.domain.entities.step.StepDailyEntity
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.anyLong
import org.mockito.Mockito

/**
 * Test scenario for the implementation detail for obtaining FitBit step information for a given period of days.
 */
internal class GetFitBitForLastDaysUseCaseImplTest {
    private val testDeviceId = 1L
    private val testDayRequest = 7
    private val testResultDaySteps = 1_000

    private lateinit var prefs: IPrefs
    private lateinit var appRepository: IAppRepository
    private lateinit var getFitBitForLastDaysUseCase: IGetFitBitForLastDaysUseCase

    @Before
    fun setUp() {
        prefs = Mockito.mock(IPrefs::class.java)
        appRepository = Mockito.mock(IAppRepository::class.java)
        getFitBitForLastDaysUseCase = GetFitBitForLastDaysUseCaseImpl(prefs, appRepository)
    }

    @Test
    fun `given valid day input when called return list of step data`() {
        runBlocking {
            val testShoeDevice = provideTestShoeDevice()
            val testFitBitDailySteps = provideTestFitBitStepResponse(testDayRequest)
            Mockito.`when`(prefs.shoeDevice).thenReturn(testShoeDevice)
            Mockito.`when`(
                appRepository.getDailyStepsForPeriod(
                    anyLong(),
                    anyLong(),
                )
            ).thenReturn(testFitBitDailySteps)
            val result =
                getFitBitForLastDaysUseCase.getFitbitForLastDays(
                    testDayRequest,
                    FitbitDataType.STEPS
                )

            Mockito.verify(prefs, Mockito.times(1)).shoeDevice
            Mockito.verify(appRepository, Mockito.times(1))
                .getDailyStepsForPeriod(
                    anyLong(),
                    anyLong(),
                )

            assertNotNull(result)
            assertEquals(testDayRequest, result.size)
        }
    }

    @Test
    fun `given valid day input when called return empty list of step data`() {
        runBlocking {
            val testShoeDevice = provideTestShoeDevice()
            Mockito.`when`(prefs.shoeDevice).thenReturn(testShoeDevice)
            Mockito.`when`(
                appRepository.getDailyStepsForPeriod(
                    anyLong(),
                    anyLong(),
                )
            ).thenReturn(emptyList())
            val result =
                getFitBitForLastDaysUseCase.getFitbitForLastDays(
                    testDayRequest,
                    FitbitDataType.STEPS
                )

            Mockito.verify(prefs, Mockito.times(1)).shoeDevice
            Mockito.verify(appRepository, Mockito.times(1))
                .getDailyStepsForPeriod(
                    anyLong(),
                    anyLong(),
                )

            assertNotNull(result)
            assertEquals(0, result.size)
        }
    }

    @Test(expected = Exception::class)
    fun `given an invalid day input when called throw exception`() {
        runBlocking {
            val testShoeDevice = provideTestShoeDevice()
            Mockito.`when`(prefs.shoeDevice).thenReturn(testShoeDevice)
            Mockito.`when`(
                appRepository.getDailyStepsForPeriod(
                    anyLong(),
                    anyLong(),
                )
            ).thenReturn(emptyList())
            getFitBitForLastDaysUseCase.getFitbitForLastDays(
                -1,
                FitbitDataType.STEPS
            )

            Mockito.verify(prefs, Mockito.times(1)).shoeDevice
            Mockito.verify(appRepository, Mockito.times(1))
                .getDailyStepsForPeriod(
                    anyLong(),
                    anyLong(),
                )
        }
    }

    @Test(expected = Exception::class)
    fun `given an invalid FitBit device input when called throw exception`() {
        runBlocking {
            Mockito.`when`(prefs.shoeDevice).thenReturn(null)
            Mockito.`when`(
                appRepository.getDailyStepsForPeriod(
                    anyLong(),
                    anyLong(),
                )
            ).thenReturn(emptyList())
            getFitBitForLastDaysUseCase.getFitbitForLastDays(
                -1,
                FitbitDataType.STEPS
            )

            Mockito.verify(prefs, Mockito.times(1)).shoeDevice
            Mockito.verify(appRepository, Mockito.times(1))
                .getDailyStepsForPeriod(
                    anyLong(),
                    anyLong(),
                )
        }
    }

    private fun provideTestShoeDevice(): ShoeDevice = ShoeDevice(
        deviceCode = "testDeviceCode",
        lowThreshold = -1.0,
        highThreshold = -1.0,
        deviceMac = null,
        device = null
    )

    private fun provideTestFitBitStepResponse(days: Int): List<StepDailyEntity> {
        val testData = mutableListOf<StepDailyEntity>()
        for (i in 0 until days) {
            testData.add(
                StepDailyEntity(
                    id = i,
                    deviceId = testDeviceId,
                    steps = testResultDaySteps,
                    activityCalories = 1_000,
                    calories_out = 1_000,
                    heartRateMin = 50.0,
                    heartRateAvg = 60.0,
                    heartRateMax = 70.0,
                    restingHeartRate = 50.0,
                    minutesAsleep = 480,
                    minutesInBed = 480,
                    timestamp = System.currentTimeMillis()
                )
            )
        }

        return testData
    }
}