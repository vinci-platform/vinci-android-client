/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: AddStepDataUseCaseImplTest.kt
 * Author: sdelic
 *
 * History:  9.5.22. 13:07 created
 */

package com.comtrade.feature_steps.use_case

import com.comtrade.domain.contracts.repository.IAppRepository
import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.contracts.usecases.steps.IAddStepDataUseCase
import com.comtrade.domain.entities.step.StepEntity
import kotlinx.coroutines.runBlocking
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito

/**
 * Test scenario for the step persistence implementation detail.
 */
internal class AddStepDataUseCaseImplTest {
    private val testStepCount = 1_000
    private lateinit var prefs: IPrefs
    private lateinit var appRepository: IAppRepository
    private lateinit var addStepDataUseCase: IAddStepDataUseCase

    @Before
    fun setUp() {
        prefs = Mockito.mock(IPrefs::class.java)
        appRepository = Mockito.mock(IAppRepository::class.java)
        addStepDataUseCase = AddStepDataUseCaseImpl(prefs, appRepository)
    }

    @Test
    fun `given valid step data when persist return table Id`() {
        runBlocking {
            val testData = provideTestStepData()
            Mockito.`when`(
                appRepository.addSteps(testData)
            )
                .thenReturn(1L)

            val result = addStepDataUseCase.addSteps(testData)
            Mockito.verify(appRepository, Mockito.times(1))
                .addSteps(testData)
            assertNotNull(result)
            assertTrue(result > 0)
        }
    }

    @Test
    fun `given valid step data when persist return -1`() {
        runBlocking {
            val testData = provideTestStepData()
            Mockito.`when`(
                appRepository.addSteps(testData)
            )
                .thenReturn(-1L)

            val result = addStepDataUseCase.addSteps(testData)
            Mockito.verify(appRepository, Mockito.times(1))
                .addSteps(testData)
            assertNotNull(result)
            assertFalse(result > 0)
        }
    }

    @Test(expected = Exception::class)
    fun `given the below minimum step count when persist throw exception`() {
        runBlocking {
            val testData = provideTestStepData().copy(steps = 0)
            Mockito.`when`(
                appRepository.addSteps(testData)
            )
                .thenReturn(1L)

            val result = addStepDataUseCase.addSteps(testData)
            Mockito.verify(appRepository, Mockito.times(1))
                .addSteps(testData)
            assertNotNull(result)
            assertTrue(result > 0)
        }
    }


    private fun provideTestStepData(): StepEntity = StepEntity(
        id = 1,
        deviceId = 1L,
        steps = testStepCount,
        timestamp = System.currentTimeMillis(),
        syncedWithServer = false
    )
}