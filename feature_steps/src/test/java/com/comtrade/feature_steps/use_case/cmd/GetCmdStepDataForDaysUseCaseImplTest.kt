/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: IGetCmdStepDataForDaysUseCaseImplTest.kt
 * Author: sdelic
 *
 * History:  13.5.22. 10:54 created
 */

package com.comtrade.feature_steps.use_case.cmd

import com.comtrade.domain.contracts.repository.IAppRepository
import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.contracts.usecases.steps.cmd.IGetCmdStepDataForDaysUseCase
import com.comtrade.domain.entities.devices.CmdDevice
import com.comtrade.domain.entities.user.data.GetUserDevicesEntity
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.anyLong
import org.mockito.Mockito

/**
 *  Test suite for obtaining the CMD watch data for a given period of days use case implementation detail.
 */
internal class GetCmdStepDataForDaysUseCaseImplTest {
    private val testDayRequest = 7
    private val testResultDaySteps = 1_000

    private lateinit var prefs: IPrefs
    private lateinit var appRepository: IAppRepository
    private lateinit var getCmdStepDataForDaysUseCase: IGetCmdStepDataForDaysUseCase

    @Before
    fun setUp() {
        prefs = Mockito.mock(IPrefs::class.java)
        appRepository = Mockito.mock(IAppRepository::class.java)
        getCmdStepDataForDaysUseCase = GetCmdStepDataForDaysUseCaseImpl(prefs, appRepository)
    }

    @Test
    fun `given valid day input when called return graph array`() {
        runBlocking {
            val testDevice = provideTestCMDDevice()
            Mockito.`when`(prefs.cmdDevice).thenReturn(testDevice)
            Mockito.`when`(
                appRepository.getCmdStepsForPeriod(anyLong(), anyLong(), anyLong())
            ).thenReturn(testResultDaySteps)
            val result = getCmdStepDataForDaysUseCase.getCmdDataForLastDays(testDayRequest)

            Mockito.verify(prefs, Mockito.times(1)).cmdDevice
            Mockito.verify(appRepository, Mockito.times(testDayRequest))
                .getCmdStepsForPeriod(anyLong(), anyLong(), anyLong())

            Assert.assertNotNull(result)
            Assert.assertEquals(testDayRequest, result.size)

            //Check if the sum total of steps is correct for the number of days required.
            Assert.assertEquals(
                testDayRequest * testResultDaySteps,
                result.fold(0) { acc, i -> acc + i.value })
        }
    }

    @Test
    fun `given valid day input when called return zero step graph array`() {
        runBlocking {
            val testDevice = provideTestCMDDevice()
            Mockito.`when`(prefs.cmdDevice).thenReturn(testDevice)
            Mockito.`when`(
                appRepository.getCmdStepsForPeriod(anyLong(), anyLong(), anyLong())
            ).thenReturn(0)
            val result = getCmdStepDataForDaysUseCase.getCmdDataForLastDays(testDayRequest)

            Mockito.verify(prefs, Mockito.times(1)).cmdDevice
            Mockito.verify(appRepository, Mockito.times(testDayRequest))
                .getCmdStepsForPeriod(anyLong(), anyLong(), anyLong())

            Assert.assertNotNull(result)
            Assert.assertEquals(testDayRequest, result.size)

            //Check if the sum total of steps is correct for the number of days required.
            Assert.assertEquals(
                0,
                result.fold(0) { acc, i -> acc + i.value })
        }
    }

    @Test(expected = Exception::class)
    fun `given an invalid cmd device when called throw exception`() {
        runBlocking {
            Mockito.`when`(prefs.cmdDevice).thenReturn(null)
            Mockito.`when`(
                appRepository.getCmdStepsForPeriod(anyLong(), anyLong(), anyLong())
            ).thenReturn(0)
            val result = getCmdStepDataForDaysUseCase.getCmdDataForLastDays(testDayRequest)

            Mockito.verify(prefs, Mockito.times(1)).cmdDevice
            Mockito.verify(appRepository, Mockito.times(testDayRequest))
                .getCmdStepsForPeriod(anyLong(), anyLong(), anyLong())

            Assert.assertNotNull(result)
            Assert.assertEquals(testDayRequest, result.size)

            //Check if the sum total of steps is correct for the number of days required.
            Assert.assertEquals(
                0,
                result.fold(0) { acc, i -> acc + i.value })
        }
    }

    private fun provideTestCMDDevice(): CmdDevice = CmdDevice(
        GetUserDevicesEntity(
            deviceId = 1L,
            name = "testCMDDevice",
            description = "test CMD device description",
            uuid = "some random uuid",
            deviceType = "WRIST WATCH",
            active = true
        )
    )
}