/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: StepsModule.kt
 * Author: sdelic
 *
 * History:  9.5.22. 13:42 created
 */

package com.comtrade.feature_steps.di

import com.comtrade.domain.contracts.repository.IAppRepository
import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.contracts.usecases.steps.IAddStepDataUseCase
import com.comtrade.domain.contracts.usecases.steps.IGetStepsForPreviousDaysUseCase
import com.comtrade.domain.contracts.usecases.steps.cmd.IGetCmdStepDataForDaysUseCase
import com.comtrade.domain.contracts.usecases.steps.fitbit.IGetFitBitForLastDaysUseCase
import com.comtrade.feature_steps.use_case.AddStepDataUseCaseImpl
import com.comtrade.feature_steps.use_case.GetStepsForPreviousDaysUseCaseImpl
import com.comtrade.feature_steps.use_case.cmd.GetCmdStepDataForDaysUseCaseImpl
import com.comtrade.feature_steps.use_case.fitbit.GetFitBitForLastDaysUseCaseImpl
import dagger.Module
import dagger.Provides

/**
 * The Dagger DI step module. Provides implementations for the step data use cases.
 */
@Module
class StepsModule {

    @Provides
    fun provideAddStepsUseCase(
        prefs: IPrefs,
        appRepository: IAppRepository
    ): IAddStepDataUseCase = AddStepDataUseCaseImpl(prefs, appRepository)

    @Provides
    fun provideGetStepsForPreviousDaysUseCase(
        prefs: IPrefs,
        appRepository: IAppRepository
    ): IGetStepsForPreviousDaysUseCase =
        GetStepsForPreviousDaysUseCaseImpl(prefs, appRepository)

    @Provides
    fun provideFitBitDataForPreviousDaysUseCase(
        prefs: IPrefs,
        appRepository: IAppRepository
    ): IGetFitBitForLastDaysUseCase =
        GetFitBitForLastDaysUseCaseImpl(prefs, appRepository)

    @Provides
    fun provideCMDDataForPreviousDaysUseCase(
        prefs: IPrefs,
        appRepository: IAppRepository
    ): IGetCmdStepDataForDaysUseCase =
        GetCmdStepDataForDaysUseCaseImpl(prefs, appRepository)

}