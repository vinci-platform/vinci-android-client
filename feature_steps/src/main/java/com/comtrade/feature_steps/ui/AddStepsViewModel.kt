/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: AddStepsViewModel.kt
 * Author: sdelic
 *
 * History:  9.5.22. 13:41 created
 */

package com.comtrade.feature_steps.ui

import androidx.lifecycle.*
import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.contracts.usecases.steps.IAddStepDataUseCase
import com.comtrade.domain.entities.step.StepEntity
import com.comtrade.domain_ui.state.RequestUIState
import com.comtrade.feature_steps.ui.AddStepsViewModel.AddStepsRequest.AddSteps
import com.comtrade.feature_steps.ui.AddStepsViewModel.AddStepsRequest.DefaultState
import kotlinx.coroutines.Dispatchers
import java.time.Instant
import java.time.temporal.ChronoUnit
import javax.inject.Inject

/**
 * Android ViewModel component for adding new step data.
 */
class AddStepsViewModel @Inject constructor(
    private val prefs: IPrefs,
    private val addStepDataUseCase: IAddStepDataUseCase
) :
    ViewModel() {

    private val INTERVAL: Long = 300
    private var endDate: Instant
    private var stepCount: Int = 0

    init {
        endDate = Instant.now().truncatedTo(ChronoUnit.DAYS)
        while (Instant.now() > endDate) {
            endDate = endDate.plusSeconds(INTERVAL)
        }
    }

    private val addStepsRequest = MutableLiveData<AddStepsRequest>(DefaultState)
    val addStepsObserver: LiveData<RequestUIState<Long>> =
        Transformations.switchMap(addStepsRequest) {
            liveData(context = viewModelScope.coroutineContext + Dispatchers.IO) {
                when (it) {
                    DefaultState -> emit(RequestUIState.DefaultState)

                    is AddSteps -> {
                        emit(RequestUIState.OnLoading)
                        try {
                            val result =
                                addStepDataUseCase.addSteps(it.steps)

                            if (result > 0) {
                                emit(RequestUIState.OnSuccess(result))
                            } else {
                                emit(RequestUIState.OnError(null)) //UI handles the text translation.
                            }

                        } catch (e: Exception) {
                            e.printStackTrace()
                            emit(RequestUIState.OnError(e.message))
                        }
                    }
                }
            }
        }

    /**
     * Add new step count.
     * @param steps New Step count to be persisted.
     */
    fun addSteps(steps: Int) {
        if (Instant.now() > endDate) {
            val shoeDevice = prefs.shoeDevice
            if (shoeDevice != null && stepCount > 0) {
                val stepRecord =
                    StepEntity(
                        0,
                        shoeDevice.deviceId,
                        stepCount,
                        endDate.minusSeconds(INTERVAL).toEpochMilli(),
                        false
                    )
                addSteps(stepRecord)
            }
            endDate = endDate.plusSeconds(INTERVAL)
            stepCount = steps

        } else {
            // temporary fix for damaged insole
            stepCount += if (steps > 6)
                6
            else
                steps
        }
    }

    /**
     * Add step data.
     * @param steps New Step data to be persisted.
     */
    private fun addSteps(steps: StepEntity) {
        addStepsRequest.value = AddSteps(steps)
    }

    fun resetState() {
        addStepsRequest.value = DefaultState
    }

    /**
     * Add step data request states.
     * @property AddSteps the request state.
     * @property DefaultState the reset/default state for an request. Dispatching the event to be handled by some other observing component.
     */
    private sealed class AddStepsRequest {
        data class AddSteps(val steps: StepEntity) : AddStepsRequest()
        object DefaultState : AddStepsRequest()
    }
}