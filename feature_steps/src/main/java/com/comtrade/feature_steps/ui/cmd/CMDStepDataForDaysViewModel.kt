/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: CMDStepDataForDaysViewModel.kt
 * Author: sdelic
 *
 * History:  13.5.22. 11:37 created
 */

package com.comtrade.feature_steps.ui.cmd

import androidx.lifecycle.*
import com.comtrade.domain.contracts.usecases.steps.cmd.IGetCmdStepDataForDaysUseCase
import com.comtrade.domain.entities.graph.GraphData
import com.comtrade.domain_ui.state.RequestUIState
import com.comtrade.feature_steps.ui.cmd.CMDStepDataForDaysViewModel.GetDayStepsRequest.DefaultState
import com.comtrade.feature_steps.ui.cmd.CMDStepDataForDaysViewModel.GetDayStepsRequest.GetSteps
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

/**
 * Android ViewModel component for handling the UI states for the CMD step data obtain feature.
 */
class CMDStepDataForDaysViewModel @Inject constructor(private val getCmdStepDataForDaysUseCase: IGetCmdStepDataForDaysUseCase) :
    ViewModel() {

    private val getDayStepsRequest =
        MutableLiveData<GetDayStepsRequest>(DefaultState)
    val getCMDDayStepsObserver: LiveData<RequestUIState<Map<Int, Array<GraphData>>>> =
        Transformations.switchMap(getDayStepsRequest) {
            liveData(context = viewModelScope.coroutineContext + Dispatchers.IO) {
                when (it) {
                    DefaultState -> emit(RequestUIState.DefaultState)
                    is GetSteps -> {
                        emit(RequestUIState.OnLoading)
                        try {

                            val result = mutableMapOf<Int, Array<GraphData>>()
                            for (dayInterval in it.numberOfDays) {
                                val resultSteps =
                                    getCmdStepDataForDaysUseCase.getCmdDataForLastDays(dayInterval)
                                result[dayInterval] = resultSteps
                            }

                            emit(RequestUIState.OnSuccess(result))
                        } catch (e: Exception) {
                            e.printStackTrace()
                            emit(RequestUIState.OnError(e.message))
                        }
                    }
                }
            }
        }

    /**
     * Obtain CMD step data for the previous n days.
     * @param numberOfDays List of day intervals for which to obtain the step data.
     */
    fun getCmdDataForLastDays(numberOfDays: IntArray) {
        getDayStepsRequest.value = GetSteps(numberOfDays)
    }

    fun resetState() {
        getDayStepsRequest.value = DefaultState
    }

    /**
     * Get CMD step data request states.
     * @property GetSteps the request state.
     * @property DefaultState the reset/default state for an request. Dispatching the event to be handled by some other observing component.
     */
    private sealed class GetDayStepsRequest {
        data class GetSteps(val numberOfDays: IntArray) : GetDayStepsRequest() {
            override fun equals(other: Any?): Boolean {
                if (this === other) return true
                if (javaClass != other?.javaClass) return false

                other as GetSteps

                if (!numberOfDays.contentEquals(other.numberOfDays)) return false

                return true
            }

            override fun hashCode(): Int {
                return numberOfDays.contentHashCode()
            }
        }

        object DefaultState : GetDayStepsRequest()
    }
}