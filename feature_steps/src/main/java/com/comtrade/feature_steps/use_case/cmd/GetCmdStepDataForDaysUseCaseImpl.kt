/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: IGetCmdStepDataForDaysUseCaseImpl.kt
 * Author: sdelic
 *
 * History:  13.5.22. 10:15 created
 */

package com.comtrade.feature_steps.use_case.cmd

import com.comtrade.domain.contracts.repository.IAppRepository
import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.contracts.usecases.steps.cmd.IGetCmdStepDataForDaysUseCase
import com.comtrade.domain.entities.graph.GraphData
import com.comtrade.domain.time.AppDateTimeConstants
import java.time.LocalDate
import java.time.ZoneId
import java.time.temporal.ChronoUnit
import java.util.*
import javax.inject.Inject

/**
 * Implementation detail for obtaining the CMD watch data for a given period of days use case.
 */
internal class GetCmdStepDataForDaysUseCaseImpl @Inject constructor(
    private val prefs: IPrefs,
    private val appRepository: IAppRepository
) : IGetCmdStepDataForDaysUseCase {
    override suspend fun getCmdDataForLastDays(numberOfDays: Int): Array<GraphData> {
        val cmdDevice = prefs.cmdDevice ?: throw Exception("No CMD device found!")

        val stepData: MutableList<GraphData> = mutableListOf()
        var date = LocalDate.now().atStartOfDay(ZoneId.of(AppDateTimeConstants.DISPLAY_ZONE_ID))
            .toInstant()
        for (i in 0 until numberOfDays) {
            val steps = GraphData()
            steps.value = appRepository.getCmdStepsForPeriod(
                cmdDevice.deviceId,
                date.toEpochMilli(), date.plus(
                    1,
                    ChronoUnit.DAYS
                ).toEpochMilli()
            )
            steps.dateTime = Date(date.toEpochMilli())
            stepData.add(0, steps)
            date = date.minus(1, ChronoUnit.DAYS)
        }
        return stepData.toTypedArray()
    }
}