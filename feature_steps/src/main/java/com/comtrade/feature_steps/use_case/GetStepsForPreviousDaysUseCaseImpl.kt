/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: GetStepsForPreviousDaysUseCaseImpl.kt
 * Author: sdelic
 *
 * History:  9.5.22. 15:59 created
 */

package com.comtrade.feature_steps.use_case

import com.comtrade.domain.contracts.repository.IAppRepository
import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.contracts.usecases.steps.IGetStepsForPreviousDaysUseCase
import com.comtrade.domain.entities.graph.GraphData
import java.time.Instant
import java.time.temporal.ChronoUnit
import java.util.*
import javax.inject.Inject

/**
 * Implementation detail for obtaining the step data for a given number of previous days use case.
 */
internal class GetStepsForPreviousDaysUseCaseImpl @Inject constructor(
    private val prefs: IPrefs,
    private val appRepository: IAppRepository
) : IGetStepsForPreviousDaysUseCase {
    override suspend fun getDataForLastDays(numberOfDays: Int): Array<GraphData> {

        val shoeDevice = prefs.shoeDevice
            ?: throw Exception("No shoe device found.")

        if (numberOfDays < 1) {
            throw Exception("Number of days must be greater than 0: $numberOfDays")
        }

        val stepData: MutableList<GraphData> = mutableListOf()
        var date = Instant.now().truncatedTo(ChronoUnit.DAYS)
        for (i in 0 until numberOfDays) {
            val steps = GraphData()
            steps.value = appRepository.getStepsForPeriod(
                shoeDevice.deviceId,
                date.toEpochMilli(),
                date.plus(
                    1,
                    ChronoUnit.DAYS
                ).toEpochMilli()
            )
            steps.dateTime = Date(date.toEpochMilli())
            stepData.add(0, steps)
            date = date.minus(1, ChronoUnit.DAYS)
        }
        return stepData.toTypedArray()
    }
}