/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: SudokuView.kt
 * Author: sdelic
 *
 * History:  18.4.22. 12:32 created
 */

package com.comtrade.game_sudoku.ui.view

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import com.comtrade.game_sudoku.Sudoku
import kotlin.math.min

internal class SudokuView(context: Context, attributeSet: AttributeSet) :
    View(context, attributeSet) {
    private var cellBorder = 0F
    private var cellSize: Float = 0F
    private var selectedRow = -1
    private var selectedColumn = -1

    var sudoku: Sudoku? = null

    private val bigLine = Paint().apply {
        style = Paint.Style.STROKE
        color = Color.BLACK
        strokeWidth = 4F
    }
    private val smallLine = Paint().apply {
        style = Paint.Style.STROKE
        color = Color.BLACK
        strokeWidth = 2F
    }
    private val selectedCell = Paint().apply {
        style = Paint.Style.FILL_AND_STROKE
        color = Color.parseColor("#004597")
    }
    private val startingCell = Paint().apply {
        style = Paint.Style.FILL_AND_STROKE
        color = Color.parseColor("#5ba4fa")
    }
    private val emptyCell = Paint().apply {
        style = Paint.Style.FILL_AND_STROKE
        color = Color.WHITE
    }
    private val blackFont = Paint().apply {
        style = Paint.Style.FILL_AND_STROKE
        color = Color.BLACK
        textSize = 64F
    }
    private val redFont = Paint().apply {
        style = Paint.Style.FILL_AND_STROKE
        color = Color.RED
        textSize = 64F
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        val size = min(widthMeasureSpec, heightMeasureSpec)
        setMeasuredDimension(size, size)
    }

    override fun onDraw(canvas: Canvas?) {
        cellSize = (width.toFloat() - 2 * cellBorder) / 9
        fillCells(canvas!!)
        drawBoard(canvas)
    }

    private fun fillCells(canvas: Canvas) {
        if (sudoku != null) {
            for (i in 0..8) {
                for (j in 0..8) {
                    var color: Paint = when {
                        sudoku!!.isValid() -> startingCell
                        (i == selectedRow && j == selectedColumn) -> selectedCell
                        sudoku!!.cells[i][j] < 0 -> startingCell
                        else -> emptyCell
                    }
                    canvas.drawRect(
                        cellBorder + j * cellSize + 1,
                        cellBorder + i * cellSize + 1,
                        cellBorder + j * cellSize + cellSize - 1,
                        cellBorder + i * cellSize + cellSize - 1,
                        color
                    )
                    color = if (sudoku!!.cells[i][j] < 0) {
                        blackFont
                    } else if (sudoku!!.cells[i][j] == sudoku!!.solution[i][j]) {
                        blackFont
                    } else {
                        redFont
                    }
                    if (sudoku!!.cells[i][j] != 0) {
                        canvas.drawText(
                            kotlin.math.abs(sudoku!!.cells[i][j]).toString(),
                            j * cellSize + cellSize / 2 - 16,
                            i * cellSize + cellSize / 2 + 32,
                            color
                        )
                    }
                }
            }
        }
    }

    private fun drawBoard(canvas: Canvas) {
        for (i in 0..9) {
            canvas.drawLine(
                cellBorder,
                cellBorder + i * cellSize,
                width.toFloat() - cellBorder,
                cellBorder + i * cellSize,
                if (i % 3 == 0)
                    bigLine
                else
                    smallLine
            )
            canvas.drawLine(
                cellBorder + i * cellSize,
                cellBorder,
                cellBorder + i * cellSize,
                width.toFloat() - cellBorder,
                if (i % 3 == 0)
                    bigLine
                else
                    smallLine
            )
        }
    }

    private fun calculateSelectedCell(x: Float, y: Float) {
        selectedRow = (y / cellSize).toInt()
        selectedColumn = (x / cellSize).toInt()
        invalidate()
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        return when (event!!.action) {
            MotionEvent.ACTION_DOWN -> {
                calculateSelectedCell(event.x, event.y)
                true
            }
            else -> false
        }
    }

    fun buttonClick(button: Int) {
        if (sudoku == null) {
            return
        }
        if (selectedRow >= 0 && selectedColumn >= 0 && sudoku!!.cells[selectedRow][selectedColumn] >= 0) {
            if (sudoku!!.cells[selectedRow][selectedColumn] != button) {
                sudoku!!.cells[selectedRow][selectedColumn] = button
                if (sudoku!!.cells[selectedRow][selectedColumn] != sudoku!!.solution[selectedRow][selectedColumn]) {
                    sudoku!!.errors++
                }
            } else {
                sudoku!!.cells[selectedRow][selectedColumn] = 0
            }
            if (sudoku!!.isValid()) {
                selectedRow = -1
                selectedColumn = -1
            }
            requestLayout()
        }
    }
}