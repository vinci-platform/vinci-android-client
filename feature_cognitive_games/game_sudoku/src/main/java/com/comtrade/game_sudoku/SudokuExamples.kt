/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: SudokuExamples.kt
 * Author: sdelic
 *
 * History:  21.2.22. 10:43 created
 */

package com.comtrade.game_sudoku

internal class SudokuExamples {
    var easy = arrayOf(
        arrayOf(
            arrayOf(9, 3, 7, 4, 5, 8, 1, 2, 6),
            arrayOf(6, 4, 2, 1, 9, 3, 5, 7, 8),
            arrayOf(8, 1, 5, 6, 2, 7, 4, 9, 3),
            arrayOf(4, 6, 9, 3, 1, 5, 2, 8, 7),
            arrayOf(1, 7, 8, 2, 6, 4, 3, 5, 9),
            arrayOf(2, 5, 3, 7, 8, 9, 6, 1, 4),
            arrayOf(7, 8, 6, 5, 4, 2, 9, 3, 1),
            arrayOf(3, 2, 1, 9, 7, 6, 8, 4, 5),
            arrayOf(5, 9, 4, 8, 3, 1, 7, 6, 2)
        ),
        arrayOf(
            arrayOf(0, 0, 1, 1, 0, 1, 1, 0, 0),
            arrayOf(1, 0, 0, 1, 1, 0, 0, 0, 1),
            arrayOf(0, 1, 0, 0, 0, 0, 0, 1, 0),
            arrayOf(0, 0, 1, 0, 0, 1, 1, 0, 1),
            arrayOf(1, 1, 1, 0, 1, 0, 1, 1, 0),
            arrayOf(1, 1, 1, 1, 0, 0, 0, 0, 0),
            arrayOf(1, 1, 0, 1, 0, 0, 0, 1, 0),
            arrayOf(1, 1, 0, 0, 1, 0, 0, 0, 0),
            arrayOf(1, 1, 1, 1, 1, 1, 1, 0, 1)
        ),
        arrayOf(
            arrayOf(7, 8, 4, 1, 5, 9, 3, 2, 6),
            arrayOf(5, 6, 3, 7, 4, 2, 8, 1, 9),
            arrayOf(2, 9, 1, 6, 8, 3, 4, 7, 5),
            arrayOf(4, 2, 6, 9, 7, 1, 5, 8, 3),
            arrayOf(1, 5, 9, 3, 2, 8, 6, 4, 7),
            arrayOf(3, 7, 8, 4, 6, 5, 2, 9, 1),
            arrayOf(6, 4, 2, 5, 9, 7, 1, 3, 8),
            arrayOf(8, 3, 7, 2, 1, 6, 9, 5, 4),
            arrayOf(9, 1, 5, 8, 3, 4, 7, 6, 2)
        ),
        arrayOf(
            arrayOf(0, 0, 0, 1, 1, 0, 1, 1, 1),
            arrayOf(0, 1, 0, 0, 0, 1, 0, 1, 0),
            arrayOf(1, 0, 0, 1, 1, 0, 1, 1, 0),
            arrayOf(0, 1, 1, 1, 0, 0, 0, 0, 0),
            arrayOf(1, 1, 0, 1, 1, 0, 1, 0, 1),
            arrayOf(0, 1, 0, 1, 0, 1, 1, 1, 0),
            arrayOf(0, 0, 0, 1, 1, 1, 1, 1, 0),
            arrayOf(1, 0, 0, 0, 0, 1, 1, 0, 0),
            arrayOf(1, 0, 1, 1, 0, 0, 1, 1, 0)
        ),
        arrayOf(
            arrayOf(7, 6, 8, 3, 1, 2, 5, 9, 4),
            arrayOf(2, 1, 5, 4, 9, 7, 3, 6, 8),
            arrayOf(4, 9, 3, 8, 6, 5, 1, 7, 2),
            arrayOf(3, 7, 6, 9, 2, 8, 4, 5, 1),
            arrayOf(8, 4, 2, 7, 5, 1, 6, 3, 9),
            arrayOf(9, 5, 1, 6, 3, 4, 8, 2, 7),
            arrayOf(6, 8, 7, 5, 4, 9, 2, 1, 3),
            arrayOf(1, 3, 9, 2, 8, 6, 7, 4, 5),
            arrayOf(5, 2, 4, 1, 7, 3, 9, 8, 6)
        ),
        arrayOf(
            arrayOf(1, 1, 0, 1, 1, 1, 0, 0, 1),
            arrayOf(0, 0, 0, 0, 1, 1, 1, 1, 0),
            arrayOf(0, 1, 0, 0, 1, 0, 0, 1, 0),
            arrayOf(0, 1, 1, 1, 0, 1, 0, 0, 1),
            arrayOf(1, 1, 1, 0, 1, 0, 0, 0, 0),
            arrayOf(1, 0, 1, 0, 0, 1, 0, 1, 0),
            arrayOf(0, 1, 0, 1, 0, 0, 1, 0, 0),
            arrayOf(1, 1, 0, 0, 1, 1, 1, 1, 0),
            arrayOf(0, 1, 1, 1, 0, 0, 1, 1, 1)
        )
    )

    var hard = arrayOf(
        arrayOf(
            arrayOf(3, 6, 5, 1, 4, 8, 7, 2, 9),
            arrayOf(8, 7, 2, 3, 9, 6, 5, 4, 1),
            arrayOf(9, 4, 1, 5, 7, 2, 3, 6, 8),
            arrayOf(4, 9, 6, 8, 3, 7, 1, 5, 2),
            arrayOf(5, 8, 3, 2, 1, 9, 6, 7, 4),
            arrayOf(1, 2, 7, 6, 5, 4, 9, 8, 3),
            arrayOf(6, 1, 4, 9, 2, 5, 8, 3, 7),
            arrayOf(2, 5, 9, 7, 8, 3, 4, 1, 6),
            arrayOf(7, 3, 8, 4, 6, 1, 2, 9, 5)
        ),
        arrayOf(
            arrayOf(1, 1, 0, 0, 1, 0, 1, 0, 0),
            arrayOf(0, 0, 0, 0, 0, 1, 0, 0, 1),
            arrayOf(0, 0, 1, 0, 1, 0, 0, 1, 0),
            arrayOf(0, 0, 0, 0, 0, 0, 1, 1, 1),
            arrayOf(1, 1, 1, 0, 1, 0, 0, 1, 1),
            arrayOf(1, 1, 0, 0, 0, 0, 1, 0, 0),
            arrayOf(1, 1, 0, 0, 1, 1, 0, 0, 0),
            arrayOf(0, 0, 0, 0, 0, 1, 0, 0, 0),
            arrayOf(1, 0, 1, 0, 0, 0, 0, 0, 0)
        ),
        arrayOf(
            arrayOf(9, 2, 8, 5, 7, 1, 4, 3, 6),
            arrayOf(5, 3, 6, 4, 2, 9, 1, 8, 7),
            arrayOf(4, 7, 1, 8, 3, 6, 9, 5, 2),
            arrayOf(1, 5, 3, 9, 6, 7, 2, 4, 8),
            arrayOf(2, 6, 7, 1, 8, 4, 5, 9, 3),
            arrayOf(8, 4, 9, 2, 5, 3, 6, 7, 1),
            arrayOf(6, 8, 5, 3, 4, 2, 7, 1, 9),
            arrayOf(7, 9, 4, 6, 1, 8, 3, 2, 5),
            arrayOf(3, 1, 2, 7, 9, 5, 8, 6, 4)
        ),
        arrayOf(
            arrayOf(0, 1, 0, 1, 1, 1, 0, 1, 0),
            arrayOf(0, 0, 0, 0, 1, 1, 0, 0, 0),
            arrayOf(0, 0, 0, 1, 0, 0, 0, 1, 0),
            arrayOf(0, 0, 0, 1, 0, 0, 1, 1, 0),
            arrayOf(1, 0, 0, 0, 1, 0, 0, 0, 0),
            arrayOf(0, 0, 1, 0, 0, 1, 0, 0, 1),
            arrayOf(0, 0, 1, 1, 0, 1, 0, 1, 1),
            arrayOf(0, 1, 0, 0, 1, 0, 0, 1, 1),
            arrayOf(0, 1, 0, 0, 0, 1, 0, 0, 0)
        ),
        arrayOf(
            arrayOf(3, 2, 6, 8, 4, 5, 1, 9, 7),
            arrayOf(9, 5, 1, 6, 7, 3, 4, 8, 2),
            arrayOf(8, 7, 4, 9, 1, 2, 3, 5, 6),
            arrayOf(4, 3, 2, 5, 6, 9, 7, 1, 8),
            arrayOf(5, 9, 8, 7, 2, 1, 6, 4, 3),
            arrayOf(6, 1, 7, 3, 8, 4, 9, 2, 5),
            arrayOf(7, 6, 9, 4, 5, 8, 2, 3, 1),
            arrayOf(1, 8, 3, 2, 9, 6, 5, 7, 4),
            arrayOf(2, 4, 5, 1, 3, 7, 8, 6, 9)
        ),
        arrayOf(
            arrayOf(0, 0, 1, 0, 1, 0, 1, 0, 1),
            arrayOf(1, 1, 0, 1, 0, 0, 0, 0, 0),
            arrayOf(0, 0, 1, 0, 0, 1, 1, 0, 0),
            arrayOf(0, 0, 1, 0, 0, 0, 1, 0, 0),
            arrayOf(1, 0, 0, 0, 0, 1, 1, 0, 1),
            arrayOf(0, 0, 0, 0, 1, 0, 0, 1, 0),
            arrayOf(1, 0, 0, 0, 0, 0, 1, 0, 1),
            arrayOf(0, 0, 0, 1, 1, 0, 0, 0, 0),
            arrayOf(1, 1, 1, 1, 1, 0, 0, 0, 0)
        )
    )
}
