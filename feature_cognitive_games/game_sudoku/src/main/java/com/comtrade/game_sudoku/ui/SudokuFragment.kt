/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: SudokuFragment.kt
 * Author: sdelic
 *
 * History:  18.4.22. 13:53 created
 */

package com.comtrade.game_sudoku.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import com.comtrade.game_sudoku.R
import com.comtrade.game_sudoku.Sudoku
import com.comtrade.game_sudoku.databinding.FragmentSudokuBinding


class SudokuFragment : Fragment() {

    private var _binding: FragmentSudokuBinding? = null
    private val binding get() = _binding!!

    private var game: Int? = null

    companion object {
        const val TAG: String = "SudokuFragment"
        const val GAME_KEY = "GAME_KEY"

        fun newInstance(game: Int): SudokuFragment {
            val sudokuFragment = SudokuFragment()
            val extras = bundleOf(GAME_KEY to game)
            sudokuFragment.arguments = extras
            return sudokuFragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentSudokuBinding.inflate(inflater, container, false)
        val gameData = arguments?.getInt(GAME_KEY)
        if (gameData != null) {
            game = gameData
        } else {
            activity?.supportFragmentManager?.popBackStack()
            return null
        }
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val supportedCognitiveGames = resources.getStringArray(R.array.supported_cognitive_games)
        var gameName = ""
        game?.let {
            if (it >= 0 && it < supportedCognitiveGames.size) {
                gameName = supportedCognitiveGames[it]
            }
        }

        binding.sudokuView.sudoku = Sudoku(game ?: 0)
        binding.sudokuFragmentLevel.text = gameName
        binding.sudokuFragmentErrors.text = getString(
            R.string.sudoku_game_screen_errors_placeholder,
            binding.sudokuView.sudoku!!.errors.toString()
        )
        setListeners()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun setListeners() {
        val numberButtons = arrayOf(
            binding.sudokuButton1,
            binding.sudokuButton2,
            binding.sudokuButton3,
            binding.sudokuButton4,
            binding.sudokuButton5,
            binding.sudokuButton6,
            binding.sudokuButton7,
            binding.sudokuButton8,
            binding.sudokuButton9,
        )

        for (button in numberButtons.indices) {
            numberButtons[button].setOnClickListener {
                val buttonNumber = button + 1
                binding.sudokuView.buttonClick(buttonNumber)

                updateErrorCount()
            }
        }
    }

    private fun updateErrorCount() {
        if (binding.sudokuView.sudoku != null) {
            binding.sudokuFragmentErrors.text = getString(
                R.string.sudoku_game_screen_errors_placeholder,
                binding.sudokuView.sudoku!!.errors.toString()
            )
        }
    }
}