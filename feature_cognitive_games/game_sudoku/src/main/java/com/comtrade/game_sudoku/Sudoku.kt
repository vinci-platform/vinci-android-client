/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: Sudoku.kt
 * Author: sdelic
 *
 * History:  21.2.22. 10:43 created
 */

package com.comtrade.game_sudoku

import com.comtrade.domain_games.types.CognitiveGames

internal class Sudoku(game: Int) {
    var solution = arrayOf(
        arrayOf(0, 0, 0, 0, 0, 0, 0, 0, 0),
        arrayOf(0, 0, 0, 0, 0, 0, 0, 0, 0),
        arrayOf(0, 0, 0, 0, 0, 0, 0, 0, 0),
        arrayOf(0, 0, 0, 0, 0, 0, 0, 0, 0),
        arrayOf(0, 0, 0, 0, 0, 0, 0, 0, 0),
        arrayOf(0, 0, 0, 0, 0, 0, 0, 0, 0),
        arrayOf(0, 0, 0, 0, 0, 0, 0, 0, 0),
        arrayOf(0, 0, 0, 0, 0, 0, 0, 0, 0),
        arrayOf(0, 0, 0, 0, 0, 0, 0, 0, 0)
    )
    var cells = arrayOf(
        arrayOf(0, 0, 0, 0, 0, 0, 0, 0, 0),
        arrayOf(0, 0, 0, 0, 0, 0, 0, 0, 0),
        arrayOf(0, 0, 0, 0, 0, 0, 0, 0, 0),
        arrayOf(0, 0, 0, 0, 0, 0, 0, 0, 0),
        arrayOf(0, 0, 0, 0, 0, 0, 0, 0, 0),
        arrayOf(0, 0, 0, 0, 0, 0, 0, 0, 0),
        arrayOf(0, 0, 0, 0, 0, 0, 0, 0, 0),
        arrayOf(0, 0, 0, 0, 0, 0, 0, 0, 0),
        arrayOf(0, 0, 0, 0, 0, 0, 0, 0, 0)
    )
    private var isValid = false
    var errors = 0

    init {
        val number = (0..2).random()
        val transform = (0..3).random()
        val sudokuExamples = SudokuExamples()

        val tempSolution = if (game == CognitiveGames.SUDOKU_EASY.ordinal) {
            sudokuExamples.easy[2 * number]
        } else {
            sudokuExamples.hard[2 * number]
        }
        val tempCells = if (game == CognitiveGames.SUDOKU_EASY.ordinal) {
            sudokuExamples.easy[2 * number + 1]
        } else {
            sudokuExamples.hard[2 * number + 1]
        }

        val replace: List<Int> = listOf(1, 2, 3, 4, 5, 6, 7, 8, 9).shuffled()
        for (i in 0..8) {
            for (j in 0..8) {
                when (transform) {
                    3 -> {
                        solution[i][j] = tempSolution[8 - j][8 - i]
                        cells[i][j] = tempCells[8 - j][8 - i]
                    }
                    2 -> {
                        solution[i][j] = tempSolution[i][8 - j]
                        cells[i][j] = tempCells[i][8 - j]
                    }
                    1 -> {
                        solution[i][j] = tempSolution[j][i]
                        cells[i][j] = tempCells[j][i]
                    }
                    else -> {
                        solution[i][j] = tempSolution[i][j]
                        cells[i][j] = tempCells[i][j]
                    }
                }
                solution[i][j] = replace[solution[i][j] - 1]
                cells[i][j] = -1 * solution[i][j] * cells[i][j]
            }
        }
    }

    override fun toString(): String {
        var ret = ""
        for (row in cells) {
            for (column in row) {
                ret += " $column"
            }
            ret += "\n"
        }
        return ret
    }

    fun isValid(): Boolean {
        if (isValid) {
            return true
        }
        for (i in 0..8) {
            for (j in 0..8) {
                if (cells[i][j] == 0) {
                    return false
                }
                if (cells[i][j] > 0 && cells[i][j] != solution[i][j]) {
                    return false
                }
            }
        }
        isValid = true
        return true
    }
}