package com.comtrade.game_sudoku.util

import androidx.annotation.IdRes
import androidx.annotation.StringRes
import androidx.test.espresso.Espresso
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers.*
import org.hamcrest.CoreMatchers.not
import org.hamcrest.core.AllOf

/**
 * Assert all views with the given resource Id's are displayed using the Espresso testing library view assertions.
 */
internal fun assertViewsDisplayed(@IdRes vararg viewIds: Int) {
    for (viewId in viewIds) {
        Espresso.onView(withId(viewId))
            .check(ViewAssertions.matches(isCompletelyDisplayed()))
    }
}

/**
 * Assert all views with the given resource Id's are not displayed using the Espresso testing library view assertions.
 */
internal fun assertViewsNotDisplayed(@IdRes vararg viewIds: Int) {
    for (viewId in viewIds) {
        Espresso.onView(withId(viewId))
            .check(ViewAssertions.matches(not(isDisplayed())))
    }
}

/**
 * Espresso assertion of input fields for the correct hint, empty state and input type.
 */
internal fun assertEditFieldForHintAndInputType(
    @IdRes viewId: Int,
    @StringRes hintId: Int,
    inputType: Int
) {
    Espresso.onView(withId(viewId)).check(
        ViewAssertions.matches(
            AllOf.allOf(
                withHint(hintId),
                withText(""),
                withInputType(inputType)
            )
        )
    )
}

/**
 * Check view for matching text value
 * @param viewId assertion view Id
 * @param stringId String resource Id
 */
internal fun assertViewHasCaption(@IdRes viewId: Int, @StringRes stringId: Int) {
    Espresso.onView(withId(viewId)).check(ViewAssertions.matches(withText(stringId)))
}

/**
 *
 * Assert all views with the given resource Id's are enabled using the Espresso testing library view assertions.
 */
internal fun assertButtonsEnabled(@IdRes vararg viewIds: Int) {
    for (buttonId in viewIds) {
        Espresso.onView(withId(buttonId))
            .check(ViewAssertions.matches(isEnabled()))
    }
}