/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: SudokuFragmentTest.kt
 * Author: sdelic
 *
 * History:  18.4.22. 13:57 created
 */

package com.comtrade.game_sudoku.ui

import android.content.Context
import androidx.core.os.bundleOf
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import com.comtrade.domain_games.types.CognitiveGames
import com.comtrade.game_sudoku.R
import com.comtrade.game_sudoku.util.assertViewsDisplayed
import org.junit.Before
import org.junit.Test

/**
 * SudokuFragment screen UI tests.
 * Assert all of the view components are displayed.
 * Assert that all views are in their correct initial state.
 */
internal class SudokuFragmentTest {

    @Before
    fun setUp() {
        //Providing test data
        val arguments = bundleOf(
            SudokuFragment.GAME_KEY to CognitiveGames.SUDOKU_EASY,
        )
        launchFragmentInContainer<SudokuFragment>(arguments)
    }

    @Test
    fun sudoku_screen_is_displayed() {
        assertViewsDisplayed(
            R.id.sudoku_title_layout,
            R.id.sudoku_fragment_level,
            R.id.sudoku_fragment_errors,
            R.id.sudoku_view,
            R.id.sudoku_grid,
            R.id.sudoku_button_1,
            R.id.sudoku_button_2,
            R.id.sudoku_button_3,
            R.id.sudoku_button_4,
            R.id.sudoku_button_5,
            R.id.sudoku_button_6,
            R.id.sudoku_button_7,
            R.id.sudoku_button_8,
            R.id.sudoku_button_9
        )
    }

    @Test
    fun easy_level_text_is_displayed() {
        onView(withId(R.id.sudoku_fragment_level)).check(matches(withText("Sudoku - Easy")))
    }

    @Test
    fun error_count_text_0_is_displayed_at_start() {
        val initialErrorText = ApplicationProvider.getApplicationContext<Context>()
            .getString(R.string.sudoku_game_screen_errors_placeholder, 0)
        onView(withId(R.id.sudoku_fragment_errors)).check(matches(withText(initialErrorText)))
    }

    /**
     * From 1 to 9 keys displayed.
     */
    @Test
    fun numbered_buttons_have_correct_captions() {
        val numberedButtons = arrayOf(
            R.id.sudoku_button_1,
            R.id.sudoku_button_2,
            R.id.sudoku_button_3,
            R.id.sudoku_button_4,
            R.id.sudoku_button_5,
            R.id.sudoku_button_6,
            R.id.sudoku_button_7,
            R.id.sudoku_button_8,
            R.id.sudoku_button_9
        )
        for (button in numberedButtons.indices) {
            val value = button + 1
            onView(withId(numberedButtons[button])).check(matches(withText(value.toString())))
        }
    }

}