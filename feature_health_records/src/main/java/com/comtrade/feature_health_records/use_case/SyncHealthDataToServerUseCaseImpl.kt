/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: SyncHealthDataToServerUseCaseImpl.kt
 * Author: sdelic
 *
 * History:  3.6.22. 11:44 created
 */

package com.comtrade.feature_health_records.use_case

import com.comtrade.domain.contracts.repository.IAppRepository
import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.contracts.repository.user.UserServiceRepository
import com.comtrade.domain.contracts.usecases.health.ISyncHealthDataToServerUseCase
import com.comtrade.domain.entities.user.data.PostHealthDataRequest
import javax.inject.Inject

/**
 * Implementation detail for synchronizing health record data to the server use case.
 */
@Suppress("SENSELESS_COMPARISON")
internal class SyncHealthDataToServerUseCaseImpl @Inject constructor(
    private val prefs: IPrefs,
    private val appRepository: IAppRepository,
    private val userServiceRepository: UserServiceRepository
) : ISyncHealthDataToServerUseCase {
    override suspend fun syncHealthToVinci(): Boolean {

        val userId = prefs.userId

        if (userId == null || userId < 1L) {
            throw Exception("Invalid UserId passed: $userId")
        }

        val userToken = prefs.token

        if (userToken.isBlank()) {
            throw Exception("No valid User token provided: $userToken")
        }

        val healthRecords = appRepository.getHealthRecordsForSync(userId)

        for (healthRecord in healthRecords) {
            val request = PostHealthDataRequest(
                healthRecord.userId,
                healthRecord.timestamp,
                healthRecord.score,
                healthRecord.available_score,
                healthRecord.stepsScore,
                healthRecord.whoqolScore,
                healthRecord.ipaqScore,
                healthRecord.feelingsScore
            )

            val result =
                userServiceRepository.postHealthData(
                    userToken,
                    request
                )

            if (result) {
                prefs.addLog("Send health data successful: $request")
                val updateResult = appRepository
                    .updateHealthEntity(healthRecord.copy(syncedWithServer = true))
                if (updateResult != 1) {
                    return false
                }
            } else {
                return false
            }
        }

        return true
    }
}