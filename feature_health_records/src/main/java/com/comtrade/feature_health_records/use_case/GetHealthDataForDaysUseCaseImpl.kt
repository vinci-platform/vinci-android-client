/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: GetHealthDataForDaysUseCaseImpl.kt
 * Author: sdelic
 *
 * History:  13.5.22. 14:02 created
 */

package com.comtrade.feature_health_records.use_case

import com.comtrade.domain.contracts.repository.IAppRepository
import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.contracts.usecases.health.IGetHealthDataForDaysUseCase
import com.comtrade.domain.entities.graph.GraphData
import com.comtrade.domain.entities.step.HealthDataType
import com.comtrade.domain.time.AppDateTimeConstants
import java.time.LocalDate
import java.time.ZoneId
import java.util.*
import javax.inject.Inject

/**
 *  Implementation detail for the obtaining of the User's health data for n days use case.
 */
internal class GetHealthDataForDaysUseCaseImpl @Inject constructor(
    private val prefs: IPrefs,
    private val appRepository: IAppRepository
) : IGetHealthDataForDaysUseCase {
    @Suppress("SENSELESS_COMPARISON", "FoldInitializerAndIfToElvis")
    override suspend fun getHealthDataLastDays(
        numberOfDays: Int,
        dataType: HealthDataType
    ): Array<GraphData> {

        val userId = prefs.userId

        if (userId == null) {
            throw Exception("User Id cannot be null: $userId")
        }

        val healthData: MutableList<GraphData> = mutableListOf()
        var date = LocalDate.now().atStartOfDay(ZoneId.of(AppDateTimeConstants.DISPLAY_ZONE_ID))
        for (i in 0 until numberOfDays) {
            val healthRecord = appRepository
                .getHealthRecord(userId, date.toEpochSecond() * 1000)
            val point = GraphData()
            point.value = if (healthRecord != null) {
                when (dataType) {
                    HealthDataType.POINTS -> healthRecord.score.toInt()
                    HealthDataType.STEPS -> (healthRecord.stepsScore * 100).toInt()
                    HealthDataType.IPAQ -> (healthRecord.ipaqScore * 100).toInt()
                    HealthDataType.WHOQOL -> (healthRecord.whoqolScore * 100).toInt()
                }
            } else {
                0
            }
            point.dateTime = Date(date.toEpochSecond() * 1000)
            healthData.add(0, point)
            date = date.minusDays(1)
        }
        return healthData.toTypedArray()
    }
}