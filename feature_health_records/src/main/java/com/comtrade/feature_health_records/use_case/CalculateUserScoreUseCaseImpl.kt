/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: CalculateUserScoreImpl.kt
 * Author: sdelic
 *
 * History:  6.4.22. 12:10 created
 */

package com.comtrade.feature_health_records.use_case

import androidx.annotation.VisibleForTesting
import com.comtrade.domain.contracts.usecases.health.ICalculateUserScoreUseCase
import com.comtrade.domain.entities.health.HealthEntity
import com.comtrade.domain.survey.SurveyConstants
import com.comtrade.domain.survey.SurveyConstants.IPAQ_SURVEY_GOAL
import com.comtrade.domain.survey.SurveyConstants.SURVEY_INTERVAL
import com.comtrade.domain.survey.SurveyTypes
import com.comtrade.domain.time.AppDateTimeConstants
import java.time.LocalDate
import java.time.ZoneId
import javax.inject.Inject

/**
 * Implementation detail for calculating User's score based on the step goal provided.
 */
internal class CalculateUserScoreUseCaseImpl @Inject constructor(
    private val calculateScore: CalculateScore
) : ICalculateUserScoreUseCase {

    private val updatedInterval = 86_400_000 //1 day in milliseconds.

    override suspend fun calculateScore(stepGoal: Int): Int {
        var healthEntity: HealthEntity?
        val utcTime = LocalDate.now().atStartOfDay(ZoneId.of(AppDateTimeConstants.DISPLAY_ZONE_ID))
        val lastHealthRecord =
            calculateScore.getLastHealthUseCase.getLastHealthRecord(calculateScore.prefs.userId)

        var timestamp: Long =
            if (lastHealthRecord != null) lastHealthRecord.timestamp + updatedInterval else utcTime.minusDays(
                7
            ).toEpochSecond() * 1000

        // Calculate values for previous days
        while (timestamp < utcTime.toEpochSecond() * 1000) {
            healthEntity = calculateValue(timestamp, stepGoal)
            calculateScore.insertHealthEntityUseCase.insertHealthEntity(healthEntity)
            timestamp += 86400 * 1000
        }
        // Calculate current value
        healthEntity = calculateValue(utcTime.toEpochSecond() * 1000, stepGoal)

        return healthEntity.score.toInt()
    }

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    suspend fun calculateValue(timestamp: Long, stepGoal: Int): HealthEntity {
        var score = 0.0
        val availableScore = 0.0
        val stepsScore = calculateSteps(timestamp, stepGoal)
        val ipaqScore =
            calculateSurveyScore(
                SurveyTypes.IPAQ,
                timestamp - SURVEY_INTERVAL,
                timestamp + 86400 * 1000
            )
        val whoqolScore = calculateSurveyScore(
            SurveyTypes.WHOQOL_BREF,
            timestamp - SURVEY_INTERVAL,
            timestamp + 86400 * 1000
        )
        val feelingsScore =
            calculateSurveyScore(
                SurveyTypes.FEELINGS,
                timestamp - SURVEY_INTERVAL,
                timestamp + 86400 * 1000
            )

        var healthRecord = HealthEntity(
            -1,
            userId = calculateScore.prefs.userId,
            timestamp = timestamp,
            score = score,
            available_score = availableScore,
            stepsScore = stepsScore,
            ipaqScore = ipaqScore,
            whoqolScore = whoqolScore,
            feelingsScore = feelingsScore,
            syncedWithServer = false
        )


        healthRecord = addScore(healthRecord, healthRecord.stepsScore, .20)
        healthRecord = addScore(healthRecord, healthRecord.ipaqScore, .20)
        healthRecord = addScore(healthRecord, healthRecord.whoqolScore, .40)
        healthRecord = addScore(healthRecord, healthRecord.feelingsScore, .10)

        score =
            if (healthRecord.available_score > 0.0) (healthRecord.score * 100 / healthRecord.available_score) else 0.0

        return healthRecord.copy(score = score)
    }

    /**
     * Gather information from all supported devices and
     */
    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    suspend fun calculateSteps(timestamp: Long, stepGoal: Int): Double {

        val fitBitSteps = calculateScore.prefs.fitBitDevice?.let {
            val fitBitRecord = calculateScore.getDailyStepsUseCase
                .getDailySteps(it.deviceId, timestamp)
            if (fitBitRecord != null) {
                return@let fitBitRecord.steps
            }
            return@let 0
        } ?: 0

        val insolesSteps = calculateScore.prefs.shoeDevice?.let {
            return@let calculateScore.getStepsForPeriodUseCase.getStepsForPeriod(
                it.deviceId,
                timestamp,
                timestamp + 86400 * 1000
            )
        } ?: 0

        val cmdSteps = calculateScore.prefs.cmdDevice?.let {
            return@let calculateScore.getCmdStepsForPeriodUseCase.getCmdStepsForPeriod(
                it.deviceId,
                timestamp,
                timestamp + 86400 * 1000
            )
        } ?: 0

        val numberOfSteps = arrayOf(
            fitBitSteps,
            insolesSteps,
            cmdSteps
        ).sum()

        return if (numberOfSteps > stepGoal)
            if (numberOfSteps > 2 * stepGoal)
                1.5
            else
                1.0 + (numberOfSteps - stepGoal) / 2 / stepGoal
        else
            numberOfSteps.toDouble() / stepGoal
    }

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    suspend fun calculateSurveyScore(surveyType: String, startTime: Long, endTime: Long): Double {
        val surveyData = calculateScore.getSurveyRecordsUseCase
            .getSurveyRecords(calculateScore.prefs.userId, surveyType, startTime, endTime)
        if (surveyData.isNotEmpty()) {
            when (surveyType) {
                SurveyTypes.IPAQ -> {
                    val ret = surveyData[0].scoringResult / IPAQ_SURVEY_GOAL.toDouble()
                    return if (ret > 1.0) 1.0 else ret
                }
                SurveyTypes.WHOQOL_BREF -> {
                    return surveyData[0].scoringResult / SurveyConstants.WHOQOL_BREF_GOAL
                }
                SurveyTypes.FEELINGS -> {
                    return surveyData[0].scoringResult / SurveyConstants.FEELINGS_GOAL
                }
            }
        }
        return 0.0
    }

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    fun addScore(healthEntity: HealthEntity, score: Double, weight: Double): HealthEntity {
        return if (score > 0) {
            val updatedScore = healthEntity.score + weight * score
            val updatedAvailableScore = healthEntity.available_score + weight
            healthEntity.copy(score = updatedScore, available_score = updatedAvailableScore)
        } else {
            healthEntity
        }
    }
}