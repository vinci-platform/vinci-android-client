/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: GetHealthDataForDaysViewModel.kt
 * Author: sdelic
 *
 * History:  13.5.22. 14:43 created
 */

package com.comtrade.feature_health_records.ui

import androidx.lifecycle.*
import com.comtrade.domain.contracts.usecases.health.IGetHealthDataForDaysUseCase
import com.comtrade.domain.entities.graph.GraphData
import com.comtrade.domain.entities.step.HealthDataType
import com.comtrade.domain_ui.state.RequestUIState
import com.comtrade.feature_health_records.ui.GetHealthDataForDaysViewModel.GetDayDataRequest.DefaultState
import com.comtrade.feature_health_records.ui.GetHealthDataForDaysViewModel.GetDayDataRequest.GetData
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

/**
 * Android ViewModel component for handling the possible UI states for the obtaining of the health data for n days use case.
 */
class GetHealthDataForDaysViewModel @Inject constructor(private val getHealthDataForDaysUseCase: IGetHealthDataForDaysUseCase) :
    ViewModel() {

    private val getHealthDataRequest =
        MutableLiveData<GetDayDataRequest>(DefaultState)
    val getHealthDataObserver: LiveData<RequestUIState<Map<HealthDataType, Array<GraphData>>>> =
        Transformations.switchMap(getHealthDataRequest) {
            liveData(context = viewModelScope.coroutineContext + Dispatchers.IO) {
                when (it) {
                    DefaultState -> emit(RequestUIState.DefaultState)
                    is GetData -> {
                        emit(RequestUIState.OnLoading)
                        try {

                            val healthDataTypes = arrayOf(
                                HealthDataType.IPAQ,
                                HealthDataType.POINTS,
                                HealthDataType.WHOQOL,
                                HealthDataType.STEPS
                            )
                            val result = mutableMapOf<HealthDataType, Array<GraphData>>()
                            for (healthDataType in healthDataTypes) {
                                val resultSteps =
                                    getHealthDataForDaysUseCase.getHealthDataLastDays(
                                        it.numberOfDays,
                                        healthDataType
                                    )
                                result[healthDataType] = resultSteps
                            }

                            emit(RequestUIState.OnSuccess(result))
                        } catch (e: Exception) {
                            e.printStackTrace()
                            emit(RequestUIState.OnError(e.message))
                        }
                    }
                }
            }
        }

    /**
     * Obtain health data for the previous n days.
     * @param numberOfDays List of day intervals for which to obtain the step data.
     */
    fun getHealthDataLastDays(numberOfDays: Int) {
        getHealthDataRequest.value = GetData(numberOfDays)
    }

    fun resetState() {
        getHealthDataRequest.value = DefaultState
    }

    /**
     * Get health data data request states.
     * @property GetData the request state.
     * @property DefaultState the reset/default state for an request. Dispatching the event to be handled by some other observing component.
     */
    private sealed class GetDayDataRequest {
        data class GetData(val numberOfDays: Int) : GetDayDataRequest()
        object DefaultState : GetDayDataRequest()
    }
}