/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: CalculateUserHealthScoreViewModel.kt
 * Author: sdelic
 *
 * History:  7.4.22. 11:06 created
 */

package com.comtrade.feature_health_records.ui.score

import androidx.lifecycle.*
import com.comtrade.domain.contracts.usecases.health.ICalculateUserScoreUseCase
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

/**
 * Android ViewModel component for calculating the User's health score based on health records data and step goals.
 */
class CalculateUserHealthScoreViewModel @Inject constructor(private val calculateUserScore: ICalculateUserScoreUseCase) :
    ViewModel() {

    private val calculateUserScoreRequest = MutableLiveData<CalculateUserScoreRequest>()
    val userHealthScore: LiveData<CalculateUserHealthScoreUIState> =
        Transformations.switchMap(calculateUserScoreRequest) {
            liveData(context = viewModelScope.coroutineContext + Dispatchers.IO) {
                emit(CalculateUserHealthScoreUIState.OnLoading)

                try {
                    val data = calculateUserScore.calculateScore(it.stepGoal)
                    emit(CalculateUserHealthScoreUIState.OnSuccess(data))

                } catch (e: Exception) {
                    e.printStackTrace()
                    emit(CalculateUserHealthScoreUIState.OnError(e.message))
                }
            }
        }

    fun calculateHealthScore(stepGoal: Int) {
        calculateUserScoreRequest.value = CalculateUserScoreRequest(stepGoal)
    }

}

/**
 * The immutable data holder for the User-related calculate health score request.
 */
internal data class CalculateUserScoreRequest(val stepGoal: Int)