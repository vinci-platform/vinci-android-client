/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: HealthRecordsModule.kt
 * Author: sdelic
 *
 * History:  5.4.22. 15:49 created
 */

package com.comtrade.feature_health_records.di

import com.comtrade.domain.contracts.repository.IAppRepository
import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.contracts.repository.user.UserServiceRepository
import com.comtrade.domain.contracts.usecases.health.ICalculateUserScoreUseCase
import com.comtrade.domain.contracts.usecases.health.IGetHealthDataForDaysUseCase
import com.comtrade.domain.contracts.usecases.health.ISyncHealthDataToServerUseCase
import com.comtrade.feature_health_records.use_case.*
import com.comtrade.feature_health_records.use_case.steps.GetCmdStepsForPeriodUseCaseImpl
import com.comtrade.feature_health_records.use_case.steps.GetDailyStepsUseCaseImpl
import com.comtrade.feature_health_records.use_case.steps.GetStepsForPeriodUseCaseImpl
import com.comtrade.feature_health_records.use_case.survey.GetSurveyRecordsUseCaseImpl
import dagger.Module
import dagger.Provides

/**
 * Dagger DI health records module. Provides implementation details for the User health-related information.
 */
@Module
class HealthRecordsModule {

    @Provides
    internal fun provideCalculateScoreDependencies(
        prefs: IPrefs,
        appRepository: IAppRepository
    ): CalculateScore =
        CalculateScore(
            prefs = prefs,
            getLastHealthUseCase = GetLastHealthUseCaseImpl(appRepository),
            insertHealthEntityUseCase = InsertHealthEntityUseCaseImpl(appRepository),
            getSurveyRecordsUseCase = GetSurveyRecordsUseCaseImpl(appRepository),
            getDailyStepsUseCase = GetDailyStepsUseCaseImpl(appRepository),
            getStepsForPeriodUseCase = GetStepsForPeriodUseCaseImpl(appRepository),
            getCmdStepsForPeriodUseCase = GetCmdStepsForPeriodUseCaseImpl(appRepository)
        )

    @Provides
    internal fun provideCalculateUserScoreUseCase(calculateScore: CalculateScore): ICalculateUserScoreUseCase =
        CalculateUserScoreUseCaseImpl(calculateScore)

    @Provides
    internal fun provideGetHealthDataForDaysUseCase(
        prefs: IPrefs,
        appRepository: IAppRepository
    ): IGetHealthDataForDaysUseCase = GetHealthDataForDaysUseCaseImpl(prefs, appRepository)

    @Provides
    internal fun provideSyncHealthDataToServerUseCase(
        prefs: IPrefs,
        appRepository: IAppRepository,
        userServiceRepository: UserServiceRepository
    ): ISyncHealthDataToServerUseCase =
        SyncHealthDataToServerUseCaseImpl(prefs, appRepository, userServiceRepository)
}