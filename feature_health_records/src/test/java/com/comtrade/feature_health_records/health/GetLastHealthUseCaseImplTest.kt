/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: GetLastHealthUseCaseImplTest.kt
 * Author: sdelic
 *
 * History:  5.4.22. 15:13 created
 */

package com.comtrade.feature_health_records.health

import com.comtrade.domain.contracts.repository.IAppRepository
import com.comtrade.domain.entities.health.HealthEntity
import com.comtrade.feature_health_records.use_case.GetLastHealthUseCaseImpl
import kotlinx.coroutines.runBlocking
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.*

/**
 * Test scenario for the obtain of the last health record data.
 */
internal class GetLastHealthUseCaseImplTest {

    private lateinit var appRepository: IAppRepository
    private lateinit var getLastHealthUseCaseImpl: GetLastHealthUseCaseImpl

    @Before
    fun setUp() {
        appRepository = mock(IAppRepository::class.java)
        getLastHealthUseCaseImpl = GetLastHealthUseCaseImpl(appRepository)
    }

    @Test
    fun `given a valid user id return last health record`() {
        val testId = 1L
        runBlocking {
            val testData = getTestHealthEntity()
            `when`(appRepository.getLastHealthRecord(anyLong())).thenReturn(testData)
            //Make call
            val data = getLastHealthUseCaseImpl.getLastHealthRecord(testId)
            verify(appRepository, times(1)).getLastHealthRecord(testId)
            assertNotNull(data)
            assertEquals(testData, data)

        }
    }

    @Test
    fun `given a valid user id last health record does not exist`() {
        val testId = 1L
        runBlocking {
            `when`(appRepository.getLastHealthRecord(anyLong())).thenReturn(null)

            //Make call
            val data = getLastHealthUseCaseImpl.getLastHealthRecord(testId)
            verify(appRepository, times(1)).getLastHealthRecord(testId)
            assertNull(data)
        }
    }
}

/**
 * Provide test data.
 */
internal fun getTestHealthEntity(): HealthEntity = HealthEntity(
    1,
    1L,
    System.currentTimeMillis(),
    0.0,
    0.0,
    0.0,
    1.0,
    2.0,
    3.4,
    false
)