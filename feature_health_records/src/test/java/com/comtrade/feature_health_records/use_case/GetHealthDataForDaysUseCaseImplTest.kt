/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: GetHealthDataForDaysUseCaseImplTest.kt
 * Author: sdelic
 *
 * History:  13.5.22. 14:03 created
 */

package com.comtrade.feature_health_records.use_case

import com.comtrade.domain.contracts.repository.IAppRepository
import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.contracts.usecases.health.IGetHealthDataForDaysUseCase
import com.comtrade.domain.entities.step.HealthDataType
import com.comtrade.feature_health_records.health.getTestHealthEntity
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.anyLong
import org.mockito.Mockito

/**
 *  Test scenario for the obtaining of the User's health data for n days use case implementation detail.
 */
internal class GetHealthDataForDaysUseCaseImplTest {
    private val testUserId = 1L
    private val testDayRequest = 7
    private lateinit var prefs: IPrefs
    private lateinit var appRepository: IAppRepository
    private lateinit var getHealthDataForDaysUseCase: IGetHealthDataForDaysUseCase

    @Before
    fun setUp() {
        prefs = Mockito.mock(IPrefs::class.java)
        appRepository = Mockito.mock(IAppRepository::class.java)
        getHealthDataForDaysUseCase = GetHealthDataForDaysUseCaseImpl(prefs, appRepository)
    }

    @Test
    fun `given a valid input when called return list of graph data`() {
        runBlocking {

            val healthDataTypes = arrayOf(
                HealthDataType.IPAQ,
                HealthDataType.POINTS,
                HealthDataType.WHOQOL,
                HealthDataType.STEPS
            )

            for (testDataType in healthDataTypes) {
                val testData = getTestHealthEntity()
                Mockito.`when`(prefs.userId).thenReturn(testUserId)
                Mockito.`when`(appRepository.getHealthRecord(anyLong(), anyLong()))
                    .thenReturn(testData)
                val result =
                    getHealthDataForDaysUseCase.getHealthDataLastDays(testDayRequest, testDataType)

                assertNotNull(result)
                assertEquals(testDayRequest, result.size)
            }

            Mockito.verify(prefs, Mockito.times(healthDataTypes.size)).userId
            Mockito.verify(appRepository, Mockito.times(testDayRequest * healthDataTypes.size))
                .getHealthRecord(anyLong(), anyLong())
        }
    }

    @Test
    fun `given a valid input when called return null for health record`() {
        runBlocking {

            val healthDataTypes = arrayOf(
                HealthDataType.IPAQ,
                HealthDataType.POINTS,
                HealthDataType.WHOQOL,
                HealthDataType.STEPS
            )

            for (testDataType in healthDataTypes) {
                Mockito.`when`(prefs.userId).thenReturn(testUserId)
                Mockito.`when`(appRepository.getHealthRecord(anyLong(), anyLong()))
                    .thenReturn(null)
                val result =
                    getHealthDataForDaysUseCase.getHealthDataLastDays(testDayRequest, testDataType)

                assertNotNull(result)
                assertEquals(testDayRequest, result.size)
            }

            Mockito.verify(prefs, Mockito.times(healthDataTypes.size)).userId
            Mockito.verify(appRepository, Mockito.times(testDayRequest * healthDataTypes.size))
                .getHealthRecord(anyLong(), anyLong())
        }
    }

    @Test(expected = Exception::class)
    fun `given an invalid userId input when called throw exception`() {
        runBlocking {

            val healthDataTypes = arrayOf(
                HealthDataType.IPAQ,
                HealthDataType.POINTS,
                HealthDataType.WHOQOL,
                HealthDataType.STEPS
            )

            for (testDataType in healthDataTypes) {
                val testData = getTestHealthEntity()
                Mockito.`when`(prefs.userId).thenReturn(null)
                Mockito.`when`(appRepository.getHealthRecord(anyLong(), anyLong()))
                    .thenReturn(testData)
                val result =
                    getHealthDataForDaysUseCase.getHealthDataLastDays(testDayRequest, testDataType)

                assertNotNull(result)
                assertEquals(testDayRequest, result.size)
            }

            Mockito.verify(prefs, Mockito.times(healthDataTypes.size)).userId
            Mockito.verify(appRepository, Mockito.times(testDayRequest * healthDataTypes.size))
                .getHealthRecord(anyLong(), anyLong())
        }
    }
}