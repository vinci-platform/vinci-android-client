/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: InsertHealthEntityUseCaseImplTest.kt
 * Author: sdelic
 *
 * History:  6.4.22. 09:31 created
 */

package com.comtrade.feature_health_records.use_case

import com.comtrade.domain.contracts.repository.IAppRepository
import com.comtrade.feature_health_records.health.getTestHealthEntity
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.`when`

/**
 * Test scenario for the persistence of the User health entity.
 */
internal class InsertHealthEntityUseCaseImplTest {
    private lateinit var appRepository: IAppRepository
    private lateinit var insertHealthEntityUseCaseImpl: InsertHealthEntityUseCaseImpl

    @Before
    fun setUp() {
        appRepository = Mockito.mock(IAppRepository::class.java)
        insertHealthEntityUseCaseImpl = InsertHealthEntityUseCaseImpl(appRepository)
    }

    @Test
    fun `given a valid health entity insert the data`() {
        runBlocking {
            val testInsertData = getTestHealthEntity()
            `when`(appRepository.insertHealthEntity(testInsertData)).thenReturn(1L)
            val result = insertHealthEntityUseCaseImpl.insertHealthEntity(testInsertData)
            Mockito.verify(appRepository, Mockito.times(1)).insertHealthEntity(testInsertData)

            assertNotNull(result)
            assertTrue(result > 0)
        }
    }

    @Test
    fun `given an invalid health entity no data inserted`() {
        runBlocking {
            val testInsertData = getTestHealthEntity().copy(id = -1)
            `when`(appRepository.insertHealthEntity(testInsertData)).thenReturn(0L)
            val result = insertHealthEntityUseCaseImpl.insertHealthEntity(testInsertData)
            Mockito.verify(appRepository, Mockito.times(1)).insertHealthEntity(testInsertData)

            assertNotNull(result)
            assertTrue(result == 0L)
        }
    }

}