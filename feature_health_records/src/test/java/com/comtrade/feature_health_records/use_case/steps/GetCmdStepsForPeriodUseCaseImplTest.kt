/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: GetCmdStepsForPeriodUseCaseImplTest.kt
 * Author: sdelic
 *
 * History:  7.4.22. 09:44 created
 */

package com.comtrade.feature_health_records.use_case.steps

import com.comtrade.domain.contracts.repository.IAppRepository
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import java.time.Instant

/**
 * Test scenario for obtaining the Cmd device steps for a
 */
internal class GetCmdStepsForPeriodUseCaseImplTest {
    private val testDeviceId = 1L
    private lateinit var appRepository: IAppRepository
    private lateinit var getCmdStepsForPeriodUseCaseImpl: GetCmdStepsForPeriodUseCaseImpl

    @Before
    fun setUp() {
        appRepository = Mockito.mock(IAppRepository::class.java)
        getCmdStepsForPeriodUseCaseImpl = GetCmdStepsForPeriodUseCaseImpl(appRepository)
    }

    @Test
    fun `given a valid input request return 0 steps`() {
        val startTimePeriod = Instant.now().toEpochMilli()
        val endTimePeriod = Instant.now().toEpochMilli()
        runBlocking {
            Mockito.`when`(
                appRepository.getCmdStepsForPeriod(
                    Mockito.anyLong(),
                    Mockito.anyLong(),
                    Mockito.anyLong()
                )
            ).thenReturn(0)
            val responseData = getCmdStepsForPeriodUseCaseImpl.getCmdStepsForPeriod(
                testDeviceId,
                startTimePeriod,
                endTimePeriod
            )

            Mockito.verify(appRepository, Mockito.times(1))
                .getCmdStepsForPeriod(testDeviceId, startTimePeriod, endTimePeriod)

            assertNotNull(responseData)
            assertEquals(0, responseData)
        }
    }

    @Test
    fun `given a valid input request return number of steps`() {
        val startTimePeriod = Instant.now().toEpochMilli()
        val endTimePeriod = Instant.now().toEpochMilli()
        runBlocking {
            Mockito.`when`(
                appRepository.getCmdStepsForPeriod(
                    Mockito.anyLong(),
                    Mockito.anyLong(),
                    Mockito.anyLong()
                )
            ).thenReturn(10_000)
            val responseData = getCmdStepsForPeriodUseCaseImpl.getCmdStepsForPeriod(
                testDeviceId,
                startTimePeriod,
                endTimePeriod
            )

            Mockito.verify(appRepository, Mockito.times(1))
                .getCmdStepsForPeriod(testDeviceId, startTimePeriod, endTimePeriod)

            assertNotNull(responseData)
            assertEquals(10_000, responseData)
        }
    }

    @Test(expected = Exception::class)
    fun `given an invalid device Id throw exception`() {
        val startTimePeriod = Instant.now().toEpochMilli()
        val endTimePeriod = Instant.now().toEpochMilli()
        runBlocking {
            Mockito.`when`(
                appRepository.getCmdStepsForPeriod(
                    Mockito.anyLong(),
                    Mockito.anyLong(),
                    Mockito.anyLong()
                )
            ).thenReturn(10_000)
            val responseData = getCmdStepsForPeriodUseCaseImpl.getCmdStepsForPeriod(
                -1L,
                startTimePeriod,
                endTimePeriod
            )

            Mockito.verify(appRepository, Mockito.times(1))
                .getCmdStepsForPeriod(-1L, startTimePeriod, endTimePeriod)

            assertNotNull(responseData)
            assertEquals(10_000, responseData)
        }
    }

    @Test
    fun `given an invalid input request return 0 steps`() {
        val startTimePeriod = Instant.now().toEpochMilli()
        runBlocking {
            Mockito.`when`(
                appRepository.getCmdStepsForPeriod(
                    Mockito.anyLong(),
                    Mockito.anyLong(),
                    Mockito.anyLong()
                )
            ).thenReturn(0)
            val responseData = getCmdStepsForPeriodUseCaseImpl.getCmdStepsForPeriod(
                testDeviceId,
                startTimePeriod,
                startTimePeriod
            )

            Mockito.verify(appRepository, Mockito.times(1))
                .getCmdStepsForPeriod(testDeviceId, startTimePeriod, startTimePeriod)

            assertNotNull(responseData)
            assertEquals(0, responseData)
        }
    }
}