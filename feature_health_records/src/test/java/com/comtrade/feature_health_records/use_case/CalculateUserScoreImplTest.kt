/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: CalculateUserScoreImplTest.kt
 * Author: sdelic
 *
 * History:  6.4.22. 12:13 created
 */

package com.comtrade.feature_health_records.use_case

import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.contracts.usecases.health.IGetLastHealthUseCase
import com.comtrade.domain.contracts.usecases.health.IInsertHealthEntityUseCase
import com.comtrade.domain.contracts.usecases.steps.IGetDailyStepsUseCase
import com.comtrade.domain.contracts.usecases.steps.IGetStepsForPeriodUseCase
import com.comtrade.domain.contracts.usecases.steps.cmd.IGetCmdStepsForPeriodUseCase
import com.comtrade.domain.contracts.usecases.survey.IGetSurveyRecordsUseCase
import com.comtrade.domain.survey.SurveyTypes
import com.comtrade.feature_health_records.health.getTestHealthEntity
import com.comtrade.feature_health_records.use_case.survey.getTestSurveyResultData
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.anyLong
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mockito
import java.time.Instant

/**
 * Test scenario for the persistence of the User health entity.
 */
internal class CalculateUserScoreImplTest {
    private val testStepGoal = 1_000
    private lateinit var prefs: IPrefs
    private lateinit var getLastHealthUseCase: IGetLastHealthUseCase
    private lateinit var insertHealthEntityUseCase: IInsertHealthEntityUseCase
    private lateinit var getDailyStepsUseCase: IGetDailyStepsUseCase
    private lateinit var getStepsForPeriodUseCase: IGetStepsForPeriodUseCase
    private lateinit var getCmdStepsForPeriodUseCase: IGetCmdStepsForPeriodUseCase
    private lateinit var getSurveyRecordsUseCase: IGetSurveyRecordsUseCase

    private lateinit var calculateUserScoreImpl: CalculateUserScoreUseCaseImpl

    @Before
    fun setUp() {
        prefs = Mockito.mock(IPrefs::class.java)
        Mockito.`when`(prefs.userId).thenReturn(
            1L
        )
        getLastHealthUseCase = Mockito.mock(IGetLastHealthUseCase::class.java)
        insertHealthEntityUseCase = Mockito.mock(IInsertHealthEntityUseCase::class.java)
        getDailyStepsUseCase = Mockito.mock(IGetDailyStepsUseCase::class.java)
        getStepsForPeriodUseCase = Mockito.mock(IGetStepsForPeriodUseCase::class.java)
        getCmdStepsForPeriodUseCase = Mockito.mock(IGetCmdStepsForPeriodUseCase::class.java)
        getSurveyRecordsUseCase = Mockito.mock(IGetSurveyRecordsUseCase::class.java)
        val calculateScore = CalculateScore(
            prefs,
            getLastHealthUseCase,
            insertHealthEntityUseCase,
            getDailyStepsUseCase,
            getStepsForPeriodUseCase,
            getCmdStepsForPeriodUseCase,
            getSurveyRecordsUseCase
        )
        calculateUserScoreImpl = CalculateUserScoreUseCaseImpl(calculateScore)
    }

    @Test
    fun `given a step goal return a health score value`() {
        runBlocking {
            Mockito.`when`(getLastHealthUseCase.getLastHealthRecord(Mockito.anyLong())).thenReturn(
                getTestHealthEntity()
            )
            Mockito.`when`(
                getSurveyRecordsUseCase.getSurveyRecords(
                    anyLong(),
                    anyString(),
                    anyLong(),
                    anyLong()
                )
            ).thenReturn(getTestSurveyResultData(100, SurveyTypes.WHOQOL_BREF))
            val resultData = calculateUserScoreImpl.calculateScore(testStepGoal)
            assertNotNull(resultData)

        }
    }

    @Test
    fun `given a problem with calculating health score return 0`() {
        runBlocking {
            Mockito.`when`(getLastHealthUseCase.getLastHealthRecord(Mockito.anyLong())).thenReturn(
                getTestHealthEntity()
            )
            Mockito.`when`(
                getSurveyRecordsUseCase.getSurveyRecords(
                    anyLong(),
                    anyString(),
                    anyLong(),
                    anyLong()
                )
            ).thenReturn(emptyList())
            val resultData = calculateUserScoreImpl.calculateScore(testStepGoal)
            assertNotNull(resultData)
            assertEquals(0, resultData)
        }
    }

    @Test
    fun `given a valid time interval and step goal return health record`() {
        runBlocking {
            val timestamp = Instant.now().toEpochMilli()

            Mockito.`when`(getLastHealthUseCase.getLastHealthRecord(Mockito.anyLong())).thenReturn(
                getTestHealthEntity()
            )
            Mockito.`when`(
                getSurveyRecordsUseCase.getSurveyRecords(
                    anyLong(),
                    anyString(),
                    anyLong(),
                    anyLong()
                )
            ).thenReturn(emptyList())
            val resultData =
                calculateUserScoreImpl.calculateValue(timestamp, testStepGoal)

            assertNotNull(resultData)
        }
    }
}