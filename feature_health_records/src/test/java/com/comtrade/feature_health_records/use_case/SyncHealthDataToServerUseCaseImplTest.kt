/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: SyncHealthDataToServerUseCaseImplTest.kt
 * Author: sdelic
 *
 * History:  3.6.22. 11:44 created
 */

package com.comtrade.feature_health_records.use_case

import com.comtrade.domain.contracts.repository.IAppRepository
import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.contracts.repository.user.UserServiceRepository
import com.comtrade.domain.contracts.usecases.health.ISyncHealthDataToServerUseCase
import com.comtrade.domain.entities.health.HealthEntity
import com.comtrade.domain.entities.user.data.PostHealthDataRequest
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mockito
import java.time.Instant

/**
 * Test scenario for synchronizing health record data to the server use case implementation detail.
 */
internal class SyncHealthDataToServerUseCaseImplTest {
    private val testUserId = 1L
    private val testToken = "testToken"
    private val testRecordsSize = 1_000

    private lateinit var prefs: IPrefs
    private lateinit var appRepository: IAppRepository
    private lateinit var userServiceRepository: UserServiceRepository
    private lateinit var syncHealthDataToServerUseCase: ISyncHealthDataToServerUseCase

    @Before
    fun setUp() {
        prefs = Mockito.mock(IPrefs::class.java)
        appRepository = Mockito.mock(IAppRepository::class.java)
        userServiceRepository = Mockito.mock(UserServiceRepository::class.java)
        syncHealthDataToServerUseCase =
            SyncHealthDataToServerUseCaseImpl(prefs, appRepository, userServiceRepository)
    }

    @Test
    fun `given valid input data when called return true`() {
        runBlocking {
            val testData = provideTestHealthRecords()
            Mockito.`when`(prefs.userId).thenReturn(testUserId)
            Mockito.`when`(prefs.token).thenReturn(testToken)
            Mockito.`when`(appRepository.getHealthRecordsForSync(testUserId)).thenReturn(testData)
            Mockito.`when`(
                userServiceRepository.postHealthData(
                    anyString(), any(
                        PostHealthDataRequest::class.java
                    )
                )
            ).thenReturn(true)

            Mockito.`when`(appRepository.updateHealthEntity(any(HealthEntity::class.java)))
                .thenReturn(1)


            //execute call
            val result = syncHealthDataToServerUseCase.syncHealthToVinci()

            Mockito.verify(prefs, Mockito.times(1)).userId
            Mockito.verify(prefs, Mockito.times(1)).token
            Mockito.verify(appRepository, Mockito.times(1)).getHealthRecordsForSync(testUserId)
            Mockito.verify(userServiceRepository, Mockito.times(testRecordsSize)).postHealthData(
                anyString(), any(
                    PostHealthDataRequest::class.java
                )
            )

            Mockito.verify(appRepository, Mockito.times(testRecordsSize))
                .updateHealthEntity(any(HealthEntity::class.java))


            assertTrue(result)
        }
    }

    @Test
    fun `given valid input data when called return true for no sync records`() {
        runBlocking {
            Mockito.`when`(prefs.userId).thenReturn(testUserId)
            Mockito.`when`(prefs.token).thenReturn(testToken)
            Mockito.`when`(appRepository.getHealthRecordsForSync(testUserId))
                .thenReturn(emptyList())

            //execute call
            val result = syncHealthDataToServerUseCase.syncHealthToVinci()

            Mockito.verify(prefs, Mockito.times(1)).userId
            Mockito.verify(prefs, Mockito.times(1)).token
            Mockito.verify(appRepository, Mockito.times(1)).getHealthRecordsForSync(testUserId)
            Mockito.verify(userServiceRepository, Mockito.times(0)).postHealthData(
                anyString(), any(
                    PostHealthDataRequest::class.java
                )
            )

            Mockito.verify(appRepository, Mockito.times(0))
                .updateHealthEntity(any(HealthEntity::class.java))


            assertTrue(result)
        }
    }

    @Test(expected = Exception::class)
    fun `given invalid userId data when called throw exception`() {
        runBlocking {
            val testData = provideTestHealthRecords()
            Mockito.`when`(prefs.userId).thenReturn(null)
            Mockito.`when`(prefs.token).thenReturn(testToken)
            Mockito.`when`(appRepository.getHealthRecordsForSync(testUserId)).thenReturn(testData)
            Mockito.`when`(
                userServiceRepository.postHealthData(
                    anyString(), any(
                        PostHealthDataRequest::class.java
                    )
                )
            ).thenReturn(true)

            Mockito.`when`(appRepository.updateHealthEntity(any(HealthEntity::class.java)))
                .thenReturn(1)


            //execute call
            val result = syncHealthDataToServerUseCase.syncHealthToVinci()

            Mockito.verify(prefs, Mockito.times(1)).userId
            Mockito.verify(prefs, Mockito.times(1)).token
            Mockito.verify(appRepository, Mockito.times(1)).getHealthRecordsForSync(testUserId)
            Mockito.verify(userServiceRepository, Mockito.times(testRecordsSize)).postHealthData(
                anyString(), any(
                    PostHealthDataRequest::class.java
                )
            )

            Mockito.verify(appRepository, Mockito.times(testRecordsSize))
                .updateHealthEntity(any(HealthEntity::class.java))


            assertFalse(result)
        }
    }

    @Test(expected = Exception::class)
    fun `given invalid token input data when called throw exception`() {
        runBlocking {
            val testData = provideTestHealthRecords()
            Mockito.`when`(prefs.userId).thenReturn(testUserId)
            Mockito.`when`(prefs.token).thenReturn("")
            Mockito.`when`(appRepository.getHealthRecordsForSync(testUserId)).thenReturn(testData)
            Mockito.`when`(
                userServiceRepository.postHealthData(
                    anyString(), any(
                        PostHealthDataRequest::class.java
                    )
                )
            ).thenReturn(true)

            Mockito.`when`(appRepository.updateHealthEntity(any(HealthEntity::class.java)))
                .thenReturn(1)


            //execute call
            val result = syncHealthDataToServerUseCase.syncHealthToVinci()

            Mockito.verify(prefs, Mockito.times(1)).userId
            Mockito.verify(prefs, Mockito.times(1)).token


            assertFalse(result)
        }
    }

    @Test
    fun `given valid input data when called return false for failed network call`() {
        runBlocking {
            val testData = provideTestHealthRecords()
            Mockito.`when`(prefs.userId).thenReturn(testUserId)
            Mockito.`when`(prefs.token).thenReturn(testToken)
            Mockito.`when`(appRepository.getHealthRecordsForSync(testUserId)).thenReturn(testData)
            Mockito.`when`(
                userServiceRepository.postHealthData(
                    anyString(), any(
                        PostHealthDataRequest::class.java
                    )
                )
            ).thenReturn(false)

            Mockito.`when`(appRepository.updateHealthEntity(any(HealthEntity::class.java)))
                .thenReturn(1)


            //execute call
            val result = syncHealthDataToServerUseCase.syncHealthToVinci()

            Mockito.verify(prefs, Mockito.times(1)).userId
            Mockito.verify(prefs, Mockito.times(1)).token
            Mockito.verify(appRepository, Mockito.times(1)).getHealthRecordsForSync(testUserId)
            Mockito.verify(userServiceRepository, Mockito.times(1)).postHealthData(
                anyString(), any(
                    PostHealthDataRequest::class.java
                )
            )

            Mockito.verify(appRepository, Mockito.times(0))
                .updateHealthEntity(any(HealthEntity::class.java))


            assertFalse(result)
        }
    }

    @Test
    fun `given valid input data when called return false for database update problem`() {
        runBlocking {
            val testData = provideTestHealthRecords()
            Mockito.`when`(prefs.userId).thenReturn(testUserId)
            Mockito.`when`(prefs.token).thenReturn(testToken)
            Mockito.`when`(appRepository.getHealthRecordsForSync(testUserId)).thenReturn(testData)
            Mockito.`when`(
                userServiceRepository.postHealthData(
                    anyString(), any(
                        PostHealthDataRequest::class.java
                    )
                )
            ).thenReturn(true)

            Mockito.`when`(appRepository.updateHealthEntity(any(HealthEntity::class.java)))
                .thenReturn(-1)


            //execute call
            val result = syncHealthDataToServerUseCase.syncHealthToVinci()

            Mockito.verify(prefs, Mockito.times(1)).userId
            Mockito.verify(prefs, Mockito.times(1)).token
            Mockito.verify(appRepository, Mockito.times(1)).getHealthRecordsForSync(testUserId)
            Mockito.verify(userServiceRepository, Mockito.times(1)).postHealthData(
                anyString(), any(
                    PostHealthDataRequest::class.java
                )
            )

            Mockito.verify(appRepository, Mockito.times(1))
                .updateHealthEntity(any(HealthEntity::class.java))


            assertFalse(result)
        }
    }

    private fun provideTestHealthRecords(): List<HealthEntity> {
        val testData = mutableListOf<HealthEntity>()

        for (i in 0 until testRecordsSize) {
            testData.add(
                HealthEntity(
                    i,
                    testUserId,
                    Instant.now().toEpochMilli(),
                    0.0,
                    0.0,
                    0.0,
                    1.0,
                    2.0,
                    3.4,
                    false
                )
            )
        }

        return testData
    }
}

/**
 * Kotlin null type handling for Mockito.
 */
internal fun <T> any(type: Class<T>): T = Mockito.any(type)