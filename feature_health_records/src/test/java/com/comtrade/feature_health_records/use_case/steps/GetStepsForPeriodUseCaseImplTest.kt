/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: GetStepsForPeriodUseCaseImplTest.kt
 * Author: sdelic
 *
 * History:  7.4.22. 09:18 created
 */

package com.comtrade.feature_health_records.use_case.steps

import com.comtrade.domain.contracts.repository.IAppRepository
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import org.mockito.Mockito.anyLong
import java.time.Instant

/**
 * Test scenario for the obtaining of step data for a given period use case.
 */
internal class GetStepsForPeriodUseCaseImplTest {
    private val testDeviceId = 1L
    private lateinit var appRepository: IAppRepository
    private lateinit var getStepsForPeriodUseCaseImpl: GetStepsForPeriodUseCaseImpl

    @Before
    fun setUp() {
        appRepository = Mockito.mock(IAppRepository::class.java)
        getStepsForPeriodUseCaseImpl = GetStepsForPeriodUseCaseImpl(appRepository)
    }

    @Test
    fun `given a valid input request return 0 steps for period`() {
        val startPeriodTimeStamp = Instant.now().toEpochMilli()
        val endPeriodTimeStamp = Instant.now().toEpochMilli()
        runBlocking {
            `when`(
                appRepository.getStepsForPeriod(
                    anyLong(),
                    anyLong(),
                    anyLong()
                )
            ).thenReturn(0)
            val responseData = getStepsForPeriodUseCaseImpl.getStepsForPeriod(
                testDeviceId,
                startPeriodTimeStamp,
                endPeriodTimeStamp
            )

            Mockito.verify(appRepository, Mockito.times(1))
                .getStepsForPeriod(testDeviceId, startPeriodTimeStamp, endPeriodTimeStamp)


            assertNotNull(responseData)
            assertEquals(0, responseData)
        }
    }

    @Test
    fun `given a valid input request return steps for period`() {
        val startPeriodTimeStamp = Instant.now().toEpochMilli()
        val endPeriodTimeStamp = Instant.now().toEpochMilli()
        runBlocking {
            `when`(
                appRepository.getStepsForPeriod(
                    anyLong(),
                    anyLong(),
                    anyLong()
                )
            ).thenReturn(10_000)
            val responseData = getStepsForPeriodUseCaseImpl.getStepsForPeriod(
                testDeviceId,
                startPeriodTimeStamp,
                endPeriodTimeStamp
            )

            Mockito.verify(appRepository, Mockito.times(1))
                .getStepsForPeriod(testDeviceId, startPeriodTimeStamp, endPeriodTimeStamp)


            assertNotNull(responseData)
            assertEquals(10_000, responseData)
        }
    }

    @Test(expected = Exception::class)
    fun `given an invalid device Id throw exception`() {
        val startPeriodTimeStamp = Instant.now().toEpochMilli()
        val endPeriodTimeStamp = Instant.now().toEpochMilli()
        runBlocking {
            `when`(
                appRepository.getStepsForPeriod(
                    anyLong(),
                    anyLong(),
                    anyLong()
                )
            ).thenReturn(10_000)
            val responseData = getStepsForPeriodUseCaseImpl.getStepsForPeriod(
                -1L,
                startPeriodTimeStamp,
                endPeriodTimeStamp
            )

            Mockito.verify(appRepository, Mockito.times(1))
                .getStepsForPeriod(-1L, startPeriodTimeStamp, endPeriodTimeStamp)


            assertNotNull(responseData)
            assertEquals(10_000, responseData)
        }
    }

    @Test
    fun `given an invalid input request return 0 steps`() {
        val startPeriodTimeStamp = Instant.now().toEpochMilli()
        runBlocking {
            `when`(
                appRepository.getStepsForPeriod(
                    anyLong(),
                    anyLong(),
                    anyLong()
                )
            ).thenReturn(0)
            val responseData = getStepsForPeriodUseCaseImpl.getStepsForPeriod(
                testDeviceId,
                startPeriodTimeStamp,
                startPeriodTimeStamp
            )

            Mockito.verify(appRepository, Mockito.times(1))
                .getStepsForPeriod(testDeviceId, startPeriodTimeStamp, startPeriodTimeStamp)


            assertNotNull(responseData)
            assertEquals(0, responseData)
        }
    }
}