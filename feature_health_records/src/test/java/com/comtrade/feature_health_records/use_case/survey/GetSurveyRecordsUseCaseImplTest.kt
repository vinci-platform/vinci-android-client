/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: GetSurveyRecordsUseCaseImplTest.kt
 * Author: sdelic
 *
 * History:  6.4.22. 16:43 created
 */

package com.comtrade.feature_health_records.use_case.survey

import com.comtrade.domain.contracts.repository.IAppRepository
import com.comtrade.domain.entities.survey.SurveyDataModel
import com.comtrade.domain.survey.SurveyTypes
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.anyLong
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mockito
import java.time.Instant

/**
 * Test scenario for the implementation detail for obtaining the survey records data.
 */
internal class GetSurveyRecordsUseCaseImplTest {
    private val testUserId = 1L
    private val surveyTypes = arrayOf(
        SurveyTypes.FEELINGS,
        SurveyTypes.IPAQ,
        SurveyTypes.WHOQOL_BREF
    )
    private lateinit var appRepository: IAppRepository
    private lateinit var getSurveyRecordsUseCaseImpl: GetSurveyRecordsUseCaseImpl

    @Before
    fun setUp() {
        appRepository = Mockito.mock(IAppRepository::class.java)
        getSurveyRecordsUseCaseImpl = GetSurveyRecordsUseCaseImpl(appRepository)
    }

    @Test
    fun `given a valid input and survey type return empty survey records`() {
        runBlocking {

            for (surveyType in surveyTypes) {
                Mockito.`when`(
                    appRepository.getSurveyRecords(
                        anyLong(), anyString(), anyLong(),
                        anyLong()
                    )
                ).thenReturn(listOf())
                val requestData = GetSurveyRecordsRequest(
                    testUserId,
                    surveyType,
                    Instant.now().toEpochMilli(),
                    Instant.now().toEpochMilli()
                )
                val resultData = getSurveyRecordsUseCaseImpl.getSurveyRecords(
                    requestData.userId,
                    requestData.surveyType,
                    requestData.startTime,
                    requestData.endTime
                )

                Mockito.verify(appRepository, Mockito.times(1)).getSurveyRecords(
                    requestData.userId,
                    requestData.surveyType,
                    requestData.startTime,
                    requestData.endTime
                )

                assertNotNull(resultData)
                assertEquals(0, resultData.size)
            }
        }
    }

    @Test
    fun `given a valid input and survey type return list of survey records`() {
        val testItemNumber = 100
        runBlocking {

            for (surveyType in surveyTypes) {
                Mockito.`when`(
                    appRepository.getSurveyRecords(
                        anyLong(), anyString(), anyLong(),
                        anyLong()
                    )
                ).thenReturn(getTestSurveyResultData(testItemNumber, surveyType))
                val requestData = GetSurveyRecordsRequest(
                    testUserId,
                    surveyType,
                    Instant.now().toEpochMilli(),
                    Instant.now().toEpochMilli()
                )
                val resultData = getSurveyRecordsUseCaseImpl.getSurveyRecords(
                    requestData.userId,
                    requestData.surveyType,
                    requestData.startTime,
                    requestData.endTime
                )

                Mockito.verify(appRepository, Mockito.times(1)).getSurveyRecords(
                    requestData.userId,
                    requestData.surveyType,
                    requestData.startTime,
                    requestData.endTime
                )

                assertNotNull(resultData)
                assertEquals(testItemNumber, resultData.size)
                //Asserting first and last list element for correctness of survey type
                assertEquals(surveyType, resultData[0].surveyType)
                assertEquals(surveyType, resultData[resultData.size - 1].surveyType)
            }
        }
    }

    @Test
    fun `given an invalid input and survey type return empty list survey records`() {
        runBlocking {

            for (surveyType in surveyTypes) {
                Mockito.`when`(
                    appRepository.getSurveyRecords(
                        anyLong(), anyString(), anyLong(),
                        anyLong()
                    )
                ).thenReturn(null)
                val requestData = GetSurveyRecordsRequest(
                    -1,
                    surveyType,
                    -1,
                    -1
                )
                val resultData = getSurveyRecordsUseCaseImpl.getSurveyRecords(
                    requestData.userId,
                    requestData.surveyType,
                    requestData.startTime,
                    requestData.endTime
                )

                Mockito.verify(appRepository, Mockito.times(1)).getSurveyRecords(
                    requestData.userId,
                    requestData.surveyType,
                    requestData.startTime,
                    requestData.endTime
                )

                assertNotNull(resultData)
                assertEquals(0, resultData.size)
            }
        }
    }
}

/**
 * Test request survey records data holder.
 */
internal data class GetSurveyRecordsRequest(
    val userId: Long,
    val surveyType: String,
    val startTime: Long,
    val endTime: Long
)

/**
 * Obtain test data for a given survey type.
 */
internal fun getTestSurveyResultData(items: Int = 1, surveyType: String): List<SurveyDataModel> {

    val testData = mutableListOf<SurveyDataModel>()
    for (i in 0 until items) {
        testData.add(
            SurveyDataModel(
                id = i,
                userExtraId = 1L,
                scoringResult = 10,
                surveyType = surveyType,
                identifier = "",
                createdTime = Instant.now().toEpochMilli(),
                endTime = Instant.now().toEpochMilli(),
                assessmentData = "",
                additionalInfo = "",
                syncedWithServer = false
            )
        )
    }

    return testData
}