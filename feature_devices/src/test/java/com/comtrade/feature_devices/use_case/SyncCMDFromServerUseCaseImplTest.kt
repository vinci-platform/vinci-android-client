/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: SyncCMDFromServerUseCaseImplTest.kt
 * Author: sdelic
 *
 * History:  13.5.22. 16:45 created
 */

package com.comtrade.feature_devices.use_case

import com.comtrade.domain.contracts.repository.IAppRepository
import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.contracts.repository.user.UserServiceRepository
import com.comtrade.domain.contracts.usecases.device.ISyncCMDDeviceFromServerUseCase
import com.comtrade.domain.entities.devices.CmdDevice
import com.comtrade.domain.entities.devices.data.CmdWatchEntity
import com.comtrade.domain.entities.devices.data.GetCmdWatchDataEntity
import com.comtrade.domain.entities.devices.data.GetCmdWatchDataRequest
import com.comtrade.domain.entities.user.data.GetUserDevicesEntity
import kotlinx.coroutines.runBlocking
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mockito
import java.time.Instant

/**
 * Test scenario for the CMD device synchronization from the server use case implementation detail.
 */
internal class SyncCMDFromServerUseCaseImplTest {
    private val testDeviceId = 1L
    private val testMaxTimeStamp = 1_000L
    private val testToken = "testToken"
    private val numberOfTestEntities = 1_000L
    private lateinit var prefs: IPrefs
    private lateinit var userServiceRepository: UserServiceRepository
    private lateinit var appRepository: IAppRepository
    private lateinit var syncCMDDeviceFromServerUseCase: ISyncCMDDeviceFromServerUseCase

    @Before
    fun setUp() {
        prefs = Mockito.mock(IPrefs::class.java)
        userServiceRepository = Mockito.mock(UserServiceRepository::class.java)
        appRepository = Mockito.mock(IAppRepository::class.java)
        syncCMDDeviceFromServerUseCase =
            SyncCMDFromServerUseCaseImpl(prefs, userServiceRepository, appRepository)
    }

    @Test
    fun `given valid input data when called return true`() {
        runBlocking {
            val testDevice = provideTestCMDDevice()
            val testDeviceData = provideCmdEntityTestResponse()
            Mockito.`when`(prefs.cmdDevice).thenReturn(testDevice)
            Mockito.`when`(prefs.token).thenReturn(testToken)
            Mockito.`when`(appRepository.getMaxTimestamp(testDevice.deviceId))
                .thenReturn(testMaxTimeStamp)
            Mockito.`when`(
                userServiceRepository.getCmdWatchData(
                    anyString(), any(
                        GetCmdWatchDataRequest::class.java
                    )
                )
            )
                .thenReturn(testDeviceData)

            Mockito.`when`(appRepository.insertCMDEntity(any(CmdWatchEntity::class.java)))
                .thenReturn(1L)

            val result = syncCMDDeviceFromServerUseCase.syncCmdFromServer()

            Mockito.verify(prefs, Mockito.times(1)).cmdDevice
            Mockito.verify(prefs, Mockito.times(1)).token
            Mockito.verify(appRepository, Mockito.times(1)).getMaxTimestamp(testDevice.deviceId)
            Mockito.verify(userServiceRepository, Mockito.times(1)).getCmdWatchData(
                anyString(), any(
                    GetCmdWatchDataRequest::class.java
                )
            )

            Mockito.verify(appRepository, Mockito.times(numberOfTestEntities.toInt()))
                .insertCMDEntity(
                    any(CmdWatchEntity::class.java)
                )
            assertNotNull(result)
            assertTrue(result)
        }
    }

    @Test
    fun `given valid input data when called return false for db insertion`() {
        runBlocking {
            val testDevice = provideTestCMDDevice()
            val testDeviceData = provideCmdEntityTestResponse()
            Mockito.`when`(prefs.cmdDevice).thenReturn(testDevice)
            Mockito.`when`(prefs.token).thenReturn(testToken)
            Mockito.`when`(appRepository.getMaxTimestamp(testDevice.deviceId))
                .thenReturn(testMaxTimeStamp)
            Mockito.`when`(
                userServiceRepository.getCmdWatchData(
                    anyString(), any(
                        GetCmdWatchDataRequest::class.java
                    )
                )
            )
                .thenReturn(testDeviceData)

            Mockito.`when`(appRepository.insertCMDEntity(any(CmdWatchEntity::class.java)))
                .thenReturn(0)

            val result = syncCMDDeviceFromServerUseCase.syncCmdFromServer()

            Mockito.verify(prefs, Mockito.times(1)).cmdDevice
            Mockito.verify(prefs, Mockito.times(1)).token
            Mockito.verify(appRepository, Mockito.times(1)).getMaxTimestamp(testDevice.deviceId)
            Mockito.verify(userServiceRepository, Mockito.times(1)).getCmdWatchData(
                anyString(), any(
                    GetCmdWatchDataRequest::class.java
                )
            )

            Mockito.verify(appRepository, Mockito.times(1))
                .insertCMDEntity(
                    any(CmdWatchEntity::class.java)
                )
            assertNotNull(result)
            assertFalse(result)
        }
    }

    @Test
    fun `given valid input data when called return false for no test data`() {
        runBlocking {
            val testDevice = provideTestCMDDevice()
            Mockito.`when`(prefs.cmdDevice).thenReturn(testDevice)
            Mockito.`when`(prefs.token).thenReturn(testToken)
            Mockito.`when`(appRepository.getMaxTimestamp(testDevice.deviceId))
                .thenReturn(testMaxTimeStamp)
            Mockito.`when`(
                userServiceRepository.getCmdWatchData(
                    anyString(), any(
                        GetCmdWatchDataRequest::class.java
                    )
                )
            )
                .thenReturn(null)

            Mockito.`when`(appRepository.insertCMDEntity(any(CmdWatchEntity::class.java)))
                .thenReturn(1L)

            val result = syncCMDDeviceFromServerUseCase.syncCmdFromServer()

            Mockito.verify(prefs, Mockito.times(1)).cmdDevice
            Mockito.verify(prefs, Mockito.times(1)).token
            Mockito.verify(appRepository, Mockito.times(1)).getMaxTimestamp(testDevice.deviceId)
            Mockito.verify(userServiceRepository, Mockito.times(1)).getCmdWatchData(
                anyString(), any(
                    GetCmdWatchDataRequest::class.java
                )
            )

            Mockito.verify(appRepository, Mockito.times(0))
                .insertCMDEntity(
                    any(CmdWatchEntity::class.java)
                )
            assertNotNull(result)
            assertFalse(result)
        }
    }

    @Test(expected = Exception::class)
    fun `given an invalid device input when called thrown exception`() {
        runBlocking {
            val testDevice = provideTestCMDDevice()
            Mockito.`when`(prefs.cmdDevice).thenReturn(null)
            Mockito.`when`(prefs.token).thenReturn(testToken)
            Mockito.`when`(appRepository.getMaxTimestamp(testDevice.deviceId))
                .thenReturn(testMaxTimeStamp)
            Mockito.`when`(
                userServiceRepository.getCmdWatchData(
                    anyString(), any(
                        GetCmdWatchDataRequest::class.java
                    )
                )
            )
                .thenReturn(null)

            Mockito.`when`(appRepository.insertCMDEntity(any(CmdWatchEntity::class.java)))
                .thenReturn(1L)

            val result = syncCMDDeviceFromServerUseCase.syncCmdFromServer()

            Mockito.verify(prefs, Mockito.times(1)).cmdDevice
            Mockito.verify(prefs, Mockito.times(0)).token
            Mockito.verify(appRepository, Mockito.times(0)).getMaxTimestamp(testDevice.deviceId)
            Mockito.verify(userServiceRepository, Mockito.times(0)).getCmdWatchData(
                anyString(), any(
                    GetCmdWatchDataRequest::class.java
                )
            )

            Mockito.verify(appRepository, Mockito.times(0))
                .insertCMDEntity(
                    any(CmdWatchEntity::class.java)
                )
            assertNotNull(result)
            assertFalse(result)
        }
    }

    @Test(expected = Exception::class)
    fun `given an invalid device Id input when called thrown exception`() {
        runBlocking {
            val testDevice = provideTestCMDDevice()
            testDevice.deviceId = 0
            Mockito.`when`(prefs.cmdDevice).thenReturn(testDevice)
            Mockito.`when`(prefs.token).thenReturn(testToken)
            Mockito.`when`(appRepository.getMaxTimestamp(testDevice.deviceId))
                .thenReturn(testMaxTimeStamp)
            Mockito.`when`(
                userServiceRepository.getCmdWatchData(
                    anyString(), any(
                        GetCmdWatchDataRequest::class.java
                    )
                )
            )
                .thenReturn(null)

            Mockito.`when`(appRepository.insertCMDEntity(any(CmdWatchEntity::class.java)))
                .thenReturn(1L)

            val result = syncCMDDeviceFromServerUseCase.syncCmdFromServer()

            Mockito.verify(prefs, Mockito.times(1)).cmdDevice
            Mockito.verify(prefs, Mockito.times(0)).token
            Mockito.verify(appRepository, Mockito.times(0)).getMaxTimestamp(testDevice.deviceId)
            Mockito.verify(userServiceRepository, Mockito.times(0)).getCmdWatchData(
                anyString(), any(
                    GetCmdWatchDataRequest::class.java
                )
            )

            Mockito.verify(appRepository, Mockito.times(0))
                .insertCMDEntity(
                    any(CmdWatchEntity::class.java)
                )
            assertNotNull(result)
            assertFalse(result)
        }
    }

    @Test(expected = Exception::class)
    fun `given an invalid token input when called thrown exception`() {
        runBlocking {
            val testDevice = provideTestCMDDevice()
            Mockito.`when`(prefs.cmdDevice).thenReturn(testDevice)
            Mockito.`when`(prefs.token).thenReturn(null)
            Mockito.`when`(appRepository.getMaxTimestamp(testDevice.deviceId))
                .thenReturn(testMaxTimeStamp)
            Mockito.`when`(
                userServiceRepository.getCmdWatchData(
                    anyString(), any(
                        GetCmdWatchDataRequest::class.java
                    )
                )
            )
                .thenReturn(null)

            Mockito.`when`(appRepository.insertCMDEntity(any(CmdWatchEntity::class.java)))
                .thenReturn(1L)

            val result = syncCMDDeviceFromServerUseCase.syncCmdFromServer()

            Mockito.verify(prefs, Mockito.times(1)).cmdDevice
            Mockito.verify(prefs, Mockito.times(0)).token
            Mockito.verify(appRepository, Mockito.times(0)).getMaxTimestamp(testDevice.deviceId)
            Mockito.verify(userServiceRepository, Mockito.times(0)).getCmdWatchData(
                anyString(), any(
                    GetCmdWatchDataRequest::class.java
                )
            )

            Mockito.verify(appRepository, Mockito.times(0))
                .insertCMDEntity(
                    any(CmdWatchEntity::class.java)
                )
            assertNotNull(result)
            assertFalse(result)
        }
    }


    private fun provideTestCMDDevice(): CmdDevice = CmdDevice(
        GetUserDevicesEntity(
            deviceId = testDeviceId,
            name = "testCMDDevice",
            description = "test CMD device description",
            uuid = "some random uuid",
            deviceType = "WRIST WATCH",
            active = true
        )
    )

    private fun provideCmdEntityTestResponse(): GetCmdWatchDataEntity {
        val testData = mutableListOf<CmdWatchEntity>()
        for (i in 0 until numberOfTestEntities) {
            testData.add(
                CmdWatchEntity(
                    id = i,
                    coordinates = null,
                    steps = "10000",
                    timestamp = Instant.now().toEpochMilli(),
                    deviceId = testDeviceId
                )
            )
        }
        return GetCmdWatchDataEntity(h02 = testData)
    }

    /**
     * Kotlin null type handling for Mockito.
     */
    private fun <T> any(type: Class<T>): T = Mockito.any(type)
}