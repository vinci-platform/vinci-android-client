/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: UpdateShoeDataUseCaseImplTest.kt
 * Author: sdelic
 *
 * History:  11.7.22. 11:19 created
 */

package com.comtrade.feature_devices.use_case

import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.contracts.repository.user.UserServiceRepository
import com.comtrade.domain.contracts.usecases.device.IUpdateShoeDataUseCase
import com.comtrade.domain.entities.devices.data.UpdateShoeDataRequest
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito

/**
 * Test scenario for the update shoe device data use case implementation detail.
 */
internal class UpdateShoeDataUseCaseImplTest {
    private val testToken = "testToken"

    private lateinit var prefs: IPrefs
    private lateinit var userServiceRepository: UserServiceRepository
    private lateinit var updateShoeDataUseCase: IUpdateShoeDataUseCase

    @Before
    fun setUp() {
        prefs = Mockito.mock(IPrefs::class.java)
        userServiceRepository = Mockito.mock(UserServiceRepository::class.java)
        updateShoeDataUseCase = UpdateShoeDataUseCaseImpl(prefs, userServiceRepository)
    }

    @Test
    fun `given a valid input request when called then return true`() {

        runBlocking {

            val updateDataRequest = provideTestRequestData()
            Mockito.`when`(prefs.token).thenReturn(testToken)
            Mockito.`when`(userServiceRepository.updateShoeData(testToken, updateDataRequest))
                .thenReturn(true)

            //Execute api call
            val result = updateShoeDataUseCase.updateShoeData(updateDataRequest)

            Mockito.verify(prefs, Mockito.times(1))
                .token

            Mockito.verify(userServiceRepository, Mockito.times(1))
                .updateShoeData(testToken, updateDataRequest)

            assertNotNull(result)
            assertTrue(result)
        }

    }


    @Test(expected = Exception::class)
    fun `given an invalid token input request when called throw exception`() {

        runBlocking {

            val updateDataRequest = provideTestRequestData()
            Mockito.`when`(prefs.token).thenReturn(null)
            Mockito.`when`(userServiceRepository.updateShoeData(testToken, updateDataRequest))
                .thenReturn(true)

            //Execute api call
            val result = updateShoeDataUseCase.updateShoeData(updateDataRequest)

            Mockito.verify(prefs, Mockito.times(1))
                .token

            Mockito.verify(userServiceRepository, Mockito.times(0))
                .updateShoeData(testToken, updateDataRequest)

            assertNotNull(result)
            assertTrue(result)
        }

    }

    @Test(expected = Exception::class)
    fun `given an invalid body request input request when called throw exception`() {

        runBlocking {

            val updateDataRequest = provideTestRequestData()
            updateDataRequest.deviceId = -1

            Mockito.`when`(prefs.token).thenReturn(testToken)
            Mockito.`when`(userServiceRepository.updateShoeData(testToken, updateDataRequest))
                .thenReturn(true)

            //Execute api call
            val result = updateShoeDataUseCase.updateShoeData(updateDataRequest)

            Mockito.verify(prefs, Mockito.times(1))
                .token

            Mockito.verify(userServiceRepository, Mockito.times(0))
                .updateShoeData(testToken, updateDataRequest)

            assertNotNull(result)
            assertTrue(result)
        }

    }

    private fun provideTestRequestData(): UpdateShoeDataRequest {
        return UpdateShoeDataRequest(
            1,
            1,
            "test data",
            System.currentTimeMillis()
        )
    }
}