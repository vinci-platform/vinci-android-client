/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: AddDeviceUseCaseImplTest.kt
 * Author: sdelic
 *
 * History:  8.7.22. 11:04 created
 */

package com.comtrade.feature_devices.use_case

import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.contracts.repository.user.UserServiceRepository
import com.comtrade.domain.contracts.usecases.device.IAddDeviceUseCase
import com.comtrade.domain.entities.devices.data.AddDeviceRequest
import com.comtrade.domain.entities.devices.data.AddDeviceResponse
import com.comtrade.domain.settings.SupportedDeviceTypes
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertNull
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito

/**
 * Test suite for the adding of a new device use case implementation detail.
 */
internal class AddDeviceUseCaseImplTest {
    private val testToken = "testToken"
    private val testDeviceId = 1L
    private val testDeviceName = "testDevice"
    private val testDeviceUUID = "some random uuid"

    private lateinit var prefs: IPrefs
    private lateinit var userServiceRepository: UserServiceRepository
    private lateinit var addDeviceUseCase: IAddDeviceUseCase

    @Before
    fun setUp() {
        prefs = Mockito.mock(IPrefs::class.java)
        userServiceRepository = Mockito.mock(UserServiceRepository::class.java)
        addDeviceUseCase = AddDeviceUseCaseImpl(prefs, userServiceRepository)
    }

    @Test
    fun `given valid request data when called return response data`() = runBlocking {
        val testDevice = provideTestDevice()
        val testDeviceResponse = provideTestDeviceResponse()
        Mockito.`when`(prefs.token).thenReturn(testToken)
        Mockito.`when`(userServiceRepository.addDevice(testToken, testDevice))
            .thenReturn(testDeviceResponse)

        //Make call
        val result = addDeviceUseCase.addDevice(testDevice)

        Mockito.verify(prefs, Mockito.times(1)).token
        Mockito.verify(userServiceRepository, Mockito.times(1)).addDevice(testToken, testDevice)

        assertNotNull(result)
    }

    @Test
    fun `given valid request data when called return null`() = runBlocking {
        val testDevice = provideTestDevice()
        Mockito.`when`(prefs.token).thenReturn(testToken)
        Mockito.`when`(userServiceRepository.addDevice(testToken, testDevice))
            .thenReturn(null)

        //Make call
        val result = addDeviceUseCase.addDevice(testDevice)

        Mockito.verify(prefs, Mockito.times(1)).token
        Mockito.verify(userServiceRepository, Mockito.times(1)).addDevice(testToken, testDevice)

        assertNull(result)
    }

    @Test(expected = Exception::class)
    fun `given an invalid token  when called throw exception`() = runBlocking {
        val testDevice = provideTestDevice()
        val testDeviceResponse = provideTestDeviceResponse()
        Mockito.`when`(prefs.token).thenReturn(null)
        Mockito.`when`(userServiceRepository.addDevice(testToken, testDevice))
            .thenReturn(testDeviceResponse)

        //Make call
        val result = addDeviceUseCase.addDevice(testDevice)

        Mockito.verify(prefs, Mockito.times(1)).token
        Mockito.verify(userServiceRepository, Mockito.times(1)).addDevice(testToken, testDevice)

        assertNotNull(result)
    }

    @Test(expected = Exception::class)
    fun `given an invalid request payload when called throw exception`() = runBlocking {
        val testDevice = provideTestDevice().copy(uuid = "")
        val testDeviceResponse = provideTestDeviceResponse()
        Mockito.`when`(prefs.token).thenReturn(testToken)
        Mockito.`when`(userServiceRepository.addDevice(testToken, testDevice))
            .thenReturn(testDeviceResponse)

        //Make call
        val result = addDeviceUseCase.addDevice(testDevice)

        Mockito.verify(prefs, Mockito.times(1)).token
        Mockito.verify(userServiceRepository, Mockito.times(1)).addDevice(testToken, testDevice)

        assertNotNull(result)
    }

    private fun provideTestDevice(): AddDeviceRequest = AddDeviceRequest(
        name = testDeviceName,
        description = "test CMD device description",
        uuid = testDeviceUUID,
        deviceType = SupportedDeviceTypes.WATCH.name,
        active = true
    )

    private fun provideTestDeviceResponse(): AddDeviceResponse = AddDeviceResponse(
        id = testDeviceId,
        name = testDeviceName,
        description = "",
        uuid = testDeviceUUID,
        deviceType = SupportedDeviceTypes.WATCH.name,
        active = true,
        startTimestamp = null,
        userExtraId = 1L,
        userExtraFirstName = null,
        userExtraLastName = null,
        userFamilyId = null,
        userFamilyFirstName = null,
        userFamilyLastName = null,
        userOrganisationId = null,
        userOrganisationFirstName = "",
        userOrganisationLastName = "",
        alerts = emptyList()
    )

}