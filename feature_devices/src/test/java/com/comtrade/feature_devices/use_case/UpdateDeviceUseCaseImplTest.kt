/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: UpdateDeviceUseCaseImplTest.kt
 * Author: sdelic
 *
 * History:  29.6.22. 13:09 created
 */

package com.comtrade.feature_devices.use_case

import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.contracts.repository.user.UserServiceRepository
import com.comtrade.domain.contracts.usecases.device.IUpdateDeviceUseCase
import com.comtrade.domain.entities.devices.ShoeDevice
import com.comtrade.domain.entities.devices.data.UpdateDeviceRequest
import com.comtrade.domain.entities.user.data.GetUserDevicesEntity
import com.comtrade.domain.settings.SupportedDeviceTypes
import kotlinx.coroutines.runBlocking
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import java.util.*

/**
 * Test scenario for the updating the attached device data use case implementation detail.
 */
internal class UpdateDeviceUseCaseImplTest {

    private val testToken = "testToken"
    private val testUserExtraId = 1L
    private val testDeviceId = 1L

    private lateinit var prefs: IPrefs
    private lateinit var userServiceRepository: UserServiceRepository
    private lateinit var updateDeviceUseCase: IUpdateDeviceUseCase

    @Before
    fun setUp() {
        prefs = Mockito.mock(IPrefs::class.java)
        userServiceRepository = Mockito.mock(UserServiceRepository::class.java)
        updateDeviceUseCase = UpdateDeviceUseCaseImpl(prefs, userServiceRepository)
    }

    @Test
    fun `given valid input data when called return true`() = runBlocking {

        val testShoeDevice = provideTestData()

        Mockito.`when`(prefs.token).thenReturn(testToken)
        Mockito.`when`(prefs.userExtraId).thenReturn(testUserExtraId)
        Mockito.`when`(prefs.shoeDevice).thenReturn(testShoeDevice)

        val requestData = UpdateDeviceRequest(
            testShoeDevice.deviceId,
            testShoeDevice.deviceName,
            testShoeDevice.toString(),
            testShoeDevice.deviceUUID,
            SupportedDeviceTypes.SHOE.name,
            true,
            testUserExtraId
        )

        Mockito.`when`(userServiceRepository.updateDevice(testToken, requestData)).thenReturn(true)

        val result = updateDeviceUseCase.updateDevice()

        Mockito.verify(prefs, Mockito.times(1)).token
        Mockito.verify(prefs, Mockito.times(1)).userExtraId
        Mockito.verify(prefs, Mockito.times(1)).shoeDevice
        Mockito.verify(userServiceRepository, Mockito.times(1))
            .updateDevice(testToken, requestData)

        assertNotNull(result)
        assertTrue(result)
    }

    @Test
    fun `given valid input data when called return false for update result`() = runBlocking {

        val testShoeDevice = provideTestData()

        Mockito.`when`(prefs.token).thenReturn(testToken)
        Mockito.`when`(prefs.userExtraId).thenReturn(testUserExtraId)
        Mockito.`when`(prefs.shoeDevice).thenReturn(testShoeDevice)

        val requestData = UpdateDeviceRequest(
            testShoeDevice.deviceId,
            testShoeDevice.deviceName,
            testShoeDevice.toString(),
            testShoeDevice.deviceUUID,
            SupportedDeviceTypes.SHOE.name,
            true,
            testUserExtraId
        )

        Mockito.`when`(userServiceRepository.updateDevice(testToken, requestData)).thenReturn(false)

        val result = updateDeviceUseCase.updateDevice()

        Mockito.verify(prefs, Mockito.times(1)).token
        Mockito.verify(prefs, Mockito.times(1)).userExtraId
        Mockito.verify(prefs, Mockito.times(1)).shoeDevice
        Mockito.verify(userServiceRepository, Mockito.times(1))
            .updateDevice(testToken, requestData)

        assertNotNull(result)
        assertFalse(result)
    }

    @Test(expected = Exception::class)
    fun `given an invalid token input data when called throw exception`() = runBlocking {

        val testShoeDevice = provideTestData()

        Mockito.`when`(prefs.token).thenReturn(null)
        Mockito.`when`(prefs.userExtraId).thenReturn(testUserExtraId)
        Mockito.`when`(prefs.shoeDevice).thenReturn(testShoeDevice)

        val requestData = UpdateDeviceRequest(
            testShoeDevice.deviceId,
            testShoeDevice.deviceName,
            testShoeDevice.toString(),
            testShoeDevice.deviceUUID,
            SupportedDeviceTypes.SHOE.name,
            true,
            testUserExtraId
        )

        Mockito.`when`(userServiceRepository.updateDevice(testToken, requestData)).thenReturn(false)

        val result = updateDeviceUseCase.updateDevice()

        Mockito.verify(prefs, Mockito.times(1)).token
        Mockito.verify(prefs, Mockito.times(1)).userExtraId
        Mockito.verify(prefs, Mockito.times(1)).shoeDevice
        Mockito.verify(userServiceRepository, Mockito.times(1))
            .updateDevice(testToken, requestData)

        assertNotNull(result)
        assertFalse(result)
    }

    @Test(expected = Exception::class)
    fun `given invalid input device data when called throw exception`() = runBlocking {

        val testShoeDevice = provideTestData()

        Mockito.`when`(prefs.token).thenReturn(testToken)
        Mockito.`when`(prefs.userExtraId).thenReturn(testUserExtraId)
        Mockito.`when`(prefs.shoeDevice).thenReturn(null)

        val requestData = UpdateDeviceRequest(
            testShoeDevice.deviceId,
            testShoeDevice.deviceName,
            testShoeDevice.toString(),
            testShoeDevice.deviceUUID,
            SupportedDeviceTypes.SHOE.name,
            true,
            testUserExtraId
        )

        Mockito.`when`(userServiceRepository.updateDevice(testToken, requestData)).thenReturn(false)

        val result = updateDeviceUseCase.updateDevice()

        Mockito.verify(prefs, Mockito.times(1)).token
        Mockito.verify(prefs, Mockito.times(1)).userExtraId
        Mockito.verify(prefs, Mockito.times(1)).shoeDevice
        Mockito.verify(userServiceRepository, Mockito.times(1))
            .updateDevice(testToken, requestData)

        assertNotNull(result)
        assertFalse(result)
    }

    @Test(expected = Exception::class)
    fun `given invalid user extra Id when called throw exception`() = runBlocking {

        val testShoeDevice = provideTestData()

        Mockito.`when`(prefs.token).thenReturn(testToken)
        Mockito.`when`(prefs.userExtraId).thenReturn(-1)
        Mockito.`when`(prefs.shoeDevice).thenReturn(testShoeDevice)

        val requestData = UpdateDeviceRequest(
            testShoeDevice.deviceId,
            testShoeDevice.deviceName,
            testShoeDevice.toString(),
            testShoeDevice.deviceUUID,
            SupportedDeviceTypes.SHOE.name,
            true,
            -1
        )

        Mockito.`when`(userServiceRepository.updateDevice(testToken, requestData)).thenReturn(false)

        val result = updateDeviceUseCase.updateDevice()

        Mockito.verify(prefs, Mockito.times(1)).token
        Mockito.verify(prefs, Mockito.times(1)).userExtraId
        Mockito.verify(prefs, Mockito.times(1)).shoeDevice
        Mockito.verify(userServiceRepository, Mockito.times(1))
            .updateDevice(testToken, requestData)

        assertNotNull(result)
        assertFalse(result)
    }

    private fun provideTestData() = ShoeDevice(
        deviceCode = "Test Code",
        lowThreshold = -1.0,
        highThreshold = -1.0,
        deviceMac = null,
        device = GetUserDevicesEntity(
            deviceId = testDeviceId,
            name = "Shoe Test device",
            description = "",
            uuid = UUID.randomUUID().toString(),
            deviceType = SupportedDeviceTypes.SHOE.name,
            active = true,
        )
    )
}