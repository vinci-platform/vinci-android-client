/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: DisconnectFitBitUseCaseImplTest.kt
 * Author: sdelic
 *
 * History:  27.6.22. 12:52 created
 */

package com.comtrade.feature_devices.use_case.fitbit

import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.contracts.repository.user.UserServiceRepository
import com.comtrade.domain.contracts.usecases.device.IDisconnectFitBitUseCase
import com.comtrade.domain.entities.devices.FitBitDevice
import com.comtrade.domain.entities.devices.data.UpdateDeviceRequest
import com.comtrade.domain.entities.user.account.GetUserExtraEntity
import com.comtrade.domain.entities.user.data.GetUserDevicesEntity
import com.comtrade.domain.settings.SupportedDeviceTypes
import kotlinx.coroutines.runBlocking
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import java.util.*

/**
 *  Test scenario for the disconnecting a FitBit device use case implementation detail
 */
internal class DisconnectFitBitUseCaseImplTest {
    private val testToken = "testToken"
    private val testUserId = 1L
    private val testUserExtraId = 1L
    private val testDeviceId = 1L

    private lateinit var prefs: IPrefs
    private lateinit var userServiceRepository: UserServiceRepository
    private lateinit var disconnectFitBitUseCase: IDisconnectFitBitUseCase

    @Before
    fun setUp() {
        prefs = Mockito.mock(IPrefs::class.java)
        userServiceRepository = Mockito.mock(UserServiceRepository::class.java)
        disconnectFitBitUseCase = DisconnectFitBitUseCaseImpl(prefs, userServiceRepository)
    }

    @Test
    fun `given an attached FitBit device when called return true`() = runBlocking {

        val fitBitTestDevice = provideFitBitTestDevice()
        val testUserExtraResponse = getTestUserResponse()
        Mockito.`when`(prefs.token).thenReturn(testToken)
        Mockito.`when`(prefs.fitBitDevice).thenReturn(fitBitTestDevice)
        Mockito.`when`(prefs.userExtraId).thenReturn(testUserExtraId)
        Mockito.`when`(prefs.userId).thenReturn(testUserId)

        val requestData = UpdateDeviceRequest(
            fitBitTestDevice.deviceId,
            fitBitTestDevice.deviceName,
            fitBitTestDevice.description,
            fitBitTestDevice.deviceUUID,
            SupportedDeviceTypes.FITBIT_WATCH.name,
            false,
            testUserExtraId
        )

        Mockito.`when`(userServiceRepository.updateDevice(testToken, requestData)).thenReturn(true)
        Mockito.`when`(userServiceRepository.getUserExtra(testToken, testUserId))
            .thenReturn(testUserExtraResponse)

        //Make call
        val result = disconnectFitBitUseCase.disconnectDevice()

        Mockito.verify(prefs, Mockito.times(1)).token
        Mockito.verify(prefs, Mockito.times(1)).fitBitDevice
        Mockito.verify(prefs, Mockito.times(1)).userExtraId
        Mockito.verify(prefs, Mockito.times(1)).userId
        Mockito.verify(userServiceRepository, Mockito.times(1)).updateDevice(testToken, requestData)
        Mockito.verify(userServiceRepository, Mockito.times(1)).getUserExtra(testToken, testUserId)

        assertNotNull(result)
        assertTrue(result)
    }


    @Test
    fun `given an attached FitBit device when called return false for update device`() =
        runBlocking {

            val fitBitTestDevice = provideFitBitTestDevice()
            val testUserExtraResponse = getTestUserResponse()
            Mockito.`when`(prefs.token).thenReturn(testToken)
            Mockito.`when`(prefs.fitBitDevice).thenReturn(fitBitTestDevice)
            Mockito.`when`(prefs.userExtraId).thenReturn(testUserExtraId)
            Mockito.`when`(prefs.userId).thenReturn(testUserId)

            val requestData = UpdateDeviceRequest(
                fitBitTestDevice.deviceId,
                fitBitTestDevice.deviceName,
                fitBitTestDevice.description,
                fitBitTestDevice.deviceUUID,
                SupportedDeviceTypes.FITBIT_WATCH.name,
                false,
                testUserExtraId
            )

            Mockito.`when`(userServiceRepository.updateDevice(testToken, requestData))
                .thenReturn(false)
            Mockito.`when`(userServiceRepository.getUserExtra(testToken, testUserId))
                .thenReturn(testUserExtraResponse)

            //Make call
            val result = disconnectFitBitUseCase.disconnectDevice()

            Mockito.verify(prefs, Mockito.times(1)).token
            Mockito.verify(prefs, Mockito.times(1)).fitBitDevice
            Mockito.verify(prefs, Mockito.times(1)).userExtraId
            Mockito.verify(prefs, Mockito.times(0)).userId
            Mockito.verify(userServiceRepository, Mockito.times(1))
                .updateDevice(testToken, requestData)
            Mockito.verify(userServiceRepository, Mockito.times(0))
                .getUserExtra(testToken, testUserId)

            assertNotNull(result)
            assertFalse(result)
        }

    @Test
    fun `given an attached FitBit device when called return false for obtaining user extra data`() =
        runBlocking {

            val fitBitTestDevice = provideFitBitTestDevice()
            Mockito.`when`(prefs.token).thenReturn(testToken)
            Mockito.`when`(prefs.fitBitDevice).thenReturn(fitBitTestDevice)
            Mockito.`when`(prefs.userExtraId).thenReturn(testUserExtraId)
            Mockito.`when`(prefs.userId).thenReturn(testUserId)

            val requestData = UpdateDeviceRequest(
                fitBitTestDevice.deviceId,
                fitBitTestDevice.deviceName,
                fitBitTestDevice.description,
                fitBitTestDevice.deviceUUID,
                SupportedDeviceTypes.FITBIT_WATCH.name,
                false,
                testUserExtraId
            )

            Mockito.`when`(userServiceRepository.updateDevice(testToken, requestData))
                .thenReturn(true)
            Mockito.`when`(userServiceRepository.getUserExtra(testToken, testUserId))
                .thenReturn(null)

            //Make call
            val result = disconnectFitBitUseCase.disconnectDevice()

            Mockito.verify(prefs, Mockito.times(1)).token
            Mockito.verify(prefs, Mockito.times(1)).fitBitDevice
            Mockito.verify(prefs, Mockito.times(1)).userExtraId
            Mockito.verify(prefs, Mockito.times(1)).userId
            Mockito.verify(userServiceRepository, Mockito.times(1))
                .updateDevice(testToken, requestData)
            Mockito.verify(userServiceRepository, Mockito.times(1))
                .getUserExtra(testToken, testUserId)

            assertNotNull(result)
            assertFalse(result)
        }

    @Test(expected = Exception::class)
    fun `given an invalid token when called throw exception`() = runBlocking {

        val fitBitTestDevice = provideFitBitTestDevice()
        val testUserExtraResponse = getTestUserResponse()
        Mockito.`when`(prefs.token).thenReturn(null)
        Mockito.`when`(prefs.fitBitDevice).thenReturn(fitBitTestDevice)
        Mockito.`when`(prefs.userExtraId).thenReturn(testUserExtraId)
        Mockito.`when`(prefs.userId).thenReturn(testUserId)

        val requestData = UpdateDeviceRequest(
            fitBitTestDevice.deviceId,
            fitBitTestDevice.deviceName,
            fitBitTestDevice.description,
            fitBitTestDevice.deviceUUID,
            SupportedDeviceTypes.FITBIT_WATCH.name,
            false,
            testUserExtraId
        )

        Mockito.`when`(userServiceRepository.updateDevice(testToken, requestData)).thenReturn(true)
        Mockito.`when`(userServiceRepository.getUserExtra(testToken, testUserId))
            .thenReturn(testUserExtraResponse)

        //Make call
        val result = disconnectFitBitUseCase.disconnectDevice()

        Mockito.verify(prefs, Mockito.times(1)).token
        Mockito.verify(prefs, Mockito.times(0)).fitBitDevice
        Mockito.verify(prefs, Mockito.times(0)).userExtraId
        Mockito.verify(prefs, Mockito.times(0)).userId
        Mockito.verify(userServiceRepository, Mockito.times(0)).updateDevice(testToken, requestData)
        Mockito.verify(userServiceRepository, Mockito.times(0)).getUserExtra(testToken, testUserId)

        assertNotNull(result)
        assertTrue(result)
    }

    @Test(expected = Exception::class)
    fun `given no FitBit device attached when called throw exception`() = runBlocking {

        val testUserExtraResponse = getTestUserResponse()
        Mockito.`when`(prefs.token).thenReturn(testToken)
        Mockito.`when`(prefs.fitBitDevice).thenReturn(null)
        Mockito.`when`(prefs.userExtraId).thenReturn(testUserExtraId)
        Mockito.`when`(prefs.userId).thenReturn(testUserId)

        Mockito.`when`(userServiceRepository.getUserExtra(testToken, testUserId))
            .thenReturn(testUserExtraResponse)

        //Make call
        val result = disconnectFitBitUseCase.disconnectDevice()

        Mockito.verify(prefs, Mockito.times(1)).token
        Mockito.verify(prefs, Mockito.times(1)).fitBitDevice
        Mockito.verify(prefs, Mockito.times(0)).userExtraId
        Mockito.verify(prefs, Mockito.times(0)).userId
        Mockito.verify(userServiceRepository, Mockito.times(0)).getUserExtra(testToken, testUserId)

        assertNotNull(result)
        assertTrue(result)
    }


    private fun provideFitBitTestDevice() = FitBitDevice(
        GetUserDevicesEntity(
            deviceId = testDeviceId,
            name = "FitBit Test device",
            description = "",
            uuid = UUID.randomUUID().toString(),
            deviceType = SupportedDeviceTypes.FITBIT_WATCH.name,
            active = true,
        )
    )

    private fun getTestUserResponse(): GetUserExtraEntity = GetUserExtraEntity(
        userId = testUserId,
        firstName = "test firstname",
        gender = "test gender",
        address = null,
        phone = null,
        surname = "test surname",
        birthDate = "01.01.1970",
        userFirstName = "test firstname",
        userLastName = "test lastname",
        education = null,
        maritalStatus = null,
        organizationIdentifier = null,
        familyIdentifier = null,
        friends = null,
        devices = listOf(),
        images = listOf()
    )
}