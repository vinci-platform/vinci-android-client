/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: GetFitBitUrlUseCaseImplTest.kt
 * Author: sdelic
 *
 * History:  28.6.22. 08:54 created
 */

package com.comtrade.feature_devices.use_case.fitbit

import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.contracts.repository.user.UserServiceRepository
import com.comtrade.domain.contracts.usecases.device.IGetFitBitUrlUseCase
import com.comtrade.domain.entities.devices.fitbit.GetFitBitUrlEntity
import kotlinx.coroutines.runBlocking
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito

/**
 * Test scenario for the obtaining the FitBit connection url use case implementation detail
 */
internal class GetFitBitUrlUseCaseImplTest {
    private val testToken = "testToken"
    private val testUserExtraId = 1L
    private val testResponseUrl = "testResponseUrl"
    private lateinit var prefs: IPrefs
    private lateinit var userServiceRepository: UserServiceRepository
    private lateinit var getFitBitUrlUseCase: IGetFitBitUrlUseCase

    @Before
    fun setUp() {
        prefs = Mockito.mock(IPrefs::class.java)
        userServiceRepository = Mockito.mock(UserServiceRepository::class.java)
        getFitBitUrlUseCase = GetFitBitUrlUseCaseImpl(prefs, userServiceRepository)
    }

    @Test
    fun `given a valid request when called return url entity`() = runBlocking {
        val testResponseData = provideTestFitBitUrlResponse()
        Mockito.`when`(prefs.token).thenReturn(testToken)
        Mockito.`when`(prefs.userExtraId).thenReturn(testUserExtraId)
        Mockito.`when`(userServiceRepository.getFitbitURL(testToken, testUserExtraId))
            .thenReturn(testResponseData)

        val result = getFitBitUrlUseCase.getFitbitURL()

        Mockito.verify(prefs, Mockito.times(1)).token
        Mockito.verify(prefs, Mockito.times(1)).userExtraId
        Mockito.verify(userServiceRepository, Mockito.times(1))
            .getFitbitURL(testToken, testUserExtraId)

        assertNotNull(result)
        assertEquals(testResponseData.url, result.url)
    }

    @Test
    fun `given a valid request when called return null`() = runBlocking {
        Mockito.`when`(prefs.token).thenReturn(testToken)
        Mockito.`when`(prefs.userExtraId).thenReturn(testUserExtraId)
        Mockito.`when`(userServiceRepository.getFitbitURL(testToken, testUserExtraId))
            .thenReturn(null)

        val result = getFitBitUrlUseCase.getFitbitURL()

        Mockito.verify(prefs, Mockito.times(1)).token
        Mockito.verify(prefs, Mockito.times(1)).userExtraId
        Mockito.verify(userServiceRepository, Mockito.times(1))
            .getFitbitURL(testToken, testUserExtraId)

        assertNull(result)
    }

    @Test(expected = Exception::class)
    fun `given an invalid token request when called throw exception`() = runBlocking {
        val testResponseData = provideTestFitBitUrlResponse()
        Mockito.`when`(prefs.token).thenReturn(null)
        Mockito.`when`(prefs.userExtraId).thenReturn(testUserExtraId)
        Mockito.`when`(userServiceRepository.getFitbitURL(testToken, testUserExtraId))
            .thenReturn(testResponseData)

        val result = getFitBitUrlUseCase.getFitbitURL()

        Mockito.verify(prefs, Mockito.times(1)).token
        Mockito.verify(prefs, Mockito.times(0)).userExtraId
        Mockito.verify(userServiceRepository, Mockito.times(0))
            .getFitbitURL(testToken, testUserExtraId)

        assertNotNull(result)
        assertEquals(testResponseData.url, result.url)
    }

    @Test(expected = Exception::class)
    fun `given an invalid user extra Id request when called return throw exception`() =
        runBlocking {
            val testResponseData = provideTestFitBitUrlResponse()
            Mockito.`when`(prefs.token).thenReturn(testToken)
            Mockito.`when`(prefs.userExtraId).thenReturn(-1)
            Mockito.`when`(userServiceRepository.getFitbitURL(testToken, testUserExtraId))
                .thenReturn(testResponseData)

            val result = getFitBitUrlUseCase.getFitbitURL()

            Mockito.verify(prefs, Mockito.times(1)).token
            Mockito.verify(prefs, Mockito.times(1)).userExtraId
            Mockito.verify(userServiceRepository, Mockito.times(0))
                .getFitbitURL(testToken, testUserExtraId)

            assertNotNull(result)
            assertEquals(testResponseData.url, result.url)
        }

    private fun provideTestFitBitUrlResponse(): GetFitBitUrlEntity =
        GetFitBitUrlEntity(testResponseUrl)
}