/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: GetFitBitDailyDataUseCaseImplTest.kt
 * Author: sdelic
 *
 * History:  26.5.22. 16:33 created
 */

package com.comtrade.feature_devices.use_case.fitbit

import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.contracts.repository.user.IDeviceRepository
import com.comtrade.domain.contracts.usecases.device.IGetFitBitDailyDataUseCase
import com.comtrade.domain.entities.devices.fitbit.FitBitDailyDataEntity
import com.comtrade.domain.entities.devices.fitbit.FitBitDailyPayloadEntity
import com.comtrade.domain.entities.devices.fitbit.FitBitDeviceEntity
import com.comtrade.domain.settings.SupportedDeviceTypes
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import java.time.Instant
import java.util.*

/**
 * Test scenario for obtaining the FitBit daily User data use case implementation detail.
 */
internal class GetFitBitDailyDataUseCaseImplTest {
    private val testDeviceId = 1L
    private val testTimeInterval = 1L
    private val testToken = "testToken"
    private val testDailySampleSize = 10_000

    private lateinit var prefs: IPrefs
    private lateinit var deviceRepository: IDeviceRepository
    private lateinit var getFitBitDailyDataUseCase: IGetFitBitDailyDataUseCase

    @Before
    fun setUp() {
        prefs = Mockito.mock(IPrefs::class.java)
        deviceRepository = Mockito.mock(IDeviceRepository::class.java)
        getFitBitDailyDataUseCase = GetFitBitDailyDataUseCaseImpl(prefs, deviceRepository)
    }

    @Test
    fun `given a valid input data when invoked return list of daily FitBit data`() {
        runBlocking {
            val testData = provideTestData()
            Mockito.`when`(prefs.token).thenReturn(testToken)
            Mockito.`when`(
                deviceRepository.getFitbitWatchDailyData(
                    testToken,
                    testDeviceId,
                    testTimeInterval
                )
            ).thenReturn(testData)

            val result = getFitBitDailyDataUseCase.getFitBitDailyDataForInterval(
                testDeviceId,
                testTimeInterval
            )

            Mockito.verify(prefs, Mockito.times(1)).token
            Mockito.verify(deviceRepository, Mockito.times(1)).getFitbitWatchDailyData(
                testToken,
                testDeviceId,
                testTimeInterval
            )

            assertNotNull(result)
            assertEquals(testDailySampleSize, result.size)
        }
    }

    @Test(expected = Exception::class)
    fun `given an invalid token input data when invoked throw exception`() {
        runBlocking {
            val testData = provideTestData()
            Mockito.`when`(prefs.token).thenReturn(null)
            Mockito.`when`(
                deviceRepository.getFitbitWatchDailyData(
                    "",
                    testDeviceId,
                    testTimeInterval
                )
            ).thenReturn(testData)

            val result = getFitBitDailyDataUseCase.getFitBitDailyDataForInterval(
                testDeviceId,
                testTimeInterval
            )

            Mockito.verify(prefs, Mockito.times(1)).token
            Mockito.verify(deviceRepository, Mockito.times(0)).getFitbitWatchDailyData(
                testToken,
                testDeviceId,
                testTimeInterval
            )

            assertNotNull(result)
            assertEquals(testDailySampleSize, result.size)
        }
    }

    @Test(expected = Exception::class)
    fun `given an invalid deviceId input data when invoked throw exception`() {
        runBlocking {
            val testData = provideTestData()
            Mockito.`when`(prefs.token).thenReturn(testToken)
            Mockito.`when`(
                deviceRepository.getFitbitWatchDailyData(
                    testToken,
                    -1,
                    testTimeInterval
                )
            ).thenReturn(testData)

            val result = getFitBitDailyDataUseCase.getFitBitDailyDataForInterval(
                -1,
                testTimeInterval
            )

            Mockito.verify(prefs, Mockito.times(1)).token
            Mockito.verify(deviceRepository, Mockito.times(0)).getFitbitWatchDailyData(
                testToken,
                -1,
                testTimeInterval
            )

            assertNotNull(result)
            assertEquals(testDailySampleSize, result.size)
        }
    }

    @Test(expected = Exception::class)
    fun `given an invalid timestamp input data when invoked throw exception`() {
        runBlocking {
            val testData = provideTestData()
            Mockito.`when`(prefs.token).thenReturn(testToken)
            Mockito.`when`(
                deviceRepository.getFitbitWatchDailyData(
                    testToken,
                    testDeviceId,
                    -1
                )
            ).thenReturn(testData)

            val result = getFitBitDailyDataUseCase.getFitBitDailyDataForInterval(
                testDeviceId,
                -1
            )

            Mockito.verify(prefs, Mockito.times(1)).token
            Mockito.verify(deviceRepository, Mockito.times(0)).getFitbitWatchDailyData(
                testToken,
                testDeviceId,
                -1
            )

            assertNotNull(result)
            assertEquals(testDailySampleSize, result.size)
        }
    }

    private fun provideTestData(): List<FitBitDailyPayloadEntity> {
        val testData = mutableListOf<FitBitDailyPayloadEntity>()

        for (i in 0 until testDailySampleSize) {
            testData.add(
                FitBitDailyPayloadEntity(
                    id = i.toLong(),
                    timestamp = Instant.now().toEpochMilli(),
                    device = FitBitDeviceEntity(
                        id = testDeviceId,
                        name = "FitBit Test device",
                        description = "",
                        uuid = UUID.randomUUID().toString(),
                        deviceType = SupportedDeviceTypes.FITBIT_WATCH.name,
                        active = true,
                        startTimestamp = Instant.now().toEpochMilli().toString(),
                        userExtraId = 1L
                    ),
                    data = provideFitBitDayData()
                )
            )
        }
        return testData
    }

    private fun provideFitBitDayData(): List<FitBitDailyDataEntity> {
        val testData = mutableListOf<FitBitDailyDataEntity>()

        for (i in 0 until 100) {
            testData.add(
                FitBitDailyDataEntity(
                    time = "16:46",
                    heart = 56,
                    steps = 100,
                    calories = 100,
                    floors = 0,
                    elevation = 0,
                    distance = 10,
                    timestamp = Instant.now().toEpochMilli()
                )
            )
        }
        return testData
    }
}