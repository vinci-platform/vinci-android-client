/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: GetAllDevicesUseCaseImplTest.kt
 * Author: sdelic
 *
 * History:  8.7.22. 13:22 created
 */

package com.comtrade.feature_devices.use_case

import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.contracts.repository.user.UserServiceRepository
import com.comtrade.domain.contracts.usecases.device.IGetAllDevicesUseCase
import com.comtrade.domain.entities.user.data.GetUserDevicesEntity
import com.comtrade.domain.settings.SupportedDeviceTypes
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito

/**
 * Test scenario for the obtaining of all User devices use case implementation detail.
 */
internal class GetAllDevicesUseCaseImplTest {
    private val testToken = "testToken"
    private val testUserLogin = "testUserLogin"
    private val testResponseDataSize =
        1_000L // Stress testing value, no single User should approach this number in the real world scenario.
    private lateinit var prefs: IPrefs
    private lateinit var userServiceRepository: UserServiceRepository
    private lateinit var getAllDevicesUseCase: IGetAllDevicesUseCase

    @Before
    fun setUp() {
        prefs = Mockito.mock(IPrefs::class.java)
        userServiceRepository = Mockito.mock(UserServiceRepository::class.java)
        getAllDevicesUseCase = GetAllDevicesUseCaseImpl(prefs, userServiceRepository)
    }

    @Test
    fun `given a valid input when called return list of devices`() = runBlocking {
        val testResponseData = provideTestResponseData()
        Mockito.`when`(prefs.token).thenReturn(testToken)
        Mockito.`when`(prefs.userLogin).thenReturn(testUserLogin)
        Mockito.`when`(userServiceRepository.getUserDevices(testToken, testUserLogin))
            .thenReturn(testResponseData)

        //Execute call
        val result = getAllDevicesUseCase.getUserDevices()

        //Verify
        Mockito.verify(prefs, Mockito.times(1)).token
        Mockito.verify(prefs, Mockito.times(1)).userLogin

        assertNotNull(result)
        assertEquals(testResponseDataSize.toInt(), result.size)
    }

    @Test
    fun `given a valid input when called return empty list of devices`() = runBlocking {
        Mockito.`when`(prefs.token).thenReturn(testToken)
        Mockito.`when`(prefs.userLogin).thenReturn(testUserLogin)
        Mockito.`when`(userServiceRepository.getUserDevices(testToken, testUserLogin))
            .thenReturn(emptyList())

        //Execute call
        val result = getAllDevicesUseCase.getUserDevices()

        //Verify
        Mockito.verify(prefs, Mockito.times(1)).token
        Mockito.verify(prefs, Mockito.times(1)).userLogin

        assertNotNull(result)
        assertEquals(0, result.size)
    }

    @Test(expected = Exception::class)
    fun `given an invalid token input when called throw exception`() = runBlocking {
        val testResponseData = provideTestResponseData()
        Mockito.`when`(prefs.token).thenReturn(null)
        Mockito.`when`(prefs.userLogin).thenReturn(testUserLogin)
        Mockito.`when`(userServiceRepository.getUserDevices(testToken, testUserLogin))
            .thenReturn(testResponseData)

        //Execute call
        getAllDevicesUseCase.getUserDevices()

        //Verify
        Mockito.verify(prefs, Mockito.times(1)).token
        Mockito.verify(prefs, Mockito.times(0)).userLogin

        Unit
    }

    @Test(expected = Exception::class)
    fun `given an invalid user login input when called throw exception`() = runBlocking {
        val testResponseData = provideTestResponseData()
        Mockito.`when`(prefs.token).thenReturn(testToken)
        Mockito.`when`(prefs.userLogin).thenReturn(null)
        Mockito.`when`(userServiceRepository.getUserDevices(testToken, testUserLogin))
            .thenReturn(testResponseData)

        //Execute call
        getAllDevicesUseCase.getUserDevices()

        //Verify
        Mockito.verify(prefs, Mockito.times(1)).token
        Mockito.verify(prefs, Mockito.times(1)).userLogin

        Unit
    }

    private fun provideTestResponseData(): List<GetUserDevicesEntity> {
        val responseData = mutableListOf<GetUserDevicesEntity>()

        for (i in 0 until testResponseDataSize) {
            responseData.add(
                GetUserDevicesEntity(
                    deviceId = i,
                    name = "test device $i",
                    description = "test device description",
                    uuid = "random uuid",
                    deviceType = SupportedDeviceTypes.WATCH.name,
                    active = false
                )
            )
        }
        return responseData
    }
}