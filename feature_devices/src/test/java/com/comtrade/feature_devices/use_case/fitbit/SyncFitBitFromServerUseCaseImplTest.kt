/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: SyncFitBitFromServerUseCaseImplTest.kt
 * Author: sdelic
 *
 * History:  31.5.22. 11:54 created
 */

package com.comtrade.feature_devices.use_case.fitbit

import com.comtrade.domain.contracts.repository.IAppRepository
import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.contracts.repository.user.IDeviceRepository
import com.comtrade.domain.contracts.usecases.device.ISyncFitBitFromServerUseCase
import com.comtrade.domain.entities.devices.FitBitDevice
import com.comtrade.domain.entities.devices.fitbit.*
import com.comtrade.domain.entities.step.StepDailyEntity
import com.comtrade.domain.entities.user.data.GetUserDevicesEntity
import com.comtrade.domain.settings.SupportedDeviceTypes
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.anyLong
import org.mockito.Mockito
import java.time.Instant
import java.util.*

/**
 * Test suite for the synchronizing FitBit from server use case implementation detail.
 */
internal class SyncFitBitFromServerUseCaseImplTest {

    private val testToken = "testToken"
    private val testDeviceId = 1L
    private var testCurrentDayMaxValue: Long = 0L
    private val testTimeInterval = 1_000L
    private val testDataSize = 1_000L

    private lateinit var prefs: IPrefs
    private lateinit var appRepository: IAppRepository
    private lateinit var deviceRepository: IDeviceRepository
    private lateinit var syncFitBitFromServerUseCase: ISyncFitBitFromServerUseCase

    @Before
    fun setUp() {
        testCurrentDayMaxValue = Instant.now().toEpochMilli()

        prefs = Mockito.mock(IPrefs::class.java)
        appRepository = Mockito.mock(IAppRepository::class.java)
        deviceRepository = Mockito.mock(IDeviceRepository::class.java)
        syncFitBitFromServerUseCase =
            SyncFitBitFromServerUseCaseImpl(prefs, appRepository, deviceRepository)
    }

    @Test
    fun `given valid input data when invoked return true`() {
        runBlocking {
            val testDevice = provideFitBitTestDevice()
            val testData = provideTestResponseData()
            val testDailyData = provideTestDailyData()


            Mockito.`when`(prefs.fitBitDevice).thenReturn(testDevice)
            Mockito.`when`(prefs.token).thenReturn(testToken)
            Mockito.`when`(appRepository.getMaxDailyTimestamp(testDeviceId))
                .thenReturn(testCurrentDayMaxValue)
            Mockito.`when`(
                deviceRepository.getFitbitWatchData(
                    testToken,
                    testDeviceId,
                    testCurrentDayMaxValue
                )
            ).thenReturn(testData)

            Mockito.`when`(
                deviceRepository.getFitbitWatchDailyData(
                    testToken,
                    testDeviceId,
                    testCurrentDayMaxValue
                )
            ).thenReturn(testDailyData)

            Mockito.`when`(appRepository.insertDaily(any(StepDailyEntity::class.java)))
                .thenReturn(1L)
            val result = syncFitBitFromServerUseCase.syncFitBitFromServer()

            Mockito.verify(prefs, Mockito.times(1)).fitBitDevice
            Mockito.verify(prefs, Mockito.times(1)).token
            Mockito.verify(appRepository, Mockito.times(1)).getMaxDailyTimestamp(testDeviceId)
            Mockito.verify(deviceRepository, Mockito.times(1)).getFitbitWatchData(
                testToken,
                testDeviceId,
                testCurrentDayMaxValue
            )
            Mockito.verify(deviceRepository, Mockito.times(1)).getFitbitWatchDailyData(
                testToken,
                testDeviceId,
                testCurrentDayMaxValue
            )
            Mockito.verify(appRepository, Mockito.times(testDataSize.toInt())).getDailySteps(
                anyLong(),
                anyLong(),
            )

            Mockito.verify(appRepository, Mockito.times(testDataSize.toInt())).insertDaily(
                any(StepDailyEntity::class.java)
            )
            assertTrue(result)
        }
    }

    @Test
    fun `given valid input data when invoked return false for new data insertion`() {
        runBlocking {
            val testDevice = provideFitBitTestDevice()
            val testData = provideTestResponseData()
            val testDailyData = provideTestDailyData()

            Mockito.`when`(prefs.fitBitDevice).thenReturn(testDevice)
            Mockito.`when`(prefs.token).thenReturn(testToken)
            Mockito.`when`(appRepository.getMaxDailyTimestamp(testDeviceId))
                .thenReturn(testCurrentDayMaxValue)
            Mockito.`when`(
                deviceRepository.getFitbitWatchData(
                    testToken,
                    testDeviceId,
                    testCurrentDayMaxValue
                )
            ).thenReturn(testData)


            Mockito.`when`(
                deviceRepository.getFitbitWatchDailyData(
                    testToken,
                    testDeviceId,
                    testCurrentDayMaxValue
                )
            ).thenReturn(testDailyData)

            Mockito.`when`(appRepository.insertDaily(any(StepDailyEntity::class.java)))
                .thenReturn(-1L)
            val result = syncFitBitFromServerUseCase.syncFitBitFromServer()

            Mockito.verify(prefs, Mockito.times(1)).fitBitDevice
            Mockito.verify(prefs, Mockito.times(1)).token
            Mockito.verify(appRepository, Mockito.times(1)).getMaxDailyTimestamp(testDeviceId)
            Mockito.verify(deviceRepository, Mockito.times(1)).getFitbitWatchData(
                testToken,
                testDeviceId,
                testCurrentDayMaxValue
            )
            Mockito.verify(deviceRepository, Mockito.times(1)).getFitbitWatchDailyData(
                testToken,
                testDeviceId,
                testCurrentDayMaxValue
            )
            Mockito.verify(appRepository, Mockito.times(1)).getDailySteps(
                anyLong(),
                anyLong(),
            )

            Mockito.verify(appRepository, Mockito.times(1)).insertDaily(
                any(StepDailyEntity::class.java)
            )
            assertFalse(result)
        }
    }

    @Test(expected = Exception::class)
    fun `given no FitBit device found when invoked throw exception`() {
        runBlocking {

            Mockito.`when`(prefs.fitBitDevice).thenReturn(null)


            Mockito.`when`(appRepository.insertDaily(any(StepDailyEntity::class.java)))
                .thenReturn(1L)
            val result = syncFitBitFromServerUseCase.syncFitBitFromServer()

            Mockito.verify(prefs, Mockito.times(1)).fitBitDevice

            assertTrue(result)
        }
    }

    @Test(expected = Exception::class)
    fun `given an invalid token input data when invoked throw exception`() {
        runBlocking {
            val testDevice = provideFitBitTestDevice()
            val testData = provideTestResponseData()

            Mockito.`when`(prefs.fitBitDevice).thenReturn(testDevice)
            Mockito.`when`(prefs.token).thenReturn(null)
            Mockito.`when`(appRepository.getMaxDailyTimestamp(testDeviceId))
                .thenReturn(testCurrentDayMaxValue)
            Mockito.`when`(
                deviceRepository.getFitbitWatchData(
                    "",
                    testDeviceId,
                    testCurrentDayMaxValue
                )
            ).thenReturn(testData)


            val result = syncFitBitFromServerUseCase.syncFitBitFromServer()

            Mockito.verify(prefs, Mockito.times(1)).fitBitDevice
            Mockito.verify(prefs, Mockito.times(1)).token

            assertTrue(result)
        }
    }

    @Test
    fun `given valid for existing step daily input data when invoked return true`() {
        runBlocking {
            val testDevice = provideFitBitTestDevice()
            val testData = provideTestResponseData()
            val testDailyData = provideTestDailyData()
            val testStepEntity = provideTestStepDailyEntity()

            Mockito.`when`(prefs.fitBitDevice).thenReturn(testDevice)
            Mockito.`when`(prefs.token).thenReturn(testToken)
            Mockito.`when`(appRepository.getMaxDailyTimestamp(testDeviceId))
                .thenReturn(testCurrentDayMaxValue)
            Mockito.`when`(
                deviceRepository.getFitbitWatchData(
                    testToken,
                    testDeviceId,
                    testCurrentDayMaxValue
                )
            ).thenReturn(testData)

            Mockito.`when`(
                deviceRepository.getFitbitWatchDailyData(
                    testToken,
                    testDeviceId,
                    testCurrentDayMaxValue
                )
            ).thenReturn(testDailyData)

            Mockito.`when`(appRepository.getDailySteps(testDeviceId, testTimeInterval))
                .thenReturn(testStepEntity)
            Mockito.`when`(appRepository.updateDaily(any(StepDailyEntity::class.java)))
                .thenReturn(1)

            //Call
            val result = syncFitBitFromServerUseCase.syncFitBitFromServer()

            Mockito.verify(prefs, Mockito.times(1)).fitBitDevice
            Mockito.verify(prefs, Mockito.times(1)).token
            Mockito.verify(appRepository, Mockito.times(1)).getMaxDailyTimestamp(testDeviceId)
            Mockito.verify(deviceRepository, Mockito.times(1)).getFitbitWatchData(
                testToken,
                testDeviceId,
                testCurrentDayMaxValue
            )
            Mockito.verify(deviceRepository, Mockito.times(1)).getFitbitWatchDailyData(
                testToken,
                testDeviceId,
                testCurrentDayMaxValue
            )
            Mockito.verify(appRepository, Mockito.times(testDataSize.toInt())).getDailySteps(
                anyLong(),
                anyLong(),
            )

            assertTrue(result)
        }
    }

    @Test
    fun `given valid for existing step daily input data when invoked return false for update`() {
        runBlocking {
            val testDevice = provideFitBitTestDevice()
            val testData = provideTestResponseData()
            val testDailyData = provideTestDailyData()
            val testStepEntity = provideTestStepDailyEntity()

            Mockito.`when`(prefs.fitBitDevice).thenReturn(testDevice)
            Mockito.`when`(prefs.token).thenReturn(testToken)
            Mockito.`when`(appRepository.getMaxDailyTimestamp(testDeviceId))
                .thenReturn(testCurrentDayMaxValue)
            Mockito.`when`(
                deviceRepository.getFitbitWatchData(
                    testToken,
                    testDeviceId,
                    testCurrentDayMaxValue
                )
            ).thenReturn(testData)

            Mockito.`when`(
                deviceRepository.getFitbitWatchDailyData(
                    testToken,
                    testDeviceId,
                    testCurrentDayMaxValue
                )
            ).thenReturn(testDailyData)

            Mockito.`when`(appRepository.getDailySteps(testDeviceId, testTimeInterval))
                .thenReturn(testStepEntity)
            Mockito.`when`(appRepository.updateDaily(any(StepDailyEntity::class.java)))
                .thenReturn(-1)

            //Call
            val result = syncFitBitFromServerUseCase.syncFitBitFromServer()

            Mockito.verify(prefs, Mockito.times(1)).fitBitDevice
            Mockito.verify(prefs, Mockito.times(1)).token
            Mockito.verify(appRepository, Mockito.times(1)).getMaxDailyTimestamp(testDeviceId)
            Mockito.verify(deviceRepository, Mockito.times(1)).getFitbitWatchData(
                testToken,
                testDeviceId,
                testCurrentDayMaxValue
            )
            Mockito.verify(deviceRepository, Mockito.times(1)).getFitbitWatchDailyData(
                testToken,
                testDeviceId,
                testCurrentDayMaxValue
            )
            Mockito.verify(appRepository, Mockito.times(1)).getDailySteps(
                anyLong(),
                anyLong(),
            )

            assertFalse(result)
        }
    }

    private fun provideFitBitTestDevice() = FitBitDevice(
        GetUserDevicesEntity(
            deviceId = testDeviceId,
            name = "FitBit Test device",
            description = "",
            uuid = UUID.randomUUID().toString(),
            deviceType = SupportedDeviceTypes.FITBIT_WATCH.name,
            active = true,
        )
    )

    private fun provideTestStepDailyEntity(): StepDailyEntity = StepDailyEntity(
        id = 1,
        deviceId = 1L,
        steps = 10_0000,
        activityCalories = 5_000,
        calories_out = 1_000,
        heartRateMin = 75.0,
        heartRateAvg = 80.0,
        heartRateMax = 85.0,
        restingHeartRate = 70.0,
        minutesAsleep = 360,
        minutesInBed = 360,
        timestamp = Instant.now().toEpochMilli()
    )

    private fun provideTestResponseData(): List<FitBitDataPayloadEntity> {
        val testData = mutableListOf<FitBitDataPayloadEntity>()

        for (i in 0 until testDataSize) {
            testData.add(
                FitBitDataPayloadEntity(
                    id = i,
                    timestamp = testTimeInterval,
                    sleepData = SleepDataEntity(
                        summary = SleepDataSummaryEntity(100, 50)
                    ),
                    activityData = ActivityDataEntity(
                        activityCalories = 1_000,
                        caloriesBMR = 1_000,
                        caloriesOut = 1_000,
                        elevation = 0,
                        fairlyActiveMinutes = 10,
                        floors = 10,
                        lightlyActiveMinutes = 20,
                        marginalCalories = 0,
                        restingHeartRate = 10,
                        sedentaryMinutes = 0,
                        steps = 1_000,
                        veryActiveMinutes = 1,
                        distances = emptyList(),
                        heartRateZones = null
                    ),
                    device = FitBitDeviceEntity(
                        id = testDeviceId,
                        name = "FitBit Test device",
                        description = "",
                        uuid = UUID.randomUUID().toString(),
                        deviceType = SupportedDeviceTypes.FITBIT_WATCH.name,
                        active = true,
                        startTimestamp = Instant.now().toEpochMilli().toString(),
                        userExtraId = 1L
                    )
                )
            )
        }

        return testData
    }

    private fun provideTestDailyData(): List<FitBitDailyPayloadEntity> {
        val testData = mutableListOf<FitBitDailyPayloadEntity>()
        val dailyData = mutableListOf<FitBitDailyDataEntity>()

        //arbitrary intervals.
        for (i in 0 until 100) {
            dailyData.add(
                FitBitDailyDataEntity(
                    time = "Time interval",
                    heart = 65,
                    steps = 1_000,
                    calories = 1_000,
                    floors = 10,
                    elevation = 10,
                    distance = 10,
                    timestamp = Instant.now().toEpochMilli()
                )
            )
        }

        for (i in 0 until 10L) {
            testData.add(
                FitBitDailyPayloadEntity(
                    id = i,
                    timestamp = Instant.now().toEpochMilli(),
                    device = FitBitDeviceEntity(
                        id = testDeviceId,
                        name = "FitBit Test device",
                        description = "",
                        uuid = UUID.randomUUID().toString(),
                        deviceType = SupportedDeviceTypes.FITBIT_WATCH.name,
                        active = true,
                        startTimestamp = "Test timestamp",
                        userExtraId = i
                    ),
                    data = dailyData
                )
            )
        }

        return testData
    }

    /**
     * Kotlin null type handling for Mockito.
     */
    private fun <T> any(type: Class<T>): T = Mockito.any(type)
}
