/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: DeleteUserDeviceUseCaseImplTest.kt
 * Author: sdelic
 *
 * History:  29.4.22. 12:59 created
 */

package com.comtrade.feature_devices.use_case

import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.contracts.repository.user.IDeviceRepository
import com.comtrade.domain.contracts.usecases.device.IDeleteUserDeviceUseCase
import kotlinx.coroutines.runBlocking
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.Mockito

/**
 * Test scenario for the delete User device implementation detail.
 */
internal class DeleteUserDeviceUseCaseImplTest {
    private val testDeviceId = 1L
    private val testToken = "testToken"

    private lateinit var prefs: IPrefs
    private lateinit var deviceRepository: IDeviceRepository
    private lateinit var deleteUserDeviceUseCase: IDeleteUserDeviceUseCase

    @Before
    fun setUp() {
        prefs = Mockito.mock(IPrefs::class.java)
        deviceRepository = Mockito.mock(IDeviceRepository::class.java)
        deleteUserDeviceUseCase = DeleteUserDeviceUseCaseImpl(prefs, deviceRepository)
    }

    @Test
    fun `given valid device Id when delete user device return true`() {
        runBlocking {
            Mockito.`when`(prefs.token).thenReturn(testToken)
            Mockito.`when`(
                deviceRepository.deleteUserDevice(
                    ArgumentMatchers.anyString(),
                    ArgumentMatchers.anyLong()
                )
            )
                .thenReturn(true)
            val result = deleteUserDeviceUseCase.deleteUserDevice(testDeviceId)
            Mockito.verify(prefs, Mockito.times(1)).token
            Mockito.verify(deviceRepository, Mockito.times(1))
                .deleteUserDevice(testToken, testDeviceId)
            assertNotNull(result)
            assertTrue(result)
        }
    }

    @Test
    fun `given valid device Id when delete user device return false`() {
        runBlocking {
            Mockito.`when`(prefs.token).thenReturn(testToken)
            Mockito.`when`(
                deviceRepository.deleteUserDevice(
                    ArgumentMatchers.anyString(),
                    ArgumentMatchers.anyLong()
                )
            )
                .thenReturn(false)
            val result = deleteUserDeviceUseCase.deleteUserDevice(testDeviceId)
            Mockito.verify(prefs, Mockito.times(1)).token
            Mockito.verify(deviceRepository, Mockito.times(1))
                .deleteUserDevice(testToken, testDeviceId)
            assertNotNull(result)
            assertFalse(result)
        }
    }

    @Test(expected = Exception::class)
    fun `given invalid token when delete user device throw exception`() {
        runBlocking {
            Mockito.`when`(prefs.token).thenReturn("")
            Mockito.`when`(
                deviceRepository.deleteUserDevice(
                    ArgumentMatchers.anyString(),
                    ArgumentMatchers.anyLong()
                )
            )
                .thenReturn(true)
            val result = deleteUserDeviceUseCase.deleteUserDevice(testDeviceId)
            Mockito.verify(prefs, Mockito.times(1)).token
            Mockito.verify(deviceRepository, Mockito.times(1))
                .deleteUserDevice("", testDeviceId)
            assertNotNull(result)
            assertTrue(result)
        }
    }

    @Test(expected = Exception::class)
    fun `given invalid deviceId when delete user device throw exception`() {
        runBlocking {
            Mockito.`when`(prefs.token).thenReturn(testToken)
            Mockito.`when`(
                deviceRepository.deleteUserDevice(
                    ArgumentMatchers.anyString(),
                    ArgumentMatchers.anyLong()
                )
            )
                .thenReturn(true)
            val result = deleteUserDeviceUseCase.deleteUserDevice(-1L)
            Mockito.verify(prefs, Mockito.times(1)).token
            Mockito.verify(deviceRepository, Mockito.times(1))
                .deleteUserDevice(testToken, -1L)
            assertNotNull(result)
            assertTrue(result)
        }
    }
}