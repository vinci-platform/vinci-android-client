/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: GetFitBitDataForPeriodUseCaseImplTest.kt
 * Author: sdelic
 *
 * History:  27.5.22. 16:06 created
 */

package com.comtrade.feature_devices.use_case.fitbit

import com.comtrade.domain.contracts.repository.IAppRepository
import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.contracts.usecases.device.IGetFitBitDataForPeriodUseCase
import com.comtrade.domain.entities.devices.FitBitDevice
import com.comtrade.domain.entities.step.FitbitDataType
import com.comtrade.domain.entities.step.StepDailyEntity
import com.comtrade.domain.entities.user.data.GetUserDevicesEntity
import com.comtrade.domain.settings.SupportedDeviceTypes
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.anyLong
import org.mockito.Mockito
import java.util.*

/**
 * Test scenario for the obtaining of the FitBit device data for n days use case implementation detail.
 */
internal class GetFitBitDataForPeriodUseCaseImplTest {
    private val testNumberOfDays = 10
    private val testDeviceId = 1L
    private val testStepDataSize = 10_000
    private val testResultDaySteps = 10_000
    private val fitBitTypes = arrayOf(
        FitbitDataType.STEPS,
        FitbitDataType.ACTIVITY_CALORIES,
        FitbitDataType.CALORIES_OUT,
        FitbitDataType.MINUTES_ASLEEP,
        FitbitDataType.MINUTES_IN_BED,
        FitbitDataType.RESTING_HEART_RATE
    )
    private lateinit var prefs: IPrefs
    private lateinit var appRepository: IAppRepository
    private lateinit var getFitBitDataForPeriodUseCase: IGetFitBitDataForPeriodUseCase

    @Before
    fun setUp() {
        prefs = Mockito.mock(IPrefs::class.java)
        appRepository = Mockito.mock(IAppRepository::class.java)
        getFitBitDataForPeriodUseCase = GetFitBitDataForPeriodUseCaseImpl(prefs, appRepository)
    }

    @Test
    fun `given a valid input data when called return the list of FitBit data`() {
        runBlocking {
            val testDevice = provideFitBitTestDevice()
            val testStepData = provideDailyStepTestData()
            Mockito.`when`(prefs.fitBitDevice).thenReturn(testDevice)
            Mockito.`when`(appRepository.getDailyStepsForPeriod(anyLong(), anyLong()))
                .thenReturn(testStepData)
            for (fitBitDataType in fitBitTypes) {
                val result = getFitBitDataForPeriodUseCase.getFitbitForLastDays(
                    testNumberOfDays,
                    fitBitDataType
                )

                assertNotNull(result)
                assertEquals(testNumberOfDays, result.size)
            }

            Mockito.verify(prefs, Mockito.times(fitBitTypes.size)).fitBitDevice
            Mockito.verify(appRepository, Mockito.times(fitBitTypes.size))
                .getDailyStepsForPeriod(anyLong(), anyLong())
        }
    }

    @Test(expected = Exception::class)
    fun `given an invalid input day data when called throw exception`() {
        runBlocking {
            val testDevice = provideFitBitTestDevice()
            val testStepData = provideDailyStepTestData()
            Mockito.`when`(prefs.fitBitDevice).thenReturn(testDevice)
            Mockito.`when`(appRepository.getDailyStepsForPeriod(anyLong(), anyLong()))
                .thenReturn(testStepData)
            for (fitBitDataType in fitBitTypes) {
                val result = getFitBitDataForPeriodUseCase.getFitbitForLastDays(
                    -1,
                    fitBitDataType
                )

                assertNotNull(result)
                assertEquals(testNumberOfDays, result.size)
            }

            Mockito.verify(prefs, Mockito.times(fitBitTypes.size)).fitBitDevice
            Mockito.verify(appRepository, Mockito.times(fitBitTypes.size))
                .getDailyStepsForPeriod(anyLong(), anyLong())
        }
    }

    @Test(expected = Exception::class)
    fun `given an non-existing FitBit device input data when called throw exception`() {
        runBlocking {
            val testStepData = provideDailyStepTestData()
            Mockito.`when`(prefs.fitBitDevice).thenReturn(null)
            Mockito.`when`(appRepository.getDailyStepsForPeriod(anyLong(), anyLong()))
                .thenReturn(testStepData)
            for (fitBitDataType in fitBitTypes) {
                val result = getFitBitDataForPeriodUseCase.getFitbitForLastDays(
                    testNumberOfDays,
                    fitBitDataType
                )

                assertNotNull(result)
                assertEquals(testNumberOfDays, result.size)
            }

            Mockito.verify(prefs, Mockito.times(fitBitTypes.size)).fitBitDevice
            Mockito.verify(appRepository, Mockito.times(fitBitTypes.size))
                .getDailyStepsForPeriod(anyLong(), anyLong())
        }
    }

    private fun provideFitBitTestDevice() = FitBitDevice(
        GetUserDevicesEntity(
            deviceId = testDeviceId,
            name = "FitBit Test device",
            description = "",
            uuid = UUID.randomUUID().toString(),
            deviceType = SupportedDeviceTypes.FITBIT_WATCH.name,
            active = true,
        )
    )

    private fun provideDailyStepTestData(): List<StepDailyEntity> {
        val testData = mutableListOf<StepDailyEntity>()
        for (i in 0 until testStepDataSize) {
            testData.add(
                StepDailyEntity(
                    id = i,
                    deviceId = testDeviceId,
                    steps = testResultDaySteps,
                    activityCalories = 1_000,
                    calories_out = 1_000,
                    heartRateMin = 50.0,
                    heartRateAvg = 60.0,
                    heartRateMax = 70.0,
                    restingHeartRate = 50.0,
                    minutesAsleep = 480,
                    minutesInBed = 480,
                    timestamp = System.currentTimeMillis()
                )
            )
        }

        return testData
    }
}