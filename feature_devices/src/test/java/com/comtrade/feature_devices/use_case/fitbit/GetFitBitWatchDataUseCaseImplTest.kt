/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: GetFitBitWatchDataUseCaseImplTest.kt
 * Author: sdelic
 *
 * History:  25.5.22. 12:27 created
 */

package com.comtrade.feature_devices.use_case.fitbit

import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.contracts.repository.user.IDeviceRepository
import com.comtrade.domain.contracts.usecases.device.IGetFitBitWatchDataUseCase
import com.comtrade.domain.entities.devices.fitbit.*
import com.comtrade.domain.settings.SupportedDeviceTypes
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import java.time.Instant
import java.util.*

/**
 * Testing scenario for the FitBit watch data obtaining use case implementation detail.
 */
internal class GetFitBitWatchDataUseCaseImplTest {
    private val testDeviceId = 1L
    private val testTimeInterval = 1_000L
    private val testToken = "testToken"
    private val testDataSize = 1_000L

    private lateinit var prefs: IPrefs
    private lateinit var deviceRepository: IDeviceRepository
    private lateinit var getFitBitWatchDataUseCaseImpl: IGetFitBitWatchDataUseCase

    @Before
    fun setUp() {
        prefs = Mockito.mock(IPrefs::class.java)
        deviceRepository = Mockito.mock(IDeviceRepository::class.java)
        getFitBitWatchDataUseCaseImpl = GetFitBitWatchDataUseCaseImpl(prefs, deviceRepository)
    }

    @Test
    fun `given valid device Id when invoked return FitBit data`() {
        runBlocking {
            Mockito.`when`(prefs.token).thenReturn(testToken)
            val testData = provideTestResponseData()
            Mockito.`when`(
                deviceRepository.getFitbitWatchData(
                    testToken,
                    testDeviceId,
                    testTimeInterval
                )
            ).thenReturn(testData)

            val result =
                getFitBitWatchDataUseCaseImpl.getFitbitWatchData(testDeviceId, testTimeInterval)

            Mockito.verify(prefs, Mockito.times(1)).token
            Mockito.verify(deviceRepository, Mockito.times(1))
                .getFitbitWatchData(testToken, testDeviceId, testTimeInterval)

            assertNotNull(result)
            assertEquals(testDataSize.toInt(), result.size)
        }
    }

    @Test(expected = Exception::class)
    fun `given an invalid user token when invoked throw exception`() {
        runBlocking {
            Mockito.`when`(prefs.token).thenReturn(null)
            val testData = provideTestResponseData()
            Mockito.`when`(
                deviceRepository.getFitbitWatchData(
                    testToken,
                    testDeviceId,
                    testTimeInterval
                )
            ).thenReturn(testData)

            val result =
                getFitBitWatchDataUseCaseImpl.getFitbitWatchData(testDeviceId, testTimeInterval)

            Mockito.verify(prefs, Mockito.times(1)).token
            Mockito.verify(deviceRepository, Mockito.times(1))
                .getFitbitWatchData(testToken, testDeviceId, testTimeInterval)

            assertNotNull(result)
            assertEquals(0, result.size)
        }
    }

    @Test(expected = Exception::class)
    fun `given an invalid device Id when invoked throw exception`() {
        runBlocking {
            Mockito.`when`(prefs.token).thenReturn(testToken)
            val testData = provideTestResponseData()
            Mockito.`when`(
                deviceRepository.getFitbitWatchData(
                    testToken,
                    -1,
                    testTimeInterval
                )
            ).thenReturn(testData)

            val result =
                getFitBitWatchDataUseCaseImpl.getFitbitWatchData(-1, testTimeInterval)

            Mockito.verify(prefs, Mockito.times(1)).token
            Mockito.verify(deviceRepository, Mockito.times(1))
                .getFitbitWatchData(testToken, -1, testTimeInterval)

            assertNotNull(result)
            assertEquals(0, result.size)
        }
    }

    @Test(expected = Exception::class)
    fun `given invalid timestamp when invoked throw exception`() {
        runBlocking {
            Mockito.`when`(prefs.token).thenReturn(testToken)
            val testData = provideTestResponseData()
            Mockito.`when`(
                deviceRepository.getFitbitWatchData(
                    testToken,
                    testDeviceId,
                    -1
                )
            ).thenReturn(testData)

            val result =
                getFitBitWatchDataUseCaseImpl.getFitbitWatchData(testDeviceId, -1)

            Mockito.verify(prefs, Mockito.times(1)).token
            Mockito.verify(deviceRepository, Mockito.times(1))
                .getFitbitWatchData(testToken, testDeviceId, -1)

            assertNotNull(result)
            assertEquals(0, result.size)
        }
    }

    private fun provideTestResponseData(): List<FitBitDataPayloadEntity> {
        val testData = mutableListOf<FitBitDataPayloadEntity>()

        for (i in 0 until testDataSize) {
            testData.add(
                FitBitDataPayloadEntity(
                    id = i,
                    timestamp = testTimeInterval,
                    sleepData = SleepDataEntity(
                        summary = SleepDataSummaryEntity(100, 50)
                    ),
                    activityData = ActivityDataEntity(
                        activityCalories = 1_000,
                        caloriesBMR = 1_000,
                        caloriesOut = 1_000,
                        elevation = 0,
                        fairlyActiveMinutes = 10,
                        floors = 10,
                        lightlyActiveMinutes = 20,
                        marginalCalories = 0,
                        restingHeartRate = 10,
                        sedentaryMinutes = 0,
                        steps = 1_000,
                        veryActiveMinutes = 1,
                        distances = emptyList(),
                        heartRateZones = null
                    ),
                    device = FitBitDeviceEntity(
                        id = testDeviceId,
                        name = "FitBit Test device",
                        description = "",
                        uuid = UUID.randomUUID().toString(),
                        deviceType = SupportedDeviceTypes.FITBIT_WATCH.name,
                        active = true,
                        startTimestamp = Instant.now().toEpochMilli().toString(),
                        userExtraId = 1L
                    )
                )
            )
        }

        return testData
    }
}