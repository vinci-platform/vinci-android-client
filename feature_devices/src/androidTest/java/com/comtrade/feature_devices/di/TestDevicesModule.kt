/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: TestDevicesModule.kt
 * Author: sdelic
 *
 * History:  29.4.22. 16:10 created
 */

package com.comtrade.feature_devices.di

import com.comtrade.domain.contracts.usecases.device.*
import dagger.Module
import dagger.Provides
import org.mockito.Mockito

@Module
class TestDevicesModule {
    @Provides
    fun provideDeleteUserDeviceUseCase(
    ): IDeleteUserDeviceUseCase = Mockito.mock(IDeleteUserDeviceUseCase::class.java)

    @Provides
    fun provideSyncCMDFromServerUseCase(): ISyncCMDDeviceFromServerUseCase =
        Mockito.mock(ISyncCMDDeviceFromServerUseCase::class.java)

    @Provides
    fun provideGetFitBitDataUseCase(): IGetFitBitWatchDataUseCase =
        Mockito.mock(IGetFitBitWatchDataUseCase::class.java)

    @Provides
    fun provideGetFitBitDailyDataUseCase(): IGetFitBitDailyDataUseCase =
        Mockito.mock(IGetFitBitDailyDataUseCase::class.java)

    @Provides
    fun provideGetFitBitDataForPeriodUseCase(): IGetFitBitDataForPeriodUseCase =
        Mockito.mock(IGetFitBitDataForPeriodUseCase::class.java)

    @Provides
    fun provideSyncFitBitFromServerUseCase(): ISyncFitBitFromServerUseCase =
        Mockito.mock(ISyncFitBitFromServerUseCase::class.java)

    @Provides
    fun provideDisconnectFitBitUseCase(): IDisconnectFitBitUseCase =
        Mockito.mock(IDisconnectFitBitUseCase::class.java)

    @Provides
    fun provideGetFitBitUrlUseCase(): IGetFitBitUrlUseCase =
        Mockito.mock(IGetFitBitUrlUseCase::class.java)

    @Provides
    fun provideUpdateShoeDeviceUseCase(): IUpdateDeviceUseCase =
        Mockito.mock(IUpdateDeviceUseCase::class.java)
}