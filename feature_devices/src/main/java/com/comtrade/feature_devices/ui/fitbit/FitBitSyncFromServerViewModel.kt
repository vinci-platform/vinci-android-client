/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: FitBitSyncFromServerViewModel.kt
 * Author: sdelic
 *
 * History:  31.5.22. 14:57 created
 */

package com.comtrade.feature_devices.ui.fitbit

import androidx.lifecycle.*
import com.comtrade.domain.contracts.usecases.device.ISyncFitBitFromServerUseCase
import com.comtrade.domain_ui.state.RequestUIState
import com.comtrade.feature_devices.ui.fitbit.FitBitSyncFromServerViewModel.SyncFitBitDataRequest.DefaultState
import com.comtrade.feature_devices.ui.fitbit.FitBitSyncFromServerViewModel.SyncFitBitDataRequest.FitBitDataRequest
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

/**
 * Android ViewModel component for dispatching the possible UI states for the synchronizing FitBit from server use case implementation detail.
 */
class FitBitSyncFromServerViewModel @Inject constructor(private val syncFitBitFromServerUseCase: ISyncFitBitFromServerUseCase) :
    ViewModel() {

    private val syncFitBitDataRequest: MutableLiveData<SyncFitBitDataRequest> =
        MutableLiveData(DefaultState)

    val syncFitBitDataObserver: LiveData<RequestUIState<Boolean>> =
        Transformations.switchMap(syncFitBitDataRequest) {
            liveData(context = viewModelScope.coroutineContext + Dispatchers.IO) {
                when (it) {
                    DefaultState -> emit(RequestUIState.DefaultState)
                    FitBitDataRequest -> {
                        emit(RequestUIState.OnLoading)
                        try {
                            val result =
                                syncFitBitFromServerUseCase.syncFitBitFromServer()

                            if (result) {
                                emit(RequestUIState.OnSuccess(result))
                            } else {
                                emit(RequestUIState.OnError(null))
                            }

                        } catch (e: Exception) {
                            e.printStackTrace()
                            emit(RequestUIState.OnError(e.message))
                        }
                    }
                }
            }
        }


    fun syncFitBitFromServer() {
        syncFitBitDataRequest.value = FitBitDataRequest
    }

    fun resetState() {
        syncFitBitDataRequest.value = DefaultState
    }

    /**
     * Sync FitBit watch data from server request states.
     * @property FitBitDataRequest the request state.
     * @property DefaultState the reset/default state for an request. Dispatching the event to be handled by some other observing component.
     */
    private sealed class SyncFitBitDataRequest {
        object FitBitDataRequest : SyncFitBitDataRequest()
        object DefaultState : SyncFitBitDataRequest()
    }
}