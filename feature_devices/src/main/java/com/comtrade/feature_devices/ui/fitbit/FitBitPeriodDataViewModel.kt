/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: FitBitPeriodDataViewModel.kt
 * Author: sdelic
 *
 * History:  30.5.22. 10:15 created
 */

package com.comtrade.feature_devices.ui.fitbit

import androidx.lifecycle.*
import com.comtrade.domain.contracts.usecases.device.IGetFitBitDataForPeriodUseCase
import com.comtrade.domain.entities.graph.GraphData
import com.comtrade.domain.entities.step.FitbitDataType
import com.comtrade.domain_ui.state.RequestUIState
import com.comtrade.feature_devices.ui.fitbit.FitBitPeriodDataViewModel.GetFitBitDataRequest.DefaultState
import com.comtrade.feature_devices.ui.fitbit.FitBitPeriodDataViewModel.GetFitBitDataRequest.FitBitData
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

/**
 * Android ViewModel component for the obtaining of the FitBit device data for n days and handling the UI state.
 */
class FitBitPeriodDataViewModel @Inject constructor(private val getFitBitDataForPeriodUseCase: IGetFitBitDataForPeriodUseCase) :
    ViewModel() {

    private val getFitBiDataRequest: MutableLiveData<GetFitBitDataRequest> =
        MutableLiveData(DefaultState)

    val fitBitPeriodDataObserver: LiveData<RequestUIState<Map<FitbitDataType, Map<Int, Array<GraphData>>>>> =
        Transformations.switchMap(getFitBiDataRequest) {
            liveData(context = viewModelScope.coroutineContext + Dispatchers.IO) {
                when (it) {
                    DefaultState -> emit(RequestUIState.DefaultState)
                    is FitBitData -> {
                        emit(RequestUIState.OnLoading)
                        try {

                            val result = mutableMapOf<FitbitDataType, Map<Int, Array<GraphData>>>()

                            //For each passed data type, calculate corresponding day period request
                            for (dataType in it.dataTypes) {
                                val dayResult = mutableMapOf<Int, Array<GraphData>>()
                                for (dayInterval in it.numberOfDays) {
                                    val resultSteps =
                                        getFitBitDataForPeriodUseCase.getFitbitForLastDays(
                                            dayInterval,
                                            dataType
                                        )
                                    dayResult[dayInterval] = resultSteps
                                }
                                result[dataType] = dayResult
                            }

                            emit(RequestUIState.OnSuccess(result))

                        } catch (e: Exception) {
                            e.printStackTrace()
                            emit(RequestUIState.OnError(e.message))
                        }
                    }
                }
            }
        }


    fun getFitBitDataForTypes(
        numberOfDays: IntArray,
        dataType: Array<FitbitDataType>
    ) {
        getFitBiDataRequest.value = FitBitData(numberOfDays, dataType)
    }

    fun resetState() {
        getFitBiDataRequest.value = DefaultState
    }

    /**
     * Obtain FitBit watch data request states.
     * @property FitBitData the request state.
     * @property DefaultState the reset/default state for an request. Dispatching the event to be handled by some other observing component.
     */
    private sealed class GetFitBitDataRequest {
        data class FitBitData(
            val numberOfDays: IntArray,
            val dataTypes: Array<FitbitDataType>
        ) : GetFitBitDataRequest() {
            override fun equals(other: Any?): Boolean {
                if (this === other) return true
                if (javaClass != other?.javaClass) return false

                other as FitBitData

                if (!numberOfDays.contentEquals(other.numberOfDays)) return false
                if (!dataTypes.contentEquals(other.dataTypes)) return false

                return true
            }

            override fun hashCode(): Int {
                var result = numberOfDays.contentHashCode()
                result = 31 * result + dataTypes.contentHashCode()
                return result
            }
        }

        object DefaultState : GetFitBitDataRequest()
    }
}