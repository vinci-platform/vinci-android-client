/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: UpdateDeviceViewModel.kt
 * Author: sdelic
 *
 * History:  29.6.22. 14:37 created
 */

package com.comtrade.feature_devices.ui

import androidx.lifecycle.*
import com.comtrade.domain.contracts.usecases.device.IUpdateDeviceUseCase
import com.comtrade.domain_ui.state.RequestUIState
import com.comtrade.feature_devices.ui.UpdateDeviceViewModel.UpdateDeviceRequest.DefaultState
import com.comtrade.feature_devices.ui.UpdateDeviceViewModel.UpdateDeviceRequest.UpdateDevice
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

/**
 * Android ViewModel component for the updating the attached device data use case and UI states.
 */
class UpdateDeviceViewModel @Inject constructor(private val updateDeviceUseCase: IUpdateDeviceUseCase) :
    ViewModel() {

    private val updateDeviceRequest: MutableLiveData<UpdateDeviceRequest> =
        MutableLiveData(DefaultState)

    val updateDeviceObserve: LiveData<RequestUIState<Boolean>> =
        Transformations.switchMap(updateDeviceRequest) {
            liveData(context = viewModelScope.coroutineContext + Dispatchers.IO) {
                when (it) {
                    DefaultState -> emit(RequestUIState.DefaultState)
                    UpdateDevice -> {
                        emit(RequestUIState.OnLoading)
                        try {
                            val result =
                                updateDeviceUseCase.updateDevice()

                            if (result) {
                                emit(RequestUIState.OnSuccess(result))
                            } else {
                                emit(RequestUIState.OnError(null))
                            }

                        } catch (e: Exception) {
                            e.printStackTrace()
                            emit(RequestUIState.OnError(e.message))
                        }
                    }
                }
            }
        }

    /**
     * Update User device.
     */
    fun updateDevice() {
        updateDeviceRequest.value = UpdateDevice
    }

    fun resetState() {
        updateDeviceRequest.value = DefaultState
    }

    /**
     * Update User device data request states.
     * @property UpdateDevice the request state.
     * @property DefaultState the reset/default state for an request. Dispatching the event to be handled by some other observing component.
     */
    private sealed class UpdateDeviceRequest {
        object UpdateDevice : UpdateDeviceRequest()
        object DefaultState : UpdateDeviceRequest()
    }
}