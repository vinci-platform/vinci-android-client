/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: FitBitDataViewModel.kt
 * Author: sdelic
 *
 * History:  25.5.22. 13:07 created
 */

package com.comtrade.feature_devices.ui.fitbit

import androidx.lifecycle.*
import com.comtrade.domain.contracts.usecases.device.IGetFitBitDailyDataUseCase
import com.comtrade.domain.contracts.usecases.device.IGetFitBitWatchDataUseCase
import com.comtrade.feature_devices.ui.fitbit.FitBitDataViewModel.GetFitBitDataRequest.DefaultState
import com.comtrade.feature_devices.ui.fitbit.FitBitDataViewModel.GetFitBitDataRequest.FitBitData
import com.comtrade.feature_devices.ui.state.APICallSuccessUIState
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

/**
 * Android ViewModel component for the obtaining of FitBit User data use case.
 */
class FitBitDataViewModel @Inject constructor(
    private val getFitBitWatchDataUseCase: IGetFitBitWatchDataUseCase,
    private val getFitBitDailyDataUseCase: IGetFitBitDailyDataUseCase
) :
    ViewModel() {

    private val getFitBitWatchDataRequest =
        MutableLiveData<GetFitBitDataRequest>(DefaultState)

    val getFitBitWatchDataObserver: LiveData<APICallSuccessUIState<FitBitPresenterData>> =
        Transformations.switchMap(getFitBitWatchDataRequest) {
            liveData(context = viewModelScope.coroutineContext + Dispatchers.IO) {
                when (it) {
                    DefaultState -> emit(APICallSuccessUIState.DefaultState)
                    is FitBitData -> {
                        emit(APICallSuccessUIState.OnLoading)
                        try {
                            val resultFitBitData =
                                getFitBitWatchDataUseCase.getFitbitWatchData(
                                    it.deviceId,
                                    it.timeStamp
                                )

                            val resultFitBitDailyData =
                                getFitBitDailyDataUseCase.getFitBitDailyDataForInterval(
                                    it.deviceId,
                                    it.timeStamp
                                )

                            if (resultFitBitData.isEmpty() && resultFitBitDailyData.isEmpty()) {
                                emit(APICallSuccessUIState.OnSuccess(FitBitPresenterData()))
                            } else {
                                var presenterData = FitBitPresenterData()
                                if (resultFitBitData.isNotEmpty()) {
                                    val steps = resultFitBitData.sumOf { it.activityData.steps }

                                    val caloriesActivity =
                                        resultFitBitData.sumOf { it.activityData.activityCalories }

                                    val caloriesOut =
                                        resultFitBitData.sumOf { it.activityData.caloriesOut }

                                    val totalCalories = caloriesActivity + caloriesOut

                                    val sleepData =
                                        resultFitBitData.sumOf { it.sleepData.summary.totalMinutesAsleep }

                                    presenterData =
                                        presenterData.copy(
                                            steps = steps.toString(),
                                            calories = totalCalories.toString(),
                                            sleep = sleepData.toString()
                                        )

                                }

                                if (resultFitBitDailyData.isNotEmpty()) {
                                    val hr =
                                        resultFitBitDailyData.last().data.last { element -> element.heart != null && element.heart!! > 0 }.heart
                                            ?: 0
                                    val hrAvg = resultFitBitDailyData.sumOf {
                                        it.data.sumOf { data ->
                                            data.heart ?: 0
                                        } / it.data.size
                                    } / resultFitBitDailyData.size

                                    presenterData =
                                        presenterData.copy(
                                            hr = hr.toString(),
                                            hrAvg = hrAvg.toString(),
                                        )
                                }

                                emit(
                                    APICallSuccessUIState.OnSuccess(
                                        presenterData
                                    )
                                )

                            }

                        } catch (e: Exception) {
                            e.printStackTrace()
                            emit(APICallSuccessUIState.OnError(e.message))
                        }
                    }
                }
            }
        }


    fun getFitBitWatchData(deviceId: Long, timeStamp: Long) {
        getFitBitWatchDataRequest.value = FitBitData(deviceId, timeStamp)
    }

    fun resetState() {
        getFitBitWatchDataRequest.value = DefaultState
    }

    /**
     * Obtain FitBit watch data request states.
     * @property FitBitData the request state.
     * @property DefaultState the reset/default state for an request. Dispatching the event to be handled by some other observing component.
     */
    private sealed class GetFitBitDataRequest {
        data class FitBitData(val deviceId: Long, val timeStamp: Long) : GetFitBitDataRequest()
        object DefaultState : GetFitBitDataRequest()
    }
}