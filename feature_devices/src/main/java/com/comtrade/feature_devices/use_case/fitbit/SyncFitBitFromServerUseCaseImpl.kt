/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: SyncFitBitFromServerUseCaseImpl.kt
 * Author: sdelic
 *
 * History:  31.5.22. 11:52 created
 */

package com.comtrade.feature_devices.use_case.fitbit

import com.comtrade.domain.contracts.repository.IAppRepository
import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.contracts.repository.user.IDeviceRepository
import com.comtrade.domain.contracts.usecases.device.ISyncFitBitFromServerUseCase
import com.comtrade.domain.entities.step.StepDailyEntity
import java.time.Instant
import javax.inject.Inject

/**
 * Implementation detail for the synchronizing FitBit from server use case.
 */
internal class SyncFitBitFromServerUseCaseImpl @Inject constructor(
    private val prefs: IPrefs,
    private val appRepository: IAppRepository,
    private val deviceRepository: IDeviceRepository
) :
    ISyncFitBitFromServerUseCase {
    override suspend fun syncFitBitFromServer(): Boolean {

        val fitBitDevice = prefs.fitBitDevice ?: throw Exception("No FitBit watch device found.")

        val userToken = prefs.token
        if (userToken.isBlank()) {
            throw Exception("Authentication token cannot be empty!")
        }

        // TODO: check for double conversion of timestamp
        val maxDatabaseDate = Instant.ofEpochMilli(
            appRepository.getMaxDailyTimestamp(fitBitDevice.deviceId)
        )
        val result =
            deviceRepository.getFitbitWatchData(
                userToken,
                fitBitDevice.deviceId, maxDatabaseDate.toEpochMilli()
            )

        val resultHeartRateData =
            deviceRepository.getFitbitWatchDailyData(
                userToken,
                fitBitDevice.deviceId, maxDatabaseDate.toEpochMilli()
            )

        if (result.isNotEmpty()) {
            prefs.addLog("Received fitbit data from server: $result")
            for (data in result) {
                prefs.addLog("Received fitbit data from server: $data")

                var heartRateMin = 0.0
                var heartRateMax = 0.0
                var heartRateAvg = 0.0
                //extract heart rate data for the given timestamp
                if (resultHeartRateData.isNotEmpty()) {
                    val dayData = resultHeartRateData.filter { it.timestamp >= data.timestamp }
                    heartRateMin = dayData.minOf {
                        it.data.minOf { heartData ->
                            heartData.heart ?: 0
                        }
                    }.toDouble()

                    heartRateMax = dayData.maxOf {
                        it.data.maxOf { heartData ->
                            heartData.heart ?: 0
                        }
                    }.toDouble()

                    heartRateAvg = (dayData.sumOf {
                        it.data.sumOf { heartData ->
                            heartData.heart ?: 0
                        } / it.data.size
                    } / dayData.size).toDouble()
                }

                var databaseSteps: StepDailyEntity? = appRepository
                    .getDailySteps(fitBitDevice.deviceId, data.timestamp)
                if (databaseSteps == null) {
                    databaseSteps = StepDailyEntity(
                        0,
                        fitBitDevice.deviceId,
                        data.activityData.steps,
                        data.activityData.activityCalories,
                        data.activityData.caloriesOut,
                        heartRateMin,
                        heartRateAvg,
                        heartRateMax,
                        data.activityData.restingHeartRate?.toDouble() ?: 0.0,
                        data.sleepData.summary.totalMinutesAsleep,
                        data.sleepData.summary.totalTimeInBed,
                        data.timestamp
                    )
                    val insertionResult = appRepository.insertDaily(databaseSteps)
                    if (insertionResult < 1L) {
                        return false
                    }
                } else {
                    databaseSteps = databaseSteps.copy(
                        steps = data.activityData.steps,
                        activityCalories = data.activityData.activityCalories,
                        calories_out = data.activityData.caloriesOut,
                        heartRateMin = heartRateMin,
                        heartRateAvg = heartRateAvg,
                        heartRateMax = heartRateMax,
                        restingHeartRate = data.activityData.restingHeartRate?.toDouble()
                            ?: 0.0,
                        minutesAsleep = data.sleepData.summary.totalMinutesAsleep,
                        minutesInBed = data.sleepData.summary.totalTimeInBed
                    )
                    val updateResult = appRepository.updateDaily(databaseSteps)
                    if (updateResult != 1) {
                        return false
                    }
                }
            }
        }

        return true
    }
}