/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: SyncCMDFromServerUseCaseImpl.kt
 * Author: sdelic
 *
 * History:  13.5.22. 16:44 created
 */

package com.comtrade.feature_devices.use_case

import com.comtrade.domain.contracts.repository.IAppRepository
import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.contracts.repository.user.UserServiceRepository
import com.comtrade.domain.contracts.usecases.device.ISyncCMDDeviceFromServerUseCase
import com.comtrade.domain.entities.devices.data.CmdWatchEntity
import com.comtrade.domain.entities.devices.data.GetCmdWatchDataRequest
import com.comtrade.domain.time.AppDateTimeConstants
import java.time.Instant
import java.time.temporal.ChronoUnit
import javax.inject.Inject

/**
 * Implementation detail for the CMD device synchronization from the server use case.
 */
internal class SyncCMDFromServerUseCaseImpl @Inject constructor(
    private val prefs: IPrefs,
    private val userServiceRepository: UserServiceRepository,
    private val appRepository: IAppRepository
) : ISyncCMDDeviceFromServerUseCase {
    @Suppress("SENSELESS_COMPARISON")
    override suspend fun syncCmdFromServer(): Boolean {

        val cmdDevice = prefs.cmdDevice ?: throw Exception("No CMD device found attached!")

        if (cmdDevice.deviceId < 1L) {
            throw Exception("The CMD device has an invalid device Id value: ${cmdDevice.deviceId}")
        }

        val token = prefs.token
        if (token.isBlank()) {
            throw Exception("No valid User token provided: $token")
        }

        var startTimestamp =
            appRepository.getMaxTimestamp(cmdDevice.deviceId)
        if (startTimestamp == 0.toLong()) {
            startTimestamp =
                Instant.now().minus(AppDateTimeConstants.DAYS_IN_YEAR.toLong(), ChronoUnit.DAYS)
                    .toEpochMilli()
        }
        val cmdNetworkData = userServiceRepository.getCmdWatchData(
            token,
            GetCmdWatchDataRequest(
                Instant.ofEpochMilli(startTimestamp + 1000).toString(),
                cmdDevice.deviceId.toString(),
                Instant.now().toString()
            )
        )

        if (cmdNetworkData != null) {
            prefs.addLog("Received watch data from server: $cmdNetworkData")
            for (cmdRecord in cmdNetworkData.h02!!) {
                val insertionResult = appRepository.insertCMDEntity(
                    CmdWatchEntity(
                        id = 0,
                        coordinates = null,
                        steps = cmdRecord.steps!!.toString(), //check if double conversion exists ,was .toDouble().roundToInt()
                        timestamp = cmdRecord.timestamp * 1000,
                        deviceId = cmdDevice.deviceId,
                    )
                )
                if (insertionResult < 1L) {
                    return false
                }
            }
        } else {
            return false
        }

        return true
    }
}