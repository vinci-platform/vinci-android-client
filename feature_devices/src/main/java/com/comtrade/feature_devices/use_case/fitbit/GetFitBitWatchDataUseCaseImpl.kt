/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: GetFitBitWatchDataUseCaseImpl.kt
 * Author: sdelic
 *
 * History:  25.5.22. 12:26 created
 */

package com.comtrade.feature_devices.use_case.fitbit

import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.contracts.repository.user.IDeviceRepository
import com.comtrade.domain.contracts.usecases.device.IGetFitBitWatchDataUseCase
import com.comtrade.domain.entities.devices.fitbit.FitBitDataPayloadEntity
import javax.inject.Inject

/**
 * Implementation detail for the FitBit watch data obtaining use case.
 */
internal class GetFitBitWatchDataUseCaseImpl @Inject constructor(
    private val prefs: IPrefs,
    private val deviceRepository: IDeviceRepository
) : IGetFitBitWatchDataUseCase {
    override suspend fun getFitbitWatchData(
        deviceId: Long,
        timestamp: Long
    ): List<FitBitDataPayloadEntity> {
        val userToken = prefs.token

        if (userToken.isBlank()) {
            throw Exception("Authentication token cannot be empty!")
        }

        if (deviceId < 1) {
            throw Exception("Device Id cannot be smaller than 1. Id sent: $deviceId")
        }

        if (timestamp < 0) {
            throw Exception("Timestamp cannot be negative: $timestamp")
        }

        return deviceRepository.getFitbitWatchData(userToken, deviceId, timestamp)
    }
}