/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: GetFitBitDataForPeriodUseCaseImpl.kt
 * Author: sdelic
 *
 * History:  27.5.22. 16:05 created
 */

package com.comtrade.feature_devices.use_case.fitbit

import com.comtrade.domain.contracts.repository.IAppRepository
import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.contracts.usecases.device.IGetFitBitDataForPeriodUseCase
import com.comtrade.domain.entities.graph.GraphData
import com.comtrade.domain.entities.step.FitbitDataType
import java.time.LocalDate
import java.time.ZoneOffset
import java.time.temporal.ChronoUnit
import java.util.*
import javax.inject.Inject

/**
 * Implementation detail for the obtaining of the FitBit device data for n days use case
 */
internal class GetFitBitDataForPeriodUseCaseImpl @Inject constructor(
    private val prefs: IPrefs,
    private val appRepository: IAppRepository
) :
    IGetFitBitDataForPeriodUseCase {
    override suspend fun getFitbitForLastDays(
        numberOfDays: Int,
        dataType: FitbitDataType
    ): Array<GraphData> {

        if (numberOfDays < 1) {
            throw Exception("Request number of days cannot be lower than 1 :$numberOfDays")
        }

        val device = prefs.fitBitDevice ?: throw Exception("No FitBit device found!")

        val today = LocalDate.now().atStartOfDay(ZoneOffset.UTC)
        val startDate = today.minus(numberOfDays.toLong() - 1, ChronoUnit.DAYS)
        val response: MutableList<GraphData> = mutableListOf()
        val dataFromDatabase = appRepository.getDailyStepsForPeriod(
            device.deviceId,
            startDate.toInstant().toEpochMilli()
        )
        if (dataFromDatabase.isNotEmpty()) {
            var returnDate = startDate.toLocalDate()
            while (returnDate <= today.toLocalDate()) {
                val graphData = GraphData()
                graphData.dateTime = Date.from(
                    returnDate.atStartOfDay().atZone(ZoneOffset.UTC).toInstant()
                )
                for (dayData in dataFromDatabase)
                    if (dayData.timestamp == returnDate.atStartOfDay().atZone(ZoneOffset.UTC)
                            .toInstant()
                            .toEpochMilli()
                    ) {
                        graphData.value = when (dataType) {
                            FitbitDataType.STEPS -> dayData.steps
                            FitbitDataType.ACTIVITY_CALORIES -> dayData.activityCalories
                            FitbitDataType.CALORIES_OUT -> dayData.calories_out
                            FitbitDataType.MINUTES_ASLEEP -> dayData.minutesAsleep
                            FitbitDataType.MINUTES_IN_BED -> dayData.minutesInBed
                            FitbitDataType.RESTING_HEART_RATE -> dayData.restingHeartRate.toInt()
                        }
                        break
                    }
                response.add(graphData)
                returnDate = returnDate.plusDays(1)
            }
        }

        return response.toTypedArray()
    }
}