/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: DisconnectFitBitUseCaseImpl.kt
 * Author: sdelic
 *
 * History:  27.6.22. 12:50 created
 */

package com.comtrade.feature_devices.use_case.fitbit

import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.contracts.repository.user.UserServiceRepository
import com.comtrade.domain.contracts.usecases.device.IDisconnectFitBitUseCase
import com.comtrade.domain.entities.devices.data.UpdateDeviceRequest
import com.comtrade.domain.settings.SupportedDeviceTypes
import javax.inject.Inject

/**
 * Implementation detail for disconnecting a FitBit device use case.
 */
internal class DisconnectFitBitUseCaseImpl @Inject constructor(
    private val prefs: IPrefs,
    private val userServiceRepository: UserServiceRepository
) : IDisconnectFitBitUseCase {
    @Suppress("USELESS_ELVIS")
    override suspend fun disconnectDevice(): Boolean {

        val fitBitDevice = prefs.fitBitDevice ?: throw Exception("No FitBit device attached!")

        val userToken = prefs.token

        if (userToken.isBlank()) {
            throw Exception("Authentication token cannot be empty!")
        }

        val userExtraId = prefs.userExtraId

        val result = userServiceRepository.updateDevice(
            userToken,
            UpdateDeviceRequest(
                fitBitDevice.deviceId,
                fitBitDevice.deviceName,
                fitBitDevice.description,
                fitBitDevice.deviceUUID,
                SupportedDeviceTypes.FITBIT_WATCH.name,
                false,
                userExtraId
            )
        )

        if (!result) {
            return false
        }

        //Update local status
        fitBitDevice.active = false
        prefs.fitBitDevice = fitBitDevice

        //Sync the User extra data.
        val userExtraData = userServiceRepository.getUserExtra(
            userToken, prefs.userId
        ) ?: return false

        prefs.setUserExtraData(userExtraData)

        return true
    }
}