/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: DevicesModule.kt
 * Author: sdelic
 *
 * History:  29.4.22. 14:33 created
 */

package com.comtrade.feature_devices.di

import com.comtrade.domain.contracts.repository.IAppRepository
import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.contracts.repository.user.IDeviceRepository
import com.comtrade.domain.contracts.repository.user.UserServiceRepository
import com.comtrade.domain.contracts.usecases.device.*
import com.comtrade.feature_devices.use_case.DeleteUserDeviceUseCaseImpl
import com.comtrade.feature_devices.use_case.SyncCMDFromServerUseCaseImpl
import com.comtrade.feature_devices.use_case.UpdateDeviceUseCaseImpl
import com.comtrade.feature_devices.use_case.fitbit.*
import dagger.Module
import dagger.Provides

/**
 * Dagger DI devices module for providing implementation details.
 */
@Module
class DevicesModule {

    @Provides
    fun provideDeleteUserDeviceUseCase(
        prefs: IPrefs,
        deviceRepository: IDeviceRepository
    ): IDeleteUserDeviceUseCase = DeleteUserDeviceUseCaseImpl(prefs, deviceRepository)

    @Provides
    fun provideSyncCMDFromServerUseCase(
        prefs: IPrefs,
        userServiceRepository: UserServiceRepository,
        appRepository: IAppRepository
    ): ISyncCMDDeviceFromServerUseCase =
        SyncCMDFromServerUseCaseImpl(prefs, userServiceRepository, appRepository)

    @Provides
    fun provideGetFitBitDataUseCase(
        prefs: IPrefs,
        deviceRepository: IDeviceRepository,
    ): IGetFitBitWatchDataUseCase =
        GetFitBitWatchDataUseCaseImpl(prefs, deviceRepository)

    @Provides
    fun provideGetFitBitDailyDataUseCase(
        prefs: IPrefs,
        deviceRepository: IDeviceRepository,
    ): IGetFitBitDailyDataUseCase =
        GetFitBitDailyDataUseCaseImpl(prefs, deviceRepository)

    @Provides
    fun provideGetFitBitDataForPeriodUseCase(
        prefs: IPrefs,
        appRepository: IAppRepository,
    ): IGetFitBitDataForPeriodUseCase =
        GetFitBitDataForPeriodUseCaseImpl(prefs, appRepository)

    @Provides
    fun provideSyncFitBitFromServerUseCase(
        prefs: IPrefs,
        appRepository: IAppRepository,
        deviceRepository: IDeviceRepository
    ): ISyncFitBitFromServerUseCase =
        SyncFitBitFromServerUseCaseImpl(prefs, appRepository, deviceRepository)

    @Provides
    fun provideDisconnectFitBitUseCase(
        prefs: IPrefs,
        userServiceRepository: UserServiceRepository,
    ): IDisconnectFitBitUseCase =
        DisconnectFitBitUseCaseImpl(prefs, userServiceRepository)

    @Provides
    fun provideGetFitBitUrlUseCase(
        prefs: IPrefs,
        userServiceRepository: UserServiceRepository,
    ): IGetFitBitUrlUseCase =
        GetFitBitUrlUseCaseImpl(prefs, userServiceRepository)

    @Provides
    fun provideUpdateShoeDeviceUseCase(
        prefs: IPrefs,
        userServiceRepository: UserServiceRepository,
    ): IUpdateDeviceUseCase =
        UpdateDeviceUseCaseImpl(prefs, userServiceRepository)
}