/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: BitmapPictureUtil.kt
 * Author: sdelic
 *
 * History:  5.5.22. 12:23 created
 */

package com.comtrade.feature_account.util

import android.graphics.Bitmap
import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.feature_account.contracts.IBitmapUtil
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStream

object BitmapPictureUtil : IBitmapUtil {

    override fun saveImage(
        directoryPath: String,
        fileName: String,
        transformedImageBitmap: Bitmap?
    ): String {
        val directory = File(directoryPath)
        if (!directory.exists()) {
            directory.mkdirs()
        }
        val file = File(directory, fileName)
        if (file.exists()) {
            file.delete()
        }

        saveImageToStream(transformedImageBitmap, FileOutputStream(file))
        return file.absolutePath
    }

    override fun deleteProfileImage(prefs: IPrefs): Boolean {

        val filePath = prefs.userProfilePicture
        val file = File(filePath)
        val result = if (file.exists() && !file.absolutePath.startsWith("content:/")) {
            file.delete()
        } else {
            false
        }
        prefs.userProfilePicture = ""
        return result
    }

    private fun saveImageToStream(bitmap: Bitmap?, outputStream: OutputStream?) {
        if (outputStream != null) {
            try {
                bitmap?.compress(Bitmap.CompressFormat.JPEG, 100, outputStream)
                outputStream.close()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }
}