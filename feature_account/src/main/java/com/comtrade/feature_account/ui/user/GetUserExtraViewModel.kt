/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: GetUserExtraViewModel.kt
 * Author: sdelic
 *
 * History:  27.4.22. 15:33 created
 */

package com.comtrade.feature_account.ui.user

import androidx.lifecycle.*
import com.comtrade.domain.contracts.usecases.account.IGetUserExtraUseCase
import com.comtrade.domain.entities.user.account.GetUserExtraEntity
import com.comtrade.feature_account.ui.state.APICallSuccessUIState
import com.comtrade.feature_account.ui.user.GetUserExtraViewModel.GetUserExtraRequest.DefaultState
import com.comtrade.feature_account.ui.user.GetUserExtraViewModel.GetUserExtraRequest.GetUserExtra
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

/**
 * Android ViewModel component for generating the User extra UI states.
 */
class GetUserExtraViewModel @Inject constructor(private val getUserExtraUseCase: IGetUserExtraUseCase) :
    ViewModel() {

    private val getUserExtraRequest =
        MutableLiveData<GetUserExtraRequest>(DefaultState)

    val userExtraState: LiveData<APICallSuccessUIState<GetUserExtraEntity>> =
        Transformations.switchMap(getUserExtraRequest) {
            liveData(context = viewModelScope.coroutineContext + Dispatchers.IO) {
                when (it) {
                    DefaultState -> emit(APICallSuccessUIState.DefaultState)
                    is GetUserExtra -> {
                        emit(APICallSuccessUIState.OnLoading)
                        try {
                            val result =
                                getUserExtraUseCase.getUserExtra(it.userId)

                            emit(APICallSuccessUIState.OnSuccess(result))
                        } catch (e: Exception) {
                            e.printStackTrace()
                            emit(APICallSuccessUIState.OnError(e.message))
                        }
                    }
                }
            }
        }

    fun getUser(userId: Long) {
        getUserExtraRequest.value =
            GetUserExtra(userId)
    }

    fun resetState() {
        getUserExtraRequest.value =
            DefaultState
    }

    /**
     * Get User extra data request states.
     * @property GetUserExtra the request state.
     * @property DefaultState the reset/default state for an request. Dispatching the event to be handled by some other observing component.
     */
    private sealed class GetUserExtraRequest {
        data class GetUserExtra(val userId: Long) : GetUserExtraRequest()
        object DefaultState : GetUserExtraRequest()
    }
}