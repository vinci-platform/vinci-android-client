/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: RefreshUserDataViewModel.kt
 * Author: sdelic
 *
 * History:  5.5.22. 15:13 created
 */

package com.comtrade.feature_account.ui.user

import androidx.lifecycle.*
import com.comtrade.domain.contracts.usecases.account.IRefreshUserData
import com.comtrade.domain.entities.user.account.GetUserEntity
import com.comtrade.feature_account.ui.state.APICallSuccessUIState
import com.comtrade.feature_account.ui.user.RefreshUserDataViewModel.RefreshUserDataRequest.DefaultState
import com.comtrade.feature_account.ui.user.RefreshUserDataViewModel.RefreshUserDataRequest.RefreshUserData
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

/**
 * Android ViewModel component for the refresh User data use case.
 * Handles the possible UI states for this use case.
 */
class RefreshUserDataViewModel @Inject constructor(private val refreshUserData: IRefreshUserData) :
    ViewModel() {

    private val refreshUserDataRequest =
        MutableLiveData<RefreshUserDataRequest>(DefaultState)

    val refreshUserDataObserve: LiveData<APICallSuccessUIState<Boolean>> =
        Transformations.switchMap(refreshUserDataRequest) {
            liveData(context = viewModelScope.coroutineContext + Dispatchers.IO) {
                when (it) {
                    DefaultState -> emit(APICallSuccessUIState.DefaultState)
                    is RefreshUserData -> {
                        emit(APICallSuccessUIState.OnLoading)
                        try {
                            val result =
                                refreshUserData.refreshUserData(it.userEntity, it.directory)
                            emit(APICallSuccessUIState.OnSuccess(result))
                        } catch (e: Exception) {
                            e.printStackTrace()
                            emit(APICallSuccessUIState.OnError(e.message))
                        }
                    }
                }
            }
        }

    /**
     * Synchronizes the User data across the network layer.
     * @param userEntity The User entity data.
     * @param directory The Profile image storage directory.
     */
    fun refreshUserData(userEntity: GetUserEntity, directory: String) {
        refreshUserDataRequest.value = RefreshUserData(userEntity, directory)
    }


    fun resetState() {
        refreshUserDataRequest.value = DefaultState
    }

    /**
     * Refresh the User data request states.
     * @property RefreshUserData the request state.
     * @property DefaultState the reset/default state for an request. Dispatching the event to be handled by some other observing component.
     */
    private sealed class RefreshUserDataRequest {
        data class RefreshUserData(val userEntity: GetUserEntity, val directory: String) :
            RefreshUserDataRequest()

        object DefaultState : RefreshUserDataRequest()
    }
}