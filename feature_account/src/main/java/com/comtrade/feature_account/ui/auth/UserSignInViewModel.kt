/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: UserSignInViewModel.kt
 * Author: sdelic
 *
 * History:  21.4.22. 11:26 created
 */

package com.comtrade.feature_account.ui.auth

import androidx.lifecycle.*
import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.contracts.usecases.auth.IUserSignInUseCase
import com.comtrade.feature_account.ui.auth.UserSignInViewModel.SignIntRequest.DefaultState
import com.comtrade.feature_account.ui.auth.UserSignInViewModel.SignIntRequest.SignInEvent
import com.comtrade.feature_account.ui.state.APICallSuccessUIState
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

/**
 * Android ViewModel component for User sign in use case.
 */
class UserSignInViewModel @Inject constructor(
    prefs: IPrefs,
    userSignInUseCase: IUserSignInUseCase
) : ViewModel() {

    private val signInRequest = MutableLiveData<SignIntRequest>(DefaultState)
    val signInEventObservable: LiveData<APICallSuccessUIState<Boolean>> =
        Transformations.switchMap(signInRequest) {
            liveData(context = viewModelScope.coroutineContext + Dispatchers.IO) {
                when (it) {
                    DefaultState -> emit(APICallSuccessUIState.DefaultState)
                    is SignInEvent -> {
                        emit(APICallSuccessUIState.OnLoading)
                        try {

                            val result =
                                userSignInUseCase.authenticateUser(
                                    it.username,
                                    it.password,
                                    it.rememberMe
                                )
                            if (result.tokenId != null) {
                                prefs.token = result.tokenId ?: ""
                                prefs.userAutologin = true
                                prefs.userLogin = it.username
                                prefs.userPassword = it.password

                                emit(APICallSuccessUIState.OnSuccess(true))
                            } else {
                                emit(APICallSuccessUIState.OnError(null))
                            }

                        } catch (e: Exception) {
                            e.printStackTrace()
                            emit(APICallSuccessUIState.OnError(e.message))
                        }
                    }
                }
            }
        }

    fun signIn(
        username: String,
        password: String,
        rememberMe: Boolean
    ) {
        signInRequest.value = SignInEvent(username, password, rememberMe)
    }

    fun resetState() {
        signInRequest.value = DefaultState
    }


    /**
     * Save event record data request states.
     * @property SignInEvent the request state with the request payload.
     * @property DefaultState the reset/default state for an request. Dispatching the event to be handled by some other observing component.
     */
    private sealed class SignIntRequest {
        data class SignInEvent(
            val username: String,
            val password: String,
            val rememberMe: Boolean
        ) : SignIntRequest()

        object DefaultState : SignIntRequest()
    }
}