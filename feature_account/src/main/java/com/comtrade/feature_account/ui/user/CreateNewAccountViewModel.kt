/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: CreateNewAccountViewModel.kt
 * Author: sdelic
 *
 * History:  13.4.22. 12:24 created
 */

package com.comtrade.feature_account.ui.user

import androidx.lifecycle.*
import com.comtrade.domain.contracts.usecases.account.ICreateNewUserUseCase
import com.comtrade.feature_account.ui.state.APICallSuccessUIState
import com.comtrade.feature_account.ui.user.CreateNewAccountRequest.CreateAccountRequest
import com.comtrade.feature_account.ui.user.CreateNewAccountRequest.DefaultState
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

/**
 * Android ViewModel component for the create new User use case flow.
 */
class CreateNewAccountViewModel @Inject constructor(private val createNewUserUseCase: ICreateNewUserUseCase) :
    ViewModel() {

    private val createAccountRequest =
        MutableLiveData<CreateNewAccountRequest>(DefaultState)
    val createNewAccount: LiveData<APICallSuccessUIState<Boolean>> =
        Transformations.switchMap(createAccountRequest) {
            liveData(context = viewModelScope.coroutineContext + Dispatchers.IO) {
                when (it) {
                    is CreateAccountRequest -> {
                        emit(APICallSuccessUIState.OnLoading)
                        try {
                            val data = createNewUserUseCase.createUser(
                                it.login,
                                it.email,
                                it.password,
                                it.role
                            )
                            emit(APICallSuccessUIState.OnSuccess(data))

                        } catch (e: Exception) {
                            e.printStackTrace()
                            emit(APICallSuccessUIState.OnError(e.message))
                        }
                    }
                    DefaultState -> emit(APICallSuccessUIState.DefaultState)
                }
            }
        }

    fun createAccount(login: String, email: String, password: String, role: String) {
        createAccountRequest.value =
            CreateAccountRequest(login, email, password, role)
    }

    fun resetState() {
        createAccountRequest.value = DefaultState
    }
}

/**
 * Create new User request states.
 * @property CreateAccountRequest the request state with the request payload.
 * @property DefaultState the reset/default state for an request. Dispatching the event to be handled by some other observing component.
 */
private sealed class CreateNewAccountRequest {
    //The immutable data holder for the create new User request.
    data class CreateAccountRequest(
        val login: String,
        val email: String,
        val password: String,
        val role: String
    ) : CreateNewAccountRequest()

    object DefaultState : CreateNewAccountRequest()

}