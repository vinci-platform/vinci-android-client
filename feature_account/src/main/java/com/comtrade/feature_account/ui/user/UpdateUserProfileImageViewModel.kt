/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: UpdateUserProfileImageViewModel.kt
 * Author: sdelic
 *
 * History:  28.6.22. 14:52 created
 */

package com.comtrade.feature_account.ui.user

import androidx.lifecycle.*
import com.comtrade.domain.contracts.usecases.account.IUpdateUserProfileImageUseCase
import com.comtrade.domain_ui.state.RequestUIState
import com.comtrade.feature_account.ui.user.UpdateUserProfileImageViewModel.UpdateUserProfileImageRequest.DefaultState
import com.comtrade.feature_account.ui.user.UpdateUserProfileImageViewModel.UpdateUserProfileImageRequest.UpdateUserProfileImage
import kotlinx.coroutines.Dispatchers
import java.io.File
import javax.inject.Inject

/**
 * Android ViewModel component for for uploading a new User profile image use case and UI states.
 */
class UpdateUserProfileImageViewModel @Inject constructor(private val updateUserProfileImageUseCase: IUpdateUserProfileImageUseCase) :
    ViewModel() {

    private val updateUserProfileImageRequest: MutableLiveData<UpdateUserProfileImageRequest> =
        MutableLiveData(DefaultState)

    val updateUserProfileImageObserve: LiveData<RequestUIState<Boolean>> =
        Transformations.switchMap(updateUserProfileImageRequest) {
            liveData(context = viewModelScope.coroutineContext + Dispatchers.IO) {
                when (it) {
                    DefaultState -> emit(RequestUIState.DefaultState)
                    is UpdateUserProfileImage -> {
                        emit(RequestUIState.OnLoading)
                        try {
                            val result =
                                updateUserProfileImageUseCase.saveImage(it.file)

                            if (result) {
                                emit(RequestUIState.OnSuccess(result))
                            } else {
                                emit(RequestUIState.OnError(null))
                            }

                        } catch (e: Exception) {
                            e.printStackTrace()
                            emit(RequestUIState.OnError(e.message))
                        }
                    }
                }
            }
        }


    fun saveImage(file: File) {
        updateUserProfileImageRequest.value =
            UpdateUserProfileImage(file)
    }

    fun resetState() {
        updateUserProfileImageRequest.value = DefaultState
    }

    /**
     * Update the User profile image data..
     * @property UpdateUserProfileImage the request state.
     * @property DefaultState the reset/default state for an request. Dispatching the event to be handled by some other observing component.
     */
    private sealed class UpdateUserProfileImageRequest {
        data class UpdateUserProfileImage(val file: File) : UpdateUserProfileImageRequest()
        object DefaultState : UpdateUserProfileImageRequest()
    }
}