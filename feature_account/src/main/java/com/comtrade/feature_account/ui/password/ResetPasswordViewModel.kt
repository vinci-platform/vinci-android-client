/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: ResetPasswordViewModel.kt
 * Author: sdelic
 *
 * History:  7.4.22. 16:12 created
 */

package com.comtrade.feature_account.ui.password

import androidx.lifecycle.*
import com.comtrade.domain.contracts.usecases.account.IResetPasswordUseCase
import com.comtrade.feature_account.ui.password.ResetPasswordViewModel.ForgotPasswordRequest.DefaultState
import com.comtrade.feature_account.ui.password.ResetPasswordViewModel.ForgotPasswordRequest.SavePasswordPayload
import com.comtrade.feature_account.ui.state.APICallSuccessUIState
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

/**
 * The Android ViewModel component for resetting the account password.
 */
class ResetPasswordViewModel @Inject constructor(private val resetPasswordUseCase: IResetPasswordUseCase) :
    ViewModel() {

    private val resetPasswordRequest = MutableLiveData<ForgotPasswordRequest>(DefaultState)
    val resetPassword: LiveData<APICallSuccessUIState<Boolean>> =
        Transformations.switchMap(resetPasswordRequest) {
            liveData(context = viewModelScope.coroutineContext + Dispatchers.IO) {
                when (it) {
                    DefaultState -> emit(APICallSuccessUIState.DefaultState)
                    is SavePasswordPayload -> {
                        emit(APICallSuccessUIState.OnLoading)

                        try {
                            val data = resetPasswordUseCase.resetPassword(it.payload)
                            emit(APICallSuccessUIState.OnSuccess(data))

                        } catch (e: Exception) {
                            e.printStackTrace()
                            emit(APICallSuccessUIState.OnError(e.message))
                        }
                    }
                }
            }
        }

    fun resetPassword(email: String) {
        resetPasswordRequest.value = SavePasswordPayload(email)
    }

    fun resetState() {
        resetPasswordRequest.value = DefaultState
    }

    /**
     * Reset password data request states.
     * @property SavePasswordPayload the request state with the request payload.
     * @property DefaultState the reset/default state for an request. Dispatching the event to be handled by some other observing component.
     */
    private sealed class ForgotPasswordRequest {
        data class SavePasswordPayload(val payload: String) : ForgotPasswordRequest()
        object DefaultState : ForgotPasswordRequest()
    }
}