/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: CreateNewUserUseCaseImpl.kt
 * Author: sdelic
 *
 * History:  13.4.22. 11:27 created
 */

package com.comtrade.feature_account.use_case.user

import com.comtrade.domain.contracts.repository.user.IAccountRepository
import com.comtrade.domain.contracts.usecases.account.ICreateNewUserUseCase
import com.comtrade.domain.contracts.utils.IEmailValidator
import com.comtrade.domain.entities.user.UserRoles
import com.comtrade.domain.validation.ValidationException
import com.comtrade.domain.validation.ValidationRules
import javax.inject.Inject

/**
 * Implementation detail for creating a new User use case.
 */
internal class CreateNewUserUseCaseImpl @Inject constructor(
    private val emailValidator: IEmailValidator,
    private val accountRepository: IAccountRepository
) :
    ICreateNewUserUseCase {
    override suspend fun createUser(
        login: String,
        email: String,
        password: String,
        role: String
    ): Boolean {

        if (login.isBlank()) {
            throw Exception("Login/Username must not be empty")
        }

        if (login.containsWhitespace()) {
            throw ValidationException("Username/Login cannot contain any whitespaces")
        }

        if (email.isBlank()) {
            throw Exception("Email must not be empty")
        }

        if (!emailValidator.isValidEmail(email)) {
            throw Exception("Not a valid email address: $email")
        }

        if (password.isBlank()) {
            throw Exception("Password must not be empty")
        }

        if (password.containsWhitespace()) {
            throw ValidationException("Password cannot contain any whitespaces")
        }

        if (password.length < ValidationRules.MIN_PASSWORD_LENGTH) {
            throw Exception("Password must be a minimum length of: ${ValidationRules.MIN_PASSWORD_LENGTH}")
        }

        if (ValidationRules.HAS_UPPERCASE) {
            if (!password.containsOneUpperCaseLetter()) {
                throw ValidationException("Password must contain at least one upper case letter!")
            }
        }

        if (ValidationRules.HAS_LOWERCASE) {
            if (!password.containsOneLowerCaseLetter()) {
                throw ValidationException("Password must contain at least one lower case letter!")
            }
        }

        if (ValidationRules.HAS_DASHES_AND_UNDERSCORES) {
            if (!password.containsOneDashOrUnderscore()) {
                throw ValidationException("Password must contain at least one hyphen ('-') or underscore ('_')!")
            }
        }

        if (ValidationRules.HAS_NUMBERS) {
            if (!password.containsAtLeastOneNumber()) {
                throw ValidationException("Password must contain at least one number!")
            }
        }

        if (role.isBlank()) {
            throw Exception("Role must be set")
        }

        if (role != UserRoles.ROLE_PACIENT.name && role != UserRoles.ROLE_FAMILY.name) {
            throw Exception("Role must be of types: ${UserRoles.ROLE_PACIENT.name} or ${UserRoles.ROLE_FAMILY.name}")
        }

        return accountRepository.createUser(login, email, password, role)
    }
}

/**
 * Helper function for determining if a String contains one or more whitespaces.
 */
internal fun String.containsWhitespace(): Boolean {
    //!str.matches("\\S+") && (str.length() > 0)
    return !this.matches("\\S+".toRegex()) && this.isNotEmpty()
}

/**
 * Helper function for determining if a String contains one or more upper case letters.
 */
internal fun String.containsOneUpperCaseLetter(): Boolean {
    return this.matches(".*[A-Z].*".toRegex())
}

/**
 * Helper function for determining if a String contains one or more lower case letters.
 */
internal fun String.containsOneLowerCaseLetter(): Boolean {
    return this.matches(".*[a-z].*".toRegex())
}

/**
 * Helper function for determining if a String contains one or more dashes or underscores.
 */
internal fun String.containsOneDashOrUnderscore(): Boolean {
    return this.matches(".*[-_].*".toRegex())
}

/**
 * Helper function for determining if a String contains at least one number.
 */
internal fun String.containsAtLeastOneNumber(): Boolean {
    return this.matches(".*[0-9].*".toRegex())
}