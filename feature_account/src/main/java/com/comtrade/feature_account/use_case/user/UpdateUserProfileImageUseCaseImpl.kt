/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: UpdateUserProfileImageUseCaseImpl.kt
 * Author: sdelic
 *
 * History:  28.6.22. 12:33 created
 */

package com.comtrade.feature_account.use_case.user

import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.contracts.repository.user.UserServiceRepository
import com.comtrade.domain.contracts.usecases.account.IUpdateUserProfileImageUseCase
import com.comtrade.domain.entities.user.data.UpdateUserRequest
import com.google.gson.Gson
import java.io.File
import javax.inject.Inject

/**
 * Implementation detail for uploading a new User profile image use case.
 */
internal class UpdateUserProfileImageUseCaseImpl @Inject constructor(
    private val prefs: IPrefs,
    private val gson: Gson,
    private val userServiceRepository: UserServiceRepository
) : IUpdateUserProfileImageUseCase {
    @Suppress("USELESS_ELVIS")
    override suspend fun saveImage(file: File): Boolean {

        val userToken = prefs.token

        if (userToken.isBlank()) {
            throw Exception("Authentication token cannot be empty!")
        }

        //Get most recent User data
        val user = userServiceRepository.getUser(userToken) ?: return false

        val profileImageId = prefs.userProfilePictureId

        if (profileImageId < 0) {
            throw Exception("Invalid User profile image Id provided: $profileImageId")
        }

        //Upload new profile image file
        return userServiceRepository.updateUserImage(
            userToken,
            gson.toJson(
                UpdateUserRequest(
                    user.userId,
                    user.login,
                    user.firstName,
                    user.lastName,
                    user.gender,
                    user.address,
                    user.email,
                    user.phone,
                    "",
                    user.role
                )
            ),
            profileImageId,
            file
        )
    }
}