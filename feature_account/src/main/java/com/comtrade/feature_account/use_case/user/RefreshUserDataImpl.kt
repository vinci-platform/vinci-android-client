/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: RefreshUserDataImpl.kt
 * Author: sdelic
 *
 * History:  5.5.22. 12:31 created
 */

package com.comtrade.feature_account.use_case.user

import android.graphics.BitmapFactory
import android.util.Base64
import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.contracts.repository.user.IAccountRepository
import com.comtrade.domain.contracts.usecases.account.IRefreshUserData
import com.comtrade.domain.entities.user.account.GetUserEntity
import com.comtrade.feature_account.contracts.IBitmapUtil
import javax.inject.Inject

/**
 * Implementation detail for the continuous refresh User data implementation.
 * Stores the basic User data in the local storage.
 * Fetches the User extra information for the image information and (if any) persists the bitmap into the device storage.
 */
internal class RefreshUserDataImpl @Inject constructor(
    private val prefs: IPrefs,
    private val accountRepository: IAccountRepository,
    private val bitmapUtil: IBitmapUtil
) :
    IRefreshUserData {
    override suspend fun refreshUserData(userEntity: GetUserEntity, directory: String): Boolean {
        //Setting the User userId and other details
        prefs.setUserData(userEntity)

        val token = prefs.token
        val userId = prefs.userId

        if (token.isBlank()) {
            throw Exception("Authentication token cannot be empty!")
        }

        if (userId < 0) {
            throw Exception("Invalid UserId submitted: $userId")
        }

        // Get User extra information
        val resultData = accountRepository.getUserExtra(token, userId)
        prefs.setUserExtraData(null)
        prefs.setUserExtraData(resultData)

        if (!resultData.images.isNullOrEmpty()) {
            resultData.images?.get(0)?.let {
                val imageBytes = Base64.decode(it.image, 0)
                val image = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.size)
                val fileName = "vinci_profile_image_${prefs.userId}.jpg"
                prefs.userProfilePicture =
                    bitmapUtil.saveImage(directory, fileName, image)
                prefs.userProfilePictureId = it.id
            }
        } else {
            prefs.userProfilePicture = ""
            prefs.userProfilePictureId = 0
        }
        return true
    }
}