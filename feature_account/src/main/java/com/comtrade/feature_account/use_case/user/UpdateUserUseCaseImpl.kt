/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: UpdateUserUseCaseImpl.kt
 * Author: sdelic
 *
 * History:  12.4.22. 12:28 created
 */

package com.comtrade.feature_account.use_case.user

import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.contracts.repository.user.IAccountRepository
import com.comtrade.domain.contracts.usecases.account.IUpdateAccountUseCase
import com.comtrade.domain.entities.user.data.UpdateUserRequest
import javax.inject.Inject

/**
 * Implementation detail for updating the current User account details.
 */
internal class UpdateUserUseCaseImpl @Inject constructor(
    private val prefs: IPrefs,
    private val accountRepository: IAccountRepository
) : IUpdateAccountUseCase {
    override suspend fun updateAccount(updateUserRequest: UpdateUserRequest): Boolean {
        val token = prefs.token

        if (token.isBlank()) {
            throw Exception("Authentication token not set!")
        }

        if (updateUserRequest.firstName.isNullOrBlank()) {
            throw Exception("Field: firstName is required!")
        }

        if (updateUserRequest.lastName.isNullOrBlank()) {
            throw Exception("Field: lastName is required!")
        }

        if (updateUserRequest.gender.isNullOrBlank()) {
            throw Exception("Field: gender is required!")
        }

        if (updateUserRequest.address.isNullOrBlank()) {
            throw Exception("Field: address is required!")
        }

        if (updateUserRequest.email.isNullOrBlank()) {
            throw Exception("Field: email is required!")
        }

        if (updateUserRequest.phone.isNullOrBlank()) {
            throw Exception("Field: phone is required!")
        }

        return accountRepository.updateAccount(token, updateUserRequest)
    }
}