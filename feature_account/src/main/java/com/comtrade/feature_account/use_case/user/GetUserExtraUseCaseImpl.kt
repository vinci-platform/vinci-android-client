/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: GetUserExtraUseCaseImpl.kt
 * Author: sdelic
 *
 * History:  27.4.22. 14:55 created
 */

package com.comtrade.feature_account.use_case.user

import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.contracts.repository.user.IAccountRepository
import com.comtrade.domain.contracts.usecases.account.IGetUserExtraUseCase
import com.comtrade.domain.entities.user.account.GetUserExtraEntity
import javax.inject.Inject

/**
 * Implementation detail for obtaining User extra use case data.
 */
internal class GetUserExtraUseCaseImpl @Inject constructor(
    private val prefs: IPrefs,
    private val accountRepository: IAccountRepository
) : IGetUserExtraUseCase {
    override suspend fun getUserExtra(id: Long): GetUserExtraEntity {
        val userToken = prefs.token

        if (userToken.isBlank()) {
            throw Exception("Authentication token cannot be empty!")
        }

        if (id < 0) {
            throw Exception("User Id cannot be smaller than 0. Id sent: $id")
        }

        return accountRepository.getUserExtra(userToken, id)
    }
}