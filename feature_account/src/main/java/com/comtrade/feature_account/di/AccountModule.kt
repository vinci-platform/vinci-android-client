/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: AccountModule.kt
 * Author: sdelic
 *
 * History:  7.4.22. 15:49 created
 */

package com.comtrade.feature_account.di

import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.contracts.repository.user.IAccountRepository
import com.comtrade.domain.contracts.repository.user.UserServiceRepository
import com.comtrade.domain.contracts.usecases.account.*
import com.comtrade.domain.contracts.usecases.auth.IUserSignInUseCase
import com.comtrade.domain.contracts.utils.IEmailValidator
import com.comtrade.feature_account.contracts.IBitmapUtil
import com.comtrade.feature_account.use_case.auth.UserSignInUseCaseImpl
import com.comtrade.feature_account.use_case.password.ChangePasswordUseCaseImpl
import com.comtrade.feature_account.use_case.password.ResetPasswordFinishUseCaseImpl
import com.comtrade.feature_account.use_case.password.ResetPasswordUseCaseImpl
import com.comtrade.feature_account.use_case.user.*
import com.comtrade.feature_account.util.BitmapPictureUtil
import com.google.gson.Gson
import dagger.Module
import dagger.Provides

/**
 * Dagger DI User account module. Provides implementations for the account use cases.
 */
@Module
open class AccountModule {

    @Provides
    internal fun provideBitmapUtil(): IBitmapUtil = BitmapPictureUtil

    @Provides
    internal fun providePasswordResetUseCase(
        prefs: IPrefs,
        emailValidator: IEmailValidator,
        accountRepository: IAccountRepository
    ): IResetPasswordUseCase =
        ResetPasswordUseCaseImpl(prefs, emailValidator, accountRepository)

    @Provides
    internal fun providePasswordResetFinishUseCase(
        accountRepository: IAccountRepository
    ): IResetPasswordFinishUseCase =
        ResetPasswordFinishUseCaseImpl(accountRepository)

    @Provides
    internal fun provideChangePasswordUseCase(
        prefs: IPrefs,
        accountRepository: IAccountRepository
    ): IChangePasswordUseCase = ChangePasswordUseCaseImpl(prefs, accountRepository)

    @Provides
    internal fun provideUpdateAccountUseCase(
        prefs: IPrefs,
        accountRepository: IAccountRepository
    ): IUpdateAccountUseCase = UpdateUserUseCaseImpl(prefs, accountRepository)

    @Provides
    internal fun provideCreateNewAccountUseCase(
        emailValidator: IEmailValidator,
        accountRepository: IAccountRepository
    ): ICreateNewUserUseCase = CreateNewUserUseCaseImpl(emailValidator, accountRepository)

    @Provides
    internal open fun provideUserSignInUseCase(
        accountRepository: IAccountRepository
    ): IUserSignInUseCase = UserSignInUseCaseImpl(accountRepository)

    @Provides
    internal fun provideGetMobileAppSettingsUseCase(
        prefs: IPrefs,
        accountRepository: IAccountRepository
    ): IGetMobileAppSettingsUseCase = GetMobileAppSettingsUseCaseImpl(prefs, accountRepository)

    @Provides
    internal fun provideGetUserUseCase(
        prefs: IPrefs,
        accountRepository: IAccountRepository
    ): IGetUserUseCase = GetUserUseCaseImpl(prefs, accountRepository)

    @Provides
    internal fun provideGetUserExtraUseCase(
        prefs: IPrefs,
        accountRepository: IAccountRepository
    ): IGetUserExtraUseCase = GetUserExtraUseCaseImpl(prefs, accountRepository)

    @Provides
    internal fun provideRefreshUserDataUseCase(
        prefs: IPrefs,
        accountRepository: IAccountRepository,
        bitmapUtil: IBitmapUtil
    ): IRefreshUserData = RefreshUserDataImpl(prefs, accountRepository, bitmapUtil)

    @Provides
    internal fun provideUpdateUserProfileImageUseCase(
        prefs: IPrefs,
        gson: Gson,
        userServiceRepository: UserServiceRepository
    ): IUpdateUserProfileImageUseCase =
        UpdateUserProfileImageUseCaseImpl(prefs, gson, userServiceRepository)
}