/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: AccountComponent.kt
 * Author: sdelic
 *
 * History:  7.4.22. 16:21 created
 */

package com.comtrade.feature_account.di

import com.comtrade.domain.contracts.usecases.auth.IUserSignInUseCase
import com.comtrade.feature_account.ui.auth.UserSignInViewModelFactory
import com.comtrade.feature_account.ui.password.ChangePasswordViewModelFactory
import com.comtrade.feature_account.ui.password.ResetPasswordFinishViewModelFactory
import com.comtrade.feature_account.ui.password.ResetPasswordViewModelFactory
import com.comtrade.feature_account.ui.user.*
import dagger.Subcomponent

/**
 * Dagger DI account subcomponent to be integrated in the main app component.
 */
@Subcomponent(modules = [AccountModule::class])
interface AccountComponent {

    fun providePasswordResetViewModelFactory(): ResetPasswordViewModelFactory
    fun providePasswordResetFinishViewModelFactory(): ResetPasswordFinishViewModelFactory
    fun provideChangePasswordViewModelFactory(): ChangePasswordViewModelFactory
    fun provideUpdateAccountViewModelFactory(): UpdateAccountViewModelFactory
    fun provideCreateAccountViewModelFactory(): CreateNewAccountViewModelFactory
    fun provideUserSignInViewModelFactory(): UserSignInViewModelFactory
    fun provideMobileAppSettingsViewModelFactory(): MobileAppSettingsViewModelFactory
    fun provideGetUserViewModelFactory(): GetUserViewModelFactory
    fun provideGetUserExtraViewModelFactory(): GetUserExtraViewModelFactory
    fun provideRefreshUserDataViewModelFactory(): RefreshUserDataViewModelFactory
    fun provideUpdateUserProfileImageViewModelFactory(): UpdateUserProfileImageViewModelFactory

    fun provideUserSignInUseCase(): IUserSignInUseCase

    @Subcomponent.Factory
    interface Factory {
        fun create(): AccountComponent
    }
}