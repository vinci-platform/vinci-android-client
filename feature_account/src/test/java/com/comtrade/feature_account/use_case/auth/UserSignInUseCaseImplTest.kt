/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: UserSignInUseCaseImplTest.kt
 * Author: sdelic
 *
 * History:  21.4.22. 10:48 created
 */

package com.comtrade.feature_account.use_case.auth

import com.comtrade.domain.contracts.repository.user.IAccountRepository
import com.comtrade.domain.contracts.usecases.auth.IUserSignInUseCase
import com.comtrade.domain.entities.auth.UserAuthenticateResponseEntity
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.`when`

/**
 * Test scenario for the User authenticate/sign in use case implementation detail.
 */
internal class UserSignInUseCaseImplTest {
    private val testUsername = "TestUsername"
    private val testPassword = "TestPassword"
    private val testRememberMeFlag = true
    private val testTokenResponse = "testTokenResponse"
    private lateinit var accountRepository: IAccountRepository
    private lateinit var userSignInUseCase: IUserSignInUseCase

    @Before
    fun setUp() {
        accountRepository = Mockito.mock(IAccountRepository::class.java)
        userSignInUseCase = UserSignInUseCaseImpl(accountRepository)
    }

    @Test
    fun `given valid request data when change user authenticate return success token`() {
        runBlocking {
            `when`(
                accountRepository.authenticateUser(
                    testUsername,
                    testPassword,
                    testRememberMeFlag
                )
            ).thenReturn(getTestUserAuthenticateResponseEntity(testTokenResponse))
            val result =
                userSignInUseCase.authenticateUser(testUsername, testPassword, testRememberMeFlag)
            Mockito.verify(accountRepository, Mockito.times(1))
                .authenticateUser(testUsername, testPassword, testRememberMeFlag)
            Assert.assertNotNull(result)
            Assert.assertNotNull(result.tokenId)
            Assert.assertTrue(!result.tokenId.isNullOrBlank())
            Assert.assertEquals(testTokenResponse, result.tokenId)
        }
    }

    @Test
    fun `given valid request data when change user authenticate return null token`() {
        runBlocking {
            `when`(
                accountRepository.authenticateUser(
                    testUsername,
                    testPassword,
                    testRememberMeFlag
                )
            ).thenReturn(getTestUserAuthenticateResponseEntity(null))
            val result =
                userSignInUseCase.authenticateUser(testUsername, testPassword, testRememberMeFlag)
            Mockito.verify(accountRepository, Mockito.times(1))
                .authenticateUser(testUsername, testPassword, testRememberMeFlag)
            Assert.assertNotNull(result)
            Assert.assertNull(result.tokenId)
        }
    }

    @Test(expected = Exception::class)
    fun `given an invalid username throw exception`() {
        runBlocking {
            `when`(
                accountRepository.authenticateUser(
                    "",
                    testPassword,
                    testRememberMeFlag
                )
            ).thenReturn(getTestUserAuthenticateResponseEntity(null))
            val result =
                userSignInUseCase.authenticateUser("", testPassword, testRememberMeFlag)
            Mockito.verify(accountRepository, Mockito.times(1))
                .authenticateUser("", testPassword, testRememberMeFlag)
            Assert.assertNotNull(result)
            Assert.assertNull(result.tokenId)
        }
    }

    @Test(expected = Exception::class)
    fun `given an invalid password throw exception`() {
        runBlocking {
            `when`(
                accountRepository.authenticateUser(
                    testUsername,
                    "",
                    testRememberMeFlag
                )
            ).thenReturn(getTestUserAuthenticateResponseEntity(null))
            val result =
                userSignInUseCase.authenticateUser(testUsername, "", testRememberMeFlag)
            Mockito.verify(accountRepository, Mockito.times(1))
                .authenticateUser(testUsername, "", testRememberMeFlag)
            Assert.assertNotNull(result)
            Assert.assertNull(result.tokenId)
        }
    }

    @Test
    fun `given a username with surrounding spaces when called trim username`() {
        runBlocking {
            val spacedUsername = " ".plus(testUsername).plus("   ")
            `when`(
                accountRepository.authenticateUser(
                    testUsername,
                    testPassword,
                    testRememberMeFlag
                )
            ).thenReturn(getTestUserAuthenticateResponseEntity(testTokenResponse))
            //Passing the trimmed username test value
            val result =
                userSignInUseCase.authenticateUser(spacedUsername, testPassword, testRememberMeFlag)
            Mockito.verify(accountRepository, Mockito.times(1))
                .authenticateUser(testUsername, testPassword, testRememberMeFlag)
            Assert.assertNotNull(result)
            Assert.assertNotNull(result.tokenId)
            Assert.assertTrue(!result.tokenId.isNullOrBlank())
            Assert.assertEquals(testTokenResponse, result.tokenId)
        }
    }

    @Test
    fun `given a password with surrounding spaces when called trim password`() {
        runBlocking {
            val spacedPassword = " ".plus(testPassword).plus("   ")
            `when`(
                accountRepository.authenticateUser(
                    testUsername,
                    testPassword,
                    testRememberMeFlag
                )
            ).thenReturn(getTestUserAuthenticateResponseEntity(testTokenResponse))
            //Passing the trimmed username test value
            val result =
                userSignInUseCase.authenticateUser(testUsername, spacedPassword, testRememberMeFlag)
            Mockito.verify(accountRepository, Mockito.times(1))
                .authenticateUser(testUsername, testPassword, testRememberMeFlag)
            Assert.assertNotNull(result)
            Assert.assertNotNull(result.tokenId)
            Assert.assertTrue(!result.tokenId.isNullOrBlank())
            Assert.assertEquals(testTokenResponse, result.tokenId)
        }
    }

    /**
     * Provide test response data.
     */
    private fun getTestUserAuthenticateResponseEntity(tokenId: String?): UserAuthenticateResponseEntity =
        UserAuthenticateResponseEntity(tokenId)
}