/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: RefreshUserDataImplTest.kt
 * Author: sdelic
 *
 * History:  5.5.22. 12:34 created
 */

package com.comtrade.feature_account.use_case.user

import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.contracts.repository.user.IAccountRepository
import com.comtrade.domain.contracts.usecases.account.IRefreshUserData
import com.comtrade.domain.entities.user.UserRoles
import com.comtrade.domain.entities.user.account.GetUserEntity
import com.comtrade.domain.entities.user.account.GetUserExtraEntity
import com.comtrade.domain.entities.user.data.ImageEntity
import com.comtrade.feature_account.contracts.IBitmapUtil
import kotlinx.coroutines.runBlocking
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.any
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mockito

/**
 * Test scenario for the refresh User data implementation detail.
 */
internal class RefreshUserDataImplTest {
    private val testUserId = 1L
    private val testToken = "testToken"
    private val testUsername = "testUsername"
    private val testEmail = "testEmail@test.com"
    private val testImageStoreDirectory = "testImageStoreDirectory"
    private val testImageStorePath = "testImageStorePath"
    private lateinit var prefs: IPrefs
    private lateinit var accountRepository: IAccountRepository
    private lateinit var bitmapUtil: IBitmapUtil
    private lateinit var refreshUserDataImpl: IRefreshUserData

    @Before
    fun setUp() {
        prefs = Mockito.mock(IPrefs::class.java)
        accountRepository = Mockito.mock(IAccountRepository::class.java)
        bitmapUtil = Mockito.mock(IBitmapUtil::class.java)
        refreshUserDataImpl = RefreshUserDataImpl(prefs, accountRepository, bitmapUtil)
    }

    @Test
    fun `given a valid user data payload when refresh return true`() {
        runBlocking {
            Mockito.`when`(prefs.token).thenReturn(testToken)
            Mockito.`when`(prefs.userId).thenReturn(testUserId)
            Mockito.`when`(accountRepository.getUserExtra(testToken, testUserId))
                .thenReturn(provideTestUserExtra())

            Mockito.`when`(bitmapUtil.saveImage(anyString(), anyString(), any()))
                .thenReturn(testImageStorePath)
            val result = refreshUserDataImpl.refreshUserData(
                provideTestUserEntity(),
                testImageStoreDirectory
            )

            assertNotNull(result)
            assertTrue(result)
        }
    }

    @Test(expected = Exception::class)
    fun `given invalid token data payload when refresh throw error`() {
        runBlocking {
            Mockito.`when`(prefs.token).thenReturn("")
            Mockito.`when`(prefs.userId).thenReturn(testUserId)
            Mockito.`when`(accountRepository.getUserExtra(testToken, testUserId))
                .thenReturn(provideTestUserExtra())

            Mockito.`when`(bitmapUtil.saveImage(anyString(), anyString(), any()))
                .thenReturn(testImageStorePath)
            val result = refreshUserDataImpl.refreshUserData(
                provideTestUserEntity(),
                testImageStoreDirectory
            )

            assertNull(result)
        }
    }

    @Test(expected = Exception::class)
    fun `given invalid userId data payload when refresh throw error`() {
        runBlocking {
            Mockito.`when`(prefs.token).thenReturn(testToken)
            Mockito.`when`(prefs.userId).thenReturn(-1L)
            Mockito.`when`(accountRepository.getUserExtra(testToken, -1L))
                .thenReturn(provideTestUserExtra())

            Mockito.`when`(bitmapUtil.saveImage(anyString(), anyString(), any()))
                .thenReturn(testImageStorePath)
            val result = refreshUserDataImpl.refreshUserData(
                provideTestUserEntity(),
                testImageStoreDirectory
            )

            assertNull(result)
        }
    }

    private fun provideTestUserEntity(): GetUserEntity = GetUserEntity(
        userId = testUserId,
        login = testUsername,
        firstName = "testFirstName",
        lastName = "testLastName",
        gender = "female",
        email = testEmail,
        address = "",
        phone = "",
        role = UserRoles.ROLE_PACIENT.name,
        devices = emptyList()
    )

    private fun provideTestUserExtra(): GetUserExtraEntity {
        val imageData = listOf(
            ImageEntity(
                id = 0,
                image = """TWFueSBoYW5kcyBtYWtlIGxpZ2h0IHdvcmsu""",
                null
            )
        )
        return GetUserExtraEntity(
            id = testUserId,
            phone = null,
            firstName = "testFirstName",
            surname = "testLastName",
            address = "null",
            birthDate = null,
            userFirstName = "test firstname",
            userLastName = "test lastname",
            gender = "female",
            education = null,
            maritalStatus = null,
            userId = testUserId,
            organizationIdentifier = null,
            familyIdentifier = null,
            friends = null,
            devices = emptyList(),
            images = imageData
        )
    }
}