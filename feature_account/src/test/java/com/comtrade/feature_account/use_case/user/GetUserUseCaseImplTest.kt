/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: GetUserUseCaseImplTest.kt
 * Author: sdelic
 *
 * History:  27.4.22. 10:09 created
 */

package com.comtrade.feature_account.use_case.user

import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.contracts.repository.user.IAccountRepository
import com.comtrade.domain.contracts.usecases.account.IGetUserUseCase
import com.comtrade.domain.entities.user.UserRoles
import com.comtrade.domain.entities.user.account.GetUserEntity
import kotlinx.coroutines.runBlocking
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mockito

/**
 * Test scenario for the implementation detail or obtaining the core User data.
 */
internal class GetUserUseCaseImplTest {
    private val testToken = "testToken"

    private lateinit var prefs: IPrefs
    private lateinit var accountRepository: IAccountRepository
    private lateinit var getUserUseCase: IGetUserUseCase

    @Before
    fun setUp() {
        prefs = Mockito.mock(IPrefs::class.java)
        accountRepository = Mockito.mock(IAccountRepository::class.java)
        getUserUseCase = GetUserUseCaseImpl(prefs, accountRepository)
    }

    @Test
    fun `given valid token when get user return user data`() {
        runBlocking {
            val testResponseEntity = getTestUserResponse()
            Mockito.`when`(prefs.token).thenReturn(testToken)
            Mockito.`when`(accountRepository.getUser(anyString())).thenReturn(testResponseEntity)
            val result = getUserUseCase.getUser()
            Mockito.verify(prefs, Mockito.times(1)).token
            Mockito.verify(accountRepository, Mockito.times(1)).getUser(testToken)
            assertNotNull(result)
            assertEquals(testResponseEntity, result)
        }
    }

    @Test
    fun `given valid token when get user return null`() {
        runBlocking {
            Mockito.`when`(prefs.token).thenReturn(testToken)
            Mockito.`when`(accountRepository.getUser(anyString())).thenReturn(null)
            val result = getUserUseCase.getUser()
            Mockito.verify(prefs, Mockito.times(1)).token
            Mockito.verify(accountRepository, Mockito.times(1)).getUser(testToken)
            assertNull(result)
        }
    }

    @Test(expected = Exception::class)
    fun `given invalid input token when get user throw exception`() {
        runBlocking {
            val testResponseEntity = getTestUserResponse()
            Mockito.`when`(prefs.token).thenReturn("")
            Mockito.`when`(accountRepository.getUser(anyString())).thenReturn(testResponseEntity)
            val result = getUserUseCase.getUser()
            Mockito.verify(prefs, Mockito.times(1)).token
            Mockito.verify(accountRepository, Mockito.times(1)).getUser(testToken)
            assertNotNull(result)
            assertEquals(testResponseEntity, result)
        }
    }

    private fun getTestUserResponse(): GetUserEntity = GetUserEntity(
        userId = 1L,
        login = "TestUser",
        firstName = "test firstname",
        lastName = "test lastname",
        gender = "test gender",
        email = "test@email.com",
        address = null,
        phone = null,
        role = UserRoles.ROLE_PACIENT.name,
        devices = emptyList()
    )
}