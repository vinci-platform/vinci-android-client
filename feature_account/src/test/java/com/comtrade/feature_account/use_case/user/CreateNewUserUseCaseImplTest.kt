/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: CreateNewUserUseCaseImplTest.kt
 * Author: sdelic
 *
 * History:  13.4.22. 11:28 created
 */

package com.comtrade.feature_account.use_case.user

import com.comtrade.domain.contracts.repository.user.IAccountRepository
import com.comtrade.domain.contracts.usecases.account.ICreateNewUserUseCase
import com.comtrade.domain.contracts.utils.IEmailValidator
import com.comtrade.domain.entities.user.UserRoles
import com.comtrade.domain.validation.ValidationException
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mockito

/**
 * Test scenario for the create new user implementation detail.
 */
internal class CreateNewUserUseCaseImplTest {
    private val testLoginUsername = "testLoginUsername"
    private val testEmail = "test@test.com"
    private val testPassword = "testPassword-_5"
    private val testRole = UserRoles.ROLE_PACIENT.name
    private lateinit var accountRepository: IAccountRepository
    private lateinit var emailValidator: IEmailValidator
    private lateinit var createNewUserUseCase: ICreateNewUserUseCase

    @Before
    fun setUp() {
        accountRepository = Mockito.mock(IAccountRepository::class.java)
        emailValidator = Mockito.mock(IEmailValidator::class.java)
        createNewUserUseCase = CreateNewUserUseCaseImpl(emailValidator, accountRepository)
    }

    @Test
    fun `given valid input data when create user then return true`() {
        runBlocking {
            Mockito.`when`(
                emailValidator.isValidEmail(testEmail)
            ).thenReturn(true)
            Mockito.`when`(
                accountRepository.createUser(
                    testLoginUsername,
                    testEmail,
                    testPassword,
                    testRole
                )
            )
                .thenReturn(true)
            val result = createNewUserUseCase.createUser(
                testLoginUsername,
                testEmail,
                testPassword,
                testRole
            )
            Mockito.verify(accountRepository, Mockito.times(1))
                .createUser(
                    testLoginUsername,
                    testEmail,
                    testPassword,
                    testRole
                )
            Mockito.verify(emailValidator, Mockito.times(1))
                .isValidEmail(testEmail)
            Assert.assertNotNull(result)
            Assert.assertTrue(result)
        }
    }

    @Test
    fun `given valid input data when create user then return false`() {
        runBlocking {
            Mockito.`when`(
                emailValidator.isValidEmail(testEmail)
            ).thenReturn(true)

            Mockito.`when`(
                accountRepository.createUser(
                    testLoginUsername,
                    testEmail,
                    testPassword,
                    testRole
                )
            )
                .thenReturn(false)
            val result = createNewUserUseCase.createUser(
                testLoginUsername,
                testEmail,
                testPassword,
                testRole
            )
            Mockito.verify(accountRepository, Mockito.times(1))
                .createUser(
                    testLoginUsername,
                    testEmail,
                    testPassword,
                    testRole
                )
            Mockito.verify(emailValidator, Mockito.times(1))
                .isValidEmail(testEmail)
            Assert.assertNotNull(result)
            Assert.assertFalse(result)
        }
    }

    @Test(expected = Exception::class)
    fun `given an invalid login throw exception`() {
        runBlocking {
            Mockito.`when`(
                emailValidator.isValidEmail(testEmail)
            ).thenReturn(true)
            Mockito.`when`(
                accountRepository.createUser(
                    anyString(),
                    anyString(),
                    anyString(),
                    anyString()
                )
            )
                .thenReturn(false)
            val result = createNewUserUseCase.createUser(
                "",
                testEmail,
                testPassword,
                testRole
            )
            Mockito.verify(accountRepository, Mockito.times(1))
                .createUser(
                    "",
                    testEmail,
                    testPassword,
                    testRole
                )
            Mockito.verify(emailValidator, Mockito.times(1))
                .isValidEmail(testEmail)
            Assert.assertNotNull(result)
            Assert.assertFalse(result)
        }
    }

    @Test(expected = ValidationException::class)
    fun `given an invalid login with whitespaces throw exception`() {
        runBlocking {
            val spacedUsername = " ".plus(testLoginUsername).plus("   ")

            Mockito.`when`(
                emailValidator.isValidEmail(testEmail)
            ).thenReturn(true)

            createNewUserUseCase.createUser(
                spacedUsername,
                testEmail,
                testPassword,
                testRole
            )
        }
    }

    @Test(expected = Exception::class)
    fun `given an empty email throw exception`() {
        runBlocking {
            Mockito.`when`(
                emailValidator.isValidEmail(testEmail)
            ).thenReturn(true)
            Mockito.`when`(
                accountRepository.createUser(
                    anyString(),
                    anyString(),
                    anyString(),
                    anyString()
                )
            )
                .thenReturn(false)
            val result = createNewUserUseCase.createUser(
                testLoginUsername,
                "",
                testPassword,
                testRole
            )
            Mockito.verify(accountRepository, Mockito.times(1))
                .createUser(
                    testLoginUsername,
                    "",
                    testPassword,
                    testRole
                )
            Mockito.verify(emailValidator, Mockito.times(1))
                .isValidEmail(testEmail)
            Assert.assertNotNull(result)
            Assert.assertFalse(result)
        }
    }

    @Test(expected = Exception::class)
    fun `given an invalid email throw exception`() {
        runBlocking {
            Mockito.`when`(
                emailValidator.isValidEmail(testEmail)
            ).thenReturn(false)
            Mockito.`when`(
                accountRepository.createUser(
                    anyString(),
                    anyString(),
                    anyString(),
                    anyString()
                )
            )
                .thenReturn(false)
            val result = createNewUserUseCase.createUser(
                testLoginUsername,
                testEmail,
                testPassword,
                testRole
            )
            Mockito.verify(accountRepository, Mockito.times(1))
                .createUser(
                    testLoginUsername,
                    testEmail,
                    testPassword,
                    testRole
                )
            Mockito.verify(emailValidator, Mockito.times(1))
                .isValidEmail(testEmail)
            Assert.assertNotNull(result)
            Assert.assertFalse(result)
        }
    }

    @Test(expected = Exception::class)
    fun `given an empty password throw exception`() {
        runBlocking {
            Mockito.`when`(
                emailValidator.isValidEmail(testEmail)
            ).thenReturn(true)
            Mockito.`when`(
                accountRepository.createUser(
                    anyString(),
                    anyString(),
                    anyString(),
                    anyString()
                )
            )
                .thenReturn(false)
            val result = createNewUserUseCase.createUser(
                testLoginUsername,
                testEmail,
                "",
                testRole
            )
            Mockito.verify(accountRepository, Mockito.times(1))
                .createUser(
                    testLoginUsername,
                    testEmail,
                    "",
                    testRole
                )
            Mockito.verify(emailValidator, Mockito.times(1))
                .isValidEmail(testEmail)

            Assert.assertNotNull(result)
            Assert.assertFalse(result)
        }
    }

    @Test(expected = Exception::class)
    fun `given an invalid password length throw exception`() {
        runBlocking {
            Mockito.`when`(
                emailValidator.isValidEmail(testEmail)
            ).thenReturn(true)
            Mockito.`when`(
                accountRepository.createUser(
                    anyString(),
                    anyString(),
                    anyString(),
                    anyString()
                )
            )
                .thenReturn(false)
            val result = createNewUserUseCase.createUser(
                testLoginUsername,
                testEmail,
                "123",
                testRole
            )
            Mockito.verify(accountRepository, Mockito.times(1))
                .createUser(
                    testLoginUsername,
                    testEmail,
                    "123",
                    testRole
                )
            Mockito.verify(emailValidator, Mockito.times(1))
                .isValidEmail(testEmail)

            Assert.assertNotNull(result)
            Assert.assertFalse(result)
        }
    }

    @Test(expected = ValidationException::class)
    fun `given an invalid password with whitespaces throw exception`() {
        runBlocking {
            val spacedPassword = " ".plus(testPassword).plus("   ")

            Mockito.`when`(
                emailValidator.isValidEmail(testEmail)
            ).thenReturn(true)

            createNewUserUseCase.createUser(
                testLoginUsername,
                testEmail,
                spacedPassword,
                testRole
            )
        }
    }

    @Test(expected = ValidationException::class)
    fun `given an invalid password without any uppercase letters throw exception`() {
        runBlocking {
            val testUpperCase = "gfit38bc9yryie"

            Mockito.`when`(
                emailValidator.isValidEmail(testEmail)
            ).thenReturn(true)

            createNewUserUseCase.createUser(
                testLoginUsername,
                testEmail,
                testUpperCase,
                testRole
            )
        }
    }


    @Test(expected = ValidationException::class)
    fun `given an invalid password without any lowercase letters throw exception`() {
        runBlocking {
            val testLowerCase = "37682HFJHWSFLI349D93UR09LL"

            Mockito.`when`(
                emailValidator.isValidEmail(testEmail)
            ).thenReturn(true)

            createNewUserUseCase.createUser(
                testLoginUsername,
                testEmail,
                testLowerCase,
                testRole
            )
        }
    }

    @Test(expected = ValidationException::class)
    fun `given an invalid password without any hyphens or underscores throw exception`() {
        runBlocking {
            val testNoHyphenUnderscorePassword = "37682HFJHWSFLI349D93UR09LL"

            Mockito.`when`(
                emailValidator.isValidEmail(testEmail)
            ).thenReturn(true)

            createNewUserUseCase.createUser(
                testLoginUsername,
                testEmail,
                testNoHyphenUnderscorePassword,
                testRole
            )
        }
    }

    @Test(expected = ValidationException::class)
    fun `given an invalid password without any numbers throw exception`() {
        runBlocking {
            val testNoNumbers = "JHKJDHIUW-__AGhFGFA"

            Mockito.`when`(
                emailValidator.isValidEmail(testEmail)
            ).thenReturn(true)

            createNewUserUseCase.createUser(
                testLoginUsername,
                testEmail,
                testNoNumbers,
                testRole
            )
        }
    }

    @Test(expected = Exception::class)
    fun `given an empty role throw exception`() {
        runBlocking {
            Mockito.`when`(
                emailValidator.isValidEmail(testEmail)
            ).thenReturn(true)
            Mockito.`when`(
                accountRepository.createUser(
                    anyString(),
                    anyString(),
                    anyString(),
                    anyString()
                )
            )
                .thenReturn(false)
            val result = createNewUserUseCase.createUser(
                testLoginUsername,
                testEmail,
                testPassword,
                ""
            )
            Mockito.verify(accountRepository, Mockito.times(1))
                .createUser(
                    testLoginUsername,
                    testEmail,
                    testPassword,
                    ""
                )
            Mockito.verify(emailValidator, Mockito.times(1))
                .isValidEmail(testEmail)

            Assert.assertNotNull(result)
            Assert.assertFalse(result)
        }
    }

    @Test(expected = Exception::class)
    fun `given an invalid role throw exception`() {
        val unknownRole = "some unknown role"
        runBlocking {
            Mockito.`when`(
                emailValidator.isValidEmail(testEmail)
            ).thenReturn(true)
            Mockito.`when`(
                accountRepository.createUser(
                    anyString(),
                    anyString(),
                    anyString(),
                    anyString()
                )
            )
                .thenReturn(false)
            val result = createNewUserUseCase.createUser(
                testLoginUsername,
                testEmail,
                testPassword,
                unknownRole
            )
            Mockito.verify(accountRepository, Mockito.times(1))
                .createUser(
                    testLoginUsername,
                    testEmail,
                    testPassword,
                    unknownRole
                )
            Mockito.verify(emailValidator, Mockito.times(1))
                .isValidEmail(testEmail)

            Assert.assertNotNull(result)
            Assert.assertFalse(result)
        }
    }

    @Test
    fun `given a String with at least one uppercase return true`() {
        val testCases = arrayOf(
            "Ab",
            "ddsdB",
            "   D   ",
            "    M",
            "24628746O",
            "lalalalallalalalalalalalaalallalalaV"
        )

        for (testCase in testCases) {
            Assert.assertTrue(testCase.containsOneUpperCaseLetter())
        }
    }

    @Test
    fun `given a String without at least one uppercase return false`() {
        val testCases = arrayOf(
            "ab",
            "ddsdb",
            "   d   ",
            "    m",
            "24628746o",
            "lalalalallalalalalalalalaalallalala"
        )

        for (testCase in testCases) {
            Assert.assertFalse(testCase.containsOneUpperCaseLetter())
        }
    }

    @Test
    fun `given a String with at least one lowercase return true`() {
        val testCases = arrayOf(
            "Ab",
            "DKHFJELKb",
            "   b   ",
            "    m",
            "24628746o",
            "KJDSHFKYUEHFKDfDKJFKDFDJ"
        )

        for (testCase in testCases) {
            Assert.assertTrue(testCase.containsOneLowerCaseLetter())
        }
    }

    @Test
    fun `given a String without at least one lowercase return false`() {
        val testCases = arrayOf(
            "AB",
            "DKHFJELKB",
            "   B   ",
            "    M",
            "24628746O",
            "KJDSHFKYUEHFKDFDKJFKDFDJ"
        )

        for (testCase in testCases) {
            Assert.assertFalse(testCase.containsOneLowerCaseLetter())
        }
    }

    @Test
    fun `given a String with at least one dash or underscore return true`() {
        val testCases = arrayOf(
            "Ab___",
            "DKHFJ--ELKb",
            "   -   ",
            "    _",
            "24628746o----------------_______________",
            "_KJDSHFKYUEHFKDfDKJFKDFDJ-"
        )

        for (testCase in testCases) {
            Assert.assertTrue(testCase.containsOneDashOrUnderscore())
        }
    }

    @Test
    fun `given a String without at least one dash or underscore return false`() {
        val testCases = arrayOf(
            "Ab//",
            "DKHFJELKb",
            "   .   ",
            "    .",
            "24628746o************~~~~~~~~~~~`",
            "~KJDSHFKYUEHFKDfDKJFKDFDJ!"
        )

        for (testCase in testCases) {
            Assert.assertFalse(testCase.containsOneDashOrUnderscore())
        }
    }

    @Test
    fun `given a String with at least one number return true`() {
        val testCases = arrayOf(
            "Ab2___",
            "DKH2FJ--ELKb",
            "   0   ",
            "    2",
            "24628746o----------------_______________",
            "_KJDSHFKYUE423HFKDfDKJFKDFDJ-"
        )

        for (testCase in testCases) {
            Assert.assertTrue(testCase.containsAtLeastOneNumber())
        }
    }

    @Test
    fun `given a String without at least one number return false`() {
        val testCases = arrayOf(
            "Ab___",
            "DKHFJ--ELKb",
            "   -   ",
            "    _",
            "o----------------_______________",
            "_KJDSHFKYUEHFKDfDKJFKDFDJ-"
        )

        for (testCase in testCases) {
            Assert.assertFalse(testCase.containsAtLeastOneNumber())
        }
    }
}