/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: UpdateUserUseCaseImplTest.kt
 * Author: sdelic
 *
 * History:  12.4.22. 12:29 created
 */

package com.comtrade.feature_account.use_case.user

import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.contracts.repository.user.IAccountRepository
import com.comtrade.domain.contracts.usecases.account.IUpdateAccountUseCase
import com.comtrade.domain.entities.user.data.UpdateUserRequest
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito

/**
 * Test scenario for the update account implementation detail.
 */
internal class UpdateUserUseCaseImplTest {
    private val testToken = "testToken"

    private lateinit var prefs: IPrefs
    private lateinit var accountRepository: IAccountRepository
    private lateinit var updateAccountUseCase: IUpdateAccountUseCase

    @Before
    fun setUp() {
        prefs = Mockito.mock(IPrefs::class.java)
        accountRepository = Mockito.mock(IAccountRepository::class.java)
        updateAccountUseCase = UpdateUserUseCaseImpl(prefs, accountRepository)
    }

    @Test
    fun `given valid input data when update user then return true`() {
        runBlocking {
            val requestData = getTestUpdateUserRequest()
            Mockito.`when`(prefs.token).thenReturn(testToken)
            Mockito.`when`(accountRepository.updateAccount(testToken, requestData))
                .thenReturn(true)
            val result = updateAccountUseCase.updateAccount(requestData)
            Mockito.verify(prefs, Mockito.times(1)).token
            Mockito.verify(accountRepository, Mockito.times(1))
                .updateAccount(testToken, requestData)
            Assert.assertNotNull(result)
            Assert.assertTrue(result)
        }
    }

    @Test
    fun `given valid input data when update user then return false`() {
        runBlocking {
            val requestData = getTestUpdateUserRequest()
            Mockito.`when`(prefs.token).thenReturn(testToken)
            Mockito.`when`(accountRepository.updateAccount(testToken, requestData))
                .thenReturn(false)
            val result = updateAccountUseCase.updateAccount(requestData)
            Mockito.verify(prefs, Mockito.times(1)).token
            Mockito.verify(accountRepository, Mockito.times(1))
                .updateAccount(testToken, requestData)
            Assert.assertNotNull(result)
            Assert.assertFalse(result)
        }
    }

    @Test(expected = Exception::class)
    fun `given an invalid token throw exception`() {
        runBlocking {
            val requestData = getTestUpdateUserRequest()
            Mockito.`when`(prefs.token).thenReturn("")
            Mockito.`when`(accountRepository.updateAccount(testToken, requestData))
                .thenReturn(false)
            val result = updateAccountUseCase.updateAccount(requestData)
            Mockito.verify(prefs, Mockito.times(1)).token
            Mockito.verify(accountRepository, Mockito.times(1))
                .updateAccount(testToken, requestData)
            Assert.assertNotNull(result)
            Assert.assertFalse(result)
        }
    }

    @Test(expected = Exception::class)
    fun `given an invalid request data throw exception`() {
        runBlocking {
            val requestData = getTestUpdateUserRequest().copy(
                firstName = "",
                lastName = "",
                gender = "",
                address = "",
                email = "",
                phone = "",
            )
            Mockito.`when`(prefs.token).thenReturn(testToken)
            Mockito.`when`(accountRepository.updateAccount(testToken, requestData))
                .thenReturn(false)
            val result = updateAccountUseCase.updateAccount(requestData)
            Mockito.verify(prefs, Mockito.times(1)).token
            Mockito.verify(accountRepository, Mockito.times(1))
                .updateAccount(testToken, requestData)
            Assert.assertNotNull(result)
            Assert.assertFalse(result)
        }
    }


    private fun getTestUpdateUserRequest(): UpdateUserRequest = UpdateUserRequest(
        id = null,
        login = null,
        firstName = "testFirstName",
        lastName = "testLastName",
        gender = "MALE",
        address = "testAddress",
        email = "test@test.com",
        phone = "0123456789",
        friends = null,
    )
}