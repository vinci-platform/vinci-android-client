/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: GetMobileAppSettingsUseCaseImplTest.kt
 * Author: sdelic
 *
 * History:  26.4.22. 10:15 created
 */

package com.comtrade.feature_account.use_case.user

import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.contracts.repository.user.IAccountRepository
import com.comtrade.domain.contracts.usecases.account.IGetMobileAppSettingsUseCase
import com.comtrade.domain.entities.user.account.MobileAppSettingsEntity
import com.comtrade.domain.settings.MobileAppSettings
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.anyLong
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mockito

/**
 * Test scenario for the
 */
internal class GetMobileAppSettingsUseCaseImplTest {
    private val testToken = "testToken"
    private val testUserId = 1L

    private lateinit var prefs: IPrefs
    private lateinit var accountRepository: IAccountRepository
    private lateinit var getMobileAppSettingsUseCase: IGetMobileAppSettingsUseCase

    @Before
    fun setUp() {
        prefs = Mockito.mock(IPrefs::class.java)
        accountRepository = Mockito.mock(IAccountRepository::class.java)
        getMobileAppSettingsUseCase = GetMobileAppSettingsUseCaseImpl(prefs, accountRepository)
    }

    @Test
    fun `given valid input data when get mobile app settings return default step goal and sync interval`() {
        runBlocking {
            Mockito.`when`(prefs.token).thenReturn(testToken)
            Mockito.`when`(prefs.userId).thenReturn(testUserId)
            Mockito.`when`(accountRepository.getMobileAppSettings(anyString(), anyLong()))
                .thenReturn(
                    emptyList()
                )

            val result = getMobileAppSettingsUseCase.getMobileAppSettings()
            Mockito.verify(prefs, Mockito.times(1)).token
            Mockito.verify(accountRepository, Mockito.times(1)).getMobileAppSettings(testToken, 0)

            Assert.assertNotNull(result)
            Assert.assertEquals(MobileAppSettings.DEFAULT_STEP_GOAL, result.stepGoal)
            Assert.assertEquals(MobileAppSettings.DEFAULT_APP_SYNC_INTERVAL, result.syncInterval)
        }
    }

    @Test
    fun `given valid input data when get mobile app settings return step goal and sync interval`() {
        runBlocking {
            Mockito.`when`(prefs.token).thenReturn(testToken)
            Mockito.`when`(prefs.userId).thenReturn(testUserId)
            Mockito.`when`(accountRepository.getMobileAppSettings(anyString(), anyLong()))
                .thenReturn(provideTestMobileAppSettingsResponse())

            val result = getMobileAppSettingsUseCase.getMobileAppSettings()
            Mockito.verify(prefs, Mockito.times(1)).token
            Mockito.verify(accountRepository, Mockito.times(1)).getMobileAppSettings(testToken, 0)

            Assert.assertNotNull(result)
            Assert.assertEquals(100_000, result.syncInterval)
            Assert.assertEquals(100, result.stepGoal)
        }
    }

    @Test(expected = Exception::class)
    fun `given invalid input token when get mobile app settings throw exception`() {
        runBlocking {
            Mockito.`when`(prefs.token).thenReturn("")
            Mockito.`when`(prefs.userId).thenReturn(testUserId)
            Mockito.`when`(accountRepository.getMobileAppSettings(anyString(), anyLong()))
                .thenReturn(provideTestMobileAppSettingsResponse())

            val result = getMobileAppSettingsUseCase.getMobileAppSettings()
            Mockito.verify(prefs, Mockito.times(1)).token
            Mockito.verify(accountRepository, Mockito.times(1)).getMobileAppSettings("", 0)

            Assert.assertNotNull(result)
            Assert.assertEquals(0, result.stepGoal)
            Assert.assertEquals(0, result.syncInterval)
        }
    }

    @Test(expected = Exception::class)
    fun `given invalid input userId when get mobile app settings throw exception`() {
        runBlocking {
            Mockito.`when`(prefs.token).thenReturn(testToken)
            Mockito.`when`(prefs.userId).thenReturn(-1L)
            Mockito.`when`(accountRepository.getMobileAppSettings(anyString(), anyLong()))
                .thenReturn(provideTestMobileAppSettingsResponse())

            val result = getMobileAppSettingsUseCase.getMobileAppSettings(null)
            Mockito.verify(prefs, Mockito.times(1)).token
            Mockito.verify(prefs, Mockito.times(1)).userId
            Mockito.verify(accountRepository, Mockito.times(1)).getMobileAppSettings(testToken, -1L)
            Assert.assertNotNull(result)
            Assert.assertEquals(0, result.stepGoal)
            Assert.assertEquals(0, result.syncInterval)
        }
    }


    private fun provideTestMobileAppSettingsResponse(): List<MobileAppSettingsEntity> {
        val testData = mutableListOf<MobileAppSettingsEntity>()

        for (i in 0 until 10) {
            testData.add(
                if (i % 2 == 0) {
                    MobileAppSettingsEntity(
                        id = i.toLong(),
                        name = "sync_interval",
                        value = "100",
                        userId = 1L
                    )
                } else {
                    MobileAppSettingsEntity(
                        id = i.toLong(),
                        name = "step_goal",
                        value = "100",
                        userId = 1L
                    )
                }
            )
        }

        return testData
    }
}