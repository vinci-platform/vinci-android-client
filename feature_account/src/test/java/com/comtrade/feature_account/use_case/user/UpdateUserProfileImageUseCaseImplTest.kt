/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: UpdateUserProfileImageUseCaseImplTest.kt
 * Author: sdelic
 *
 * History:  28.6.22. 12:34 created
 */

package com.comtrade.feature_account.use_case.user

import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.contracts.repository.user.UserServiceRepository
import com.comtrade.domain.contracts.usecases.account.IUpdateUserProfileImageUseCase
import com.comtrade.domain.entities.user.UserRoles
import com.comtrade.domain.entities.user.account.GetUserEntity
import com.comtrade.domain.entities.user.data.UpdateUserRequest
import com.google.gson.Gson
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import java.io.File

/**
 * Test scenario for uploading a new User profile image use case implementation detail
 */
internal class UpdateUserProfileImageUseCaseImplTest {
    private val testToken = "testToken"
    private val testProfileImageId = 1L
    private lateinit var testFile: File
    private lateinit var prefs: IPrefs
    private lateinit var gson: Gson
    private lateinit var userServiceRepository: UserServiceRepository
    private lateinit var updateUserProfileImageUseCase: IUpdateUserProfileImageUseCase

    @Before
    fun setUp() {
        testFile = File.createTempFile("test", null) //Creates a test file with the .tmp extension
        prefs = Mockito.mock(IPrefs::class.java)
        userServiceRepository = Mockito.mock(UserServiceRepository::class.java)
        gson = Gson()
        updateUserProfileImageUseCase =
            UpdateUserProfileImageUseCaseImpl(prefs, gson, userServiceRepository)
    }

    @After
    fun tearDown() {
        try {
            testFile.delete()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    @Test
    fun `given a valid input file when called return true`() = runBlocking {

        val testUserData = provideTestUserData()
        val testUserJson = gson.toJson(
            UpdateUserRequest(
                testUserData.userId,
                testUserData.login,
                testUserData.firstName,
                testUserData.lastName,
                testUserData.gender,
                testUserData.address,
                testUserData.email,
                testUserData.phone,
                "",
                testUserData.role
            )
        )
        Mockito.`when`(prefs.token).thenReturn(testToken)
        Mockito.`when`(prefs.userProfilePictureId).thenReturn(testProfileImageId)
        Mockito.`when`(userServiceRepository.getUser(testToken)).thenReturn(testUserData)
        Mockito.`when`(
            userServiceRepository.updateUserImage(
                testToken,
                testUserJson,
                testProfileImageId,
                testFile
            )
        ).thenReturn(true)

        val result = updateUserProfileImageUseCase.saveImage(testFile)

        Mockito.verify(prefs, Mockito.times(1)).token
        Mockito.verify(prefs, Mockito.times(1)).userProfilePictureId
        Mockito.verify(userServiceRepository, Mockito.times(1)).getUser(testToken)
        Mockito.verify(userServiceRepository, Mockito.times(1)).updateUserImage(
            testToken,
            testUserJson,
            testProfileImageId,
            testFile
        )

        assertTrue(result)
    }

    @Test
    fun `given a valid input file when called return false`() = runBlocking {

        val testUserData = provideTestUserData()
        val testUserJson = gson.toJson(
            UpdateUserRequest(
                testUserData.userId,
                testUserData.login,
                testUserData.firstName,
                testUserData.lastName,
                testUserData.gender,
                testUserData.address,
                testUserData.email,
                testUserData.phone,
                "",
                testUserData.role
            )
        )
        Mockito.`when`(prefs.token).thenReturn(testToken)
        Mockito.`when`(prefs.userProfilePictureId).thenReturn(testProfileImageId)
        Mockito.`when`(userServiceRepository.getUser(testToken)).thenReturn(testUserData)
        Mockito.`when`(
            userServiceRepository.updateUserImage(
                testToken,
                testUserJson,
                testProfileImageId,
                testFile
            )
        ).thenReturn(false)

        val result = updateUserProfileImageUseCase.saveImage(testFile)

        Mockito.verify(prefs, Mockito.times(1)).token
        Mockito.verify(prefs, Mockito.times(1)).userProfilePictureId
        Mockito.verify(userServiceRepository, Mockito.times(1)).getUser(testToken)
        Mockito.verify(userServiceRepository, Mockito.times(1)).updateUserImage(
            testToken,
            testUserJson,
            testProfileImageId,
            testFile
        )

        assertFalse(result)
    }

    @Test
    fun `given a valid input file when called return false due to not obtaining user information`() =
        runBlocking {


            Mockito.`when`(prefs.token).thenReturn(testToken)
            Mockito.`when`(prefs.userProfilePictureId).thenReturn(testProfileImageId)
            Mockito.`when`(userServiceRepository.getUser(testToken)).thenReturn(null)

            val result = updateUserProfileImageUseCase.saveImage(testFile)

            Mockito.verify(prefs, Mockito.times(1)).token
            Mockito.verify(prefs, Mockito.times(0)).userProfilePictureId
            Mockito.verify(userServiceRepository, Mockito.times(1)).getUser(testToken)

            assertFalse(result)
        }

    @Test(expected = Exception::class)
    fun `given an invalid token input when called throw exception`() = runBlocking {

        val testUserData = provideTestUserData()
        val testUserJson = gson.toJson(
            UpdateUserRequest(
                testUserData.userId,
                testUserData.login,
                testUserData.firstName,
                testUserData.lastName,
                testUserData.gender,
                testUserData.address,
                testUserData.email,
                testUserData.phone,
                "",
                testUserData.role
            )
        )
        Mockito.`when`(prefs.token).thenReturn(null)
        Mockito.`when`(prefs.userProfilePictureId).thenReturn(testProfileImageId)
        Mockito.`when`(userServiceRepository.getUser(testToken)).thenReturn(testUserData)
        Mockito.`when`(
            userServiceRepository.updateUserImage(
                testToken,
                testUserJson,
                testProfileImageId,
                testFile
            )
        ).thenReturn(true)

        val result = updateUserProfileImageUseCase.saveImage(testFile)

        Mockito.verify(prefs, Mockito.times(1)).token
        Mockito.verify(prefs, Mockito.times(1)).userProfilePictureId
        Mockito.verify(userServiceRepository, Mockito.times(1)).getUser(testToken)
        Mockito.verify(userServiceRepository, Mockito.times(1)).updateUserImage(
            testToken,
            testUserJson,
            testProfileImageId,
            testFile
        )

        assertTrue(result)
    }

    @Test(expected = Exception::class)
    fun `given an invalid profile image Id input when called throw exception`() = runBlocking {

        val testUserData = provideTestUserData()
        val testUserJson = gson.toJson(
            UpdateUserRequest(
                testUserData.userId,
                testUserData.login,
                testUserData.firstName,
                testUserData.lastName,
                testUserData.gender,
                testUserData.address,
                testUserData.email,
                testUserData.phone,
                "",
                testUserData.role
            )
        )
        Mockito.`when`(prefs.token).thenReturn(testToken)
        Mockito.`when`(prefs.userProfilePictureId).thenReturn(-1)
        Mockito.`when`(userServiceRepository.getUser(testToken)).thenReturn(testUserData)

        val result = updateUserProfileImageUseCase.saveImage(testFile)

        Mockito.verify(prefs, Mockito.times(1)).token
        Mockito.verify(prefs, Mockito.times(1)).userProfilePictureId
        Mockito.verify(userServiceRepository, Mockito.times(1)).getUser(testToken)
        Mockito.verify(userServiceRepository, Mockito.times(0)).updateUserImage(
            testToken,
            testUserJson,
            testProfileImageId,
            testFile
        )

        assertTrue(result)
    }


    private fun provideTestUserData(): GetUserEntity = GetUserEntity(
        userId = 1L,
        login = "TestUser",
        firstName = "test firstname",
        lastName = "test lastname",
        gender = "test gender",
        email = "test@email.com",
        address = null,
        phone = null,
        role = UserRoles.ROLE_PACIENT.name,
        devices = emptyList()
    )
}