/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: ResetPasswordUseCaseImplTest.kt
 * Author: sdelic
 *
 * History:  7.4.22. 15:51 created
 */

package com.comtrade.feature_account.use_case.password

import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.contracts.repository.user.IAccountRepository
import com.comtrade.domain.contracts.utils.IEmailValidator
import kotlinx.coroutines.runBlocking
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito

/**
 * Test scenario for the rest account password implementation detail.
 */
internal class ResetPasswordUseCaseImplTest {
    private val testToken = "testToken"
    private val testEmail = "testEmail@someemail.com"
    private lateinit var prefs: IPrefs
    private lateinit var emailValidator: IEmailValidator
    private lateinit var accountRepository: IAccountRepository
    private lateinit var resetPasswordUseCaseImpl: ResetPasswordUseCaseImpl

    @Before
    fun setUp() {
        prefs = Mockito.mock(IPrefs::class.java)
        emailValidator = Mockito.mock(IEmailValidator::class.java)
        accountRepository = Mockito.mock(IAccountRepository::class.java)
        resetPasswordUseCaseImpl =
            ResetPasswordUseCaseImpl(prefs, emailValidator, accountRepository)
    }

    @Test
    fun `given a valid email address when reset password return true`() {
        runBlocking {
            Mockito.`when`(prefs.token).thenReturn(testToken)
            Mockito.`when`(emailValidator.isValidEmail(testEmail)).thenReturn(true)
            Mockito.`when`(accountRepository.resetPassword(testToken, testEmail)).thenReturn(true)
            val result = resetPasswordUseCaseImpl.resetPassword(testEmail)
            Mockito.verify(prefs, Mockito.times(1)).token
            Mockito.verify(emailValidator, Mockito.times(1)).isValidEmail(testEmail)
            Mockito.verify(accountRepository, Mockito.times(1)).resetPassword(testToken, testEmail)
            assertNotNull(result)
            assertTrue(result)
        }
    }

    @Test
    fun `given a valid email address when reset password return false`() {
        runBlocking {
            Mockito.`when`(prefs.token).thenReturn(testToken)
            Mockito.`when`(emailValidator.isValidEmail(testEmail)).thenReturn(true)
            Mockito.`when`(accountRepository.resetPassword(testToken, testEmail))
                .thenReturn(false)

            val result = resetPasswordUseCaseImpl.resetPassword(testEmail)
            Mockito.verify(prefs, Mockito.times(1)).token
            Mockito.verify(emailValidator, Mockito.times(1)).isValidEmail(testEmail)
            Mockito.verify(accountRepository, Mockito.times(1)).resetPassword(testToken, testEmail)

            assertNotNull(result)
            assertFalse(result)
        }
    }

    @Test(expected = Exception::class)
    fun `given an invalid email address when throw exception`() {
        val invalidEmail = "not an email"
        runBlocking {
            Mockito.`when`(prefs.token).thenReturn(testToken)
            Mockito.`when`(emailValidator.isValidEmail(invalidEmail)).thenReturn(false)
            val result = resetPasswordUseCaseImpl.resetPassword(invalidEmail)
            Mockito.verify(prefs, Mockito.times(1)).token
            Mockito.verify(emailValidator, Mockito.times(1)).isValidEmail(invalidEmail)

            assertNotNull(result)
            assertTrue(result)
        }
    }

    @Test(expected = Exception::class)
    fun `given an invalid token throw exception`() {
        runBlocking {
            Mockito.`when`(prefs.token).thenReturn(null)
            Mockito.`when`(emailValidator.isValidEmail(testEmail)).thenReturn(false)
            val result = resetPasswordUseCaseImpl.resetPassword(testEmail)
            Mockito.verify(prefs, Mockito.times(1)).token
            Mockito.verify(emailValidator, Mockito.times(1)).isValidEmail(testEmail)

            assertNotNull(result)
            assertTrue(result)
        }
    }


}