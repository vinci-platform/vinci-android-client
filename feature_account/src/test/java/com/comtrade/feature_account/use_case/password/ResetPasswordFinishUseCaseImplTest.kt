/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: ResetPasswordFinishUseCaseImplTest.kt
 * Author: sdelic
 *
 * History:  11.4.22. 10:24 created
 */

package com.comtrade.feature_account.use_case.password

import com.comtrade.domain.contracts.repository.user.IAccountRepository
import com.comtrade.domain.contracts.usecases.account.IResetPasswordFinishUseCase
import com.comtrade.domain.entities.user.account.ResetPasswordFinishRequest
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.*

/**
 * Test scenario for the second step in the User reset password use case implementation.
 */
internal class ResetPasswordFinishUseCaseImplTest {
    private val testActivate = true
    private val testActivationKey = "testActivationKey"
    private val testNewPassword = "testNewPassword"
    private lateinit var accountRepository: IAccountRepository
    private lateinit var resetPasswordFinishUseCase: IResetPasswordFinishUseCase

    @Before
    fun setUp() {
        accountRepository = mock(IAccountRepository::class.java)
        resetPasswordFinishUseCase = ResetPasswordFinishUseCaseImpl(accountRepository)
    }

    @Test
    fun `given a valid request when reset password finish return true`() {
        runBlocking {
            `when`(
                accountRepository.resetPasswordFinish(
                    ResetPasswordFinishRequest(
                        testActivate,
                        testActivationKey,
                        testNewPassword
                    )
                )
            ).thenReturn(true)
            val result =
                resetPasswordFinishUseCase.resetPasswordFinish(
                    ResetPasswordFinishRequest(
                        testActivate,
                        testActivationKey,
                        testNewPassword
                    )
                )
            Assert.assertNotNull(result)
            Assert.assertTrue(result)
        }
    }

    @Test(expected = Exception::class)
    fun `given an invalid activation key throw exception`() {
        runBlocking {
            `when`(
                accountRepository.resetPasswordFinish(
                    ResetPasswordFinishRequest(
                        anyBoolean(),
                        anyString(),
                        anyString()
                    )
                )
            ).thenReturn(true)
            val result =
                resetPasswordFinishUseCase.resetPasswordFinish(
                    ResetPasswordFinishRequest(
                        testActivate,
                        "",
                        testNewPassword
                    )
                )
            Assert.assertNotNull(result)
            Assert.assertTrue(result)
        }
    }

    @Test(expected = Exception::class)
    fun `given an invalid password throw exception`() {
        runBlocking {
            `when`(
                accountRepository.resetPasswordFinish(
                    ResetPasswordFinishRequest(
                        anyBoolean(),
                        anyString(),
                        anyString()
                    )
                )
            ).thenReturn(true)
            val result =
                resetPasswordFinishUseCase.resetPasswordFinish(
                    ResetPasswordFinishRequest(
                        testActivate,
                        testActivationKey,
                        ""
                    )
                )
            Assert.assertNotNull(result)
            Assert.assertTrue(result)
        }
    }

    @Test(expected = Exception::class)
    fun `given valid request payload when password reset return false`() {
        runBlocking {
            `when`(
                accountRepository.resetPasswordFinish(
                    ResetPasswordFinishRequest(
                        anyBoolean(),
                        anyString(),
                        anyString()
                    )
                )
            ).thenReturn(false)
            val result =
                resetPasswordFinishUseCase.resetPasswordFinish(
                    ResetPasswordFinishRequest(
                        testActivate,
                        testActivationKey,
                        ""
                    )
                )
            Assert.assertNotNull(result)
            Assert.assertFalse(result)
        }
    }

}