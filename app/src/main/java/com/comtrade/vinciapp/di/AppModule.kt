package com.comtrade.vinciapp.di

import android.app.Application
import android.os.Environment
import com.comtrade.data.di.AppDataComponent
import com.comtrade.domain.contracts.utils.IEmailValidator
import com.comtrade.domain.di.AppQualifiers
import com.comtrade.feature_events.di.EventsComponent
import com.comtrade.vinciapp.BuildConfig
import com.comtrade.vinciapp.util.EmailValidator
import dagger.Module
import dagger.Provides
import java.io.File
import javax.inject.Named

/**
 * Application scoped Dagger DI module.
 */
@Module(subcomponents = [AppDataComponent::class, EventsComponent::class])
internal class AppModule(private val application: Application) {

    @ApplicationScope
    @Provides
    fun provideApplicationContext(): Application = application

    @ApplicationScope
    @Provides
    @Named(AppQualifiers.ROOT_IMAGE_DIRECTORY)
    fun provideRootProfileImageDirectory(application: Application): String =
        application.getExternalFilesDir(
            Environment.DIRECTORY_PICTURES
        )
            .toString() + File.separator + "vinci"

    @ApplicationScope
    @Provides
    @Named(AppQualifiers.APP_BASE_URL)
    fun provideBaseUrlAddress(): String = BuildConfig.BASE_URL

    @ApplicationScope
    @Provides
    @Named(AppQualifiers.TERMS_AND_CONDITIONS_FILE_LOCATION)
    fun provideTermsAndConditionsFileLocation(): String =
        "file:///android_asset/vInci_Terms_And_Conditions.html"

    @ApplicationScope
    @Provides
    fun provideEmailValidator(): IEmailValidator = EmailValidator
}