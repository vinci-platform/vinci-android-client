package com.comtrade.vinciapp.di

import android.app.Application
import com.comtrade.data.di.AppDataComponent
import com.comtrade.data.di.AppDataModule
import com.comtrade.data.stepDatabase.StepDatabase
import com.comtrade.data.stepDatabase.SurveyDao
import com.comtrade.domain.contracts.repository.IAppRepository
import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.contracts.repository.user.IAccountRepository
import com.comtrade.domain.contracts.repository.user.UserServiceRepository
import com.comtrade.domain.contracts.usecases.survey.ISaveSurveyDataUseCase
import com.comtrade.domain.di.AppQualifiers
import com.comtrade.feature_account.di.AccountComponent
import com.comtrade.feature_devices.di.DevicesComponent
import com.comtrade.feature_events.di.EventsComponent
import com.comtrade.feature_friends.di.FriendsComponent
import com.comtrade.feature_health_records.di.HealthRecordsComponent
import com.comtrade.feature_questionnaire.di.QuestionnaireModule
import com.comtrade.feature_questionnaire.survey.ui.FeelingsSurveyViewModelFactory
import com.comtrade.feature_questionnaire.survey.ui.SurveyViewModelFactory
import com.comtrade.feature_questionnaire.survey.ui.SyncSurveyFromVinciViewModelFactory
import com.comtrade.feature_questionnaire.survey.ui.SyncSurveyToVinciViewModelFactory
import com.comtrade.feature_steps.di.StepsComponent
import com.comtrade.vinciapp.connection.ConnectionMonitorViewModelFactory
import com.comtrade.vinciapp.receiver.AlarmReceiver
import com.comtrade.vinciapp.receiver.SnoozeReceiver
import com.comtrade.vinciapp.view.activity.main.view.MainActivity
import com.comtrade.vinciapp.view.activity.splash.view.SplashActivity
import com.comtrade.vinciapp.view.widget.addNewDevice.view.AddNewDeviceFragment
import com.comtrade.vinciapp.view.widget.chooseFriends.view.ChooseFriendsFragment
import com.comtrade.vinciapp.view.widget.cognitiveGames.view.CognitiveGamesFragment
import com.comtrade.vinciapp.view.widget.connectedDeviceList.view.ConnectedDeviceListFragment
import com.comtrade.vinciapp.view.widget.devicesSetup.view.DevicesSetupFragment
import com.comtrade.vinciapp.view.widget.editProfileAccount.view.EditProfileAccountFragment
import com.comtrade.vinciapp.view.widget.fitBitScreen.view.FitBitScreenFragment
import com.comtrade.vinciapp.view.widget.friendsView.view.FriendsViewFragment
import com.comtrade.vinciapp.view.widget.historyGraph.view.HistoryGraphFragment
import com.comtrade.vinciapp.view.widget.inviteFriends.view.InviteFriendsFragment
import com.comtrade.vinciapp.view.widget.login.view.LoginFragment
import com.comtrade.vinciapp.view.widget.mainScreen.view.MainScreenFragment
import com.comtrade.vinciapp.view.widget.nextEventList.view.NextEventListFragment
import com.comtrade.vinciapp.view.widget.notificationsSettings.view.NotificationsSettingsFragment
import com.comtrade.vinciapp.view.widget.outdoorActivities.view.OutdoorActivitiesFragment
import com.comtrade.vinciapp.view.widget.physicalActivities.view.PhysicalActivitiesFragment
import com.comtrade.vinciapp.view.widget.profilePicture.view.ProfilePictureFragment
import com.comtrade.vinciapp.view.widget.questionnaireElderly.view.ElderlyQuestionnaireFragment
import com.comtrade.vinciapp.view.widget.questionnaireIPA.view.IPAQuestionnaireFragment
import com.comtrade.vinciapp.view.widget.questionnaireSurvey.view.SurveyQuestionnaireFragment
import com.comtrade.vinciapp.view.widget.questionnaireUserNeeds.view.UserNeedsQuestionnaireFragment
import com.comtrade.vinciapp.view.widget.settingEmergencyNumber.view.SettingEmergencyNumberFragment
import com.comtrade.vinciapp.view.widget.terms.view.TermsFragment
import com.comtrade.vinciapp.view.widget.welcome.view.WelcomeFragment
import com.google.gson.Gson
import com.vinci.feature_sync.di.SyncServiceComponent
import dagger.Component
import javax.inject.Named

@Component(modules = [AppModule::class, AppDataModule::class, QuestionnaireModule::class])
@ApplicationScope
interface AppComponent {

    val appDataComponentBuilder: AppDataComponent.Builder
    val accountComponentFactory: AccountComponent.Factory
    val eventsComponentFactory: EventsComponent.Factory
    val healthRecordsComponentFactory: HealthRecordsComponent.Factory
    val devicesComponentFactory: DevicesComponent.Factory
    val stepsComponentFactory: StepsComponent.Factory
    val friendsComponentFactory: FriendsComponent.Factory
    val syncComponentBuilder: SyncServiceComponent.Builder

    fun provideFeelingsSurveyViewModelFactory(): FeelingsSurveyViewModelFactory
    fun provideSyncSurveyToVinciViewModelFactory(): SyncSurveyToVinciViewModelFactory
    fun provideSyncSurveyFromVinciViewModelFactory(): SyncSurveyFromVinciViewModelFactory
    fun provideSurveyViewModelFactory(): SurveyViewModelFactory
    fun provideConnectionMonitorViewModelFactory(): ConnectionMonitorViewModelFactory

    fun provideApplicationContext(): Application

    @Named(AppQualifiers.ROOT_IMAGE_DIRECTORY)
    fun provideRootProfileImageDirectory(): String

    @Named(AppQualifiers.APP_BASE_URL)
    fun provideBaseUrlAddress(): String

    @Named(AppQualifiers.TERMS_AND_CONDITIONS_FILE_LOCATION)
    fun provideTermsAndConditionsFileLocation(): String

    fun provideAppPrefs(): IPrefs

    fun provideGson(): Gson

    fun provideStepDatabase(): StepDatabase

    fun provideSurveyDao(): SurveyDao

    fun provideAppRepository(): IAppRepository

    fun provideAccountRepository(): IAccountRepository

    fun provideSaveSurveyUseCase(): ISaveSurveyDataUseCase

    fun provideUserServiceRepository(): UserServiceRepository

    //Activities
    fun inject(splashActivity: SplashActivity)
    fun inject(splashActivity: MainActivity)

    //Fragments
    fun inject(baseFragment: WelcomeFragment)
    fun inject(baseFragment: LoginFragment)
    fun inject(baseFragment: TermsFragment)
    fun inject(baseFragment: EditProfileAccountFragment)
    fun inject(baseFragment: MainScreenFragment)
    fun inject(baseFragment: SettingEmergencyNumberFragment)
    fun inject(baseFragment: NextEventListFragment)
    fun inject(baseFragment: CognitiveGamesFragment)
    fun inject(baseFragment: HistoryGraphFragment)
    fun inject(baseFragment: OutdoorActivitiesFragment)
    fun inject(baseFragment: FriendsViewFragment)
    fun inject(baseFragment: ChooseFriendsFragment)
    fun inject(baseFragment: PhysicalActivitiesFragment)
    fun inject(baseFragment: ConnectedDeviceListFragment)
    fun inject(baseFragment: FitBitScreenFragment)
    fun inject(baseFragment: AddNewDeviceFragment)
    fun inject(baseFragment: DevicesSetupFragment)
    fun inject(baseFragment: ProfilePictureFragment)
    fun inject(baseFragment: NotificationsSettingsFragment)
    fun inject(baseFragment: InviteFriendsFragment)
    fun inject(baseFragment: SurveyQuestionnaireFragment)
    fun inject(baseFragment: ElderlyQuestionnaireFragment)
    fun inject(baseFragment: UserNeedsQuestionnaireFragment)
    fun inject(baseFragment: IPAQuestionnaireFragment)

    //Broadcast receivers
    fun inject(broadcastReceiver: AlarmReceiver)
    fun inject(broadcastReceiver: SnoozeReceiver)
}