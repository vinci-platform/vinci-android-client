package com.comtrade.vinciapp.di

import javax.inject.Scope

/**
 * Custom application Dagger DI scope.
 */
@Scope
@Retention
internal annotation class ApplicationScope