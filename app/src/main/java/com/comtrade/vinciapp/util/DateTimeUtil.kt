/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: DateTimeUtil.kt
 * Author: sdelic
 *
 * History:  24.3.22. 11:58 created
 */

package com.comtrade.vinciapp.util

import com.comtrade.domain.time.AppDateTimeConstants
import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter

/**
 * Obtain the formatted date and time value for the supplied instant value.
 * @param instant The required instant value to be formatted.
 * @param dateTimeFormat The required format. The default is the app display date time format.
 * @return Formatted date time value or empty string in case of formatting exception being thrown.
 */
internal fun getFormattedTimeFromInstant(
    instant: Instant,
    dateTimeFormat: String = AppDateTimeConstants.DISPLAY_DATE_TIME_FORMAT
): String {
    val localDateTime = LocalDateTime.ofInstant(
        instant,
        ZoneId.of(AppDateTimeConstants.DISPLAY_ZONE_ID)
    )
    return try {
        DateTimeFormatter.ofPattern(dateTimeFormat)
            .format(localDateTime)
    } catch (exception: Exception) {
        exception.printStackTrace()
        ""
    }
}
