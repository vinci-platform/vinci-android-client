/*
 * Copyright (c) 2020, Comtrade
 *  All rights reserved.
 *
 *  Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of the <organization> nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *   DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  Package:
 *  File: NotificationUtils.kt
 *  Author:
 *
 *  Description:
 *
 *  History: 8/25/20 12:52 PM <name>  Initial code
 */

package com.comtrade.vinciapp.util

import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Build
import androidx.core.app.NotificationCompat
import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.receiver.AlarmReceiver
import com.comtrade.vinciapp.receiver.SnoozeReceiver
import com.comtrade.vinciapp.view.activity.main.view.MainActivity

fun NotificationManager.sendNotification(
    messageBody: String?,
    applicationContext: Context,
    minSnooze: Int,
    nextFragment: String?,
    prefs: IPrefs
) {
    val contentIntent = Intent(applicationContext, MainActivity::class.java)
    val notificationId = prefs.notificationId
    val requestCode = prefs.notificationRequestCode
    contentIntent.putExtra(AlarmReceiver.NEXT_FRAGMENT_KEY, nextFragment)
    val contentPendingIntent = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
        PendingIntent.getActivity(
            applicationContext,
            notificationId,
            contentIntent,
            PendingIntent.FLAG_IMMUTABLE
        )
    } else {
        PendingIntent.getActivity(
            applicationContext,
            notificationId,
            contentIntent,
            PendingIntent.FLAG_UPDATE_CURRENT
        )
    }

    val vinciLogo = BitmapFactory.decodeResource(
        applicationContext.resources,
        R.drawable.ic_vinci_logo
    )

    val bigPicStyle = NotificationCompat.BigPictureStyle()
        .bigPicture(vinciLogo)
        .bigLargeIcon(null)

    val snoozeIntent = Intent(applicationContext, SnoozeReceiver::class.java)
    snoozeIntent.putExtra(SnoozeReceiver.SNOOZE_MIN_KEY, minSnooze)
    val snoozePendingIntent: PendingIntent = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
        PendingIntent.getBroadcast(
            applicationContext,
            requestCode,
            snoozeIntent,
            PendingIntent.FLAG_IMMUTABLE
        )
    } else {
        PendingIntent.getBroadcast(
            applicationContext,
            requestCode,
            snoozeIntent,
            PendingIntent.FLAG_UPDATE_CURRENT
        )
    }

    val builder = NotificationCompat.Builder(
        applicationContext,
        applicationContext.getString(R.string.notification_channel_id)
    )
        .setSmallIcon(R.drawable.ic_vinci_logo)
        .setContentTitle(
            applicationContext
                .getString(R.string.notification_title)
        )
        .setContentText(messageBody)
        .setContentIntent(contentPendingIntent)
        .setAutoCancel(true)
        .setStyle(bigPicStyle)
        .setLargeIcon(vinciLogo)
        .addAction(
            R.drawable.ic_vinci_logo,
            applicationContext.getString(R.string.notification_snooze_button_text),
            snoozePendingIntent
        )

        .setPriority(NotificationCompat.PRIORITY_HIGH)
    notify(notificationId, builder.build())

    prefs.notificationId = notificationId + 1
    prefs.notificationRequestCode = requestCode + 1
}

fun NotificationManager.cancelNotifications() {
    cancelAll()
}