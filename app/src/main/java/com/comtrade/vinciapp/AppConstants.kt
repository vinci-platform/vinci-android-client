/*
 *  Copyright (c) 2020, Comtrade
 *  All rights reserved.
 *
 *  Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of the <organization> nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *   DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  Package: com.comtrade.vinciapp
 *  File: AppConstants.kt
 *  Author: Milenko Bojanic <milenko.bojanic@comtrade.com>
 *
 *  Description: application constants
 *
 *  History: 8/11/20 Milenko  Initial code
 *
 *
 */

package com.comtrade.vinciapp

object AppConstants {

    const val SMS_TO = "smsto:"
    const val SMS_BODY = "sms_body"
    const val CONTACTS_EXTRA = "contacts_extra"
    const val MAIL_TO = "mailto:"
    const val MAIL_SUBJECT = "VinciApp"
    const val MAIL_BODY = "Join VinciApp!"
    const val CAREGIVER = "caregiver"
    const val EMPTY_STRING = ""

    const val CONNECTED = "Connected"
    const val DISCONNECTED = "Disconnected"
    const val AVAILABLE = "Available"

    const val CLIENT_ID = "22BRL6"
    const val CLIENT_SECRET = "b90ab93a1d35dad710c0bbce2d94fdcb"
    const val CALLBACK_URL = "vinciapp://callback"
    const val FITBIT_BASE_URL = "https://api.fitbit.com"
    const val AUTH_CODE = "authorization_code"
    const val TOKEN_EXP_TIME = "604800"
    const val TOKEN_TYPE_BASIC = "Basic"
    const val TOKEN_TYPE_BEARER = "Bearer"
    const val STEP_COUNTER = "Step counter"
}
