/*
 *  Copyright (c) 2020, Comtrade
 *  All rights reserved.
 *
 *  Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of the <organization> nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *   DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  Package: com.comtrade.vinciapp.view.widget.questionnaireSurvey.view.questionViews
 *  File: QuestionnaireSurveyGradationQuestion.kt
 *  Author: Milenko Bojanić <milenko.bojanic@comtrade.com>
 *
 *  Description: Fragment screen for QOL questionnaire gradation questions
 *
 *  History: 8/11/20 Kosta  Initial code
 *
 *
 */

package com.comtrade.vinciapp.view.widget.questionnaireUserNeeds.view.questionViews

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.RadioButton
import androidx.fragment.app.activityViewModels
import com.comtrade.feature_questionnaire.survey.ui.SurveyViewModel
import com.comtrade.vinciapp.VinciApp
import com.comtrade.vinciapp.base.BaseFragment
import com.comtrade.vinciapp.databinding.ViewQuestionnaireRadioButtonQuestionBinding
import com.comtrade.vinciapp.view.widget.questionnaireSurvey.model.SurveyQuestionnaireItem

class QuestionnaireRadioButtonQuestion : BaseFragment(), View.OnClickListener {

    private var _binding: ViewQuestionnaireRadioButtonQuestionBinding? = null
    private val binding get() = _binding!!

    private var questionId: String? = ""
    private var questionNumber: String? = ""
    var question: String? = ""
    var order: Boolean? = true
    var answers: Array<String> = emptyArray()

    private val surveyViewModel: SurveyViewModel by activityViewModels {
        (requireContext().applicationContext as VinciApp).getAppComponent()
            .provideSurveyViewModelFactory()
    }

    companion object {
        const val TAG: String = "QuestionnaireRadioButtonQuestion"

        const val QUESTION_ID_KEY: String = "question_id"
        const val QUESTION_NUMBER_KEY: String = "question_number"
        const val QUESTION_KEY: String = "question"
        const val ORDER_KEY: String = "order"
        const val ANSWERS_KEY: String = "answers"

        const val ZERO = 0
        const val ONE: Int = 1
        const val TWO: Int = 2
        const val THREE: Int = 3
        const val FOUR: Int = 4
        const val FIVE: Int = 5

        fun newInstance(questionItem: SurveyQuestionnaireItem): QuestionnaireRadioButtonQuestion {
            val fragment = QuestionnaireRadioButtonQuestion()
            val args = Bundle()
            args.putString(QUESTION_ID_KEY, questionItem.questionId)
            args.putString(QUESTION_NUMBER_KEY, questionItem.questionNumber)
            args.putString(QUESTION_KEY, questionItem.question)
            args.putBoolean(ORDER_KEY, questionItem.order)
            args.putStringArray(ANSWERS_KEY, questionItem.answers)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding =
            ViewQuestionnaireRadioButtonQuestionBinding.inflate(
                inflater,
                container,
                false
            )
        questionId = arguments?.getString(QUESTION_ID_KEY)
        questionNumber = arguments?.getString(QUESTION_NUMBER_KEY)
        question = arguments?.getString(QUESTION_KEY)
        order = arguments?.getBoolean(ORDER_KEY)
        answers = arguments?.getStringArray(ANSWERS_KEY) as Array<String>
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.viewQuestionnaireRadioButtonQuestionNumber.text = questionNumber
        binding.viewQuestionnaireRadioButtonQuestionDescription.text = question
        setAnswerTexts()
        setListeners()
    }

    // TODO: Simplify into a loop
    private fun setAnswerTexts() {
        if (answers.isNotEmpty()) {
            binding.viewQuestionnaireScreenRadioButtonOne.text = answers[0]
            binding.viewQuestionnaireScreenRadioButtonOne.visibility = VISIBLE
        } else {
            binding.viewQuestionnaireScreenRadioButtonOne.visibility = GONE
        }
        if (answers.size > 1) {
            binding.viewQuestionnaireScreenRadioButtonTwo.text = answers[1]
            binding.viewQuestionnaireScreenRadioButtonTwo.visibility = VISIBLE
        } else {
            binding.viewQuestionnaireScreenRadioButtonTwo.visibility = GONE
        }
        if (answers.size > 2) {
            binding.viewQuestionnaireScreenRadioButtonThree.text = answers[2]
            binding.viewQuestionnaireScreenRadioButtonThree.visibility = VISIBLE
        } else {
            binding.viewQuestionnaireScreenRadioButtonThree.visibility = GONE
        }
        if (answers.size > 3) {
            binding.viewQuestionnaireScreenRadioButtonFour.text = answers[3]
            binding.viewQuestionnaireScreenRadioButtonFour.visibility = VISIBLE
        } else {
            binding.viewQuestionnaireScreenRadioButtonFour.visibility = GONE
        }
        if (answers.size > 4) {
            binding.viewQuestionnaireScreenRadioButtonFive.text = answers[4]
            binding.viewQuestionnaireScreenRadioButtonFive.visibility = VISIBLE
        } else {
            binding.viewQuestionnaireScreenRadioButtonFive.visibility = GONE
        }
        if (answers.size > 5) {
            binding.viewQuestionnaireScreenRadioButtonSix.text = answers[5]
            binding.viewQuestionnaireScreenRadioButtonSix.visibility = VISIBLE
        } else {
            binding.viewQuestionnaireScreenRadioButtonSix.visibility = GONE
        }
    }

    private fun setListeners() {
        binding.viewQuestionnaireScreenRadioButtonOne.setOnClickListener(this)
        binding.viewQuestionnaireScreenRadioButtonTwo.setOnClickListener(this)
        binding.viewQuestionnaireScreenRadioButtonThree.setOnClickListener(this)
        binding.viewQuestionnaireScreenRadioButtonFour.setOnClickListener(this)
        binding.viewQuestionnaireScreenRadioButtonFive.setOnClickListener(this)
        binding.viewQuestionnaireScreenRadioButtonSix.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
        if (!(view as RadioButton).isChecked) {
            surveyViewModel.questionnaireUserNeedsAnswers!![
                questionId
                    ?: ""
            ] = ""
        } else {
            when (view) {
                binding.viewQuestionnaireScreenRadioButtonOne -> {
                    surveyViewModel.questionnaireUserNeedsAnswers!![
                        questionId
                            ?: ""
                    ] = "1"
                }
                binding.viewQuestionnaireScreenRadioButtonTwo -> {
                    surveyViewModel.questionnaireUserNeedsAnswers!![
                        questionId
                            ?: ""
                    ] = "2"
                }
                binding.viewQuestionnaireScreenRadioButtonThree -> {
                    surveyViewModel.questionnaireUserNeedsAnswers!![
                        questionId
                            ?: ""
                    ] = "3"
                }
                binding.viewQuestionnaireScreenRadioButtonFour -> {
                    surveyViewModel.questionnaireUserNeedsAnswers!![
                        questionId
                            ?: ""
                    ] = "4"
                }
                binding.viewQuestionnaireScreenRadioButtonFive -> {
                    surveyViewModel.questionnaireUserNeedsAnswers!![
                        questionId
                            ?: ""
                    ] = "5"
                }
                binding.viewQuestionnaireScreenRadioButtonSix -> {
                    surveyViewModel.questionnaireUserNeedsAnswers!![
                        questionId
                            ?: ""
                    ] = "6"
                }
            }
        }
    }
}
