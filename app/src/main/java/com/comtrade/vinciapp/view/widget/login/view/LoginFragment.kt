/*
 *  Copyright (c) 2020, Comtrade
 *  All rights reserved.
 *
 *  Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of the <organization> nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *   DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  Package: com.comtrade.vinciapp.view.widget.login.view
 *  File: LoginFragment.kt
 *  Author: Milenko Bojanic <milenko.bojanic@comtrade.com>
 *
 *  Description: Fragment screen for login flow
 *
 *  History: 8/11/20 Milenko  Initial code
 *
 *
 */

package com.comtrade.vinciapp.view.widget.login.view

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.di.AppQualifiers
import com.comtrade.feature_account.ui.auth.UserSignInViewModel
import com.comtrade.feature_account.ui.state.APICallSuccessUIState
import com.comtrade.feature_account.ui.user.GetUserViewModel
import com.comtrade.feature_account.ui.user.RefreshUserDataViewModel
import com.comtrade.vinciapp.AppConstants
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.VinciApp
import com.comtrade.vinciapp.base.BaseFragment
import com.comtrade.vinciapp.databinding.ElevatedProgressViewBinding
import com.comtrade.vinciapp.databinding.FragmentLoginBinding
import com.comtrade.vinciapp.util.AlertDialogUtils
import com.comtrade.vinciapp.util.BiggerDotPasswordTransformationMethod
import com.comtrade.vinciapp.util.ViewUtils
import com.comtrade.vinciapp.view.activity.caregiver.view.CaregiverActivity
import com.comtrade.vinciapp.view.activity.main.view.MainActivity
import com.comtrade.vinciapp.view.widget.login.core.LoginPresenter
import com.comtrade.vinciapp.view.widget.login.core.contract.LoginContract
import com.comtrade.vinciapp.view.widget.questionnaireSurvey.view.SurveyQuestionnaireFragment
import java.util.*
import javax.inject.Inject
import javax.inject.Named

@SuppressLint("VisibleForTests")
class LoginFragment : BaseFragment(), LoginContract.View, View.OnClickListener {

    private var _binding: FragmentLoginBinding? = null
    private val binding get() = _binding!!

    private var _bindingElevatedView: ElevatedProgressViewBinding? = null
    private val bindingElevatedView get() = _bindingElevatedView!!

    @Inject
    lateinit var prefs: IPrefs

    @Inject
    @Named(AppQualifiers.ROOT_IMAGE_DIRECTORY)
    lateinit var directory: String

    private val presenter: LoginContract.Presenter by lazy {
        LoginPresenter()
    }

    private val signInViewModel: UserSignInViewModel by viewModels {
        (requireContext().applicationContext as VinciApp).getAccountComponent()
            .provideUserSignInViewModelFactory()
    }

    private val getUserViewModel: GetUserViewModel by viewModels {
        (requireContext().applicationContext as VinciApp).getAccountComponent()
            .provideGetUserViewModelFactory()
    }

    private val refreshUserDataViewModel: RefreshUserDataViewModel by viewModels {
        (requireContext().applicationContext as VinciApp).getAccountComponent()
            .provideRefreshUserDataViewModelFactory()
    }

    companion object {
        const val TAG: String = "LoginFragment"

        fun newInstance(): LoginFragment {
            return LoginFragment()
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (context.applicationContext as VinciApp).getAppComponent()
            .inject(this@LoginFragment)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentLoginBinding.inflate(inflater, container, false)
        _bindingElevatedView = ElevatedProgressViewBinding.bind(binding.root)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeSignIn()
        observeUserData()
        observeUserRefreshData()
        setListeners()
        createChannel(
            getString(R.string.notification_channel_id),
            getString(R.string.notification_channel_name)
        )
        binding.loginFragmentPasswordEditText.transformationMethod =
            BiggerDotPasswordTransformationMethod()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun observeSignIn() {
        signInViewModel.signInEventObservable.observe(viewLifecycleOwner) {
            when (it) {
                APICallSuccessUIState.DefaultState -> {
                    binding.loginFragmentUsernameEditText.text = null
                    binding.loginFragmentPasswordEditText.text = null
                }
                is APICallSuccessUIState.OnError -> {
                    hideProgressBar()

                    prefs.connectionStatus =
                        getString(R.string.connection_status_authenticate_error, it)
                    prefs.connectionError =
                        prefs.connectionError + "\n" + prefs.connectionStatus

                    it.message?.let { message ->
                        showUnexpectedError(message)
                    }
                    signInViewModel.resetState()
                }
                APICallSuccessUIState.OnLoading -> {
                    prefs.connectionError = ""
                    prefs.connectionStatus = getString(R.string.connection_status_waiting_for_login)
                    showProgressBar()
                }
                is APICallSuccessUIState.OnSuccess -> {
                    hideProgressBar()

                    prefs.connectionStatus = getString(R.string.connection_status_login_successful)

                    setupSurveyNotification()
                    getUserViewModel.getUser()
                    signInViewModel.resetState()
                }
            }
        }
    }

    private fun observeUserData() {
        getUserViewModel.userState.observe(viewLifecycleOwner) {
            when (it) {
                APICallSuccessUIState.DefaultState -> {}
                is APICallSuccessUIState.OnError -> {
                    hideProgressBar()
                    it.message?.let { message ->
                        showUnexpectedError(message)
                    }
                    getUserViewModel.resetState()
                }
                APICallSuccessUIState.OnLoading -> {
                    showProgressBar()
                }
                is APICallSuccessUIState.OnSuccess -> {
                    hideProgressBar()
                    refreshUserDataViewModel.refreshUserData(it.data, directory)
                    getUserViewModel.resetState()
                }
            }
        }
    }

    private fun observeUserRefreshData() {
        refreshUserDataViewModel.refreshUserDataObserve.observe(viewLifecycleOwner) {
            when (it) {
                APICallSuccessUIState.DefaultState -> {}
                is APICallSuccessUIState.OnError -> {
                    hideProgressBar()
                    it.message?.let { message ->
                        prefs.connectionError =
                            prefs.connectionError + "\n" + getString(
                            R.string.get_user_extra_error,
                            message
                        )
                        showHttpError(message)
                    }
                    refreshUserDataViewModel.resetState()
                }
                APICallSuccessUIState.OnLoading -> {
                    showProgressBar()
                }
                is APICallSuccessUIState.OnSuccess -> {
                    hideProgressBar()
                    if (!it.data) {
                        showUnexpectedError(getString(R.string.login_screen_user_data_error))
                    } else {
                        openMainScreen()
                    }
                    refreshUserDataViewModel.resetState()
                }
            }
        }
    }

    private fun setListeners() {
        binding.loginFragmentLoginButton.setOnClickListener(this)
        binding.loginFragmentForgotPasswordTextView.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
        when (view) {
            binding.loginFragmentLoginButton -> {
                loginUser()
            }

            binding.loginFragmentForgotPasswordTextView -> {
                presenter.openForgotPasswordScreen(requireActivity().supportFragmentManager)
            }
        }
    }

    private fun loginUser() {
        signInViewModel.signIn(
            binding.loginFragmentUsernameEditText.text.toString(),
            binding.loginFragmentPasswordEditText.text.toString(),
            true,
        )
    }

    private fun setupSurveyNotification() {
        // Setting for Survey Questionnaire Notification
        // called when you first run the application and set repeated notification at 9am
        // currently token doesn't exist
        // when backend is implemented it will be called when it has a token (Prefs(requireActivity()).token != "")
        if (prefs.token == "" && prefs.isNotificationSet) {
            val calendar = Calendar.getInstance()
            calendar.set(Calendar.HOUR_OF_DAY, 9)
            calendar.set(Calendar.MINUTE, 0)
            calendar.set(Calendar.SECOND, 0)
            startAlarm(
                calendar,
                180,
                SurveyQuestionnaireFragment.TAG,
                getString(R.string.notifications_WHOQOL_questionnaire)
            )
            prefs.isNotificationSet = false
        }
    }

    override fun openMainScreen() {
        val intent: Intent = if (binding.loginFragmentUsernameEditText.text.toString()
            .lowercase(Locale.ROOT) == AppConstants.CAREGIVER
        ) {
            Intent(activity, CaregiverActivity::class.java)
        } else {
            Intent(activity, MainActivity::class.java)
        }

        activity?.startActivity(intent)
        activity?.finish()
    }

    override fun showHttpError(message: String) {
        val title = getString(R.string.shared_error_code_title)
        AlertDialogUtils.showInfoAlertDialog(this.requireContext(), title, message)
    }

    override fun showUnexpectedError(message: String) {
        AlertDialogUtils.showUnexpectedErrorAlertDialog(this.requireContext(), message)
    }

    override fun showProgressBar() {
        ViewUtils.showView(bindingElevatedView.containerProgress)
    }

    override fun hideProgressBar() {
        ViewUtils.hideView(bindingElevatedView.containerProgress)
    }
}
