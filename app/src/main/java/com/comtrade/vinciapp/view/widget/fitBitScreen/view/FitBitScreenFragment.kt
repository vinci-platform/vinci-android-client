/*
 *  Copyright (c) 2020, Comtrade
 *  All rights reserved.
 *
 *  Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of the <organization> nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *   DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  Package: com.comtrade.vinciapp.view.widget.addDevice.view
 *  File: FitBitScreenFragment.kt
 *  Author: Kosta Eric <kosta.eric@comtrade.com>
 *
 *  Description: Fitbit fragment screen for displaying data from smartwatch
 *
 *  History: 9/8/20 Kosta  Initial code
 *
 *
 */

package com.comtrade.vinciapp.view.widget.fitBitScreen.view

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.entities.devices.UserDeviceEntity
import com.comtrade.domain.time.AppSyncIntervals
import com.comtrade.domain_ui.state.RequestUIState
import com.comtrade.feature_account.ui.user.GetUserExtraViewModel
import com.comtrade.feature_devices.ui.fitbit.FitBitDataViewModel
import com.comtrade.feature_devices.ui.fitbit.FitBitDisconnectViewModel
import com.comtrade.feature_devices.ui.state.APICallSuccessUIState
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.VinciApp
import com.comtrade.vinciapp.base.BaseFragment
import com.comtrade.vinciapp.databinding.FragmentFitbitBinding
import com.comtrade.vinciapp.view.widget.fitBitScreen.core.contract.FitBitScreenContract
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.time.Duration
import java.time.Instant
import javax.inject.Inject

@SuppressLint("VisibleForTests")
class FitBitScreenFragment : BaseFragment(), FitBitScreenContract.View, View.OnClickListener {

    private var _binding: FragmentFitbitBinding? = null
    private val binding get() = _binding!!

    @Inject
    lateinit var prefs: IPrefs

    private val userExtraViewModel by viewModels<GetUserExtraViewModel> {
        (requireContext().applicationContext as VinciApp).getAccountComponent()
            .provideGetUserExtraViewModelFactory()
    }

    private val fitBitDataViewModel by viewModels<FitBitDataViewModel> {
        (requireContext().applicationContext as VinciApp).getDevicesComponent()
            .provideFitBitDataViewModelFactory()
    }

    private val disconnectFitBitViewModel by viewModels<FitBitDisconnectViewModel> {
        (requireContext().applicationContext as VinciApp).getDevicesComponent()
            .provideFitBitDisconnectViewModelFactory()
    }

    companion object {
        const val TAG: String = "FitBitFragment"

        fun newInstance(): FitBitScreenFragment {
            return FitBitScreenFragment()
        }
    }

    @SuppressLint("VisibleForTests")
    override fun onAttach(context: Context) {
        super.onAttach(context)
        (context.applicationContext as VinciApp).getAppComponent()
            .inject(this@FitBitScreenFragment)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentFitbitBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeUserData()
        observeFitBitData()
        observeDisconnectFitBit()
        binding.fitbitFragmentDisconnectButton.setOnClickListener(this)
    }

    override fun onResume() {
        super.onResume()

        // Get User info
        userExtraViewModel.getUser(prefs.userId)

        // Get FitBit last 24h data periodically
        viewLifecycleOwner.lifecycleScope.launch(Dispatchers.Main) {

            while (true) {
                prefs.fitBitDevice?.let {
                    fitBitDataViewModel.getFitBitWatchData(
                        it.deviceId,
                        Instant.now().minus(Duration.ofDays(1)).toEpochMilli()
                    )
                }

                delay(AppSyncIntervals.FITBIT_DEVICE_REFRESH_INTERVAL)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onClick(view: View?) {
        when (view) {
            binding.fitbitFragmentDisconnectButton -> disconnectFitBitViewModel.disconnectDevice()
        }
    }

    /**
     * Observe the User data  possible UI states for the general information
     */
    private fun observeUserData() {

        userExtraViewModel.userExtraState.observe(viewLifecycleOwner) {
            when (it) {
                com.comtrade.feature_account.ui.state.APICallSuccessUIState.DefaultState -> {}
                is com.comtrade.feature_account.ui.state.APICallSuccessUIState.OnError -> {
                    it.message?.let { message ->
                        showShortToast(message)
                    }
                    userExtraViewModel.resetState()
                }
                com.comtrade.feature_account.ui.state.APICallSuccessUIState.OnLoading -> {}
                is com.comtrade.feature_account.ui.state.APICallSuccessUIState.OnSuccess -> {
                    setUserFieldValues(
                        "${it.data.userFirstName ?: getString(R.string.common_NA)} ${
                        it.data.userLastName ?: getString(
                            R.string.common_NA
                        )
                        }",
                        getString(R.string.common_NA),
                        it.data.gender ?: getString(R.string.common_NA),
                        it.data.birthDate ?: getString(R.string.common_NA)
                    )
                }
            }
        }
    }

    /**
     * Observe the possible UI states for the FitBit data obtaining.
     */
    private fun observeFitBitData() {
        fitBitDataViewModel.getFitBitWatchDataObserver.observe(viewLifecycleOwner) {
            when (it) {
                APICallSuccessUIState.DefaultState -> {}
                is APICallSuccessUIState.OnError -> {
                    it.message?.let { message ->
                        showShortToast(message)
                    }
                    fitBitDataViewModel.resetState()
                }
                APICallSuccessUIState.OnLoading -> {}
                is APICallSuccessUIState.OnSuccess -> {
                    val presenterData = it.data
                    updateFitbitDataView(
                        presenterData.steps,
                        presenterData.calories,
                        presenterData.hr,
                        presenterData.hrAvg,
                        presenterData.sleep,
                    )
                    fitBitDataViewModel.resetState()
                }
            }
        }
    }

    /**
     * Observe the possible UI states for the FitBit disconnect action.
     */
    private fun observeDisconnectFitBit() {
        disconnectFitBitViewModel.disconnectFitBitDeviceObserve.observe(viewLifecycleOwner) {
            when (it) {
                RequestUIState.DefaultState -> {}
                is RequestUIState.OnError -> {
                    it.message?.let { message ->
                        showShortToast(message)
                    }
                    disconnectFitBitViewModel.resetState()
                }
                RequestUIState.OnLoading -> {}
                is RequestUIState.OnSuccess -> {
                    requireActivity().onBackPressed()

                    disconnectFitBitViewModel.resetState()
                }
            }
        }
    }

    private fun setUserFieldValues(name: String, age: String, gender: String, dob: String) {
        binding.fitbitFragmentNameValue.text = name
        binding.fitbitFragmentAgeValue.text = age
        binding.fitbitFragmentGenderValue.text = gender
        binding.fitbitFragmentDobValue.text = dob
    }

    override fun updateFitbitDataView(
        steps: String,
        calories: String,
        hr: String,
        hrAvg: String,
        sleep: String,
    ) {
        binding.fitbitFragmentStepsValue.text = steps
        binding.fitbitFragmentKcalValue.text = calories
        binding.fitbitFragmentHrValue.text = if (hr == "0") getString(R.string.common_NA) else hr
        if (hrAvg == "0") {
            binding.fitbitFragmentHrAvgValue.visibility = View.GONE
        } else {
            binding.fitbitFragmentHrAvgValue.visibility = View.VISIBLE
            binding.fitbitFragmentHrAvgValue.text =
                getString(R.string.fitbit_screen_24_h_hr_avg_text, hrAvg)
        }
        binding.fitbitFragmentSleepValue.text =
            if (sleep == "0") getString(R.string.common_NA) else sleep
    }

    override fun updateFitBitUser(user: UserDeviceEntity) {
        val name = user.fullName
        val age = user.age.toString()
        val gender = user.gender
        val dob = user.dob
        setUserFieldValues(name = name, age = age, gender = gender, dob = dob)
    }
}
