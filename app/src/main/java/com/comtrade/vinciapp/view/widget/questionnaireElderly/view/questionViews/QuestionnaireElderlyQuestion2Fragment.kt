/*
 *  Copyright (c) 2020, Comtrade
 *  All rights reserved.
 *
 *  Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of the <organization> nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *   DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  Package: com.comtrade.vinciapp.view.widget.questionnaireElderly.view.questionViews
 *  File: QuestionnaireElderlyQuestion2Fragment.kt
 *  Author: Milenko Bojanic <milenko.bojanic@comtrade.com>
 *
 *  Description: Fragment screen for Elderly questionnaire second question
 *
 *  History: 8/11/20 Milenko  Initial code
 *
 *
 */

package com.comtrade.vinciapp.view.widget.questionnaireElderly.view.questionViews

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import com.comtrade.feature_questionnaire.survey.ui.SurveyViewModel
import com.comtrade.vinciapp.VinciApp
import com.comtrade.vinciapp.databinding.ViewElderlyQuestionnaireQuestion2Binding

class QuestionnaireElderlyQuestion2Fragment : QuestionnaireElderlyQuestionFragment() {

    private var _binding: ViewElderlyQuestionnaireQuestion2Binding? = null
    private val binding get() = _binding!!

    private val surveyViewModel: SurveyViewModel by activityViewModels {
        (requireContext().applicationContext as VinciApp).getAppComponent()
            .provideSurveyViewModelFactory()
    }

    companion object {
        const val TAG: String = "QuestionnaireElderlyQuestion2Fragment"

        fun newInstance(): QuestionnaireElderlyQuestion2Fragment {
            return QuestionnaireElderlyQuestion2Fragment()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding =
            ViewElderlyQuestionnaireQuestion2Binding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun storeAnswer(): String {
        val answers = mutableListOf<String>()
        if (binding.elderlyQuestionnaireQuestion2Answer1.isChecked)
            answers.add(binding.elderlyQuestionnaireQuestion2Answer1.text.toString())
        if (binding.elderlyQuestionnaireQuestion2Answer2.isChecked)
            answers.add(binding.elderlyQuestionnaireQuestion2Answer2.text.toString())
        if (binding.elderlyQuestionnaireQuestion2Answer3.isChecked)
            answers.add(binding.elderlyQuestionnaireQuestion2Answer3.text.toString())
        if (binding.elderlyQuestionnaireQuestion2Answer4.isChecked)
            answers.add(binding.elderlyQuestionnaireQuestion2Answer4.text.toString())
        if (binding.elderlyQuestionnaireQuestion2Answer5.isChecked)
            answers.add(binding.elderlyQuestionnaireQuestion2Answer5.toString())
        if (binding.elderlyQuestionnaireQuestion2Answer6.isChecked)
            answers.add(binding.elderlyQuestionnaireQuestion2Answer6.text.toString())
        if (binding.elderlyQuestionnaireQuestion2Answer7.isChecked)
            answers.add(binding.elderlyQuestionnaireQuestion2Answer7.text.toString())
        surveyViewModel.assessmentData!![2] = answers.toString()
        return ""
    }
}
