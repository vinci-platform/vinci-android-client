/*
 *  Copyright (c) 2020, Comtrade
 *  All rights reserved.
 *
 *  Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of the <organization> nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *   DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  Package: com.comtrade.vinciapp.view.widget.questionnaireIPA.view.questionViews
 *  File: QuestionnaireIPARadioButtonQuestions.kt
 *  Author: Kosta Eric <kosta.eric@comtrade.com>
 *
 *  Description: View for IPA questionnaire radio button screens
 *
 *  History: 8/11/20 Kosta  Initial code
 *
 *
 */

package com.comtrade.vinciapp.view.widget.questionnaireIPA.view.questionViews

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.comtrade.vinciapp.base.BaseFragment
import com.comtrade.vinciapp.databinding.ViewIpaQuestionnaireRadioButtonQuestionBinding

class QuestionnaireIPARadioButtonQuestions : BaseFragment() {

    private var _binding: ViewIpaQuestionnaireRadioButtonQuestionBinding? = null
    private val binding get() = _binding!!

    private var questionNumber: String? = ""
    private var questionDescription: String? = ""
    private var question: String? = ""
    private var hiddenQuestion: String? = ""

    companion object {
        const val TAG: String = "QuestionnaireIPARadioButtonQuestions"

        private const val QUESTION_NUMBER_KEY: String = "question_number"
        private const val QUESTION_DESCRIPTION_KEY: String = "question_description"
        private const val QUESTION_KEY: String = "question"
        private const val HIDDEN_QUESTION_KEY: String = "hidden_question"

        fun newInstance(
            questionNumber: String,
            questionDescription: String,
            question: String,
            hiddenQuestion: String
        ): QuestionnaireIPARadioButtonQuestions {
            val fragment = QuestionnaireIPARadioButtonQuestions()
            val args = Bundle()
            args.putString(QUESTION_NUMBER_KEY, questionNumber)
            args.putString(QUESTION_DESCRIPTION_KEY, questionDescription)
            args.putString(QUESTION_KEY, question)
            args.putString(HIDDEN_QUESTION_KEY, hiddenQuestion)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding =
            ViewIpaQuestionnaireRadioButtonQuestionBinding.inflate(inflater, container, false)
        arguments?.let {
            questionNumber = it.getString(QUESTION_NUMBER_KEY)
            questionDescription = it.getString(QUESTION_DESCRIPTION_KEY)
            question = it.getString(QUESTION_KEY)
            hiddenQuestion = it.getString(HIDDEN_QUESTION_KEY)
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.ipaQuestionnaireScreenRadioButtonQuestionNumber.text = questionNumber
        binding.ipaQuestionnaireScreenRadioButtonQuestionDescription.text = questionDescription
        binding.ipaQuestionnaireScreenRadioButtonQuestionQuestion.text = question
        binding.ipaQuestionnaireScreenRadioButtonQuestionHiddenQuestion.text = hiddenQuestion
    }
}
