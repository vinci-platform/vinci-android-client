/*
 *  Copyright (c) 2020, Comtrade
 *  All rights reserved.
 *
 *  Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of the <organization> nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *   DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  Package: com.comtrade.vinciapp.view.widget.smsMessage.view
 *  File: SmsMessageFragment.kt
 *  Author: Milenko Bojanic <milenko.bojanic@comtrade.com>
 *
 *  Description: Fragment for sending SMS invitations to friends
 *
 *  History: 8/11/20 Milenko Initial code
 *
 *
 */

package com.comtrade.vinciapp.view.widget.smsMessage.view

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.comtrade.vinciapp.AppConstants
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.base.BaseFragment
import com.comtrade.vinciapp.databinding.FragmentSmsMessageBinding
import com.comtrade.vinciapp.util.AlertDialogUtils
import com.comtrade.vinciapp.view.widget.smsMessage.core.SmsMessagePresenter
import com.comtrade.vinciapp.view.widget.smsMessage.core.contract.SmsMessageContract

class SmsMessageFragment : BaseFragment(), SmsMessageContract.View, View.OnClickListener {

    private var _binding: FragmentSmsMessageBinding? = null
    private val binding get() = _binding!!

    private var contactsNumbers: String? = ""

    private val presenter: SmsMessageContract.Presenter by lazy { SmsMessagePresenter() }

    companion object {
        const val TAG: String = "SmsMessageFragment"

        fun newInstance(): SmsMessageFragment {
            return SmsMessageFragment()
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSmsMessageBinding.inflate(inflater, container, false)
        contactsNumbers = arguments?.getString(AppConstants.CONTACTS_EXTRA)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListeners()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun setListeners() {
        binding.smsMessageFragmentBackButton.setOnClickListener(this)
        binding.smsMessageFragmentInviteButton.setOnClickListener(this)
    }


    override fun onClick(view: View?) {
        when (view) {
            binding.smsMessageFragmentBackButton -> {
                presenter.backToChooseContactsScreen(requireActivity().supportFragmentManager)
            }
            binding.smsMessageFragmentInviteButton -> {
                openSmsMessageApp(binding.smsMessageFragmentMessageText.text.toString())
                presenter.showAddDeviceScreen(requireActivity().supportFragmentManager)
            }
        }
    }

    private fun openSmsMessageApp(smsBody: String) {
        val smsIntent = Intent(Intent.ACTION_SENDTO)
        smsIntent.data = Uri.parse(AppConstants.SMS_TO + contactsNumbers)
        smsIntent.putExtra(AppConstants.SMS_BODY, smsBody)
        if (smsIntent.resolveActivity(requireActivity().packageManager) != null) {
            requireActivity().startActivity(smsIntent)
        } else {
            AlertDialogUtils.showInfoAlertDialog(
                requireContext(),
                getString(R.string.shared_unexpected_error_title),
                getString(R.string.sms_message_screen_error_message)
            )
        }
    }
}