/*
 *  Copyright (c) 2020, Comtrade
 *  All rights reserved.
 *
 *  Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of the <organization> nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *   DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  Package: com.comtrade.vinciapp.view.widget.addNewDevice.view
 *  File: AddNewDeviceFragment.kt
 *  Author: Sofija Andjelkovic <sofija.andjelkovic@comtrade.com>
 *
 *  Description: a two-button fragment for different types of adding a new device
 *
 *  History: 8/11/20 Sofija  Initial code
 *
 *
 */

package com.comtrade.vinciapp.view.widget.healthPointsHistory.view

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.viewModels
import com.comtrade.domain.entities.graph.GraphData
import com.comtrade.domain.entities.step.HealthDataType
import com.comtrade.domain_ui.state.RequestUIState
import com.comtrade.feature_health_records.ui.GetHealthDataForDaysViewModel
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.VinciApp
import com.comtrade.vinciapp.view.adapter.QuestionnaireViewPagerAdapter
import com.comtrade.vinciapp.view.widget.historyGraph.view.HistoryGraphFragment
import com.comtrade.vinciapp.view.widget.historyGraph.view.HistoryGraphScreen
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import java.text.DecimalFormat
import kotlin.math.roundToInt

@SuppressLint("VisibleForTests")
class HealthPointsHistoryFragment : HistoryGraphFragment() {
    private lateinit var healthPoints: Array<GraphData>
    private lateinit var whoqolPoints: Array<GraphData>
    private lateinit var ipaqPoints: Array<GraphData>
    private lateinit var stepsPoints: Array<GraphData>

    private val healthDataForDaysViewModel: GetHealthDataForDaysViewModel by viewModels {
        (requireContext().applicationContext as VinciApp).getHealthRecordsComponent()
            .provideHealthDataForDaysViewModelFactory()
    }

    companion object {
        const val TAG: String = "HealthPointsHistoryFragment"
        const val HEALTH_RECORD_DAYS = 7

        const val HEALTH_POINTS_KEY = "Health Points"
        const val WHO_QOL_POINTS_KEY = "WHO Quality Of Life Points"
        const val IPAQ_POINTS_KEY = "IPAQ Points"
        const val STEPS_POINTS_KEY = "Steps Points"

        fun newInstance(): HealthPointsHistoryFragment {
            return HealthPointsHistoryFragment()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeHealthData()
    }

    private fun observeHealthData() {
        healthDataForDaysViewModel.getHealthDataObserver.observe(viewLifecycleOwner) {
            when (it) {
                RequestUIState.DefaultState -> {
                    hideProgressBar()
                }
                is RequestUIState.OnError -> {
                    hideProgressBar()
                    it.message?.let { _ ->
                        showShortToast(
                            getString(R.string.step_history_screen_health_point_error)
                        )
                    }
                    healthDataForDaysViewModel.resetState()
                }
                RequestUIState.OnLoading -> {
                    showProgressBar()
                }
                is RequestUIState.OnSuccess -> {
                    hideProgressBar()
                    handleHealthData(it.data)
                    healthDataForDaysViewModel.resetState()
                }
            }
        }
    }

    /**
     * Parse the obtained data and attach it to the adapter component.
     */
    private fun handleHealthData(data: Map<HealthDataType, Array<GraphData>>) {
        healthPoints = data[HealthDataType.POINTS]!!
        whoqolPoints = data[HealthDataType.WHOQOL]!!
        ipaqPoints = data[HealthDataType.IPAQ]!!
        stepsPoints = data[HealthDataType.STEPS]!!

        Log.w("Health points: ", healthPoints.size.toString())
        Log.w("WHO QOL points: ", whoqolPoints.size.toString())
        Log.w("IPAQ points: ", ipaqPoints.size.toString())
        Log.w("Step points: ", stepsPoints.size.toString())

        setAdapter()
    }

    private fun setAdapter() {
        fragmentsPagerAdapter =
            QuestionnaireViewPagerAdapter(childFragmentManager, viewLifecycleOwner.lifecycle)

        val healthPointsData: HashMap<String, Array<Int>> = hashMapOf()
        healthPointsData[HEALTH_POINTS_KEY] = getDataForLastWeek(healthPoints)

        val whoqolPointsData: HashMap<String, Array<Int>> = hashMapOf()
        whoqolPointsData[WHO_QOL_POINTS_KEY] = getDataForLastWeek(whoqolPoints)

        val ipaqPointsData: HashMap<String, Array<Int>> = hashMapOf()
        ipaqPointsData[IPAQ_POINTS_KEY] = getDataForLastWeek(ipaqPoints)

        val stepsPointsData: HashMap<String, Array<Int>> = hashMapOf()
        stepsPointsData[STEPS_POINTS_KEY] = getDataForLastWeek(stepsPoints)

        val labels = getDataLabelsForLastWeek(healthPoints)

        fragmentsPagerAdapter.addFragment(
            HistoryGraphScreen.newInstance(
                getString(R.string.health_points_history_screen_points),
                getString(R.string.health_points_history_screen_average),
                DecimalFormat("#,###").format(
                    healthPointsData.asIterable().first().value.asIterable().average().roundToInt()
                ).toString(),
                getString(R.string.health_points_history_screen_max),
                DecimalFormat("#,###").format(
                    healthPointsData.asIterable().first().value.asIterable().maxOrNull()
                ).toString(),
                labels,
                healthPointsData
            )
        )

        fragmentsPagerAdapter.addFragment(
            HistoryGraphScreen.newInstance(
                getString(R.string.health_points_history_who_qol_points),
                getString(R.string.health_points_history_screen_average),
                DecimalFormat("#,###").format(
                    whoqolPointsData.asIterable().first().value.asIterable().average().roundToInt()
                ).toString(),
                getString(R.string.health_points_history_screen_max),
                DecimalFormat("#,###").format(
                    whoqolPointsData.asIterable().first().value.asIterable().maxOrNull()
                ).toString(),
                labels,
                whoqolPointsData
            )
        )

        fragmentsPagerAdapter.addFragment(
            HistoryGraphScreen.newInstance(
                getString(R.string.health_points_history_ipaq_points),
                getString(R.string.health_points_history_screen_average),
                DecimalFormat("#,###").format(
                    ipaqPointsData.asIterable().first().value.asIterable().average().roundToInt()
                ).toString(),
                getString(R.string.health_points_history_screen_max),
                DecimalFormat("#,###").format(
                    ipaqPointsData.asIterable().first().value.asIterable().maxOrNull()
                ).toString(),
                labels,
                ipaqPointsData
            )
        )

        fragmentsPagerAdapter.addFragment(
            HistoryGraphScreen.newInstance(
                getString(R.string.health_points_history_steps_points),
                getString(R.string.health_points_history_screen_average),
                DecimalFormat("#,###").format(
                    stepsPointsData.asIterable().first().value.asIterable().average().roundToInt()
                ).toString(),
                getString(R.string.health_points_history_screen_max),
                DecimalFormat("#,###").format(
                    stepsPointsData.asIterable().first().value.asIterable().maxOrNull()
                ).toString(),
                labels,
                stepsPointsData
            )
        )
        viewPager.adapter = fragmentsPagerAdapter
        setTab()
    }

    override fun setTab() {
        TabLayoutMediator(tabLayout, viewPager) { tab, position ->
            when (position) {
                0 -> tab.text = getString(R.string.health_points_history_screen_points)
                1 -> tab.text = getString(R.string.health_points_history_screen_who_qol)
                2 -> tab.text = getString(R.string.health_points_history_screen_ipaq)
                3 -> tab.text = getString(R.string.health_points_history_screen_steps)
            }
        }.attach()
        tabLayout.tabGravity = TabLayout.GRAVITY_FILL
    }

    override fun getData() {
        healthDataForDaysViewModel.getHealthDataLastDays(HEALTH_RECORD_DAYS)
    }
}
