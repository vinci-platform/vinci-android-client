/*
 *  Copyright (c) 2020, Comtrade
 *  All rights reserved.
 *
 *  Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of the <organization> nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *   DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  Package: com.comtrade.vinciapp.view.widget.medication.view
 *  File: MedicationFragment.kt
 *  Author: Kosta Eric <kosta.eric@comtrade.com>
 *
 *  Description: Fragment for medication screen
 *
 *  History: 8/11/20 Kosta Initial code
 *
 *
 */

package com.comtrade.vinciapp.view.widget.medication.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.comtrade.vinciapp.base.BaseFragment
import com.comtrade.vinciapp.databinding.FragmentMedicationBinding
import com.comtrade.vinciapp.view.widget.medication.core.MedicationPresenter
import com.comtrade.vinciapp.view.widget.medication.core.contract.MedicationContract

@Deprecated("Screen component is not used anywhere in the application outside its test class.")
class MedicationFragment : BaseFragment(), MedicationContract.View, View.OnClickListener {

    private var _binding: FragmentMedicationBinding? = null
    private val binding get() = _binding!!

    private val presenter: MedicationContract.Presenter by lazy { MedicationPresenter() }

    companion object {
        const val TAG: String = "MedicationFragment"

        fun newInstance(): MedicationFragment {
            return MedicationFragment()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMedicationBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListeners()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun setListeners() {
        binding.medicationFragmentButtonYes.setOnClickListener(this)
        binding.medicationFragmentButtonNo.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
        when (view) {
            binding.medicationFragmentButtonYes -> {
                presenter.showMainScreen(requireActivity().supportFragmentManager)
            }
            binding.medicationFragmentButtonNo -> {
                presenter.showMedicationSnoozeScreen(requireActivity().supportFragmentManager)
            }
        }
    }
}
