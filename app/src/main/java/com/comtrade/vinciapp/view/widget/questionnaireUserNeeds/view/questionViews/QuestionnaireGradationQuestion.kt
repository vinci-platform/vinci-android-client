/*
 *  Copyright (c) 2020, Comtrade
 *  All rights reserved.
 *
 *  Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of the <organization> nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *   DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  Package: com.comtrade.vinciapp.view.widget.questionnaireSurvey.view.questionViews
 *  File: QuestionnaireSurveyGradationQuestion.kt
 *  Author: Milenko Bojanić <milenko.bojanic@comtrade.com>
 *
 *  Description: Fragment screen for QOL questionnaire gradation questions
 *
 *  History: 8/11/20 Kosta  Initial code
 *
 *
 */

package com.comtrade.vinciapp.view.widget.questionnaireUserNeeds.view.questionViews

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.activityViewModels
import com.comtrade.feature_questionnaire.survey.ui.SurveyViewModel
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.VinciApp
import com.comtrade.vinciapp.base.BaseFragment
import com.comtrade.vinciapp.databinding.ViewQuestionnaireGradationQuestionBinding
import com.comtrade.vinciapp.view.widget.questionnaireSurvey.model.SurveyQuestionnaireItem

class QuestionnaireGradationQuestion : BaseFragment(), View.OnClickListener {

    private var _binding: ViewQuestionnaireGradationQuestionBinding? = null
    private val binding get() = _binding!!

    private var mainQuestion: String? = ""
    var questionId: String? = ""
    private var questionNumber: String? = ""
    var question: String? = ""
    var order: Boolean? = true
    var answers: Array<String> = emptyArray()
    private var selectedButton: Int = ZERO

    private val surveyViewModel: SurveyViewModel by activityViewModels {
        (requireContext().applicationContext as VinciApp).getAppComponent()
            .provideSurveyViewModelFactory()
    }

    companion object {
        const val TAG: String = "QuestionnaireGradationQuestion"

        const val QUESTION_ID_KEY: String = "question_id"
        const val MAIN_QUESTION_KEY: String = "main_question"
        const val QUESTION_NUMBER_KEY: String = "question_number"
        const val QUESTION_KEY: String = "question"
        const val ORDER_KEY: String = "order"
        const val ANSWERS_KEY: String = "answers"

        const val ZERO = 0
        const val ONE: Int = 1
        const val TWO: Int = 2
        const val THREE: Int = 3
        const val FOUR: Int = 4
        const val FIVE: Int = 5

        fun newInstance(
            mainQuestion: String,
            questionItem: SurveyQuestionnaireItem
        ): QuestionnaireGradationQuestion {
            val fragment = QuestionnaireGradationQuestion()
            val args = Bundle()
            args.putString(MAIN_QUESTION_KEY, mainQuestion)
            args.putString(QUESTION_ID_KEY, questionItem.questionId)
            args.putString(QUESTION_NUMBER_KEY, questionItem.questionNumber)
            args.putString(QUESTION_KEY, questionItem.question)
            args.putBoolean(ORDER_KEY, questionItem.order)
            args.putStringArray(ANSWERS_KEY, questionItem.answers)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding =
            ViewQuestionnaireGradationQuestionBinding.inflate(
                inflater,
                container,
                false
            )
        mainQuestion = arguments?.getString(MAIN_QUESTION_KEY)
        questionId = arguments?.getString(QUESTION_ID_KEY)
        questionNumber = arguments?.getString(QUESTION_NUMBER_KEY)
        question = arguments?.getString(QUESTION_KEY)
        order = arguments?.getBoolean(ORDER_KEY)
        answers = arguments?.getStringArray(ANSWERS_KEY) as Array<String>
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.viewQuestionnaireGradationInfoLabel.text = mainQuestion
        binding.viewQuestionnaireGradationQuestionNumber.text = questionNumber
        binding.viewQuestionnaireGradationQuestionText.text = question
        setAnswerTexts()
        setAnswerNumbers()
        setAnswerColors()
        setListeners()
    }

    // TODO: Simplify
    private fun setAnswerNumbers() {
        if (order != false) {
            binding.viewQuestionnaireGradationButton1NumberText.text =
                getString(R.string.view_questionnaire_gradation_1_answer_number)
            binding.viewQuestionnaireGradationButton2NumberText.text =
                getString(R.string.view_questionnaire_gradation_2_answer_number)
            binding.viewQuestionnaireGradationButton3NumberText.text =
                getString(R.string.view_questionnaire_gradation_3_answer_number)
            binding.viewQuestionnaireGradationButton4NumberText.text =
                getString(R.string.view_questionnaire_gradation_4_answer_number)
            binding.viewQuestionnaireGradationButton5NumberText.text =
                getString(R.string.view_questionnaire_gradation_5_answer_number)
        } else {
            binding.viewQuestionnaireGradationButton1NumberText.text =
                getString(R.string.view_questionnaire_gradation_5_answer_number)
            binding.viewQuestionnaireGradationButton2NumberText.text =
                getString(R.string.view_questionnaire_gradation_4_answer_number)
            binding.viewQuestionnaireGradationButton3NumberText.text =
                getString(R.string.view_questionnaire_gradation_3_answer_number)
            binding.viewQuestionnaireGradationButton4NumberText.text =
                getString(R.string.view_questionnaire_gradation_2_answer_number)
            binding.viewQuestionnaireGradationButton5NumberText.text =
                getString(R.string.view_questionnaire_gradation_1_answer_number)
        }
    }

    // TODO: Simplify
    private fun setAnswerTexts() {
        if (order != false) {
            binding.viewQuestionnaireGradationButton1Text.text = answers[0]
            binding.viewQuestionnaireGradationButton2Text.text = answers[1]
            binding.viewQuestionnaireGradationButton3Text.text = answers[2]
            binding.viewQuestionnaireGradationButton4Text.text = answers[3]
            binding.viewQuestionnaireGradationButton5Text.text = answers[4]
        } else {
            binding.viewQuestionnaireGradationButton1Text.text = answers[4]
            binding.viewQuestionnaireGradationButton2Text.text = answers[3]
            binding.viewQuestionnaireGradationButton3Text.text = answers[2]
            binding.viewQuestionnaireGradationButton4Text.text = answers[1]
            binding.viewQuestionnaireGradationButton5Text.text = answers[0]
        }
    }

    private fun setListeners() {
        val answerButtons = arrayOf(
            binding.viewQuestionnaireGradationButton1,
            binding.viewQuestionnaireGradationButton2,
            binding.viewQuestionnaireGradationButton3,
            binding.viewQuestionnaireGradationButton4,
            binding.viewQuestionnaireGradationButton5,
        )

        for (answerButton in answerButtons) {
            answerButton.setOnClickListener(this)
        }
    }

    override fun onClick(view: View?) {
        when (view) {
            binding.viewQuestionnaireGradationButton1 -> {
                updateGradationAnswer(
                    ONE,
                    binding.viewQuestionnaireGradationButton1,
                    binding.viewQuestionnaireGradationButton1Text,
                    binding.viewQuestionnaireGradationButton1NumberText
                )
            }
            binding.viewQuestionnaireGradationButton2 -> {
                updateGradationAnswer(
                    TWO,
                    binding.viewQuestionnaireGradationButton2,
                    binding.viewQuestionnaireGradationButton2Text,
                    binding.viewQuestionnaireGradationButton2NumberText
                )
            }
            binding.viewQuestionnaireGradationButton3 -> {
                updateGradationAnswer(
                    THREE,
                    binding.viewQuestionnaireGradationButton3,
                    binding.viewQuestionnaireGradationButton3Text,
                    binding.viewQuestionnaireGradationButton3NumberText
                )
            }
            binding.viewQuestionnaireGradationButton4 -> {
                updateGradationAnswer(
                    FOUR,
                    binding.viewQuestionnaireGradationButton4,
                    binding.viewQuestionnaireGradationButton4Text,
                    binding.viewQuestionnaireGradationButton4NumberText
                )
            }
            binding.viewQuestionnaireGradationButton5 -> {
                updateGradationAnswer(
                    FIVE,
                    binding.viewQuestionnaireGradationButton5,
                    binding.viewQuestionnaireGradationButton5Text,
                    binding.viewQuestionnaireGradationButton5NumberText
                )
            }
        }
    }

    private fun updateGradationAnswer(
        number: Int,
        image: ImageView,
        text: TextView,
        numberText: TextView
    ) {
        setAnswerColors()
        setUnselectedTextsColors()
        if (selectedButton == number) {
            resetSelection()
        } else {
            selectedButton = number
            surveyViewModel.questionnaireUserNeedsAnswers!![
                questionId
                    ?: ""
            ] = number.toString()
            setSelectedButtonColors(image)
            setSelectedTextsColors(
                text,
                numberText
            )
        }
    }

    private fun resetSelection() {
        selectedButton = ZERO
        surveyViewModel.questionnaireUserNeedsAnswers!![
            questionId
                ?: ""
        ] = ""
    }

    private fun setSelectedButtonColors(image: ImageView) {
        image.background = ContextCompat.getDrawable(
            requireContext(),
            R.drawable.rounded_corner_selected_button_background
        )
    }

    private fun setSelectedTextsColors(text: TextView, number: TextView) {
        text.setTextColor(ContextCompat.getColor(requireContext(), R.color.colorWhiteWith50Opacity))
        number.setTextColor(
            ContextCompat.getColor(
                requireContext(),
                R.color.colorWhiteWith50Opacity
            )
        )
    }

    private fun setAnswerColors() {

        val colorBackgroundMap = mapOf(
            binding.viewQuestionnaireGradationButton1 to R.drawable.rounded_corner_lighter_button_background,
            binding.viewQuestionnaireGradationButton2 to R.drawable.rounded_corner_light_button_background,
            binding.viewQuestionnaireGradationButton3 to R.drawable.rounded_corner_medium_button_background,
            binding.viewQuestionnaireGradationButton4 to R.drawable.rounded_corner_dark_button_background,
            binding.viewQuestionnaireGradationButton5 to R.drawable.rounded_corner_darker_button_background
        )

        for (mapItem in colorBackgroundMap) {
            mapItem.key.background = ContextCompat.getDrawable(
                requireContext(),
                mapItem.value
            )
        }
    }

    private fun setUnselectedTextsColors() {

        val textViews = arrayOf(
            binding.viewQuestionnaireGradationButton1Text,
            binding.viewQuestionnaireGradationButton1NumberText,
            binding.viewQuestionnaireGradationButton2Text,
            binding.viewQuestionnaireGradationButton2NumberText,
            binding.viewQuestionnaireGradationButton3Text,
            binding.viewQuestionnaireGradationButton3NumberText,
            binding.viewQuestionnaireGradationButton4Text,
            binding.viewQuestionnaireGradationButton4NumberText,
            binding.viewQuestionnaireGradationButton5Text,
            binding.viewQuestionnaireGradationButton5NumberText
        )

        for (view in textViews) {
            view.setTextColor(
                ContextCompat.getColor(
                    requireContext(),
                    R.color.colorWhite
                )
            )
        }
    }
}
