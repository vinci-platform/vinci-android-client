/*
 *  Copyright (c) 2020, Comtrade
 *  All rights reserved.
 *
 *  Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of the <organization> nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *   DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  Package: com.comtrade.vinciapp.view.widget.questionnaireIPA.view
 *  File: IPAQuestionnaireFragment.kt
 *  Author: Kosta Eric <kosta.eric@comtrade.com>
 *
 *  Description: Fragment for IPA questionnaire screen
 *
 *  History: 8/11/20 Kosta  Initial code
 *
 *
 */

package com.comtrade.vinciapp.view.widget.questionnaireIPA.view

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.RadioButton
import android.widget.TextView
import androidx.core.view.get
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import com.comtrade.domain.survey.SurveyTypes
import com.comtrade.feature_questionnaire.survey.ui.SurveyViewModel
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.VinciApp
import com.comtrade.vinciapp.base.BaseFragment
import com.comtrade.vinciapp.databinding.FragmentIpaQuestionnaireBinding
import com.comtrade.vinciapp.util.SimpleTextWatcher
import com.comtrade.vinciapp.util.SwipeControlTouchListener
import com.comtrade.vinciapp.util.SwipeDirection
import com.comtrade.vinciapp.util.ViewUtils
import com.comtrade.vinciapp.view.adapter.QuestionnaireViewPagerAdapter
import com.comtrade.vinciapp.view.widget.questionnaireIPA.core.contract.IIPAQuestionnaireEndScreen
import com.comtrade.vinciapp.view.widget.questionnaireIPA.core.contract.IIPAQuestionnaireInputScreen
import com.comtrade.vinciapp.view.widget.questionnaireIPA.view.questionViews.QuestionnaireIPAEndScreen
import com.comtrade.vinciapp.view.widget.questionnaireIPA.view.questionViews.QuestionnaireIPAInputQuestions
import com.comtrade.vinciapp.view.widget.questionnaireIPA.view.questionViews.QuestionnaireIPARadioButtonQuestions
import com.comtrade.vinciapp.view.widget.questionnaireIPA.view.questionViews.QuestionnaireIPAStartScreen
import kotlin.math.roundToLong

class IPAQuestionnaireFragment :
    BaseFragment(),
    View.OnClickListener {

    private var _binding: FragmentIpaQuestionnaireBinding? = null
    private val binding get() = _binding!!

    private lateinit var fragmentsPagerAdapter: QuestionnaireViewPagerAdapter

    private val maxHours: Int = 7 * 24

    private val surveyViewModel: SurveyViewModel by activityViewModels {
        (requireContext().applicationContext as VinciApp).getAppComponent()
            .provideSurveyViewModelFactory()
    }

    private val swipeControlTouchListener by lazy {
        SwipeControlTouchListener()
    }

    companion object {
        const val TAG: String = "IPAQuestionnaireFragment"

        private const val SURVEY_TYPE = SurveyTypes.IPAQ

        fun newInstance(): IPAQuestionnaireFragment {
            return IPAQuestionnaireFragment()
        }
    }

    @SuppressLint("VisibleForTests")
    override fun onAttach(context: Context) {
        super.onAttach(context)
        (context.applicationContext as VinciApp).getAppComponent()
            .inject(this@IPAQuestionnaireFragment)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentIpaQuestionnaireBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        surveyViewModel.startSurvey(SURVEY_TYPE)

        setAdapter()
        setListeners()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun setListeners() {
        binding.ipaQuestionnaireFragmentBackButton.setOnClickListener(this)
        binding.ipaQuestionnaireFragmentNextButton.setOnClickListener(this)
        binding.ipaQuestionnaireFragmentSaveButton.setOnClickListener(this)

        // Obtain the underlying RecyclerView component from the ViewPager and attach listener.
        val viewPagerRecyclerView = binding.ipaQuestionnaireFragmentViewPager[0] as? RecyclerView
        viewPagerRecyclerView?.apply {
            addOnItemTouchListener(swipeControlTouchListener)
        }

        binding.ipaQuestionnaireFragmentViewPager.registerOnPageChangeCallback(object :
                ViewPager2.OnPageChangeCallback() {
                override fun onPageSelected(position: Int) {
                    super.onPageSelected(position)
                    val fragment: Fragment = fragmentsPagerAdapter.createFragment(position)
                    swipeControlTouchListener.setSwipeDirection(SwipeDirection.ALL)

                    if (position == fragmentsPagerAdapter.itemCount - 1) {
                        ViewUtils.hideView(binding.ipaQuestionnaireFragmentNextButton)
                        ViewUtils.showView(binding.ipaQuestionnaireFragmentSaveButton)
                    } else {
                        ViewUtils.hideView(binding.ipaQuestionnaireFragmentSaveButton)
                        ViewUtils.showView(binding.ipaQuestionnaireFragmentNextButton)
                    }

                    if (position == 1) {
                        val hoursEditText =
                            fragment.view?.findViewById<EditText>(R.id.ipa_questionnaire_screen_input_question_edit_text_hours)
                        val minutesEditText =
                            fragment.view?.findViewById<EditText>(R.id.ipa_questionnaire_screen_input_question_edit_text_minutes)

                        if (hoursEditText?.text.toString().isEmpty() || minutesEditText?.text.toString()
                            .isEmpty()
                        ) {
                            swipeControlTouchListener.setSwipeDirection(SwipeDirection.LEFT)
                        } else {
                            if (inputScreenValidationCheck(maxHours).isEmpty()) {
                                swipeControlTouchListener.setSwipeDirection(SwipeDirection.LEFT)
                            } else {
                                swipeControlTouchListener.setSwipeDirection(SwipeDirection.ALL)
                            }
                        }
                        setInputScreenListeners(hoursEditText, minutesEditText, maxHours)
                    } else if (position > 1 && position != fragmentsPagerAdapter.itemCount - 1) {
                        val hoursEditText =
                            fragment.view?.findViewById<EditText>(R.id.ipa_questionnaire_screen_radio_button_question_edit_text_hours)
                        val minutesEditText =
                            fragment.view?.findViewById<EditText>(R.id.ipa_questionnaire_screen_radio_button_question_edit_text_minutes)
                        val daysEditText =
                            fragment.view?.findViewById<EditText>(R.id.ipa_questionnaire_screen_radio_button_question_edit_text_days)
                        val radioButtonNone =
                            fragment.view?.findViewById<RadioButton>(R.id.ipa_questionnaire_screen_radio_button_none)
                        val radioButtonDays =
                            fragment.view?.findViewById<RadioButton>(R.id.ipa_questionnaire_screen_radio_button_days)

                        if (!radioButtonNone?.isChecked!! && !radioButtonDays?.isChecked!!) {
                            swipeControlTouchListener.setSwipeDirection(SwipeDirection.LEFT)
                        } else if (radioButtonNone.isChecked) {
                            swipeControlTouchListener.setSwipeDirection(SwipeDirection.ALL)
                        } else if (radioButtonDays?.isChecked!!) {
                            if (daysEditText?.text.toString()
                                .isEmpty() || hoursEditText?.text.toString()
                                    .isEmpty() || minutesEditText?.text.toString().isEmpty()
                            ) {
                                swipeControlTouchListener.setSwipeDirection(SwipeDirection.LEFT)
                            } else {
                                if (radioButtonScreenValidationCheck(fragment.view).isEmpty()) {
                                    swipeControlTouchListener.setSwipeDirection(SwipeDirection.LEFT)
                                } else {
                                    swipeControlTouchListener.setSwipeDirection(SwipeDirection.ALL)
                                }
                            }
                            showHiddenScreenItems(fragment.view)
                        }

                        setRadioButtonScreenListeners(
                            fragment.view,
                            daysEditText,
                            hoursEditText,
                            minutesEditText,
                            radioButtonNone,
                            radioButtonDays
                        )
                    }
                }
            })
    }

    private fun setAdapter() {
        fragmentsPagerAdapter =
            QuestionnaireViewPagerAdapter(childFragmentManager, viewLifecycleOwner.lifecycle)
        fragmentsPagerAdapter.addFragment(QuestionnaireIPAStartScreen.newInstance())
        fragmentsPagerAdapter.addFragment(QuestionnaireIPAInputQuestions.newInstance())
        addIPAQuestionnaireRadioButtonQuestions()
        fragmentsPagerAdapter.addFragment(QuestionnaireIPAEndScreen.newInstance())
        binding.ipaQuestionnaireFragmentViewPager.adapter = fragmentsPagerAdapter
        binding.ipaQuestionnaireFragmentViewPager.offscreenPageLimit = 10
    }

    override fun onClick(view: View?) {
        when (view) {
            binding.ipaQuestionnaireFragmentBackButton -> {
                decreaseViewPagerCurrentItem()
            }
            binding.ipaQuestionnaireFragmentNextButton -> {
                if (binding.ipaQuestionnaireFragmentViewPager.currentItem == 1) {
                    val result = inputScreenValidationCheck(maxHours)
                    if (result.isNotEmpty()) {
                        increaseViewPagerCurrentItem()
                        surveyViewModel.assessmentData!![1] = result
                    }
                } else if (binding.ipaQuestionnaireFragmentViewPager.currentItem > 1 && binding.ipaQuestionnaireFragmentViewPager.currentItem != fragmentsPagerAdapter.itemCount - 1) {
                    val fragment: Fragment =
                        fragmentsPagerAdapter.createFragment(binding.ipaQuestionnaireFragmentViewPager.currentItem)
                    val noneButton =
                        fragment.view?.findViewById<RadioButton>(R.id.ipa_questionnaire_screen_radio_button_none)

                    if (noneButton!!.isChecked) {
                        surveyViewModel.assessmentData!![binding.ipaQuestionnaireFragmentViewPager.currentItem] =
                            "0"
                        increaseViewPagerCurrentItem()
                    } else {
                        val result = radioButtonScreenValidationCheck(fragment.view)
                        if (result.isNotEmpty()) {
                            surveyViewModel.assessmentData!![binding.ipaQuestionnaireFragmentViewPager.currentItem] =
                                result
                            increaseViewPagerCurrentItem()
                        }
                    }
                } else {
                    increaseViewPagerCurrentItem()
                }
            }
            binding.ipaQuestionnaireFragmentSaveButton -> {

                surveyViewModel.stopAndSaveActiveSurvey()

                binding.ipaQuestionnaireFragmentViewPager.currentItem = 0

                // Go to root fragment
                requireActivity().supportFragmentManager.popBackStack(
                    null,
                    FragmentManager.POP_BACK_STACK_INCLUSIVE
                )
            }
        }
    }

    private fun setInputScreenListeners(
        hoursEditText: EditText?,
        minutesEditText: EditText?,
        maxHours: Int
    ) {
        hoursEditText?.addTextChangedListener(object : SimpleTextWatcher(maxHours) {
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (inputScreenValidationCheck(maxHours).isNotEmpty()) {
                    swipeControlTouchListener.setSwipeDirection(SwipeDirection.ALL)
                } else {
                    swipeControlTouchListener.setSwipeDirection(SwipeDirection.LEFT)
                }
            }
        })

        minutesEditText?.addTextChangedListener(object : SimpleTextWatcher(maxHours) {
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (inputScreenValidationCheck(maxHours).isNotEmpty()) {
                    swipeControlTouchListener.setSwipeDirection(SwipeDirection.ALL)
                } else {
                    swipeControlTouchListener.setSwipeDirection(SwipeDirection.LEFT)
                }
            }
        })
    }

    private fun setRadioButtonScreenListeners(
        view: View?,
        daysEditText: EditText?,
        hoursEditText: EditText?,
        minutesEditText: EditText?,
        radioButtonNone: RadioButton?,
        radioButtonDays: RadioButton?
    ) {
        daysEditText?.addTextChangedListener(object : SimpleTextWatcher() {
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (radioButtonScreenValidationCheck(view).isNotEmpty()) {
                    swipeControlTouchListener.setSwipeDirection(SwipeDirection.ALL)
                } else {
                    swipeControlTouchListener.setSwipeDirection(SwipeDirection.LEFT)
                }
            }
        })
        hoursEditText?.addTextChangedListener(object : SimpleTextWatcher() {
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (radioButtonScreenValidationCheck(view).isNotEmpty()) {
                    swipeControlTouchListener.setSwipeDirection(SwipeDirection.ALL)
                } else {
                    swipeControlTouchListener.setSwipeDirection(SwipeDirection.LEFT)
                }
            }
        })
        minutesEditText?.addTextChangedListener(object : SimpleTextWatcher() {
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (radioButtonScreenValidationCheck(view).isNotEmpty()) {
                    swipeControlTouchListener.setSwipeDirection(SwipeDirection.ALL)
                } else {
                    swipeControlTouchListener.setSwipeDirection(SwipeDirection.LEFT)
                }
            }
        })
        radioButtonNone?.setOnClickListener {
            swipeControlTouchListener.setSwipeDirection(SwipeDirection.ALL)
            hideHiddenScreenItems(view)
        }
        radioButtonDays?.setOnClickListener {
            if (daysEditText?.text.toString().isEmpty() || hoursEditText?.text.toString()
                .isEmpty() || minutesEditText?.text.toString().isEmpty()
            ) {
                swipeControlTouchListener.setSwipeDirection(SwipeDirection.LEFT)
            } else {
                swipeControlTouchListener.setSwipeDirection(SwipeDirection.ALL)
            }
            showHiddenScreenItems(view)
        }
    }

    private fun addIPAQuestionnaireRadioButtonQuestions() {
        fragmentsPagerAdapter.addFragment(
            QuestionnaireIPARadioButtonQuestions.newInstance(
                getString(R.string.IPA_questionnaire_screen_question_2_number),
                getString(R.string.IPA_questionnaire_screen_question_2_description),
                getString(R.string.IPA_questionnaire_screen_question_2_question),
                getString(R.string.IPA_questionnaire_screen_question_2_hidden_question)
            )
        )
        fragmentsPagerAdapter.addFragment(
            QuestionnaireIPARadioButtonQuestions.newInstance(
                getString(R.string.IPA_questionnaire_screen_question_3_number),
                getString(R.string.IPA_questionnaire_screen_question_3_description),
                getString(R.string.IPA_questionnaire_screen_question_3_question),
                getString(R.string.IPA_questionnaire_screen_question_3_hidden_question)
            )
        )
        fragmentsPagerAdapter.addFragment(
            QuestionnaireIPARadioButtonQuestions.newInstance(
                getString(R.string.IPA_questionnaire_screen_question_4_number),
                getString(R.string.IPA_questionnaire_screen_question_4_description),
                getString(R.string.IPA_questionnaire_screen_question_4_question),
                getString(R.string.IPA_questionnaire_screen_question_4_hidden_question)
            )
        )
    }

    private fun convertToMinutes(value: String): Int {
        return if (value.indexOf(":") < 0)
            Integer.parseInt(value)
        else
            Integer.parseInt(value.substring(0, 1)) *
                (
                    Integer.parseInt(value.substring(2, 4)) * 60 +
                        Integer.parseInt(value.substring(5))
                    )
    }

    private fun calculateResult() {
        val assesmentData = surveyViewModel.assessmentData!!
        val walking = convertToMinutes(assesmentData[2]!!) * 3.3
        val moderate = convertToMinutes(assesmentData[3]!!) * 4
        val vigorous = convertToMinutes(assesmentData[4]!!) * 8
        surveyViewModel.setScore((walking + moderate + vigorous).roundToLong())
    }

    private fun increaseViewPagerCurrentItem() {
        if (binding.ipaQuestionnaireFragmentViewPager.currentItem == fragmentsPagerAdapter.itemCount - 1) {
            return
        } else if (binding.ipaQuestionnaireFragmentViewPager.currentItem == fragmentsPagerAdapter.itemCount - 2) {
            calculateResult()

            binding.ipaQuestionnaireFragmentViewPager.setCurrentItem(
                binding.ipaQuestionnaireFragmentViewPager.currentItem + 1,
                true
            )
            val currentItem = binding.ipaQuestionnaireFragmentViewPager.currentItem
            // Update the final score.
            if (currentItem == fragmentsPagerAdapter.itemCount - 1) {
                try {
                    val fragment = fragmentsPagerAdapter.createFragment(currentItem)
                    if (fragment is IIPAQuestionnaireEndScreen) {
                        val score =
                            surveyViewModel.getScore().toString()
                        fragment.updateScoreValue(score)
                    }
                } catch (exception: Exception) {
                    exception.printStackTrace()
                }
            }
        } else {
            binding.ipaQuestionnaireFragmentViewPager.setCurrentItem(
                binding.ipaQuestionnaireFragmentViewPager.currentItem + 1,
                true
            )
        }
    }

    private fun decreaseViewPagerCurrentItem() {
        if (binding.ipaQuestionnaireFragmentViewPager.currentItem == 0) {
            requireActivity().onBackPressed()
        } else {
            binding.ipaQuestionnaireFragmentViewPager.setCurrentItem(
                binding.ipaQuestionnaireFragmentViewPager.currentItem - 1,
                true
            )
        }
    }

    private fun inputScreenValidationCheck(maxHours: Int = 23): String {
        val validationHours = hoursValidationCheckInput(maxHours)
        val validationMinutes = minutesValidationCheckInput()
        return if (validationHours >= 0 && validationMinutes >= 0)
            "${validationHours.toString().padStart(2, '0')}:${
            validationMinutes.toString().padStart(2, '0')
            }"
        else
            ""
    }

    private fun hoursValidationCheckInput(maxHours: Int): Int {
        val currentItem = binding.ipaQuestionnaireFragmentViewPager.currentItem
        try {
            val fragment = fragmentsPagerAdapter.createFragment(currentItem)
            if (fragment is IIPAQuestionnaireInputScreen) {
                return fragment.hoursValidationCheckInput(maxHours)
            }
        } catch (exception: Exception) {
            exception.printStackTrace()
        }
        return -1
    }

    private fun minutesValidationCheckInput(): Int {
        val currentItem = binding.ipaQuestionnaireFragmentViewPager.currentItem
        try {
            val fragment = fragmentsPagerAdapter.createFragment(currentItem)
            if (fragment is IIPAQuestionnaireInputScreen) {
                return fragment.minutesValidationCheckInput()
            }
        } catch (exception: Exception) {
            exception.printStackTrace()
        }
        return -1
    }

    private fun radioButtonScreenValidationCheck(view: View?): String {
        val validationDays = daysValidationCheckRadio(view)
        val validationHours = hoursValidationCheckRadio(view)
        val validationMinutes = minutesValidationCheckRadio(view)
        return if (validationDays >= 1 && validationHours >= 0 && validationMinutes >= 0)
            "$validationDays:${
            validationHours.toString().padStart(2, '0')
            }:${validationMinutes.toString().padStart(2, '0')}"
        else
            ""
    }

    private fun hoursValidationCheckRadio(view: View?): Int {
        val hoursInput: EditText? =
            view?.findViewById(R.id.ipa_questionnaire_screen_radio_button_question_edit_text_hours)

        if (hoursInput?.text.toString().isEmpty()) {
            hoursInput?.error = getString(R.string.input_empty_field_message)
        } else {
            val hours = Integer.parseInt(hoursInput?.text.toString())
            if (hours > 23 || hours < 0)
                hoursInput?.error = getString(R.string.invalid_number_hours)
            else
                return hours
        }
        return -1
    }

    private fun minutesValidationCheckRadio(view: View?): Int {
        val minutesInput: EditText? =
            view?.findViewById(R.id.ipa_questionnaire_screen_radio_button_question_edit_text_minutes)

        if (minutesInput?.text.toString().isEmpty()) {
            minutesInput?.error = getString(R.string.input_empty_field_message)
        } else {
            val minutes = Integer.parseInt(minutesInput?.text.toString())
            if (minutes > 59 || minutes < 0)
                minutesInput?.error = getString(R.string.invalid_number_minutes)
            else
                return minutes
        }
        return -1
    }

    private fun daysValidationCheckRadio(view: View?): Int {
        val daysInput: EditText? =
            view?.findViewById(R.id.ipa_questionnaire_screen_radio_button_question_edit_text_days)

        if (daysInput?.text.toString().isEmpty()) {
            daysInput?.error = getString(R.string.input_empty_field_message)
        } else {
            val days = Integer.parseInt(daysInput?.text.toString())
            if (days > 7 || days < 1)
                daysInput?.error = getString(R.string.invalid_number_days)
            else
                return days
        }
        return -1
    }

    private fun showHiddenScreenItems(view: View?) {
        view?.findViewById<EditText>(R.id.ipa_questionnaire_screen_radio_button_question_edit_text_days)?.visibility =
            View.VISIBLE
        view?.findViewById<TextView>(R.id.ipa_questionnaire_screen_radio_button_question_hidden_question)?.visibility =
            View.VISIBLE
        view?.findViewById<LinearLayout>(R.id.ipa_questionnaire_screen_radio_button_question_hidden_layout)?.visibility =
            View.VISIBLE
    }

    private fun hideHiddenScreenItems(view: View?) {
        view?.findViewById<EditText>(R.id.ipa_questionnaire_screen_radio_button_question_edit_text_days)?.visibility =
            View.GONE
        view?.findViewById<TextView>(R.id.ipa_questionnaire_screen_radio_button_question_hidden_question)?.visibility =
            View.GONE
        view?.findViewById<LinearLayout>(R.id.ipa_questionnaire_screen_radio_button_question_hidden_layout)?.visibility =
            View.GONE
    }
}
