/*
 *  Copyright (c) 2020, Comtrade
 *  All rights reserved.
 *
 *  Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of the <organization> nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *   DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  Package: com.comtrade.vinciapp.view.widget.devicesSetup.view
 *  File: DevicesSetupFragment.kt
 *  Author: Milenko Bojanic <milenko.bojanic@comtrade.com>
 *
 *  Description: Fragment screen for initial setup of devices
 *
 *  History: 8/11/20 Milenko  Initial code
 *
 *
 */

package com.comtrade.vinciapp.view.widget.devicesSetup.view

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.VinciApp
import com.comtrade.vinciapp.base.BaseFragment
import com.comtrade.vinciapp.databinding.FragmentDevicesSetupBinding
import com.comtrade.vinciapp.util.AlertDialogUtils
import com.comtrade.vinciapp.view.activity.main.view.MainActivity
import com.comtrade.vinciapp.view.widget.devicesSetup.core.DevicesSetupPresenter
import com.comtrade.vinciapp.view.widget.devicesSetup.core.contract.DevicesSetupContract
import javax.inject.Inject

class DevicesSetupFragment : BaseFragment(), DevicesSetupContract.View, View.OnClickListener {

    private var _binding: FragmentDevicesSetupBinding? = null
    private val binding get() = _binding!!

    private val presenter: DevicesSetupContract.Presenter by lazy {
        DevicesSetupPresenter()
    }

    @Inject
    lateinit var prefs: IPrefs

    companion object {
        const val TAG: String = "DevicesSetupFragment"

        fun newInstance(): DevicesSetupFragment {
            return DevicesSetupFragment()
        }
    }

    @SuppressLint("VisibleForTests")
    override fun onAttach(context: Context) {
        super.onAttach(context)
        (context.applicationContext as VinciApp).getAppComponent()
            .inject(this@DevicesSetupFragment)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentDevicesSetupBinding.inflate(inflater, container, false)
        return binding.root
    }

    private fun showMainScreen() {
        activity?.apply {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //        SERVER
        // presenter.getUserInfo(this.requireContext())
        setListeners()
        if (prefs.isDeviceAdded) {
            binding.devicesSetupFragmentAddDeviceButton.setCompoundDrawablesWithIntrinsicBounds(
                0,
                0,
                R.drawable.ic_checked,
                0
            )
        }
        if (prefs.isQuestionnaireStarted) {
            binding.devicesSetupFragmentStartWithQuestionnaireButton.setCompoundDrawablesWithIntrinsicBounds(
                0,
                0,
                R.drawable.ic_checked,
                0
            )
        }
        if (prefs.isEmergencyNumberSet) {
            binding.devicesSetupFragmentSetEmergencyNumber.setCompoundDrawablesWithIntrinsicBounds(
                0,
                0,
                R.drawable.ic_checked,
                0
            )
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun setListeners() {
        binding.devicesSetupFragmentProfileImage.setOnClickListener(this)
        binding.devicesSetupFragmentAddDeviceButton.setOnClickListener(this)
        binding.devicesSetupFragmentStartWithQuestionnaireButton.setOnClickListener(this)
        binding.devicesSetupFragmentSetupLaterButton.setOnClickListener(this)
        binding.devicesSetupFragmentSetEmergencyNumber.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
        when (view) {
            binding.devicesSetupFragmentProfileImage -> {
                presenter.showProfilePictureScreen(requireActivity().supportFragmentManager)
            }
            binding.devicesSetupFragmentAddDeviceButton -> {
                presenter.showAddDeviceScreen(requireActivity().supportFragmentManager)
            }
            binding.devicesSetupFragmentStartWithQuestionnaireButton -> {
                presenter.showQuestionnaireScreen(
                    requireActivity().supportFragmentManager
                )
            }
            binding.devicesSetupFragmentSetupLaterButton -> {
                showMainScreen()
            }
            binding.devicesSetupFragmentSetEmergencyNumber -> {
                presenter.showSettingEmergencyNumberScreen(requireActivity().supportFragmentManager)
            }
        }
    }

    override fun showUnexpectedErrorMessage(message: String) {
        AlertDialogUtils.showUnexpectedErrorAlertDialog(this.requireContext(), message)
    }
}
