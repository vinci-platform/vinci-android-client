/*
 *  Copyright (c) 2020, Comtrade
 *  All rights reserved.
 *
 *  Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of the <organization> nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *   DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  Package: com.comtrade.vinciapp.view.widget.chooseContacts.view
 *  File: ChooseContactsFragment.kt
 *  Author: Milenko Bojanic <milenko.bojanic@comtrade.com>
 *
 *  Description: Fragment screen for choosing contacts
 *
 *  History: 8/11/20 Milenko  Initial code
 *
 *
 */

package com.comtrade.vinciapp.view.widget.inviteFriends.view

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.recyclerview.widget.LinearLayoutManager
import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.entities.user.Contact
import com.comtrade.vinciapp.AppConstants
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.VinciApp
import com.comtrade.vinciapp.base.BaseFragment
import com.comtrade.vinciapp.databinding.FragmentInviteFriendsBinding
import com.comtrade.vinciapp.util.AlertDialogUtils
import com.comtrade.vinciapp.view.widget.inviteFriends.view.adapter.InviteFriendsAdapter
import javax.inject.Inject

class InviteFriendsFragment :
    BaseFragment(),
    View.OnClickListener {

    private var _binding: FragmentInviteFriendsBinding? = null
    private val binding get() = _binding!!

    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var adapter: InviteFriendsAdapter
    private lateinit var smsBody: String

    private var contactList: MutableList<Contact> = mutableListOf()

    @Inject
    lateinit var prefs: IPrefs

    companion object {
        const val TAG: String = "InviteFriendsFragment"

        const val SMS_BODY_KEY: String = "SMS_BODY_KEY"

        fun newInstance(smsBody: String): InviteFriendsFragment {
            val fragment = InviteFriendsFragment()
            val arguments = bundleOf(SMS_BODY_KEY to smsBody)
            fragment.arguments = arguments
            return fragment
        }
    }

    @SuppressLint("VisibleForTests")
    override fun onAttach(context: Context) {
        super.onAttach(context)
        (context.applicationContext as VinciApp).getAppComponent()
            .inject(this@InviteFriendsFragment)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentInviteFriendsBinding.inflate(inflater, container, false)
        arguments?.getString(SMS_BODY_KEY)?.let {
            smsBody = it
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        contactList = mutableListOf()
        contactList.addAll(prefs.userFriends)
        setListeners()
        setAdapter()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun setListeners() {
        binding.inviteFriendsFragmentBackButton.setOnClickListener(this)
        binding.inviteFriendsFragmentInviteButton.setOnClickListener(this)
    }

    private fun setAdapter() {
        linearLayoutManager = LinearLayoutManager(requireContext())
        binding.inviteFriendsFragmentListView.layoutManager = linearLayoutManager
        adapter = InviteFriendsAdapter(contactList)
        binding.inviteFriendsFragmentListView.adapter = adapter
    }

    override fun onClick(view: View?) {

        when (view) {
            binding.inviteFriendsFragmentBackButton -> {
                requireActivity().supportFragmentManager.popBackStackImmediate()
            }
            binding.inviteFriendsFragmentInviteButton -> {
                openSmsMessageApp(smsBody)
            }
        }
    }

    private fun openSmsMessageApp(smsBody: String) {
        val phoneNumbers = adapter.getListOfNumbers()
        if (phoneNumbers.isNotEmpty()) {
            val smsIntent = Intent(Intent.ACTION_SENDTO)
            smsIntent.data = Uri.parse(AppConstants.SMS_TO + ":" + phoneNumbers)
            smsIntent.putExtra(
                AppConstants.SMS_BODY,
                smsBody

            )
            if (smsIntent.resolveActivity(requireActivity().packageManager) != null) {
                requireActivity().startActivity(smsIntent)
            } else {
                AlertDialogUtils.showInfoAlertDialog(
                    requireContext(),
                    getString(R.string.shared_unexpected_error_title),
                    getString(R.string.sms_message_screen_error_message)
                )
            }
        }
    }
}
