/*
 *  Copyright (c) 2020, Comtrade
 *  All rights reserved.
 *
 *  Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of the <organization> nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *   DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  Package: com.comtrade.vinciapp.view.widget.nextEventList.view
 *  File: NextEventListFragment.kt
 *  Author: Sofija Andjelkovic <sofija.andjelkovic@comtrade.com>
 *
 *  Description: screen with list of next events
 *
 *  History: 8/27/20 Sofija Initial code
 *
 *
 */

package com.comtrade.vinciapp.view.widget.nextEventList.view

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.drawable.ClipDrawable.HORIZONTAL
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.CheckBox
import android.widget.RadioGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.entities.event.EventEntity
import com.comtrade.domain_ui.state.RequestUIState
import com.comtrade.feature_events.di.EventsComponent
import com.comtrade.feature_events.ui.events.EventsViewModel
import com.comtrade.feature_events.ui.events.SaveEventViewModel
import com.comtrade.feature_events.ui.events.UpdateEventViewModel
import com.comtrade.feature_events.ui.events.UserEventRecordsUIState
import com.comtrade.vinciapp.AppConstants
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.VinciApp
import com.comtrade.vinciapp.base.BaseFragment
import com.comtrade.vinciapp.databinding.FragmentNextEventListBinding
import com.comtrade.vinciapp.util.AlertDialogUtils
import com.comtrade.vinciapp.view.widget.nextEventList.core.NextEventListPresenter
import com.comtrade.vinciapp.view.widget.nextEventList.core.contract.NextEventListContract
import com.comtrade.vinciapp.view.widget.nextEventList.view.adapter.NextEventListAdapter
import com.kunzisoft.switchdatetime.SwitchDateTimeDialogFragment
import java.text.DateFormat
import java.time.Instant
import java.util.*
import javax.inject.Inject

class NextEventListFragment :
    BaseFragment(),
    NextEventListContract.View,
    View.OnClickListener,
    NextEventListAdapter.NextEventItemOnClickListener {

    private var _binding: FragmentNextEventListBinding? = null
    private val binding get() = _binding!!

    private lateinit var linearLayoutManager: LinearLayoutManager

    private var dateTimeFragment: SwitchDateTimeDialogFragment? = null
    private var postponeDialog: Dialog? = null
    private var dismissDialog: Dialog? = null

    private var adapter: NextEventListAdapter? = null

    private val presenter: NextEventListContract.Presenter by lazy { NextEventListPresenter() }

    @Inject
    lateinit var prefs: IPrefs

    private val eventsViewModel: EventsViewModel by viewModels {
        getEventsComponent().provideEventsViewModelFactory()
    }

    private val saveEventsViewModel: SaveEventViewModel by viewModels {
        getEventsComponent().provideSaveEventViewModelFactory()
    }

    private val updateEventsViewModel: UpdateEventViewModel by viewModels {
        getEventsComponent().provideUpdateEventViewModelFactory()
    }

    companion object {
        const val TAG: String = "NextEventListFragment"

        fun newInstance(): NextEventListFragment {
            return NextEventListFragment()
        }
    }

    @SuppressLint("VisibleForTests")
    override fun onAttach(context: Context) {
        super.onAttach(context)
        (context.applicationContext as VinciApp).getAppComponent()
            .inject(this@NextEventListFragment)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentNextEventListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val itemDecor = DividerItemDecoration(requireContext(), HORIZONTAL)
        binding.nextEventListView.addItemDecoration(itemDecor)
        getUpcomingEvents()
        setEventPersistenceObservers()
        setEventUpdateObservers()
        setListeners()
    }

    override fun onPause() {
        super.onPause()
        // Closing here because of save instance state call in onStop()
        dateTimeFragment?.apply {
            if (isAdded) {
                dismiss()
            }
        }
    }

    override fun onStop() {
        super.onStop()

        postponeDialog?.apply {
            if (isShowing) {
                dismiss()
            }
        }
        dismissDialog?.apply {
            if (isShowing) {
                dismiss()
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun getUpcomingEvents() {

        eventsViewModel.eventRecords.observe(viewLifecycleOwner) {
            when (it) {
                UserEventRecordsUIState.OnEmptyDataSet -> {}
                is UserEventRecordsUIState.OnError -> {
                    it.message?.let { message ->
                        showShortToast(message)
                    }
                }
                UserEventRecordsUIState.OnLoading -> {}
                is UserEventRecordsUIState.OnSuccess -> {
                    adapter =
                        NextEventListAdapter(it.data.toMutableList(), this@NextEventListFragment)
                    linearLayoutManager = LinearLayoutManager(requireContext())
                    binding.nextEventListView.layoutManager = linearLayoutManager
                    binding.nextEventListView.adapter = adapter
                }
            }
        }

        eventsViewModel.getUserEventRecords(
            prefs.userId,
            Instant.now().toEpochMilli()
        )
    }

    private fun setEventPersistenceObservers() {
        saveEventsViewModel.storeEventEntity.observe(viewLifecycleOwner) {
            when (it) {
                RequestUIState.DefaultState -> {}
                is RequestUIState.OnError -> {
                    it.message.let { message ->
                        if (message != null) {
                            showShortToast(message)
                        } else {
                            showShortToast(getString(R.string.error_problem_creating_new_event))
                        }
                    }
                    saveEventsViewModel.resetState()
                }
                RequestUIState.OnLoading -> {}
                is RequestUIState.OnSuccess -> {
                    saveEventsViewModel.resetState()
                }
            }
        }
    }

    private fun setEventUpdateObservers() {
        updateEventsViewModel.updateEventEntity.observe(viewLifecycleOwner) {
            when (it) {
                RequestUIState.DefaultState -> {}
                is RequestUIState.OnError -> {
                    it.message.let { message ->
                        if (message != null) {
                            showShortToast(message)
                        } else {
                            val errorMessage = getString(R.string.next_event_error_update)
                            showShortToast(errorMessage)
                        }
                    }
                    updateEventsViewModel.resetState()
                }
                RequestUIState.OnLoading -> {}
                is RequestUIState.OnSuccess -> {
                    updateEventsViewModel.resetState()
                }
            }
        }
    }

    private fun setListeners() {
        binding.nextEventViewFragmentAddActivityButton.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v) {
            binding.nextEventViewFragmentAddActivityButton -> {
                presenter.openOutdoorActivitiesScreen(requireActivity().supportFragmentManager)
            }
        }
    }

    override fun showPostponeDialog(eventEntity: EventEntity, itemAdapterPosition: Int) {
        postponeDialog?.apply {
            if (isShowing) {
                dismiss()
            }
        }
        requireContext().let {
            postponeDialog = Dialog(it)
            postponeDialog?.apply {
                requestWindowFeature(Window.FEATURE_NO_TITLE)
                setCancelable(false)
                setContentView(R.layout.postpone_activity_dialog)
                val saveButton =
                    findViewById<Button>(R.id.postpone_activity_dialog_save_button)
                val cancelButton =
                    findViewById<Button>(R.id.postpone_activity_dialog_cancel_button)
                val radioGroup =
                    findViewById<RadioGroup>(R.id.postpone_activity_dialog_radio_group)
                val dateTimePickerLayout =
                    findViewById<ConstraintLayout>(R.id.postpone_activity_dialog_date_picker_layout)
                val checkBoxNotifyFriends =
                    findViewById<CheckBox>(R.id.postpone_activity_dialog_notify_friends_checkbox)
                val activityText = findViewById<TextView>(R.id.postpone_activity_activity_text)
                val timeText = findViewById<TextView>(R.id.postpone_activity_time_text)
                val dateText = findViewById<TextView>(R.id.postpone_activity_date_text)
                val dateTimePickerText =
                    findViewById<TextView>(R.id.postpone_activity_dialog_datetime_spinner)
                var postponeDate: Date? = null
                var postponeTimeText: String? = getString(R.string.next_event_postpone_15_minutes)

                activityText.text =
                    it.getString(R.string.next_event_activity_placeholder, eventEntity.title)
                timeText.text =
                    it.getString(R.string.next_event_time_placeholder, eventEntity.getTime())
                dateText.text =
                    it.getString(R.string.next_event_date_placeholder, eventEntity.getDate())
                val activityName: String = eventEntity.title
                var postponeSelectionId = radioGroup.checkedRadioButtonId
                radioGroup.setOnCheckedChangeListener { _, checkedId ->
                    postponeSelectionId = checkedId
                    if (checkedId == R.id.postpone_activity_dialog_radio_button_custom) {
                        dateTimePickerLayout.visibility = View.VISIBLE
                    } else {
                        dateTimePickerLayout.visibility = View.GONE
                    }
                }

                dateTimePickerText.setOnClickListener {
                    showCalendar { date ->
                        dateTimePickerText.text =
                            DateFormat.getDateTimeInstance(DateFormat.DATE_FIELD, DateFormat.SHORT)
                                .format(date)
                        postponeTimeText =
                            DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.SHORT)
                                .format(date)
                        postponeDate = date
                    }
                }

                saveButton.setOnClickListener { _ ->

                    val eventRecordUpdate = when (postponeSelectionId) {
                        R.id.postpone_activity_dialog_radio_button_15_minutes -> {
                            postponeTimeText = it.getString(R.string.next_event_postpone_15_minutes)
                            val existingTimeStamp = eventEntity.timestamp + 15 * 60 * 1000
                            eventEntity.copy(timestamp = existingTimeStamp)
                        }
                        R.id.postpone_activity_dialog_radio_button_30_minutes -> {
                            postponeTimeText = it.getString(R.string.next_event_postpone_30_minutes)
                            val existingTimeStamp = eventEntity.timestamp + 30 * 60 * 1000
                            eventEntity.copy(timestamp = existingTimeStamp)
                        }
                        R.id.postpone_activity_dialog_radio_button_1_hour -> {
                            postponeTimeText = it.getString(R.string.next_event_postpone_1_hour)
                            val existingTimeStamp = eventEntity.timestamp + 60 * 60 * 1000
                            eventEntity.copy(timestamp = existingTimeStamp)
                        }
                        else ->
                            eventEntity.copy(timestamp = postponeDate!!.toInstant().toEpochMilli())
                    }
                    // Update existing event
                    updateEventsViewModel.updateEventEntity(eventRecordUpdate.copy(deleted = true))
                    // Persists the updated event
                    val eventEntityInsert = eventRecordUpdate.copy(id = 0, syncedWithServer = false)
                    saveEventsViewModel.storeEventEntity(eventEntityInsert)

                    adapter?.updateItemAtPosition(eventRecordUpdate, itemAdapterPosition)
                    val notifyFriends: Boolean = checkBoxNotifyFriends.isChecked
                    val textMessage: String =
                        it.getString(R.string.next_event_i_am_postponing_activity) + "\n $activityName " + it.getString(
                            R.string.next_event_i_am_postponing_activity_for
                        ) + " $postponeTimeText"
                    if (notifyFriends) {
                        sendSMSMessage(textMessage)
                    }
                    dismiss()
                }
                cancelButton.setOnClickListener { dismiss() }
                show()
            }
        }
    }

    override fun showDismissDialog(eventEntity: EventEntity, itemAdapterPosition: Int) {
        dismissDialog?.apply {
            if (isShowing) {
                dismiss()
            }
        }
        requireContext().let {
            dismissDialog = Dialog(requireContext())
            dismissDialog?.apply {

                requestWindowFeature(Window.FEATURE_NO_TITLE)
                setCancelable(false)
                setContentView(R.layout.dismiss_activity_dialog)
                val checkBoxNotifyFriends =
                    findViewById<CheckBox>(R.id.dismiss_activity_dialog_notify_friends_checkbox)
                val yesButton =
                    findViewById<Button>(R.id.dismiss_activity_dialog_yes_button)
                val noButton =
                    findViewById<Button>(R.id.dismiss_activity_dialog_no_button)
                val activityText =
                    findViewById<TextView>(R.id.dismiss_activity_dialog_activity_text)
                val timeText = findViewById<TextView>(R.id.dismiss_activity_dialog_time_text)
                val dateText = findViewById<TextView>(R.id.dismiss_activity_dialog_date_text)
                activityText.text =
                    it.getString(R.string.next_event_activity_placeholder, eventEntity.title)
                timeText.text =
                    it.getString(R.string.next_event_time_placeholder, eventEntity.getTime())
                dateText.text =
                    it.getString(R.string.next_event_date_placeholder, eventEntity.getDate())
                yesButton.setOnClickListener { _ ->
                    val eventEntityUpdate = eventEntity.copy(deleted = true)
                    // TODO Not updating correctly in the flow
                    updateEventsViewModel.updateEventEntity(eventEntityUpdate)
                    adapter?.removeItemAtPosition(itemAdapterPosition)
                    val notifyFriends: Boolean = checkBoxNotifyFriends.isChecked
                    if (notifyFriends) {
                        sendSMSMessage(
                            it.getString(R.string.next_event_dismiss_canceling_text) + eventEntityUpdate.title + "\n" + eventEntityUpdate.getDate() + "\n" + eventEntityUpdate.getTime()
                        )
                    }
                    dismiss()
                }
                noButton.setOnClickListener {
                    dismiss()
                }
                show()
            }
        }
    }

    override fun openSmsMessageApp(eventEntity: EventEntity) {
        requireContext().let {
            val textMessage: String = it.getString(R.string.next_event_join_me) + "\n" +
                it.getString(
                    R.string.next_event_activity_placeholder,
                    eventEntity.title
                ) + "\n" +
                it.getString(
                    R.string.next_event_date_placeholder,
                    eventEntity.getDate()
                ) + "\n" +
                it.getString(
                    R.string.next_event_time_placeholder,
                    eventEntity.getTime()
                ) + "\n"
            sendSMSMessage(textMessage)
        }
    }

    /**
     * Launch SMS sending external intent.
     */
    private fun sendSMSMessage(textMessage: String) {
        requireContext().let {
            val smsIntent = Intent(Intent.ACTION_SENDTO)
            smsIntent.data = Uri.parse(AppConstants.SMS_TO)
            smsIntent.putExtra(
                AppConstants.SMS_BODY,
                textMessage
            )
            if (smsIntent.resolveActivity(it.packageManager) != null) {
                it.startActivity(smsIntent)
            } else {
                AlertDialogUtils.showInfoAlertDialog(
                    it,
                    it.getString(R.string.shared_unexpected_error_title),
                    it.getString(R.string.sms_message_screen_error_message)
                )
            }
        }
    }

    private fun showCalendar(block: (date: Date) -> Unit) {

        dateTimeFragment?.apply {
            if (isAdded) {
                dismiss()
            }
        }

        dateTimeFragment = SwitchDateTimeDialogFragment.newInstance(
            getString(R.string.next_event_postpone_for_text),
            getString(R.string.common_button_set),
            getString(R.string.common_button_cancel)
        )

        dateTimeFragment?.apply {
            setOnButtonClickListener(object :
                    SwitchDateTimeDialogFragment.OnButtonWithNeutralClickListener {
                    override fun onPositiveButtonClick(date: Date) {
                        block(date)
                    }

                    override fun onNeutralButtonClick(date: Date?) {}
                    override fun onNegativeButtonClick(date: Date?) {}
                })

            val calendar = Calendar.getInstance()
            startAtCalendarView()
            setDefaultDateTime(
                GregorianCalendar(
                    calendar.get(Calendar.YEAR),
                    calendar.get(Calendar.MONTH),
                    calendar.get(Calendar.DAY_OF_MONTH),
                    calendar.get(Calendar.HOUR_OF_DAY),
                    calendar.get(Calendar.MINUTE),
                    calendar.get(Calendar.SECOND)
                ).time
            )
        }

        activity?.let {
            dateTimeFragment?.apply {
                if (isAdded) {
                    return@let
                }

                show(
                    it.supportFragmentManager,
                    "DatePickerDialog"
                )
            }
        }
    }
}

/**
 * Obtain the Dagger dependency component extension.
 */
@SuppressLint("VisibleForTests")
internal fun NextEventListFragment.getEventsComponent(): EventsComponent =
    (requireContext().applicationContext as VinciApp).getEventsComponent()
