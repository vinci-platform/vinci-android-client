/*
 *  Copyright (c) 2020, Comtrade
 *  All rights reserved.
 *
 *  Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of the <organization> nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *   DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  Package: com.comtrade.vinciapp.view.widget.createUser.view
 *  File: CreateUserFragment.kt
 *  Author: Milenko Bojanic <milenko.bojanic@comtrade.com>
 *
 *  Description: Fragment screen for creating new user
 *
 *  History: 8/11/20 Milenko  Initial code
 *
 *
 */

package com.comtrade.vinciapp.view.widget.createUser.view

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.fragment.app.viewModels
import com.comtrade.domain.entities.user.UserRoles
import com.comtrade.feature_account.ui.state.APICallSuccessUIState
import com.comtrade.feature_account.ui.user.CreateNewAccountViewModel
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.VinciApp
import com.comtrade.vinciapp.base.BaseFragment
import com.comtrade.vinciapp.databinding.ElevatedProgressViewBinding
import com.comtrade.vinciapp.databinding.FragmentCreateUserBinding
import com.comtrade.vinciapp.util.*
import com.comtrade.vinciapp.view.widget.createUser.core.contract.CreateUserContract
import com.comtrade.vinciapp.view.widget.inviteContacts.view.InviteContactsFragment

@SuppressLint("VisibleForTests")
class CreateUserFragment :
    BaseFragment(),
    CreateUserContract.View,
    View.OnClickListener,
    AdapterView.OnItemSelectedListener {

    private var _binding: FragmentCreateUserBinding? = null
    private val binding get() = _binding!!

    private var _bindingElevatedView: ElevatedProgressViewBinding? = null
    private val bindingElevatedView get() = _bindingElevatedView!!

    private lateinit var adapter: ArrayAdapter<String>
    private var editTextWatcher = EditTextWatcher()
    private lateinit var role: String

    private val viewModel: CreateNewAccountViewModel by viewModels {
        (requireContext().applicationContext as VinciApp).getAccountComponent()
            .provideCreateAccountViewModelFactory()
    }

    companion object {
        const val TAG: String = "CreateUserFragment"

        fun newInstance(): CreateUserFragment {
            return CreateUserFragment()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        addRoles()
        addTextWatchers()
        setViewModelObservers()
        setListeners()
        setTransformationMethod()
    }

    private fun addRoles() {
        adapter = ArrayAdapter(requireContext(), R.layout.simple_spinner_item)
        adapter.add(UserRoles.ROLE_PACIENT.name)
        adapter.add(UserRoles.ROLE_FAMILY.name)
        binding.createUserFragmentUserRoleSpinner.adapter = adapter
    }

    private fun addTextWatchers() {
        binding.createUserFragmentUsernameEditText.addTextChangedListener(editTextWatcher)
        binding.createUserFragmentCreatePasswordEditText.addTextChangedListener(editTextWatcher)
        binding.createUserFragmentRepeatPasswordEditText.addTextChangedListener(editTextWatcher)
        binding.createUserFragmentEmailEditText.addTextChangedListener(editTextWatcher)
    }

    private fun setTransformationMethod() {
        binding.createUserFragmentCreatePasswordEditText.transformationMethod =
            BiggerDotPasswordTransformationMethod()
        binding.createUserFragmentRepeatPasswordEditText.transformationMethod =
            BiggerDotPasswordTransformationMethod()
    }

    private fun setViewModelObservers() {
        viewModel.createNewAccount.observe(viewLifecycleOwner) {
            when (it) {
                is APICallSuccessUIState.OnError -> {
                    hideProgressBar()
                    it.message?.let { message ->
                        showShortToast(message)
                    }
                    viewModel.resetState()
                }
                APICallSuccessUIState.OnLoading -> {
                    showProgressBar()
                }
                is APICallSuccessUIState.OnSuccess -> {
                    hideProgressBar()
                    if (it.data) {
                        onCreateUserSuccess()
                    } else {
                        showShortToast(getString(R.string.shared_unexpected_error_title))
                    }
                    viewModel.resetState()
                }
                APICallSuccessUIState.DefaultState -> {}
            }
        }
    }

    private fun setListeners() {
        binding.createUserFragmentConfirmButton.setOnClickListener(this)
        binding.createUserFragmentUserRoleSpinner.onItemSelectedListener = this
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentCreateUserBinding.inflate(inflater, container, false)
        _bindingElevatedView = ElevatedProgressViewBinding.bind(binding.root)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onClick(view: View?) {
        when (view) {
            binding.createUserFragmentConfirmButton -> {
                validateInput()
            }
        }
    }

    private fun validateInput() {
        val username = binding.createUserFragmentUsernameEditText.text.toString()
        val email = binding.createUserFragmentEmailEditText.text.toString()
        val password = binding.createUserFragmentCreatePasswordEditText.text.toString()
        val repeatPassword = binding.createUserFragmentRepeatPasswordEditText.text.toString()

        if (TextUtils.isEmpty(username)) {
            showUsernameWarning()
            return
        }
        if (TextUtils.isEmpty(email)) {
            showEmailEmptyWarning()
            return
        }
        if (!EmailValidator.isValidEmail(email)) {
            showEmailNotValidWarning()
            return
        }
        if (password.length < 4) {
            showPasswordTooShortWarning()
        }
        if (password != repeatPassword) {
            showPasswordDoesNotMatchWarning()
            return
        }

        createUser(username, email, password, role)
    }

    /**
     * Invoke API call.
     */
    private fun createUser(login: String, email: String, password: String, role: String) {
        viewModel.createAccount(login, email, password, role)
    }

    private fun onCreateUserSuccess() {
        // Navigate to next screen.
        openInviteContactsScreen()
    }

    override fun showUsernameWarning() {
        ViewUtils.showView(binding.createUserFragmentUsernameWarning)
    }

    override fun showEmailEmptyWarning() {
        binding.createUserFragmentEmailWarning.text =
            getString(R.string.create_user_screen_email_empty_warning)
        ViewUtils.showView(binding.createUserFragmentEmailWarning)
    }

    override fun showEmailNotValidWarning() {
        binding.createUserFragmentEmailWarning.text =
            getString(R.string.create_user_screen_email_invalid_warning)
        ViewUtils.showView(binding.createUserFragmentEmailWarning)
    }

    override fun showPasswordTooShortWarning() {
        ViewUtils.showView(binding.createUserFragmentCreatePasswordWarning)
    }

    override fun showPasswordDoesNotMatchWarning() {
        ViewUtils.showView(binding.createUserFragmentRepeatPasswordWarning)
    }

    override fun openInviteContactsScreen() {
        requireActivity().supportFragmentManager
            .beginTransaction()
            .replace(
                R.id.prelogin_activity_content,
                InviteContactsFragment.newInstance(),
                InviteContactsFragment.TAG
            )
            .addToBackStack(null)
            .commit()
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        role = parent?.getItemAtPosition(position).toString()
    }

    override fun showHttpError(message: String?) {
        val title = getString(R.string.shared_error_code_title)
        val dialogMessage = message ?: getString(R.string.shared_unexpected_error_title)
        AlertDialogUtils.showInfoAlertDialog(this.requireContext(), title, dialogMessage)
    }

    override fun showProgressBar() {
        ViewUtils.showView(bindingElevatedView.containerProgress)
    }

    override fun hideProgressBar() {
        ViewUtils.hideView(bindingElevatedView.containerProgress)
    }

    inner class EditTextWatcher : SimpleTextWatcher() {
        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            super.onTextChanged(s, start, before, count)
            if (!TextUtils.isEmpty(binding.createUserFragmentUsernameEditText.text.toString()) && ViewUtils.isViewVisible(
                    binding.createUserFragmentUsernameWarning
                )
            ) {
                ViewUtils.hideView(binding.createUserFragmentUsernameWarning)
            }
            if (!TextUtils.isEmpty(binding.createUserFragmentEmailEditText.text.toString()) && ViewUtils.isViewVisible(
                    binding.createUserFragmentEmailWarning
                )
            ) {
                ViewUtils.hideView(binding.createUserFragmentEmailWarning)
            }
            if (!TextUtils.isEmpty(binding.createUserFragmentCreatePasswordEditText.text.toString()) && ViewUtils.isViewVisible(
                    binding.createUserFragmentCreatePasswordWarning
                )
            ) {
                ViewUtils.hideView(binding.createUserFragmentCreatePasswordWarning)
            }
            if (!TextUtils.isEmpty(binding.createUserFragmentRepeatPasswordEditText.text.toString()) && ViewUtils.isViewVisible(
                    binding.createUserFragmentRepeatPasswordWarning
                )
            ) {
                ViewUtils.hideView(binding.createUserFragmentRepeatPasswordWarning)
            }
        }
    }
}
