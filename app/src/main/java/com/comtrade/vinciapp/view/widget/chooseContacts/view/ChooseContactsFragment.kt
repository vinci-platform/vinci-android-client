/*
 *  Copyright (c) 2020, Comtrade
 *  All rights reserved.
 *
 *  Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of the <organization> nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *   DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  Package: com.comtrade.vinciapp.view.widget.chooseContacts.view
 *  File: ChooseContactsFragment.kt
 *  Author: Milenko Bojanic <milenko.bojanic@comtrade.com>
 *
 *  Description: Fragment screen for choosing contacts
 *
 *  History: 8/11/20 Milenko  Initial code
 *
 *
 */

package com.comtrade.vinciapp.view.widget.chooseContacts.view

import android.database.Cursor
import android.os.Bundle
import android.provider.ContactsContract
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.comtrade.domain.entities.user.Contact
import com.comtrade.vinciapp.base.BaseFragment
import com.comtrade.vinciapp.databinding.FragmentChooseContactsBinding
import com.comtrade.vinciapp.view.widget.chooseContacts.core.ChooseContactsPresenter
import com.comtrade.vinciapp.view.widget.chooseContacts.core.contract.ChooseContactsContract
import com.comtrade.vinciapp.view.widget.chooseContacts.view.adapter.ContactsAdapter

class ChooseContactsFragment : BaseFragment(), ChooseContactsContract.View, View.OnClickListener {

    private var _binding: FragmentChooseContactsBinding? = null
    private val binding get() = _binding!!

    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var adapter: ContactsAdapter

    private val presenter: ChooseContactsContract.Presenter by lazy { ChooseContactsPresenter() }

    private var contactList: MutableList<Contact> = mutableListOf()

    companion object {
        const val TAG: String = "ChooseContactsFragment"

        fun newInstance(): ChooseContactsFragment {
            return ChooseContactsFragment()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentChooseContactsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListeners()
        getContactsList()
        setAdapter()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun setListeners() {
        binding.chooseContactsFragmentBackButton.setOnClickListener(this)
        binding.chooseContactsFragmentNextButton.setOnClickListener(this)
    }

    private fun setAdapter() {
        linearLayoutManager = LinearLayoutManager(requireContext())
        binding.chooseContactsFragmentListView.layoutManager = linearLayoutManager
        adapter = ContactsAdapter(contactList)
        binding.chooseContactsFragmentListView.adapter = adapter
    }

    private fun getContactsList() {
        val cursor = requireActivity().contentResolver.query(
            ContactsContract.Contacts.CONTENT_URI,
            null,
            null,
            null,
            null
        )

        var numbers: Cursor? = null
        if ((cursor?.count ?: 0) > 0) {
            while (cursor != null && cursor.moveToNext()) {
                val id = cursor.getString(
                    cursor.getColumnIndexOrThrow(ContactsContract.Contacts._ID)
                )
                val name = cursor.getString(
                    cursor.getColumnIndexOrThrow(
                        ContactsContract.Contacts.DISPLAY_NAME
                    )
                )

                val hasPhone = cursor.getString(
                    cursor.getColumnIndexOrThrow(
                        ContactsContract.Contacts.HAS_PHONE_NUMBER
                    )
                )

                var number: String? = ""

                if (hasPhone.equals("1", ignoreCase = true)) {
                    numbers = requireActivity().contentResolver.query(
                        ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                        ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + id,
                        null, null
                    )
                    numbers?.moveToFirst()
                    number = numbers?.getString(numbers.getColumnIndexOrThrow("data1"))
                }

                val contact = Contact(id, name, number, false)
                contactList.add(contact)
            }
        }

        cursor?.close()
        numbers?.close()
    }

    override fun onClick(view: View?) {
        when (view) {
            binding.chooseContactsFragmentBackButton -> {
                presenter.backToInviteContactsScreen(requireActivity().supportFragmentManager)
            }
            binding.chooseContactsFragmentNextButton -> {
                presenter.showSmsMessageScreen(
                    requireActivity().supportFragmentManager,
                    adapter.getSelectedContacts()
                )
            }
        }
    }
}
