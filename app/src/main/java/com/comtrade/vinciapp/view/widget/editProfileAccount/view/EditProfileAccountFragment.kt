/*
 *  Copyright (c) 2020, Comtrade
 *  All rights reserved.
 *
 *  Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of the <organization> nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *   DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  Package: com.comtrade.vinciapp.view.widget.editProfileAccount.view
 *  File: EditProfileAccountFragment.kt
 *  Author: Sofija Andjelkovic <sofija.andjelkovic@comtrade.com>
 *
 *  Description: fragment for changing information about the user
 *
 *  History: 8/11/20 Sofija  Initial code
 *
 *
 */

package com.comtrade.vinciapp.view.widget.editProfileAccount.view

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.EditText
import androidx.fragment.app.viewModels
import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.entities.user.data.UpdateUserRequest
import com.comtrade.feature_account.ui.state.APICallSuccessUIState
import com.comtrade.feature_account.ui.user.UpdateAccountViewModel
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.VinciApp
import com.comtrade.vinciapp.base.BaseFragment
import com.comtrade.vinciapp.databinding.FragmentEditProfileAccountBinding
import com.comtrade.vinciapp.view.widget.editProfileAccount.core.contract.EditProfileAccountContract
import com.comtrade.vinciapp.view.widget.editProfileAccount.view.adapter.Gender
import javax.inject.Inject

@SuppressLint("VisibleForTests")
class EditProfileAccountFragment :
    BaseFragment(),
    EditProfileAccountContract.View,
    View.OnClickListener {

    private var _binding: FragmentEditProfileAccountBinding? = null
    private val binding get() = _binding!!

    private lateinit var genderAdapter: ArrayAdapter<String>
    private val tagEditProfile = "EditProfile"

    @Inject
    lateinit var prefs: IPrefs

    private val viewModel: UpdateAccountViewModel by viewModels {
        (requireContext().applicationContext as VinciApp).getAccountComponent()
            .provideUpdateAccountViewModelFactory()
    }

    companion object {
        const val TAG: String = "EditProfileAccountFragment"

        fun newInstance(): EditProfileAccountFragment {
            return EditProfileAccountFragment()
        }
    }

    @SuppressLint("VisibleForTests")
    override fun onAttach(context: Context) {
        super.onAttach(context)
        (context.applicationContext as VinciApp).getAppComponent()
            .inject(this@EditProfileAccountFragment)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentEditProfileAccountBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setViewModelObservers()

        setListeners()

        binding.editProfileAccountFirstNameText.setText(prefs.userFirstName)
        binding.editProfileAccountLastNameText.setText(prefs.userLastName)
        binding.editProfileAccountAddressText.setText(prefs.userAddress)
        binding.editProfileAccountPhoneText.setText(prefs.userPhone)
        binding.editProfileAccountEmailText.setText(prefs.userEmail)
        binding.editProfileAccountGenderSpinner.setSelection(genderAdapter.getPosition(prefs.userGender))
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun setViewModelObservers() {
        viewModel.updateAccount.observe(viewLifecycleOwner) {
            when (it) {
                is APICallSuccessUIState.OnError -> {
                    it.message?.let { message ->
                        showShortToast(message)
                    }
                }
                APICallSuccessUIState.OnLoading -> {}
                is APICallSuccessUIState.OnSuccess -> {
                    viewModel.resetState()
                    if (it.data) {
                        onUpdateUserSuccess()
                    } else {
                        showShortToast(getString(R.string.shared_unexpected_error_title))
                    }
                }
                APICallSuccessUIState.DefaultState -> {}
            }
        }
    }

    private fun setListeners() {
        binding.editProfileAccountSaveButton.setOnClickListener(this)
        genderAdapter = ArrayAdapter(requireContext(), R.layout.simple_spinner_item)
        genderAdapter.add(Gender.Male.name)
        genderAdapter.add(Gender.Female.name)
        binding.editProfileAccountGenderSpinner.adapter = genderAdapter
    }

    /**
     * Check i input field empty and display matching toast message.
     */
    private fun validateInputField(inputField: EditText, errorMessage: String): Boolean {
        if (inputField.text.toString().isBlank()) {
            inputField.requestFocus()
            inputField.error = errorMessage
            return false
        }
        return true
    }

    override fun onClick(view: View?) {
        when (view) {
            binding.editProfileAccountSaveButton -> {

                if (!validateInputField(
                        binding.editProfileAccountFirstNameText,
                        getString(R.string.edit_profile_account_screen_enter_first_name)
                    )
                ) {
                    return
                }
                val firstName = binding.editProfileAccountFirstNameText.text.toString()

                if (!validateInputField(
                        binding.editProfileAccountLastNameText,
                        getString(R.string.edit_profile_account_screen_enter_last_name)
                    )
                ) {
                    return
                }
                val lastName = binding.editProfileAccountLastNameText.text.toString()

                if (!validateInputField(
                        binding.editProfileAccountAddressText,
                        getString(R.string.edit_profile_account_screen_enter_address)
                    )
                ) {
                    return
                }
                val address = binding.editProfileAccountAddressText.text.toString()

                if (!validateInputField(
                        binding.editProfileAccountEmailText,
                        getString(R.string.edit_profile_account_screen_enter_email)
                    )
                ) {
                    return
                }
                val email = binding.editProfileAccountEmailText.text.toString()

                if (!validateInputField(
                        binding.editProfileAccountPhoneText,
                        getString(R.string.edit_profile_account_screen_enter_phone_number)
                    )
                ) {
                    return
                }
                val phone =
                    binding.editProfileAccountPhoneText.text.toString()
                val gender = binding.editProfileAccountGenderSpinner.selectedItem.toString()

                viewModel.updateAccount(
                    UpdateUserRequest(
                        prefs.userId,
                        prefs.userLogin,
                        firstName,
                        lastName,
                        gender,
                        address,
                        email,
                        phone,
                    )
                )
                prefs.userFirstName = firstName
                prefs.userLastName = lastName
                prefs.userAddress = address
                prefs.userPhone = phone
                prefs.userGender = gender
            }
        }
    }

    override fun onUpdateUserSuccess() {
        Log.d(tagEditProfile, "User updated successfully")
        showShortToast(getString(R.string.edit_profile_account_screen_update_success))
        requireActivity().supportFragmentManager.popBackStack()
    }

    override fun onUpdateUserFailure(throwable: Throwable, message: String?) {
        message?.let {
            Log.d(tagEditProfile, it)
        }
    }
}
