/*
 *  Copyright (c) 2020, Comtrade
 *  All rights reserved.
 *
 *  Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of the <organization> nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *   DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  Package: com.comtrade.vinciapp.view.widget.welcome.core
 *  File: WelcomePresenter.kt
 *  Author: Milenko Bojanic <milenko.bojanic@comtrade.com>
 *
 *  Description: welcome presenter
 *
 *  History: 8/11/20 Milenko  Initial code
 *
 *
 */

package com.comtrade.vinciapp.view.widget.welcome.core

import android.app.Activity
import android.content.Intent
import androidx.fragment.app.FragmentManager
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.base.BasePresenter
import com.comtrade.vinciapp.view.activity.main.view.MainActivity
import com.comtrade.vinciapp.view.widget.createUser.view.CreateUserFragment
import com.comtrade.vinciapp.view.widget.inviteGoogleContacts.view.InviteGoogleContactsFragment
import com.comtrade.vinciapp.view.widget.login.view.LoginFragment
import com.comtrade.vinciapp.view.widget.terms.view.TermsFragment
import com.comtrade.vinciapp.view.widget.welcome.core.contract.WelcomeContract

class WelcomePresenter
    : BasePresenter(), WelcomeContract.Presenter {

    override fun showLoginScreen(fragmentManager: FragmentManager) {
        changePreloginActivityFragment(
            fragmentManager,
            LoginFragment.newInstance(),
            LoginFragment.TAG
        )
    }

    override fun showCreateUserScreen(fragmentManager: FragmentManager) {
        changePreloginActivityFragment(
            fragmentManager,
            CreateUserFragment.newInstance(),
            CreateUserFragment.TAG
        )
    }

    override fun showInviteGoogleContactsScreen(fragmentManager: FragmentManager) {
        fragmentManager.beginTransaction()
            .replace(
                R.id.prelogin_activity_content,
                InviteGoogleContactsFragment.newInstance(),
                InviteGoogleContactsFragment.TAG
            )
            .addToBackStack(null)
            .commit()
    }

    override fun showTermsAndConditionsScreen(fragmentManager: FragmentManager) {
        changePreloginActivityFragment(
            fragmentManager,
            TermsFragment.newInstance(),
            TermsFragment.TAG
        )
    }

    override fun openMainScreen(activity: Activity) {
        val intent = Intent(activity, MainActivity::class.java)
        activity.startActivity(intent)
        activity.finish()
    }
}