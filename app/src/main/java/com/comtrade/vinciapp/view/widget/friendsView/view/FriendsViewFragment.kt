/*
 *  Copyright (c) 2020, Comtrade
 *  All rights reserved.
 *
 *  Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of the <organization> nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *   DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  Package: com.comtrade.vinciapp.view.widget.physicalActivities.view
 *  File: PhysicalActivitiesFragment.kt
 *  Author: Kosta Eric <kosta.eric@comtrade.com>
 *
 *  Description: Fragment for physical activity screen
 *
 *  History: 8/11/20 Kosta  Initial code
 *
 *
 */

package com.comtrade.vinciapp.view.widget.friendsView.view

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.net.Uri
import android.os.Bundle
import android.provider.ContactsContract
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.entities.user.Contact
import com.comtrade.domain_ui.state.RequestUIState
import com.comtrade.feature_friends.ui.GetFriendByPhoneViewModel
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.VinciApp
import com.comtrade.vinciapp.base.BaseFragment
import com.comtrade.vinciapp.databinding.FragmentFriendsViewBinding
import com.comtrade.vinciapp.util.AlertDialogUtils
import com.comtrade.vinciapp.view.widget.friendsView.adapter.FriendsListAdapter
import com.comtrade.vinciapp.view.widget.friendsView.core.FriendsViewPresenter
import com.comtrade.vinciapp.view.widget.friendsView.core.contract.FriendsViewContract
import javax.inject.Inject

@SuppressLint("VisibleForTests")
class FriendsViewFragment :
    BaseFragment(),
    FriendsViewContract.View,
    View.OnClickListener {

    private var _binding: FragmentFriendsViewBinding? = null
    private val binding get() = _binding!!

    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var adapter: FriendsListAdapter

    @Inject
    lateinit var prefs: IPrefs

    private val presenter: FriendsViewContract.Presenter by lazy {
        FriendsViewPresenter()
    }

    private val getFriendByPhoneViewModel: GetFriendByPhoneViewModel by viewModels {
        (requireContext().applicationContext as VinciApp).getFriendsComponent()
            .provideGetFriendByPhoneViewModelFactory()
    }

    companion object {
        const val TAG: String = "FriendsViewFragment"
        const val CONTACT_PERMISSION = Manifest.permission.READ_CONTACTS

        fun newInstance(): FriendsViewFragment {
            return FriendsViewFragment()
        }
    }

    private val contactsPermissionActivity =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) {
            if (!it) {
                AlertDialogUtils.showInfoAlertDialog(
                    this.requireContext(),
                    getString(R.string.shared_permission_denied_alert_title),
                    getString(R.string.shared_permission_denied_alert_message)
                )
            } else {
                dispatchPickContactIntent()
            }
        }

    private val pickContactActivityResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            if (it.resultCode == Activity.RESULT_OK) {
                it.data?.data?.let { uri ->
                    persistContactData(uri)
                }
            }
        }

    @SuppressLint("VisibleForTests")
    override fun onAttach(context: Context) {
        super.onAttach(context)
        (context.applicationContext as VinciApp).getAppComponent()
            .inject(this@FriendsViewFragment)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentFriendsViewBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListeners()
        observeFriendsByPhone()
        setAdapter()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onDestroy() {
        super.onDestroy()
        contactsPermissionActivity.apply {
            unregister()
        }

        pickContactActivityResult.apply {
            unregister()
        }
    }

    private fun setListeners() {
        binding.friendsViewFragmentAddButton.setOnClickListener(this)
    }

    private fun setAdapter() {
        val friendsList = prefs.userFriends
        linearLayoutManager = LinearLayoutManager(requireContext())
        binding.friendsViewList.layoutManager = linearLayoutManager
        adapter = FriendsListAdapter(friendsList as MutableList<Contact>)
        binding.friendsViewList.adapter = adapter
    }

    override fun onClick(view: View?) {
        when (view) {
            binding.friendsViewFragmentAddButton -> {
                chooseContact()
            }
        }
    }

    private fun isContactPermissionGranted(): Boolean {
        val permission = ContextCompat.checkSelfPermission(
            this.requireContext(),
            CONTACT_PERMISSION
        )
        return permission == PackageManager.PERMISSION_GRANTED
    }

    private fun dispatchPickContactIntent() {
        val contactsIntent =
            Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI)
        if (contactsIntent.resolveActivity(requireActivity().packageManager) != null) {
            pickContactActivityResult.launch(contactsIntent)
        } else {
            AlertDialogUtils.showInfoAlertDialog(
                requireContext(), getString(R.string.shared_unexpected_error_title),
                getString(
                    R.string.setting_emergency_number_phone_number_error_message
                )
            )
        }
    }

    /**
     * Update the UI state based on the obtaining of the friend data status.
     */
    private fun observeFriendsByPhone() {
        getFriendByPhoneViewModel.getContactByPhoneObserve.observe(viewLifecycleOwner) {
            when (it) {
                RequestUIState.DefaultState -> {}
                is RequestUIState.OnError -> {
                    it.message?.let { message ->
                        showShortToast(message)
                    }
                    getFriendByPhoneViewModel.resetState()
                }
                RequestUIState.OnLoading -> {}
                is RequestUIState.OnSuccess -> {

                    if (it.data) {
                        setAdapter()
                    } else {
                        showShortToast(getString(R.string.choose_friends_screen_create_contact_error))
                    }
                    getFriendByPhoneViewModel.resetState()
                }
            }
        }
    }

    private fun chooseContact() {
        if (isContactPermissionGranted()) {
            presenter.openChooseFriendsScreen(requireActivity().supportFragmentManager)
        } else {
            contactsPermissionActivity.launch(CONTACT_PERMISSION)
        }
    }

    private fun persistContactData(contactData: Uri?) {
        val resolver: ContentResolver = requireActivity().contentResolver
        val cursor: Cursor? =
            contactData?.let { resolver.query(it, null, null, null, null, null) }
        if (cursor != null) {
            cursor.moveToFirst()
            val phoneNumber =
                cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.NUMBER))
            val contactName =
                cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME))

            getFriendByPhoneViewModel.getContactByNumber(phoneNumber, contactName)

            cursor.close()
        }
    }
}
