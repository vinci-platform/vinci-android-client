/*
 *  Copyright (c) 2020, Comtrade
 *  All rights reserved.
 *
 *  Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of the <organization> nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *   DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  Package: com.comtrade.vinciapp.view.widget.createNewPassword.view
 *  File: CreateNewPasswordFragment.kt
 *  Author: Milenko Bojanic <milenko.bojanic@comtrade.com>
 *
 *  Description: Fragment screen for creating new password
 *
 *  History: 8/11/20 Milenko  Initial code
 *
 *
 */

package com.comtrade.vinciapp.view.widget.createNewPassword.view

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import com.comtrade.feature_account.di.AccountComponent
import com.comtrade.feature_account.ui.password.ChangePasswordViewModel
import com.comtrade.feature_account.ui.state.APICallSuccessUIState
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.VinciApp
import com.comtrade.vinciapp.base.BaseFragment
import com.comtrade.vinciapp.databinding.ElevatedProgressViewBinding
import com.comtrade.vinciapp.databinding.FragmentCreateNewPasswordBinding
import com.comtrade.vinciapp.util.BiggerDotPasswordTransformationMethod
import com.comtrade.vinciapp.util.ViewUtils

class CreateNewPasswordFragment :
    BaseFragment(),
    View.OnClickListener {

    private var _binding: FragmentCreateNewPasswordBinding? = null
    private val binding get() = _binding!!

    private var _bindingElevatedView: ElevatedProgressViewBinding? = null
    private val bindingElevatedView get() = _bindingElevatedView!!

    private val changePasswordViewModel: ChangePasswordViewModel by viewModels {
        getAccountComponent().provideChangePasswordViewModelFactory()
    }

    companion object {
        const val TAG: String = "CreateNewPasswordFragment"

        fun newInstance(): CreateNewPasswordFragment {
            return CreateNewPasswordFragment()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentCreateNewPasswordBinding.inflate(inflater, container, false)
        _bindingElevatedView = ElevatedProgressViewBinding.bind(binding.root)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        changePasswordViewModel.changePassword.observe(viewLifecycleOwner) {
            when (it) {
                is APICallSuccessUIState.OnError -> {
                    hideProgressBar()
                    it.message?.let { _ ->
                        showShortToast(getString(R.string.create_new_password_screen_password_change_fail_message))
                    }
                }
                APICallSuccessUIState.OnLoading -> showProgressBar()
                is APICallSuccessUIState.OnSuccess -> {
                    hideProgressBar()
                    if (it.data) {
                        onPasswordChanged()
                    } else {
                        showShortToast(getString(R.string.shared_unexpected_error_title))
                    }
                }
                APICallSuccessUIState.DefaultState -> {}
            }
        }

        setListeners()
        setTransformationMethod()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun setListeners() {
        binding.createNewPasswordFragmentLoginButton.setOnClickListener(this)
    }

    private fun setTransformationMethod() {
        binding.createNewPasswordFragmentTemporaryPasswordEditText.transformationMethod =
            BiggerDotPasswordTransformationMethod()
        binding.createNewPasswordFragmentNewPasswordEditText.transformationMethod =
            BiggerDotPasswordTransformationMethod()
        binding.createNewPasswordFragmentConfirmPasswordEditText.transformationMethod =
            BiggerDotPasswordTransformationMethod()
    }

    private fun showProgressBar() {
        ViewUtils.showView(bindingElevatedView.containerProgress)
    }

    private fun hideProgressBar() {
        ViewUtils.hideView(bindingElevatedView.containerProgress)
    }

    override fun onClick(view: View?) {
        when (view) {
            binding.createNewPasswordFragmentLoginButton -> {
                validatePasswords()
            }
        }
    }

    private fun onPasswordChanged() {
        showShortToast(getString(R.string.create_new_password_screen_success_message))
        // Clear fields
        val fields = arrayOf(
            binding.createNewPasswordFragmentTemporaryPasswordEditText,
            binding.createNewPasswordFragmentNewPasswordEditText,
            binding.createNewPasswordFragmentConfirmPasswordEditText
        )

        for (field in fields) {
            field.text = null
        }
        // Pop fragment stack
        requireActivity().supportFragmentManager.popBackStack()
    }

    private fun validatePasswords() {

        val oldPassword = binding.createNewPasswordFragmentTemporaryPasswordEditText.text
        val newPassword = binding.createNewPasswordFragmentNewPasswordEditText.text
        val newPasswordConfirm = binding.createNewPasswordFragmentConfirmPasswordEditText.text
        if (oldPassword != null && oldPassword.toString().isBlank()) {
            binding.createNewPasswordFragmentTemporaryPasswordEditText.error =
                getString(R.string.create_new_password_screen_old_password_error)
            return
        }

        if (newPassword != null && newPassword.toString().isBlank()) {
            binding.createNewPasswordFragmentNewPasswordEditText.error =
                getString(R.string.create_new_password_screen_new_password_error)
            return
        }

        if (newPasswordConfirm != null && newPasswordConfirm.toString().isBlank()) {
            binding.createNewPasswordFragmentConfirmPasswordEditText.error =
                getString(R.string.create_new_password_screen_confirm_password_error)
            return
        }

        if (newPassword.toString() != newPasswordConfirm.toString()) {
            val passwordsDoNotMatch = getString(R.string.create_user_screen_repeat_password_warning)
            binding.createNewPasswordFragmentNewPasswordEditText.error = passwordsDoNotMatch
            binding.createNewPasswordFragmentConfirmPasswordEditText.error = passwordsDoNotMatch
            showShortToast(passwordsDoNotMatch)
            return
        }

        changePasswordRequest(oldPassword.toString(), newPassword.toString())
    }

    /**
     * Execute API call request.
     */
    private fun changePasswordRequest(oldPassword: String, newPassword: String) {
        changePasswordViewModel.changePassword(oldPassword, newPassword)
    }
}

/**
 * Obtain the Dagger dependency account component extension.
 */
@SuppressLint("VisibleForTests")
internal fun CreateNewPasswordFragment.getAccountComponent(): AccountComponent =
    (requireContext().applicationContext as VinciApp).getAccountComponent()
