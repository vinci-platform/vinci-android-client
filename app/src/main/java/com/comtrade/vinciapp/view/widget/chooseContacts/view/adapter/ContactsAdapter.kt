/*
 *  Copyright (c) 2020, Comtrade
 *  All rights reserved.
 *
 *  Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of the <organization> nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *   DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  Package: com.comtrade.vinciapp.view.widget.chooseContacts.view.adapter
 *  File: ContactsAdapter.kt
 *  Author: Milenko Bojanic <milenko.bojanic@comtrade.com>
 *
 *  Description: choose contacts adapter
 *
 *  History: 8/11/20 Milenko  Initial code
 *
 *
 */

package com.comtrade.vinciapp.view.widget.chooseContacts.view.adapter

import android.util.SparseBooleanArray
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import androidx.recyclerview.widget.RecyclerView
import com.comtrade.domain.entities.user.Contact
import com.comtrade.vinciapp.R

class ContactsAdapter(private val list: MutableList<Contact>) :
    RecyclerView.Adapter<ContactsAdapter.ViewHolder>() {

    var checkedContactsList: MutableList<Contact> = mutableListOf()
    val itemStateArray: SparseBooleanArray = SparseBooleanArray()

    inner class ViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
        RecyclerView.ViewHolder(inflater.inflate(R.layout.contacts_list_item, parent, false)),
        View.OnClickListener {
        private val mCheckBox: CheckBox = itemView.findViewById(R.id.contact_item)

        init {
            mCheckBox.setOnClickListener(this)
        }

        fun bind(contact: Contact, position: Int) {
            mCheckBox.text = contact.name
            mCheckBox.isChecked = itemStateArray.get(position, false)
        }

        override fun onClick(v: View?) {
            if (!itemStateArray.get(bindingAdapterPosition, false)) {
                checkedContactsList.add(list[bindingAdapterPosition])
                mCheckBox.isChecked = true
                itemStateArray.put(bindingAdapterPosition, true)
            } else {
                checkedContactsList.remove(list[bindingAdapterPosition])
                mCheckBox.isChecked = false
                itemStateArray.put(bindingAdapterPosition, false)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(inflater, parent)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val contact: Contact = list[position]
        holder.bind(contact, position)
    }

    fun getSelectedContacts(): MutableList<Contact> {
        return checkedContactsList
    }
}
