/*
 *  Copyright (c) 2020, Comtrade
 *  All rights reserved.
 *
 *  Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of the <organization> nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *   DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  Package: com.comtrade.vinciapp.view.widget.questionnaireSurvey.view
 *  File: SurveyQuestionnaireFragment.kt
 *  Author: Kosta Eric <kosta.eric@comtrade.com>
 *
 *  Description: Fragment for Survey questionnaire screens
 *
 *  History: 8/11/20 Kosta  Initial code
 *
 *
 */

package com.comtrade.vinciapp.view.widget.questionnaireSurvey.view

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.activityViewModels
import androidx.viewpager2.widget.ViewPager2
import com.comtrade.domain.survey.SurveyTypes
import com.comtrade.feature_questionnaire.survey.ui.SurveyViewModel
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.VinciApp
import com.comtrade.vinciapp.base.BaseFragment
import com.comtrade.vinciapp.databinding.FragmentSurveyQuestionnaireBinding
import com.comtrade.vinciapp.util.ViewUtils
import com.comtrade.vinciapp.view.adapter.QuestionnaireViewPagerAdapter
import com.comtrade.vinciapp.view.widget.questionnaireSurvey.core.contract.IQuestionnaireSurveyEndScreen
import com.comtrade.vinciapp.view.widget.questionnaireSurvey.model.SurveyQuestionnaireItem
import com.comtrade.vinciapp.view.widget.questionnaireSurvey.view.questionViews.QuestionnaireSurveyEditableTextQuestions
import com.comtrade.vinciapp.view.widget.questionnaireSurvey.view.questionViews.QuestionnaireSurveyEndScreen
import com.comtrade.vinciapp.view.widget.questionnaireSurvey.view.questionViews.QuestionnaireSurveyGradationQuestion

class SurveyQuestionnaireFragment :
    BaseFragment(),
    View.OnClickListener {

    private var _binding: FragmentSurveyQuestionnaireBinding? = null
    private val binding get() = _binding!!

    private lateinit var fragmentsPagerAdapter: QuestionnaireViewPagerAdapter

    private lateinit var questionIds: Array<out String>

    private val surveyViewModel: SurveyViewModel by activityViewModels {
        (requireContext().applicationContext as VinciApp).getAppComponent()
            .provideSurveyViewModelFactory()
    }

    companion object {
        const val TAG: String = "SurveyQuestionnaireFragment"

        const val ZERO = 0

        const val SURVEY_TYPE = SurveyTypes.WHOQOL_BREF

        fun newInstance(): SurveyQuestionnaireFragment {
            return SurveyQuestionnaireFragment()
        }
    }

    @SuppressLint("VisibleForTests")
    override fun onAttach(context: Context) {
        super.onAttach(context)
        (context.applicationContext as VinciApp).getAppComponent()
            .inject(this@SurveyQuestionnaireFragment)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSurveyQuestionnaireBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        surveyViewModel.startSurvey(SURVEY_TYPE)

        questionIds = resources.getStringArray(R.array.question_ids)
        setAdapter(questionIds)
        setListeners()
        setQuestionnaireAnswersMap(questionIds)
    }

    private fun setAdapter(questionIds: Array<out String>) {
        fragmentsPagerAdapter =
            QuestionnaireViewPagerAdapter(childFragmentManager, viewLifecycleOwner.lifecycle)
        addGradationQuestionsFragments(questionIds)
        fragmentsPagerAdapter.addFragment(QuestionnaireSurveyEditableTextQuestions.newInstance())
        fragmentsPagerAdapter.addFragment(QuestionnaireSurveyEndScreen.newInstance())
        binding.surveyQuestionnaireFragmentViewPager.adapter = fragmentsPagerAdapter
        binding.surveyQuestionnaireFragmentViewPager.offscreenPageLimit = 50 // code smell
    }

    private fun setListeners() {
        binding.surveyQuestionnaireFragmentNextButton.setOnClickListener(this)
        binding.surveyQuestionnaireFragmentBackButton.setOnClickListener(this)
        binding.surveyQuestionnaireFragmentSaveButton.setOnClickListener(this)
        binding.surveyQuestionnaireFragmentViewPager.registerOnPageChangeCallback(object :
                ViewPager2.OnPageChangeCallback() {
                override fun onPageSelected(position: Int) {
                    super.onPageSelected(position)
                    if (position == fragmentsPagerAdapter.itemCount - 1) {
                        ViewUtils.hideView(binding.surveyQuestionnaireFragmentNextButton)
                        ViewUtils.showView(binding.surveyQuestionnaireFragmentSaveButton)
                    } else {
                        ViewUtils.hideView(binding.surveyQuestionnaireFragmentSaveButton)
                        ViewUtils.showView(binding.surveyQuestionnaireFragmentNextButton)
                    }
                }
            })
    }

    private fun setQuestionnaireAnswersMap(questionIds: Array<out String>) {

        val questionsMap = mutableMapOf<String, String>()
        for (questionId in questionIds) {
            questionsMap[questionId] = ZERO.toString()
        }

        surveyViewModel.questionnaireQolSurveyAnswers = questionsMap
    }

    override fun onClick(view: View?) {
        when (view) {
            binding.surveyQuestionnaireFragmentNextButton -> {
                increaseViewPagerCurrentItem()
            }
            binding.surveyQuestionnaireFragmentBackButton -> {
                decreaseViewPagerCurrentItem()
            }
            binding.surveyQuestionnaireFragmentSaveButton -> {
                if (countZeros() >= 6) {
                    showShortToast(getString(R.string.survey_questionnaire_screen_not_enough_answers_toast_message))
                } else {

                    surveyViewModel.stopAndSaveActiveSurvey()

                    binding.surveyQuestionnaireFragmentViewPager.currentItem = 0

                    // Go to root fragment
                    requireActivity().supportFragmentManager.popBackStack(
                        null,
                        FragmentManager.POP_BACK_STACK_INCLUSIVE
                    )
                }
            }
        }
    }

    private fun increaseViewPagerCurrentItem() {
        if (binding.surveyQuestionnaireFragmentViewPager.currentItem == fragmentsPagerAdapter.itemCount - 1) {
            return
        } else {

            binding.surveyQuestionnaireFragmentViewPager.setCurrentItem(
                binding.surveyQuestionnaireFragmentViewPager.currentItem + 1,
                true
            )
            val currentItem = binding.surveyQuestionnaireFragmentViewPager.currentItem

            // Update the final score.
            if (currentItem == fragmentsPagerAdapter.itemCount - 1) {
                try {

                    val editableTextFragment = fragmentsPagerAdapter.createFragment(currentItem - 1)

                    // Collected data from the text fields
                    if (editableTextFragment is QuestionnaireSurveyEditableTextQuestions) {
                        editableTextFragment.collectAnswers()
                    }

                    val fragment = fragmentsPagerAdapter.createFragment(currentItem)
                    if (fragment is IQuestionnaireSurveyEndScreen) {
                        fragment.updateScoreValue(calculateScore())
                    }
                } catch (exception: Exception) {
                    exception.printStackTrace()
                }
            }
        }
    }

    private fun decreaseViewPagerCurrentItem() {
        if (binding.surveyQuestionnaireFragmentViewPager.currentItem == 0) {
            requireActivity().onBackPressed()
        } else {
            binding.surveyQuestionnaireFragmentViewPager.setCurrentItem(
                binding.surveyQuestionnaireFragmentViewPager.currentItem - 1,
                true
            )
        }
    }

    private fun addGradationQuestionsFragments(questionIds: Array<out String>) {
        fragmentsPagerAdapter.addFragment(
            QuestionnaireSurveyGradationQuestion.newInstance(
                SurveyQuestionnaireItem(
                    questionIds[0],
                    getString(R.string.survey_questionnaire_screen_question_1_number),
                    getString(R.string.survey_questionnaire_screen_question_1_text),
                    true,
                    resources.getStringArray(R.array.whoqol_variants_good)
                )
            )
        )
        fragmentsPagerAdapter.addFragment(
            QuestionnaireSurveyGradationQuestion.newInstance(
                SurveyQuestionnaireItem(
                    questionIds[1],
                    getString(R.string.survey_questionnaire_screen_question_2_number),
                    getString(R.string.survey_questionnaire_screen_question_2_text),
                    true,
                    resources.getStringArray(R.array.whoqol_variants_satisfied)
                )
            )
        )
        fragmentsPagerAdapter.addFragment(
            QuestionnaireSurveyGradationQuestion.newInstance(
                SurveyQuestionnaireItem(
                    questionIds[2],
                    getString(R.string.survey_questionnaire_screen_question_3_number),
                    getString(R.string.survey_questionnaire_screen_question_3_text),
                    false,
                    resources.getStringArray(R.array.whoqol_variants_degree)
                )
            )
        )
        fragmentsPagerAdapter.addFragment(
            QuestionnaireSurveyGradationQuestion.newInstance(
                SurveyQuestionnaireItem(
                    questionIds[3],
                    getString(R.string.survey_questionnaire_screen_question_4_number),
                    getString(R.string.survey_questionnaire_screen_question_4_text),
                    false,
                    resources.getStringArray(R.array.whoqol_variants_degree)
                )
            )
        )
        fragmentsPagerAdapter.addFragment(
            QuestionnaireSurveyGradationQuestion.newInstance(
                SurveyQuestionnaireItem(
                    questionIds[4],
                    getString(R.string.survey_questionnaire_screen_question_5_number),
                    getString(R.string.survey_questionnaire_screen_question_5_text),
                    true,
                    resources.getStringArray(R.array.whoqol_variants_degree)
                )
            )
        )
        fragmentsPagerAdapter.addFragment(
            QuestionnaireSurveyGradationQuestion.newInstance(
                SurveyQuestionnaireItem(
                    questionIds[5],
                    getString(R.string.survey_questionnaire_screen_question_6_number),
                    getString(R.string.survey_questionnaire_screen_question_6_text),
                    true,
                    resources.getStringArray(R.array.whoqol_variants_degree)
                )
            )
        )
        fragmentsPagerAdapter.addFragment(
            QuestionnaireSurveyGradationQuestion.newInstance(
                SurveyQuestionnaireItem(
                    questionIds[6],
                    getString(R.string.survey_questionnaire_screen_question_7_number),
                    getString(R.string.survey_questionnaire_screen_question_7_text),
                    true,
                    resources.getStringArray(R.array.whoqol_variants_degree_secondary)
                )
            )
        )
        fragmentsPagerAdapter.addFragment(
            QuestionnaireSurveyGradationQuestion.newInstance(
                SurveyQuestionnaireItem(
                    questionIds[7],
                    getString(R.string.survey_questionnaire_screen_question_8_number),
                    getString(R.string.survey_questionnaire_screen_question_8_text),
                    true,
                    resources.getStringArray(R.array.whoqol_variants_degree_secondary)
                )
            )
        )
        fragmentsPagerAdapter.addFragment(
            QuestionnaireSurveyGradationQuestion.newInstance(
                SurveyQuestionnaireItem(
                    questionIds[8],
                    getString(R.string.survey_questionnaire_screen_question_9_number),
                    getString(R.string.survey_questionnaire_screen_question_9_text),
                    true,
                    resources.getStringArray(R.array.whoqol_variants_degree_secondary)
                )
            )
        )
        fragmentsPagerAdapter.addFragment(
            QuestionnaireSurveyGradationQuestion.newInstance(
                SurveyQuestionnaireItem(
                    questionIds[9],
                    getString(R.string.survey_questionnaire_screen_question_10_number),
                    getString(R.string.survey_questionnaire_screen_question_10_text),
                    true,
                    resources.getStringArray(R.array.whoqol_variants_how_completely)
                )
            )
        )
        fragmentsPagerAdapter.addFragment(
            QuestionnaireSurveyGradationQuestion.newInstance(
                SurveyQuestionnaireItem(
                    questionIds[10],
                    getString(R.string.survey_questionnaire_screen_question_11_number),
                    getString(R.string.survey_questionnaire_screen_question_11_text),
                    true,
                    resources.getStringArray(R.array.whoqol_variants_how_completely)
                )
            )
        )
        fragmentsPagerAdapter.addFragment(
            QuestionnaireSurveyGradationQuestion.newInstance(
                SurveyQuestionnaireItem(
                    questionIds[11],
                    getString(R.string.survey_questionnaire_screen_question_12_number),
                    getString(R.string.survey_questionnaire_screen_question_12_text),
                    true,
                    resources.getStringArray(R.array.whoqol_variants_how_completely)
                )
            )
        )
        fragmentsPagerAdapter.addFragment(
            QuestionnaireSurveyGradationQuestion.newInstance(
                SurveyQuestionnaireItem(
                    questionIds[12],
                    getString(R.string.survey_questionnaire_screen_question_13_number),
                    getString(R.string.survey_questionnaire_screen_question_13_text),
                    true,
                    resources.getStringArray(R.array.whoqol_variants_how_completely)
                )
            )
        )
        fragmentsPagerAdapter.addFragment(
            QuestionnaireSurveyGradationQuestion.newInstance(
                SurveyQuestionnaireItem(
                    questionIds[13],
                    getString(R.string.survey_questionnaire_screen_question_14_number),
                    getString(R.string.survey_questionnaire_screen_question_14_text),
                    true,
                    resources.getStringArray(R.array.whoqol_variants_how_completely)
                )
            )
        )
        fragmentsPagerAdapter.addFragment(
            QuestionnaireSurveyGradationQuestion.newInstance(
                SurveyQuestionnaireItem(
                    questionIds[14],
                    getString(R.string.survey_questionnaire_screen_question_15_number),
                    getString(R.string.survey_questionnaire_screen_question_15_text),
                    true,
                    resources.getStringArray(R.array.whoqol_variants_good)
                )
            )
        )
        fragmentsPagerAdapter.addFragment(
            QuestionnaireSurveyGradationQuestion.newInstance(
                SurveyQuestionnaireItem(
                    questionIds[15],
                    getString(R.string.survey_questionnaire_screen_question_16_number),
                    getString(R.string.survey_questionnaire_screen_question_16_text),
                    true,
                    resources.getStringArray(R.array.whoqol_variants_satisfied)
                )
            )
        )
        fragmentsPagerAdapter.addFragment(
            QuestionnaireSurveyGradationQuestion.newInstance(
                SurveyQuestionnaireItem(
                    questionIds[16],
                    getString(R.string.survey_questionnaire_screen_question_17_number),
                    getString(R.string.survey_questionnaire_screen_question_17_text),
                    true,
                    resources.getStringArray(R.array.whoqol_variants_satisfied)
                )
            )
        )
        fragmentsPagerAdapter.addFragment(
            QuestionnaireSurveyGradationQuestion.newInstance(
                SurveyQuestionnaireItem(
                    questionIds[17],
                    getString(R.string.survey_questionnaire_screen_question_18_number),
                    getString(R.string.survey_questionnaire_screen_question_18_text),
                    true,
                    resources.getStringArray(R.array.whoqol_variants_satisfied)
                )
            )
        )
        fragmentsPagerAdapter.addFragment(
            QuestionnaireSurveyGradationQuestion.newInstance(
                SurveyQuestionnaireItem(
                    questionIds[18],
                    getString(R.string.survey_questionnaire_screen_question_19_number),
                    getString(R.string.survey_questionnaire_screen_question_19_text),
                    true,
                    resources.getStringArray(R.array.whoqol_variants_satisfied)
                )
            )
        )
        fragmentsPagerAdapter.addFragment(
            QuestionnaireSurveyGradationQuestion.newInstance(
                SurveyQuestionnaireItem(
                    questionIds[19],
                    getString(R.string.survey_questionnaire_screen_question_20_number),
                    getString(R.string.survey_questionnaire_screen_question_20_text),
                    true,
                    resources.getStringArray(R.array.whoqol_variants_satisfied)
                )
            )
        )
        fragmentsPagerAdapter.addFragment(
            QuestionnaireSurveyGradationQuestion.newInstance(
                SurveyQuestionnaireItem(
                    questionIds[20],
                    getString(R.string.survey_questionnaire_screen_question_21_number),
                    getString(R.string.survey_questionnaire_screen_question_21_text),
                    true,
                    resources.getStringArray(R.array.whoqol_variants_satisfied)
                )
            )
        )
        fragmentsPagerAdapter.addFragment(
            QuestionnaireSurveyGradationQuestion.newInstance(
                SurveyQuestionnaireItem(
                    questionIds[21],
                    getString(R.string.survey_questionnaire_screen_question_22_number),
                    getString(R.string.survey_questionnaire_screen_question_22_text),
                    true,
                    resources.getStringArray(R.array.whoqol_variants_satisfied)
                )
            )
        )
        fragmentsPagerAdapter.addFragment(
            QuestionnaireSurveyGradationQuestion.newInstance(
                SurveyQuestionnaireItem(
                    questionIds[22],
                    getString(R.string.survey_questionnaire_screen_question_23_number),
                    getString(R.string.survey_questionnaire_screen_question_23_text),
                    true,
                    resources.getStringArray(R.array.whoqol_variants_satisfied)
                )
            )
        )
        fragmentsPagerAdapter.addFragment(
            QuestionnaireSurveyGradationQuestion.newInstance(
                SurveyQuestionnaireItem(
                    questionIds[23],
                    getString(R.string.survey_questionnaire_screen_question_24_number),
                    getString(R.string.survey_questionnaire_screen_question_24_text),
                    true,
                    resources.getStringArray(R.array.whoqol_variants_satisfied)
                )
            )
        )
        fragmentsPagerAdapter.addFragment(
            QuestionnaireSurveyGradationQuestion.newInstance(
                SurveyQuestionnaireItem(
                    questionIds[24],
                    getString(R.string.survey_questionnaire_screen_question_25_number),
                    getString(R.string.survey_questionnaire_screen_question_25_text),
                    true,
                    resources.getStringArray(R.array.whoqol_variants_satisfied)
                )
            )
        )
        fragmentsPagerAdapter.addFragment(
            QuestionnaireSurveyGradationQuestion.newInstance(
                SurveyQuestionnaireItem(
                    questionIds[25],
                    getString(R.string.survey_questionnaire_screen_question_26_number),
                    getString(R.string.survey_questionnaire_screen_question_26_text),
                    false,
                    resources.getStringArray(R.array.whoqol_variants_times)
                )
            )
        )
    }

    private fun countZeros(): Int {
        var count = 0
        surveyViewModel.questionnaireQolSurveyAnswers!!.forEach { (_, value) ->
            run {
                value.toIntOrNull()?.let {
                    if (it == 0) {
                        count++
                    }
                }
            }
        }
        return count
    }

    private fun calculateScore(): String {
        val map = surveyViewModel.questionnaireQolSurveyAnswers!!

        var q3: Int = map[questionIds[2]]?.toInt() ?: 0
        if (q3 == 0) {
            q3 = 6
        }
        var q4: Int = map[questionIds[3]]?.toInt() ?: 0
        if (q4 == 0) {
            q4 = 6
        }

        val q5: Int = map[questionIds[4]]?.toInt() ?: 0

        val q6: Int = map[questionIds[5]]?.toInt() ?: 0

        val q7: Int = map[questionIds[6]]?.toInt() ?: 0

        val q8: Int = map[questionIds[7]]?.toInt() ?: 0

        val q9: Int = map[questionIds[8]]?.toInt() ?: 0

        val q10: Int = map[questionIds[9]]?.toInt() ?: 0

        val q11: Int = map[questionIds[10]]?.toInt() ?: 0

        val q12: Int = map[questionIds[11]]?.toInt() ?: 0

        val q13: Int = map[questionIds[12]]?.toInt() ?: 0

        val q14: Int = map[questionIds[13]]?.toInt() ?: 0

        val q15: Int = map[questionIds[14]]?.toInt() ?: 0

        val q16: Int = map[questionIds[15]]?.toInt() ?: 0

        val q17: Int = map[questionIds[16]]?.toInt() ?: 0

        val q18: Int = map[questionIds[17]]?.toInt() ?: 0

        val q19: Int = map[questionIds[18]]?.toInt() ?: 0

        val q20: Int = map[questionIds[19]]?.toInt() ?: 0

        val q21: Int = map[questionIds[20]]?.toInt() ?: 0

        val q22: Int = map[questionIds[21]]?.toInt() ?: 0

        val q23: Int = map[questionIds[22]]?.toInt() ?: 0

        val q24: Int = map[questionIds[23]]?.toInt() ?: 0

        val q25: Int = map[questionIds[24]]?.toInt() ?: 0

        var q26: Int = map[questionIds[25]]?.toInt() ?: 0
        if (q26 == 0) {
            q26 = 6
        }

        var dom1score = (6 - q3 + 6 - q4 + q10 + q15 + q16 + q17 + q18) / 7.0
        dom1score *= 4

        var dom2score = (q5 + q6 + q7 + q11 + q19 + 6 - q26) / 6.0
        dom2score *= 4

        var dom3score = (q20 + q21 + q22) / 3.0
        dom3score *= 4

        var dom4score = (q8 + q9 + q12 + q13 + q14 + q23 + q24 + q25) / 8.0
        dom4score *= 4

        val score = (dom1score + dom2score + dom3score + dom4score).toLong()
        surveyViewModel.setScore(score)

        return surveyViewModel.getScore().toString()
    }
}
