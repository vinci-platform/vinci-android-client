/*
 *  Copyright (c) 2020, Comtrade
 *  All rights reserved.
 *
 *  Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of the <organization> nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *   DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  Package: com.comtrade.vinciapp.view.widget.questionnaireSurvey.view.questionViews
 *  File: QuestionnaireSurveyGradationQuestion.kt
 *  Author: Milenko Bojanić <milenko.bojanic@comtrade.com>
 *
 *  Description: Fragment screen for QOL questionnaire gradation questions
 *
 *  History: 8/11/20 Kosta  Initial code
 *
 *
 */

package com.comtrade.vinciapp.view.widget.questionnaireUserNeeds.view.questionViews

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import com.comtrade.feature_questionnaire.survey.ui.SurveyViewModel
import com.comtrade.vinciapp.VinciApp
import com.comtrade.vinciapp.base.BaseFragment
import com.comtrade.vinciapp.databinding.ViewQuestionnaireCheckboxQuestionBinding
import com.comtrade.vinciapp.view.widget.questionnaireSurvey.model.SurveyQuestionnaireItem

@SuppressLint("VisibleForTests")
class QuestionnaireCheckboxQuestion : BaseFragment(), View.OnClickListener {

    private var _binding: ViewQuestionnaireCheckboxQuestionBinding? = null
    private val binding get() = _binding!!

    private var questionId: String? = ""
    private var questionNumber: String? = ""
    var question: String? = ""
    var order: Boolean? = true
    var answers: Array<String> = emptyArray()
    var textBoxText: String = ""

    private val surveyViewModel: SurveyViewModel by activityViewModels {
        (requireContext().applicationContext as VinciApp).getAppComponent()
            .provideSurveyViewModelFactory()
    }

    companion object {
        const val TAG: String = "QuestionnaireCheckboxQuestion"

        const val QUESTION_ID_KEY: String = "question_id"
        const val QUESTION_NUMBER_KEY: String = "question_number"
        const val QUESTION_KEY: String = "question"
        const val ORDER_KEY: String = "order"
        const val ANSWERS_KEY: String = "answers"

        const val ZERO = 0
        const val ONE: Int = 1
        const val TWO: Int = 2
        const val THREE: Int = 3
        const val FOUR: Int = 4
        const val FIVE: Int = 5

        fun newInstance(questionItem: SurveyQuestionnaireItem): QuestionnaireCheckboxQuestion {
            val fragment = QuestionnaireCheckboxQuestion()
            val args = Bundle()
            args.putString(QUESTION_ID_KEY, questionItem.questionId)
            args.putString(QUESTION_NUMBER_KEY, questionItem.questionNumber)
            args.putString(QUESTION_KEY, questionItem.question)
            args.putBoolean(ORDER_KEY, questionItem.order)
            args.putStringArray(ANSWERS_KEY, questionItem.answers)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding =
            ViewQuestionnaireCheckboxQuestionBinding.inflate(
                inflater,
                container,
                false
            )
        questionId = arguments?.getString(QUESTION_ID_KEY)
        questionNumber = arguments?.getString(QUESTION_NUMBER_KEY)
        question = arguments?.getString(QUESTION_KEY)
        order = arguments?.getBoolean(ORDER_KEY)
        answers = arguments?.getStringArray(ANSWERS_KEY) as Array<String>
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.viewQuestionnaireCheckboxQuestionNumber.text = questionNumber
        binding.viewQuestionnaireCheckboxQuestionDescription.text = question
        setAnswerTexts()
        setListeners()
    }

    // TODO: Simplify into a loop
    private fun setAnswerTexts() {
        if (answers.isNotEmpty()) {
            binding.viewQuestionnaireCheckboxOne.text = answers[0]
            binding.viewQuestionnaireCheckboxOne.visibility = VISIBLE
        } else {
            binding.viewQuestionnaireCheckboxOne.visibility = GONE
        }
        if (answers.size > 1) {
            binding.viewQuestionnaireCheckboxTwo.text = answers[1]
            binding.viewQuestionnaireCheckboxTwo.visibility = VISIBLE
        } else {
            binding.viewQuestionnaireCheckboxTwo.visibility = GONE
        }
        if (answers.size > 2) {
            binding.viewQuestionnaireCheckboxThree.text = answers[2]
            binding.viewQuestionnaireCheckboxThree.visibility = VISIBLE
        } else {
            binding.viewQuestionnaireCheckboxThree.visibility = GONE
        }
        if (answers.size > 3) {
            binding.viewQuestionnaireCheckboxFour.text = answers[3]
            binding.viewQuestionnaireCheckboxFour.visibility = VISIBLE
        } else {
            binding.viewQuestionnaireCheckboxFour.visibility = GONE
        }
        if (answers.size > 4) {
            binding.viewQuestionnaireCheckboxFive.text = answers[4]
            binding.viewQuestionnaireCheckboxFive.visibility = VISIBLE
        } else {
            binding.viewQuestionnaireCheckboxFive.visibility = GONE
        }
        if (answers.size > 5) {
            binding.viewQuestionnaireCheckboxSix.text = answers[5]
            binding.viewQuestionnaireCheckboxSix.visibility = VISIBLE
        } else {
            binding.viewQuestionnaireCheckboxSix.visibility = GONE
        }
        if (answers.size > 6) {
            binding.viewQuestionnaireCheckboxSeven.text = answers[6]
            binding.viewQuestionnaireCheckboxSeven.visibility = VISIBLE
        } else {
            binding.viewQuestionnaireCheckboxSeven.visibility = GONE
        }
        if (answers.size > 7) {
            binding.viewQuestionnaireCheckboxEight.text = answers[7]
            binding.viewQuestionnaireCheckboxEight.visibility = VISIBLE
            binding.viewQuestionnaireCheckboxEightEditText.visibility = VISIBLE
        } else {
            binding.viewQuestionnaireCheckboxEight.visibility = GONE
        }
        if (answers.size > 8) {
            binding.viewQuestionnaireCheckboxNine.text = answers[8]
            binding.viewQuestionnaireCheckboxNine.visibility = VISIBLE
        } else {
            binding.viewQuestionnaireCheckboxNine.visibility = GONE
        }
    }

    private fun setListeners() {
        binding.viewQuestionnaireCheckboxOne.setOnClickListener(this)
        binding.viewQuestionnaireCheckboxTwo.setOnClickListener(this)
        binding.viewQuestionnaireCheckboxThree.setOnClickListener(this)
        binding.viewQuestionnaireCheckboxFour.setOnClickListener(this)
        binding.viewQuestionnaireCheckboxFive.setOnClickListener(this)
        binding.viewQuestionnaireCheckboxSix.setOnClickListener(this)
        binding.viewQuestionnaireCheckboxSeven.setOnClickListener(this)
        binding.viewQuestionnaireCheckboxEight.setOnClickListener(this)
        binding.viewQuestionnaireCheckboxNine.setOnClickListener(this)
        binding.viewQuestionnaireCheckboxEightEditText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {}
            override fun beforeTextChanged(
                s: CharSequence,
                start: Int,
                count: Int,
                after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence,
                start: Int,
                before: Int,
                count: Int
            ) {
                textBoxText = s.toString()
                storeResults()
            }
        })
    }

    private fun storeResults() {
        var result = ""
        if (answers.isNotEmpty()) {
            result = if (binding.viewQuestionnaireCheckboxOne.isChecked) "1" else "0"
        }
        if (answers.size > 1) {
            result += "," + if (binding.viewQuestionnaireCheckboxTwo.isChecked) "1" else "0"
        }
        if (answers.size > 2) {
            result += "," + if (binding.viewQuestionnaireCheckboxThree.isChecked) "1" else "0"
        }
        if (answers.size > 3) {
            result += "," + if (binding.viewQuestionnaireCheckboxFour.isChecked) "1" else "0"
        }
        if (answers.size > 4) {
            result += "," + if (binding.viewQuestionnaireCheckboxFive.isChecked) "1" else "0"
        }
        if (answers.size > 5) {
            result += "," + if (binding.viewQuestionnaireCheckboxSix.isChecked) "1" else "0"
        }
        if (answers.size > 6) {
            result += "," + if (binding.viewQuestionnaireCheckboxSeven.isChecked) "1" else "0"
        }
        if (answers.size > 7) {
            result += "," + if (binding.viewQuestionnaireCheckboxEight.isChecked) {
                "1($textBoxText)"
            } else {
                "0"
            }
        }
        if (answers.size > 8) {
            result += "," + if (binding.viewQuestionnaireCheckboxNine.isChecked) "1" else "0"
        }
        surveyViewModel.questionnaireUserNeedsAnswers!![
                questionId
                    ?: ""
        ] = result
    }

    override fun onClick(view: View?) {
        storeResults()
    }
}
