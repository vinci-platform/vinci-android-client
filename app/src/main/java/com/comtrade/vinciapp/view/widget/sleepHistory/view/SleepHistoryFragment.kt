/*
 *  Copyright (c) 2020, Comtrade
 *  All rights reserved.
 *
 *  Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of the <organization> nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *   DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  Package: com.comtrade.vinciapp.view.widget.addNewDevice.view
 *  File: AddNewDeviceFragment.kt
 *  Author: Sofija Andjelkovic <sofija.andjelkovic@comtrade.com>
 *
 *  Description: a two-button fragment for different types of adding a new device
 *
 *  History: 8/11/20 Sofija  Initial code
 *
 *
 */

package com.comtrade.vinciapp.view.widget.sleepHistory.view

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.viewModels
import com.comtrade.domain.entities.graph.GraphData
import com.comtrade.domain.entities.step.FitbitDataType
import com.comtrade.domain.extensions.asRoundInt
import com.comtrade.domain_ui.state.RequestUIState
import com.comtrade.feature_devices.ui.fitbit.FitBitPeriodDataViewModel
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.VinciApp
import com.comtrade.vinciapp.view.adapter.QuestionnaireViewPagerAdapter
import com.comtrade.vinciapp.view.widget.historyGraph.view.HistoryGraphFragment
import com.comtrade.vinciapp.view.widget.historyGraph.view.HistoryGraphScreen
import java.text.DecimalFormat

@SuppressLint("VisibleForTests")
class SleepHistoryFragment : HistoryGraphFragment() {

    private lateinit var minutesAsleepLastWeek: Array<GraphData>
    private lateinit var minutesAsleepLastMonth: Array<GraphData>
    private lateinit var minutesAsleepLastHalfYear: Array<GraphData>
    private lateinit var minutesAsleepLastYear: Array<GraphData>

    private lateinit var minutesInBedLastWeek: Array<GraphData>
    private lateinit var minutesInBedLastMonth: Array<GraphData>
    private lateinit var minutesInBedLastHalfYear: Array<GraphData>
    private lateinit var minutesInBedLastYear: Array<GraphData>

    private val fitBitPeriodDataViewModel: FitBitPeriodDataViewModel by viewModels {
        (requireContext().applicationContext as VinciApp).getDevicesComponent()
            .provideFitBitPeriodDataViewModelFactory()
    }


    companion object {
        const val TAG: String = "SleepHistoryFragment"

        fun newInstance(): SleepHistoryFragment {
            return SleepHistoryFragment()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeSleepData()
    }

    /**
     * Handle the possible UI states regarding the FitBit sleep data.
     */
    private fun observeSleepData() {

        fitBitPeriodDataViewModel.fitBitPeriodDataObserver.observe(viewLifecycleOwner) {
            when (it) {
                RequestUIState.DefaultState -> {
                    hideProgressBar()
                }
                is RequestUIState.OnError -> {
                    hideProgressBar()
                    it.message?.let { _ ->
                        showShortToast(
                            getString(R.string.sleep_history_screen_fitbit_error)
                        )
                    }
                    fitBitPeriodDataViewModel.resetState()
                }
                RequestUIState.OnLoading -> {
                    showProgressBar()
                }
                is RequestUIState.OnSuccess -> {
                    hideProgressBar()
                    handleObtainedFitBitData(it.data)
                    fitBitPeriodDataViewModel.resetState()
                }
            }
        }
    }

    private fun handleObtainedFitBitData(data: Map<FitbitDataType, Map<Int, Array<GraphData>>>) {

        for (activityData in data) {
            when (activityData.key) {
                FitbitDataType.MINUTES_ASLEEP -> populateMinutesAsleep(activityData.value)
                FitbitDataType.MINUTES_IN_BED -> populateMinutesInBed(activityData.value)
                else -> {}
            }
        }

        setAdapter()
    }

    private fun populateMinutesAsleep(value: Map<Int, Array<GraphData>>) {
        minutesAsleepLastWeek =
            value[7]!!
        minutesAsleepLastMonth =
            value[30]!!
        minutesAsleepLastHalfYear =
            value[183]!!
        minutesAsleepLastYear =
            value[365]!!

        Log.w("FITBIT LAST WEEK", minutesAsleepLastWeek.size.toString())
        Log.w("FITBIT LAST MONTH", minutesAsleepLastMonth.size.toString())
        Log.w("FITBIT LAST HALF YEAR", minutesAsleepLastHalfYear.size.toString())
        Log.w("FITBIT LAST YEAR", minutesAsleepLastYear.size.toString())
    }

    private fun populateMinutesInBed(value: Map<Int, Array<GraphData>>) {
        minutesInBedLastWeek =
            value[7]!!
        minutesInBedLastMonth =
            value[30]!!
        minutesInBedLastHalfYear =
            value[183]!!
        minutesInBedLastYear =
            value[365]!!

        Log.w("FITBIT LAST WEEK", minutesInBedLastWeek.size.toString())
        Log.w("FITBIT LAST MONTH", minutesInBedLastMonth.size.toString())
        Log.w("FITBIT LAST HALF YEAR", minutesInBedLastHalfYear.size.toString())
        Log.w("FITBIT LAST YEAR", minutesInBedLastYear.size.toString())
    }

    private fun setAdapter() {
        fragmentsPagerAdapter =
            QuestionnaireViewPagerAdapter(childFragmentManager, viewLifecycleOwner.lifecycle)

        val weekData: HashMap<String, Array<Int>> = hashMapOf()
        val monthData: HashMap<String, Array<Int>> = hashMapOf()
        val halfYearData: HashMap<String, Array<Int>> = hashMapOf()
        val yearData: HashMap<String, Array<Int>> = hashMapOf()

        weekData["Minutes Asleep"] = getDataForLastWeek(minutesAsleepLastWeek)
        monthData["Minutes Asleep"] = getDataForLastMonth(minutesAsleepLastMonth)
        halfYearData["Minutes Asleep"] = getDataForLastMonths(minutesAsleepLastHalfYear)
        yearData["Minutes Asleep"] = getDataForLastMonths(minutesAsleepLastYear)

        weekData["Minutes In Bed"] = getDataForLastWeek(minutesInBedLastWeek)
        monthData["Minutes In Bed"] = getDataForLastMonth(minutesInBedLastMonth)
        halfYearData["Minutes In Bed"] = getDataForLastMonths(minutesInBedLastHalfYear)
        yearData["Minutes In Bed"] = getDataForLastMonths(minutesInBedLastYear)

        val labelsLastWeek = getDataLabelsForLastWeek(minutesAsleepLastWeek)
        val labelsLastMonth = getDataLabelsForLastMonth(minutesAsleepLastMonth)
        val labelsLastHalfYear = getDataLabelsForLastMonths(minutesAsleepLastHalfYear)
        val labelsLastYear = getDataLabelsForLastMonths(minutesAsleepLastYear)

        fragmentsPagerAdapter.addFragment(
            HistoryGraphScreen.newInstance(
                getString(R.string.sleep_history_screen_fitbit_title),
                getString(R.string.history_graph_screen_average),
                DecimalFormat("#,###").format(
                    weekData.asIterable().first().value.asIterable().average().asRoundInt()
                ).toString(),
                getString(R.string.history_graph_screen_total),
                DecimalFormat("#,###").format(
                    weekData.asIterable().first().value.asIterable().sum()
                ).toString(),
                labelsLastWeek,
                weekData
            )
        )
        fragmentsPagerAdapter.addFragment(
            HistoryGraphScreen.newInstance(
                getString(R.string.sleep_history_screen_fitbit_title),
                getString(R.string.history_graph_screen_average),
                DecimalFormat("#,###").format(
                    monthData.asIterable().first().value.asIterable().average().asRoundInt()
                ).toString(),
                getString(R.string.history_graph_screen_total),
                DecimalFormat("#,###").format(
                    monthData.asIterable().first().value.asIterable().sum()
                ).toString(),
                labelsLastMonth,
                monthData
            )
        )
        fragmentsPagerAdapter.addFragment(
            HistoryGraphScreen.newInstance(
                getString(R.string.sleep_history_screen_fitbit_title),
                getString(R.string.history_graph_screen_average),
                DecimalFormat("#,###").format(
                    halfYearData.asIterable().first().value.asIterable().average().asRoundInt()
                ).toString(),
                getString(R.string.history_graph_screen_total),
                DecimalFormat("#,###").format(
                    halfYearData.asIterable().first().value.asIterable().sum()
                ).toString(),
                labelsLastHalfYear,
                halfYearData
            )
        )
        fragmentsPagerAdapter.addFragment(
            HistoryGraphScreen.newInstance(
                getString(R.string.sleep_history_screen_fitbit_title),
                getString(R.string.history_graph_screen_average),
                DecimalFormat("#,###").format(
                    yearData.asIterable().first().value.asIterable().average().asRoundInt()
                ).toString(),
                getString(R.string.history_graph_screen_total),
                DecimalFormat("#,###").format(
                    yearData.asIterable().first().value.asIterable().sum()
                ).toString(),
                labelsLastYear,
                yearData
            )
        )

        viewPager.adapter = fragmentsPagerAdapter

        //Attach Tab to view adapter.
        setTab()
    }

    override fun getData() {
        fitBitPeriodDataViewModel.getFitBitDataForTypes(
            intArrayOf(7, 30, 183, 365),
            arrayOf(FitbitDataType.MINUTES_ASLEEP, FitbitDataType.MINUTES_IN_BED)
        )
    }
}


