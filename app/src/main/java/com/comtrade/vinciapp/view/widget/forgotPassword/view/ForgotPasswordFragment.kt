/*
 *  Copyright (c) 2020, Comtrade
 *  All rights reserved.
 *
 *  Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of the <organization> nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *   DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  Package: com.comtrade.vinciapp.view.widget.forgotPassword.view
 *  File: ForgotPasswordFragment.kt
 *  Author: Milenko Bojanic <milenko.bojanic@comtrade.com>
 *
 *  Description: Fragment screen used in case the user forgot the password
 *
 *  History: 8/11/20 Milenko  Initial code
 *
 *
 */

package com.comtrade.vinciapp.view.widget.forgotPassword.view

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import com.comtrade.feature_account.di.AccountComponent
import com.comtrade.feature_account.ui.password.ResetPasswordViewModel
import com.comtrade.feature_account.ui.state.APICallSuccessUIState
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.VinciApp
import com.comtrade.vinciapp.base.BaseFragment
import com.comtrade.vinciapp.databinding.ElevatedProgressViewBinding
import com.comtrade.vinciapp.databinding.FragmentForgotPasswordBinding
import com.comtrade.vinciapp.util.AlertDialogUtils
import com.comtrade.vinciapp.util.ViewUtils
import com.comtrade.vinciapp.view.widget.forgotPassword.core.contract.ForgotPasswordContract

class ForgotPasswordFragment : BaseFragment(), ForgotPasswordContract.View, View.OnClickListener {

    private var _binding: FragmentForgotPasswordBinding? = null
    private val binding get() = _binding!!

    private var _bindingElevatedView: ElevatedProgressViewBinding? = null
    private val bindingElevatedView get() = _bindingElevatedView!!

    private val resetPasswordViewModel: ResetPasswordViewModel by viewModels {
        getAccountComponent().providePasswordResetViewModelFactory()
    }

    companion object {
        const val TAG: String = "ForgotPasswordFragment"

        fun newInstance(): ForgotPasswordFragment {
            return ForgotPasswordFragment()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentForgotPasswordBinding.inflate(inflater, container, false)
        _bindingElevatedView = ElevatedProgressViewBinding.bind(binding.root)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        resetPasswordViewModel.resetPassword.observe(viewLifecycleOwner) {
            when (it) {
                is APICallSuccessUIState.OnError -> {
                    hideProgressBar()
                    it.message?.let { message ->
                        showShortToast(message)
                    }
                    resetPasswordViewModel.resetState()
                }
                APICallSuccessUIState.OnLoading -> {
                    showProgressBar()
                }
                is APICallSuccessUIState.OnSuccess -> {
                    hideProgressBar()
                    if (it.data) {
                        navigateToFinishStep()
                    } else {
                        showShortToast(getString(R.string.shared_unexpected_error_title))
                    }
                    resetPasswordViewModel.resetState()
                }
                APICallSuccessUIState.DefaultState -> {
                    binding.forgotPasswordFragmentEmailEditText.text = null
                }
            }
        }

        setListeners()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun setListeners() {
        binding.forgotPasswordFragmentEmailEditText.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                binding.forgotPasswordFragmentEmailEditText.error = null
            }

            override fun afterTextChanged(p0: Editable?) {
            }
        })
        binding.forgotPasswordFragmentResetButton.setOnClickListener(this)
    }

    override fun showHttpError(errorCode: String, message: String) {
        val title = getString(R.string.shared_error_code_title) + " $errorCode"
        AlertDialogUtils.showInfoAlertDialog(this.requireContext(), title, message)
    }

    override fun showUnexpectedError(message: String) {
        AlertDialogUtils.showUnexpectedErrorAlertDialog(this.requireContext(), message)
    }

    override fun showProgressBar() {
        ViewUtils.showView(bindingElevatedView.containerProgress)
    }

    override fun hideProgressBar() {
        ViewUtils.hideView(bindingElevatedView.containerProgress)
    }

    override fun onClick(view: View?) {
        when (view) {
            binding.forgotPasswordFragmentResetButton -> {
                val userEmail = binding.forgotPasswordFragmentEmailEditText.text
                if (userEmail != null && userEmail.toString().isBlank()) {
                    binding.forgotPasswordFragmentEmailEditText.error =
                        getString(R.string.create_user_screen_email_empty_warning)
                } else {
                    resetPassword(userEmail.toString())
                }
            }
        }
    }

    /**
     * Request API call.
     */
    private fun resetPassword(userEmail: String) {
        resetPasswordViewModel.resetPassword(userEmail)
    }

    /**
     * Proceed to the next step.
     */
    private fun navigateToFinishStep() {
        requireActivity().supportFragmentManager.beginTransaction()
            .replace(
                R.id.prelogin_activity_content,
                ForgotPasswordFinishFragment.newInstance(),
                ForgotPasswordFinishFragment.TAG
            )
            .addToBackStack(null)
            .commit()
    }
}

/**
 * Obtain the Dagger dependency account component extension.
 */
@SuppressLint("VisibleForTests")
internal fun ForgotPasswordFragment.getAccountComponent(): AccountComponent =
    (requireContext().applicationContext as VinciApp).getAccountComponent()
