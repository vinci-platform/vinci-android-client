/*
 *  Copyright (c) 2020, Comtrade
 *  All rights reserved.
 *
 *  Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of the <organization> nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *   DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  Package: com.comtrade.vinciapp.view.widget.connectedDeviceList.view.adapter
 *  File: ConnectedDeviceListAdapter.kt
 *  Author: Sofija Andjelkovic <sofija.andjelkovic@comtrade.com>
 *
 *  Description: connected device list adapter
 *
 *  History: 9/16/20 Sofija  Initial code
 *
 *
 */

package com.comtrade.vinciapp.view.widget.friendsView.adapter

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.comtrade.domain.entities.user.Contact
import com.comtrade.vinciapp.R

class FriendsListAdapter(private val list: MutableList<Contact>) :
    RecyclerView.Adapter<FriendsListAdapter.ViewHolder>() {

    inner class ViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
        RecyclerView.ViewHolder(inflater.inflate(R.layout.friends_item, parent, false)),
        View.OnClickListener {

        private var nameTextView: TextView =
            itemView.findViewById(R.id.friends_item_name_text)
        private var phoneTextView: TextView =
            itemView.findViewById(R.id.friends_item_phone_text)
        private var callImage: ImageView =
            itemView.findViewById(R.id.friends_item_call_image)
        private var smsImage: ImageView =
            itemView.findViewById(R.id.friends_item_sms_image)

        private var context: Context = parent.context

        init {
            callImage.setOnClickListener(this)
            smsImage.setOnClickListener(this)
        }

        override fun onClick(view: View?) {
            when (view) {
                callImage -> {
                    dispatchPhoneCallIntent(phoneTextView.text.toString())
                }
                smsImage -> {
                    sendSms(phoneTextView.text.toString(), "")
                }
            }
        }

        fun bind(name: String, phone: String) {
            nameTextView.text = name
            phoneTextView.text = phone
        }

        private fun dispatchPhoneCallIntent(phone: String) {
            val dialIntent = Intent(Intent.ACTION_DIAL)
            dialIntent.data = Uri.parse("tel:$phone")
            startActivity(context, dialIntent, Bundle.EMPTY)
        }

        private fun sendSms(phone: String, text: String) {
            val smsIntent = Intent(Intent.ACTION_VIEW)
            smsIntent.data = Uri.parse("sms:$phone")
            smsIntent.putExtra("sms_body", text)
            startActivity(context, smsIntent, Bundle.EMPTY)
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(inflater, parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val name = list[position].name
        val number = list[position].number!!
        holder.bind(name, number)
    }

    override fun getItemCount(): Int {
        return list.size
    }
}
