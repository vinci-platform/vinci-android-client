/*
 *  Copyright (c) 2020, Comtrade
 *  All rights reserved.
 *
 *  Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of the <organization> nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *   DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  Package: com.comtrade.vinciapp.view.widget.chooseContacts.view
 *  File: ChooseContactsFragment.kt
 *  Author: Milenko Bojanic <milenko.bojanic@comtrade.com>
 *
 *  Description: Fragment screen for choosing contacts
 *
 *  History: 8/11/20 Milenko  Initial code
 *
 *
 */

package com.comtrade.vinciapp.view.widget.chooseFriends.view

import android.annotation.SuppressLint
import android.app.Activity
import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.database.Cursor
import android.net.Uri
import android.os.Bundle
import android.provider.ContactsContract
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.entities.user.Contact
import com.comtrade.domain.entities.user.data.UpdateUserRequest
import com.comtrade.domain_ui.state.RequestUIState
import com.comtrade.feature_account.ui.state.APICallSuccessUIState
import com.comtrade.feature_account.ui.user.UpdateAccountViewModel
import com.comtrade.feature_friends.ui.GetAllUserPhonesViewModel
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.VinciApp
import com.comtrade.vinciapp.base.BaseFragment
import com.comtrade.vinciapp.databinding.FragmentChooseFriendsBinding
import com.comtrade.vinciapp.util.AlertDialogUtils
import com.comtrade.vinciapp.view.widget.chooseFriends.core.ChooseFriendsPresenter
import com.comtrade.vinciapp.view.widget.chooseFriends.core.contract.ChooseFriendsContract
import com.comtrade.vinciapp.view.widget.chooseFriends.view.adapter.ChooseFriendsAdapter
import com.google.gson.Gson
import javax.inject.Inject

@SuppressLint("VisibleForTests")
class ChooseFriendsFragment : BaseFragment(), ChooseFriendsContract.View, View.OnClickListener {

    private var _binding: FragmentChooseFriendsBinding? = null
    private val binding get() = _binding!!

    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var adapter: ChooseFriendsAdapter

    @Inject
    lateinit var prefs: IPrefs

    @Inject
    lateinit var gson: Gson

    private val presenter: ChooseFriendsContract.Presenter by lazy {
        ChooseFriendsPresenter(this@ChooseFriendsFragment)
    }

    private var contactList: MutableSet<Contact> = mutableSetOf()

    private val viewModel: UpdateAccountViewModel by viewModels {
        (requireContext().applicationContext as VinciApp).getAccountComponent()
            .provideUpdateAccountViewModelFactory()
    }

    private val getAllUSerPhonesViewModel: GetAllUserPhonesViewModel by viewModels {
        (requireContext().applicationContext as VinciApp).getFriendsComponent()
            .provideGetAllUserPhonesViewModelFactory()
    }

    companion object {
        const val TAG: String = "ChooseFriendsFragment"

        fun newInstance(): ChooseFriendsFragment {
            return ChooseFriendsFragment()
        }
    }

    private val pickContactActivityResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            if (it.resultCode == Activity.RESULT_OK) {
                it.data?.data?.let { uri ->
                    val contactResult = queryNumberContent(uri)
                    contactResult?.let { contact ->
                        updateContactListData(contact)
                    }
                }
            }
        }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (context.applicationContext as VinciApp).getAppComponent()
            .inject(this@ChooseFriendsFragment)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentChooseFriendsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        contactList = mutableSetOf()
        setViewModelObservers()
        getAllUserPhonesObserve()
        setListeners()
        setAdapter()
        getAllUSerPhonesViewModel.getAllUserPhones()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onDestroy() {
        super.onDestroy()

        pickContactActivityResult.apply {
            unregister()
        }
    }

    private fun setViewModelObservers() {
        viewModel.updateAccount.observe(viewLifecycleOwner) {
            when (it) {
                is APICallSuccessUIState.OnError -> {
                    onUpdateUserFailure(it.message)
                }
                APICallSuccessUIState.OnLoading -> {
                }
                is APICallSuccessUIState.OnSuccess -> {
                    viewModel.resetState()
                    if (it.data) {
                        onUpdateUserSuccess()
                    } else {
                        showShortToast(getString(R.string.shared_unexpected_error_title))
                    }
                }
                APICallSuccessUIState.DefaultState -> {}
            }
        }
    }

    private fun getAllUserPhonesObserve() {
        getAllUSerPhonesViewModel.getAllUserPhonesObserve.observe(viewLifecycleOwner) {
            when (it) {
                RequestUIState.DefaultState -> {}
                is RequestUIState.OnError -> {
                    it.message?.let { message ->
                        showShortToast(message)
                    }
                    getAllUSerPhonesViewModel.resetState()
                }
                RequestUIState.OnLoading -> {}
                is RequestUIState.OnSuccess -> {
                    contactList = getContactsList(requireContext())
                    presenter.filterUsers(prefs, contactList.toList(), it.data)
                    getAllUSerPhonesViewModel.resetState()
                }
            }
        }
    }

    private fun setListeners() {
        binding.chooseFriendsFragmentBackButton.setOnClickListener(this)
        binding.chooseFriendsFragmentSaveButton.setOnClickListener(this)
        binding.chooseFriendsFragmentInviteButton.setOnClickListener(this)
    }

    private fun setAdapter() {
        linearLayoutManager = LinearLayoutManager(requireContext())
        binding.chooseFriendsFragmentListView.layoutManager = linearLayoutManager
        adapter = ChooseFriendsAdapter(contactList.toMutableList())
        binding.chooseFriendsFragmentListView.adapter = adapter
    }

    @Suppress("SENSELESS_COMPARISON")
    private fun updateAdapterContact(contact: Contact) {
        if (adapter != null) {
            adapter.addNewContact(contact)
        }
    }

    override fun onClick(view: View?) {

        when (view) {
            binding.chooseFriendsFragmentBackButton -> {
                requireActivity().supportFragmentManager.popBackStackImmediate()
            }
            binding.chooseFriendsFragmentSaveButton -> {
                saveCheckedContactsAndGoBack()
            }
            binding.chooseFriendsFragmentInviteButton -> {
                // Pick contacts from the device
                dispatchPickContactIntent()
            }
        }
    }

    private fun saveCheckedContactsAndGoBack() {
        prefs.userFriends = adapter.getSelectedContacts()
        contactList = prefs.userFriends.toMutableSet()
        viewModel.updateAccount(
            UpdateUserRequest(
                prefs.userId,
                prefs.userLogin,
                prefs.userFirstName,
                prefs.userLastName,
                prefs.userGender,
                prefs.userAddress,
                prefs.userEmail,
                prefs.userPhone,
                gson.toJson(prefs.userFriends)
            )
        )
    }

    private fun dispatchPickContactIntent() {
        val contactsIntent =
            Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI)
        if (contactsIntent.resolveActivity(requireActivity().packageManager) != null) {
            pickContactActivityResult.launch(contactsIntent)
        } else {
            AlertDialogUtils.showInfoAlertDialog(
                requireContext(), getString(R.string.shared_unexpected_error_title),
                getString(
                    R.string.setting_emergency_number_phone_number_error_message
                )
            )
        }
    }

    private fun queryNumberContent(uri: Uri): Contact? {
        val resolver: ContentResolver = requireActivity().contentResolver
        val cursor: Cursor? =
            uri.let { resolver.query(it, null, null, null, null, null) }
        return if (cursor != null) {
            cursor.moveToFirst()
            val id = cursor.getString(
                cursor.getColumnIndexOrThrow(ContactsContract.Contacts._ID)
            )
            val phoneNumber =
                cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.NUMBER))
            val contactName =
                cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME))
            cursor.close()
            Contact(id, contactName, phoneNumber, isSelected = false)
        } else {
            null
        }
    }

    private fun updateContactListData(contact: Contact) {
        val filteredList = contactList.filter { item -> item.number == contact.number }
        if (filteredList.isNotEmpty()) {
            showShortToast(getString(R.string.choose_friends_screen_contact_already_added))
        } else {
            contactList.add(contact)
            updateAdapterContact(contact)
        }
    }

    private fun getContactsList(context: Context): MutableSet<Contact> {
        val contactList: MutableSet<Contact> = mutableSetOf()

        val cursor = context.contentResolver.query(
            ContactsContract.Contacts.CONTENT_URI,
            null,
            null,
            null,
            null
        )

        var numbers: Cursor? = null
        if ((cursor?.count ?: 0) > 0) {
            while (cursor != null && cursor.moveToNext()) {
                val id = cursor.getString(
                    cursor.getColumnIndexOrThrow(ContactsContract.Contacts._ID)
                )
                val name = cursor.getString(
                    cursor.getColumnIndexOrThrow(
                        ContactsContract.Contacts.DISPLAY_NAME
                    )
                )

                val hasPhone = cursor.getString(
                    cursor.getColumnIndexOrThrow(
                        ContactsContract.Contacts.HAS_PHONE_NUMBER
                    )
                )

                var number: String? = ""

                if (hasPhone.equals("1", ignoreCase = true)) {
                    numbers = context.contentResolver.query(
                        ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                        ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + id,
                        null, null
                    )
                    numbers?.moveToFirst()
                    number = numbers?.getString(numbers.getColumnIndexOrThrow("data1"))
                }

                val contact = Contact(id, name, number, false)
                contactList.add(contact)
            }
        }

        cursor?.close()
        numbers?.close()

        return contactList
    }

    override fun onContactsFilterResult(resultData: List<Contact>) {
        adapter.setContacts(resultData)
        adapter.notifyDataSetChanged()
    }

    override fun onUpdateUserSuccess() {
        Log.d(TAG, "onUpdateUserSuccess: User updated successfully")
        prefs.addLog("User update successful")
        showShortToast(getString(R.string.edit_profile_account_screen_update_success))
        requireActivity().supportFragmentManager.popBackStackImmediate()
    }

    override fun onUpdateUserFailure(message: String?) {
        message?.let {
            Log.d(TAG, it)
            prefs.addLog("User update error: $it")
            showShortToast(message)
        }
    }
}
