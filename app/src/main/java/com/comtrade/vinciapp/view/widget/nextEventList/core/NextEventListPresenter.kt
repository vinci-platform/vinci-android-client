/*
 *  Copyright (c) 2020, Comtrade
 *  All rights reserved.
 *
 *  Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of the <organization> nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *   DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  Package: com.comtrade.vinciapp.view.widget.nextEventList.core
 *  File: NextEventListPresenter.kt
 *  Author: Sofija Andjelkovic <sofija.andjelkovic@comtrade.com>
 *
 *  Description: next event list presenter
 *
 *  History: 8/27/20 Sofija  Initial code
 *
 *
 */

package com.comtrade.vinciapp.view.widget.nextEventList.core

import androidx.fragment.app.FragmentManager
import com.comtrade.vinciapp.base.BasePresenter
import com.comtrade.vinciapp.view.widget.inviteFriends.view.InviteFriendsFragment
import com.comtrade.vinciapp.view.widget.nextEventList.core.contract.NextEventListContract
import com.comtrade.vinciapp.view.widget.outdoorActivities.view.OutdoorActivitiesFragment

class NextEventListPresenter :
    BasePresenter(),
    NextEventListContract.Presenter {

    override fun openOutdoorActivitiesScreen(fragmentManager: FragmentManager) {
        changeMainActivityFragment(
            fragmentManager,
            OutdoorActivitiesFragment.newInstance(),
            OutdoorActivitiesFragment.TAG
        )
    }

    override fun openInviteFriendsFragment(fragmentManager: FragmentManager, smsBody: String) {
        changeMainActivityFragment(
            fragmentManager,
            InviteFriendsFragment.newInstance(smsBody),
            InviteFriendsFragment.TAG
        )
    }
}
