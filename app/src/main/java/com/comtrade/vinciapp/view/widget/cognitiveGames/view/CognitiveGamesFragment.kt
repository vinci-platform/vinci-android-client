/*
 *  Copyright (c) 2020, Comtrade
 *  All rights reserved.
 *
 *  Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of the <organization> nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *   DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  Package: com.comtrade.vinciapp.view.widget.cognitiveGames.view
 *  File: CognitiveGamesFragment.kt
 *  Author: Kosta Eric <kosta.eric@comtrade.com>
 *
 *  Description: Fragment for cognitive games screen
 *
 *  History: 8/11/20 Kosta  Initial code
 *
 *
 */

package com.comtrade.vinciapp.view.widget.cognitiveGames.view

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.fragment.app.viewModels
import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.entities.event.EventEntity
import com.comtrade.domain_ui.state.RequestUIState
import com.comtrade.feature_events.ui.events.SaveEventViewModel
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.VinciApp
import com.comtrade.vinciapp.base.BaseFragment
import com.comtrade.vinciapp.databinding.FragmentCognitiveGamesBinding
import com.comtrade.vinciapp.view.widget.cognitiveGames.core.CognitiveGamesPresenter
import com.comtrade.vinciapp.view.widget.cognitiveGames.core.contract.CognitiveGamesContract
import com.kunzisoft.switchdatetime.SwitchDateTimeDialogFragment
import java.text.DateFormat
import java.time.Instant
import java.util.*
import javax.inject.Inject

@SuppressLint("VisibleForTests")
class CognitiveGamesFragment :
    BaseFragment(),
    CognitiveGamesContract.View,
    AdapterView.OnItemSelectedListener,
    View.OnClickListener {

    private var _binding: FragmentCognitiveGamesBinding? = null
    private val binding get() = _binding!!

    private lateinit var adapter: ArrayAdapter<String>
    private lateinit var dateTimeFragment: SwitchDateTimeDialogFragment
    private var cognitiveGame: Int? = null
    private var messageDate: String = ""
    private lateinit var calendarDate: Date

    @Inject
    lateinit var prefs: IPrefs

    private val presenter: CognitiveGamesContract.Presenter by lazy { CognitiveGamesPresenter() }

    private val saveEventsViewModel: SaveEventViewModel by viewModels {
        (requireContext().applicationContext as VinciApp).getEventsComponent()
            .provideSaveEventViewModelFactory()
    }

    companion object {
        const val TAG: String = "CognitiveGamesFragment"
        const val TAG_DATETIME_FRAGMENT: String = "TAG_DATETIME_FRAGMENT"

        fun newInstance(): CognitiveGamesFragment {
            return CognitiveGamesFragment()
        }
    }

    @SuppressLint("VisibleForTests")
    override fun onAttach(context: Context) {
        super.onAttach(context)
        (context.applicationContext as VinciApp).getAppComponent()
            .inject(this@CognitiveGamesFragment)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentCognitiveGamesBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        addGames()
        setEventPersistenceObservers()
        setListeners()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun addGames() {
        adapter = ArrayAdapter(requireContext(), R.layout.simple_spinner_item)
        val supportedCognitiveGames = resources.getStringArray(R.array.supported_cognitive_games)
        for (cognitiveGame in supportedCognitiveGames) {
            adapter.add(cognitiveGame)
        }
        binding.cognitiveGamesFragmentSpinner.adapter = adapter
    }

    private fun setEventPersistenceObservers() {
        saveEventsViewModel.storeEventEntity.observe(viewLifecycleOwner) {
            when (it) {
                RequestUIState.DefaultState -> {}
                is RequestUIState.OnError -> {
                    it.message.let { message ->
                        if (message != null) {
                            showShortToast(message)
                        } else {
                            showShortToast(getString(R.string.error_problem_creating_new_event))
                        }
                    }
                    saveEventsViewModel.resetState()
                }
                RequestUIState.OnLoading -> {}
                is RequestUIState.OnSuccess -> {
                    presenter.startNotificationTimer(requireContext(), it.data)
                    saveEventsViewModel.resetState()
                    showToastForNotification()
                }
            }
        }
    }

    private fun setListeners() {
        binding.cognitiveGamesFragmentSpinner.onItemSelectedListener = this
        binding.cognitiveGamesFragmentDatetimeSpinner.setOnClickListener(this)
        binding.cognitiveGamesSaveButton.setOnClickListener(this)
        binding.cognitiveGamesStartButton.setOnClickListener(this)
        setDateTimeListener()
    }

    private fun setDateTimeListener() {
        dateTimeFragment = SwitchDateTimeDialogFragment.newInstance(
            getString(R.string.cognitive_games_screen_picker_title),
            getString(R.string.common_button_set),
            getString(R.string.common_button_cancel)
        )
        dateTimeFragment.setOnButtonClickListener(object :
                SwitchDateTimeDialogFragment.OnButtonWithNeutralClickListener {
                override fun onPositiveButtonClick(date: Date) {
                    val selectedDate =
                        DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.SHORT).format(date)
                    messageDate = selectedDate
                    binding.cognitiveGamesFragmentDatetimeSpinner.text = selectedDate
                    binding.cognitiveGamesSaveButton.isEnabled = true
                    calendarDate = date
                }

                override fun onNeutralButtonClick(date: Date?) {}
                override fun onNegativeButtonClick(date: Date?) {}
            })
    }

    override fun onClick(view: View?) {
        when (view) {
            binding.cognitiveGamesFragmentDatetimeSpinner -> {
                showCalendar()
            }
            binding.cognitiveGamesStartButton -> {
                cognitiveGame?.let {
                    presenter.showSudokuScreen(requireActivity().supportFragmentManager, it)
                }
            }
            binding.cognitiveGamesSaveButton -> {
                createNotification()
            }
        }
    }

    private fun showCalendar() {
        val calendar = Calendar.getInstance()
        dateTimeFragment.startAtCalendarView()
        dateTimeFragment.setDefaultDateTime(
            GregorianCalendar(
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH),
                calendar.get(Calendar.HOUR_OF_DAY),
                calendar.get(Calendar.MINUTE),
                calendar.get(Calendar.SECOND)
            ).time
        )

        activity?.let {
            if (dateTimeFragment.isAdded) {
                return@let
            }
            dateTimeFragment.show(
                it.supportFragmentManager,
                TAG_DATETIME_FRAGMENT
            )
        }
    }

    private fun createNotification() {
        if (calendarDate.time > Date().time) {
            val supportedCognitiveGames =
                resources.getStringArray(R.array.supported_cognitive_games)
            cognitiveGame?.let {
                val eventEntity =
                    EventEntity(
                        0,
                        prefs.userId,
                        calendarDate.time,
                        Instant.now().toEpochMilli(),
                        calendarDate.time,
                        "",
                        supportedCognitiveGames[it],
                        getString(R.string.cognitive_games_notification_title),
                        ""
                    )
                saveEventsViewModel.storeEventEntity(eventEntity)
            }
        } else {
            showShortToast(getString(R.string.cognitive_games_time_negative_message))
        }
    }

    private fun showToastForNotification() {
        showShortToast(getString(R.string.cognitive_games_activity_saved_toast_message))
        proceedToInviteFriendsIfChecked()
    }

    private fun proceedToInviteFriendsIfChecked() {
        if (binding.cognitiveGamesFragmentInviteFriendsCheckbox.isChecked) {
            val smsBody = getString(
                R.string.cognitive_games_invite_contacts_message,
                cognitiveGame,
                messageDate
            )
            presenter.openInviteFriendsFragment(requireActivity().supportFragmentManager, smsBody)
        } else {
            goBack()
        }
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        cognitiveGame = position

        // Checking only for the currently implemented games: Sudoku easy and hard
        val sudoku = cognitiveGame == 0 || cognitiveGame == 1

        binding.cognitiveGamesFragmentDatetimeSpinner.isEnabled =
            sudoku // Setting events only for Sudoku
        binding.cognitiveGamesFragmentInviteFriendsCheckbox.isEnabled = !sudoku
        binding.cognitiveGamesStartButton.isEnabled = sudoku
        if (sudoku) {
            binding.cognitiveGamesSaveButton.isEnabled = false
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {}

    private fun goBack() {
        requireActivity().supportFragmentManager.popBackStackImmediate()
    }
}
