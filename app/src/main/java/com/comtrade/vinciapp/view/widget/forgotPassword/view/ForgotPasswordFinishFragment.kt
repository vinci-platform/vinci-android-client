/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: ForgotPasswordFinishFragment.kt
 * Author: sdelic
 *
 * History:  11.4.22. 11:16 created
 */

package com.comtrade.vinciapp.view.widget.forgotPassword.view

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import com.comtrade.feature_account.di.AccountComponent
import com.comtrade.feature_account.ui.password.ResetPasswordFinishViewModel
import com.comtrade.feature_account.ui.state.APICallSuccessUIState
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.VinciApp
import com.comtrade.vinciapp.base.BaseFragment
import com.comtrade.vinciapp.databinding.ElevatedProgressViewBinding
import com.comtrade.vinciapp.databinding.FragmentForgotPasswordFinishBinding
import com.comtrade.vinciapp.util.ViewUtils
import com.comtrade.vinciapp.view.widget.forgotPassword.core.contract.IForgotPasswordFinishView
import com.comtrade.vinciapp.view.widget.login.view.LoginFragment

/**
 * Second and final step in the User reset password process view component.
 * Only displayed when a successful activation key has been received by the User via email message.
 */
internal class ForgotPasswordFinishFragment : BaseFragment(), IForgotPasswordFinishView {

    private var _binding: FragmentForgotPasswordFinishBinding? = null
    private val binding get() = _binding!!

    private var _bindingElevatedView: ElevatedProgressViewBinding? = null
    private val bindingElevatedView get() = _bindingElevatedView!!

    private val viewModel: ResetPasswordFinishViewModel by viewModels {
        getAccountComponent().providePasswordResetFinishViewModelFactory()
    }

    companion object {
        const val TAG: String = "ForgotPasswordFinishFragment"

        fun newInstance(): ForgotPasswordFinishFragment {
            return ForgotPasswordFinishFragment()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentForgotPasswordFinishBinding.inflate(inflater, container, false)
        _bindingElevatedView = ElevatedProgressViewBinding.bind(binding.root)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.resetPasswordFinish.observe(viewLifecycleOwner) {
            when (it) {
                is APICallSuccessUIState.OnError -> {
                    hideProgressBar()
                    it.message?.let { message ->
                        showShortToast(message)
                    }
                }
                APICallSuccessUIState.OnLoading -> showProgressBar()
                is APICallSuccessUIState.OnSuccess -> {
                    hideProgressBar()
                    if (it.data) {
                        navigateToLoginScreen()
                    } else {
                        showShortToast(getString(R.string.shared_unexpected_error_title))
                    }
                }
                APICallSuccessUIState.DefaultState -> {}
            }
        }

        setListeners()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun showProgressBar() {
        ViewUtils.showView(bindingElevatedView.containerProgress)
    }

    override fun hideProgressBar() {
        ViewUtils.hideView(bindingElevatedView.containerProgress)
    }

    private fun setListeners() {
        binding.resetPasswordButton.setOnClickListener {
            validateFields()
        }
    }

    /**
     * Validate fields and execute API call.
     */
    private fun validateFields() {
        val activationKey = binding.activationKeyEditText.text
        val newPassword = binding.newPasswordEditText.text
        val newPasswordConfirm = binding.confirmPasswordEditText.text
        if (activationKey != null && activationKey.toString().isBlank()) {
            binding.activationKeyEditText.error =
                getString(R.string.forgot_password_finish_screen_activation_key_error)
            return
        }

        if (newPassword != null && newPassword.toString().isBlank()) {
            binding.newPasswordEditText.error =
                getString(R.string.create_new_password_screen_new_password_error)
            return
        }

        if (newPasswordConfirm != null && newPasswordConfirm.toString().isBlank()) {
            binding.confirmPasswordEditText.error =
                getString(R.string.create_new_password_screen_confirm_password_error)
            return
        }

        if (newPassword.toString() != newPasswordConfirm.toString()) {
            val passwordsDoNotMatch = getString(R.string.create_user_screen_repeat_password_warning)
            binding.newPasswordEditText.error = passwordsDoNotMatch
            binding.confirmPasswordEditText.error = passwordsDoNotMatch
            showShortToast(passwordsDoNotMatch)
            return
        }

        resetPasswordRequest(activationKey.toString(), newPassword.toString())
    }

    /**
     * Execute API call request.
     */
    private fun resetPasswordRequest(activationKey: String, newPassword: String) {
        viewModel.resetPasswordFinish(activationKey, newPassword)
    }

    /**
     * Clear field states and navigate User to the login screen.
     */
    private fun navigateToLoginScreen() {

        showShortToast(getString(R.string.create_new_password_screen_success_message))
        // Clear fields
        val fields = arrayOf(
            binding.activationKeyEditText,
            binding.newPasswordEditText,
            binding.confirmPasswordEditText
        )

        for (field in fields) {
            field.text = null
        }
        // Pop back to the login screen
        requireActivity().supportFragmentManager.popBackStack(LoginFragment.TAG, 0)
    }
}

/**
 * Obtain the Dagger dependency account component extension.
 */
@SuppressLint("VisibleForTests")
internal fun ForgotPasswordFinishFragment.getAccountComponent(): AccountComponent =
    (requireContext().applicationContext as VinciApp).getAccountComponent()
