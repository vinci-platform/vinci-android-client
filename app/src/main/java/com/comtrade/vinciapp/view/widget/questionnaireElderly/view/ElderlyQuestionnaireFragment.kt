/*
 *  Copyright (c) 2020, Comtrade
 *  All rights reserved.
 *
 *  Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of the <organization> nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *   DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  Package: com.comtrade.vinciapp.view.widget.questionnaireElderly.view
 *  File: ElderlyQuestionnaireFragment.kt
 *  Author: Milenko Bojanic <milenko.bojanic@comtrade.com>
 *
 *  Description: Fragment screen for Elderly questionnaire container
 *
 *  History: 8/11/20 Milenko  Initial code
 *
 *
 */

package com.comtrade.vinciapp.view.widget.questionnaireElderly.view

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.activityViewModels
import androidx.viewpager2.widget.ViewPager2
import com.comtrade.domain.survey.SurveyTypes
import com.comtrade.feature_questionnaire.survey.ui.SurveyViewModel
import com.comtrade.vinciapp.VinciApp
import com.comtrade.vinciapp.base.BaseFragment
import com.comtrade.vinciapp.databinding.FragmentElderlyQuestionnaireBinding
import com.comtrade.vinciapp.view.adapter.QuestionnaireViewPagerAdapter
import com.comtrade.vinciapp.view.widget.questionnaireElderly.view.questionViews.*

class ElderlyQuestionnaireFragment :
    BaseFragment(),
    View.OnClickListener {

    private var _binding: FragmentElderlyQuestionnaireBinding? = null
    private val binding get() = _binding!!

    private lateinit var fragmentsPagerAdapter: QuestionnaireViewPagerAdapter

    private val surveyViewModel: SurveyViewModel by activityViewModels {
        (requireContext().applicationContext as VinciApp).getAppComponent()
            .provideSurveyViewModelFactory()
    }

    companion object {
        const val TAG: String = "ElderlyQuestionnaireFragment"
        private const val SURVEY_TYPE = SurveyTypes.ELDERLY

        fun newInstance(): ElderlyQuestionnaireFragment {
            return ElderlyQuestionnaireFragment()
        }
    }

    @SuppressLint("VisibleForTests")
    override fun onAttach(context: Context) {
        super.onAttach(context)
        (context.applicationContext as VinciApp).getAppComponent()
            .inject(this@ElderlyQuestionnaireFragment)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentElderlyQuestionnaireBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        surveyViewModel.startSurvey(SURVEY_TYPE)
        setAdapter()
        setListeners()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun setAdapter() {
        fragmentsPagerAdapter =
            QuestionnaireViewPagerAdapter(childFragmentManager, viewLifecycleOwner.lifecycle)
        fragmentsPagerAdapter.addFragment(QuestionnaireElderlyQuestion1Fragment.newInstance())
        fragmentsPagerAdapter.addFragment(QuestionnaireElderlyQuestion2Fragment.newInstance())
        fragmentsPagerAdapter.addFragment(QuestionnaireElderlyQuestion3Fragment.newInstance())
        fragmentsPagerAdapter.addFragment(QuestionnaireElderlyQuestion4Fragment.newInstance())
        fragmentsPagerAdapter.addFragment(QuestionnaireElderlyQuestion5Fragment.newInstance())
        fragmentsPagerAdapter.addFragment(QuestionnaireElderlyQuestion6Fragment.newInstance())
        fragmentsPagerAdapter.addFragment(QuestionnaireElderlyQuestion7Fragment.newInstance())
        fragmentsPagerAdapter.addFragment(QuestionnaireElderlyEndFragment.newInstance())
        binding.questionnaireFragmentViewPager.adapter = fragmentsPagerAdapter
        binding.questionnaireFragmentViewPager.offscreenPageLimit = 10
    }

    private fun setListeners() {
        binding.questionnaireFragmentBackButton.setOnClickListener(this)
        binding.questionnaireFragmentNextButton.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
        when (view) {
            binding.questionnaireFragmentBackButton ->
                decreaseViewPagerCurrentItem(binding.questionnaireFragmentViewPager)
            binding.questionnaireFragmentNextButton ->
                if (binding.questionnaireFragmentViewPager.currentItem == fragmentsPagerAdapter.itemCount - 1) {

                    surveyViewModel.stopAndSaveActiveSurvey()

                    // Go to root fragment
                    requireActivity().supportFragmentManager.popBackStack(
                        null,
                        FragmentManager.POP_BACK_STACK_INCLUSIVE
                    )
                } else {
                    val item =
                        fragmentsPagerAdapter.createFragment(binding.questionnaireFragmentViewPager.currentItem) as QuestionnaireElderlyQuestionFragment
                    val error = item.storeAnswer()
                    if (error == "") {
                        binding.questionnaireFragmentViewPager.currentItem++
                    } else {
                        showShortToast(error)
                    }
                }
        }
    }

    /**
     * Navigate to the previous ViewPager view or exit screen if it's the first page.
     */
    private fun decreaseViewPagerCurrentItem(viewPager: ViewPager2) {
        if (viewPager.currentItem == 0) {
            requireActivity().onBackPressed()
        } else {
            viewPager.setCurrentItem(
                viewPager.currentItem - 1,
                true
            )
        }
    }
}
