/*
 *  Copyright (c) 2020, Comtrade
 *  All rights reserved.
 *
 *  Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of the <organization> nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *   DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  Package: com.comtrade.vinciapp.view.widget.physicalActivities.view
 *  File: PhysicalActivitiesFragment.kt
 *  Author: Kosta Eric <kosta.eric@comtrade.com>
 *
 *  Description: Fragment for physical activity screen
 *
 *  History: 8/11/20 Kosta  Initial code
 *
 *
 */

package com.comtrade.vinciapp.view.widget.physicalActivities.view

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.VinciApp
import com.comtrade.vinciapp.base.BaseFragment
import com.comtrade.vinciapp.databinding.FragmentPhysicalActivityBinding
import com.comtrade.vinciapp.view.widget.physicalActivities.core.PhysicalActivitiesPresenter
import com.comtrade.vinciapp.view.widget.physicalActivities.core.contract.PhysicalActivitiesContract
import javax.inject.Inject

class PhysicalActivitiesFragment :
    BaseFragment(),
    PhysicalActivitiesContract.View,
    View.OnClickListener {

    private var _binding: FragmentPhysicalActivityBinding? = null
    private val binding get() = _binding!!

    @Inject
    lateinit var prefs: IPrefs

    private val presenter: PhysicalActivitiesContract.Presenter by lazy {
        PhysicalActivitiesPresenter()
    }

    companion object {
        const val TAG: String = "PhysicalActivitiesFragment"

        fun newInstance(): PhysicalActivitiesFragment {
            return PhysicalActivitiesFragment()
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (context.applicationContext as VinciApp).getAppComponent()
            .inject(this@PhysicalActivitiesFragment)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentPhysicalActivityBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListeners()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun setListeners() {
        binding.physicalActivityFragmentStepCountContainer.setOnClickListener(this)
        binding.physicalActivityFragmentOutdoorActivitiesContainer.setOnClickListener(this)
        binding.physicalActivityFragmentIPAQButton.setOnClickListener(this)
        binding.physicalActivityFragmentCaloriesContainer.setOnClickListener(this)
        binding.physicalActivityFragmentSleepContainer.setOnClickListener(this)
        binding.physicalActivityFragmentHeartRateContainer.setOnClickListener(this)
        binding.physicalActivityFragmentHealthPointsContainer.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
        when (view) {
            binding.physicalActivityFragmentStepCountContainer -> {
                if (isStepCountingDeviceAvailable(prefs)) {
                    showShortToast(getString(R.string.choose_activties_screen_no_step_device_message))
                } else {
                    presenter.openStepsHistoryScreen(requireActivity().supportFragmentManager)
                }
            }
            binding.physicalActivityFragmentOutdoorActivitiesContainer -> {
                presenter.openOutdoorActivitiesScreen(requireActivity().supportFragmentManager)
            }
            binding.physicalActivityFragmentCaloriesContainer -> {
                presenter.openCaloriesHistoryScreen(requireActivity().supportFragmentManager)
            }
            binding.physicalActivityFragmentSleepContainer -> {
                presenter.openSleepHistoryScreen(requireActivity().supportFragmentManager)
            }
            binding.physicalActivityFragmentHeartRateContainer -> {
                presenter.openHeartRateHistoryScreen(requireActivity().supportFragmentManager)
            }
            binding.physicalActivityFragmentHealthPointsContainer -> {
                presenter.openHealthPointsHistoryScreen(requireActivity().supportFragmentManager)
            }
            binding.physicalActivityFragmentIPAQButton -> {
                presenter.openIPAQuestionnaireScreen(requireActivity().supportFragmentManager)
            }
        }
    }

    /**
     * Check if User has assigned device for measuring steps in local storage.
     */
    private fun isStepCountingDeviceAvailable(prefs: IPrefs): Boolean =
        (prefs.fitBitDevice == null || !prefs.fitBitDevice!!.active) &&
            (prefs.shoeDevice == null || !prefs.shoeDevice!!.active) &&
            (prefs.cmdDevice == null || !prefs.cmdDevice!!.active)
}
