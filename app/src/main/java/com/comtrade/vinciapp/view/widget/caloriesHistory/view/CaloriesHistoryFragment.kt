/*
 *  Copyright (c) 2020, Comtrade
 *  All rights reserved.
 *
 *  Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of the <organization> nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *   DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  Package: com.comtrade.vinciapp.view.widget.addNewDevice.view
 *  File: AddNewDeviceFragment.kt
 *  Author: Sofija Andjelkovic <sofija.andjelkovic@comtrade.com>
 *
 *  Description: a two-button fragment for different types of adding a new device
 *
 *  History: 8/11/20 Sofija  Initial code
 *
 *
 */

package com.comtrade.vinciapp.view.widget.caloriesHistory.view

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.viewModels
import com.comtrade.domain.entities.graph.GraphData
import com.comtrade.domain.entities.step.FitbitDataType
import com.comtrade.domain.extensions.asRoundInt
import com.comtrade.domain_ui.state.RequestUIState
import com.comtrade.feature_devices.ui.fitbit.FitBitPeriodDataViewModel
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.VinciApp
import com.comtrade.vinciapp.view.adapter.QuestionnaireViewPagerAdapter
import com.comtrade.vinciapp.view.widget.caloriesHistory.core.contract.CaloriesHistoryContract
import com.comtrade.vinciapp.view.widget.historyGraph.view.HistoryGraphFragment
import com.comtrade.vinciapp.view.widget.historyGraph.view.HistoryGraphScreen
import java.text.DecimalFormat

@SuppressLint("VisibleForTests")
class CaloriesHistoryFragment : HistoryGraphFragment(), CaloriesHistoryContract.View {

    private lateinit var caloriesOutLastWeek: Array<GraphData>
    private lateinit var caloriesOutLastMonth: Array<GraphData>
    private lateinit var caloriesOutLastHalfYear: Array<GraphData>
    private lateinit var caloriesOutLastYear: Array<GraphData>

    private lateinit var activityCaloriesLastWeek: Array<GraphData>
    private lateinit var activityCaloriesLastMonth: Array<GraphData>
    private lateinit var activityCaloriesLastHalfYear: Array<GraphData>
    private lateinit var activityCaloriesLastYear: Array<GraphData>

    private val fitBitPeriodDataViewModel: FitBitPeriodDataViewModel by viewModels {
        (requireContext().applicationContext as VinciApp).getDevicesComponent()
            .provideFitBitPeriodDataViewModelFactory()
    }


    companion object {
        const val TAG: String = "CaloriesHistoryFragment"

        fun newInstance(): CaloriesHistoryFragment {
            return CaloriesHistoryFragment()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeCaloriesData()
    }

    /**
     * Handle the possible UI states regarding the FitBit calories data.
     */
    private fun observeCaloriesData() {
        fitBitPeriodDataViewModel.fitBitPeriodDataObserver.observe(viewLifecycleOwner) {
            when (it) {
                RequestUIState.DefaultState -> {
                    hideProgressBar()
                }
                is RequestUIState.OnError -> {
                    hideProgressBar()
                    it.message?.let { _ ->
                        showShortToast(
                            getString(R.string.calories_history_screen_fitbit_calories_error)
                        )
                    }
                    fitBitPeriodDataViewModel.resetState()
                }
                RequestUIState.OnLoading -> {
                    showProgressBar()
                }
                is RequestUIState.OnSuccess -> {
                    hideProgressBar()
                    handleObtainedFitBitData(it.data)
                    fitBitPeriodDataViewModel.resetState()
                }
            }
        }
    }

    private fun handleObtainedFitBitData(data: Map<FitbitDataType, Map<Int, Array<GraphData>>>) {

        for (activityData in data) {
            when (activityData.key) {
                FitbitDataType.ACTIVITY_CALORIES -> populateActivityCalories(activityData.value)
                FitbitDataType.CALORIES_OUT -> populateCaloriesOutData(activityData.value)
                else -> {}
            }
        }

        setAdapter()
    }

    private fun populateActivityCalories(value: Map<Int, Array<GraphData>>) {
        activityCaloriesLastWeek =
            value[7]!!
        activityCaloriesLastMonth =
            value[30]!!
        activityCaloriesLastHalfYear =
            value[183]!!
        activityCaloriesLastYear =
            value[365]!!

        Log.w("FITBIT LAST WEEK", activityCaloriesLastWeek.size.toString())
        Log.w("FITBIT LAST MONTH", activityCaloriesLastMonth.size.toString())
        Log.w("FITBIT LAST HALF YEAR", activityCaloriesLastHalfYear.size.toString())
        Log.w("FITBIT LAST YEAR", activityCaloriesLastYear.size.toString())
    }

    private fun populateCaloriesOutData(value: Map<Int, Array<GraphData>>) {
        caloriesOutLastWeek = value[7]!!
        caloriesOutLastMonth =
            value[30]!!
        caloriesOutLastHalfYear =
            value[183]!!
        caloriesOutLastYear =
            value[365]!!

        Log.w("FITBIT LAST WEEK", caloriesOutLastWeek.size.toString())
        Log.w("FITBIT LAST MONTH", caloriesOutLastMonth.size.toString())
        Log.w("FITBIT LAST HALF YEAR", caloriesOutLastHalfYear.size.toString())
        Log.w("FITBIT LAST YEAR", caloriesOutLastYear.size.toString())
    }


    private fun setAdapter() {
        fragmentsPagerAdapter =
            QuestionnaireViewPagerAdapter(childFragmentManager, viewLifecycleOwner.lifecycle)

        val weekData: HashMap<String, Array<Int>> = hashMapOf()
        val monthData: HashMap<String, Array<Int>> = hashMapOf()
        val halfYearData: HashMap<String, Array<Int>> = hashMapOf()
        val yearData: HashMap<String, Array<Int>> = hashMapOf()

        weekData["Calories Out"] = getDataForLastWeek(caloriesOutLastWeek)
        monthData["Calories Out"] = getDataForLastMonth(caloriesOutLastMonth)
        halfYearData["Calories Out"] = getDataForLastMonths(caloriesOutLastHalfYear)
        yearData["Calories Out"] = getDataForLastMonths(caloriesOutLastYear)

        weekData["Activity Calories"] = getDataForLastWeek(activityCaloriesLastWeek)
        monthData["Activity Calories"] = getDataForLastMonth(activityCaloriesLastMonth)
        halfYearData["Activity Calories"] = getDataForLastMonths(activityCaloriesLastHalfYear)
        yearData["Activity Calories"] = getDataForLastMonths(activityCaloriesLastYear)

        val labelsLastWeek = getDataLabelsForLastWeek(caloriesOutLastWeek)
        val labelsLastMonth = getDataLabelsForLastMonth(caloriesOutLastMonth)
        val labelsLastHalfYear = getDataLabelsForLastMonths(caloriesOutLastHalfYear)
        val labelsLastYear = getDataLabelsForLastMonths(caloriesOutLastYear)

        fragmentsPagerAdapter.addFragment(
            HistoryGraphScreen.newInstance(
                getString(R.string.calories_history_screen_title),
                getString(R.string.calories_history_screen_average_activity_calories),
                DecimalFormat("#,###").format(
                    weekData.values.sumOf { it.average() }.asRoundInt()
                ).toString(),
                getString(R.string.calories_history_screen_total_activity_calories),
                DecimalFormat("#,###").format(
                    weekData.values.sumOf { it.sum() }
                ).toString(),
                labelsLastWeek,
                weekData
            )
        )
        fragmentsPagerAdapter.addFragment(
            HistoryGraphScreen.newInstance(
                getString(R.string.calories_history_screen_title),
                getString(R.string.calories_history_screen_average_activity_calories),
                DecimalFormat("#,###").format(
                    monthData.values.sumOf { it.average() }.asRoundInt()
                ).toString(),
                getString(R.string.calories_history_screen_total_activity_calories),
                DecimalFormat("#,###").format(
                    monthData.values.sumOf { it.sum() }
                ).toString(),
                labelsLastMonth,
                monthData
            )
        )
        fragmentsPagerAdapter.addFragment(
            HistoryGraphScreen.newInstance(
                getString(R.string.calories_history_screen_title),
                getString(R.string.calories_history_screen_average_activity_calories),
                DecimalFormat("#,###").format(
                    halfYearData.values.sumOf { it.average() }.asRoundInt()
                ).toString(),
                getString(R.string.calories_history_screen_total_activity_calories),
                DecimalFormat("#,###").format(
                    halfYearData.values.sumOf { it.sum() }
                ).toString(),
                labelsLastHalfYear,
                halfYearData
            )
        )
        fragmentsPagerAdapter.addFragment(
            HistoryGraphScreen.newInstance(
                getString(R.string.calories_history_screen_title),
                getString(R.string.calories_history_screen_average_activity_calories),
                DecimalFormat("#,###").format(
                    yearData.values.sumOf { it.average() }.asRoundInt()
                ).toString(),
                getString(R.string.calories_history_screen_total_activity_calories),
                DecimalFormat("#,###").format(
                    yearData.values.sumOf { it.sum() }
                ).toString(),
                labelsLastYear,
                yearData
            )
        )

        viewPager.adapter = fragmentsPagerAdapter

        //Attach Tab to view adapter.
        setTab()
    }

    override fun getData() {

        fitBitPeriodDataViewModel.getFitBitDataForTypes(
            intArrayOf(7, 30, 183, 365),
            arrayOf(FitbitDataType.CALORIES_OUT, FitbitDataType.ACTIVITY_CALORIES)
        )
    }
}


