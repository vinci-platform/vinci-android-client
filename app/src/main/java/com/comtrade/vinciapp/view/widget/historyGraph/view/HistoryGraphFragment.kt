/*
 *  Copyright (c) 2020, Comtrade
 *  All rights reserved.
 *
 *  Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of the <organization> nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *   DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  Package: com.comtrade.vinciapp.view.widget.addNewDevice.view
 *  File: AddNewDeviceFragment.kt
 *  Author: Sofija Andjelkovic <sofija.andjelkovic@comtrade.com>
 *
 *  Description: a two-button fragment for different types of adding a new device
 *
 *  History: 8/11/20 Sofija  Initial code
 *
 *
 */

package com.comtrade.vinciapp.view.widget.historyGraph.view

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager2.widget.ViewPager2
import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.entities.graph.GraphData
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.VinciApp
import com.comtrade.vinciapp.base.BaseFragment
import com.comtrade.vinciapp.databinding.ElevatedProgressViewBinding
import com.comtrade.vinciapp.databinding.FragmentHistoryGraphBinding
import com.comtrade.vinciapp.view.adapter.QuestionnaireViewPagerAdapter
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import java.time.DayOfWeek
import java.time.Month
import java.time.YearMonth
import javax.inject.Inject

abstract class HistoryGraphFragment : BaseFragment() {

    private var _binding: FragmentHistoryGraphBinding? = null
    private val binding get() = _binding!!

    private var _bindingElevatedView: ElevatedProgressViewBinding? = null
    private val bindingElevatedView get() = _bindingElevatedView!!

    protected lateinit var tabLayout: TabLayout
    protected lateinit var viewPager: ViewPager2
    protected lateinit var fragmentsPagerAdapter: QuestionnaireViewPagerAdapter

    @Inject
    lateinit var prefs: IPrefs

    @SuppressLint("VisibleForTests")
    override fun onAttach(context: Context) {
        super.onAttach(context)
        (context.applicationContext as VinciApp).getAppComponent()
            .inject(this@HistoryGraphFragment)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentHistoryGraphBinding.inflate(inflater, container, false)
        _bindingElevatedView = ElevatedProgressViewBinding.bind(binding.root)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tabLayout = view.findViewById(R.id.history_graphs_tab_layout)
        viewPager = view.findViewById(R.id.history_graphs_view_pager)

        getData()
        setListeners()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
        _bindingElevatedView = null
    }

    private fun setListeners() {
        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                viewPager.currentItem = tab.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {}
            override fun onTabReselected(tab: TabLayout.Tab) {}
        })
    }

    protected open fun setTab() {
        TabLayoutMediator(tabLayout, viewPager) { tab, position ->
            when (position) {
                0 -> tab.text = getString(R.string.history_graph_screen_7_days)
                1 -> tab.text = getString(R.string.history_graph_screen_30_days)
                2 -> tab.text = getString(R.string.history_graph_screen_6_months)
                3 -> tab.text = getString(R.string.history_graph_screen_1_year)
            }
        }.attach()
        tabLayout.tabGravity = TabLayout.GRAVITY_FILL
    }

    abstract fun getData()

    // Last week
    protected fun getDataForLastWeek(stepData: Array<GraphData>): Array<Int> {
        val dataArray: MutableList<Int> = mutableListOf()

        if (stepData.count() == 7) {
            stepData.forEach {
                dataArray.add(it.value)
            }
        }
        return dataArray.toTypedArray()
    }

    protected fun getDataLabelsForLastWeek(stepsLastWeek: Array<GraphData>): Array<String> {
        val dataArray: MutableList<String> = mutableListOf()

        stepsLastWeek.forEach {
            var dayOfTheWeek = it.dateTime.day
            if (dayOfTheWeek == 0) {
                dayOfTheWeek = 7
            }
            dataArray.add(DayOfWeek.of(dayOfTheWeek).name)
        }

        return dataArray.toTypedArray()
    }

    // Last month
    protected fun getDataForLastMonth(stepData: Array<GraphData>): Array<Int> {
        val dataArray: MutableList<Int> = mutableListOf()
        var partialArray: MutableList<Int>

        if (stepData.count() == 30)
            for (y in 0..24 step 6) {
                partialArray = emptyArray<Int>().toMutableList()
                for (x in y..y + 5) {
                    partialArray.add(stepData[x].value)
                    stepData[x]
                }
                dataArray.add(partialArray.sum())
            }

        return dataArray.toTypedArray()
    }

    protected fun getDataLabelsForLastMonth(stepsLastMonth: Array<GraphData>): Array<String> {
        val dataArray: MutableList<String> = mutableListOf()

        if (stepsLastMonth.isNotEmpty()) {
            for (y in 0..24 step 6) {
                val str =
                    "${stepsLastMonth[y].dateTime.month + 1}/${stepsLastMonth[y].dateTime.date}-${stepsLastMonth[y + 5].dateTime.date}"
                dataArray.add(str)
            }
        }

        return dataArray.toTypedArray()
    }

    // Last half year or year
    protected fun getDataForLastMonths(graphHistory: Array<GraphData>): Array<Int> {
        val dataArray: MutableList<Int> = mutableListOf()
        var partialArray: MutableList<Int>
        var range: Int

        if (graphHistory.isNotEmpty()) {
            var offset =
                YearMonth.of(graphHistory[0].dateTime.year, graphHistory[0].dateTime.month + 1)
                    .lengthOfMonth() - graphHistory[0].dateTime.date
            val firstMonth = graphHistory[offset + 1].dateTime.month + 1
            val lastMonth = graphHistory.last().dateTime.month + 1
            val lastDay = graphHistory.last().dateTime.date

            if (firstMonth + 1 > lastMonth) {
                for (x in firstMonth..12) {
                    partialArray = emptyArray<Int>().toMutableList()
                    range = YearMonth.of(graphHistory[offset + 1].dateTime.year, x).lengthOfMonth()

                    for (y in 1..range) {
                        partialArray.add(graphHistory[offset + y].value)
                    }
                    offset += YearMonth.of(graphHistory[offset + 1].dateTime.year, x)
                        .lengthOfMonth()
                    dataArray.add(partialArray.sum())
                }

                for (x in 1..lastMonth) {
                    partialArray = emptyArray<Int>().toMutableList()
                    range = YearMonth.of(graphHistory[offset + 1].dateTime.year, x).lengthOfMonth()

                    if (x == lastMonth) {
                        range = lastDay
                    }

                    for (y in 1..range) {
                        partialArray.add(graphHistory[offset + y].value)
                    }
                    offset += YearMonth.of(graphHistory[offset + 1].dateTime.year, x)
                        .lengthOfMonth()
                    dataArray.add(partialArray.sum())
                }
            } else {
                for (x in firstMonth..lastMonth) {
                    partialArray = emptyArray<Int>().toMutableList()
                    range = YearMonth.of(graphHistory[offset + 1].dateTime.year, x).lengthOfMonth()

                    if (x == lastMonth) {
                        range = lastDay
                    }

                    for (y in 1..range) {
                        partialArray.add(graphHistory[offset + y].value)
                    }
                    offset += YearMonth.of(graphHistory[offset + 1].dateTime.year, x)
                        .lengthOfMonth()
                    dataArray.add(partialArray.sum())
                }
            }
        }

        return dataArray.toTypedArray()
    }

    protected fun getDataLabelsForLastMonths(graphHistory: Array<GraphData>): Array<String> {
        val dataArray: MutableList<String> = mutableListOf()

        if (graphHistory.isNotEmpty()) {
            val offset =
                YearMonth.of(graphHistory[0].dateTime.year, graphHistory[0].dateTime.month + 1)
                    .lengthOfMonth() - graphHistory[0].dateTime.date
            val firstMonth = graphHistory[offset + 1].dateTime.month + 1
            val lastMonth = graphHistory.last().dateTime.month + 1

            if (firstMonth + 1 > lastMonth) {
                for (y in firstMonth..12) {
                    val str = Month.of(y).name
                    dataArray.add(str)
                }
                for (y in 1..lastMonth) {
                    val str = Month.of(y).name
                    dataArray.add(str)
                }
            } else {
                for (y in firstMonth..lastMonth) {
                    val str = Month.of(y).name
                    dataArray.add(str)
                }
            }
        }
        return dataArray.toTypedArray()
    }

    protected fun showProgressBar() {
        bindingElevatedView.containerProgress.visibility = View.VISIBLE
    }

    protected fun hideProgressBar() {
        bindingElevatedView.containerProgress.visibility = View.GONE
    }
}
