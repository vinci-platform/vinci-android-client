/*
 *  Copyright (c) 2020, Comtrade
 *  All rights reserved.
 *
 *  Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of the <organization> nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *   DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  Package: com.comtrade.vinciapp.view.activity.main.view
 *  File: MainActivity.kt
 *  Author: Milenko Bojanic <milenko.bojanic@comtrade.com>
 *
 *  Description: Main activity class, container for fragments after login
 *
 *  History: 8/11/20 Milenko  Initial code
 *
 *
 */

package com.comtrade.vinciapp.view.activity.main.view

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.location.LocationManager
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.Window
import android.widget.Button
import android.widget.TextView
import androidx.activity.viewModels
import androidx.core.location.LocationManagerCompat
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout.LOCK_MODE_LOCKED_CLOSED
import androidx.drawerlayout.widget.DrawerLayout.LOCK_MODE_UNLOCKED
import androidx.lifecycle.lifecycleScope
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.di.AppQualifiers
import com.comtrade.domain.settings.MobileAppSettings
import com.comtrade.domain.time.AppSyncIntervals
import com.comtrade.domain_ui.state.RequestUIState
import com.comtrade.feature_account.ui.auth.UserSignInViewModel
import com.comtrade.feature_account.ui.state.APICallSuccessUIState
import com.comtrade.feature_account.ui.user.GetUserViewModel
import com.comtrade.feature_account.ui.user.MobileAppSettingsViewModel
import com.comtrade.feature_account.ui.user.RefreshUserDataViewModel
import com.comtrade.feature_health_records.di.HealthRecordsComponent
import com.comtrade.feature_health_records.ui.SyncHealthToServerViewModel
import com.comtrade.feature_health_records.ui.score.CalculateUserHealthScoreUIState
import com.comtrade.feature_health_records.ui.score.CalculateUserHealthScoreViewModel
import com.comtrade.feature_steps.ui.AddStepsViewModel
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.VinciApp
import com.comtrade.vinciapp.base.BaseActivity
import com.comtrade.vinciapp.connection.ConnectionMonitorViewModel
import com.comtrade.vinciapp.connection.NetworkStateUI
import com.comtrade.vinciapp.databinding.ActivityMainBinding
import com.comtrade.vinciapp.databinding.ActivityMainNavigationBinding
import com.comtrade.vinciapp.receiver.AlarmReceiver
import com.comtrade.vinciapp.util.getFormattedTimeFromInstant
import com.comtrade.vinciapp.view.activity.main.core.MainPresenter
import com.comtrade.vinciapp.view.activity.main.core.contract.MainContract
import com.comtrade.vinciapp.view.activity.prelogin.view.PreloginActivity
import com.google.android.material.navigation.NavigationView
import com.vinci.feature_sync.ui.SyncViewModel
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.coroutines.*
import java.lang.Long.max
import java.time.Instant
import javax.inject.Inject
import javax.inject.Named

@SuppressLint("VisibleForTests")
class MainActivity :
    BaseActivity(),
    MainContract.View,
    View.OnClickListener,
    NavigationView.OnNavigationItemSelectedListener {

    private var syncInterval = MobileAppSettings.DEFAULT_APP_SYNC_INTERVAL
    var stepGoal = MobileAppSettings.DEFAULT_STEP_GOAL

    private var _binding: ActivityMainNavigationBinding? = null
    private val binding get() = _binding!!

    private var _bindingActivityMain: ActivityMainBinding? = null
    private val bindingActivityMain get() = _bindingActivityMain!!

    private var doubleBackToExitPressedOnce = false
    private lateinit var profileImageView: View
    private lateinit var profileImage: CircleImageView
    private val tagMainActivity = "MainActivity"
    private var connectionJob: Job? = null

    private var locationSettingsDialog: Dialog? = null

    @Inject
    lateinit var prefs: IPrefs

    @Inject
    @Named(AppQualifiers.ROOT_IMAGE_DIRECTORY)
    lateinit var directory: String

    private val signInViewModel: UserSignInViewModel by viewModels {
        (applicationContext as VinciApp).getAccountComponent()
            .provideUserSignInViewModelFactory()
    }

    private val mobileAppSettingsViewModel: MobileAppSettingsViewModel by viewModels {
        (applicationContext as VinciApp).getAccountComponent()
            .provideMobileAppSettingsViewModelFactory()
    }

    private val getUserViewModel: GetUserViewModel by viewModels {
        (applicationContext as VinciApp).getAccountComponent()
            .provideGetUserViewModelFactory()
    }

    private val refreshUserDataViewModel: RefreshUserDataViewModel by viewModels {
        (applicationContext as VinciApp).getAccountComponent()
            .provideRefreshUserDataViewModelFactory()
    }

    private val addStepsViewModel: AddStepsViewModel by viewModels {
        (applicationContext as VinciApp).getStepsComponent()
            .provideAddStepsViewModelFactory()
    }

    private val syncServiceViewModel: SyncViewModel by viewModels {
        (applicationContext as VinciApp).getSyncServiceComponent()
            .provideSyncViewModelFactory()
    }

    private val calculateHealthScoreViewModel: CalculateUserHealthScoreViewModel by viewModels {
        getHealthRecordsComponent().provideCalculateUserHealthScoreViewModelFactory()
    }

    private val syncHealthToServerViewModel: SyncHealthToServerViewModel by viewModels {
        getHealthRecordsComponent().provideSyncHealthToServerViewModelFactory()
    }

    // Network connectivity monitor
    private val connectionMonitorViewModel: ConnectionMonitorViewModel by viewModels {
        (applicationContext as VinciApp).getAppComponent()
            .provideConnectionMonitorViewModelFactory()
    }

    private val presenter: MainContract.Presenter by lazy {
        MainPresenter()
    }

    override fun getContentView(): View {
        _binding = ActivityMainNavigationBinding.inflate(layoutInflater)
        _bindingActivityMain = ActivityMainBinding.bind(binding.root)
        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        (application as VinciApp).appComponent.inject(this@MainActivity)

        observeSignIn()
        observeNetworkConnection()
        observeMobileAppSettings()
        observeGetUser()
        observeUserRefreshData()
        observeUserHealthScore()
        observeSyncHealthToServer()
        observeSyncServices()
        observeAddSteps()
        if (savedInstanceState == null) {
            if (prefs.isFirstRun) {
                presenter.openDevicesSetupScreen(supportFragmentManager)
                prefs.isFirstRun = false
            } else {

                val newFragment: String? = intent.getStringExtra(AlarmReceiver.NEXT_FRAGMENT_KEY)
                if (newFragment.toString() == "SurveyQuestionnaireFragment") {
                    presenter.openSurveyQuestionnaireScreen(supportFragmentManager)
                } else {
                    presenter.openMainScreen(supportFragmentManager)
                }
            }
        }

        bindingActivityMain.mainActivityMenuButton.setOnClickListener(this)
        bindingActivityMain.mainActivityBackButton.setOnClickListener(this)
        binding.navigationView.setNavigationItemSelectedListener(this)
        profileImageView = binding.navigationView.getHeaderView(0)
        profileImage = profileImageView.findViewById(R.id.navigation_header_profile_image)
        profileImage.setOnClickListener(this)
        handleNavigation()
        updateNavigationBar()

        // Check if location is turned on.
        if (shouldShowLocationDialog()) {
            showLocationSettingsDialog()
        }
    }

    override fun onStop() {
        super.onStop()

        locationSettingsDialog?.apply {
            if (isShowing) {
                dismiss()
            }
        }
    }

    override fun onDestroy() {

        connectionJob?.cancel()

        super.onDestroy()
    }

    private fun observeSignIn() {
        signInViewModel.signInEventObservable.observe(this@MainActivity) {
            when (it) {
                APICallSuccessUIState.DefaultState -> {
                }
                is APICallSuccessUIState.OnError -> {
                    prefs.connectionStatus =
                        getString(R.string.connection_status_authenticate_error, it)
                    prefs.connectionError =
                        prefs.connectionError + "\n" + prefs.connectionStatus

                    it.message?.let { message ->
                        showShortToast(this@MainActivity, message)
                    }
                    signInViewModel.resetState()
                    logout()
                }
                APICallSuccessUIState.OnLoading -> {
                    prefs.connectionError = ""
                    prefs.connectionStatus = getString(R.string.connection_status_waiting_for_login)
                }
                is APICallSuccessUIState.OnSuccess -> {
                    prefs.connectionStatus = getString(R.string.connection_status_login_successful)
                    startVinciConnectionHandler()
                    signInViewModel.resetState()
                }
            }
        }

        if (prefs.userAutologin) {
            signInViewModel.signIn(prefs.userLogin, prefs.userPassword, true)
        } else {
            logout()
        }
    }

    private fun observeNetworkConnection() {
        connectionMonitorViewModel.networkConnectionObserve.observe(this@MainActivity) {
            when (it) {
                is NetworkStateUI.NetworkType -> {
                    if (it.isAvailable) {
                        bindingActivityMain.imgNetworkStatus.visibility = View.GONE
                        bindingActivityMain.progressSyncServices.visibility = View.GONE
                        bindingActivityMain.imgSyncStatus.visibility = View.VISIBLE
                    } else {
                        bindingActivityMain.progressSyncServices.visibility = View.GONE
                        bindingActivityMain.imgSyncStatus.visibility = View.GONE
                        bindingActivityMain.imgNetworkStatus.visibility = View.VISIBLE
                    }
                }
            }
        }
    }

    private fun observeMobileAppSettings() {
        mobileAppSettingsViewModel.mobileAppSettings.observe(this@MainActivity) {
            when (it) {
                APICallSuccessUIState.DefaultState -> {}
                is APICallSuccessUIState.OnError -> {
                    it.message?.let { message ->
                        prefs.connectionError =
                            prefs.connectionError + "\n" + getString(
                                R.string.get_mobile_app_settings_error,
                                message
                            )
                        showShortToast(
                            this@MainActivity,
                            getString(
                                R.string.get_mobile_app_settings_error,
                                message
                            )
                        )
                    }
                    mobileAppSettingsViewModel.resetState()
                }
                APICallSuccessUIState.OnLoading -> {}
                is APICallSuccessUIState.OnSuccess -> {
                    syncInterval = it.data.syncInterval
                    stepGoal = it.data.stepGoal
                    mobileAppSettingsViewModel.resetState()
                }
            }
        }
    }

    private fun observeGetUser() {
        getUserViewModel.userState.observe(this@MainActivity) {
            when (it) {
                APICallSuccessUIState.DefaultState -> {}
                is APICallSuccessUIState.OnError -> {
                    it.message?.let { message ->
                        prefs.connectionError =
                            prefs.connectionError + "\n" + getString(
                                R.string.get_user_error,
                                message
                            )
                        showShortToast(this@MainActivity, message)
                    }
                    getUserViewModel.resetState()
                }
                APICallSuccessUIState.OnLoading -> {}
                is APICallSuccessUIState.OnSuccess -> {
                    refreshUserDataViewModel.refreshUserData(it.data, directory)
                    syncDataWithServer()
                    getUserViewModel.resetState()
                }
            }
        }
    }

    private fun observeUserRefreshData() {
        refreshUserDataViewModel.refreshUserDataObserve.observe(this@MainActivity) {
            when (it) {
                APICallSuccessUIState.DefaultState -> {}
                is APICallSuccessUIState.OnError -> {
                    it.message?.let { message ->
                        prefs.connectionError =
                            prefs.connectionError + "\n" + getString(
                                R.string.get_user_extra_error, message
                            )
                        showShortToast(this@MainActivity, message)
                    }
                    refreshUserDataViewModel.resetState()
                }
                APICallSuccessUIState.OnLoading -> {}
                is APICallSuccessUIState.OnSuccess -> {
                    if (!it.data) {
                        showShortToast(
                            this@MainActivity,
                            getString(R.string.main_screen_refresh_user_data_error)
                        )
                    }

                    refreshUserDataViewModel.resetState()
                }
            }
        }
    }

    private fun startVinciConnectionHandler() {
        connectionJob = CoroutineScope(Dispatchers.Main).launch {
            try {
                while (true) {
                    prefs.connectionError = ""
                    val start = Instant.now().toEpochMilli()
                    mobileAppSettingsViewModel.getMobileAppSettings()
                    getUserViewModel.getUser()

                    val delay1 =
                        syncInterval - (Instant.now().toEpochMilli() - start)
                    delay(max(delay1, AppSyncIntervals.VINCI_CONNECTION_UPDATE_INTERVAL))
                }
            } catch (e: Exception) {
                prefs.addLog("Master sync error: $e", true)
            }
        }
    }

    /**
     * Update the UI state based on the calculated health score.
     */
    private fun observeUserHealthScore() {
        calculateHealthScoreViewModel.userHealthScore.observe(this@MainActivity) {
            when (it) {
                is CalculateUserHealthScoreUIState.OnError -> Log.d(
                    "CALCULATE_USER_SCORE",
                    "startVinciConnectionHandler: ${it.message} "
                )
                CalculateUserHealthScoreUIState.OnLoading -> {} // Show progress bar if needed
                is CalculateUserHealthScoreUIState.OnSuccess -> {}
            }
        }
    }

    private fun observeSyncServices() {
        syncServiceViewModel.syncServicesObserve.observe(this@MainActivity) {
            when (it) {
                RequestUIState.DefaultState -> {
                    bindingActivityMain.imgNetworkStatus.visibility = View.GONE
                    bindingActivityMain.progressSyncServices.visibility = View.GONE
                    bindingActivityMain.imgSyncStatus.visibility = View.VISIBLE
                    bindingActivityMain.imgSyncStatus.setImageResource(R.drawable.ic_devices_background_white)
                }
                is RequestUIState.OnError -> handleSyncError(it)

                RequestUIState.OnLoading -> {
                    bindingActivityMain.imgSyncStatus.visibility = View.GONE
                    bindingActivityMain.imgNetworkStatus.visibility = View.GONE
                    bindingActivityMain.progressSyncServices.visibility = View.VISIBLE
                }
                is RequestUIState.OnSuccess -> handleSyncSuccess(it)
            }
        }
    }

    /**
     * Data synchronization major error handler.
     */
    private fun handleSyncError(it: RequestUIState.OnError) {
        it.message.let { message ->
            if (message != null) {
                prefs.connectionError = prefs.connectionError + "\n" + message
                prefs.addLog(message, true)
            } else {
                val errorMessage =
                    getString(R.string.main_screen_error_data_to_vinci_synchronization)
                logErrorMessage(prefs, errorMessage)
            }
        }
        prefs.connectionStatus =
            getString(R.string.connection_sync_error_at, prefs.lastSuccessfulSync)

        bindingActivityMain.imgNetworkStatus.visibility = View.GONE
        bindingActivityMain.progressSyncServices.visibility = View.GONE
        bindingActivityMain.imgSyncStatus.visibility = View.VISIBLE
        bindingActivityMain.imgSyncStatus.setImageResource(R.drawable.ic_devices_background_red)
        syncServiceViewModel.resetState()
    }

    /**
     * Data synchronization success handler.
     */
    private fun handleSyncSuccess(it: RequestUIState.OnSuccess<Int>) {
        bindingActivityMain.imgNetworkStatus.visibility = View.GONE
        bindingActivityMain.progressSyncServices.visibility = View.GONE
        bindingActivityMain.imgSyncStatus.visibility = View.VISIBLE
        when {
            it.data > 85 -> bindingActivityMain.imgSyncStatus.setImageResource(R.drawable.ic_devices_background_green)
            it.data in 51..85 -> bindingActivityMain.imgSyncStatus.setImageResource(R.drawable.ic_devices_background_yellow)
            it.data <= 50 -> bindingActivityMain.imgSyncStatus.setImageResource(R.drawable.ic_devices_background_red)
        }

        // If anything lower, we display the synchronization error status message
        if (it.data < 100) {
            prefs.connectionError =
                getString(R.string.main_screen_error_data_to_vinci_synchronization)
            prefs.connectionStatus =
                getString(R.string.connection_sync_error_at, prefs.lastSuccessfulSync)
        } else {
            prefs.connectionError = ""
            val formattedTime = getFormattedTimeFromInstant(Instant.now())
            prefs.lastSuccessfulSync = formattedTime
            prefs.connectionStatus =
                getString(
                    R.string.connection_sync_successful_at,
                    prefs.lastSuccessfulSync
                )
        }
    }

    /**
     * Update the UI state based on the health data synchronization to server action.
     */
    private fun observeSyncHealthToServer() {
        syncHealthToServerViewModel.syncHealthToServerObserver.observe(this@MainActivity) {
            when (it) {
                RequestUIState.DefaultState -> {}
                is RequestUIState.OnError -> {
                    it.message.let { message ->
                        if (message != null) {
                            prefs.connectionError = prefs.connectionError + "\n" + message
                            prefs.addLog(message, true)
                        } else {
                            val errorMessage =
                                getString(R.string.main_screen_error_health_data_synchronization)
                            logErrorMessage(prefs, errorMessage)
                        }
                    }
                    syncHealthToServerViewModel.resetState()
                }
                RequestUIState.OnLoading -> {}
                is RequestUIState.OnSuccess -> {
                    syncHealthToServerViewModel.resetState()
                }
            }
        }
    }

    /**
     * Update the UI state based on the add step result if needed.
     */
    private fun observeAddSteps() {
        addStepsViewModel.addStepsObserver.observe(this@MainActivity) {
            when (it) {
                RequestUIState.DefaultState -> {}
                is RequestUIState.OnError -> {
                    it.message?.let { message ->
                        Log.d(MainActivity::class.simpleName, "observeAddSteps: $message")
                    }
                    showShortToast(
                        this@MainActivity,
                        getString(R.string.main_screen_add_step_data_error)
                    )
                    addStepsViewModel.resetState()
                }
                RequestUIState.OnLoading -> {}
                is RequestUIState.OnSuccess -> {
                    addStepsViewModel.resetState()
                }
            }
        }
    }

    /**
     * Synchronize User survey and event data with the Vinci server.
     */
    private fun syncDataWithServer() {
        syncServiceViewModel.startSyncService()

        calculateUserHealthScore()
    }

    /**
     * Calculate User health score and sync data with server.
     */
    private fun calculateUserHealthScore() {
        calculateHealthScoreViewModel.calculateHealthScore(stepGoal)
        syncHealthToServerViewModel.syncHealthToServer()
    }

    /**
     * Toggle menu and back button arrow visibility based on the items currently in the navigation stack.
     */
    private fun handleNavigation() {
        supportFragmentManager.addOnBackStackChangedListener {
            if (supportFragmentManager.backStackEntryCount > 0) {
                // Show back arrow
                bindingActivityMain.mainActivityMenuButton.visibility = View.GONE
                bindingActivityMain.mainActivityBackButton.visibility = View.VISIBLE
                // Lock drawer in closed state
                binding.drawerLayout.setDrawerLockMode(LOCK_MODE_LOCKED_CLOSED)
            } else {
                // Show menu button
                bindingActivityMain.mainActivityBackButton.visibility = View.GONE
                bindingActivityMain.mainActivityMenuButton.visibility = View.VISIBLE
                binding.drawerLayout.setDrawerLockMode(LOCK_MODE_UNLOCKED)
            }
        }
    }

    fun updateNavigationBar() {
        val profileImageView: View = binding.navigationView.getHeaderView(0)
        val profileImage: CircleImageView =
            profileImageView.findViewById(R.id.navigation_header_profile_image)

        val navigationHeaderProfileEmail =
            profileImageView.findViewById<TextView>(R.id.navigation_header_profile_email)
        navigationHeaderProfileEmail?.apply {
            text = prefs.userEmail
        }

        if (prefs.userProfilePicture.isNotEmpty()) {
            Glide.with(this@MainActivity)
                .load(prefs.userProfilePicture)
                .apply(
                    RequestOptions().diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true)
                )
                .centerCrop()
                .into(profileImage)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.navigation_home -> {
                presenter.goToMainScreen(supportFragmentManager)
            }
            R.id.navigation_profile -> {
                presenter.openProfileSettingsScreen(supportFragmentManager)
            }
            R.id.navigation_questionnaire -> {
                presenter.openQuestionnaireScreen(supportFragmentManager)
            }
            R.id.navigation_devices -> {
                presenter.openAddDeviceScreen(supportFragmentManager)
            }
            R.id.navigation_settings -> {
                presenter.openNotificationsSettingsScreen(supportFragmentManager)
            }
            R.id.navigation_logout -> {
                logout()
            }
            R.id.navigation_about -> {
                presenter.openAboutScreen(supportFragmentManager)
            }
        }
        binding.drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onClick(view: View?) {
        when (view) {
            bindingActivityMain.mainActivityMenuButton -> {
                binding.drawerLayout.openDrawer(GravityCompat.START)
            }
            bindingActivityMain.mainActivityBackButton -> {
                super.onBackPressed()
            }
            profileImage -> {
                presenter.openProfilePictureScreen(supportFragmentManager)
                binding.drawerLayout.closeDrawer(GravityCompat.START)
            }
        }
    }

    private fun logout() {
        prefs.userAutologin = false
        prefs.userLogin = ""
        prefs.userPassword = ""
        prefs.setUserData(null)
        prefs.setUserExtraData(null)
        val intent = Intent(this, PreloginActivity::class.java)
        startActivity(intent)
        finish()
    }

    override fun onBackPressed() {
        if (doubleBackToExitPressedOnce || supportFragmentManager.backStackEntryCount != 0) {
            super.onBackPressed()
            return
        }

        this.doubleBackToExitPressedOnce = true
        showShortToast(this, getString(R.string.main_screen_double_back_to_exit_message))

        lifecycleScope.launch(Dispatchers.Main) {
            returnBackPressedToOriginalState()
        }
    }

    private suspend fun returnBackPressedToOriginalState() {
        delay(1500)
        doubleBackToExitPressedOnce = false
    }

    override fun onUpdateShoeDeviceSuccess() {
        Log.d(tagMainActivity, "Device updated successfully")
    }

    override fun onUpdateShoeDeviceFailure(throwable: Throwable, message: String?) {
        message?.let {
            Log.d(tagMainActivity, message)
        }
    }

    /**
     * Checks if location settings intent can be resolved on a particular
     * device and if the location settings in enabled.
     * @return The display location enabling dialog rationale.
     */
    private fun shouldShowLocationDialog(): Boolean {
        val locationSettingsIntent =
            Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS)
        val canResolveSettingsActivity =
            locationSettingsIntent.resolveActivity(packageManager) != null

        val locationManager = getSystemService(
            Context.LOCATION_SERVICE
        ) as LocationManager
        return canResolveSettingsActivity && !LocationManagerCompat.isLocationEnabled(
            locationManager
        )
    }

    /**
     * Enable device location dialog.
     */
    private fun showLocationSettingsDialog() {
        locationSettingsDialog = Dialog(this@MainActivity)
        locationSettingsDialog?.apply {

            requestWindowFeature(Window.FEATURE_NO_TITLE)
            setCancelable(false)
            setContentView(R.layout.dialog_enable_location)
            findViewById<Button>(R.id.dismiss_activity_dialog_yes_button).setOnClickListener {
                // Launch settings activity.
                startActivity(Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS))
                dismiss()
            }
            findViewById<Button>(R.id.dismiss_activity_dialog_no_button).setOnClickListener {
                dismiss()
            }
            show()
        }
    }

    /**
     * Log any connection error messages into the local storage.
     */
    private fun logErrorMessage(prefs: IPrefs, errorMessage: String) {
        prefs.connectionError = prefs.connectionError + "\n" + errorMessage
        prefs.addLog(errorMessage, true)
    }
}

/**
 * Obtain the Dagger dependency component extension.
 */
@SuppressLint("VisibleForTests")
internal fun MainActivity.getHealthRecordsComponent(): HealthRecordsComponent =
    (applicationContext as VinciApp).getHealthRecordsComponent()
