/*
 *  Copyright (c) 2020, Comtrade
 *  All rights reserved.
 *
 *  Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of the <organization> nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *   DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  Package: com.comtrade.vinciapp.view.widget.addNewDevice.view
 *  File: AddNewDeviceFragment.kt
 *  Author: Sofija Andjelkovic <sofija.andjelkovic@comtrade.com>
 *
 *  Description: a two-button fragment for different types of adding a new device
 *
 *  History: 8/11/20 Sofija  Initial code
 *
 *
 */

package com.comtrade.vinciapp.view.widget.heartRateHistory.view

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.viewModels
import com.comtrade.domain.entities.graph.GraphData
import com.comtrade.domain.entities.step.FitbitDataType
import com.comtrade.domain.extensions.asRoundInt
import com.comtrade.domain_ui.state.RequestUIState
import com.comtrade.feature_devices.ui.fitbit.FitBitPeriodDataViewModel
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.VinciApp
import com.comtrade.vinciapp.view.adapter.QuestionnaireViewPagerAdapter
import com.comtrade.vinciapp.view.widget.historyGraph.view.HistoryGraphFragment
import com.comtrade.vinciapp.view.widget.historyGraph.view.HistoryGraphScreen
import java.text.DecimalFormat

@SuppressLint("VisibleForTests")
class HeartRateHistoryFragment : HistoryGraphFragment() {
    private lateinit var restingHeartRateLastWeek: Array<GraphData>
    private lateinit var restingHeartRateLastMonth: Array<GraphData>
    private lateinit var restingHeartRateLastHalfYear: Array<GraphData>
    private lateinit var restingHeartRateLastYear: Array<GraphData>

    private val fitBitPeriodDataViewModel: FitBitPeriodDataViewModel by viewModels {
        (requireContext().applicationContext as VinciApp).getDevicesComponent()
            .provideFitBitPeriodDataViewModelFactory()
    }

    companion object {
        const val TAG: String = "HeartRateHistoryFragment"

        fun newInstance(): HeartRateHistoryFragment {
            return HeartRateHistoryFragment()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeHeartRateData()
    }

    /**
     * Handle the possible UI states regarding the FitBit heart rate data.
     */
    private fun observeHeartRateData() {
        fitBitPeriodDataViewModel.fitBitPeriodDataObserver.observe(viewLifecycleOwner) {
            when (it) {
                RequestUIState.DefaultState -> {
                    hideProgressBar()
                }
                is RequestUIState.OnError -> {
                    hideProgressBar()
                    it.message?.let { _ ->
                        showShortToast(
                            getString(R.string.heart_rate_history_screen_fitbit_error)
                        )
                    }
                    fitBitPeriodDataViewModel.resetState()
                }
                RequestUIState.OnLoading -> {
                    showProgressBar()
                }
                is RequestUIState.OnSuccess -> {
                    hideProgressBar()
                    handleObtainedFitBitData(it.data)
                    fitBitPeriodDataViewModel.resetState()
                }
            }
        }
    }

    private fun handleObtainedFitBitData(data: Map<FitbitDataType, Map<Int, Array<GraphData>>>) {

        for (activityData in data) {
            when (activityData.key) {
                FitbitDataType.RESTING_HEART_RATE -> {
                    restingHeartRateLastYear =
                        activityData.value[365]!!
                    restingHeartRateLastWeek = activityData.value[7]!!
                    restingHeartRateLastMonth = activityData.value[30]!!
                    restingHeartRateLastHalfYear = activityData.value[183]!!

                    Log.w("FITBIT LAST YEAR", restingHeartRateLastYear.size.toString())
                    Log.w("FITBIT LAST WEEK", restingHeartRateLastWeek.size.toString())
                    Log.w("FITBIT LAST MONTH", restingHeartRateLastMonth.size.toString())
                    Log.w("FITBIT LAST HALF YEAR", restingHeartRateLastHalfYear.size.toString())
                }
                else -> {}
            }
        }

        setAdapter()
    }

    private fun setAdapter() {
        fragmentsPagerAdapter =
            QuestionnaireViewPagerAdapter(childFragmentManager, viewLifecycleOwner.lifecycle)

        val weekData: HashMap<String, Array<Int>> = hashMapOf()
        val monthData: HashMap<String, Array<Int>> = hashMapOf()
        val halfYearData: HashMap<String, Array<Int>> = hashMapOf()
        val yearData: HashMap<String, Array<Int>> = hashMapOf()

        weekData["Resting Heart Rate"] = getDataForLastWeek(restingHeartRateLastWeek)
        monthData["Resting Heart Rate"] = getDataForLastMonth(restingHeartRateLastMonth)
        halfYearData["Resting Heart Rate"] = getDataForLastMonths(restingHeartRateLastHalfYear)
        yearData["Resting Heart Rate"] = getDataForLastMonths(restingHeartRateLastYear)

        val labelsLastWeek = getDataLabelsForLastWeek(restingHeartRateLastWeek)
        val labelsLastMonth = getDataLabelsForLastMonth(restingHeartRateLastMonth)
        val labelsLastHalfYear = getDataLabelsForLastMonths(restingHeartRateLastHalfYear)
        val labelsLastYear = getDataLabelsForLastMonths(restingHeartRateLastYear)

        fragmentsPagerAdapter.addFragment(
            HistoryGraphScreen.newInstance(
                getString(R.string.heart_rate_history_screen_fitbit_title),
                getString(R.string.history_graph_screen_average),
                DecimalFormat("#,###").format(
                    weekData.asIterable().first().value.asIterable().average().asRoundInt()
                ).toString(),
                getString(R.string.history_graph_screen_max),
                DecimalFormat("#,###").format(
                    weekData.asIterable().first().value.asIterable().maxOrDefault(0)
                ).toString(),
                labelsLastWeek,
                weekData
            )
        )
        fragmentsPagerAdapter.addFragment(
            HistoryGraphScreen.newInstance(
                getString(R.string.heart_rate_history_screen_fitbit_title),
                getString(R.string.history_graph_screen_average),
                DecimalFormat("#,###").format(
                    monthData.asIterable().first().value.asIterable().average().asRoundInt()
                ).toString(),
                getString(R.string.history_graph_screen_max),
                DecimalFormat("#,###").format(
                    monthData.asIterable().first().value.asIterable().maxOrDefault(0)
                ).toString(),
                labelsLastMonth,
                monthData
            )
        )
        fragmentsPagerAdapter.addFragment(
            HistoryGraphScreen.newInstance(
                getString(R.string.heart_rate_history_screen_fitbit_title),
                getString(R.string.history_graph_screen_average),
                DecimalFormat("#,###").format(
                    halfYearData.asIterable().first().value.asIterable().average().asRoundInt()
                ).toString(),
                getString(R.string.history_graph_screen_max),
                DecimalFormat("#,###").format(
                    halfYearData.asIterable().first().value.asIterable().maxOrDefault(0)
                ).toString(),
                labelsLastHalfYear,
                halfYearData
            )
        )
        fragmentsPagerAdapter.addFragment(
            HistoryGraphScreen.newInstance(
                getString(R.string.heart_rate_history_screen_fitbit_title),
                getString(R.string.history_graph_screen_average),
                DecimalFormat("#,###").format(
                    yearData.asIterable().first().value.asIterable().average().asRoundInt()
                ).toString(),
                getString(R.string.history_graph_screen_max),
                DecimalFormat("#,###").format(
                    yearData.asIterable().first().value.asIterable().maxOrDefault(0)
                ).toString(),
                labelsLastYear,
                yearData
            )
        )

        viewPager.adapter = fragmentsPagerAdapter

        // Attach Tab to view adapter.
        setTab()
    }

    override fun getData() {

        fitBitPeriodDataViewModel.getFitBitDataForTypes(
            intArrayOf(7, 30, 183, 365),
            arrayOf(FitbitDataType.RESTING_HEART_RATE)
        )
    }
}

/**
 * Helper extension function for providing a default value when obtaining a maximum value from an Iterable or a default provided value of the same type.
 */
fun <T : Comparable<T>> Iterable<T>.maxOrDefault(defaultValue: T): T {
    val iterator = iterator()
    if (!iterator.hasNext()) return defaultValue
    var max = iterator.next()
    while (iterator.hasNext()) {
        val e = iterator.next()
        if (max < e) max = e
    }
    return max
}
