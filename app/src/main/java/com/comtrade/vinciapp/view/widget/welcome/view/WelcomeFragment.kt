/*
 *  Copyright (c) 2020, Comtrade
 *  All rights reserved.
 *
 *  Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of the <organization> nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *   DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  Package: com.comtrade.vinciapp.view.widget.welcome.view
 *  File: WelcomeFragment.kt
 *  Author: Milenko Bojanic <milenko.bojanic@comtrade.com>
 *
 *  Description: Welcome screen of the application
 *
 *  History: 8/11/20 Milenko  Initial code
 *
 *
 */

package com.comtrade.vinciapp.view.widget.welcome.view

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.VinciApp
import com.comtrade.vinciapp.base.BaseFragment
import com.comtrade.vinciapp.databinding.ElevatedProgressViewBinding
import com.comtrade.vinciapp.databinding.FragmentWelcomeBinding
import com.comtrade.vinciapp.util.AlertDialogUtils
import com.comtrade.vinciapp.util.ViewUtils
import com.comtrade.vinciapp.view.widget.welcome.core.WelcomePresenter
import com.comtrade.vinciapp.view.widget.welcome.core.contract.WelcomeContract
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.facebook.share.Sharer
import com.facebook.share.model.ShareLinkContent
import com.facebook.share.widget.ShareDialog
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import javax.inject.Inject


class WelcomeFragment : BaseFragment(), WelcomeContract.View, View.OnClickListener {

    private var _binding: FragmentWelcomeBinding? = null
    private val binding get() = _binding!!

    private var _bindingElevatedView: ElevatedProgressViewBinding? = null
    private val bindingElevatedView get() = _bindingElevatedView!!

    private var callbackManager: CallbackManager? = null
    private var linkContent: ShareLinkContent? = null

    //  private lateinit var auth: FirebaseAuth
    //   private lateinit var signInClient: GoogleSignInClient
    private lateinit var signInOptions: GoogleSignInOptions

    @Inject
    lateinit var prefs: IPrefs

    private val presenter: WelcomeContract.Presenter by lazy {
        WelcomePresenter()
    }

    companion object {
        const val TAG: String = "WelcomeFragment"

        fun newInstance(): WelcomeFragment {
            return WelcomeFragment()
        }
    }

//    private val googleSignInActionResult =
//        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
//            val task: Task<GoogleSignInAccount> = GoogleSignIn.getSignedInAccountFromIntent(it.data)
//            try {
//                val account = task.getResult(ApiException::class.java)
//                if (account != null) {
//                    googleFirebaseAuth(account)
//                }
//            } catch (e: ApiException) {
//                showShortToast(getString(R.string.welcome_screen_google_exception, e))
//            }
//        }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (context.applicationContext as VinciApp).getAppComponent()
            .inject(this@WelcomeFragment)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentWelcomeBinding.inflate(inflater, container, false)
        _bindingElevatedView = ElevatedProgressViewBinding.bind(binding.root)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListeners()
//        auth = FirebaseAuth.getInstance()
//        setupGoogleLogin()
        binding.welcomeFragmentTermsCheckbox.isChecked = prefs.isTermsAccepted
    }

    override fun onResume() {
        super.onResume()
        val areTermsAccepted = prefs.isTermsAccepted
        binding.welcomeFragmentTermsCheckbox.isChecked = areTermsAccepted
        binding.welcomeFragmentLoginButton.isEnabled = areTermsAccepted
        binding.welcomeFragmentCreateAccountButton.isEnabled = areTermsAccepted
        binding.welcomeFragmentFacebookButton.isEnabled = false
        binding.welcomeFragmentGoogleButton.isEnabled = false
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun setListeners() {
        binding.welcomeFragmentLoginButton.setOnClickListener(this)
        binding.welcomeFragmentCreateAccountButton.setOnClickListener(this)
        binding.welcomeFragmentFacebookButton.setOnClickListener(this)
        binding.welcomeFragmentGoogleButton.setOnClickListener(this)
        binding.welcomeFragmentTermsCheckbox.setOnClickListener(this)
    }

    private fun login() {
//        val loginIntent: Intent = signInClient.signInIntent
//        googleSignInActionResult.launch(loginIntent)
    }

    // private fun setupGoogleLogin() {
    // TODO: Enable feature when new Google account created.
//        signInOptions = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
//            .requestIdToken(getString(R.string.default_web_client_id))
//            .requestEmail()
//            .build()
//        signInClient = GoogleSignIn.getClient(this.requireActivity(), signInOptions)
//    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        callbackManager?.onActivityResult(requestCode, resultCode, data)

    }

    //   private fun googleFirebaseAuth(acct: GoogleSignInAccount) {
//        val credential = GoogleAuthProvider.getCredential(acct.idToken, null)
//        auth.signInWithCredential(credential).addOnCompleteListener {
//            if (it.isSuccessful) {
//                presenter.showInviteGoogleContactsScreen(requireActivity().supportFragmentManager)
//            } else {
//                showShortToast(getString(R.string.google_error_text))
//            }
//        }
    //   }

    override fun onClick(view: View?) {
        when (view) {
            binding.welcomeFragmentLoginButton -> {
                presenter.showLoginScreen(requireActivity().supportFragmentManager)
            }
            binding.welcomeFragmentCreateAccountButton -> {
                presenter.showCreateUserScreen(requireActivity().supportFragmentManager)
            }
            binding.welcomeFragmentFacebookButton -> {
                val shareDialog = ShareDialog(this)
                callbackManager = CallbackManager.Factory.create()

                LoginManager.getInstance()
                    .logInWithReadPermissions(this, listOf("public_profile", "email"))
                LoginManager.getInstance().registerCallback(callbackManager,
                    object : FacebookCallback<LoginResult> {
                        override fun onSuccess(loginResult: LoginResult) {
                            shareDialog.registerCallback(callbackManager,
                                object : FacebookCallback<Sharer.Result> {
                                    override fun onSuccess(result: Sharer.Result?) {
                                        presenter.openMainScreen(requireActivity())
                                    }

                                    override fun onCancel() {
                                        presenter.openMainScreen(requireActivity())
                                    }

                                    override fun onError(error: FacebookException?) {
                                        showShortToast(getString(R.string.welcome_screen_facebook_share_error))
                                    }
                                }
                            )

                            linkContent = ShareLinkContent.Builder()
                                .setQuote("Test link")
                                .setContentUrl(Uri.parse("https://developers.facebook.com"))
                                .build()
                            shareDialog.show(linkContent)
                        }

                        override fun onCancel() {
                            showShortToast(getString(R.string.welcome_screen_facebook_cancelled_message))
                        }

                        override fun onError(error: FacebookException) {
                            showShortToast(getString(R.string.welcome_screen_facebook_error))
                        }
                    }
                )
            }
            binding.welcomeFragmentGoogleButton -> {
                login()
            }
            binding.welcomeFragmentTermsCheckbox -> {
                checkTermsAndConditions()
            }
        }
    }

    private fun checkTermsAndConditions() {
        if (binding.welcomeFragmentTermsCheckbox.isChecked) {
            presenter.showTermsAndConditionsScreen(
                requireActivity().supportFragmentManager
            )
        } else {
            binding.welcomeFragmentTermsCheckbox.isChecked = false
            prefs.isTermsAccepted = false
            binding.welcomeFragmentLoginButton.isEnabled = false
            binding.welcomeFragmentCreateAccountButton.isEnabled = false
            binding.welcomeFragmentFacebookButton.isEnabled = false
            binding.welcomeFragmentGoogleButton.isEnabled = false
        }
    }

    override fun showHttpError(errorCode: String, message: String) {
        val title = getString(R.string.shared_error_code_title) + " $errorCode"
        AlertDialogUtils.showInfoAlertDialog(this.requireContext(), title, message)
    }

    override fun showUnexpectedError(message: String) {
        AlertDialogUtils.showUnexpectedErrorAlertDialog(this.requireContext(), message)
    }

    override fun showProgressBar() {
        ViewUtils.showView(bindingElevatedView.containerProgress)
    }

    override fun hideProgressBar() {
        ViewUtils.hideView(bindingElevatedView.containerProgress)
    }
}