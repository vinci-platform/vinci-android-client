/*
 *  Copyright (c) 2020, Comtrade
 *  All rights reserved.
 *
 *  Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of the <organization> nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *   DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  Package: com.comtrade.vinciapp.view.widget.mainScreen.view
 *  File: MainScreenFragment.kt
 *  Author: Milenko Bojanic <milenko.bojanic@comtrade.com>
 *
 *  Description: Fragment screen for Main screen of the application
 *
 *  History: 8/11/20 Milenko Initial code
 *           8/20/20 Sofija  Implement emergency call
 *           9/15/20 Sofija Implement status of devices
 *
 *
 */

package com.comtrade.vinciapp.view.widget.mainScreen.view

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothManager
import android.bluetooth.le.BluetoothLeScanner
import android.bluetooth.le.ScanCallback
import android.bluetooth.le.ScanResult
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.PointF
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.NonNull
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.time.AppSyncIntervals
import com.comtrade.feature_events.di.EventsComponent
import com.comtrade.feature_events.ui.events.EventsViewModel
import com.comtrade.feature_events.ui.events.UserEventRecordsUIState
import com.comtrade.feature_health_records.di.HealthRecordsComponent
import com.comtrade.feature_health_records.ui.score.CalculateUserHealthScoreUIState
import com.comtrade.feature_health_records.ui.score.CalculateUserHealthScoreViewModel
import com.comtrade.feature_questionnaire.survey.ui.FeelingsSurveyViewModel
import com.comtrade.feature_questionnaire.survey.ui.state.SaveSurveyUIState
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.VinciApp
import com.comtrade.vinciapp.base.BaseFragment
import com.comtrade.vinciapp.databinding.FragmentMainScreenBinding
import com.comtrade.vinciapp.di.AppComponent
import com.comtrade.vinciapp.util.AlertDialogUtils
import com.comtrade.vinciapp.view.activity.main.view.MainActivity
import com.comtrade.vinciapp.view.widget.mainScreen.core.MainScreenPresenter
import com.comtrade.vinciapp.view.widget.mainScreen.core.contract.MainScreenContract
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import ru.dimorinny.showcasecard.ShowCaseView
import ru.dimorinny.showcasecard.position.Position
import ru.dimorinny.showcasecard.radius.Radius
import java.time.Instant
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit
import javax.inject.Inject

class MainScreenFragment :
    BaseFragment(),
    MainScreenContract.View,
    View.OnClickListener {

    private val showHintsDelay = 5L

    private var _binding: FragmentMainScreenBinding? = null
    private val binding get() = _binding!!

    private var listDevices = mutableSetOf<String?>()

    private val eventsViewModel: EventsViewModel by viewModels {
        getEventsComponent().provideEventsViewModelFactory()
    }

    private val calculateHealthScoreViewModel: CalculateUserHealthScoreViewModel by viewModels {
        getHealthRecordsComponent().provideCalculateUserHealthScoreViewModelFactory()
    }

    private val feelingsSurveyViewModel: FeelingsSurveyViewModel by viewModels {
        getAppComponent().provideFeelingsSurveyViewModelFactory()
    }

    @Inject
    lateinit var prefs: IPrefs

    private val presenter: MainScreenContract.Presenter by lazy {
        MainScreenPresenter()
    }

    companion object {
        const val TAG: String = "MainScreenFragment"

        fun newInstance(): MainScreenFragment {
            return MainScreenFragment()
        }
    }

    @SuppressLint("MissingPermission")
    private val bluetoothScanPermissionActionResult =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) {
            if (!it) {
                AlertDialogUtils.showInfoAlertDialog(
                    requireContext(),
                    getString(R.string.PERMISSION_TITLE),
                    getString(R.string.RATIONALE_LOCATION)
                )
            } else {
                bluetoothLeScanner?.startScan(bleScanner)
            }
        }

    private val bluetoothEnableActionResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            if (it.resultCode == Activity.RESULT_OK) {
                scanDevice()
            } else {
                binding.mainScreenFragmentDevicesBackground.setImageResource(R.drawable.ic_devices_background_red)
            }
        }

    private val locationPermissionActionResult =
        registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) {
            var result = true
            for (mapEntry in it) {
                if (!mapEntry.value) {
                    result = false
                    break
                }
            }

            if (!result) {
                AlertDialogUtils.showInfoAlertDialog(
                    this.requireContext(),
                    getString(R.string.shared_permission_denied_alert_title),
                    getString(R.string.shared_permission_denied_alert_message)
                )
            } else {
                scanDevice()
            }
        }

    private val callPermissionActionResult =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) {
            if (it) {
                dispatchPhoneCallIntent()
            } else {
                AlertDialogUtils.showInfoAlertDialog(
                    this.requireContext(),
                    getString(R.string.shared_permission_denied_alert_title),
                    getString(R.string.shared_permission_denied_alert_message)
                )
            }
        }

    @SuppressLint("VisibleForTests")
    override fun onAttach(context: Context) {
        super.onAttach(context)
        (context.applicationContext as VinciApp).getAppComponent()
            .inject(this@MainScreenFragment)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMainScreenBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        updateMainScreen()
        setListeners()
        createChannel(
            getString(R.string.notification_channel_id),
            getString(R.string.notification_channel_name)
        )
        // showing only on the first run of the application
        if (prefs.showHints) {
            disableEnableViews(false)
            Handler(Looper.getMainLooper()).postDelayed({
                showHints(
                    getString(R.string.main_screen_hints_points_won),
                    280f,
                    binding.mainScreenFragmentPoints,
                    0
                )
            }, showHintsDelay)

            prefs.showHints = false
        }

        observeViewModels()
        observeScoreData()
        updateScore()
        updateNextEvent()
        listDevices.clear()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun observeScoreData() {
        calculateHealthScoreViewModel.userHealthScore.observe(viewLifecycleOwner) {
            when (it) {
                is CalculateUserHealthScoreUIState.OnError -> Log.d(
                    tag,
                    "updateScore: ${it.message} "
                )
                CalculateUserHealthScoreUIState.OnLoading -> {} // Update progress if needed
                is CalculateUserHealthScoreUIState.OnSuccess -> updateScorePoints(it.data)
            }
        }
    }

    private fun updateNextEvent() {

        eventsViewModel.eventRecords.observe(viewLifecycleOwner) {
            when (it) {
                UserEventRecordsUIState.OnEmptyDataSet -> {
                    binding.mainScreenFragmentEventTextView.text =
                        getString(R.string.main_screen_no_next_event_label)
                }
                is UserEventRecordsUIState.OnError -> {}
                UserEventRecordsUIState.OnLoading -> {
                    binding.mainScreenFragmentEventTextView.text =
                        getString(R.string.main_screen_no_next_event_label)
                }
                is UserEventRecordsUIState.OnSuccess -> {
                    val start = Instant.ofEpochMilli(it.data[0].timestamp)
                    val formatter: DateTimeFormatter =
                        if (start.truncatedTo(ChronoUnit.DAYS) == Instant.now()
                                .truncatedTo(ChronoUnit.DAYS)
                        ) {
                            DateTimeFormatter.ofPattern("HH:mm")
                        } else {
                            DateTimeFormatter.ofPattern("MM/dd/yyyy HH:mm")
                        }
                    val formattedString: String =
                        start.atZone(ZoneId.systemDefault()).format(formatter)
                    binding.mainScreenFragmentEventTextView.text =
                        getString(R.string.main_screen_next_event_info_label, formattedString)
                }
            }
        }

        eventsViewModel.getUserEventRecords(
            prefs.userId,
            Instant.now().toEpochMilli()
        )
    }

    // TODO: problematic code with infinite loops.
    private fun updateScore() {
        viewLifecycleOwner.lifecycleScope.launch {
            while (true) {
                if (activity != null && activity is MainActivity && view != null) { // Check for lifecycle view.
                    val stepGoal = (activity as MainActivity).stepGoal
                    calculateHealthScoreViewModel.calculateHealthScore(stepGoal)
                } else {
                    break
                }

                delay(AppSyncIntervals.HEALTH_SCORE_UPDATE_INTERVAL)
            }
        }
    }

    private fun updateScorePoints(score: Int) {
        binding.mainScreenFragmentPoints.text = score.toString()
        binding.mainScreenFragmentPointsValue.text = when {
            score < 50 -> ""
            score < 100 -> getString(R.string.main_screen_points_good)
            else -> getString(R.string.main_screen_points_excellent)
        }
    }

    @SuppressLint("MissingPermission")
    override fun onStop() {
        val bluetoothManager =
            requireContext().getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        val bluetoothAdapter = bluetoothManager.adapter
        if (bluetoothAdapter != null && bluetoothLeScanner != null) {
            if (isPermissionGranted(Manifest.permission.BLUETOOTH_SCAN)) {
                bluetoothLeScanner?.stopScan(bleScanner)
            }
        }

        super.onStop()
    }

    private fun observeViewModels() {
        feelingsSurveyViewModel.storePointsResult.observe(viewLifecycleOwner) {
            when (it) {
                SaveSurveyUIState.DefaultState -> {}
                is SaveSurveyUIState.OnError -> {
                    Log.d(
                        tag,
                        "save survey error: ${it.message} "
                    )
                }
                SaveSurveyUIState.OnLoading -> {}
                is SaveSurveyUIState.OnSuccess -> {}
            }
        }
    }

    private fun setListeners() {
        binding.mainScreenFragmentCognitiveGamesContainer.setOnClickListener(this)
        binding.mainScreenFragmentPhysicalActivityContainer.setOnClickListener(this)
        binding.mainScreenFragmentFriendsContainer.setOnClickListener(this)
        binding.mainScreenFragmentQualityOfLifeContainer.setOnClickListener(this)
        binding.mainScreenFragmentTodayFeelingsContainer.setOnClickListener(this)
        binding.mainScreenFragmentEmergencyCallContainer.setOnClickListener(this)
        binding.mainScreenFragmentEventBackgroundImage.setOnClickListener(this)
        binding.mainScreenFragmentProfileImage.setOnClickListener(this)
    }

    private fun showHints(
        content: String,
        radius: Float,
        view: View,
        type: Int
    ) {
        val location = IntArray(2)
        view.getLocationInWindow(location)
        val x: Float = location[0].toFloat() + view.width / 2
        val y: Float = location[1].toFloat() + view.height / 2
        ShowCaseView.Builder(this.requireActivity())
            .withTypedPosition(Position(PointF(x, y)))
            .withTypedRadius(Radius(radius))
            .withContent(content)
            .withDismissListener {
                when (type) {
                    0 -> {
                        showHints(
                            getString(R.string.main_screen_hints_next_event),
                            radius,
                            binding.mainScreenFragmentEventTextView,
                            1
                        )
                    }
                    1 -> {
                        showHints(
                            getString(R.string.main_screen_hints_quality_of_life),
                            radius,
                            binding.mainScreenFragmentQualityOfLifeContainer,
                            2
                        )
                    }
                    2 -> {
                        showHints(
                            getString(R.string.main_screen_hints_physical_activity),
                            radius,
                            binding.mainScreenFragmentPhysicalActivityContainer,
                            3
                        )
                    }
                    3 -> {
                        showHints(
                            getString(R.string.main_screen_hints_friends),
                            radius,
                            binding.mainScreenFragmentFriendsContainer,
                            4
                        )
                    }
                    4 -> {
                        showHints(
                            getString(R.string.main_screen_hints_cognitive_games),
                            radius,
                            binding.mainScreenFragmentCognitiveGamesContainer,
                            5
                        )
                    }
                    5 -> {
                        showHints(
                            getString(R.string.main_screen_hints_emergency_call),
                            radius,
                            binding.mainScreenFragmentEmergencyCallContainer,
                            6
                        )
                    }
                    6 -> {
                        showHints(
                            getString(R.string.main_screen_hints_todays_feelings),
                            radius,
                            binding.mainScreenFragmentTodayFeelingsContainer,
                            7
                        )
                    }
                    7 -> {
                        Handler(Looper.getMainLooper()).postDelayed({
                            disableEnableViews(true)
                        }, 1)
                    }
                }
            }
            .build()
            .show(this.requireActivity())
    }

    private fun disableEnableViews(enable: Boolean) {
        binding.mainScreenFragmentPhysicalActivityContainer.isEnabled = enable
        binding.mainScreenFragmentQualityOfLifeContainer.isEnabled = enable
        binding.mainScreenFragmentCognitiveGamesContainer.isEnabled = enable
        binding.mainScreenFragmentFriendsContainer.isEnabled = enable
        binding.mainScreenFragmentEmergencyCallContainer.isEnabled = enable
        binding.mainScreenFragmentTodayFeelingsContainer.isEnabled = enable
        binding.mainScreenFragmentEventBackgroundImage.isEnabled = enable
    }

    override fun onClick(view: View?) {
        when (view) {
            binding.mainScreenFragmentCognitiveGamesContainer -> {
                presenter.openCognitiveGamesScreen(requireActivity().supportFragmentManager)
            }
            binding.mainScreenFragmentPhysicalActivityContainer -> {
                presenter.openPhysicalActivitiesScreen(requireActivity().supportFragmentManager)
            }
            binding.mainScreenFragmentFriendsContainer -> {
                presenter.showFriendsViewScreen(requireActivity().supportFragmentManager)
            }
            binding.mainScreenFragmentQualityOfLifeContainer -> {
                presenter.showWhoQolQuestionnaireScreen(requireActivity().supportFragmentManager)
            }
            binding.mainScreenFragmentEmergencyCallContainer -> {
                openEmergencyCall()
            }
            binding.mainScreenFragmentTodayFeelingsContainer -> {
                openFeelingDialog()
            }
            binding.mainScreenFragmentEventBackgroundImage -> {
                presenter.showNextEventListScreen(requireActivity().supportFragmentManager)
            }
            binding.mainScreenFragmentProfileImage -> {
                presenter.openProfileSettingsScreen(requireActivity().supportFragmentManager)
            }
        }
    }

    private fun openEmergencyCall() {
        if (prefs.isEmergencyNumberSet) {
            emergencyCallAlert()
        } else {
            prefs.isNowSetPhoneNumber = true
            presenter.showSettingEmergencyNumberScreen(requireActivity().supportFragmentManager)
        }
    }

    private val callEmergencyContact = { _: DialogInterface, _: Int ->
        executePhoneCall()
    }

    /**
     * Check for Call permission and dispatch call Intent.
     */
    private fun executePhoneCall() {
        if (isCallPermissionGranted()) {
            dispatchPhoneCallIntent()
        } else {
            callPermissionActionResult.launch(Manifest.permission.CALL_PHONE)
        }
    }

    private fun emergencyCallAlert() {
        val builder = AlertDialog.Builder(requireActivity())
        with(builder) {
            setTitle(
                getString(R.string.main_screen_make_call_emergency_contact)
            )
            setMessage(
                getString(
                    R.string.main_screen_make_call_question_placeholder,
                    prefs.emergencyNumberName,
                    prefs.emergencyNumber
                )
            )
            setPositiveButton(
                getString(R.string.common_radio_button_yes),
                DialogInterface.OnClickListener(callEmergencyContact)
            )
            setNeutralButton(getString(R.string.common_button_cancel), null)
            show()
        }
    }

    private fun isCallPermissionGranted(): Boolean {
        return isPermissionGranted(Manifest.permission.CALL_PHONE)
    }

    private fun isLocationPermissionGranted(): Boolean {
        return isPermissionGranted(Manifest.permission.ACCESS_FINE_LOCATION)
    }

    private fun isBluetoothPermissionGranted(): Boolean {
        val bluetoothPermission =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                Manifest.permission.BLUETOOTH_CONNECT
            } else {
                Manifest.permission.BLUETOOTH
            }
        return isPermissionGranted(bluetoothPermission)
    }

    /**
     * Check if a permission has already been granted.
     */
    private fun isPermissionGranted(@NonNull permission: String): Boolean {
        return ContextCompat.checkSelfPermission(
            this.requireContext(),
            permission
        ) == PackageManager.PERMISSION_GRANTED
    }

    private fun dispatchPhoneCallIntent() {
        prefs.isNowSetPhoneNumber = false
        val phoneCallIntent =
            Intent(Intent.ACTION_CALL, Uri.parse("tel:${prefs.emergencyNumber}"))
        if (phoneCallIntent.resolveActivity(requireActivity().packageManager) != null) {
            startActivity(phoneCallIntent)
        } else {
            AlertDialogUtils.showInfoAlertDialog(
                requireContext(), getString(R.string.shared_unexpected_error_title),
                getString(
                    R.string.main_screen_call_action_error_text
                )
            )
        }
    }

    private fun openFeelingDialog() {
        val dialogBuilder: AlertDialog.Builder = AlertDialog.Builder(this.activity)
        val dialogView: View = layoutInflater.inflate(R.layout.fragment_feeling_dialog, null)

        dialogBuilder.setView(dialogView)
        val dialog: AlertDialog = dialogBuilder.show()

        setDialogListeners(dialogView, dialog)
    }

    private fun setDialogListeners(dialogView: View, dialog: AlertDialog) {
        val verySadEmoteImage =
            dialogView.findViewById<ImageView>(R.id.feeling_dialog_fragment_very_sad_image_button)
        val sadEmoteImage =
            dialogView.findViewById<ImageView>(R.id.feeling_dialog_fragment_sad_image_button)
        val neutralEmoteImage =
            dialogView.findViewById<ImageView>(R.id.feeling_dialog_fragment_neutral_image_button)
        val happyEmoteImage =
            dialogView.findViewById<ImageView>(R.id.feeling_dialog_fragment_happy_image_button)
        val veryHappyEmoteImage =
            dialogView.findViewById<ImageView>(R.id.feeling_dialog_fragment_very_happy_image_button)
        val cancelButton =
            dialogView.findViewById<Button>(R.id.feeling_dialog_fragment_cancel_button)
        val okButton = dialogView.findViewById<Button>(R.id.feeling_dialog_fragment_ok_button)

        var itemSelected: String

        okButton.setOnClickListener { dialog.cancel() }
        verySadEmoteImage.setOnClickListener {
            setDefaultEmotes(dialogView)
            verySadEmoteImage.setImageResource(R.drawable.ic_very_sad_blue)
            itemSelected = getString(R.string.main_screen_how_do_you_feel_emotion_very_sad)
            okButton.setOnClickListener {
                showShortToast(itemSelected)
                storeFeelingsPoints(R.string.main_screen_how_do_you_feel_emotion_very_sad)
                dialog.cancel()
            }
        }
        sadEmoteImage.setOnClickListener {
            setDefaultEmotes(dialogView)
            sadEmoteImage.setImageResource(R.drawable.ic_sad_blue)
            itemSelected = getString(R.string.main_screen_how_do_you_feel_emotion_sad)
            okButton.setOnClickListener {
                showShortToast(itemSelected)
                storeFeelingsPoints(R.string.main_screen_how_do_you_feel_emotion_sad)
                dialog.cancel()
            }
        }
        neutralEmoteImage.setOnClickListener {
            setDefaultEmotes(dialogView)
            neutralEmoteImage.setImageResource(R.drawable.ic_neutral_blue)
            itemSelected = getString(R.string.main_screen_how_do_you_feel_emotion_neutral)
            okButton.setOnClickListener {
                showShortToast(itemSelected)
                storeFeelingsPoints(R.string.main_screen_how_do_you_feel_emotion_neutral)
                dialog.cancel()
            }
        }
        happyEmoteImage.setOnClickListener {
            setDefaultEmotes(dialogView)
            happyEmoteImage.setImageResource(R.drawable.ic_happy_blue)
            itemSelected = getString(R.string.main_screen_how_do_you_feel_emotion_happy)
            okButton.setOnClickListener {
                showShortToast(itemSelected)
                storeFeelingsPoints(R.string.main_screen_how_do_you_feel_emotion_happy)
                dialog.cancel()
            }
        }
        veryHappyEmoteImage.setOnClickListener {
            setDefaultEmotes(dialogView)
            veryHappyEmoteImage.setImageResource(R.drawable.ic_very_happy_blue)
            itemSelected = getString(R.string.main_screen_how_do_you_feel_emotion_very_happy)
            okButton.setOnClickListener {
                showShortToast(itemSelected)
                storeFeelingsPoints(R.string.main_screen_how_do_you_feel_emotion_very_happy)
                dialog.cancel()
            }
        }
        cancelButton.setOnClickListener { dialog.cancel() }
    }

    private fun setDefaultEmotes(dialogView: View) {
        dialogView.findViewById<ImageView>(R.id.feeling_dialog_fragment_very_sad_image_button)
            .setImageResource(R.drawable.ic_very_sad)
        dialogView.findViewById<ImageView>(R.id.feeling_dialog_fragment_sad_image_button)
            .setImageResource(R.drawable.ic_sad)
        dialogView.findViewById<ImageView>(R.id.feeling_dialog_fragment_neutral_image_button)
            .setImageResource(R.drawable.ic_neutral)
        dialogView.findViewById<ImageView>(R.id.feeling_dialog_fragment_happy_image_button)
            .setImageResource(R.drawable.ic_happy)
        dialogView.findViewById<ImageView>(R.id.feeling_dialog_fragment_very_happy_image_button)
            .setImageResource(R.drawable.ic_very_happy)
    }

    private fun storeFeelingsPoints(@StringRes itemSelected: Int) {
        val points = when (itemSelected) {
            R.string.main_screen_how_do_you_feel_emotion_very_sad -> 1
            R.string.main_screen_how_do_you_feel_emotion_sad -> 2
            R.string.main_screen_how_do_you_feel_emotion_neutral -> 3
            R.string.main_screen_how_do_you_feel_emotion_happy -> 4
            else -> 5
        }

        feelingsSurveyViewModel.resetState()
        feelingsSurveyViewModel.storeFeelingPoints(points)
    }

    @SuppressLint("MissingPermission")
    private val bleScanner = object : ScanCallback() {
        override fun onScanResult(callbackType: Int, result: ScanResult?) {
            super.onScanResult(callbackType, result)
            // makes a list of nearby devices whose name is not null
            if (result?.device?.name !== null) {
                listDevices.add(result.device.address)
            }
            Log.d(
                "DeviceListActivity",
                "onScanResult: ${result?.device?.address} - ${result?.device?.name}"
            )
        }

        override fun onBatchScanResults(results: MutableList<ScanResult>?) {
            super.onBatchScanResults(results)
            Log.d("DeviceListActivity", "onBatchScanResults:$results")
        }

        override fun onScanFailed(errorCode: Int) {
            super.onScanFailed(errorCode)
            Log.d("DeviceListActivity", "onScanFailed: $errorCode")
        }
    }

    @SuppressLint("MissingPermission")
    private fun scanDevice() {
        val bluetoothManager =
            requireContext().getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        val bluetoothAdapter = bluetoothManager.adapter
        if (bluetoothAdapter == null) {
            Log.d("BT", "The device doesn't support Bluetooth")
        } else {
            if (!bluetoothAdapter.isEnabled) {
                bluetoothEnableActionResult.launch(Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE))
            } else if (!isLocationPermissionGranted()) {
                locationPermissionActionResult.launch(
                    arrayOf(
                        Manifest.permission.ACCESS_FINE_LOCATION,
                    )
                )
            } else if (!isBluetoothPermissionGranted()) {
                val bluetoothPermission =
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                        Manifest.permission.BLUETOOTH_CONNECT
                    } else {
                        Manifest.permission.BLUETOOTH
                    }
                bluetoothScanPermissionActionResult.launch(bluetoothPermission)
            } else if (bluetoothAdapter.isEnabled && isLocationPermissionGranted() && isBluetoothPermissionGranted()) {
                bluetoothLeScanner?.startScan(bleScanner)
            }
        }
    }

    private val bluetoothLeScanner: BluetoothLeScanner?
        get() {
            val bluetoothManager =
                context?.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
            val bluetoothAdapter = bluetoothManager.adapter
            return bluetoothAdapter.bluetoothLeScanner
        }

    override fun updateMainScreen() {
        var name = "${prefs.userFirstName} ${prefs.userLastName}"
        if (name.isBlank())
            name = prefs.userEmail
        binding.mainScreenFragmentProfileName.text = name

        if (prefs.userProfilePicture.isNotEmpty()) {
            Glide.with(requireContext())
                .load(prefs.userProfilePicture)
                .apply(
                    RequestOptions().diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true)
                )
                .centerCrop()
                .into(binding.mainScreenFragmentProfileImage)
        }
    }
}

/**
 * Obtain the Dagger dependency component extension.
 */
@SuppressLint("VisibleForTests")
internal fun MainScreenFragment.getEventsComponent(): EventsComponent =
    (requireContext().applicationContext as VinciApp).getEventsComponent()

/**
 * Obtain the Dagger dependency health record component extension.
 */
@SuppressLint("VisibleForTests")
internal fun MainScreenFragment.getHealthRecordsComponent(): HealthRecordsComponent =
    (requireContext().applicationContext as VinciApp).getHealthRecordsComponent()

/**
 * Obtain the Dagger dependency questionnaire component extension.
 */
@SuppressLint("VisibleForTests")
internal fun MainScreenFragment.getAppComponent(): AppComponent =
    (requireContext().applicationContext as VinciApp).getAppComponent()
