/*
 *  Copyright (c) 2020, Comtrade
 *  All rights reserved.
 *
 *  Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of the <organization> nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *   DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  Package: com.comtrade.vinciapp.view.activity.main.view
 *  File: FitBitActivity.kt
 *  Author: Kosta Eric <kosta.eric@comtrade.com>
 *
 *  Description: Fitbit activity class
 *
 *  History: 9/8/20 Kosta  Initial code
 *
 *
 */

package com.comtrade.vinciapp.view.activity.fitBit.view

import android.content.Intent
import android.util.Log
import android.view.View
import com.comtrade.vinciapp.base.BaseActivity
import com.comtrade.vinciapp.databinding.ActivityFitbitBinding
import com.comtrade.vinciapp.view.activity.fitBit.core.FitBitPresenter
import com.comtrade.vinciapp.view.activity.fitBit.core.contract.FitBitContract
import com.comtrade.vinciapp.view.activity.main.view.MainActivity

class FitBitActivity : BaseActivity(), FitBitContract.View {

    private lateinit var _binding: ActivityFitbitBinding

    private val presenter: FitBitContract.Presenter by lazy {
        FitBitPresenter()
    }

    override fun getContentView(): View {
        _binding = ActivityFitbitBinding.inflate(layoutInflater)
        return _binding.root
    }

    override fun onBackPressed() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }

    override fun onResume() {
        super.onResume()
        Log.w("RESUME", "ON RESUME")
        presenter.openStepHistoryScreen(supportFragmentManager)
    }
}
