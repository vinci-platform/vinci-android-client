/*
 *  Copyright (c) 2020, Comtrade
 *  All rights reserved.
 *
 *  Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of the <organization> nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *   DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  Package: com.comtrade.vinciapp.view.widget.settingEmergencyNumber.view
 *  File: SettingEmergencyNumberFragment.kt
 *  Author: Sofija Andjelkovic <sofija.andjelkovic@comtrade.com>
 *
 *  Description: fragment for choose phone number from contacts or enter phone number
 *
 *  History: 8/13/20 Sofija  Initial code
 *
 *
 */

package com.comtrade.vinciapp.view.widget.settingEmergencyNumber.view


import android.Manifest
import android.app.Activity.RESULT_OK
import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.net.Uri
import android.os.Bundle
import android.provider.ContactsContract
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.vinciapp.R.string
import com.comtrade.vinciapp.VinciApp
import com.comtrade.vinciapp.base.BaseFragment
import com.comtrade.vinciapp.databinding.FragmentSettingEmergencyNumberBinding
import com.comtrade.vinciapp.util.AlertDialogUtils
import com.comtrade.vinciapp.util.SimpleTextWatcher
import com.comtrade.vinciapp.util.ViewUtils
import com.comtrade.vinciapp.view.activity.main.view.MainActivity
import com.comtrade.vinciapp.view.widget.settingEmergencyNumber.core.SettingEmergencyNumberPresenter
import com.comtrade.vinciapp.view.widget.settingEmergencyNumber.core.contract.SettingEmergencyNumberContract
import javax.inject.Inject


class SettingEmergencyNumberFragment : BaseFragment(), SettingEmergencyNumberContract.View,
    View.OnClickListener {

    private var _binding: FragmentSettingEmergencyNumberBinding? = null
    private val binding get() = _binding!!

    private var phoneNumber: String = ""
    private var contactName: String = ""
    private var editTextWatcher = EditTextWatcher()

    @Inject
    lateinit var prefs: IPrefs

    private val presenter: SettingEmergencyNumberContract.Presenter by lazy {
        SettingEmergencyNumberPresenter(
            this
        )
    }

    /**
     * Registering the Contacts pick Intent result.
     */
    private val pickContactActivityResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
            if (result.resultCode == RESULT_OK) {
                result.data?.data?.let {
                    queryEmergencyNumberContent(it)
                }
            }

        }

    private val contactPermissionActivityResult =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) {
            if (!it) {
                AlertDialogUtils.showInfoAlertDialog(
                    this.requireContext(),
                    getString(string.shared_permission_denied_alert_title),
                    getString(string.shared_permission_denied_alert_message)
                )
            } else {
                dispatchPickContactIntent()
            }
        }

    companion object {
        const val TAG: String = "SettingEmergencyNumberFragment"
        const val CONTACT_PERMISSION = Manifest.permission.READ_CONTACTS

        fun newInstance(): SettingEmergencyNumberFragment {
            return SettingEmergencyNumberFragment()
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (context.applicationContext as VinciApp).getAppComponent()
            .inject(this@SettingEmergencyNumberFragment)
    }

    @Suppress("UselessCallOnNotNull")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        addTextWatchers()
        setListeners()
        if (!prefs.emergencyNumber.isNullOrBlank()) {
            binding.settingEmergencyNumberChooseContactButton.text =
                prefs.emergencyNumberName
        }
        if (!prefs.emergencyNumberName.isNullOrBlank()) {
            binding.settingEmergencyNumberPhoneNumber.setText(prefs.emergencyNumber)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSettingEmergencyNumberBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onDestroy() {
        super.onDestroy()
        // Clean-up
        pickContactActivityResult.apply {
            unregister()
        }

        contactPermissionActivityResult.apply {
            unregister()
        }
    }

    private fun addTextWatchers() {
        binding.settingEmergencyNumberPhoneNumber.addTextChangedListener(editTextWatcher)
    }

    private fun setListeners() {
        binding.settingEmergencyNumberCancelButton.setOnClickListener(this)
        binding.settingEmergencyNumberChooseContactButton.setOnClickListener(this)
        binding.settingEmergencyNumberOkButton.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
        when (view) {
            binding.settingEmergencyNumberCancelButton -> {
                openDeviceSetupScreen()
            }
            binding.settingEmergencyNumberChooseContactButton -> {
                chooseContact()
            }
            binding.settingEmergencyNumberOkButton -> {
                checkPhoneNumber()
            }
        }
    }

    private fun checkPhoneNumber() {
        prefs.emergencyNumber =
            binding.settingEmergencyNumberPhoneNumber.text.toString()
        prefs.emergencyNumberName =
            binding.settingEmergencyNumberChooseContactButton.text.toString()
        prefs.isEmergencyNumberSet = true
        presenter.checkPhoneNumber(
            binding.settingEmergencyNumberPhoneNumber.text.toString(),
            prefs.isQuestionnaireStarted,
            prefs.isDeviceAdded
        )
    }

    override fun showPhoneNumberEmptyWarning() {
        binding.settingEmergencyNumberWarningText.text =
            getString(string.setting_emergency_number_phone_number_empty_warning)
        ViewUtils.showView(binding.settingEmergencyNumberWarningText)
    }

    override fun showPhoneNumberNotValidWarning() {
        binding.settingEmergencyNumberWarningText.text =
            getString(string.setting_emergency_number_phone_number_invalid_warning)
        ViewUtils.showView(binding.settingEmergencyNumberWarningText)
    }

    override fun openMainScreen() {
        val intent = Intent(activity, MainActivity::class.java)
        activity?.startActivity(intent)
        activity?.finish()
    }

    override fun openDeviceSetupScreen() {
        requireActivity().supportFragmentManager
            .popBackStackImmediate()
    }

    inner class EditTextWatcher : SimpleTextWatcher() {
        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            super.onTextChanged(s, start, before, count)
            if (!TextUtils.isEmpty(binding.settingEmergencyNumberWarningText.text.toString()) && ViewUtils.isViewVisible(
                    binding.settingEmergencyNumberWarningText
                )
            ) {
                ViewUtils.hideView(binding.settingEmergencyNumberWarningText)
            }
        }
    }


    private fun isContactPermissionGranted(): Boolean {
        val permission = ContextCompat.checkSelfPermission(
            this.requireContext(),
            CONTACT_PERMISSION
        )
        return permission == PackageManager.PERMISSION_GRANTED
    }

    private fun chooseContact() {
        if (isContactPermissionGranted()) {
            dispatchPickContactIntent()
        } else {
            contactPermissionActivityResult.launch(CONTACT_PERMISSION)
        }
    }

    private fun dispatchPickContactIntent() {
        val contactsIntent =
            Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI)
        if (contactsIntent.resolveActivity(requireActivity().packageManager) != null) {
            pickContactActivityResult.launch(contactsIntent)
        } else {
            AlertDialogUtils.showInfoAlertDialog(
                requireContext(), getString(string.shared_unexpected_error_title), getString(
                    string.setting_emergency_number_phone_number_error_message
                )
            )
        }
    }

    /**
     * Query the Cursor provider for the existence of the contact number and name.
     * Sets the number and display name to the matching view components.
     * @param uri The picked contact data Uri.
     */
    private fun queryEmergencyNumberContent(uri: Uri) {
        val resolver: ContentResolver = requireActivity().contentResolver
        val cursor: Cursor? =
            uri.let { resolver.query(it, null, null, null, null, null) }
        if (cursor != null) {
            cursor.moveToFirst()
            phoneNumber =
                cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.NUMBER))
            contactName =
                cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME))
            binding.settingEmergencyNumberPhoneNumber.setText(phoneNumber)
            binding.settingEmergencyNumberChooseContactButton.text = contactName
            cursor.close()
        }
    }
}