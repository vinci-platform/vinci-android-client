/*
 *  Copyright (c) 2020, Comtrade
 *  All rights reserved.
 *
 *  Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of the <organization> nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *   DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  Package: com.comtrade.vinciapp.view.widget.chooseContacts.core
 *  File: ChooseContactsPresenter.kt
 *  Author: Milenko Bojanic <milenko.bojanic@comtrade.com>
 *
 *  Description: choose contacts presenter
 *
 *  History: 8/11/20 Milenko  Initial code
 *
 *
 */

package com.comtrade.vinciapp.view.widget.chooseFriends.core

import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.entities.user.Contact
import com.comtrade.domain.entities.user.data.GetAllUserPhonesEntity
import com.comtrade.vinciapp.base.BasePresenter
import com.comtrade.vinciapp.view.widget.chooseFriends.core.contract.ChooseFriendsContract

class ChooseFriendsPresenter(
    private val view: ChooseFriendsContract.View,
) : BasePresenter(),
    ChooseFriendsContract.Presenter {

    //Temporary solution that will be replaced with a full fledged feature
    override fun filterUsers(
        prefs: IPrefs,
        contactList: List<Contact>,
        allContacts: List<GetAllUserPhonesEntity>
    ) {

        //Existing contacts
        val resultList: MutableList<Contact> = mutableListOf()
        resultList.addAll(prefs.userFriends)

        //Search though the local list of contacts.
        for (contact in contactList) {
            if (!contact.number.isNullOrBlank()) {
                var selected = false
                for (user in resultList) {
                    if (contact.number == user.number) {
                        user.isSelected = true
                        selected = true
                        break
                    }
                }
                if (!selected) {
                    for (user in allContacts) {
                        if (user.phone == contact.number) {
                            val filteredList =
                                resultList.filter { item -> item.number == contact.number }
                            if (filteredList.isEmpty()) {
                                resultList.add(contact)
                            }
                        }
                    }
                }
            }
        }

        view.onContactsFilterResult(resultList)
    }
}
