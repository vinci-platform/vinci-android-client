/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: AboutDependenciesAdapter.kt
 * Author: sdelic
 *
 * History:  1.4.22. 09:47 created
 */

package com.comtrade.vinciapp.view.widget.aboutDependencies

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.comtrade.domain.entities.legal.DependencyInfo
import com.comtrade.vinciapp.R

/**
 * The About dependencies feature RecyclerView adapter component.
 * Provides interface for item click handling.
 */
internal class AboutDependenciesAdapter(private val dependencyItemOnClickListener: DependencyItemOnClickListener) :
    RecyclerView.Adapter<AboutDependenciesAdapter.ViewHolder>() {

    private val dataSet = mutableListOf<DependencyInfo>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.about_dependencies_list_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val dependencyInfo = dataSet[position]
        holder.dependencyName.text = dependencyInfo.name

        if (dependencyInfo.description.isNotBlank()) {
            holder.dependencyDescription.visibility = View.VISIBLE
            holder.dependencyDescription.text = dependencyInfo.description
        } else {
            holder.dependencyDescription.visibility = View.GONE
        }
        if (dependencyInfo.license.isEmpty()) {
            holder.dependencyLicense.text = ""
        } else {
            holder.dependencyLicense.text = dependencyInfo.license[0].name
        }
        holder.dependencyUrl.text = dependencyInfo.url
    }

    override fun getItemCount(): Int = dataSet.size

    internal fun updateDataSet(newData: Array<DependencyInfo>) {
        if (newData.isNotEmpty()) {
            dataSet.clear()
            dataSet.addAll(newData)
            notifyItemRangeInserted(0, dataSet.size)
        }
    }

    inner class ViewHolder(
        view: View
    ) :
        RecyclerView.ViewHolder(view), View.OnClickListener {

        val dependencyName: TextView
        val dependencyDescription: TextView
        val dependencyLicense: TextView
        val dependencyUrl: TextView

        init {
            view.setOnClickListener(this)
            dependencyName = view.findViewById(R.id.dependency_name)
            dependencyDescription = view.findViewById(R.id.dependency_description)
            dependencyLicense = view.findViewById(R.id.dependency_license)
            dependencyUrl = view.findViewById(R.id.dependency_url)
        }

        override fun onClick(p0: View?) {
            val item = dataSet[bindingAdapterPosition]
            dependencyItemOnClickListener.onItemClick(item.url)
        }
    }

    /**
     * Callback definition for the action to be invoked when an item has been clicked on.
     */
    interface DependencyItemOnClickListener {
        fun onItemClick(dependencyUrl: String?)
    }
}
