/*
 *  Copyright (c) 2020, Comtrade
 *  All rights reserved.
 *
 *  Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of the <organization> nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *   DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  Package: com.comtrade.vinciapp.view.widget.devicesSetup.core
 *  File: DevicesSetupPresenter.kt
 *  Author: Milenko Bojanic <milenko.bojanic@comtrade.com>
 *
 *  Description: device setup presenter
 *
 *  History: 8/11/20 Milenko  Initial code
 *
 *
 */

package com.comtrade.vinciapp.view.widget.devicesSetup.core

import androidx.fragment.app.FragmentManager
import com.comtrade.vinciapp.base.BasePresenter
import com.comtrade.vinciapp.view.widget.addNewDevice.view.AddNewDeviceFragment
import com.comtrade.vinciapp.view.widget.devicesSetup.core.contract.DevicesSetupContract
import com.comtrade.vinciapp.view.widget.profilePicture.view.ProfilePictureFragment
import com.comtrade.vinciapp.view.widget.questionnaireMenu.view.QuestionnaireMenuFragment
import com.comtrade.vinciapp.view.widget.settingEmergencyNumber.view.SettingEmergencyNumberFragment

class DevicesSetupPresenter : BasePresenter(), DevicesSetupContract.Presenter {

    override fun showProfilePictureScreen(fragmentManager: FragmentManager) {
        changeMainActivityFragment(
            fragmentManager,
            ProfilePictureFragment.newInstance(),
            ProfilePictureFragment.TAG
        )
    }

    override fun showAddDeviceScreen(fragmentManager: FragmentManager) {
        changeMainActivityFragment(
            fragmentManager,
            AddNewDeviceFragment.newInstance(),
            AddNewDeviceFragment.TAG
        )
    }

    override fun showQuestionnaireScreen(fragmentManager: FragmentManager) {
        changeMainActivityFragment(
            fragmentManager,
            QuestionnaireMenuFragment.newInstance(),
            QuestionnaireMenuFragment.TAG
        )
    }

    override fun showSettingEmergencyNumberScreen(fragmentManager: FragmentManager) {
        changeMainActivityFragment(
            fragmentManager,
            SettingEmergencyNumberFragment.newInstance(),
            SettingEmergencyNumberFragment.TAG
        )
    }
}
