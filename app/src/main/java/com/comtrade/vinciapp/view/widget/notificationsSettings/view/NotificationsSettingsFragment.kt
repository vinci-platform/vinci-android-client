/*
 *  Copyright (c) 2020, Comtrade
 *  All rights reserved.
 *
 *  Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of the <organization> nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *   DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  Package: com.comtrade.vinciapp.view.widget.notificationsSettings.view
 *  File: NotificationsSettingsFragment.kt
 *  Author: Sofija Andjelkovic <sofija.andjelkovic@comtrade.com>
 *
 *  Description: screen for notifications settings
 *
 *  History: 9/30/20 Sofija  Initial code
 *
 *
 */

package com.comtrade.vinciapp.view.widget.notificationsSettings.view

import android.annotation.SuppressLint
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.VinciApp
import com.comtrade.vinciapp.base.BaseFragment
import com.comtrade.vinciapp.databinding.FragmentNotificationsSettingsBinding
import com.comtrade.vinciapp.view.widget.notificationsSettings.core.contract.NotificationsSettingsContract
import javax.inject.Inject

class NotificationsSettingsFragment :
    BaseFragment(),
    NotificationsSettingsContract.View,
    View.OnClickListener {

    private var _binding: FragmentNotificationsSettingsBinding? = null
    private val binding get() = _binding!!

    @Inject
    lateinit var prefs: IPrefs

    companion object {
        const val TAG: String = "NotificationsSettingsFragment"

        fun newInstance(): NotificationsSettingsFragment {
            return NotificationsSettingsFragment()
        }
    }

    @SuppressLint("VisibleForTests")
    override fun onAttach(context: Context) {
        super.onAttach(context)
        (context.applicationContext as VinciApp).getAppComponent()
            .inject(this@NotificationsSettingsFragment)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentNotificationsSettingsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.notificationsSettingsScreenConnectionStatus.text =
            prefs.connectionStatus
        setListeners()
        setLoggingFromState(prefs.isLogEnabled)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun setListeners() {
        binding.notificationsSettingsCancelButton.setOnClickListener(this)
        binding.notificationsSettingsSaveButton.setOnClickListener(this)
        binding.notificationsSettingsCopyLogButton.setOnClickListener(this)
        binding.notificationsSettingsScreenConnectionStatus.setOnClickListener(this)
        binding.notificationsSettingsOutdoorActivitySwitch.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                binding.notificationsSettingsOutdoorActivitySwitch.text =
                    getString(R.string.notifications_settings_screen_outdoor_activity_switch_enabled_text)
            } else {
                binding.notificationsSettingsOutdoorActivitySwitch.text =
                    getString(R.string.notifications_settings_screen_outdoor_activity_switch_disabled_text)
            }
        }
        binding.notificationsSettingsQualityOfLifeSwitch.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                binding.notificationsSettingsQualityOfLifeSwitch.text =
                    getString(R.string.notifications_settings_screen_outdoor_activity_switch_enabled_text)
                binding.notificationsSettingsRadioGroup.visibility = View.VISIBLE
                binding.notificationsSettingsRadioGroupText.visibility = View.VISIBLE
            } else {
                binding.notificationsSettingsQualityOfLifeSwitch.text =
                    getString(R.string.notifications_settings_screen_outdoor_activity_switch_disabled_text)
                binding.notificationsSettingsRadioGroup.visibility = View.GONE
                binding.notificationsSettingsRadioGroupText.visibility = View.GONE
            }
        }
        binding.notificationsSettingsLoggingSwitch.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                binding.notificationsSettingsLoggingSwitch.text =
                    getString(R.string.notifications_settings_screen_logging_switch_enabled_text)
            } else {
                binding.notificationsSettingsLoggingSwitch.text =
                    getString(R.string.notifications_settings_screen_logging_switch_disabled_text)
            }
        }
    }

    private fun setLoggingFromState(isLoggingEnabled: Boolean) {
        binding.notificationsSettingsLoggingSwitch.isChecked = isLoggingEnabled
    }

    override fun onClick(view: View?) {
        when (view) {
            binding.notificationsSettingsCancelButton -> {
                openMainScreen()
            }
            binding.notificationsSettingsSaveButton -> {
                prefs.isLogEnabled =
                    binding.notificationsSettingsLoggingSwitch.isChecked
                openMainScreen()
            }
            binding.notificationsSettingsCopyLogButton -> {
                val clipboard =
                    requireActivity().getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
                val clip = ClipData.newPlainText(
                    getString(R.string.notifications_settings_copied_text),
                    prefs.logFile
                )
                clipboard.setPrimaryClip(clip)
                showShortToast(getString(R.string.notifications_settings_copied_text_to_clipboard))
            }
            binding.notificationsSettingsScreenConnectionStatus -> {
                showShortToast(prefs.connectionError)
            }
        }
    }

    override fun openMainScreen() {
        requireActivity().supportFragmentManager
            .popBackStackImmediate()
    }
}
