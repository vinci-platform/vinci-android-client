/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: AboutDependenciesFragment.kt
 * Author: sdelic
 *
 * History:  31.3.22. 15:59 created
 */

package com.comtrade.vinciapp.view.widget.aboutDependencies

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import com.comtrade.feature_about_dependencies.di.AboutDependenciesComponent
import com.comtrade.feature_about_dependencies.ui.AboutDependenciesUIState
import com.comtrade.feature_about_dependencies.ui.AboutDependenciesViewModel
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.VinciApp
import com.comtrade.vinciapp.base.BaseFragment
import com.comtrade.vinciapp.databinding.ElevatedProgressViewBinding
import com.comtrade.vinciapp.databinding.FragmentAboutDependenciesBinding

/**
 * The dependency information and licensing view component.
 */
internal class AboutDependenciesFragment :
    BaseFragment(),
    AboutDependenciesAdapter.DependencyItemOnClickListener {

    private var _binding: FragmentAboutDependenciesBinding? = null
    private val binding get() = _binding!!

    private var _bindingElevatedView: ElevatedProgressViewBinding? = null
    private val bindingElevatedView get() = _bindingElevatedView!!

    private val dependenciesAdapter: AboutDependenciesAdapter by lazy {
        AboutDependenciesAdapter(this@AboutDependenciesFragment)
    }

    private val viewModel: AboutDependenciesViewModel by viewModels {
        getAppComponent().provideViewModelFactory()
    }

    companion object {
        const val TAG: String = "AboutDependenciesFragment"

        fun newInstance(): AboutDependenciesFragment {
            return AboutDependenciesFragment()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentAboutDependenciesBinding.inflate(inflater, container, false)
        _bindingElevatedView = ElevatedProgressViewBinding.bind(binding.root)
        binding.dependenciesList.apply {
            adapter = dependenciesAdapter
            setHasFixedSize(true)
        }
        viewModel.dependencyInformation.observe(viewLifecycleOwner) {
            handleDataState(it)
        }
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
        _bindingElevatedView = null
    }

    /**
     * Handler for the possible UI states.
     */
    private fun handleDataState(aboutDependenciesUIState: AboutDependenciesUIState) {
        when (aboutDependenciesUIState) {
            AboutDependenciesUIState.OnEmptyDataSet -> handleEmptyDataState()

            is AboutDependenciesUIState.OnError -> handleErrorState(aboutDependenciesUIState)

            AboutDependenciesUIState.OnLoading -> handleLoadingState()

            is AboutDependenciesUIState.OnSuccess -> handleSuccessState(aboutDependenciesUIState)
        }
    }

    private fun handleSuccessState(aboutDependenciesUIState: AboutDependenciesUIState.OnSuccess) {
        bindingElevatedView.containerProgress.visibility = View.GONE
        binding.dependenciesEmpty.visibility = View.GONE
        binding.dependenciesList.visibility = View.VISIBLE
        dependenciesAdapter.updateDataSet(
            aboutDependenciesUIState.data
        )
    }

    private fun handleErrorState(aboutDependenciesUIState: AboutDependenciesUIState.OnError) {
        bindingElevatedView.containerProgress.visibility = View.GONE
        binding.dependenciesList.visibility = View.GONE
        binding.dependenciesEmpty.visibility = View.VISIBLE
        if (aboutDependenciesUIState.message != null) {
            aboutDependenciesUIState.message?.let {
                showShortToast(it)
            }
        } else {
            showShortToast(getString(R.string.shared_unexpected_error_title))
        }
    }

    private fun handleEmptyDataState() {
        bindingElevatedView.containerProgress.visibility = View.GONE
        binding.dependenciesList.visibility = View.GONE
        binding.dependenciesEmpty.visibility = View.VISIBLE
    }

    private fun handleLoadingState() {
        binding.dependenciesList.visibility = View.GONE
        binding.dependenciesEmpty.visibility = View.GONE
        bindingElevatedView.containerProgress.visibility = View.VISIBLE
    }

    override fun onItemClick(dependencyUrl: String?) {
        dependencyUrl?.let {
            if (it.isNotBlank()) {
                val defaultBrowser =
                    Intent(Intent.ACTION_VIEW, Uri.parse(it))
                startActivity(defaultBrowser)
            }
        }
    }
}

/**
 * Obtain the Dagger dependency component extension.
 */
internal fun AboutDependenciesFragment.getAppComponent(): AboutDependenciesComponent =
    (requireContext().applicationContext as VinciApp).getAboutDependenciesComponent()
