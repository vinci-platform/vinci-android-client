/*
 *  Copyright (c) 2020, Comtrade
 *  All rights reserved.
 *
 *  Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of the <organization> nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *   DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  Package: com.comtrade.vinciapp.view.widget.questionnaireIPA.view.questionViews
 *  File: QuestionnaireIPARadioButtonQuestions.kt
 *  Author: Kosta Eric <kosta.eric@comtrade.com>
 *
 *  Description: View for IPA questionnaire radio button screens
 *
 *  History: 8/11/20 Kosta  Initial code
 *
 *
 */

package com.comtrade.vinciapp.view.widget.historyGraph.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.base.BaseFragment
import com.comtrade.vinciapp.databinding.ViewHistoryGraphScreenBinding
import com.github.aachartmodel.aainfographics.aachartcreator.AAChartModel
import com.github.aachartmodel.aainfographics.aachartcreator.AAChartType
import com.github.aachartmodel.aainfographics.aachartcreator.AAChartView
import com.github.aachartmodel.aainfographics.aachartcreator.AASeriesElement
import com.github.aachartmodel.aainfographics.aatools.AAGradientColor
import com.github.aachartmodel.aainfographics.aatools.AALinearGradientDirection
import java.io.Serializable

class HistoryGraphScreen : BaseFragment() {
    private var _binding: ViewHistoryGraphScreenBinding? = null
    private val binding get() = _binding!!

    private lateinit var xAxisLabels: Array<String>
    private lateinit var data: HashMap<String, Array<Int>>
    private var title: String = ""
    private var firstTitle: String = ""
    private var firstValue: String = ""
    private var secondTitle: String = ""
    private var secondValue: String = ""

    companion object {
        const val TAG: String = "HistoryGraphScreen"

        const val LABELS_KEY: String = "labels"
        const val DATA_KEY: String = "data"
        const val TITLE_KEY: String = "title"
        const val FIRST_TITLE_KEY: String = "first_title"
        const val FIRST_VALUE_KEY: String = "first_value"
        const val SECOND_TITLE_KEY: String = "second_title"
        const val SECOND_VALUE_KEY: String = "second_value"

        fun newInstance(
            title: String,
            firstTitle: String,
            firstValue: String,
            secondTitle: String,
            secondValue: String,
            xAxisLabels: Array<String>,
            data: HashMap<String, Array<Int>>?
        ): HistoryGraphScreen {
            val fragment = HistoryGraphScreen()
            val args = Bundle()
            args.putStringArray(LABELS_KEY, xAxisLabels)
            args.putSerializable(DATA_KEY, data as Serializable)
            args.putString(TITLE_KEY, title)
            args.putString(FIRST_TITLE_KEY, firstTitle)
            args.putString(FIRST_VALUE_KEY, firstValue)
            args.putString(SECOND_TITLE_KEY, secondTitle)
            args.putString(SECOND_VALUE_KEY, secondValue)

            fragment.arguments = args
            return fragment
        }
    }

    @Suppress("UNCHECKED_CAST")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = ViewHistoryGraphScreenBinding.inflate(inflater, container, false)
        xAxisLabels = arguments?.getStringArray(LABELS_KEY) as Array<String>
        data = arguments?.getSerializable(DATA_KEY) as HashMap<String, Array<Int>>

        title = arguments?.getString(TITLE_KEY) as String
        firstTitle = arguments?.getString(FIRST_TITLE_KEY) as String
        firstValue = arguments?.getString(FIRST_VALUE_KEY) as String
        secondTitle = arguments?.getString(SECOND_TITLE_KEY) as String
        secondValue = arguments?.getString(SECOND_VALUE_KEY) as String

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.historyGraphFirstTitle.text = firstTitle
        binding.historyGraphFirstValue.text = firstValue
        binding.historyGraphSecondTitle.text = secondTitle
        binding.historyGraphSecondValue.text = secondValue

        val graphColors = listOf(
            AAGradientColor.PurpleLake,
            AAGradientColor.linearGradient(
                AALinearGradientDirection.ToTop,
                "#004597",
                "#60A9FF"
            ),
            AAGradientColor.NeonGlow
        )

        drawChart(data, title, xAxisLabels, graphColors)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    @Suppress("UNCHECKED_CAST")
    private fun drawChart(
        data: HashMap<String, Array<Int>>,
        title: String,
        xAxisLabels: Array<String>,
        graphColors: List<Map<String, Any>>
    ) {

        val aaChartView: AAChartView = binding.AAChartView
        val dataArray: MutableList<AASeriesElement> = mutableListOf()
        val colorIterator = graphColors.listIterator()
        for (dataset in data) {
            dataArray.add(
                AASeriesElement()
                    .name(dataset.key)
                    .data(dataset.value as Array<Any>)
                    .color(colorIterator.next())

            )
        }
        val aaChartModel: AAChartModel = AAChartModel()
            .chartType(AAChartType.Column)
            .title(title)
            .backgroundColor(R.color.colorWhite)
            .gradientColorEnable(true)
            .xAxisLabelsEnabled(true)
            .categories(xAxisLabels)
            .yAxisTitle("")
            .series(dataArray.toTypedArray())
        aaChartView.aa_drawChartWithChartModel(aaChartModel)
    }
}
