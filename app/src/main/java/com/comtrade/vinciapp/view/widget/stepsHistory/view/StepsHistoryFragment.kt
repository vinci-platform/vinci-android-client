/*
 *  Copyright (c) 2020, Comtrade
 *  All rights reserved.
 *
 *  Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of the <organization> nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *   DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  Package: com.comtrade.vinciapp.view.widget.addNewDevice.view
 *  File: AddNewDeviceFragment.kt
 *  Author: Sofija Andjelkovic <sofija.andjelkovic@comtrade.com>
 *
 *  Description: a two-button fragment for different types of adding a new device
 *
 *  History: 8/11/20 Sofija  Initial code
 *
 *
 */

package com.comtrade.vinciapp.view.widget.stepsHistory.view

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.viewModels
import com.comtrade.domain.entities.graph.GraphData
import com.comtrade.domain.entities.step.FitbitDataType
import com.comtrade.domain_ui.state.RequestUIState
import com.comtrade.feature_devices.ui.fitbit.FitBitPeriodDataViewModel
import com.comtrade.feature_steps.ui.GetStepsForPreviousDaysViewModel
import com.comtrade.feature_steps.ui.cmd.CMDStepDataForDaysViewModel
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.VinciApp
import com.comtrade.vinciapp.view.adapter.QuestionnaireViewPagerAdapter
import com.comtrade.vinciapp.view.widget.historyGraph.view.HistoryGraphFragment
import com.comtrade.vinciapp.view.widget.historyGraph.view.HistoryGraphScreen
import com.comtrade.vinciapp.view.widget.stepsHistory.core.contract.StepsHistoryContract
import java.lang.Integer.max
import java.text.DecimalFormat
import kotlin.math.roundToInt

@SuppressLint("VisibleForTests")
class StepsHistoryFragment : HistoryGraphFragment(), StepsHistoryContract.View {

    private lateinit var fitBitStepHistoryLastWeek: Array<GraphData>
    private lateinit var fitBitStepHistoryLastMonth: Array<GraphData>
    private lateinit var fitBitStepHistoryLastHalfYear: Array<GraphData>
    private lateinit var fitBitStepHistoryLastYear: Array<GraphData>

    private lateinit var cmdStepHistoryLastWeek: Array<GraphData>
    private lateinit var cmdStepHistoryLastMonth: Array<GraphData>
    private lateinit var cmdStepHistoryLastHalfYear: Array<GraphData>
    private lateinit var cmdStepHistoryLastYear: Array<GraphData>

    private val getStepsForDaysViewModel: GetStepsForPreviousDaysViewModel by viewModels {
        (requireContext().applicationContext as VinciApp).getStepsComponent()
            .provideStepsForDaysViewModelFactory()
    }

    private val getCMDStepsForDaysViewModel: CMDStepDataForDaysViewModel by viewModels {
        (requireContext().applicationContext as VinciApp).getStepsComponent()
            .provideCMDStepDataForDaysViewModelFactory()
    }

    private val fitBitPeriodDataViewModel: FitBitPeriodDataViewModel by viewModels {
        (requireContext().applicationContext as VinciApp).getDevicesComponent()
            .provideFitBitPeriodDataViewModelFactory()
    }

    companion object {
        const val TAG: String = "StepsHistoryFragment"

        const val FITBIT_KEY = "FitBit"
        const val CMD_KEY = "CMD"


        fun newInstance(): StepsHistoryFragment {
            return StepsHistoryFragment()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeCMDShoeStepData()
        observeFitBitStepData()
    }

    /**
     * Observe the UI states for obtaining the CMD device step data.
     */
    private fun observeCMDShoeStepData() {
        getCMDStepsForDaysViewModel.getCMDDayStepsObserver.observe(viewLifecycleOwner) {
            when (it) {
                RequestUIState.DefaultState -> {}
                is RequestUIState.OnError -> {
                    it.message?.let { _ ->
                        showShortToast(
                            getString(R.string.step_history_screen_shoe_step_error)
                        )
                    }
                    getStepsForDaysViewModel.resetState()
                }
                RequestUIState.OnLoading -> {}
                is RequestUIState.OnSuccess -> {
                    onCMDDataObtained(it.data)
                    getCMDStepsForDaysViewModel.resetState()
                }
            }
        }
    }

    /**
     * Observe the UI states for obtaining the FitBit device step data.
     */
    private fun observeFitBitStepData() {
        fitBitPeriodDataViewModel.fitBitPeriodDataObserver.observe(viewLifecycleOwner) {
            when (it) {
                RequestUIState.DefaultState -> {}
                is RequestUIState.OnError -> {
                    it.message?.let { _ ->
                        showShortToast(
                            getString(R.string.step_history_screen_fitbit_error)
                        )
                    }
                    fitBitPeriodDataViewModel.resetState()
                }
                RequestUIState.OnLoading -> {}
                is RequestUIState.OnSuccess -> {
                    onFitBitDataObtained(it.data)
                    fitBitPeriodDataViewModel.resetState()
                }
            }
        }
    }

    /**
     * Handle the obtained CMD device step data parsing and adapter attachment.
     */
    private fun onCMDDataObtained(data: Map<Int, Array<GraphData>>) {
        cmdStepHistoryLastWeek = data[7]!!
        cmdStepHistoryLastMonth = data[30]!!
        cmdStepHistoryLastHalfYear = data[190]!!
        cmdStepHistoryLastYear = data[365]!!

        Log.w("CMD LAST WEEK", cmdStepHistoryLastWeek.size.toString())
        Log.w("CMD LAST MONTH", cmdStepHistoryLastMonth.size.toString())
        Log.w("CMD LAST HALF YEAR", cmdStepHistoryLastHalfYear.size.toString())
        Log.w("CMD LAST YEAR", cmdStepHistoryLastYear.size.toString())
        setAdapter()
    }

    private fun onFitBitDataObtained(data: Map<FitbitDataType, Map<Int, Array<GraphData>>>) {

        for (activityData in data) {
            when (activityData.key) {
                FitbitDataType.STEPS -> populateFitBitStepData(activityData.value)
                else -> {}
            }
        }
        setAdapter()
    }

    private fun populateFitBitStepData(value: Map<Int, Array<GraphData>>) {
        fitBitStepHistoryLastWeek =
            value[7]!!
        fitBitStepHistoryLastMonth =
            value[30]!!
        fitBitStepHistoryLastHalfYear =
            value[183]!!
        fitBitStepHistoryLastYear =
            value[365]!!

        Log.w("FITBIT LAST WEEK", fitBitStepHistoryLastWeek.size.toString())
        Log.w("FITBIT LAST MONTH", fitBitStepHistoryLastMonth.size.toString())
        Log.w("FITBIT LAST HALF YEAR", fitBitStepHistoryLastHalfYear.size.toString())
        Log.w("FITBIT LAST YEAR", fitBitStepHistoryLastYear.size.toString())
    }


    // TODO: Needs refactoring
    private fun setAdapter() {
        fragmentsPagerAdapter =
            QuestionnaireViewPagerAdapter(childFragmentManager, viewLifecycleOwner.lifecycle)
        val isFitBitDevice = fitBitStepHistoryLastWeek.isNotEmpty()
        val isCmdDevice = cmdStepHistoryLastWeek.isNotEmpty()

        if (!isFitBitDevice && !isCmdDevice) {
            return
        }

        val weekData: HashMap<String, Array<Int>> = hashMapOf()
        val monthData: HashMap<String, Array<Int>> = hashMapOf()
        val halfYearData: HashMap<String, Array<Int>> = hashMapOf()
        val yearData: HashMap<String, Array<Int>> = hashMapOf()

        var weekAverage = 0
        var weekTotal = 0
        var monthAverage = 0
        var monthTotal = 0
        var halfYearAverage = 0
        var halfYearTotal = 0
        var yearAverage = 0
        var yearTotal = 0

        var labelsLastWeek: Array<String> = emptyArray()
        var labelsLastMonth: Array<String> = emptyArray()
        var labelsLastHalfYear: Array<String> = emptyArray()
        var labelsLastYear: Array<String> = emptyArray()


        if (fitBitStepHistoryLastWeek.isNotEmpty()) {
            weekData[FITBIT_KEY] = getDataForLastWeek(fitBitStepHistoryLastWeek)
            weekAverage = weekData[FITBIT_KEY]!!.asIterable().average().roundToInt()
            weekTotal = weekData[FITBIT_KEY]!!.asIterable().sum()

            monthData[FITBIT_KEY] = getDataForLastMonth(fitBitStepHistoryLastMonth)
            monthAverage = monthData[FITBIT_KEY]!!.asIterable().average().roundToInt()
            monthTotal = monthData[FITBIT_KEY]!!.asIterable().sum()

            halfYearData[FITBIT_KEY] = getDataForLastMonths(fitBitStepHistoryLastHalfYear)
            halfYearAverage = halfYearData[FITBIT_KEY]!!.asIterable().average().roundToInt()
            halfYearTotal = halfYearData[FITBIT_KEY]!!.asIterable().sum()

            yearData[FITBIT_KEY] = getDataForLastMonths(fitBitStepHistoryLastYear)
            yearAverage = yearData[FITBIT_KEY]!!.asIterable().average().roundToInt()
            yearTotal = yearData[FITBIT_KEY]!!.asIterable().sum()

            labelsLastWeek = getDataLabelsForLastWeek(fitBitStepHistoryLastWeek)
            labelsLastMonth = getDataLabelsForLastMonth(fitBitStepHistoryLastMonth)
            labelsLastHalfYear = getDataLabelsForLastMonths(fitBitStepHistoryLastHalfYear)
            labelsLastYear = getDataLabelsForLastMonths(fitBitStepHistoryLastYear)
        }

        if (cmdStepHistoryLastWeek.isNotEmpty()) {
            weekData[CMD_KEY] = getDataForLastWeek(cmdStepHistoryLastWeek)
            weekAverage = max(weekAverage, weekData[CMD_KEY]!!.asIterable().average().roundToInt())
            weekTotal = max(weekTotal, weekData[CMD_KEY]!!.asIterable().sum())

            monthData[CMD_KEY] = getDataForLastMonth(cmdStepHistoryLastMonth)
            monthAverage =
                max(monthAverage, monthData[CMD_KEY]!!.asIterable().average().roundToInt())
            monthTotal = max(monthTotal, monthData[CMD_KEY]!!.asIterable().sum())

            halfYearData[CMD_KEY] = getDataForLastMonths(cmdStepHistoryLastHalfYear)
            halfYearAverage =
                max(halfYearAverage, halfYearData[CMD_KEY]!!.asIterable().average().roundToInt())
            halfYearTotal = max(halfYearTotal, halfYearData[CMD_KEY]!!.asIterable().sum())

            yearData[CMD_KEY] = getDataForLastMonths(cmdStepHistoryLastYear)
            yearAverage = max(yearAverage, yearData[CMD_KEY]!!.asIterable().average().roundToInt())
            yearTotal = max(yearTotal, yearData[CMD_KEY]!!.asIterable().sum())

            labelsLastWeek = getDataLabelsForLastWeek(cmdStepHistoryLastWeek)
            labelsLastMonth = getDataLabelsForLastMonth(cmdStepHistoryLastMonth)
            labelsLastHalfYear = getDataLabelsForLastMonths(cmdStepHistoryLastHalfYear)
            labelsLastYear = getDataLabelsForLastMonths(cmdStepHistoryLastYear)
        }


        fragmentsPagerAdapter.addFragment(
            HistoryGraphScreen.newInstance(
                getString(R.string.step_history_screen),
                getString(R.string.history_graph_screen_average),
                DecimalFormat("#,###").format(weekAverage).toString(),
                getString(R.string.history_graph_screen_total),
                DecimalFormat("#,###").format(weekTotal).toString(),
                labelsLastWeek,
                weekData
            )
        )
        fragmentsPagerAdapter.addFragment(
            HistoryGraphScreen.newInstance(
                getString(R.string.step_history_screen),
                getString(R.string.history_graph_screen_average),
                DecimalFormat("#,###").format(monthAverage).toString(),
                getString(R.string.history_graph_screen_total),
                DecimalFormat("#,###").format(monthTotal).toString(),
                labelsLastMonth,
                monthData
            )
        )
        fragmentsPagerAdapter.addFragment(
            HistoryGraphScreen.newInstance(
                getString(R.string.step_history_screen),
                getString(R.string.history_graph_screen_average),
                DecimalFormat("#,###").format(halfYearAverage).toString(),
                getString(R.string.history_graph_screen_total),
                DecimalFormat("#,###").format(halfYearTotal).toString(),
                labelsLastHalfYear,
                halfYearData
            )
        )
        fragmentsPagerAdapter.addFragment(
            HistoryGraphScreen.newInstance(
                getString(R.string.step_history_screen),
                getString(R.string.history_graph_screen_average),
                DecimalFormat("#,###").format(yearAverage).toString(),
                getString(R.string.history_graph_screen_total),
                DecimalFormat("#,###").format(yearTotal).toString(),
                labelsLastYear,
                yearData
            )
        )

        viewPager.adapter = fragmentsPagerAdapter
        setTab()
    }

    override fun getData() {
        //Execute DB calls for each of the supported devices.

        getFitBitDeviceStepData()

        getCMDDeviceStepData()
    }

    private fun getFitBitDeviceStepData() {
        fitBitStepHistoryLastWeek = arrayOf()
        fitBitStepHistoryLastMonth = arrayOf()
        fitBitStepHistoryLastHalfYear = arrayOf()
        fitBitStepHistoryLastYear = arrayOf()

        if (prefs.fitBitDevice != null && prefs.fitBitDevice!!.active) {

            fitBitPeriodDataViewModel.getFitBitDataForTypes(
                intArrayOf(7, 30, 183, 365),
                arrayOf(FitbitDataType.STEPS)
            )
        }
    }

    /**
     * Request CDM data if the device is active if not, set empty states and initialize the adapter component.
     */
    private fun getCMDDeviceStepData() {
        cmdStepHistoryLastWeek = arrayOf()
        cmdStepHistoryLastMonth = arrayOf()
        cmdStepHistoryLastHalfYear = arrayOf()
        cmdStepHistoryLastYear = arrayOf()

        if (prefs.cmdDevice != null && prefs.cmdDevice!!.active) {
            getCMDStepsForDaysViewModel.getCmdDataForLastDays(intArrayOf(7, 30, 190, 365))
        }
    }
}


