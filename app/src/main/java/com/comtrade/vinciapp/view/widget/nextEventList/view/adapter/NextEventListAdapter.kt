/*
 *  Copyright (c) 2020, Comtrade
 *  All rights reserved.
 *
 *  Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of the <organization> nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *   DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  Package: com.comtrade.vinciapp.view.widget.nextEventList.view.adapter
 *  File: NextEventListAdapter.kt
 *  Author: Sofija Andjelkovic <sofija.andjelkovic@comtrade.com>
 *
 *  Description: next event list adapter
 *
 *  History: 8/27/20 Sofija Initial code
 *
 *
 */

package com.comtrade.vinciapp.view.widget.nextEventList.view.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.comtrade.domain.entities.event.EventEntity
import com.comtrade.vinciapp.R

class NextEventListAdapter(
    private val list: MutableList<EventEntity>,
    private val nextEventItemOnClickListener: NextEventItemOnClickListener
) :
    RecyclerView.Adapter<NextEventListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): NextEventListAdapter.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(inflater, parent)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: NextEventListAdapter.ViewHolder, position: Int) {
        val activity = list[position].title
        val time = list[position].getTime()
        val date = list[position].getDate()
        holder.bind(activity, date, time, position)
    }

    internal fun removeItemAtPosition(itemPosition: Int) {
        if (itemPosition >= 0 && itemPosition < list.size) {
            list.removeAt(itemPosition)
            notifyItemRemoved(itemPosition)
            notifyItemRangeChanged(itemPosition, list.size)
        }
    }

    internal fun updateItemAtPosition(updatedItem: EventEntity, itemPosition: Int) {
        if (itemPosition >= 0 && itemPosition < list.size) {
            list[itemPosition] = updatedItem
            notifyItemChanged(itemPosition, updatedItem)
        }
    }

    inner class ViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
        RecyclerView.ViewHolder(inflater.inflate(R.layout.next_event, parent, false)),
        View.OnClickListener {

        private var postponeButton: Button = itemView.findViewById(R.id.next_event_postpone_button)
        private var dismissButton: Button =
            itemView.findViewById(R.id.next_event_dismiss_event_button)
        private var inviteFriendsButton: Button =
            itemView.findViewById(R.id.next_event_invite_friends_button)
        private var nextEventText: LinearLayout =
            itemView.findViewById(R.id.next_event_vertical_layout)
        private var nextEventActivity: TextView = itemView.findViewById(R.id.next_event_activity)
        private var nextEventDateTimeText: TextView =
            itemView.findViewById(R.id.next_event_date_time_text)
        private var buttonsLayout: LinearLayout =
            itemView.findViewById(R.id.next_event_horizontal_layout)

        private var hideButton: Boolean = true
        private var itemPosition: Int = -1

        init {
            postponeButton.setOnClickListener(this)
            dismissButton.setOnClickListener(this)
            inviteFriendsButton.setOnClickListener(this)
            nextEventText.setOnClickListener(this)
        }

        @SuppressLint("SetTextI18n")
        fun bind(activity: String, date: String, time: String, position: Int) {
            nextEventActivity.text = activity
            nextEventDateTimeText.text = "$date / $time"
            itemPosition = position
        }

        override fun onClick(view: View?) {
            when (view) {
                postponeButton -> {
                    nextEventItemOnClickListener.showPostponeDialog(
                        list[layoutPosition],
                        layoutPosition
                    )
                }
                dismissButton -> {
                    nextEventItemOnClickListener.showDismissDialog(
                        list[layoutPosition],
                        layoutPosition
                    )
                }
                inviteFriendsButton -> {
                    nextEventItemOnClickListener.openSmsMessageApp(list[layoutPosition])
                }
                nextEventText -> {
                    if (hideButton) {
                        buttonsLayout.visibility = View.VISIBLE
                        hideButton = false
                    } else {
                        buttonsLayout.visibility = View.GONE
                        hideButton = true
                    }
                }
            }
        }
    }

    /**
     * Callback definition for the action to be invoked when an next event item has been clicked on.
     */
    interface NextEventItemOnClickListener {

        fun showPostponeDialog(eventEntity: EventEntity, itemAdapterPosition: Int)

        fun showDismissDialog(eventEntity: EventEntity, itemAdapterPosition: Int)

        fun openSmsMessageApp(eventEntity: EventEntity)
    }
}
