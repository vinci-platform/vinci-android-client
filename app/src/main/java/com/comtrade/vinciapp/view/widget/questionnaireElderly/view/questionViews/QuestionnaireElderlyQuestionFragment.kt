package com.comtrade.vinciapp.view.widget.questionnaireElderly.view.questionViews

import com.comtrade.vinciapp.base.BaseFragment

abstract class QuestionnaireElderlyQuestionFragment : BaseFragment() {
    abstract fun storeAnswer(): String
}
