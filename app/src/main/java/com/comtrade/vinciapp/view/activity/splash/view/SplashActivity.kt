/*
 *  Copyright (c) 2020, Comtrade
 *  All rights reserved.
 *
 *  Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of the <organization> nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *   DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  Package: com.comtrade.vinciapp.view.activity.splash.view
 *  File: SplashActivity.kt
 *  Author: Milenko Bojanic <milenko.bojanic@comtrade.com>
 *
 *  Description: initial splash screen activity class
 *
 *  History: 8/11/20 Milenko Initial code
 *
 *
 */

package com.comtrade.vinciapp.view.activity.splash.view

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.lifecycleScope
import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.vinciapp.VinciApp
import com.comtrade.vinciapp.base.BaseActivity
import com.comtrade.vinciapp.databinding.ActivitySplashBinding
import com.comtrade.vinciapp.view.activity.main.view.MainActivity
import com.comtrade.vinciapp.view.activity.prelogin.view.PreloginActivity
import com.comtrade.vinciapp.view.activity.splash.core.contract.SplashContract
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject

class SplashActivity : BaseActivity(), SplashContract.View {

    private lateinit var _binding: ActivitySplashBinding

    override fun getContentView(): View {
        _binding = ActivitySplashBinding.inflate(layoutInflater)
        return _binding.root
    }

    private val splashDuration = 3_000L

    @Inject
    lateinit var prefs: IPrefs

    @SuppressLint("VisibleForTests")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        (application as VinciApp).appComponent.inject(this)

        lifecycleScope.launch(Dispatchers.Main) {
            delay(splashDuration)

            if (prefs.userAutologin && prefs.userLogin.isNotBlank() && prefs.userPassword.isNotBlank()) {
                autoLogin()
            } else {
                proceedToLogin()
            }
        }
    }

    private fun proceedToLogin() {
        val intent = Intent(this, PreloginActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun autoLogin() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }
}
