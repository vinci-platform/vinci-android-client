/*
 *  Copyright (c) 2020, Comtrade
 *  All rights reserved.
 *
 *  Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of the <organization> nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *   DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  Package: com.comtrade.vinciapp.view.widget.questionnaireIPA.view.questionViews
 *  File: QuestionnaireIPAInputQuestions.kt
 *  Author: Kosta Eric <kosta.eric@comtrade.com>
 *
 *  Description: View for IPA questionnaire input fields screen
 *
 *  History: 8/11/20 Kosta  Initial code
 *
 *
 */

package com.comtrade.vinciapp.view.widget.questionnaireIPA.view.questionViews

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.base.BaseFragment
import com.comtrade.vinciapp.databinding.ViewIpaQuestionnaireInputQuestionBinding
import com.comtrade.vinciapp.view.widget.questionnaireIPA.core.contract.IIPAQuestionnaireInputScreen

class QuestionnaireIPAInputQuestions : BaseFragment(), IIPAQuestionnaireInputScreen {

    private var _binding: ViewIpaQuestionnaireInputQuestionBinding? = null
    private val binding get() = _binding!!

    companion object {
        const val TAG: String = "QuestionnaireIPAInputQuestions"

        fun newInstance(): QuestionnaireIPAInputQuestions {
            return QuestionnaireIPAInputQuestions()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding =
            ViewIpaQuestionnaireInputQuestionBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun hoursValidationCheckInput(maxHours: Int): Int {
        val hoursInput: EditText = binding.ipaQuestionnaireScreenInputQuestionEditTextHours

        if (hoursInput.text.toString().isEmpty()) {
            hoursInput.error = getString(R.string.input_empty_field_message)
        } else {
            val hours = Integer.parseInt(hoursInput.text.toString())
            if (hours > maxHours || hours < 0)
                hoursInput.error = getString(R.string.invalid_number_hours)
            else
                return hours
        }
        return -1
    }

    override fun minutesValidationCheckInput(): Int {
        val minutesInput: EditText = binding.ipaQuestionnaireScreenInputQuestionEditTextMinutes

        if (minutesInput.text.toString().isEmpty()) {
            minutesInput.error = getString(R.string.input_empty_field_message)
        } else {
            val minutes = Integer.parseInt(minutesInput.text.toString())
            if (minutes > 59 || minutes < 0)
                minutesInput.error = getString(R.string.invalid_number_minutes)
            else
                return minutes
        }
        return -1
    }
}
