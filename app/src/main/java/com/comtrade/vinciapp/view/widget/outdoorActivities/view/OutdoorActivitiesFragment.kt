/*
 *  Copyright (c) 2020, Comtrade
 *  All rights reserved.
 *
 *  Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of the <organization> nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *   DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  Package: com.comtrade.vinciapp.view.widget.outdoorActivities.view
 *  File: OutdoorActivitiesFragment.kt
 *  Author: Sofija Andjelkovic <sofija.andjelkovic@comtrade.com>
 *
 *  Description: fragment for choosing outdoor activity
 *
 *  History: 8/11/20 Sofija  Initial code
 *  History: 8/12/20 Kosta Added date & time picker for
 *
 */

package com.comtrade.vinciapp.view.widget.outdoorActivities.view

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.fragment.app.viewModels
import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.entities.event.EventEntity
import com.comtrade.domain_ui.state.RequestUIState
import com.comtrade.feature_events.di.EventsComponent
import com.comtrade.feature_events.ui.events.SaveEventViewModel
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.VinciApp
import com.comtrade.vinciapp.base.BaseFragment
import com.comtrade.vinciapp.databinding.FragmentOutdoorActivitiesBinding
import com.comtrade.vinciapp.view.activity.main.view.MainActivity
import com.comtrade.vinciapp.view.widget.outdoorActivities.core.OutdoorActivitiesPresenter
import com.comtrade.vinciapp.view.widget.outdoorActivities.core.contract.OutdoorActivitiesContract
import com.kunzisoft.switchdatetime.SwitchDateTimeDialogFragment
import java.text.DateFormat
import java.time.Instant
import java.util.*
import javax.inject.Inject

class OutdoorActivitiesFragment :
    BaseFragment(),
    AdapterView.OnItemSelectedListener,
    View.OnClickListener {

    private var _binding: FragmentOutdoorActivitiesBinding? = null
    private val binding get() = _binding!!

    private lateinit var adapter: ArrayAdapter<String>
    private lateinit var dateTimeFragment: SwitchDateTimeDialogFragment
    private var outdoorActivity: String = ""
    private var messageDate: String = ""
    private lateinit var calendarDate: Date
    private var activity: MainActivity? = null

    @Inject
    lateinit var prefs: IPrefs

    private val presenter: OutdoorActivitiesContract.Presenter by lazy {
        OutdoorActivitiesPresenter(
            requireContext().applicationContext
        )
    }

    private val saveEventsViewModel: SaveEventViewModel by viewModels {
        getEventsComponent().provideSaveEventViewModelFactory()
    }

    companion object {
        const val TAG: String = "OutdoorActivitiesFragment"
        const val TAG_DATETIME_FRAGMENT: String = "TAG_DATETIME_FRAGMENT"

        fun newInstance(): OutdoorActivitiesFragment {
            return OutdoorActivitiesFragment()
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (context.applicationContext as VinciApp).getAppComponent()
            .inject(this@OutdoorActivitiesFragment)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentOutdoorActivitiesBinding.inflate(inflater, container, false)
        if (requireActivity() is MainActivity) {
            activity = requireActivity() as MainActivity
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        addActivities()
        setEventPersistenceObservers()
        setListeners()
        createChannel(
            getString(R.string.notification_channel_id),
            getString(R.string.notification_channel_name)
        )
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun addActivities() {
        adapter = ArrayAdapter(requireContext(), R.layout.simple_spinner_item)
        val supportedActivities = resources.getStringArray(R.array.supported_outdoor_activities)
        for (outdoorActivity in supportedActivities) {
            adapter.add(outdoorActivity)
        }
        binding.outdoorActivitiesFragmentSpinner.adapter = adapter
    }

    private fun setEventPersistenceObservers() {
        saveEventsViewModel.storeEventEntity.observe(viewLifecycleOwner) {
            when (it) {
                RequestUIState.DefaultState -> {}
                is RequestUIState.OnError -> {
                    it.message.let { message ->
                        if (message != null) {
                            showShortToast(message)
                        } else {
                            showShortToast(getString(R.string.error_problem_creating_new_event))
                        }
                    }
                    saveEventsViewModel.resetState()
                }
                RequestUIState.OnLoading -> {}
                is RequestUIState.OnSuccess -> {
                    presenter.startNotificationTimer(it.data)
                    saveEventsViewModel.resetState()
                    proceedToInviteFriendsIfChecked()
                }
            }
        }
    }

    private fun setListeners() {
        binding.outdoorActivitiesFragmentSpinner.onItemSelectedListener = this
        binding.outdoorActivitiesFragmentDatetimeSpinner.setOnClickListener(this)
        binding.outdoorActivitiesSaveButton.setOnClickListener(this)
        setDateTimeListener()
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        outdoorActivity = parent?.getItemAtPosition(position).toString()
    }

    override fun onClick(view: View?) {
        when (view) {
            binding.outdoorActivitiesFragmentDatetimeSpinner -> {
                showCalendar()
            }
            binding.outdoorActivitiesSaveButton -> {
                createNotification()
            }
        }
    }

    private fun showCalendar() {
        val calendar = Calendar.getInstance()
        dateTimeFragment.startAtCalendarView()
        dateTimeFragment.setDefaultDateTime(
            GregorianCalendar(
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH),
                calendar.get(Calendar.HOUR_OF_DAY),
                calendar.get(Calendar.MINUTE),
                calendar.get(Calendar.SECOND)
            ).time
        )

        activity?.let {
            if (dateTimeFragment.isAdded) {
                return@let
            }
            dateTimeFragment.show(
                it.supportFragmentManager,
                TAG_DATETIME_FRAGMENT
            )
        }
    }

    private fun setDateTimeListener() {
        dateTimeFragment = SwitchDateTimeDialogFragment.newInstance(
            getString(R.string.outdoor_activities_screen_picker_title),
            getString(R.string.common_button_set),
            getString(R.string.common_button_cancel)
        )
        dateTimeFragment.setOnButtonClickListener(object :
                SwitchDateTimeDialogFragment.OnButtonWithNeutralClickListener {
                override fun onPositiveButtonClick(date: Date) {
                    val selectedDate =
                        DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.SHORT).format(date)
                    messageDate = selectedDate
                    binding.outdoorActivitiesFragmentDatetimeSpinner.text = selectedDate
                    binding.outdoorActivitiesSaveButton.isEnabled = true
                    calendarDate = date
                }

                override fun onNeutralButtonClick(date: Date?) {}
                override fun onNegativeButtonClick(date: Date?) {}
            })
    }

    private fun dateTimeValidationCheckInput(): Boolean {
        val dateTimeInput: TextView = binding.outdoorActivitiesFragmentDatetimeSpinner
        var validation = true
        if (dateTimeInput.text.isEmpty()) {
            validation = false
        }
        return validation
    }

    private fun createNotification() {
        if (calendarDate.time > Date().time) {
            val eventRecord = EventEntity(
                0,
                prefs.userId,
                calendarDate.time,
                Instant.now().toEpochMilli(),
                calendarDate.time,
                "",
                outdoorActivity,
                "Activity",
                ""
            )
            saveEventsViewModel.storeEventEntity(eventRecord)
        } else {
            showShortToast(getString(R.string.outdoor_activities_activity_time_negative_message))
        }
    }

    private fun proceedToInviteFriendsIfChecked() {
        if (binding.outdoorActivitiesFragmentInviteFriendsCheckbox.isChecked) {
            val smsBody = getString(
                R.string.outdoor_activities_invite_contacts_message,
                outdoorActivity,
                messageDate
            )
            presenter.openInviteFriendsFragment(requireActivity().supportFragmentManager, smsBody)
        } else {
            presenter.openMainScreen(requireActivity().supportFragmentManager)
        }
    }
}

/**
 * Obtain the Dagger dependency component extension.
 */
@SuppressLint("VisibleForTests")
internal fun OutdoorActivitiesFragment.getEventsComponent(): EventsComponent =
    (requireContext().applicationContext as VinciApp).getEventsComponent()
