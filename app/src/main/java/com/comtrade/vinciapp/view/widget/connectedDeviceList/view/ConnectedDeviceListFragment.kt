/*
 *  Copyright (c) 2020, Comtrade
 *  All rights reserved.
 *
 *  Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of the <organization> nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *   DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  Package: com.comtrade.vinciapp.view.widget.connectedDeviceList.view
 *  File: ConnectedDeviceListFragment.kt
 *  Author: Sofija Andjelkovic <sofija.andjelkovic@comtrade.com>
 *
 *  Description: list of connected devices
 *
 *  History: 9/16/20 Sofija  Initial code
 *
 *
 */

package com.comtrade.vinciapp.view.widget.connectedDeviceList.view

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.graphics.drawable.ClipDrawable.HORIZONTAL
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.di.AppQualifiers
import com.comtrade.domain.entities.devices.data.UpdateDeviceRequest
import com.comtrade.domain.entities.user.account.GetUserEntity
import com.comtrade.domain.settings.SupportedDeviceTypes
import com.comtrade.feature_account.ui.state.APICallSuccessUIState
import com.comtrade.feature_account.ui.user.GetUserViewModel
import com.comtrade.feature_account.ui.user.RefreshUserDataViewModel
import com.comtrade.feature_devices.ui.DeleteUserDeviceViewModel
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.VinciApp
import com.comtrade.vinciapp.base.BaseFragment
import com.comtrade.vinciapp.databinding.FragmentConnectedDeviceListBinding
import com.comtrade.vinciapp.view.widget.connectedDeviceList.core.contract.ConnectedDeviceListContract
import com.comtrade.vinciapp.view.widget.connectedDeviceList.view.adapter.ConnectedDeviceListAdapter
import javax.inject.Inject
import javax.inject.Named

@SuppressLint("VisibleForTests")
class ConnectedDeviceListFragment :
    BaseFragment(),
    ConnectedDeviceListContract.View,
    ConnectedDeviceListAdapter.IRemoveConnectedDeviceListener {

    private var _binding: FragmentConnectedDeviceListBinding? = null
    private val binding get() = _binding!!

    private val getUserViewModel: GetUserViewModel by viewModels {
        (requireContext().applicationContext as VinciApp).getAccountComponent()
            .provideGetUserViewModelFactory()
    }

    private val refreshUserDataViewModel: RefreshUserDataViewModel by viewModels {
        (requireContext().applicationContext as VinciApp).getAccountComponent()
            .provideRefreshUserDataViewModelFactory()
    }

    private val deleteDeviceViewModel: DeleteUserDeviceViewModel by viewModels {
        (requireContext().applicationContext as VinciApp).getDevicesComponent()
            .provideDeleteUserDeviceViewModelFactory()
    }

    private var connectedDeviceAdapter: ConnectedDeviceListAdapter? = null

    private var deleteDeviceDialog: Dialog? = null

    @Inject
    lateinit var prefs: IPrefs

    @Inject
    @Named(AppQualifiers.ROOT_IMAGE_DIRECTORY)
    lateinit var directory: String

    companion object {
        const val TAG: String = "ConnectedDeviceListFragment"

        fun newInstance(): ConnectedDeviceListFragment {
            return ConnectedDeviceListFragment()
        }
    }

    @SuppressLint("VisibleForTests")
    override fun onAttach(context: Context) {
        super.onAttach(context)
        (context.applicationContext as VinciApp).getAppComponent()
            .inject(this@ConnectedDeviceListFragment)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentConnectedDeviceListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val connectedDeviceList = mutableListOf<ConnectedDeviceListEntity>()

        prefs.fitBitDevice?.let {
            connectedDeviceList.add(
                ConnectedDeviceListEntity(
                    it,
                    SupportedDeviceTypes.FITBIT_WATCH,
                    getString(R.string.connected_device_screen_fitbit_watch_type)
                )
            )
        }

        prefs.shoeDevice?.let {
            connectedDeviceList.add(
                ConnectedDeviceListEntity(
                    it,
                    SupportedDeviceTypes.SHOE,
                    getString(R.string.connected_device_screen_shoe_insole_type)
                )
            )
        }

        prefs.cmdDevice?.let {
            connectedDeviceList.add(
                ConnectedDeviceListEntity(
                    it,
                    SupportedDeviceTypes.WATCH,
                    getString(R.string.connected_device_screen_cmd_watch_type)
                )
            )
        }

        prefs.surveyDevice?.let {
            connectedDeviceList.add(
                ConnectedDeviceListEntity(
                    it,
                    SupportedDeviceTypes.SURVEY,
                    getString(R.string.connected_device_screen_survey_type)
                )
            )
        }

        val itemDecor = DividerItemDecoration(requireContext(), HORIZONTAL)
        binding.connectedDeviceList.addItemDecoration(itemDecor)
        setAdapter(connectedDeviceList)
        observeUserRefreshData()
        observeDeleteDeviceState()
    }

    override fun onStop() {
        super.onStop()

        deleteDeviceDialog?.apply {
            if (isShowing) {
                dismiss()
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun observeUserData(block: (getUserEntity: GetUserEntity) -> Unit) {
        getUserViewModel.userState.observe(viewLifecycleOwner) {
            when (it) {
                APICallSuccessUIState.DefaultState -> {}
                is APICallSuccessUIState.OnError -> {
                    it.message?.let { message ->
                        showShortToast(message)
                    }
                    getUserViewModel.resetState()
                }
                APICallSuccessUIState.OnLoading -> {
                }
                is APICallSuccessUIState.OnSuccess -> {
                    block(it.data)
                    getUserViewModel.resetState()
                }
            }
        }
    }

    private fun observeUserRefreshData() {
        refreshUserDataViewModel.refreshUserDataObserve.observe(viewLifecycleOwner) {
            when (it) {
                APICallSuccessUIState.DefaultState -> {}
                is APICallSuccessUIState.OnError -> {
                    it.message?.let { message ->
                        prefs.connectionError =
                            prefs.connectionError + "\n" + getString(
                            R.string.get_user_extra_error,
                            message
                        )
                        showShortToast(message)
                    }
                    refreshUserDataViewModel.resetState()
                }
                APICallSuccessUIState.OnLoading -> {}
                is APICallSuccessUIState.OnSuccess -> {
                    if (!it.data) {
                        showShortToast(
                            getString(R.string.main_screen_refresh_user_data_error)
                        )
                    }
                    refreshUserDataViewModel.resetState()
                }
            }
        }
    }

    /**
     * Observes the delete device action and updates the view state of this component acordingly.
     */
    private fun observeDeleteDeviceState() {
        deleteDeviceViewModel.deleteDeviceState.observe(viewLifecycleOwner) {
            when (it) {
                com.comtrade.feature_devices.ui.state.APICallSuccessUIState.DefaultState -> {}
                is com.comtrade.feature_devices.ui.state.APICallSuccessUIState.OnError -> {
                    it.message?.let { message ->
                        showShortToast(message)
                    }
                    deleteDeviceViewModel.resetState()
                }
                com.comtrade.feature_devices.ui.state.APICallSuccessUIState.OnLoading -> {}
                is com.comtrade.feature_devices.ui.state.APICallSuccessUIState.OnSuccess -> {
                    if (it.data) {
                        showShortToast(getString(R.string.connected_device_screen_device_deleted))

                        // Sync User data
                        getUserViewModel.getUser()
                    } else {
                        showShortToast(getString(R.string.connected_device_screen_device_delete_problem))
                    }
                    deleteDeviceViewModel.resetState()
                }
            }
        }
    }

    private fun setAdapter(connectedDeviceList: MutableList<ConnectedDeviceListEntity>) {
        val linearLayoutManager = LinearLayoutManager(requireContext())
        binding.connectedDeviceList.layoutManager = linearLayoutManager
        connectedDeviceAdapter =
            ConnectedDeviceListAdapter(connectedDeviceList, prefs, this@ConnectedDeviceListFragment)
        binding.connectedDeviceList.adapter = connectedDeviceAdapter
    }

    private fun showDeleteDeviceDialog(block: () -> Unit) {
        deleteDeviceDialog = Dialog(requireContext())
        deleteDeviceDialog?.apply {

            requestWindowFeature(Window.FEATURE_NO_TITLE)
            setCancelable(false)
            setContentView(R.layout.delete_device_dialog)
            findViewById<Button>(R.id.dismiss_activity_dialog_yes_button).setOnClickListener {
                block()
                dismiss()
            }
            findViewById<Button>(R.id.dismiss_activity_dialog_no_button).setOnClickListener {
                dismiss()
            }
            show()
        }
    }

    override fun onCMDDeviceRemoved(updateDeviceRequest: UpdateDeviceRequest, layoutPosition: Int) {

        showDeleteDeviceDialog {

            // Delete device from account
            updateDeviceRequest.id?.let {
                deleteDeviceViewModel.deleteDeviceWithId(it)
            }

            // Remove item from adapter.
            connectedDeviceAdapter?.removeItemAtPosition(layoutPosition)
        }
    }

    override fun onShoeDeviceRemoved(
        updateDeviceRequest: UpdateDeviceRequest,
        layoutPosition: Int
    ) {
        showDeleteDeviceDialog {

            updateDeviceRequest.id?.let {
                deleteDeviceViewModel.deleteDeviceWithId(it)
            }

            // Remove item from adapter.
            connectedDeviceAdapter?.removeItemAtPosition(layoutPosition)
        }
    }

    override fun onUpdateUserDeviceSuccess(data: GetUserEntity) {

        refreshUserDataViewModel.refreshUserData(userEntity = data, directory)
    }

    override fun onUpdateUserDeviceFailure(error: Throwable) {
        prefs.addLog("Error updateUserDevice: $error", true)
    }
}
