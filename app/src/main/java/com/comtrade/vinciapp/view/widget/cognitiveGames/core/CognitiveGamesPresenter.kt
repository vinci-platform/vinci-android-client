/*
 *  Copyright (c) 2020, Comtrade
 *  All rights reserved.
 *
 *  Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of the <organization> nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *   DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  Package: com.comtrade.vinciapp.view.widget.cognitiveGames.core
 *  File: CognitiveGamesPresenter.kt
 *  Author: Kosta Eric <kosta.eric@comtrade.com>
 *
 *  Description: cognitive games presenter
 *
 *  History: 8/11/20 Kosta  Initial code
 *
 *
 */

package com.comtrade.vinciapp.view.widget.cognitiveGames.core

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.core.app.AlarmManagerCompat
import androidx.fragment.app.FragmentManager
import com.comtrade.domain.entities.event.EventEntity
import com.comtrade.game_sudoku.ui.SudokuFragment
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.base.BasePresenter
import com.comtrade.vinciapp.receiver.AlarmReceiver
import com.comtrade.vinciapp.view.widget.cognitiveGames.core.contract.CognitiveGamesContract
import com.comtrade.vinciapp.view.widget.inviteFriends.view.InviteFriendsFragment
import com.comtrade.vinciapp.view.widget.mainScreen.view.MainScreenFragment

class CognitiveGamesPresenter :
    BasePresenter(),
    CognitiveGamesContract.Presenter {

    override fun showMainScreen(fragmentManager: FragmentManager) {
        changeMainActivityFragment(
            fragmentManager,
            MainScreenFragment.newInstance(),
            MainScreenFragment.TAG
        )
    }

    override fun openInviteFriendsFragment(fragmentManager: FragmentManager, smsBody: String) {
        changeMainActivityFragment(
            fragmentManager,
            InviteFriendsFragment.newInstance(smsBody),
            InviteFriendsFragment.TAG
        )
    }

    override fun showSudokuScreen(fragmentManager: FragmentManager, game: Int) {
        changeMainActivityFragment(
            fragmentManager,
            SudokuFragment.newInstance(game),
            SudokuFragment.TAG
        )
    }

    override fun startNotificationTimer(context: Context, eventEntity: EventEntity) {
        val requestId = eventEntity.id

        val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val notifyIntent = Intent(context, AlarmReceiver::class.java)
        notifyIntent.putExtra(
            AlarmReceiver.NOTIFICATION_TEXT_KEY,
            context.getString(R.string.notification_text) + " " + eventEntity.title
        )
        val notifyPendingIntent = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            PendingIntent.getBroadcast(
                context,
                requestId,
                notifyIntent,
                PendingIntent.FLAG_IMMUTABLE
            )
        } else {
            PendingIntent.getBroadcast(
                context,
                requestId,
                notifyIntent,
                PendingIntent.FLAG_UPDATE_CURRENT
            )
        }

        AlarmManagerCompat.setExactAndAllowWhileIdle(
            alarmManager,
            AlarmManager.RTC_WAKEUP,
            eventEntity.timestamp,
            notifyPendingIntent
        )
    }
}
