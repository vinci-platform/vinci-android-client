/*
 *  Copyright (c) 2020, Comtrade
 *  All rights reserved.
 *
 *  Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of the <organization> nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *   DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  Package: com.comtrade.vinciapp.view.widget.questionnaireSurvey.view
 *  File: SurveyQuestionnaireFragment.kt
 *  Author: Kosta Eric <kosta.eric@comtrade.com>
 *
 *  Description: Fragment for Survey questionnaire screens
 *
 *  History: 8/11/20 Kosta  Initial code
 *
 *
 */

package com.comtrade.vinciapp.view.widget.questionnaireUserNeeds.view

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.activityViewModels
import androidx.viewpager2.widget.ViewPager2
import com.comtrade.feature_questionnaire.survey.ui.SurveyViewModel
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.VinciApp
import com.comtrade.vinciapp.base.BaseFragment
import com.comtrade.vinciapp.databinding.FragmentSurveyQuestionnaireBinding
import com.comtrade.vinciapp.util.ViewUtils
import com.comtrade.vinciapp.view.adapter.QuestionnaireViewPagerAdapter
import com.comtrade.vinciapp.view.widget.questionnaireSurvey.model.SurveyQuestionnaireItem
import com.comtrade.vinciapp.view.widget.questionnaireUserNeeds.view.questionViews.QuestionnaireCheckboxQuestion
import com.comtrade.vinciapp.view.widget.questionnaireUserNeeds.view.questionViews.QuestionnaireEndScreen
import com.comtrade.vinciapp.view.widget.questionnaireUserNeeds.view.questionViews.QuestionnaireGradationQuestion
import com.comtrade.vinciapp.view.widget.questionnaireUserNeeds.view.questionViews.QuestionnaireRadioButtonQuestion

class UserNeedsQuestionnaireFragment :
    BaseFragment(),
    View.OnClickListener {
    private var _binding: FragmentSurveyQuestionnaireBinding? = null
    private val binding get() = _binding!!

    private lateinit var fragmentsPagerAdapter: QuestionnaireViewPagerAdapter

    private lateinit var a4Id: String
    private lateinit var a5Id: String
    private lateinit var a6Id: String
    private lateinit var a7Id: String
    private lateinit var a8Id: String
    private lateinit var a9Id: String
    private lateinit var a10Id: String
    private lateinit var a11Id: String
    private lateinit var a12Id: String
    private lateinit var b1Id: String
    private lateinit var b2Id: String
    private lateinit var b3Id: String
    private lateinit var b4aId: String
    private lateinit var b4bId: String
    private lateinit var b4cId: String
    private lateinit var b4dId: String
    private lateinit var b5aId: String
    private lateinit var b5bId: String
    private lateinit var b5cId: String
    private lateinit var b5dId: String
    private lateinit var b5eId: String
    private lateinit var b6Id: String
    private lateinit var b7Id: String
    private lateinit var b8aId: String
    private lateinit var b8bId: String
    private lateinit var b8cId: String
    private lateinit var b8dId: String
    private lateinit var b8eId: String
    private lateinit var b9aId: String
    private lateinit var b9bId: String
    private lateinit var b9cId: String
    private lateinit var b9dId: String
    private lateinit var b9eId: String
    private lateinit var b10Id: String
    private lateinit var b11Id: String
    private lateinit var b12Id: String
    private lateinit var b13Id: String

    private val surveyViewModel: SurveyViewModel by activityViewModels {
        (requireContext().applicationContext as VinciApp).getAppComponent()
            .provideSurveyViewModelFactory()
    }

    companion object {
        const val TAG: String = "UserNeedsQuestionnaireFragment"

        const val ZERO = 0

        const val SURVEY_TYPE = "USER_NEEDS"

        fun newInstance(): UserNeedsQuestionnaireFragment {
            return UserNeedsQuestionnaireFragment()
        }
    }

    @SuppressLint("VisibleForTests")
    override fun onAttach(context: Context) {
        super.onAttach(context)
        (context.applicationContext as VinciApp).getAppComponent()
            .inject(this@UserNeedsQuestionnaireFragment)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSurveyQuestionnaireBinding.inflate(inflater, container, false)
        initValues()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        surveyViewModel.startSurvey(SURVEY_TYPE)

        setAdapter()
        setListeners()
        setQuestionnaireAnswersMap()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun setAdapter() {
        fragmentsPagerAdapter =
            QuestionnaireViewPagerAdapter(childFragmentManager, viewLifecycleOwner.lifecycle)
        addQuestionsFragments()
        binding.surveyQuestionnaireFragmentViewPager.adapter = fragmentsPagerAdapter
        binding.surveyQuestionnaireFragmentViewPager.offscreenPageLimit = 50
    }

    private fun setListeners() {
        binding.surveyQuestionnaireFragmentNextButton.setOnClickListener(this)
        binding.surveyQuestionnaireFragmentBackButton.setOnClickListener(this)
        binding.surveyQuestionnaireFragmentSaveButton.setOnClickListener(this)
        binding.surveyQuestionnaireFragmentViewPager.registerOnPageChangeCallback(object :
                ViewPager2.OnPageChangeCallback() {
                override fun onPageSelected(position: Int) {
                    super.onPageSelected(position)
                    if (position == fragmentsPagerAdapter.itemCount - 1) {
                        ViewUtils.hideView(binding.surveyQuestionnaireFragmentNextButton)
                        ViewUtils.showView(binding.surveyQuestionnaireFragmentSaveButton)
                    } else {
                        ViewUtils.hideView(binding.surveyQuestionnaireFragmentSaveButton)
                        ViewUtils.showView(binding.surveyQuestionnaireFragmentNextButton)
                    }
                }
            })
    }

    private fun initValues() {
        a4Id = getString(R.string.user_needs_questionnaire_screen_question_a4_id)
        a5Id = getString(R.string.user_needs_questionnaire_screen_question_a5_id)
        a6Id = getString(R.string.user_needs_questionnaire_screen_question_a6_id)
        a7Id = getString(R.string.user_needs_questionnaire_screen_question_a7_id)
        a8Id = getString(R.string.user_needs_questionnaire_screen_question_a8_id)
        a9Id = getString(R.string.user_needs_questionnaire_screen_question_a9_id)
        a10Id = getString(R.string.user_needs_questionnaire_screen_question_a10_id)
        a11Id = getString(R.string.user_needs_questionnaire_screen_question_a11_id)
        a12Id = getString(R.string.user_needs_questionnaire_screen_question_a12_id)
        b1Id = getString(R.string.user_needs_questionnaire_screen_question_b1_id)
        b2Id = getString(R.string.user_needs_questionnaire_screen_question_b2_id)
        b3Id = getString(R.string.user_needs_questionnaire_screen_question_b3_id)
        b4aId = getString(R.string.user_needs_questionnaire_screen_question_b4a_id)
        b4bId = getString(R.string.user_needs_questionnaire_screen_question_b4b_id)
        b4cId = getString(R.string.user_needs_questionnaire_screen_question_b4c_id)
        b4dId = getString(R.string.user_needs_questionnaire_screen_question_b4d_id)
        b5aId = getString(R.string.user_needs_questionnaire_screen_question_b5a_id)
        b5bId = getString(R.string.user_needs_questionnaire_screen_question_b5b_id)
        b5cId = getString(R.string.user_needs_questionnaire_screen_question_b5c_id)
        b5dId = getString(R.string.user_needs_questionnaire_screen_question_b5d_id)
        b5eId = getString(R.string.user_needs_questionnaire_screen_question_b5e_id)
        b6Id = getString(R.string.user_needs_questionnaire_screen_question_b6_id)
        b7Id = getString(R.string.user_needs_questionnaire_screen_question_b7_id)
        b8aId = getString(R.string.user_needs_questionnaire_screen_question_b8a_id)
        b8bId = getString(R.string.user_needs_questionnaire_screen_question_b8b_id)
        b8cId = getString(R.string.user_needs_questionnaire_screen_question_b8c_id)
        b8dId = getString(R.string.user_needs_questionnaire_screen_question_b8d_id)
        b8eId = getString(R.string.user_needs_questionnaire_screen_question_b8e_id)
        b9aId = getString(R.string.user_needs_questionnaire_screen_question_b9a_id)
        b9bId = getString(R.string.user_needs_questionnaire_screen_question_b9b_id)
        b9cId = getString(R.string.user_needs_questionnaire_screen_question_b9c_id)
        b9dId = getString(R.string.user_needs_questionnaire_screen_question_b9d_id)
        b9eId = getString(R.string.user_needs_questionnaire_screen_question_b9e_id)
        b10Id = getString(R.string.user_needs_questionnaire_screen_question_b10_id)
        b11Id = getString(R.string.user_needs_questionnaire_screen_question_b11_id)
        b12Id = getString(R.string.user_needs_questionnaire_screen_question_b12_id)
        b13Id = getString(R.string.user_needs_questionnaire_screen_question_b13_id)
    }

    private fun setQuestionnaireAnswersMap() {
        surveyViewModel.questionnaireUserNeedsAnswers = mutableMapOf(
            a4Id to "",
            a5Id to "",
            a6Id to "",
            a7Id to "",
            a8Id to "",
            a9Id to "",
            a10Id to "",
            a11Id to "",
            a12Id to "",
            b1Id to "",
            b2Id to "",
            b3Id to "",
            b4aId to "",
            b4bId to "",
            b4cId to "",
            b4dId to "",
            b5aId to "",
            b5bId to "",
            b5cId to "",
            b5dId to "",
            b5eId to "",
            b6Id to "",
            b7Id to "",
            b8aId to "",
            b8bId to "",
            b8cId to "",
            b8dId to "",
            b8eId to "",
            b9aId to "",
            b9bId to "",
            b9cId to "",
            b9dId to "",
            b9eId to "",
            b10Id to "",
            b11Id to "",
            b12Id to "",
            b13Id to ""
        )
    }

    override fun onClick(view: View?) {
        when (view) {
            binding.surveyQuestionnaireFragmentNextButton -> {
                increaseViewPagerCurrentItem()
            }
            binding.surveyQuestionnaireFragmentBackButton -> {
                decreaseViewPagerCurrentItem()
            }
            binding.surveyQuestionnaireFragmentSaveButton -> {

                surveyViewModel.stopAndSaveActiveSurvey()

                binding.surveyQuestionnaireFragmentViewPager.currentItem = 0

                // Go to root fragment
                requireActivity().supportFragmentManager.popBackStack(
                    null,
                    FragmentManager.POP_BACK_STACK_INCLUSIVE
                )
            }
        }
    }

    private fun increaseViewPagerCurrentItem() {
        if (binding.surveyQuestionnaireFragmentViewPager.currentItem == fragmentsPagerAdapter.itemCount - 1) {
            return
        } else {
            if (binding.surveyQuestionnaireFragmentViewPager.currentItem == fragmentsPagerAdapter.itemCount - 2) {
                // survey_questionnaire_end_score_result.text = calculateScore()
            }
            binding.surveyQuestionnaireFragmentViewPager.setCurrentItem(
                binding.surveyQuestionnaireFragmentViewPager.currentItem + 1,
                true
            )
        }
    }

    private fun decreaseViewPagerCurrentItem() {
        if (binding.surveyQuestionnaireFragmentViewPager.currentItem == 0) {
            requireActivity().onBackPressed()
        } else {
            binding.surveyQuestionnaireFragmentViewPager.setCurrentItem(
                binding.surveyQuestionnaireFragmentViewPager.currentItem - 1,
                true
            )
        }
    }

    private fun addQuestionsFragments() {
        fragmentsPagerAdapter.addFragment(
            QuestionnaireRadioButtonQuestion.newInstance(
                SurveyQuestionnaireItem(
                    a4Id, a4Id,
                    getString(R.string.user_needs_questionnaire_screen_question_a4_text),
                    true,
                    resources.getStringArray(R.array.user_needs_variants_marital_status)
                )
            )
        )
        fragmentsPagerAdapter.addFragment(
            QuestionnaireRadioButtonQuestion.newInstance(
                SurveyQuestionnaireItem(
                    a5Id, a5Id,
                    getString(R.string.user_needs_questionnaire_screen_question_a5_text),
                    true,
                    resources.getStringArray(R.array.user_needs_variants_education)
                )
            )
        )
        fragmentsPagerAdapter.addFragment(
            QuestionnaireRadioButtonQuestion.newInstance(
                SurveyQuestionnaireItem(
                    a6Id, a6Id,
                    getString(R.string.user_needs_questionnaire_screen_question_a6_text),
                    true,
                    resources.getStringArray(R.array.user_needs_variants_frequency)
                )
            )
        )
        fragmentsPagerAdapter.addFragment(
            QuestionnaireCheckboxQuestion.newInstance(
                SurveyQuestionnaireItem(
                    a7Id, a7Id,
                    getString(R.string.user_needs_questionnaire_screen_question_a7_text),
                    true,
                    resources.getStringArray(R.array.user_needs_variants_communication)
                )
            )
        )
        fragmentsPagerAdapter.addFragment(
            QuestionnaireGradationQuestion.newInstance(
                getString(R.string.user_needs_questionnaire_agree_info_label),
                SurveyQuestionnaireItem(
                    a8Id, a8Id,
                    getString(R.string.user_needs_questionnaire_screen_question_a8_text),
                    true,
                    resources.getStringArray(R.array.user_needs_variants_agree)
                )
            )
        )
        fragmentsPagerAdapter.addFragment(
            QuestionnaireGradationQuestion.newInstance(
                getString(R.string.user_needs_questionnaire_agree_info_label),
                SurveyQuestionnaireItem(
                    a9Id, a9Id,
                    getString(R.string.user_needs_questionnaire_screen_question_a9_text),
                    true,
                    resources.getStringArray(R.array.user_needs_variants_agree)
                )
            )
        )
        fragmentsPagerAdapter.addFragment(
            QuestionnaireGradationQuestion.newInstance(
                getString(R.string.user_needs_questionnaire_agree_info_label),
                SurveyQuestionnaireItem(
                    a10Id, a10Id,
                    getString(R.string.user_needs_questionnaire_screen_question_a10_text),
                    true,
                    resources.getStringArray(R.array.user_needs_variants_agree)
                )
            )
        )
        fragmentsPagerAdapter.addFragment(
            QuestionnaireGradationQuestion.newInstance(
                getString(R.string.user_needs_questionnaire_agree_info_label),
                SurveyQuestionnaireItem(
                    a11Id, a11Id,
                    getString(R.string.user_needs_questionnaire_screen_question_a11_text),
                    true,
                    resources.getStringArray(R.array.user_needs_variants_agree)
                )
            )
        )
        fragmentsPagerAdapter.addFragment(
            QuestionnaireGradationQuestion.newInstance(
                getString(R.string.user_needs_questionnaire_agree_info_label),
                SurveyQuestionnaireItem(
                    a12Id, a12Id,
                    getString(R.string.user_needs_questionnaire_screen_question_a12_text),
                    true,
                    resources.getStringArray(R.array.user_needs_variants_agree)
                )
            )
        )
        fragmentsPagerAdapter.addFragment(
            QuestionnaireGradationQuestion.newInstance(
                getString(R.string.user_needs_questionnaire_useful_info_label),
                SurveyQuestionnaireItem(
                    b1Id, b1Id,
                    getString(R.string.user_needs_questionnaire_screen_question_b1_text),
                    true,
                    resources.getStringArray(R.array.user_needs_variants_userful)
                )
            )
        )
        fragmentsPagerAdapter.addFragment(
            QuestionnaireGradationQuestion.newInstance(
                getString(R.string.user_needs_questionnaire_useful_info_label),
                SurveyQuestionnaireItem(
                    b2Id, b2Id,
                    getString(R.string.user_needs_questionnaire_screen_question_b2_text),
                    true,
                    resources.getStringArray(R.array.user_needs_variants_userful)
                )
            )
        )
        fragmentsPagerAdapter.addFragment(
            QuestionnaireGradationQuestion.newInstance(
                getString(R.string.user_needs_questionnaire_useful_info_label),
                SurveyQuestionnaireItem(
                    b3Id, b3Id,
                    getString(R.string.user_needs_questionnaire_screen_question_b3_text),
                    true,
                    resources.getStringArray(R.array.user_needs_variants_userful)
                )
            )
        )
        fragmentsPagerAdapter.addFragment(
            QuestionnaireGradationQuestion.newInstance(
                getString(R.string.user_needs_questionnaire_useful_info_label),
                SurveyQuestionnaireItem(
                    b4aId, b4aId,
                    getString(R.string.user_needs_questionnaire_screen_question_b4_text) + " \n\n  - " + getString(
                        R.string.user_needs_questionnaire_screen_question_b4a_text
                    ),
                    true,
                    resources.getStringArray(R.array.user_needs_variants_userful)
                )
            )
        )
        fragmentsPagerAdapter.addFragment(
            QuestionnaireGradationQuestion.newInstance(
                getString(R.string.user_needs_questionnaire_useful_info_label),
                SurveyQuestionnaireItem(
                    b4bId, b4bId,
                    getString(R.string.user_needs_questionnaire_screen_question_b4_text) + " \n\n  - " + getString(
                        R.string.user_needs_questionnaire_screen_question_b4b_text
                    ),
                    true,
                    resources.getStringArray(R.array.user_needs_variants_userful)
                )
            )
        )
        fragmentsPagerAdapter.addFragment(
            QuestionnaireGradationQuestion.newInstance(
                getString(R.string.user_needs_questionnaire_useful_info_label),
                SurveyQuestionnaireItem(
                    b4cId, b4cId,
                    getString(R.string.user_needs_questionnaire_screen_question_b4_text) + " \n\n  - " + getString(
                        R.string.user_needs_questionnaire_screen_question_b4c_text
                    ),
                    true,
                    resources.getStringArray(R.array.user_needs_variants_userful)
                )
            )
        )
        fragmentsPagerAdapter.addFragment(
            QuestionnaireGradationQuestion.newInstance(
                getString(R.string.user_needs_questionnaire_useful_info_label),
                SurveyQuestionnaireItem(
                    b4dId, b4dId,
                    getString(R.string.user_needs_questionnaire_screen_question_b4_text) + " \n\n  - " + getString(
                        R.string.user_needs_questionnaire_screen_question_b4d_text
                    ),
                    true,
                    resources.getStringArray(R.array.user_needs_variants_userful)
                )
            )
        )
        fragmentsPagerAdapter.addFragment(
            QuestionnaireGradationQuestion.newInstance(
                getString(R.string.user_needs_questionnaire_useful_info_label),
                SurveyQuestionnaireItem(
                    b5aId, b5aId,
                    getString(R.string.user_needs_questionnaire_screen_question_b5_text) + " \n\n  - " + getString(
                        R.string.user_needs_questionnaire_screen_question_b5a_text
                    ),
                    true,
                    resources.getStringArray(R.array.user_needs_variants_userful)
                )
            )
        )
        fragmentsPagerAdapter.addFragment(
            QuestionnaireGradationQuestion.newInstance(
                getString(R.string.user_needs_questionnaire_useful_info_label),
                SurveyQuestionnaireItem(
                    b5bId, b5bId,
                    getString(R.string.user_needs_questionnaire_screen_question_b5_text) + " \n\n  - " + getString(
                        R.string.user_needs_questionnaire_screen_question_b5b_text
                    ),
                    true,
                    resources.getStringArray(R.array.user_needs_variants_userful)
                )
            )
        )
        fragmentsPagerAdapter.addFragment(
            QuestionnaireGradationQuestion.newInstance(
                getString(R.string.user_needs_questionnaire_useful_info_label),
                SurveyQuestionnaireItem(
                    b5cId, b5cId,
                    getString(R.string.user_needs_questionnaire_screen_question_b5_text) + " \n\n  - " + getString(
                        R.string.user_needs_questionnaire_screen_question_b5c_text
                    ),
                    true,
                    resources.getStringArray(R.array.user_needs_variants_userful)
                )
            )
        )
        fragmentsPagerAdapter.addFragment(
            QuestionnaireGradationQuestion.newInstance(
                getString(R.string.user_needs_questionnaire_useful_info_label),
                SurveyQuestionnaireItem(
                    b5dId, b5dId,
                    getString(R.string.user_needs_questionnaire_screen_question_b5_text) + " \n\n  - " + getString(
                        R.string.user_needs_questionnaire_screen_question_b5d_text
                    ),
                    true,
                    resources.getStringArray(R.array.user_needs_variants_userful)
                )
            )
        )
        fragmentsPagerAdapter.addFragment(
            QuestionnaireGradationQuestion.newInstance(
                getString(R.string.user_needs_questionnaire_useful_info_label),
                SurveyQuestionnaireItem(
                    b5eId, b5eId,
                    getString(R.string.user_needs_questionnaire_screen_question_b5_text) + " \n\n  - " + getString(
                        R.string.user_needs_questionnaire_screen_question_b5e_text
                    ),
                    true,
                    resources.getStringArray(R.array.user_needs_variants_userful)
                )
            )
        )
        fragmentsPagerAdapter.addFragment(
            QuestionnaireGradationQuestion.newInstance(
                getString(R.string.user_needs_questionnaire_useful_info_label),
                SurveyQuestionnaireItem(
                    b6Id, b6Id,
                    getString(R.string.user_needs_questionnaire_screen_question_b6_text),
                    true,
                    resources.getStringArray(R.array.user_needs_variants_userful)
                )
            )
        )
        fragmentsPagerAdapter.addFragment(
            QuestionnaireGradationQuestion.newInstance(
                getString(R.string.user_needs_questionnaire_useful_info_label),
                SurveyQuestionnaireItem(
                    b7Id, b7Id,
                    getString(R.string.user_needs_questionnaire_screen_question_b7_text),
                    true,
                    resources.getStringArray(R.array.user_needs_variants_userful)
                )
            )
        )
        fragmentsPagerAdapter.addFragment(
            QuestionnaireRadioButtonQuestion.newInstance(
                SurveyQuestionnaireItem(
                    b8aId, b8aId,
                    getString(R.string.user_needs_questionnaire_screen_question_b8_text) + " \n\n  - " + getString(
                        R.string.user_needs_questionnaire_screen_question_b8a_text
                    ),
                    true,
                    resources.getStringArray(R.array.user_needs_variants_yes_no)
                )
            )
        )
        fragmentsPagerAdapter.addFragment(
            QuestionnaireRadioButtonQuestion.newInstance(
                SurveyQuestionnaireItem(
                    b8bId, b8bId,
                    getString(R.string.user_needs_questionnaire_screen_question_b8_text) + " \n\n  - " + getString(
                        R.string.user_needs_questionnaire_screen_question_b8b_text
                    ),
                    true,
                    resources.getStringArray(R.array.user_needs_variants_yes_no)
                )
            )
        )
        fragmentsPagerAdapter.addFragment(
            QuestionnaireRadioButtonQuestion.newInstance(
                SurveyQuestionnaireItem(
                    b8cId, b8cId,
                    getString(R.string.user_needs_questionnaire_screen_question_b8_text) + " \n\n  - " + getString(
                        R.string.user_needs_questionnaire_screen_question_b8c_text
                    ),
                    true,
                    resources.getStringArray(R.array.user_needs_variants_yes_no)
                )
            )
        )
        fragmentsPagerAdapter.addFragment(
            QuestionnaireRadioButtonQuestion.newInstance(
                SurveyQuestionnaireItem(
                    b8dId, b8dId,
                    getString(R.string.user_needs_questionnaire_screen_question_b8_text) + " \n\n  - " + getString(
                        R.string.user_needs_questionnaire_screen_question_b8d_text
                    ),
                    true,
                    resources.getStringArray(R.array.user_needs_variants_yes_no)
                )
            )
        )
        fragmentsPagerAdapter.addFragment(
            QuestionnaireRadioButtonQuestion.newInstance(
                SurveyQuestionnaireItem(
                    b8eId, b8eId,
                    getString(R.string.user_needs_questionnaire_screen_question_b8_text) + " \n\n  - " + getString(
                        R.string.user_needs_questionnaire_screen_question_b8e_text
                    ),
                    true,
                    resources.getStringArray(R.array.user_needs_variants_yes_no)
                )
            )
        )
        fragmentsPagerAdapter.addFragment(
            QuestionnaireRadioButtonQuestion.newInstance(
                SurveyQuestionnaireItem(
                    b9aId, b9aId,
                    getString(R.string.user_needs_questionnaire_screen_question_b9_text) + " \n\n  - " + getString(
                        R.string.user_needs_questionnaire_screen_question_b9a_text
                    ),
                    true,
                    resources.getStringArray(R.array.user_needs_variants_yes_no)
                )
            )
        )
        fragmentsPagerAdapter.addFragment(
            QuestionnaireRadioButtonQuestion.newInstance(
                SurveyQuestionnaireItem(
                    b9bId, b9bId,
                    getString(R.string.user_needs_questionnaire_screen_question_b9_text) + " \n\n  - " + getString(
                        R.string.user_needs_questionnaire_screen_question_b9b_text
                    ),
                    true,
                    resources.getStringArray(R.array.user_needs_variants_yes_no)
                )
            )
        )
        fragmentsPagerAdapter.addFragment(
            QuestionnaireRadioButtonQuestion.newInstance(
                SurveyQuestionnaireItem(
                    b9cId, b9cId,
                    getString(R.string.user_needs_questionnaire_screen_question_b9_text) + " \n\n  - " + getString(
                        R.string.user_needs_questionnaire_screen_question_b9c_text
                    ),
                    true,
                    resources.getStringArray(R.array.user_needs_variants_yes_no)
                )
            )
        )
        fragmentsPagerAdapter.addFragment(
            QuestionnaireRadioButtonQuestion.newInstance(
                SurveyQuestionnaireItem(
                    b9dId, b9dId,
                    getString(R.string.user_needs_questionnaire_screen_question_b9_text) + " \n\n  - " + getString(
                        R.string.user_needs_questionnaire_screen_question_b9d_text
                    ),
                    true,
                    resources.getStringArray(R.array.user_needs_variants_yes_no)
                )
            )
        )
        fragmentsPagerAdapter.addFragment(
            QuestionnaireRadioButtonQuestion.newInstance(
                SurveyQuestionnaireItem(
                    b9eId, b9eId,
                    getString(R.string.user_needs_questionnaire_screen_question_b9_text) + " \n\n  - " + getString(
                        R.string.user_needs_questionnaire_screen_question_b9e_text
                    ),
                    true,
                    resources.getStringArray(R.array.user_needs_variants_yes_no)
                )
            )
        )
        fragmentsPagerAdapter.addFragment(
            QuestionnaireGradationQuestion.newInstance(
                getString(R.string.user_needs_questionnaire_useful_info_label),
                SurveyQuestionnaireItem(
                    b10Id, b10Id,
                    getString(R.string.user_needs_questionnaire_screen_question_b10_text),
                    true,
                    resources.getStringArray(R.array.user_needs_variants_userful)
                )
            )
        )
        fragmentsPagerAdapter.addFragment(
            QuestionnaireGradationQuestion.newInstance(
                getString(R.string.user_needs_questionnaire_useful_info_label),
                SurveyQuestionnaireItem(
                    b11Id, b11Id,
                    getString(R.string.user_needs_questionnaire_screen_question_b11_text),
                    true,
                    resources.getStringArray(R.array.user_needs_variants_userful)
                )
            )
        )
        fragmentsPagerAdapter.addFragment(
            QuestionnaireGradationQuestion.newInstance(
                getString(R.string.user_needs_questionnaire_useful_info_label),
                SurveyQuestionnaireItem(
                    b12Id, b12Id,
                    getString(R.string.user_needs_questionnaire_screen_question_b12_text),
                    true,
                    resources.getStringArray(R.array.user_needs_variants_userful)
                )
            )
        )
        fragmentsPagerAdapter.addFragment(
            QuestionnaireGradationQuestion.newInstance(
                getString(R.string.user_needs_questionnaire_useful_info_label),
                SurveyQuestionnaireItem(
                    b13Id, b13Id,
                    getString(R.string.user_needs_questionnaire_screen_question_b13_text),
                    true,
                    resources.getStringArray(R.array.user_needs_variants_userful)
                )
            )
        )
        fragmentsPagerAdapter.addFragment(
            QuestionnaireEndScreen.newInstance()
        )
    }
}
