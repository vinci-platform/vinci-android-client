/*
 *  Copyright (c) 2020, Comtrade
 *  All rights reserved.
 *
 *  Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of the <organization> nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *   DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  Package: com.comtrade.vinciapp.view.widget.addNewDevice.view
 *  File: AddNewDeviceFragment.kt
 *  Author: Sofija Andjelkovic <sofija.andjelkovic@comtrade.com>
 *
 *  Description: a two-button fragment for different types of adding a new device
 *
 *  History: 8/11/20 Sofija  Initial code
 *
 *
 */

package com.comtrade.vinciapp.view.widget.addNewDevice.view

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain_ui.state.RequestUIState
import com.comtrade.feature_devices.ui.fitbit.FitBitUrlViewModel
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.VinciApp
import com.comtrade.vinciapp.base.BaseFragment
import com.comtrade.vinciapp.databinding.FragmentAddNewDeviceBinding
import com.comtrade.vinciapp.view.widget.addNewDevice.core.AddNewDevicePresenter
import com.comtrade.vinciapp.view.widget.addNewDevice.core.contract.AddNewDeviceContract
import javax.inject.Inject

class AddNewDeviceFragment : BaseFragment(), View.OnClickListener {

    private var _binding: FragmentAddNewDeviceBinding? = null
    private val binding get() = _binding!!

    private val getFitBitUrlViewModel by viewModels<FitBitUrlViewModel> {
        (requireContext().applicationContext as VinciApp).getDevicesComponent()
            .provideFitBitUrlViewModelFactory()
    }

    private val presenter: AddNewDeviceContract.Presenter by lazy {
        AddNewDevicePresenter()
    }

    @Inject
    lateinit var prefs: IPrefs

    companion object {
        const val TAG: String = "AddNewDeviceFragment"

        fun newInstance(): AddNewDeviceFragment {
            return AddNewDeviceFragment()
        }
    }

    @SuppressLint("VisibleForTests")
    override fun onAttach(context: Context) {
        super.onAttach(context)
        (context.applicationContext as VinciApp).getAppComponent()
            .inject(this@AddNewDeviceFragment)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentAddNewDeviceBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListeners()
        observeFitBitAuthorization()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun setListeners() {
        binding.addNewDeviceEnterIdButton.setOnClickListener(this)
        binding.addNewDeviceConnectToFitbitButton.setOnClickListener(this)
        binding.addNewDeviceSettingsButton.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
        when (view) {
            binding.addNewDeviceEnterIdButton -> {
                prefs.isDeviceAdded = true
                presenter.openDeviceSetupScreen(requireActivity().supportFragmentManager)
            }
            binding.addNewDeviceConnectToFitbitButton -> {
                if (prefs.fitBitDevice != null &&
                    prefs.fitBitDevice!!.active
                ) {
                    presenter.openFitBitScreen(requireActivity().supportFragmentManager)
                } else {
                    getFitBitUrlViewModel.getFitbitURL()
                }
            }
            binding.addNewDeviceSettingsButton -> {
                presenter.openSettingsScreen(requireActivity().supportFragmentManager)
            }
        }
    }

    /**
     * Observe the FitBit Url authorization possible UI states.
     */
    private fun observeFitBitAuthorization() {
        getFitBitUrlViewModel.getFitBitUrlObserve.observe(viewLifecycleOwner) {
            when (it) {
                RequestUIState.DefaultState -> {}
                is RequestUIState.OnError -> {
                    onFitBitAuthorizationFailure()
                    getFitBitUrlViewModel.resetState()
                }
                RequestUIState.OnLoading -> {}
                is RequestUIState.OnSuccess -> {
                    onFitBitAuthorizationSuccess(it.data.url)
                    getFitBitUrlViewModel.resetState()
                }
            }
        }
    }

    private fun onFitBitAuthorizationSuccess(authorizationUrl: String) {
        prefs.addLog("Received Fitbit URL: $authorizationUrl")
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(authorizationUrl))
        activity?.startActivity(intent)
    }

    private fun onFitBitAuthorizationFailure() {
        prefs.addLog("Error receiving Fitbit URL", true)
        showShortToast(getString(R.string.shared_unexpected_error_title))
    }
}
