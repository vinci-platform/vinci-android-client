/*
 *  Copyright (c) 2020, Comtrade
 *  All rights reserved.
 *
 *  Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of the <organization> nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *   DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  Package: com.comtrade.vinciapp.view.widget.connectedDeviceList.view.adapter
 *  File: ConnectedDeviceListAdapter.kt
 *  Author: Sofija Andjelkovic <sofija.andjelkovic@comtrade.com>
 *
 *  Description: connected device list adapter
 *
 *  History: 9/16/20 Sofija  Initial code
 *
 *
 */

package com.comtrade.vinciapp.view.widget.connectedDeviceList.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.entities.devices.CmdDevice
import com.comtrade.domain.entities.devices.ShoeDevice
import com.comtrade.domain.entities.devices.data.UpdateDeviceRequest
import com.comtrade.domain.settings.SupportedDeviceTypes
import com.comtrade.domain.settings.UnRemovableDevices
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.view.widget.connectedDeviceList.view.ConnectedDeviceListEntity

class ConnectedDeviceListAdapter(
    private val list: MutableList<ConnectedDeviceListEntity>,
    private val prefs: IPrefs,
    private val removeConnectedDeviceListener: IRemoveConnectedDeviceListener
) :
    RecyclerView.Adapter<ConnectedDeviceListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ConnectedDeviceListAdapter.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(inflater, parent)
    }

    override fun onBindViewHolder(holder: ConnectedDeviceListAdapter.ViewHolder, position: Int) {
        val devName = list[position].userDevice.deviceName
        val devAddress = list[position].userDevice.deviceUUID
        val pairFlag = list[position].userDevice.active
        val deviceType = list[position].deviceTypeName
        holder.bind(devName, devAddress, pairFlag, deviceType)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun removeItemAtPosition(itemPosition: Int): Boolean {
        if (itemPosition in 0 until list.size) {
            list.removeAt(itemPosition)
            notifyItemRemoved(itemPosition)
            notifyItemRangeChanged(itemPosition, list.size)
        }
        return false
    }

    inner class ViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
        RecyclerView.ViewHolder(inflater.inflate(R.layout.connected_device_item, parent, false)),
        View.OnClickListener {

        private var deviceName: TextView =
            itemView.findViewById(R.id.connected_device_item_device_name_text)
        private var deviceMacAddress: TextView =
            itemView.findViewById(R.id.connected_device_item_device_mac_address_text)
        private var deviceType: TextView =
            itemView.findViewById(R.id.connected_device_item_device_type_text)

        private var markImage: ImageView = itemView.findViewById(R.id.connected_device_image)
        private var removeDeviceButton: Button =
            itemView.findViewById(R.id.connected_device_item_button)

        init {
            removeDeviceButton.setOnClickListener(this)
        }

        fun bind(name: String, macAddress: String, pair: Boolean, type: String) {
            deviceName.text = name
            deviceMacAddress.text = macAddress
            if (pair) {
                markImage.setImageResource(R.drawable.ic_check_mark)
            } else {
                markImage.setImageResource(R.drawable.ic_cross_mark)
            }

            if (type == UnRemovableDevices.SURVEY_DEVICE || type == UnRemovableDevices.FITBIT_WATCH) {
                removeDeviceButton.visibility = View.INVISIBLE
            } else {
                removeDeviceButton.visibility = View.VISIBLE
            }
            deviceType.text = type
        }

        override fun onClick(view: View?) {
            when (view) {
                removeDeviceButton -> {
                    when (val device = list[layoutPosition].userDevice) {
                        is ShoeDevice -> {
                            removeConnectedDeviceListener.onShoeDeviceRemoved(
                                UpdateDeviceRequest(
                                    device.deviceId,
                                    device.deviceName,
                                    device.toString(),
                                    device.deviceUUID,
                                    SupportedDeviceTypes.SHOE.name,
                                    false,
                                    prefs.userExtraId
                                ),
                                layoutPosition
                            )
                        }
                        is CmdDevice -> {
                            removeConnectedDeviceListener.onCMDDeviceRemoved(
                                UpdateDeviceRequest(
                                    device.deviceId,
                                    device.deviceName,
                                    device.description,
                                    device.deviceUUID,
                                    SupportedDeviceTypes.WATCH.name,
                                    false,
                                    prefs.userExtraId
                                ),
                                layoutPosition
                            )
                        }
                    }
                }
            }
        }
    }

    interface IRemoveConnectedDeviceListener {
        fun onCMDDeviceRemoved(updateDeviceRequest: UpdateDeviceRequest, layoutPosition: Int)
        fun onShoeDeviceRemoved(updateDeviceRequest: UpdateDeviceRequest, layoutPosition: Int)
    }
}
