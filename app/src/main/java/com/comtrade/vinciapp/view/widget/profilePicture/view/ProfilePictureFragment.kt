/*
 *  Copyright (c) 2020, Comtrade
 *  All rights reserved.
 *
 *  Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of the <organization> nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *   DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  Package: com.comtrade.vinciapp.view.widget.profilePicture.view
 *  File: ProfilePictureFragment.kt
 *  Author: Milenko Bojanic <milenko.bojanic@comtrade.com>
 *
 *  Description: Fragment screen for creating profile picture
 *
 *  History: 8/11/20 Milenko  Initial code
 *
 *
 */

package com.comtrade.vinciapp.view.widget.profilePicture.view

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.di.AppQualifiers
import com.comtrade.domain_ui.state.RequestUIState
import com.comtrade.feature_account.ui.user.UpdateUserProfileImageViewModel
import com.comtrade.feature_account.util.BitmapPictureUtil
import com.comtrade.feature_account.util.BitmapPictureUtil.saveImage
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.VinciApp
import com.comtrade.vinciapp.base.BaseFragment
import com.comtrade.vinciapp.databinding.FragmentProfilePictureBinding
import com.comtrade.vinciapp.util.AlertDialogUtils
import com.comtrade.vinciapp.view.activity.main.view.MainActivity
import java.io.File
import javax.inject.Inject
import javax.inject.Named

class ProfilePictureFragment : BaseFragment(), View.OnClickListener {

    companion object {
        const val TAG: String = "ProfilePictureFragment"
        const val CAMERA_PERMISSION = Manifest.permission.CAMERA

        fun newInstance(): ProfilePictureFragment {
            return ProfilePictureFragment()
        }
    }

    private var _binding: FragmentProfilePictureBinding? = null
    private val binding get() = _binding!!

    private var transformedImageBitmap: Bitmap? = null
    private var profileImageSource: String? = null

    @Inject
    lateinit var prefs: IPrefs

    @Inject
    @Named(AppQualifiers.ROOT_IMAGE_DIRECTORY)
    lateinit var directory: String

    private val updateProfileImageViewModel: UpdateUserProfileImageViewModel by viewModels {
        (requireContext().applicationContext as VinciApp).getAccountComponent()
            .provideUpdateUserProfileImageViewModelFactory()
    }

    private val cameraPermissionRequest =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) {
            if (!it) {
                AlertDialogUtils.showInfoAlertDialog(
                    this.requireContext(),
                    getString(R.string.profile_picture_screen_permission_denied_alert_title),
                    getString(R.string.profile_picture_screen_permission_denied_alert_message)
                )
            } else {
                dispatchTakePictureIntent()
            }
        }

    private val takeCameraImageActionResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            if (it.resultCode == Activity.RESULT_OK) {
                it.data?.let { data ->
                    val imageBitmap = data.extras?.get("data") as Bitmap
                    val matrix = Matrix()
                    transformedImageBitmap = Bitmap.createBitmap(
                        imageBitmap,
                        0,
                        0,
                        imageBitmap.width,
                        imageBitmap.height,
                        matrix,
                        true
                    )
                    transformedImageBitmap?.let { image ->
                        loadProfileImage(image)
                    }
                    profileImageSource = "CAPTURE"
                }
            }
        }

    private val imagePickActionResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            if (it.resultCode == Activity.RESULT_OK) {
                it.data?.data?.let { uri ->
                    val input = requireContext().contentResolver.openInputStream(uri)
                    transformedImageBitmap = BitmapFactory.decodeStream(input)
                    transformedImageBitmap?.let { image ->
                        loadProfileImage(image)
                    }
                    profileImageSource = "GALLERY"
                }
            }
        }

    @SuppressLint("VisibleForTests")
    override fun onAttach(context: Context) {
        super.onAttach(context)
        (context.applicationContext as VinciApp).appComponent.inject(this@ProfilePictureFragment)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentProfilePictureBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (prefs.userProfilePicture.isNotEmpty()) {
            loadExistingProfileImage(prefs.userProfilePicture)
        }
        setListeners()
        observeProfileImageUpdate()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun setListeners() {
        binding.profilePictureFragmentChooseExistingButton.setOnClickListener(this)
        binding.profilePictureFragmentTakePictureButton.setOnClickListener(this)
        binding.profilePictureFragmentSaveProfilePictureButton.setOnClickListener(this)
        binding.profilePictureFragmentDeleteProfilePictureButton.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
        when (view) {
            binding.profilePictureFragmentChooseExistingButton -> {
                dispatchPhotoPickerIntent()
            }
            binding.profilePictureFragmentTakePictureButton -> {
                if (isCameraPermissionGranted()) {
                    dispatchTakePictureIntent()
                } else {
                    makeCameraPermissionRequest()
                }
            }
            binding.profilePictureFragmentSaveProfilePictureButton -> {
                val fileName = "vinci_profile_image_${prefs.userId}.jpg"
                prefs.userProfilePicture = saveImage(
                    directory,
                    fileName,
                    transformedImageBitmap!!
                )

                // Execute API call
                updateProfileImageViewModel.saveImage(File(prefs.userProfilePicture))
            }
            binding.profilePictureFragmentDeleteProfilePictureButton -> {
                BitmapPictureUtil.deleteProfileImage(prefs)
                (activity as MainActivity).updateNavigationBar()
                activity?.supportFragmentManager?.popBackStack()
            }
        }
    }

    private fun observeProfileImageUpdate() {
        updateProfileImageViewModel.updateUserProfileImageObserve.observe(viewLifecycleOwner) {
            when (it) {
                RequestUIState.DefaultState -> {}
                is RequestUIState.OnError -> {
                    showShortToast(getString(R.string.profile_picture_screen_error_save_picture))
                    it.message?.let { message ->
                        Log.d(TAG, message)
                    }
                    updateProfileImageViewModel.resetState()
                }
                RequestUIState.OnLoading -> {}
                is RequestUIState.OnSuccess -> {
                    onSaveImageSuccess()
                    updateProfileImageViewModel.resetState()

                    (activity as MainActivity).updateNavigationBar()
                    activity?.supportFragmentManager?.popBackStack()
                }
            }
        }
    }

    private fun dispatchPhotoPickerIntent() {
        val photoPickerIntent = Intent(Intent.ACTION_PICK)
        photoPickerIntent.type = "image/*"
        imagePickActionResult.launch(photoPickerIntent)
    }

    private fun isCameraPermissionGranted(): Boolean {
        val permission = ContextCompat.checkSelfPermission(this.requireContext(), CAMERA_PERMISSION)
        return permission == PackageManager.PERMISSION_GRANTED
    }

    private fun makeCameraPermissionRequest() {
        cameraPermissionRequest.launch(CAMERA_PERMISSION)
    }

    private fun dispatchTakePictureIntent() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            activity?.packageManager?.let {
                takePictureIntent.resolveActivity(it)?.also {
                    takeCameraImageActionResult.launch(takePictureIntent)
                }
            }
        }
    }

    /**
     * Load saved User profile image.
     */
    private fun loadExistingProfileImage(imagePath: String) {
        Glide.with(requireContext())
            .load(imagePath)
            .apply(
                RequestOptions().diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
            )
            .centerCrop()
            .into(binding.profilePictureFragmentProfileImage)
        binding.profilePictureFragmentSaveProfilePictureButton.apply {
            isEnabled = false
            isClickable = false
        }
        binding.profilePictureFragmentDeleteProfilePictureButton.apply {
            isEnabled = true
            isClickable = true
        }
    }

    /**
     * Load new profile image and set the save button enabled and delete profile image button disabled.
     */
    private fun loadProfileImage(transformedImageBitmap: Bitmap) {
        Glide.with(requireContext())
            .load(transformedImageBitmap)
            .apply(
                RequestOptions().diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
            )
            .centerCrop()
            .into(binding.profilePictureFragmentProfileImage)

        binding.profilePictureFragmentSaveProfilePictureButton.apply {
            isEnabled = true
            isClickable = true
        }
        binding.profilePictureFragmentDeleteProfilePictureButton.apply {
            isEnabled = false
            isClickable = false
        }
    }

    private fun onSaveImageSuccess() {
        showShortToast(getString(R.string.profile_picture_screen_save_picture_success))
        Log.d(TAG, "Image saved!")
    }
}
