/*
 * Copyright (c) 2020, Comtrade
 *  All rights reserved.
 *
 *  Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of the <organization> nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *   DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  Package:
 *  File: RegisterMedicationFragment.kt
 *  Author: Milenko Bojanic <milenko.bojanic@comtrade.com>
 *
 *  Description: Fragment screen for adding prescribed medication to patient users
 *
 *  History: 9/8/20 Milenko Initial code
 */

package com.comtrade.vinciapp.view.widget.registerMedication.view

import android.app.DatePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.comtrade.vinciapp.AppConstants
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.base.BaseFragment
import com.comtrade.vinciapp.databinding.FragmentRegisterMedicationBinding
import com.comtrade.vinciapp.util.CalendarDateUtils
import com.comtrade.vinciapp.util.SimpleTextWatcher
import com.comtrade.vinciapp.view.widget.registerMedication.core.RegisterMedicationPresenter
import com.comtrade.vinciapp.view.widget.registerMedication.core.contract.RegisterMedicationContract
import com.comtrade.vinciapp.view.widget.registerMedication.view.adapter.MedicationFrequency
import com.comtrade.vinciapp.view.widget.registerMedication.view.adapter.Patients
import java.util.*

class RegisterMedicationFragment : BaseFragment(), RegisterMedicationContract.View,
    View.OnClickListener, AdapterView.OnItemSelectedListener {

    private var _binding: FragmentRegisterMedicationBinding? = null
    private val binding get() = _binding!!


    private lateinit var medicationFrequencyAdapter: ArrayAdapter<String>
    private lateinit var medicationFrequency: String
    private lateinit var patientsAdapter: ArrayAdapter<String>
    private lateinit var patient: String
    private lateinit var startDate: Date
    private lateinit var endDate: Date
    private var editTextWatcher = EditTextWatcher()

    private val presenter: RegisterMedicationContract.Presenter by lazy {
        RegisterMedicationPresenter()
    }

    companion object {
        const val TAG: String = "RegisterMedicationFragment"

        fun newInstance(): RegisterMedicationFragment {
            return RegisterMedicationFragment()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentRegisterMedicationBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListeners()
        addMedicationFrequency()
        addPatients()
        addTextWatchers()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun setListeners() {
        binding.registerMedicationFragmentStartDateSpinner.setOnClickListener(this)
        binding.registerMedicationFragmentEndDateSpinner.setOnClickListener(this)
        binding.registerMedicationFragmentAddMedicationButton.setOnClickListener(this)
        binding.registerMedicationFragmentFrequencySpinner.onItemSelectedListener = this
        binding.registerMedicationFragmentPatientsSpinner.onItemSelectedListener = this
    }

    private fun addMedicationFrequency() {
        medicationFrequencyAdapter = ArrayAdapter(requireContext(), R.layout.simple_spinner_item)
        medicationFrequencyAdapter.add(MedicationFrequency.ONE.number.toString())
        medicationFrequencyAdapter.add(MedicationFrequency.TWO.number.toString())
        medicationFrequencyAdapter.add(MedicationFrequency.THREE.number.toString())
        medicationFrequencyAdapter.add(MedicationFrequency.FOUR.number.toString())
        medicationFrequencyAdapter.add(MedicationFrequency.SIX.number.toString())
        medicationFrequencyAdapter.add(MedicationFrequency.EIGHT.number.toString())
        medicationFrequencyAdapter.add(MedicationFrequency.TWELVE.number.toString())
        medicationFrequencyAdapter.add(MedicationFrequency.SIXTEEN.number.toString())
        medicationFrequencyAdapter.add(MedicationFrequency.TWENTY_FOUR.number.toString())
        binding.registerMedicationFragmentFrequencySpinner.adapter = medicationFrequencyAdapter
    }

    private fun addPatients() {
        patientsAdapter = ArrayAdapter(requireContext(), R.layout.simple_spinner_item)
        patientsAdapter.add(Patients.JOHN_DOE.patientName)
        patientsAdapter.add(Patients.JANE_DOE.patientName)
        patientsAdapter.add(Patients.WALTER_WHITE.patientName)
        binding.registerMedicationFragmentPatientsSpinner.adapter = patientsAdapter
    }

    private fun addTextWatchers() {
        binding.registerMedicationFragmentMedicationEditText.addTextChangedListener(editTextWatcher)
        binding.registerMedicationFragmentInstructionsEditText.addTextChangedListener(
            editTextWatcher
        )
    }

    private fun showStartDateCalendar() {
        val calendar = Calendar.getInstance()
        val year = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH)
        val day = calendar.get(Calendar.DAY_OF_MONTH)

        val datePickerDialog = DatePickerDialog(
            this.requireContext(),
            { _, selectedYear, selectedMonth, selectedDay ->
                val displayDate = "$selectedDay. $selectedMonth. $selectedYear."
                startDate =
                    CalendarDateUtils.getDateFromCalendar(selectedDay, selectedMonth, selectedYear)
                binding.registerMedicationFragmentStartDateSpinner.text = displayDate
                binding.registerMedicationFragmentAddMedicationButton.isEnabled = isInputValid()
                val cal = Calendar.getInstance()
                cal.set(Calendar.YEAR, selectedYear)
            },
            year,
            month,
            day
        )
        datePickerDialog.show()
    }

    private fun showEndDateCalendar() {
        val calendar = Calendar.getInstance()
        val year = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH)
        val day = calendar.get(Calendar.DAY_OF_MONTH)

        val datePickerDialog = DatePickerDialog(
            this.requireContext(),
            { _, selectedYear, selectedMonth, selectedDay ->
                val displayDate = "$selectedDay. $selectedMonth. $selectedYear."
                endDate =
                    CalendarDateUtils.getDateFromCalendar(selectedDay, selectedMonth, selectedYear)
                binding.registerMedicationFragmentEndDateSpinner.text = displayDate
                binding.registerMedicationFragmentAddMedicationButton.isEnabled = isInputValid()
            },
            year,
            month,
            day
        )
        datePickerDialog.show()
    }

    private fun resetMedicationInput() {
        binding.registerMedicationFragmentMedicationEditText.text.clear()
        binding.registerMedicationFragmentInstructionsEditText.text.clear()
        binding.registerMedicationFragmentStartDateSpinner.text = AppConstants.EMPTY_STRING
        binding.registerMedicationFragmentEndDateSpinner.text = AppConstants.EMPTY_STRING
        binding.registerMedicationFragmentAddMedicationButton.isEnabled = false
    }

    override fun onClick(view: View?) {
        when (view) {
            binding.registerMedicationFragmentAddMedicationButton -> {
                if (endDate.before(startDate)) {
                    showShortToast(getString(R.string.register_medication_screen_date_wrong_toast))
                } else {
                    // TODO: Missing implementation
                    showShortToast(getString(R.string.register_medication_screen_medication_successfully_added_toast))
                    resetMedicationInput()
                }
            }
            binding.registerMedicationFragmentStartDateSpinner -> {
                showStartDateCalendar()
            }
            binding.registerMedicationFragmentEndDateSpinner -> {
                showEndDateCalendar()
            }
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        when (view) {
            binding.registerMedicationFragmentFrequencySpinner -> {
                medicationFrequency = parent?.getItemAtPosition(position).toString()
            }
            binding.registerMedicationFragmentPatientsSpinner -> {
                patient = parent?.getItemAtPosition(position).toString()
            }
        }
    }

    private fun isInputValid(): Boolean {
        return binding.registerMedicationFragmentMedicationEditText.text.isNotEmpty() &&
                binding.registerMedicationFragmentInstructionsEditText.text.isNotEmpty() &&
                binding.registerMedicationFragmentStartDateSpinner.text.isNotEmpty() &&
                binding.registerMedicationFragmentEndDateSpinner.text.isNotEmpty()
    }

    inner class EditTextWatcher : SimpleTextWatcher() {
        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            super.onTextChanged(s, start, before, count)
            binding.registerMedicationFragmentAddMedicationButton.isEnabled = isInputValid()
        }
    }
}