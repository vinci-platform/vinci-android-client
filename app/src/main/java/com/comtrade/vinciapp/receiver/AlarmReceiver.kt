/*
 * Copyright (c) 2020, Comtrade
 *  All rights reserved.
 *
 *  Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of the <organization> nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *   DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  Package:
 *  File: AlarmReceiver.kt
 *  Author:
 *
 *  Description:
 *
 *  History: 8/25/20 12:49 PM <name>  Initial code
 */

package com.comtrade.vinciapp.receiver

import android.app.NotificationManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import androidx.core.content.ContextCompat
import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.vinciapp.VinciApp
import com.comtrade.vinciapp.util.sendNotification
import javax.inject.Inject

class AlarmReceiver : BroadcastReceiver() {

    @Inject
    lateinit var prefs: IPrefs

    companion object {
        const val MIN_SNOOZE_KEY = "minSnooze"
        const val NEXT_FRAGMENT_KEY = "nextFragment"
        const val NOTIFICATION_TEXT_KEY = "notificationText"
    }

    override fun onReceive(context: Context, intent: Intent) {
        (context.applicationContext as VinciApp).appComponent.inject(this@AlarmReceiver)
        val snoozeMin = intent.getIntExtra(MIN_SNOOZE_KEY, 15)
        val fragment = intent.getStringExtra(NEXT_FRAGMENT_KEY)
        val notificationText = intent.getStringExtra(NOTIFICATION_TEXT_KEY)
        val notificationManager = ContextCompat.getSystemService(
            context,
            NotificationManager::class.java
        ) as NotificationManager
        notificationManager.sendNotification(
            notificationText,
            context,
            snoozeMin,
            fragment,
            prefs
        )
    }
}