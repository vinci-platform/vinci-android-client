/*
 *  Copyright (c) 2020, Comtrade
 *  All rights reserved.
 *
 *  Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of the <organization> nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *   DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  Package: com.comtrade.vinciapp
 *  File: VinciApp.kt
 *  Author: Milenko Bojanic <milenko.bojanic@comtrade.com>
 *
 *  Description: Application context class
 *
 *  History: 8/11/20 Milenko  Initial code
 *
 *
 */

package com.comtrade.vinciapp

import android.app.Application
import androidx.annotation.VisibleForTesting
import com.comtrade.data.di.AppDataModule
import com.comtrade.feature_about_dependencies.di.AboutDependenciesComponent
import com.comtrade.feature_about_dependencies.di.AboutDependenciesModule
import com.comtrade.feature_about_dependencies.di.DaggerAboutDependenciesComponent
import com.comtrade.feature_account.di.AccountComponent
import com.comtrade.feature_devices.di.DevicesComponent
import com.comtrade.feature_events.di.EventsComponent
import com.comtrade.feature_friends.di.FriendsComponent
import com.comtrade.feature_health_records.di.HealthRecordsComponent
import com.comtrade.feature_questionnaire.di.QuestionnaireModule
import com.comtrade.feature_steps.di.StepsComponent
import com.comtrade.vinciapp.di.AppComponent
import com.comtrade.vinciapp.di.AppModule
import com.comtrade.vinciapp.di.DaggerAppComponent
import com.vinci.feature_sync.di.SyncServiceComponent
import com.vinci.feature_sync.di.SyncServicesModule

/**
 * The application entry point.
 */
@VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
open class VinciApp : Application() {

    open val appComponent: AppComponent by lazy {
        DaggerAppComponent.builder()
            .appModule(AppModule(this@VinciApp))
            .appDataModule(AppDataModule(this@VinciApp))
            .questionnaireModule(QuestionnaireModule())
            .build()
    }
    private val accountComponent: AccountComponent by lazy {
        appComponent.accountComponentFactory.create()
    }
    private val eventsComponent: EventsComponent by lazy {
        appComponent.eventsComponentFactory.create()
    }
    private val healthRecordsComponent: HealthRecordsComponent by lazy {
        appComponent.healthRecordsComponentFactory.create()
    }
    private val aboutDependenciesComponent: AboutDependenciesComponent by lazy {
        DaggerAboutDependenciesComponent.builder()
            .aboutDependenciesModule(AboutDependenciesModule(this@VinciApp)).build()
    }
    private val devicesComponent: DevicesComponent by lazy {
        appComponent.devicesComponentFactory.create()
    }
    private val stepsComponent: StepsComponent by lazy {
        appComponent.stepsComponentFactory.create()
    }
    private val friendsComponent: FriendsComponent by lazy {
        appComponent.friendsComponentFactory.create()
    }
    private val syncComponent: SyncServiceComponent by lazy {
        appComponent.syncComponentBuilder.appSyncServicesModule(
            SyncServicesModule(
                eventsComponent.provideSyncUserEventsToVinciUseCase(),
                devicesComponent.provideSyncFitBitFromServerUseCase(),
                devicesComponent.provideSyncCMDFromServerUseCase()
            )
        ).build()
    }

    internal fun getAppComponent(): AppComponent = appComponent

    internal fun getAboutDependenciesComponent(): AboutDependenciesComponent =
        aboutDependenciesComponent

    internal open fun getAccountComponent(): AccountComponent = accountComponent

    internal fun getEventsComponent(): EventsComponent = eventsComponent

    internal fun getHealthRecordsComponent(): HealthRecordsComponent = healthRecordsComponent

    internal fun getDevicesComponent(): DevicesComponent = devicesComponent

    internal fun getStepsComponent(): StepsComponent = stepsComponent

    internal fun getFriendsComponent(): FriendsComponent = friendsComponent

    internal fun getSyncServiceComponent(): SyncServiceComponent = syncComponent
}
