/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: ConnectionMonitorViewModel.kt
 * Author: sdelic
 *
 * History:  23.6.22. 15:11 created
 */

package com.comtrade.vinciapp.connection

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import android.os.Build
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import javax.inject.Inject

/**
 * Android ViewModel component for monitoring and updating the network connectivity status.
 */
class ConnectionMonitorViewModel @Inject constructor(application: Application) : ViewModel() {

    private var connectivityManager: ConnectivityManager
    private var networkCallback: ConnectivityManager.NetworkCallback
    val networkConnectionObserve: MutableLiveData<NetworkStateUI> = MutableLiveData()

    init {
        //Request configuration
        val networkRequest =
            NetworkRequest.Builder().addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
                .addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR)
                .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
                .addTransportType(NetworkCapabilities.TRANSPORT_ETHERNET) // For AndroidTV or similar device.
                .build()

        //Network callback
        networkCallback = object : ConnectivityManager.NetworkCallback() {
            //Network available
            override fun onAvailable(network: Network) {
                super.onAvailable(network)
                networkConnectionObserve.postValue(
                    NetworkStateUI.NetworkType(
                        true,
                        NetworkDataType.UNKNOWN
                    )
                )
            }

            //Network connection lost
            override fun onLost(network: Network) {
                super.onLost(network)
                networkConnectionObserve.postValue(
                    NetworkStateUI.NetworkType(
                        false,
                        NetworkDataType.NONE
                    )
                )
            }

            //No network connection
            override fun onUnavailable() {
                super.onUnavailable()
                networkConnectionObserve.postValue(
                    NetworkStateUI.NetworkType(
                        false,
                        NetworkDataType.NONE
                    )
                )
            }

            //Network configurations switched. Consider delaying any mobile network connection in favor of WiFi or Ethernet connections
            override fun onCapabilitiesChanged(
                network: Network,
                networkCapabilities: NetworkCapabilities
            ) {
                super.onCapabilitiesChanged(network, networkCapabilities)

                var networkDataType: NetworkDataType = NetworkDataType.UNKNOWN

                when {
                    networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> networkDataType =
                        NetworkDataType.WIFI
                    networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> networkDataType =
                        NetworkDataType.ETHERNET
                    networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> networkDataType =
                        NetworkDataType.MOBILE
                    else -> NetworkDataType.UNKNOWN
                }
                networkConnectionObserve.postValue(
                    NetworkStateUI.NetworkType(
                        true,
                        networkDataType
                    )
                )
            }
        }

        //Register connection listener
        connectivityManager =
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                application.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            } else {
                application.getSystemService(ConnectivityManager::class.java) as ConnectivityManager

            }
        connectivityManager.requestNetwork(networkRequest, networkCallback)
    }

    override fun onCleared() {

        //Unregistering network connectivity callbacks when this component is being destroyed.
        try {
            connectivityManager.unregisterNetworkCallback(networkCallback)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}