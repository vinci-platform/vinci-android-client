<?xml version="1.0" encoding="utf-8"?>
<!--
  ~  Copyright (c) 2020, Comtrade
  ~  All rights reserved.
  ~
  ~  Licence: BSD-3-Clause (GPL compatible)
  ~
  ~  Redistribution and use in source and binary forms, with or without
  ~  modification, are permitted provided that the following conditions are met:
  ~      * Redistributions of source code must retain the above copyright
  ~        notice, this list of conditions and the following disclaimer.
  ~      * Redistributions in binary form must reproduce the above copyright
  ~        notice, this list of conditions and the following disclaimer in the
  ~        documentation and/or other materials provided with the distribution.
  ~      * Neither the name of the <organization> nor the
  ~        names of its contributors may be used to endorse or promote products
  ~        derived from this software without specific prior written permission.
  ~
  ~  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ~  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  ~  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  ~   DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
  ~  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  ~  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  ~  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ~  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  ~  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  ~  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  ~
  ~  Package: res.values
  ~  File: dimens.xml
  ~  Author: Milenko Bojanic <milenko.bojanic@comtrade.com>
  ~
  ~  Description: Dimensions used in the application
  ~
  ~  History: 8/11/20 Milenko  Initial code
  ~
  ~
  -->

<resources>
    <!-- Margins -->
    <dimen name="margin_4dp">4dp</dimen>
    <dimen name="margin_8dp">8dp</dimen>
    <dimen name="margin_10dp">10dp</dimen>
    <dimen name="margin_12dp">12dp</dimen>
    <dimen name="margin_15dp">15dp</dimen>
    <dimen name="margin_16dp">16dp</dimen>
    <dimen name="margin_20dp">20dp</dimen>
    <dimen name="margin_30dp">30dp</dimen>
    <dimen name="margin_32dp">32dp</dimen>
    <dimen name="margin_48dp">48dp</dimen>
    <dimen name="margin_64dp">64dp</dimen>
    <dimen name="margin_96dp">96dp</dimen>
    <dimen name="margin_128dp">128dp</dimen>

    <!-- Text -->
    <dimen name="text_70sp">70sp</dimen>
    <dimen name="text_50sp">50sp</dimen>
    <dimen name="text_36sp">36sp</dimen>
    <dimen name="text_28sp">28sp</dimen>
    <dimen name="text_26sp">26sp</dimen>
    <dimen name="text_22sp">22sp</dimen>
    <dimen name="text_20sp">20sp</dimen>
    <dimen name="text_18sp">18sp</dimen>
    <dimen name="text_16sp">16sp</dimen>
    <dimen name="text_14sp">14sp</dimen>
    <dimen name="text_12sp">12sp</dimen>

    <!-- Padding -->
    <dimen name="padding_5dp">5dp</dimen>
    <dimen name="padding_8dp">8dp</dimen>
    <dimen name="padding_12dp">12dp</dimen>
    <dimen name="padding_16dp">16dp</dimen>
    <dimen name="padding_28dp">28dp</dimen>
    <dimen name="padding_32dp">32dp</dimen>

    <!-- Rounded corners -->
    <dimen name="rounded_corners_radius">5dp</dimen>
    <dimen name="rounded_corners_spinner_radius">5dp</dimen>

    <!-- Progress bar -->
    <dimen name="progress_container">60dp</dimen>
    <dimen name="progress_radius">30dp</dimen>
    <dimen name="progress_elevation">3dp</dimen>
    <dimen name="progress_padding">5dp</dimen>
    <dimen name="progress_bar_size">55dp</dimen>

    <!-- Profile picture -->
    <dimen name="profile_picture_width">70dp</dimen>
    <dimen name="profile_picture_height">70dp</dimen>
    <dimen name="profile_picture_background_width">86dp</dimen>
    <dimen name="profile_picture_background_height">86dp</dimen>
    <dimen name="profile_picture_background_size">300dp</dimen>
    <dimen name="profile_picture_image_size">290dp</dimen>

    <!-- Navigation drawer -->
    <dimen name="navigation_drawer_icon_size">24dp</dimen>
    <dimen name="navigation_drawer_width">250dp</dimen>

    <!-- Layout -->
    <dimen name="layout_200dp">200dp</dimen>
    <dimen name="layout_180dp">180dp</dimen>
    <dimen name="layout_150dp">150dp</dimen>
    <dimen name="layout_120dp">120dp</dimen>
    <dimen name="layout_100dp">100dp</dimen>
    <dimen name="layout_80dp">80dp</dimen>
    <dimen name="layout_70dp">70dp</dimen>
    <dimen name="layout_60dp">60dp</dimen>
    <dimen name="layout_55dp">55dp</dimen>
    <dimen name="layout_40dp">40dp</dimen>
    <dimen name="layout_30dp">30dp</dimen>
    <dimen name="layout_16dp">16dp</dimen>

    <!-- WHOQOL Survey -->
    <dimen name="whoqol_button_width">100dp</dimen>
    <dimen name="whoqol_button_height">140dp</dimen>
    <dimen name="whoqol_answer_text_width">100dp</dimen>

    <!-- Connected devices -->
    <dimen name="connected_devices_border_size">39dp</dimen>
    <dimen name="connected_devices_background_size">35dp</dimen>
    <dimen name="connected_devices_icon_size">25dp</dimen>

    <!-- Navigation menu -->
    <dimen name="navigation_menu_background_height">60dp</dimen>
    <dimen name="navigation_menu_button_width">60dp</dimen>
    <dimen name="navigation_menu_button_height">60dp</dimen>

    <!-- Logo -->
    <dimen name="main_screen_logo_width">60dp</dimen>
    <dimen name="main_screen_logo_height">60dp</dimen>

    <!-- Feeling dialog -->
    <dimen name="feeling_dialog_smiley_size">60dp</dimen>

    <!-- Main screen -->
    <dimen name="main_screen_button_size">70dp</dimen>

    <!-- Add profile picture -->
    <dimen name="add_profile_picture_background_size">250dp</dimen>
    <dimen name="add_profile_picture_image_size">240dp</dimen>
</resources>
