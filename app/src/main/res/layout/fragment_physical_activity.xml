<?xml version="1.0" encoding="utf-8"?><!--
  ~  Copyright (c) 2020, Comtrade
  ~  All rights reserved.
  ~
  ~  Licence: BSD-3-Clause (GPL compatible)
  ~
  ~  Redistribution and use in source and binary forms, with or without
  ~  modification, are permitted provided that the following conditions are met:
  ~      * Redistributions of source code must retain the above copyright
  ~        notice, this list of conditions and the following disclaimer.
  ~      * Redistributions in binary form must reproduce the above copyright
  ~        notice, this list of conditions and the following disclaimer in the
  ~        documentation and/or other materials provided with the distribution.
  ~      * Neither the name of the <organization> nor the
  ~        names of its contributors may be used to endorse or promote products
  ~        derived from this software without specific prior written permission.
  ~
  ~  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ~  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  ~  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  ~   DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
  ~  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  ~  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  ~  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ~  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  ~  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  ~  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  ~
  ~  Package: res.layout
  ~  File: fragment_physical_activity.xml
  ~  Author: Kosta Eric <kosta.eric@comtrade.com>
  ~
  ~  Description: Layout for physical activity
  ~
  ~  History: 8/11/20 Kosta  Initial code
  ~
  ~
  -->

<androidx.constraintlayout.widget.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    android:layout_width="match_parent"
    android:layout_height="match_parent">

    <androidx.constraintlayout.widget.Guideline
        android:id="@+id/physical_activity_fragment_vertical_guideline_50"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:orientation="vertical"
        app:layout_constraintGuide_percent="0.50" />

    <androidx.constraintlayout.widget.Guideline
        android:id="@+id/physical_activity_fragment_horizontal_guideline_05"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:orientation="horizontal"
        app:layout_constraintGuide_percent="0.05" />

    <androidx.constraintlayout.widget.Guideline
        android:id="@+id/physical_activity_fragment_horizontal_guideline_30"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:orientation="horizontal"
        app:layout_constraintGuide_percent="0.30" />

    <androidx.constraintlayout.widget.Guideline
        android:id="@+id/physical_activity_fragment_horizontal_guideline_55"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:orientation="horizontal"
        app:layout_constraintGuide_percent="0.55" />

    <androidx.constraintlayout.widget.Guideline
        android:id="@+id/physical_activity_fragment_horizontal_guideline_80"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:orientation="horizontal"
        app:layout_constraintGuide_percent="0.80" />


    <LinearLayout
        android:id="@+id/physical_activity_fragment_step_count_container"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_marginStart="@dimen/layout_16dp"
        android:orientation="vertical"
        app:layout_constraintBottom_toTopOf="@id/physical_activity_fragment_horizontal_guideline_30"
        app:layout_constraintEnd_toStartOf="@id/physical_activity_fragment_vertical_guideline_50"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toBottomOf="@id/physical_activity_fragment_horizontal_guideline_05">

        <ImageView
            style="@style/MainScreenImageStyle"
            android:layout_width="@dimen/layout_100dp"
            android:layout_height="@dimen/layout_100dp"
            android:src="@drawable/ic_health_steps" />

        <TextView
            style="@style/MainScreenTextStyle"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="@string/main_screen_step_count_text" />

    </LinearLayout>


    <LinearLayout
        android:id="@+id/physical_activity_fragment_outdoor_activities_container"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_marginEnd="@dimen/layout_16dp"
        android:orientation="vertical"
        app:layout_constraintBottom_toTopOf="@id/physical_activity_fragment_horizontal_guideline_30"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintStart_toEndOf="@id/physical_activity_fragment_vertical_guideline_50"
        app:layout_constraintTop_toBottomOf="@id/physical_activity_fragment_horizontal_guideline_05">


        <ImageView
            style="@style/MainScreenImageStyle"
            android:layout_width="@dimen/layout_100dp"
            android:layout_height="@dimen/layout_100dp"
            android:src="@drawable/ic_health_outdoor" />

        <TextView
            style="@style/MainScreenTextStyle"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="@string/main_screen_outdoor_activities_text" />

    </LinearLayout>

    <LinearLayout
        android:id="@+id/physical_activity_fragment_heart_rate_container"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_marginStart="@dimen/layout_16dp"
        android:orientation="vertical"
        app:layout_constraintBottom_toTopOf="@id/physical_activity_fragment_horizontal_guideline_55"
        app:layout_constraintEnd_toStartOf="@id/physical_activity_fragment_vertical_guideline_50"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toBottomOf="@id/physical_activity_fragment_horizontal_guideline_30">


        <ImageView
            style="@style/MainScreenImageStyle"
            android:layout_width="@dimen/layout_100dp"
            android:layout_height="@dimen/layout_100dp"
            android:src="@drawable/ic_health_heartrate" />

        <TextView
            style="@style/MainScreenTextStyle"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="@string/main_screen_heart_rate_text" />

    </LinearLayout>

    <LinearLayout
        android:id="@+id/physical_activity_fragment_calories_container"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_marginEnd="@dimen/layout_16dp"
        android:orientation="vertical"
        app:layout_constraintBottom_toTopOf="@id/physical_activity_fragment_horizontal_guideline_55"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintStart_toEndOf="@id/physical_activity_fragment_vertical_guideline_50"
        app:layout_constraintTop_toBottomOf="@id/physical_activity_fragment_horizontal_guideline_30">


        <ImageView
            style="@style/MainScreenImageStyle"
            android:layout_width="@dimen/layout_100dp"
            android:layout_height="@dimen/layout_100dp"
            android:src="@drawable/ic_health_kcal" />

        <TextView
            style="@style/MainScreenTextStyle"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="@string/main_screen_calories_text" />

    </LinearLayout>

    <LinearLayout
        android:id="@+id/physical_activity_fragment_sleep_container"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_marginStart="@dimen/layout_16dp"
        android:orientation="vertical"
        app:layout_constraintBottom_toTopOf="@id/physical_activity_fragment_horizontal_guideline_80"
        app:layout_constraintEnd_toStartOf="@id/physical_activity_fragment_vertical_guideline_50"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toBottomOf="@id/physical_activity_fragment_horizontal_guideline_55">

        <ImageView
            style="@style/MainScreenImageStyle"
            android:layout_width="@dimen/layout_100dp"
            android:layout_height="@dimen/layout_100dp"
            android:src="@drawable/ic_health_sleep" />

        <TextView
            style="@style/MainScreenTextStyle"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="@string/main_screen_sleep_text" />

    </LinearLayout>

    <LinearLayout
        android:id="@+id/physical_activity_fragment_health_points_container"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_marginEnd="@dimen/layout_16dp"
        android:orientation="vertical"
        app:layout_constraintBottom_toTopOf="@id/physical_activity_fragment_horizontal_guideline_80"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintStart_toEndOf="@id/physical_activity_fragment_vertical_guideline_50"
        app:layout_constraintTop_toBottomOf="@id/physical_activity_fragment_horizontal_guideline_55">

        <ImageView
            style="@style/MainScreenImageStyle"
            android:layout_width="@dimen/layout_100dp"
            android:layout_height="@dimen/layout_100dp"
            android:src="@drawable/ic_health_points" />

        <TextView
            style="@style/MainScreenTextStyle"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="@string/main_screen_health_points_text" />

    </LinearLayout>

    <Button
        android:id="@+id/physical_activity_fragment_IPAQ_button"
        style="@style/BasicAppButtonStyle"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="@string/IPA_questionnaire_button_text"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toStartOf="parent"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toBottomOf="@id/physical_activity_fragment_horizontal_guideline_80" />

    <ImageView
        android:layout_width="8dp"
        android:layout_height="550dp"
        android:adjustViewBounds="false"
        android:scaleType="fitXY"
        android:src="@drawable/ic_line_vertical"
        app:layout_constraintBottom_toTopOf="@id/physical_activity_fragment_horizontal_guideline_80"
        app:layout_constraintEnd_toEndOf="@id/physical_activity_fragment_vertical_guideline_50"
        app:layout_constraintStart_toStartOf="@id/physical_activity_fragment_vertical_guideline_50"
        app:layout_constraintTop_toBottomOf="@id/physical_activity_fragment_horizontal_guideline_05" />

    <ImageView
        android:layout_width="match_parent"
        android:layout_height="8dp"
        android:layout_marginStart="@dimen/margin_16dp"
        android:layout_marginEnd="@dimen/margin_16dp"
        android:scaleType="fitXY"
        android:src="@drawable/ic_line_horizontal"
        app:layout_constraintBottom_toBottomOf="@id/physical_activity_fragment_horizontal_guideline_30"
        app:layout_constraintTop_toTopOf="@id/physical_activity_fragment_horizontal_guideline_30" />

    <ImageView
        android:layout_width="match_parent"
        android:layout_height="8dp"
        android:layout_marginStart="@dimen/margin_16dp"
        android:layout_marginEnd="@dimen/margin_16dp"
        android:scaleType="fitXY"
        android:src="@drawable/ic_line_horizontal"
        app:layout_constraintBottom_toBottomOf="@id/physical_activity_fragment_horizontal_guideline_55"
        app:layout_constraintTop_toTopOf="@id/physical_activity_fragment_horizontal_guideline_55" />

</androidx.constraintlayout.widget.ConstraintLayout>