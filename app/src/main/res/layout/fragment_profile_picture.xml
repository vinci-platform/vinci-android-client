<?xml version="1.0" encoding="utf-8"?>
<!--
  ~  Copyright (c) 2020, Comtrade
  ~  All rights reserved.
  ~
  ~  Licence: BSD-3-Clause (GPL compatible)
  ~
  ~  Redistribution and use in source and binary forms, with or without
  ~  modification, are permitted provided that the following conditions are met:
  ~      * Redistributions of source code must retain the above copyright
  ~        notice, this list of conditions and the following disclaimer.
  ~      * Redistributions in binary form must reproduce the above copyright
  ~        notice, this list of conditions and the following disclaimer in the
  ~        documentation and/or other materials provided with the distribution.
  ~      * Neither the name of the <organization> nor the
  ~        names of its contributors may be used to endorse or promote products
  ~        derived from this software without specific prior written permission.
  ~
  ~  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ~  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  ~  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  ~   DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
  ~  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  ~  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  ~  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ~  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  ~  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  ~  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  ~
  ~  Package: res.layout
  ~  File: fragment_profile_picture.xml
  ~  Author: Milenko Bojanic <milenko.bojanic@comtrade.com>
  ~
  ~  Description: Profile picture setup screen layout
  ~
  ~  History: 8/11/20 Milenko  Initial code
  ~
  ~
  -->

<androidx.constraintlayout.widget.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    android:layout_width="match_parent"
    android:layout_height="match_parent">

    <de.hdodenhof.circleimageview.CircleImageView
        android:id="@+id/profile_picture_fragment_profile_image_backgound"
        android:layout_width="@dimen/profile_picture_background_size"
        android:layout_height="@dimen/profile_picture_background_size"
        android:layout_marginTop="@dimen/margin_32dp"
        android:clickable="true"
        android:focusable="true"
        android:src="@drawable/ic_profile_picture_background"
        app:layout_constraintTop_toTopOf="parent"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintEnd_toEndOf="parent"/>

    <de.hdodenhof.circleimageview.CircleImageView
        android:id="@+id/profile_picture_fragment_profile_image"
        android:layout_width="@dimen/profile_picture_image_size"
        android:layout_height="@dimen/profile_picture_image_size"
        android:src="@drawable/ic_profile"
        android:clickable="true"
        android:focusable="true"
        app:layout_constraintTop_toTopOf="@+id/profile_picture_fragment_profile_image_backgound"
        app:layout_constraintStart_toStartOf="@+id/profile_picture_fragment_profile_image_backgound"
        app:layout_constraintEnd_toEndOf="@+id/profile_picture_fragment_profile_image_backgound"
        app:layout_constraintBottom_toBottomOf="@+id/profile_picture_fragment_profile_image_backgound"/>

    <ImageView
        android:id="@+id/profile_picture_fragment_user_image_view"
        android:layout_width="match_parent"
        android:layout_height="0dp"
        android:layout_marginStart="@dimen/margin_32dp"
        android:layout_marginTop="@dimen/margin_16dp"
        android:layout_marginEnd="@dimen/margin_32dp"
        app:layout_constraintBottom_toTopOf="@id/profile_picture_fragment_take_picture_button"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintHorizontal_bias="0.0"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toBottomOf="@id/profile_picture_fragment_profile_image" />

    <Button
        android:id="@+id/profile_picture_fragment_take_picture_button"
        style="@style/BasicAppButtonStyle"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="@string/profile_picture_screen_take_picture_button_text"
        app:layout_constraintBottom_toTopOf="@+id/profile_picture_fragment_choose_existing_button"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toBottomOf="@+id/profile_picture_fragment_user_image_view"
        app:layout_constraintVertical_bias="1" />

    <Button
        android:id="@+id/profile_picture_fragment_choose_existing_button"
        style="@style/BasicAppButtonStyle"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:layout_marginBottom="8dp"
        android:text="@string/profile_picture_screen_choose_existing_button_text"
        app:layout_constraintBottom_toTopOf="@+id/profile_picture_fragment_save_profile_picture_button"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintHorizontal_bias="0.25"
        app:layout_constraintStart_toStartOf="parent" />

    <Button
        android:id="@+id/profile_picture_fragment_save_profile_picture_button"
        style="@style/BasicAppButtonStyle"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:layout_marginBottom="8dp"
        android:clickable="false"
        android:enabled="true"
        android:text="@string/profile_picture_screen_save_profile_picture_button_text"
        app:layout_constraintBottom_toTopOf="@+id/profile_picture_fragment_delete_profile_picture_button"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintStart_toStartOf="parent" />

    <Button
        android:id="@+id/profile_picture_fragment_delete_profile_picture_button"
        style="@style/BasicAppButtonStyle"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:layout_marginBottom="32dp"
        android:clickable="false"
        android:enabled="false"
        android:text="@string/profile_picture_screen_delete_profile_picture_button_text"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintStart_toStartOf="parent" />

</androidx.constraintlayout.widget.ConstraintLayout>