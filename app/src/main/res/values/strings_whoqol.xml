<?xml version="1.0" encoding="utf-8"?><!--
  ~  Copyright (c) 2020, Comtrade
  ~  All rights reserved.
  ~
  ~  Licence: BSD-3-Clause (GPL compatible)
  ~
  ~  Redistribution and use in source and binary forms, with or without
  ~  modification, are permitted provided that the following conditions are met:
  ~      * Redistributions of source code must retain the above copyright
  ~        notice, this list of conditions and the following disclaimer.
  ~      * Redistributions in binary form must reproduce the above copyright
  ~        notice, this list of conditions and the following disclaimer in the
  ~        documentation and/or other materials provided with the distribution.
  ~      * Neither the name of the <organization> nor the
  ~        names of its contributors may be used to endorse or promote products
  ~        derived from this software without specific prior written permission.
  ~
  ~  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ~  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  ~  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  ~   DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
  ~  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  ~  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  ~  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ~  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  ~  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  ~  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  ~
  ~  Package: res.values
  ~  File: strings_whoqol.xml
  ~  Author: Milenko Bojanic <milenko.bojanic@comtrade.com>
  ~
  ~  Description: Strings used for QOL questionnaires
  ~
  ~  History: 8/11/20 Milenko  Initial code
  ~
  ~
  -->

<resources>
    <string-array name="whoqol_variants_good">
        <item>Very poor</item>
        <item>Poor</item>
        <item>Neither poor nor good</item>
        <item>Good</item>
        <item>Very good</item>
    </string-array>

    <string-array name="whoqol_variants_satisfied">
        <item>Very dissatisfied</item>
        <item>Dissatisfied</item>
        <item>Neither satisfied nor dissatisfied</item>
        <item>Satisfied</item>
        <item>Very satisfied</item>
    </string-array>

    <string-array name="whoqol_variants_degree">
        <item>Not at all</item>
        <item>A little</item>
        <item>A moderate amount</item>
        <item>Very much</item>
        <item>An extreme amount</item>
    </string-array>

    <string-array name="whoqol_variants_degree_secondary">
        <item>Not at all</item>
        <item>A little</item>
        <item>A moderate amount</item>
        <item>Very much</item>
        <item>Extremely</item>
    </string-array>

    <string-array name="whoqol_variants_how_completely">
        <item>Not at all</item>
        <item>A little</item>
        <item>Moderately</item>
        <item>Mostly</item>
        <item>Completely</item>
    </string-array>

    <string-array name="whoqol_variants_times">
        <item>Never</item>
        <item>Seldom</item>
        <item>Quite often</item>
        <item>Very often</item>
        <item>Always</item>
    </string-array>

    <string-array name="whoqol_variants_intro">
        <item>Not at all</item>
        <item>Not much</item>
        <item>Moderately</item>
        <item>A great deal</item>
        <item>Completely</item>
    </string-array>

    <string name="survey_questionnaire_screen_question_1_text">How would you rate your quality of life?</string>
    <string name="survey_questionnaire_screen_question_2_text">How satisfied are you with your health?</string>
    <string name="survey_questionnaire_screen_question_3_text">To what extent do you feel that physical pain prevents you from doing what you need to do?</string>
    <string name="survey_questionnaire_screen_question_4_text"> How much do you need any medical treatment to function in your daily life?</string>
    <string name="survey_questionnaire_screen_question_5_text">How much do you enjoy life?</string>
    <string name="survey_questionnaire_screen_question_6_text">To what extent do you feel your life to be meaningful?</string>
    <string name="survey_questionnaire_screen_question_7_text">How well are you able to concentrate?</string>
    <string name="survey_questionnaire_screen_question_8_text">How safe do you feel in your daily life?</string>
    <string name="survey_questionnaire_screen_question_9_text">How healthy is your physical environment?</string>
    <string name="survey_questionnaire_screen_question_10_text">Do you have enough energy for everyday life?</string>
    <string name="survey_questionnaire_screen_question_11_text">Are you able to accept your bodily appearance?</string>
    <string name="survey_questionnaire_screen_question_12_text">Have you enough money to meet your needs?</string>
    <string name="survey_questionnaire_screen_question_13_text">How available to you is the information that you need in your day-to-day life?</string>
    <string name="survey_questionnaire_screen_question_14_text">To what extent do you have the opportunity for leisure activities?</string>
    <string name="survey_questionnaire_screen_question_15_text">How well are you able to get around?</string>
    <string name="survey_questionnaire_screen_question_16_text">How satisfied are you with your sleep?</string>
    <string name="survey_questionnaire_screen_question_17_text">How satisfied are you with your ability to perform your daily living activities?</string>
    <string name="survey_questionnaire_screen_question_18_text">How satisfied are you with your capacity for work?</string>
    <string name="survey_questionnaire_screen_question_19_text">How satisfied are you with yourself?</string>
    <string name="survey_questionnaire_screen_question_20_text">How satisfied are you with your personal relationships?</string>
    <string name="survey_questionnaire_screen_question_21_text">How satisfied are you with your sex life?</string>
    <string name="survey_questionnaire_screen_question_22_text">How satisfied are you with the support you get from your friends?</string>
    <string name="survey_questionnaire_screen_question_23_text">How satisfied are you with the conditions of your living place?</string>
    <string name="survey_questionnaire_screen_question_24_text">How satisfied are you with your access to health services?</string>
    <string name="survey_questionnaire_screen_question_25_text">How satisfied are you with your transport?</string>
    <string name="survey_questionnaire_screen_question_26_text">How often do you have negative feelings such as blue mood, despair, anxiety, depression?</string>
    <string name="survey_questionnaire_screen_question_27_text">Did someone help you to fill out this form?</string>
    <string name="survey_questionnaire_screen_question_28_text">How long did it take to fill this form out?</string>
    <string name="survey_questionnaire_screen_question_29_text">Do you have any comments about the assessment?</string>

    <string name="survey_questionnaire_screen_question_1_number">1.</string>
    <string name="survey_questionnaire_screen_question_2_number">2.</string>
    <string name="survey_questionnaire_screen_question_3_number">3.</string>
    <string name="survey_questionnaire_screen_question_4_number">4.</string>
    <string name="survey_questionnaire_screen_question_5_number">5.</string>
    <string name="survey_questionnaire_screen_question_6_number">6.</string>
    <string name="survey_questionnaire_screen_question_7_number">7.</string>
    <string name="survey_questionnaire_screen_question_8_number">8.</string>
    <string name="survey_questionnaire_screen_question_9_number">9.</string>
    <string name="survey_questionnaire_screen_question_10_number">10.</string>
    <string name="survey_questionnaire_screen_question_11_number">11.</string>
    <string name="survey_questionnaire_screen_question_12_number">12.</string>
    <string name="survey_questionnaire_screen_question_13_number">13.</string>
    <string name="survey_questionnaire_screen_question_14_number">14.</string>
    <string name="survey_questionnaire_screen_question_15_number">15.</string>
    <string name="survey_questionnaire_screen_question_16_number">16.</string>
    <string name="survey_questionnaire_screen_question_17_number">17.</string>
    <string name="survey_questionnaire_screen_question_18_number">18.</string>
    <string name="survey_questionnaire_screen_question_19_number">19.</string>
    <string name="survey_questionnaire_screen_question_20_number">20.</string>
    <string name="survey_questionnaire_screen_question_21_number">21.</string>
    <string name="survey_questionnaire_screen_question_22_number">22.</string>
    <string name="survey_questionnaire_screen_question_23_number">23.</string>
    <string name="survey_questionnaire_screen_question_24_number">24.</string>
    <string name="survey_questionnaire_screen_question_25_number">25.</string>
    <string name="survey_questionnaire_screen_question_26_number">26.</string>
    <string name="survey_questionnaire_screen_question_27_number">27.</string>
    <string name="survey_questionnaire_screen_question_28_number">28.</string>
    <string name="survey_questionnaire_screen_question_29_number">29.</string>

    <string-array name="question_ids">
        <item>G1</item>
        <item>G4</item>
        <item>F1.4</item>
        <item>F11.3</item>
        <item>F4.1</item>
        <item>F24.2</item>
        <item>F5.3</item>
        <item>F16.1</item>
        <item>F22.1</item>
        <item>F2.1</item>
        <item>F7.1</item>
        <item>F18.1</item>
        <item>F20.1</item>
        <item>F21.1</item>
        <item>F9.1</item>
        <item>F3.3</item>
        <item>F10.3</item>
        <item>F12.4</item>
        <item>F6.3</item>
        <item>F13.3</item>
        <item>F15.3</item>
        <item>F14.4</item>
        <item>F17.3</item>
        <item>F19.3</item>
        <item>F23.3</item>
        <item>F8.1</item>
        <item>27</item>
        <item>28</item>
        <item>29</item>
    </string-array>


</resources>