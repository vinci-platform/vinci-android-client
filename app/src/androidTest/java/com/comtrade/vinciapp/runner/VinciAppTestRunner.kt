package com.comtrade.vinciapp.runner

import android.app.Application
import android.content.Context
import androidx.test.runner.AndroidJUnitRunner
import com.comtrade.vinciapp.TestVinciApp

/**
 * The application test runner.
 * Providing the test application context instead of the production one.
 */
class VinciAppTestRunner : AndroidJUnitRunner() {
    override fun newApplication(
        cl: ClassLoader?,
        className: String?,
        context: Context?
    ): Application {
        return super.newApplication(cl, TestVinciApp::class.java.name, context)
    }
}