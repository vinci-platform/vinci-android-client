package com.comtrade.vinciapp.di

import android.content.Context
import com.comtrade.vinciapp.di.data.TestDataModule
import com.comtrade.vinciapp.di.devices.TestDeviceModule
import com.comtrade.vinciapp.di.questionnaire.TestQuestionnaireModule
import com.comtrade.vinciapp.view.widget.editProfileAccount.view.EditProfileAccountFragmentTest
import com.comtrade.vinciapp.view.widget.settingEmergencyNumber.view.SettingEmergencyNumberFragmentTest
import dagger.BindsInstance
import dagger.Component

@Component(
    modules = [TestAppModule::class,
        TestDataModule::class,
        TestDeviceModule::class,
        TestQuestionnaireModule::class]
)
@TestApplicationScope
internal interface TestAppComponent : AppComponent {

    @Component.Factory
    interface Factory {
        fun create(@BindsInstance applicationContext: Context): TestAppComponent
    }

    fun inject(test: EditProfileAccountFragmentTest)

    fun inject(test: SettingEmergencyNumberFragmentTest)
}