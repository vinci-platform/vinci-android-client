package com.comtrade.vinciapp.di.data

import android.app.Application
import androidx.room.Room
import com.comtrade.data.stepDatabase.StepDatabase
import com.comtrade.data.stepDatabase.SurveyDao
import com.comtrade.domain.contracts.repository.IAppRepository
import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.contracts.repository.user.IAccountRepository
import com.comtrade.domain.contracts.repository.user.IDeviceRepository
import com.comtrade.domain.contracts.repository.user.UserServiceRepository
import com.comtrade.vinciapp.di.TestApplicationScope
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import org.mockito.Mockito

/**
 * Test Dagger DI module with providing mock or fake data sources.
 */
@Module
internal class TestDataModule {

    @Provides
    @TestApplicationScope
    fun provideAppPrefs(): IPrefs = Mockito.mock(IPrefs::class.java)

    @Provides
    @TestApplicationScope
    fun provideGson(): Gson = Gson().newBuilder().create()

    @Provides
    @TestApplicationScope
    fun provideStepDatabase(application: Application): StepDatabase = Room.inMemoryDatabaseBuilder(
        application,
        StepDatabase::class.java,
    ).fallbackToDestructiveMigration().build()

    @Provides
    @TestApplicationScope
    fun provideSurveyDao(): SurveyDao = Mockito.mock(SurveyDao::class.java)

    @Provides
    @TestApplicationScope
    fun provideAppRepository(): IAppRepository = Mockito.mock(IAppRepository::class.java)

    @Provides
    @TestApplicationScope
    fun provideAccountRepository(): IAccountRepository =
        Mockito.mock(IAccountRepository::class.java)

    @Provides
    @TestApplicationScope
    fun provideDeviceRepository(): IDeviceRepository =
        Mockito.mock(IDeviceRepository::class.java)

    @Provides
    @TestApplicationScope
    fun provideUserServiceRepository(): UserServiceRepository =
        Mockito.mock(UserServiceRepository::class.java)
}