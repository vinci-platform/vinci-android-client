/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: TestQuestionnaireModule.kt
 * Author: sdelic
 *
 * History:  25.3.22. 09:30 created
 */

package com.comtrade.vinciapp.di.questionnaire

import com.comtrade.domain.contracts.usecases.survey.ISaveSurveyDataUseCase
import com.comtrade.domain.contracts.usecases.survey.ISyncSurveyFromServerUseCase
import com.comtrade.domain.contracts.usecases.survey.ISyncSurveyToVinciUseCase
import com.comtrade.vinciapp.di.TestApplicationScope
import dagger.Module
import dagger.Provides
import org.mockito.Mockito

/**
 * Test Dagger DI module with providing mock or fake questionnaire sources.
 */
@Module
internal class TestQuestionnaireModule {

    @Provides
    @TestApplicationScope
    fun provideSaveSurveyUseCase(): ISaveSurveyDataUseCase =
        Mockito.mock(ISaveSurveyDataUseCase::class.java)

    @Provides
    fun provideSyncSurveyToVinciUseCase(): ISyncSurveyToVinciUseCase =
        Mockito.mock(ISyncSurveyToVinciUseCase::class.java)

    @Provides
    fun provideSyncSurveyFromVinciUseCase(): ISyncSurveyFromServerUseCase =
        Mockito.mock(ISyncSurveyFromServerUseCase::class.java)
}