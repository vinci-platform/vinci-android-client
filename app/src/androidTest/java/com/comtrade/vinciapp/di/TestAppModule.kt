package com.comtrade.vinciapp.di

import android.app.Application
import android.os.Environment
import androidx.test.core.app.ApplicationProvider
import com.comtrade.domain.contracts.utils.IEmailValidator
import com.comtrade.domain.di.AppQualifiers
import com.comtrade.vinciapp.BuildConfig
import com.comtrade.vinciapp.util.EmailValidator
import dagger.Module
import dagger.Provides
import java.io.File
import javax.inject.Named

/**
 * The Dagger DI test application module for providing mock context.
 */
@Module
internal class TestAppModule {

    @Provides
    @TestApplicationScope
    fun provideApplicationContext(): Application = ApplicationProvider.getApplicationContext()

    @TestApplicationScope
    @Provides
    @Named(AppQualifiers.ROOT_IMAGE_DIRECTORY)
    fun provideRootProfileImageDirectory(application: Application): String =
        application.getExternalFilesDir(
            Environment.DIRECTORY_PICTURES
        )
            .toString() + File.separator + "vinci"

    @TestApplicationScope
    @Provides
    @Named(AppQualifiers.APP_BASE_URL)
    fun provideBaseUrlAddress(): String = BuildConfig.BASE_URL

    @TestApplicationScope
    @Provides
    @Named(AppQualifiers.TERMS_AND_CONDITIONS_FILE_LOCATION)
    fun provideTermsAndConditionsFileLocation(): String =
        "file:///android_asset/vInci_Terms_And_Conditions.html"

    @TestApplicationScope
    @Provides
    fun provideEmailValidator(): IEmailValidator = EmailValidator
}