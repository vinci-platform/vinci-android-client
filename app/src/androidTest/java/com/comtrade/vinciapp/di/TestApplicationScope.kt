package com.comtrade.vinciapp.di

import javax.inject.Scope

/**
 * Custom Dagger DI scope for test scenarios.
 */
@Scope
@Retention
internal annotation class TestApplicationScope
