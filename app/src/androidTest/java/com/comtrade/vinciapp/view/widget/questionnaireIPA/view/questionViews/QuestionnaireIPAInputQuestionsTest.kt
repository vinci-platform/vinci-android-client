/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: QuestionnaireIPAInputQuestionsTest.kt
 * Author: sdelic
 *
 * History:  18.3.22. 11:52 created
 */

package com.comtrade.vinciapp.view.widget.questionnaireIPA.view.questionViews

import android.text.InputType
import androidx.fragment.app.testing.launchFragmentInContainer
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.util.assertEditFieldForHintAndInputType
import com.comtrade.vinciapp.util.assertViewHasCaption
import com.comtrade.vinciapp.util.assertViewsDisplayed
import org.junit.Before
import org.junit.Test

/**
 * QuestionnaireIPAInputQuestions screen UI tests.
 * Assert all of the view components are displayed.
 * Assert that all views are in their correct initial state.
 */
internal class QuestionnaireIPAInputQuestionsTest {
    @Before
    fun setUp() {
        launchFragmentInContainer<QuestionnaireIPAInputQuestions>()
    }

    @Test
    fun questionnaire_ipa_input_screen_is_displayed() {
        assertViewsDisplayed(
            R.id.ipa_questionnaire_screen_input_question_container,
            R.id.ipa_questionnaire_screen_input_question_number,
            R.id.ipa_questionnaire_screen_input_question_description,
            R.id.ipa_questionnaire_screen_input_question_question,
            R.id.ipa_questionnaire_screen_input_question_edit_text_hours,
            R.id.ipa_questionnaire_screen_input_question_edit_text_minutes
        )
    }

    @Test
    fun question_number_has_correct_text() {
        assertViewHasCaption(
            R.id.ipa_questionnaire_screen_input_question_number,
            R.string.IPA_questionnaire_screen_question_1_number
        )
    }

    @Test
    fun question_description_is_displayed_at_start() {
        assertViewHasCaption(
            R.id.ipa_questionnaire_screen_input_question_description,
            R.string.IPA_questionnaire_screen_question_1_description
        )
    }

    @Test
    fun question_is_displayed_at_start() {
        assertViewHasCaption(
            R.id.ipa_questionnaire_screen_input_question_question,
            R.string.IPA_questionnaire_screen_question_1_question
        )
    }

    @Test
    fun input_hours_has_correct_hint_and_type() {
        assertEditFieldForHintAndInputType(
            R.id.ipa_questionnaire_screen_input_question_edit_text_hours,
            R.string.ipa_questionnaire_screen_input_hint_hours,
            InputType.TYPE_CLASS_NUMBER
        )
    }

    @Test
    fun input_minutes_has_correct_hint_and_type() {
        assertEditFieldForHintAndInputType(
            R.id.ipa_questionnaire_screen_input_question_edit_text_minutes,
            R.string.ipa_questionnaire_screen_input_hint_minutes,
            InputType.TYPE_CLASS_NUMBER
        )
    }
}