/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: CognitiveGamesFragmentTest.kt
 * Author: sdelic
 *
 * History:  4.3.22. 16:29 created
 */

package com.comtrade.vinciapp.view.widget.cognitiveGames.view

import android.content.Context
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.util.assertViewsDisplayed
import org.hamcrest.CoreMatchers
import org.hamcrest.CoreMatchers.not
import org.hamcrest.core.AllOf
import org.junit.Before
import org.junit.Test

/**
 * CognitiveGamesFragment screen UI tests.
 * Assert all of the view components are displayed.
 * Assert that all views are in their correct initial state.
 */
internal class CognitiveGamesFragmentTest {

    @Before
    fun setUp() {
        launchFragmentInContainer<CognitiveGamesFragment>()
    }

    @Test
    fun cognitive_games_screen_is_displayed() {
        assertViewsDisplayed(
            R.id.cognitive_games_fragment_scroll,
            R.id.cognitive_games_screen_text_first,
            R.id.cognitive_games_fragment_spinner,
            R.id.cognitive_games_fragment_arrow_view,
            R.id.cognitive_games_fragment_datetime_spinner,
            R.id.cognitive_games_fragment_datetime_calendar_icon,
            R.id.cognitive_games_fragment_invite_friends_checkbox,
            R.id.cognitive_games_start_button,
            R.id.cognitive_games_save_button
        )
    }

    @Test
    fun views_with_correct_text_are_displayed() {
        onView(withId(R.id.cognitive_games_screen_text_first)).check(matches(withText(R.string.cognitive_games_screen_text_first)))
        //This view has hint instead of regular text
        onView(withId(R.id.cognitive_games_fragment_datetime_spinner)).check(matches(withHint(R.string.cognitive_games_screen_datetime)))
        onView(withId(R.id.cognitive_games_fragment_invite_friends_checkbox)).check(
            matches(
                withText(
                    R.string.cognitive_games_screen_invite_friends_checkbox_text
                )
            )
        )
        onView(withId(R.id.cognitive_games_start_button)).check(matches(withText(R.string.cognitive_games_screen_start_button_text)))
        onView(withId(R.id.cognitive_games_save_button)).check(matches(withText(R.string.cognitive_games_screen_save_button_text)))
    }

    @Test
    fun invite_friends_checkbox_is_unchecked() {
        onView(withId(R.id.cognitive_games_fragment_invite_friends_checkbox)).check(
            matches(
                isNotChecked()
            )
        )
    }

    @Test
    fun save_button_is_initially_disabled() {
        onView(withId(R.id.cognitive_games_save_button)).check(matches(not(isEnabled())))
    }

    @Test
    fun game_selector_displays_selected_options() {
        val supportedCognitiveGames =
            ApplicationProvider.getApplicationContext<Context>().resources.getStringArray(R.array.supported_cognitive_games)
        // Default first cognitive game shown is Sudoku easy.
        onView(withId(R.id.cognitive_games_fragment_spinner))
            .check(
                matches(
                    AllOf.allOf(
                        withSpinnerText(supportedCognitiveGames[0]),
                        isCompletelyDisplayed(),
                        withEffectiveVisibility(Visibility.VISIBLE)
                    )
                )
            )

        assertSpinnerSelection(supportedCognitiveGames[7])
        assertSpinnerSelection(supportedCognitiveGames[4])

        //Switch back to fist selection
        assertSpinnerSelection(supportedCognitiveGames[0])

    }

    /**
     * Asserts if the cognitive game selection for the passed cognitive game value.
     */
    private fun assertSpinnerSelection(cognitiveGamesSelection: String) {
        onView(withId(R.id.cognitive_games_fragment_spinner)).perform(ViewActions.click())
        Espresso.onData(
            AllOf.allOf(
                CoreMatchers.`is`(CoreMatchers.instanceOf(String::class.java)),
                CoreMatchers.`is`(cognitiveGamesSelection)
            )
        ).perform(
            ViewActions.click()
        )
        onView(withId(R.id.cognitive_games_fragment_spinner))
            .check(matches(withSpinnerText(cognitiveGamesSelection)))
    }
}