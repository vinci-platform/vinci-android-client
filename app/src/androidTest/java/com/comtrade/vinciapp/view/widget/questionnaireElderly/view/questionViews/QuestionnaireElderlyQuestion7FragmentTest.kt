/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: QuestionnaireElderlyQuestion7FragmentTest.kt
 * Author: sdelic
 *
 * History:  29.3.22. 14:27 created
 */

package com.comtrade.vinciapp.view.widget.questionnaireElderly.view.questionViews

import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.espresso.Espresso
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.util.assertViewHasCaption
import com.comtrade.vinciapp.util.assertViewsDisplayed
import org.junit.Before
import org.junit.Test

/**
 * QuestionnaireElderlyQuestion7Fragment screen UI tests.
 * Assert all of the view components are displayed.
 * Assert that all views are in their correct initial state.
 */
internal class QuestionnaireElderlyQuestion7FragmentTest {
    @Before
    fun setUp() {
        launchFragmentInContainer<QuestionnaireElderlyQuestion7Fragment>()
    }

    @Test
    fun elderly_question_7_screen_is_displayed() {
        assertViewsDisplayed(
            R.id.elderly_questionnaire_question_7,
            R.id.elderly_questionnaire_answers_7,
            R.id.elderly_questionnaire_answers_7_yes,
            R.id.elderly_questionnaire_answers_7_no
        )
    }

    @Test
    fun elderly_question_7_has_correct_text() {
        assertViewHasCaption(
            R.id.elderly_questionnaire_question_7,
            R.string.questionnaire_elderly_question_7_text
        )
    }

    @Test
    fun radio_buttons_are_not_checked_at_start() {
        val radioButtonIds = arrayOf(
            R.id.elderly_questionnaire_answers_7_yes,
            R.id.elderly_questionnaire_answers_7_no
        )

        for (radioButtonId in radioButtonIds) {
            Espresso.onView(ViewMatchers.withId(radioButtonId))
                .check(ViewAssertions.matches(ViewMatchers.isNotChecked()))
        }
    }

    @Test
    fun yes_radio_button_has_correct_caption() {
        assertViewHasCaption(
            R.id.elderly_questionnaire_answers_7_yes,
            R.string.common_radio_button_yes
        )
    }

    @Test
    fun no_radio_button_has_correct_caption() {
        assertViewHasCaption(
            R.id.elderly_questionnaire_answers_7_no,
            R.string.common_radio_button_no
        )
    }

}