/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: DevicesSetupFragmentTest.kt
 * Author: sdelic
 *
 * History:  15.3.22. 16:52 created
 */

package com.comtrade.vinciapp.view.widget.devicesSetup.view

import androidx.fragment.app.testing.launchFragmentInContainer
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.util.assertButtonsEnabled
import com.comtrade.vinciapp.util.assertViewHasCaption
import com.comtrade.vinciapp.util.assertViewsDisplayed
import org.junit.Before
import org.junit.Test

/**
 * DevicesSetupFragment screen UI tests.
 * Assert all of the view components are displayed.
 * Assert that all views are in their correct initial state.
 */
internal class DevicesSetupFragmentTest {

    @Before
    fun setUp() {
        launchFragmentInContainer<DevicesSetupFragment>()
    }

    @Test
    fun device_setup_screen_is_displayed() {
        assertViewsDisplayed(
            R.id.devices_setup_fragment_profile_image,
            R.id.devices_setup_fragment_info_text,
            R.id.devices_setup_fragment_set_emergency_number,
            R.id.devices_setup_fragment_add_device_button,
            R.id.devices_setup_fragment_start_with_questionnaire_button,
            R.id.devices_setup_fragment_setup_later_button
        )
    }

    @Test
    fun device_setup_buttons_are_enabled() {
        assertButtonsEnabled(
            R.id.devices_setup_fragment_set_emergency_number,
            R.id.devices_setup_fragment_add_device_button,
            R.id.devices_setup_fragment_start_with_questionnaire_button,
            R.id.devices_setup_fragment_setup_later_button
        )
    }

    @Test
    fun device_info_has_correct_text() {
        assertViewHasCaption(
            R.id.devices_setup_fragment_info_text,
            R.string.devices_setup_screen_info_text
        )
    }

    @Test
    fun set_emergency_number_button_has_correct_caption() {
        assertViewHasCaption(
            R.id.devices_setup_fragment_set_emergency_number,
            R.string.devices_setup_screen_set_emergency_number
        )
    }

    @Test
    fun add_device_button_has_correct_caption() {
        assertViewHasCaption(
            R.id.devices_setup_fragment_add_device_button,
            R.string.devices_setup_screen_add_device_button_text
        )
    }

    @Test
    fun start_questionnaire_button_has_correct_caption() {
        assertViewHasCaption(
            R.id.devices_setup_fragment_start_with_questionnaire_button,
            R.string.devices_setup_screen_start_with_questionnaire_button_text
        )
    }

    @Test
    fun setup_later_button_has_correct_caption() {
        assertViewHasCaption(
            R.id.devices_setup_fragment_setup_later_button,
            R.string.devices_setup_screen_setup_later_button_text
        )
    }

}