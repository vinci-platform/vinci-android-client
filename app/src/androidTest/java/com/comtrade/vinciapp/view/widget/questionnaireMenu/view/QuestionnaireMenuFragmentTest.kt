/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: QuestionnaireMenuFragmentTest.kt
 * Author: sdelic
 *
 * History:  28.3.22. 16:27 created
 */

package com.comtrade.vinciapp.view.widget.questionnaireMenu.view

import androidx.fragment.app.testing.launchFragmentInContainer
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.util.assertButtonsEnabled
import com.comtrade.vinciapp.util.assertViewHasCaption
import com.comtrade.vinciapp.util.assertViewsDisplayed
import org.junit.Before
import org.junit.Test

/**
 * QuestionnaireMenuFragment screen UI tests.
 * Assert all of the view components are displayed.
 * Assert that all views are in their correct initial state.
 */
internal class QuestionnaireMenuFragmentTest {

    @Before
    fun setUp() {
        launchFragmentInContainer<QuestionnaireMenuFragment>()
    }

    @Test
    fun questionnaire_menu_screen_is_displayed() {
        assertViewsDisplayed(
            R.id.questionnaire_menu_fragment_ipa_questionnaire_button,
            R.id.questionnaire_menu_fragment_whoqol_questionnaire_button,
            R.id.questionnaire_menu_fragment_user_needs_questionnaire_button,
            R.id.questionnaire_menu_fragment_elderly_questionnaire_button
        )
    }

    @Test
    fun questionnaire_menu_buttons_are_enabled() {
        assertButtonsEnabled(
            R.id.questionnaire_menu_fragment_ipa_questionnaire_button,
            R.id.questionnaire_menu_fragment_whoqol_questionnaire_button,
            R.id.questionnaire_menu_fragment_user_needs_questionnaire_button,
            R.id.questionnaire_menu_fragment_elderly_questionnaire_button
        )
    }

    @Test
    fun ipa_questionnaire_button_has_correct_caption() {
        assertViewHasCaption(
            R.id.questionnaire_menu_fragment_ipa_questionnaire_button,
            R.string.questionnaire_menu_screen_ipa_questionnaire_text
        )
    }

    @Test
    fun whoqol_questionnaire_button_has_correct_caption() {
        assertViewHasCaption(
            R.id.questionnaire_menu_fragment_whoqol_questionnaire_button,
            R.string.questionnaire_menu_screen_whoqol_questionnaire_text
        )
    }

    @Test
    fun user_needs_questionnaire_button_has_correct_caption() {
        assertViewHasCaption(
            R.id.questionnaire_menu_fragment_user_needs_questionnaire_button,
            R.string.questionnaire_menu_screen_user_needs_questionnaire_text
        )
    }

    @Test
    fun elderly_questionnaire_button_has_correct_caption() {
        assertViewHasCaption(
            R.id.questionnaire_menu_fragment_elderly_questionnaire_button,
            R.string.questionnaire_menu_screen_elderly_questionnaire_text
        )
    }
}