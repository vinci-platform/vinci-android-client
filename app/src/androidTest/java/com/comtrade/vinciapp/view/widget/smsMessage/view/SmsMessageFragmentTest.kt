/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: SmsMessageFragmentTest.kt
 * Author: sdelic
 *
 * History:  25.3.22. 16:15 created
 */

package com.comtrade.vinciapp.view.widget.smsMessage.view

import android.text.InputType
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.espresso.Espresso
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.withText
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.util.assertButtonsEnabled
import com.comtrade.vinciapp.util.assertEditFieldForHintAndInputType
import com.comtrade.vinciapp.util.assertViewHasCaption
import com.comtrade.vinciapp.util.assertViewsDisplayed
import org.junit.Before
import org.junit.Test

/**
 * SmsMessageFragment screen UI tests.
 * Assert all of the view components are displayed.
 * Assert that all views are in their correct initial state.
 */
internal class SmsMessageFragmentTest {

    @Before
    fun setUp() {
        launchFragmentInContainer<SmsMessageFragment>()
    }

    @Test
    fun sms_message_screen_is_displayed() {
        assertViewsDisplayed(
            R.id.sms_message_fragment_profile_image,
            R.id.sms_message_fragment_profile_name_text_view,
            R.id.sms_message_fragment_message_text,
            R.id.sms_message_fragment_buttons_container,
            R.id.sms_message_fragment_back_button,
            R.id.sms_message_fragment_invite_button
        )
    }

    @Test
    fun profile_name_is_not_displayed() {
        Espresso.onView(ViewMatchers.withId(R.id.sms_message_fragment_profile_name_text_view))
            .check(ViewAssertions.matches(withText("")))
    }

    @Test
    fun message_input_field_has_correct_hint_and_initial_state() {
        assertEditFieldForHintAndInputType(
            R.id.sms_message_fragment_message_text,
            R.string.sms_message_screen_message_hint,
            InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_FLAG_MULTI_LINE
        )
    }

    @Test
    fun sms_message_buttons_are_enabled() {
        assertButtonsEnabled(
            R.id.sms_message_fragment_back_button,
            R.id.sms_message_fragment_invite_button
        )
    }

    @Test
    fun back_button_has_correct_caption() {
        assertViewHasCaption(
            R.id.sms_message_fragment_back_button,
            R.string.sms_message_screen_back_button_text
        )
    }

    @Test
    fun invite_button_has_correct_caption() {
        assertViewHasCaption(
            R.id.sms_message_fragment_invite_button,
            R.string.sms_message_screen_invite_button_text
        )
    }

}