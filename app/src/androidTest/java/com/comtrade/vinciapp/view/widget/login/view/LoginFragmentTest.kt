package com.comtrade.vinciapp.view.widget.login.view

import android.text.InputType
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.util.assertEditFieldForHintAndInputType
import com.comtrade.vinciapp.util.assertViewsDisplayed
import com.comtrade.vinciapp.util.assertViewsNotDisplayed
import org.junit.Before
import org.junit.Test

/**
 * LoginFragment screen UI tests.
 * Assert all of the view components are displayed.
 * Assert that all views are in their correct initial state.
 * Assert input fields.
 * Assert progress bar state when login button is pressed.
 */
internal class LoginFragmentTest {

    @Before
    fun setUp() {
        launchFragmentInContainer<LoginFragment>(themeResId = R.style.Theme_AppCompat_Light)
    }

    @Test
    fun login_fragment_is_displayed() {
        assertViewsDisplayed(
            R.id.includable_logo_image,
            R.id.login_fragment_username_edit_text,
            R.id.login_fragment_password_edit_text,
            R.id.login_fragment_login_button,
            R.id.login_fragment_forgot_password_text_view
        )
        //Progress bar visible only on network calls.
        assertViewsNotDisplayed(R.id.progressBar)
    }

    @Test
    fun input_fields_have_correct_hints_and_initial_state() {

        assertEditFieldForHintAndInputType(
            R.id.login_fragment_username_edit_text,
            R.string.login_screen_enter_email_hint,
            InputType.TYPE_CLASS_TEXT
        )

        assertEditFieldForHintAndInputType(
            R.id.login_fragment_password_edit_text,
            R.string.login_screen_enter_password_hint,
            InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
        )
    }

    @Test
    fun login_button_has_correct_label() {
        onView(withId(R.id.login_fragment_login_button))
            .check(matches(withText(R.string.login_screen_login_button_text)))
    }

    @Test
    fun forgot_password_has_correct_label() {
        onView(withId(R.id.login_fragment_forgot_password_text_view))
            .check(matches(withText(R.string.login_screen_forgot_password_text)))
    }

    @Test
    fun progress_bar_is_shown_on_login_button_click() {
        assertViewsNotDisplayed(R.id.containerProgress)
        onView(withId(R.id.login_fragment_login_button)).perform(click())
        assertViewsDisplayed(R.id.containerProgress)
    }
}