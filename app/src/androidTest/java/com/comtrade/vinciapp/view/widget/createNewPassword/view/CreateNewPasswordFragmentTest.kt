/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: CreateNewPasswordFragmentTest.kt
 * Author: sdelic
 *
 * History:  25.3.22. 16:53 created
 */

package com.comtrade.vinciapp.view.widget.createNewPassword.view

import android.text.InputType
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isEnabled
import androidx.test.espresso.matcher.ViewMatchers.withId
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.util.assertEditFieldForHintAndInputType
import com.comtrade.vinciapp.util.assertViewHasCaption
import com.comtrade.vinciapp.util.assertViewsDisplayed
import com.comtrade.vinciapp.util.assertViewsNotDisplayed
import org.junit.Before
import org.junit.Test

/**
 * CreateNewPasswordFragment screen UI tests.
 * Assert all of the view components are displayed.
 * Assert that all views are in their correct initial state.
 */
internal class CreateNewPasswordFragmentTest {
    @Before
    fun setUp() {
        launchFragmentInContainer<CreateNewPasswordFragment>()
    }

    @Test
    fun create_new_password_screen_is_displayed() {
        assertViewsDisplayed(
            R.id.includable_logo_image,
            R.id.create_new_password_fragment_temporary_password_edit_text,
            R.id.create_new_password_fragment_new_password_edit_text,
            R.id.create_new_password_fragment_confirm_password_edit_text,
            R.id.create_new_password_fragment_login_button,
        )
    }

    @Test
    fun progress_view_is_hidden_at_start() {
        assertViewsNotDisplayed(
            R.id.containerProgress,
            R.id.progressBar
        )
    }

    @Test
    fun temporary_password_input_fields_has_correct_hint_and_initial_state() {
        assertEditFieldForHintAndInputType(
            R.id.create_new_password_fragment_temporary_password_edit_text,
            R.string.create_new_password_screen_temporary_password_hint,
            InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
        )
    }

    @Test
    fun new_password_input_fields_has_correct_hint_and_initial_state() {
        assertEditFieldForHintAndInputType(
            R.id.create_new_password_fragment_new_password_edit_text,
            R.string.create_new_password_screen_new_password_hint,
            InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
        )
    }

    @Test
    fun confirm_new_password_input_fields_has_correct_hint_and_initial_state() {
        assertEditFieldForHintAndInputType(
            R.id.create_new_password_fragment_confirm_password_edit_text,
            R.string.create_new_password_screen_confirm_password_hint,
            InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
        )
    }

    @Test
    fun change_password_button_is_enabled() {
        onView(withId(R.id.create_new_password_fragment_login_button)).check(matches(isEnabled()))
    }

    @Test
    fun change_password_button_has_correct_caption() {
        assertViewHasCaption(
            R.id.create_new_password_fragment_login_button,
            R.string.create_new_password_screen_create_button_text
        )
    }
}