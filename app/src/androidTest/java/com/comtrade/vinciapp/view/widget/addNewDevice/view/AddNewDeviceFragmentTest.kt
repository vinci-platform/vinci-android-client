/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: AddNewDeviceFragmentTest.kt
 * Author: sdelic
 *
 * History:  15.3.22. 16:21 created
 */

package com.comtrade.vinciapp.view.widget.addNewDevice.view

import androidx.fragment.app.testing.launchFragmentInContainer
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.util.assertButtonsEnabled
import com.comtrade.vinciapp.util.assertViewHasCaption
import com.comtrade.vinciapp.util.assertViewsDisplayed
import org.junit.Before
import org.junit.Test

/**
 * AddNewDeviceFragment screen UI tests.
 * Assert all of the view components are displayed.
 * Assert that all views are in their correct initial state.
 */
internal class AddNewDeviceFragmentTest {

    @Before
    fun setUp() {
        launchFragmentInContainer<AddNewDeviceFragment>()
    }

    @Test
    fun add_new_device_screen_is_displayed() {
        assertViewsDisplayed(
            R.id.add_new_device_connect_to_fitbit_button,
            R.id.add_new_device_enter_id_button,
            R.id.add_new_device_settings_button
        )
    }

    @Test
    fun add_new_device_buttons_are_enabled() {
        assertButtonsEnabled(
            R.id.add_new_device_connect_to_fitbit_button,
            R.id.add_new_device_enter_id_button,
            R.id.add_new_device_settings_button
        )
    }

    @Test
    fun connect_to_fitbit_button_has_correct_caption() {
        assertViewHasCaption(
            R.id.add_new_device_connect_to_fitbit_button,
            R.string.add_new_device_screen_connect_to_fitbit
        )
    }

    @Test
    fun enter_device_id_button_has_correct_caption() {
        assertViewHasCaption(
            R.id.add_new_device_enter_id_button,
            R.string.add_new_device_screen_enter_id
        )
    }

    @Test
    fun device_settings_button_has_correct_caption() {
        assertViewHasCaption(
            R.id.add_new_device_settings_button,
            R.string.add_new_device_screen_settings
        )
    }

}