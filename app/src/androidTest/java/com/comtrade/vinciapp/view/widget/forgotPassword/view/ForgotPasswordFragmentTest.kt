/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: ForgotPasswordFragmentTest.kt
 * Author: sdelic
 *
 * History:  11.3.22. 13:50 created
 */

package com.comtrade.vinciapp.view.widget.forgotPassword.view

import android.text.InputType
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.espresso.Espresso
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.util.assertEditFieldForHintAndInputType
import com.comtrade.vinciapp.util.assertViewsDisplayed
import com.comtrade.vinciapp.util.assertViewsNotDisplayed
import org.junit.Before
import org.junit.Test

/**
 * ForgotPasswordFragment screen UI tests.
 * Assert all of the view components are displayed.
 * Assert that all views are in their correct initial state.
 * Assert input field.
 * Assert progress bar state when login button is pressed.
 */
internal class ForgotPasswordFragmentTest {

    @Before
    fun setUp() {
        launchFragmentInContainer<ForgotPasswordFragment>()
    }

    @Test
    fun forgot_password_screen_is_displayed() {
        assertViewsDisplayed(
            R.id.includable_logo_image,
            R.id.forgot_password_fragment_email_edit_text,
            R.id.forgot_password_fragment_reset_button
        )

        assertViewsNotDisplayed(R.id.containerProgress)
    }

    @Test
    fun input_field_has_correct_initial_state() {
        assertEditFieldForHintAndInputType(
            R.id.forgot_password_fragment_email_edit_text,
            R.string.forgot_password_screen_enter_email_hint,
            InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS
        )
    }

    @Test
    fun password_reset_button_has_correct_label() {
        Espresso.onView(ViewMatchers.withId(R.id.forgot_password_fragment_reset_button))
            .check(ViewAssertions.matches(ViewMatchers.withText(R.string.forgot_password_screen_reset_button_text)))
    }
}