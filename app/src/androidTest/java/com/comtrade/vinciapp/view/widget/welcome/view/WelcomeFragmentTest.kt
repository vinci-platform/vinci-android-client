/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: WelcomeFragmentTest.kt
 * Author: sdelic
 *
 * History:  17.3.22. 09:38 created
 */

package com.comtrade.vinciapp.view.widget.welcome.view

import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.util.assertViewHasCaption
import com.comtrade.vinciapp.util.assertViewsDisplayed
import org.hamcrest.CoreMatchers.not
import org.junit.Before
import org.junit.Test

/**
 * WelcomeFragment screen UI tests.
 * Assert all of the view components are displayed.
 * Assert that all views are in their correct initial state.
 */
internal class WelcomeFragmentTest {


    @Before
    fun setUp() {
        launchFragmentInContainer<WelcomeFragment>()
    }

    @Test
    fun welcome_screen_is_displayed() {
        assertViewsDisplayed(
            R.id.includable_logo_image,
            R.id.welcome_fragment_login_button,
            R.id.welcome_fragment_create_account_button,
            R.id.welcome_fragment_facebook_button,
            R.id.welcome_fragment_google_button,
            R.id.welcome_fragment_terms_checkbox
        )
    }

    @Test
    fun login_button_has_correct_caption() {
        assertViewHasCaption(
            R.id.welcome_fragment_login_button,
            R.string.welcome_screen_login_button_text
        )
    }


    @Test
    fun create_account_button_has_correct_caption() {
        assertViewHasCaption(
            R.id.welcome_fragment_create_account_button,
            R.string.welcome_screen_create_account_button_text
        )
    }

    @Test
    fun facebook_button_has_correct_caption() {
        assertViewHasCaption(
            R.id.welcome_fragment_facebook_button,
            R.string.welcome_screen_facebook_button_text
        )
    }

    @Test
    fun google_button_has_correct_caption() {
        assertViewHasCaption(
            R.id.welcome_fragment_google_button,
            R.string.welcome_screen_google_button_text
        )
    }

    @Test
    fun terms_and_conditions_checkbox_has_correct_text() {
        assertViewHasCaption(
            R.id.welcome_fragment_terms_checkbox,
            R.string.welcome_screen_terms_and_conditions
        )
    }

    @Test
    fun terms_and_conditions_checkbox_is_unchecked_at_start() {
        onView(withId(R.id.welcome_fragment_terms_checkbox)).check(matches(isNotChecked()))

    }

    @Test
    fun login_button_is_disabled_at_start() {
        onView(withId(R.id.welcome_fragment_login_button)).check(matches(not(isEnabled())))
    }

    @Test
    fun create_account_button_is_disabled_at_start() {
        onView(withId(R.id.welcome_fragment_create_account_button)).check(matches(not(isEnabled())))
    }

    @Test
    fun facebook_button_is_disabled() {
        onView(withId(R.id.welcome_fragment_facebook_button)).check(matches(not(isEnabled())))
    }

    @Test
    fun google_button_is_disabled() {
        onView(withId(R.id.welcome_fragment_google_button)).check(matches(not(isEnabled())))
    }
}