/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: MainActivityTest.kt
 * Author: sdelic
 *
 * History:  1.3.22. 12:57 created
 */

package com.comtrade.vinciapp.view.activity.main.view

import android.view.Gravity
import androidx.test.core.app.launchActivity
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.DrawerMatchers.isClosed
import androidx.test.espresso.contrib.DrawerMatchers.isOpen
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.filters.LargeTest
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.end_to_end.main_screen.MainActivityAuthSetup
import com.comtrade.vinciapp.util.assertDescendantWithText
import com.comtrade.vinciapp.util.assertViewsDisplayed
import com.comtrade.vinciapp.util.assertViewsNotDisplayed
import org.junit.Test

/**
 * Main activity test suite. This is not the first run state of the view component,
 * a special test scenario will be provided for the that.
 */
@LargeTest
internal class MainActivityTest : MainActivityAuthSetup() {

    override fun setUp() {
        super.setUp()
        launchActivity<MainActivity>()
    }

    @Test
    fun main_activity_content_is_showing() {
        assertViewsDisplayed(
            R.id.main_activity_menu_background,
            R.id.main_activity_menu_container,
            R.id.main_activity_menu_button,
            R.id.main_activity_content,
            R.id.img_sync_status
        )

        assertViewsNotDisplayed(
            R.id.main_activity_back_button
        )
    }

    @Test
    fun main_fragment_is_showing() {
        assertViewsDisplayed(
            R.id.main_screen_fragment_profile_image_backgound,
            R.id.main_screen_fragment_profile_image,
            R.id.main_screen_fragment_devices_background_border,
            R.id.main_screen_fragment_devices_background,
            R.id.main_screen_fragment_devices_icon,
            R.id.main_screen_fragment_profile_name,
            R.id.main_screen_fragment_points_label,
            R.id.main_screen_fragment_points,
            R.id.main_screen_fragment_event_background_image,
            R.id.main_screen_fragment_event_text_view,
            R.id.main_screen_fragment_quality_of_life_container,
            R.id.main_screen_fragment_friends_container,
            R.id.main_screen_fragment_emergency_call_container,
            R.id.main_screen_fragment_physical_activity_container,
            R.id.main_screen_fragment_cognitive_games_container,
            R.id.main_screen_fragment_today_feelings_container
        )
    }

    @Test
    fun on_menu_click_drawer_is_displayed() {
        onView(withId(R.id.drawer_layout)).check(matches(isClosed(Gravity.START)))
        onView(withId(R.id.main_activity_menu_button)).perform(click())
        onView(withId(R.id.drawer_layout)).check(matches(isOpen(Gravity.START)))

        assertViewsDisplayed(R.id.navigation_view)

        // Assert header views. Name field is optional.
        assertViewsDisplayed(
            R.id.navigation_header_profile_image_backgound,
            R.id.navigation_header_profile_image,
            R.id.navigation_header_profile_email
        )

        // Assert menu items
        val menuItemLabels = intArrayOf(
            R.string.navigation_screen_home,
            R.string.navigation_screen_profile,
            R.string.navigation_screen_questionnaire,
            R.string.navigation_screen_devices,
            R.string.navigation_screen_settings,
            R.string.navigation_screen_logout,
            R.string.navigation_screen_about
        )
        assertDescendantWithText(R.id.navigation_view, menuItemLabels)

    }
}