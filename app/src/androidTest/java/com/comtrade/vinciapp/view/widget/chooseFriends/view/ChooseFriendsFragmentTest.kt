/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: ChooseFriendsFragmentTest.kt
 * Author: sdelic
 *
 * History:  8.3.22. 14:10 created
 */

package com.comtrade.vinciapp.view.widget.chooseFriends.view

import android.Manifest
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.rule.GrantPermissionRule
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.util.assertViewsDisplayed
import org.junit.Before
import org.junit.Rule
import org.junit.Test

/**
 * ChooseFriendsFragment screen UI tests.
 * Assert all of the view components are displayed.
 * Assert that all views are in their correct initial state.
 */
internal class ChooseFriendsFragmentTest {
    @Rule
    @JvmField
    val readContactsPermissionRule: GrantPermissionRule =
        GrantPermissionRule.grant(Manifest.permission.READ_CONTACTS)

    @Before
    fun setUp() {
        launchFragmentInContainer<ChooseFriendsFragment>()
    }

    @Test
    fun choose_friends_screen_is_displayed() {

        // Assert choose friends contact screen
        assertViewsDisplayed(
            R.id.choose_friends_fragment_list_view,
            R.id.choose_friends_fragment_buttons_container,
            R.id.choose_friends_fragment_buttons_first_container,
            R.id.choose_friends_fragment_back_button,
            R.id.choose_friends_fragment_save_button,
            R.id.choose_friends_fragment_buttons_second_container,
            R.id.choose_friends_fragment_invite_button
        )
    }

    @Test
    fun buttons_have_correct_captions() {
        onView(withId(R.id.choose_friends_fragment_back_button))
            .check(matches(withText(R.string.common_button_back)))

        onView(withId(R.id.choose_friends_fragment_save_button))
            .check(matches(withText(R.string.common_button_save)))

        onView(withId(R.id.choose_friends_fragment_invite_button))
            .check(matches(withText(R.string.choose_friends_screen_invite_button_text)))
    }
}