/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: ProfilePictureFragmentTest.kt
 * Author: sdelic
 *
 * History:  16.3.22. 12:42 created
 */

package com.comtrade.vinciapp.view.widget.profilePicture.view

import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isEnabled
import androidx.test.espresso.matcher.ViewMatchers.withId
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.TestVinciApp
import com.comtrade.vinciapp.util.assertButtonsEnabled
import com.comtrade.vinciapp.util.assertViewHasCaption
import com.comtrade.vinciapp.util.assertViewsDisplayed
import org.hamcrest.CoreMatchers.not
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.`when`

/**
 * ProfilePictureFragment screen UI tests.
 * Assert all of the view components are displayed.
 * Assert that all views are in their correct initial state.
 */
internal class ProfilePictureFragmentTest {

    @Before
    fun setUp() {
        val application = ApplicationProvider.getApplicationContext<TestVinciApp>()
        val prefs = application.appComponent.provideAppPrefs()
        //Setting profile image state to default empty state.
        `when`(prefs.userProfilePicture).thenReturn("")
        launchFragmentInContainer<ProfilePictureFragment>()
    }

    @Test
    fun profile_picture_screen_is_displayed() {
        assertViewsDisplayed(
            R.id.profile_picture_fragment_profile_image_backgound,
            R.id.profile_picture_fragment_profile_image,
            R.id.profile_picture_fragment_user_image_view,
            R.id.profile_picture_fragment_take_picture_button,
            R.id.profile_picture_fragment_choose_existing_button,
            R.id.profile_picture_fragment_save_profile_picture_button,
            R.id.profile_picture_fragment_delete_profile_picture_button
        )
    }

    @Test
    fun profile_picture_buttons_are_enabled() {
        assertButtonsEnabled(
            R.id.profile_picture_fragment_take_picture_button,
            R.id.profile_picture_fragment_choose_existing_button,
            R.id.profile_picture_fragment_save_profile_picture_button,
        )
    }

    @Test
    fun profile_picture_delete_button_is_disabled_at_start() {
        onView(withId(R.id.profile_picture_fragment_delete_profile_picture_button))
            .check(matches(not(isEnabled())))
    }

    @Test
    fun take_picture_button_has_correct_caption() {
        assertViewHasCaption(
            R.id.profile_picture_fragment_take_picture_button,
            R.string.profile_picture_screen_take_picture_button_text
        )
    }

    @Test
    fun choose_existing_button_has_correct_caption() {
        assertViewHasCaption(
            R.id.profile_picture_fragment_choose_existing_button,
            R.string.profile_picture_screen_choose_existing_button_text
        )
    }

    @Test
    fun save_profile_picture_button_has_correct_caption() {
        assertViewHasCaption(
            R.id.profile_picture_fragment_save_profile_picture_button,
            R.string.profile_picture_screen_save_profile_picture_button_text
        )
    }

    @Test
    fun delete_button_has_correct_caption() {
        assertViewHasCaption(
            R.id.profile_picture_fragment_delete_profile_picture_button,
            R.string.profile_picture_screen_delete_profile_picture_button_text
        )
    }
}