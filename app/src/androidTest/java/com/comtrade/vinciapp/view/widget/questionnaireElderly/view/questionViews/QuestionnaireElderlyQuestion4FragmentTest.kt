/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: QuestionnaireElderlyQuestion4FragmentTest.kt
 * Author: sdelic
 *
 * History:  29.3.22. 12:43 created
 */

package com.comtrade.vinciapp.view.widget.questionnaireElderly.view.questionViews

import android.text.InputType
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.espresso.Espresso
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.util.assertViewHasCaption
import com.comtrade.vinciapp.util.assertViewsDisplayed
import org.hamcrest.core.AllOf
import org.junit.Before
import org.junit.Test

/**
 * QuestionnaireElderlyQuestion4Fragment screen UI tests.
 * Assert all of the view components are displayed.
 * Assert that all views are in their correct initial state.
 */
internal class QuestionnaireElderlyQuestion4FragmentTest {
    @Before
    fun setUp() {
        launchFragmentInContainer<QuestionnaireElderlyQuestion4Fragment>()
    }

    @Test
    fun elderly_question_4_screen_is_displayed() {
        assertViewsDisplayed(
            R.id.elderly_questionnaire_question_4,
            R.id.elderly_questionnaire_question_4_answer_1,
            R.id.elderly_questionnaire_question_4_answer_2,
            R.id.elderly_questionnaire_question_4_answer_3,
            R.id.elderly_questionnaire_question_4_answer_4,
            R.id.elderly_questionnaire_question_4_answer_5,
            R.id.elderly_questionnaire_question_4_answer_6,
            R.id.elderly_questionnaire_question_4_answer_7,
            R.id.elderly_questionnaire_question_4_answer_8_label,
            R.id.elderly_questionnaire_question_4_answer_8
        )
    }

    @Test
    fun elderly_question_4_has_correct_text() {
        assertViewHasCaption(
            R.id.elderly_questionnaire_question_4,
            R.string.questionnaire_elderly_question_4_text
        )
    }

    @Test
    fun answers_are_unchecked_at_start() {
        val answerCheckBoxIds = arrayOf(
            R.id.elderly_questionnaire_question_4_answer_1,
            R.id.elderly_questionnaire_question_4_answer_2,
            R.id.elderly_questionnaire_question_4_answer_3,
            R.id.elderly_questionnaire_question_4_answer_4,
            R.id.elderly_questionnaire_question_4_answer_5,
            R.id.elderly_questionnaire_question_4_answer_6,
            R.id.elderly_questionnaire_question_4_answer_7,
        )

        for (answerCheckBoxId in answerCheckBoxIds) {
            Espresso.onView(ViewMatchers.withId(answerCheckBoxId))
                .check(ViewAssertions.matches(ViewMatchers.isNotChecked()))
        }
    }

    @Test
    fun answers_have_correct_text() {
        val answers =
            mapOf(
                R.id.elderly_questionnaire_question_4_answer_1 to R.string.questionnaire_elderly_question_4_answer_1,
                R.id.elderly_questionnaire_question_4_answer_2 to R.string.questionnaire_elderly_question_4_answer_2,
                R.id.elderly_questionnaire_question_4_answer_3 to R.string.questionnaire_elderly_question_4_answer_3,
                R.id.elderly_questionnaire_question_4_answer_4 to R.string.questionnaire_elderly_question_4_answer_4,
                R.id.elderly_questionnaire_question_4_answer_5 to R.string.questionnaire_elderly_question_4_answer_5,
                R.id.elderly_questionnaire_question_4_answer_6 to R.string.questionnaire_elderly_question_4_answer_6,
                R.id.elderly_questionnaire_question_4_answer_7 to R.string.questionnaire_elderly_question_4_answer_7
            )

        for (answer in answers) {
            assertViewHasCaption(answer.key, answer.value)
        }
    }

    @Test
    fun name_it_text_is_displayed() {
        assertViewHasCaption(
            R.id.elderly_questionnaire_question_4_answer_8_label,
            R.string.questionnaire_elderly_question_4_answer_8
        )
    }

    @Test
    fun name_it_input_field_has_correct_hint_and_initial_state() {

        Espresso.onView(ViewMatchers.withId(R.id.elderly_questionnaire_question_4_answer_8)).check(
            ViewAssertions.matches(
                AllOf.allOf(
                    ViewMatchers.withText(""),
                    ViewMatchers.withInputType(InputType.TYPE_CLASS_TEXT)
                )
            )
        )
    }
}