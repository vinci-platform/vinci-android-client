/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: QuestionnaireCheckboxQuestionTest.kt
 * Author: sdelic
 *
 * History:  29.3.22. 14:45 created
 */

package com.comtrade.vinciapp.view.widget.questionnaireUserNeeds.view.questionViews

import android.content.Context
import android.text.InputType
import androidx.core.os.bundleOf
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.util.assertViewHasCaption
import com.comtrade.vinciapp.util.assertViewsDisplayed
import com.comtrade.vinciapp.view.widget.questionnaireSurvey.model.SurveyQuestionnaireItem
import org.hamcrest.core.AllOf
import org.junit.Before
import org.junit.Test

/**
 * QuestionnaireCheckboxQuestion screen UI tests.
 * Assert all of the view components are displayed.
 * Assert that all views are in their correct initial state.
 */
internal class QuestionnaireCheckboxQuestionTest {

    private lateinit var context: Context

    @Before
    fun setUp() {
        context = ApplicationProvider.getApplicationContext()

        val testQuestionId =
            context.getString(R.string.user_needs_questionnaire_screen_question_a7_id)
        val testQuestion =
            context.getString(R.string.user_needs_questionnaire_screen_question_a7_text)
        val testAnswers =
            context.resources.getStringArray(R.array.user_needs_variants_communication)
        val questionItem = SurveyQuestionnaireItem(
            testQuestionId,
            testQuestionId,
            testQuestion,
            true,
            testAnswers
        )
        val arguments = bundleOf(
            QuestionnaireCheckboxQuestion.QUESTION_ID_KEY to questionItem.questionId,
            QuestionnaireCheckboxQuestion.QUESTION_NUMBER_KEY to questionItem.questionNumber,
            QuestionnaireCheckboxQuestion.QUESTION_KEY to questionItem.question,
            QuestionnaireCheckboxQuestion.ORDER_KEY to questionItem.order,
            QuestionnaireCheckboxQuestion.ANSWERS_KEY to questionItem.answers,
        )
        launchFragmentInContainer<QuestionnaireCheckboxQuestion>(arguments)
    }

    @Test
    fun questionnaire_checkbox_question_screen_is_displayed() {
        assertViewsDisplayed(
            R.id.view_questionnaire_checkbox_question_container,
            R.id.view_questionnaire_checkbox_question_number,
            R.id.view_questionnaire_checkbox_question_description,
            R.id.view_questionnaire_checkbox_one,
            R.id.view_questionnaire_checkbox_two,
            R.id.view_questionnaire_checkbox_three,
            R.id.view_questionnaire_checkbox_four,
            R.id.view_questionnaire_checkbox_five,
            R.id.view_questionnaire_checkbox_six,
            R.id.view_questionnaire_checkbox_seven,
            R.id.view_questionnaire_checkbox_eight,
            R.id.view_questionnaire_checkbox_eight_edit_text,
            R.id.view_questionnaire_checkbox_nine
        )
    }

    @Test
    fun question_text_is_displayed() {
        assertViewHasCaption(
            R.id.view_questionnaire_checkbox_question_description,
            R.string.user_needs_questionnaire_screen_question_a7_text
        )
    }

    @Test
    fun checkboxes_are_unchecked_at_start() {
        val answerCheckBoxIds = arrayOf(
            R.id.view_questionnaire_checkbox_one,
            R.id.view_questionnaire_checkbox_two,
            R.id.view_questionnaire_checkbox_three,
            R.id.view_questionnaire_checkbox_four,
            R.id.view_questionnaire_checkbox_five,
            R.id.view_questionnaire_checkbox_six,
            R.id.view_questionnaire_checkbox_seven,
            R.id.view_questionnaire_checkbox_eight,
            R.id.view_questionnaire_checkbox_nine
        )

        for (answerCheckBoxId in answerCheckBoxIds) {
            Espresso.onView(ViewMatchers.withId(answerCheckBoxId))
                .check(ViewAssertions.matches(ViewMatchers.isNotChecked()))
        }
    }

    @Test
    fun question_eight_input_field_has_correct_hint_and_initial_state() {
        Espresso.onView(ViewMatchers.withId(R.id.view_questionnaire_checkbox_eight_edit_text))
            .check(
                ViewAssertions.matches(
                    AllOf.allOf(
                        ViewMatchers.withText(""),
                        ViewMatchers.withInputType(InputType.TYPE_CLASS_TEXT)
                    )
                )
            )
    }

    @Test
    fun answers_have_correct_text() {
        val testAnswers =
            context.resources.getStringArray(R.array.user_needs_variants_communication)
        val answers =
            mapOf(
                R.id.view_questionnaire_checkbox_one to testAnswers[0],
                R.id.view_questionnaire_checkbox_two to testAnswers[1],
                R.id.view_questionnaire_checkbox_three to testAnswers[2],
                R.id.view_questionnaire_checkbox_four to testAnswers[3],
                R.id.view_questionnaire_checkbox_five to testAnswers[4],
                R.id.view_questionnaire_checkbox_six to testAnswers[5],
                R.id.view_questionnaire_checkbox_seven to testAnswers[6],
                R.id.view_questionnaire_checkbox_eight to testAnswers[7],
                R.id.view_questionnaire_checkbox_nine to testAnswers[8],
            )

        for (answer in answers) {
            Espresso.onView(ViewMatchers.withId(answer.key)).check(
                ViewAssertions.matches(
                    ViewMatchers.withText(
                        answer.value
                    )
                )
            )
        }
    }
}