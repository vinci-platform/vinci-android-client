/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: PhysicalActivitiesFragmentTest.kt
 * Author: sdelic
 *
 * History:  9.3.22. 11:00 created
 */

package com.comtrade.vinciapp.view.widget.physicalActivities.view

import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.util.assertViewsDisplayed
import org.hamcrest.CoreMatchers.allOf
import org.junit.Before
import org.junit.Test

/**
 * PhysicalActivitiesFragment screen UI tests.
 * Assert all of the view components are displayed.
 * Assert that all views are in their correct initial state.
 */
internal class PhysicalActivitiesFragmentTest {

    @Before
    fun setUp() {
        launchFragmentInContainer<PhysicalActivitiesFragment>()
    }

    @Test
    fun physical_activities_screen_is_displayed() {
        assertViewsDisplayed(
            R.id.physical_activity_fragment_step_count_container,
            R.id.physical_activity_fragment_outdoor_activities_container,
            R.id.physical_activity_fragment_heart_rate_container,
            R.id.physical_activity_fragment_calories_container,
            R.id.physical_activity_fragment_sleep_container,
            R.id.physical_activity_fragment_health_points_container,
            R.id.physical_activity_fragment_IPAQ_button
        )
    }

    @Test
    fun activities_have_correct_captions() {
        val activityContainersWithCaptions = mapOf(
            R.id.physical_activity_fragment_step_count_container to R.string.main_screen_step_count_text,
            R.id.physical_activity_fragment_outdoor_activities_container to R.string.main_screen_outdoor_activities_text,
            R.id.physical_activity_fragment_heart_rate_container to R.string.main_screen_heart_rate_text,
            R.id.physical_activity_fragment_calories_container to R.string.main_screen_calories_text,
            R.id.physical_activity_fragment_sleep_container to R.string.main_screen_sleep_text,
            R.id.physical_activity_fragment_health_points_container to R.string.main_screen_health_points_text,
        )

        for (activityWithCaption in activityContainersWithCaptions) {
            onView(withId(activityWithCaption.key)).check(
                matches(
                    allOf(
                        hasChildCount(2),
                        hasDescendant(
                            withText(
                                activityWithCaption.value
                            )
                        )
                    )
                )
            )
        }
    }

    @Test
    fun ipaq_button_has_correct_caption() {
        onView(withId(R.id.physical_activity_fragment_IPAQ_button)).check(matches(withText(R.string.IPA_questionnaire_button_text)))
    }

}