/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: SettingEmergencyNumberFragmentTest.kt
 * Author: sdelic
 *
 * History:  1.3.22. 15:44 created
 */

package com.comtrade.vinciapp.view.widget.settingEmergencyNumber.view

import android.text.InputType
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.util.assertEditFieldForHintAndInputType
import com.comtrade.vinciapp.util.assertViewsDisplayed
import org.hamcrest.core.AllOf.allOf
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.`when`

/**
 * SettingEmergencyNumberFragment screen UI tests.
 * Assert all of the view components are displayed.
 * Assert that all views are in their correct initial state.
 * Assert input fields.
 * Assert error display.
 */
internal class SettingEmergencyNumberFragmentTest {

    private lateinit var prefs: IPrefs

    @Before
    fun setUp() {

        val fragmentScenario = launchFragmentInContainer<SettingEmergencyNumberFragment>()
        fragmentScenario.onFragment {
            prefs = it.prefs
            `when`(prefs.emergencyNumber).thenReturn("")
        }
    }

    @Test
    fun setting_emergency_number_screen_is_displayed() {
        assertViewsDisplayed(
            R.id.setting_emergency_number_vertical_layout,
            R.id.setting_emergency_number_choose_contact_text,
            R.id.setting_emergency_number_choose_contact_button,
            R.id.setting_emergency_number_phone_number_layout,
            R.id.setting_emergency_number_phone_number,
            R.id.setting_emergency_number_button_layout,
            R.id.setting_emergency_number_cancel_button,
            R.id.setting_emergency_number_ok_button
        )
    }

    @Test
    fun correct_text_is_displayed() {

        val textResources = intArrayOf(
            R.string.setting_emergency_number_screen_text,
            R.string.setting_emergency_number_choose_contact_button,
            R.string.setting_emergency_number_or_add_text,
            R.string.common_button_cancel,
            R.string.common_button_ok
        )

        for (textResource in textResources) {
            onView(withText(textResource)).check(matches(isDisplayed()))
        }
    }

    @Test
    fun emergency_number_field_is_in_correct_state() {

        assertEditFieldForHintAndInputType(
            R.id.setting_emergency_number_phone_number,
            R.string.setting_emergency_number_phone_number_hint,
            InputType.TYPE_CLASS_TEXT or InputType.TYPE_CLASS_PHONE
        )
    }

    @Test
    fun click_ok_shows_error_for_empty_text() {
        // Remove any text displayed in the phone input field.
        onView(withId(R.id.setting_emergency_number_phone_number)).perform(clearText())
        onView(withId(R.id.setting_emergency_number_ok_button)).perform(click())
        // When input field empty, the error message should be shown.
        onView(
            allOf(
                withId(R.id.setting_emergency_number_warning_text),
                withText(R.string.setting_emergency_number_phone_number_empty_warning)
            )
        ).check(matches(isDisplayed()))
    }

    @Test
    fun click_ok_shows_error_for_invalid_phone_entry_text() {
        val invalidPhoneNumberCases = arrayOf(
            "-63563298",
            "-1",
            "0",
            "1234",
            "99999999999999999",
        )

        for (inputNumberCase in invalidPhoneNumberCases) {
            // Remove any text displayed in the phone input field.
            onView(withId(R.id.setting_emergency_number_phone_number)).perform(clearText())
            onView(withId(R.id.setting_emergency_number_phone_number)).perform(
                typeText(
                    inputNumberCase
                ), closeSoftKeyboard()
            )

            onView(withId(R.id.setting_emergency_number_ok_button)).perform(click())
            // When input field empty, the error message should be shown.
            onView(
                allOf(
                    withId(R.id.setting_emergency_number_warning_text),
                    withText(R.string.setting_emergency_number_phone_number_invalid_warning)
                )
            ).check(matches(isDisplayed()))
        }
    }

}