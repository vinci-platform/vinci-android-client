/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: HistoryGraphScreenTest.kt
 * Author: sdelic
 *
 * History:  14.3.22. 14:45 created
 */

package com.comtrade.vinciapp.view.widget.historyGraph.view

import androidx.core.os.bundleOf
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.util.assertViewsDisplayed
import org.junit.Before
import org.junit.Test

/**
 * HistoryGraphScreen screen UI tests.
 * Assert all of the view components are displayed.
 * Assert that all views are in their correct initial state.
 */
internal class HistoryGraphScreenTest {


    private val title = "Test Graph Screen Title"
    private val firstTitle = "Test First Title"
    private val firstValue = "0"

    private val secondTitle = "Test Second Title"
    private val secondValue = "0"

    @Before
    fun setUp() {
        //Providing test data
        val arguments = bundleOf(
            HistoryGraphScreen.LABELS_KEY to emptyArray<String>(),
            HistoryGraphScreen.DATA_KEY to hashMapOf<String, Array<Int>>(),
            HistoryGraphScreen.TITLE_KEY to title,
            HistoryGraphScreen.FIRST_TITLE_KEY to firstTitle,
            HistoryGraphScreen.FIRST_VALUE_KEY to firstValue,
            HistoryGraphScreen.SECOND_TITLE_KEY to secondTitle,
            HistoryGraphScreen.SECOND_VALUE_KEY to secondValue
        )
        launchFragmentInContainer<HistoryGraphScreen>(arguments)
    }

    @Test
    fun history_graph_screen_is_displayed() {
        assertViewsDisplayed(
            R.id.history_graph_first_title,
            R.id.history_graph_first_value,
            R.id.history_graph_second_title,
            R.id.history_graph_second_value,
            R.id.AAChartView
        )
    }

    @Test
    fun first_title_and_value_correct_captions() {
        onView(withId(R.id.history_graph_first_title)).check(matches(withText(firstTitle)))
        onView(withId(R.id.history_graph_first_value)).check(matches(withText(firstValue)))
    }

    @Test
    fun second_title_and_value_correct_captions() {
        onView(withId(R.id.history_graph_second_title)).check(matches(withText(secondTitle)))
        onView(withId(R.id.history_graph_second_value)).check(matches(withText(secondValue)))
    }
}