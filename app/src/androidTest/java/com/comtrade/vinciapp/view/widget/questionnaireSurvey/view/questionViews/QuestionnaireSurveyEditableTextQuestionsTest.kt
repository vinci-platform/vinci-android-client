/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: QuestionnaireSurveyEditableTextQuestionsTest.kt
 * Author: sdelic
 *
 * History:  21.6.22. 08:25 created
 */

package com.comtrade.vinciapp.view.widget.questionnaireSurvey.view.questionViews

import android.text.InputType
import androidx.annotation.IdRes
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.espresso.Espresso
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.util.assertViewHasCaption
import com.comtrade.vinciapp.util.assertViewsDisplayed
import org.hamcrest.core.AllOf
import org.junit.Before
import org.junit.Test

/**
 * QuestionnaireSurveyEditableTextQuestions screen UI tests.
 * Assert all of the view components are displayed.
 * Assert that all views are in their correct initial state.
 */
internal class QuestionnaireSurveyEditableTextQuestionsTest {

    @Before
    fun setUp() {
        launchFragmentInContainer<QuestionnaireSurveyEditableTextQuestions>()
    }

    @Test
    fun questionnaire_survey_editable_text_questions_is_displayed() {
        assertViewsDisplayed(
            R.id.survey_questionnaire_screen_question_scroll,
            R.id.survey_questionnaire_screen_question_27_number,
            R.id.survey_questionnaire_screen_question_27_text,
            R.id.survey_questionnaire_screen_question_27_answer,
            R.id.survey_questionnaire_screen_question_28_number,
            R.id.survey_questionnaire_screen_question_28_text,
            R.id.survey_questionnaire_screen_question_28_answer,
            R.id.survey_questionnaire_screen_question_29_number,
            R.id.survey_questionnaire_screen_question_29_text,
            R.id.survey_questionnaire_screen_question_29_answer
        )
    }

    @Test
    fun question_1_has_correct_text() {
        assertViewHasCaption(
            R.id.survey_questionnaire_screen_question_27_number,
            R.string.survey_questionnaire_screen_question_27_number
        )

        assertViewHasCaption(
            R.id.survey_questionnaire_screen_question_27_text,
            R.string.survey_questionnaire_screen_question_26_text
        )
    }

    @Test
    fun question_2_has_correct_text() {
        assertViewHasCaption(
            R.id.survey_questionnaire_screen_question_28_number,
            R.string.survey_questionnaire_screen_question_28_number
        )

        assertViewHasCaption(
            R.id.survey_questionnaire_screen_question_28_text,
            R.string.survey_questionnaire_screen_question_27_text
        )
    }

    @Test
    fun question_3_has_correct_text() {
        assertViewHasCaption(
            R.id.survey_questionnaire_screen_question_29_number,
            R.string.survey_questionnaire_screen_question_29_number
        )

        assertViewHasCaption(
            R.id.survey_questionnaire_screen_question_29_text,
            R.string.survey_questionnaire_screen_question_28_text
        )
    }

    @Test
    fun input_fields_have_correct_hints_and_initial_state() {
        assertEditFieldForEmptyAndInputType(
            R.id.survey_questionnaire_screen_question_27_answer,
            InputType.TYPE_CLASS_TEXT
        )

        assertEditFieldForEmptyAndInputType(
            R.id.survey_questionnaire_screen_question_28_answer,
            InputType.TYPE_CLASS_TEXT
        )

        assertEditFieldForEmptyAndInputType(
            R.id.survey_questionnaire_screen_question_29_answer,
            InputType.TYPE_CLASS_TEXT
        )
    }

    /**
     * Espresso assertion of input fields for the empty state and input type.
     */
    private fun assertEditFieldForEmptyAndInputType(
        @IdRes viewId: Int,
        inputType: Int
    ) {
        Espresso.onView(ViewMatchers.withId(viewId)).check(
            ViewAssertions.matches(
                AllOf.allOf(
                    ViewMatchers.withText(""),
                    ViewMatchers.withInputType(inputType)
                )
            )
        )
    }
}