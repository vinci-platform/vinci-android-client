/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: QuestionnaireRadioButtonQuestionTest.kt
 * Author: sdelic
 *
 * History:  29.3.22. 16:14 created
 */

package com.comtrade.vinciapp.view.widget.questionnaireUserNeeds.view.questionViews

import android.content.Context
import androidx.core.os.bundleOf
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.util.assertViewHasCaption
import com.comtrade.vinciapp.util.assertViewsDisplayed
import com.comtrade.vinciapp.view.widget.questionnaireSurvey.model.SurveyQuestionnaireItem
import org.junit.Before
import org.junit.Test

/**
 * QuestionnaireRadioButtonQuestion screen UI tests.
 * Assert all of the view components are displayed.
 * Assert that all views are in their correct initial state.
 */
internal class QuestionnaireRadioButtonQuestionTest {

    private lateinit var context: Context

    @Before
    fun setUp() {
        context = ApplicationProvider.getApplicationContext()

        val testQuestionId =
            context.getString(R.string.user_needs_questionnaire_screen_question_a7_id)
        val testQuestion =
            context.getString(R.string.user_needs_questionnaire_screen_question_a7_text)
        val testAnswers =
            context.resources.getStringArray(R.array.user_needs_variants_communication)
        val questionItem = SurveyQuestionnaireItem(
            testQuestionId,
            testQuestionId,
            testQuestion,
            true,
            testAnswers
        )
        val arguments = bundleOf(
            QuestionnaireCheckboxQuestion.QUESTION_ID_KEY to questionItem.questionId,
            QuestionnaireCheckboxQuestion.QUESTION_NUMBER_KEY to questionItem.questionNumber,
            QuestionnaireCheckboxQuestion.QUESTION_KEY to questionItem.question,
            QuestionnaireCheckboxQuestion.ORDER_KEY to questionItem.order,
            QuestionnaireCheckboxQuestion.ANSWERS_KEY to questionItem.answers,
        )
        launchFragmentInContainer<QuestionnaireRadioButtonQuestion>(arguments)
    }

    @Test
    fun questionnaire_radio_button_screen_is_displayed() {
        assertViewsDisplayed(
            R.id.view_questionnaire_radio_button_question_container,
            R.id.view_questionnaire_radio_button_question_number,
            R.id.view_questionnaire_radio_button_question_description,
            R.id.view_questionnaire_screen_radio_group,
            R.id.view_questionnaire_screen_radio_button_one,
            R.id.view_questionnaire_screen_radio_button_two,
            R.id.view_questionnaire_screen_radio_button_three,
            R.id.view_questionnaire_screen_radio_button_four,
            R.id.view_questionnaire_screen_radio_button_five,
            R.id.view_questionnaire_screen_radio_button_six,
        )
    }

    @Test
    fun question_text_is_displayed() {
        assertViewHasCaption(
            R.id.view_questionnaire_radio_button_question_description,
            R.string.user_needs_questionnaire_screen_question_a7_text
        )
    }

    @Test
    fun radio_buttons_are_unchecked_at_start() {
        val answerCheckBoxIds = arrayOf(
            R.id.view_questionnaire_screen_radio_button_one,
            R.id.view_questionnaire_screen_radio_button_two,
            R.id.view_questionnaire_screen_radio_button_three,
            R.id.view_questionnaire_screen_radio_button_four,
            R.id.view_questionnaire_screen_radio_button_five,
            R.id.view_questionnaire_screen_radio_button_six,
        )

        for (answerCheckBoxId in answerCheckBoxIds) {
            Espresso.onView(ViewMatchers.withId(answerCheckBoxId))
                .check(ViewAssertions.matches(ViewMatchers.isNotChecked()))
        }
    }

    @Test
    fun answers_have_correct_text() {
        val testAnswers =
            context.resources.getStringArray(R.array.user_needs_variants_communication)
        val answers =
            mapOf(
                R.id.view_questionnaire_screen_radio_button_one to testAnswers[0],
                R.id.view_questionnaire_screen_radio_button_two to testAnswers[1],
                R.id.view_questionnaire_screen_radio_button_three to testAnswers[2],
                R.id.view_questionnaire_screen_radio_button_four to testAnswers[3],
                R.id.view_questionnaire_screen_radio_button_five to testAnswers[4],
                R.id.view_questionnaire_screen_radio_button_six to testAnswers[5],
            )

        for (answer in answers) {
            Espresso.onView(ViewMatchers.withId(answer.key)).check(
                ViewAssertions.matches(
                    ViewMatchers.withText(
                        answer.value
                    )
                )
            )
        }
    }
}