/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: ForgotPasswordFinishFragmentTest.kt
 * Author: sdelic
 *
 * History:  11.4.22. 11:32 created
 */

package com.comtrade.vinciapp.view.widget.forgotPassword.view

import android.text.InputType
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.espresso.Espresso
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.util.assertEditFieldForHintAndInputType
import com.comtrade.vinciapp.util.assertViewHasCaption
import com.comtrade.vinciapp.util.assertViewsDisplayed
import com.comtrade.vinciapp.util.assertViewsNotDisplayed
import org.junit.Before
import org.junit.Test

/**
 * ForgotPasswordFragment screen UI tests.
 * Assert all of the view components are displayed.
 * Assert that all views are in their correct initial state.
 * Assert input field.
 * Assert progress bar state when login button is pressed.
 */
internal class ForgotPasswordFinishFragmentTest {

    @Before
    fun setUp() {
        launchFragmentInContainer<ForgotPasswordFinishFragment>()
    }

    @Test
    fun forgot_password_finish_screen_is_displayed() {
        assertViewsDisplayed(
            R.id.includable_logo_image,
            R.id.activation_email_text,
            R.id.activation_key_edit_text,
            R.id.new_password_edit_text,
            R.id.confirm_password_edit_text,
            R.id.reset_password_button
        )
    }

    @Test
    fun progress_view_is_hidden_at_start() {
        assertViewsNotDisplayed(
            R.id.containerProgress,
            R.id.progressBar
        )
    }

    @Test
    fun activation_email_view_has_correct_text() {
        assertViewHasCaption(
            R.id.activation_email_text,
            R.string.forgot_password_finish_screen_activation_email_text
        )
    }

    @Test
    fun activation_key_field_has_correct_initial_state() {
        assertEditFieldForHintAndInputType(
            R.id.activation_key_edit_text,
            R.string.forgot_password_finish_screen_activation_key_hint,
            InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS
        )
    }

    @Test
    fun new_password_input_fields_has_correct_hint_and_initial_state() {
        assertEditFieldForHintAndInputType(
            R.id.new_password_edit_text,
            R.string.create_new_password_screen_new_password_hint,
            InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
        )
    }

    @Test
    fun confirm_new_password_input_fields_has_correct_hint_and_initial_state() {
        assertEditFieldForHintAndInputType(
            R.id.confirm_password_edit_text,
            R.string.create_new_password_screen_confirm_password_hint,
            InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
        )
    }

    @Test
    fun reset_password_button_is_enabled() {
        Espresso.onView(ViewMatchers.withId(R.id.reset_password_button))
            .check(ViewAssertions.matches(ViewMatchers.isEnabled()))
    }

    @Test
    fun change_password_button_has_correct_caption() {
        assertViewHasCaption(
            R.id.reset_password_button,
            R.string.forgot_password_screen_reset_button_text
        )
    }
}