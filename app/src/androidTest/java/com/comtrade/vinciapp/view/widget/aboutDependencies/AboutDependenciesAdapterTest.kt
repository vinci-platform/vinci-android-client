/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: AboutDependenciesAdapterTest.kt
 * Author: sdelic
 *
 * History:  1.4.22. 10:11 created
 */

package com.comtrade.vinciapp.view.widget.aboutDependencies

import android.content.Context
import android.view.LayoutInflater
import androidx.test.core.app.ApplicationProvider
import com.comtrade.domain.entities.legal.DependencyInfo
import com.comtrade.domain.entities.legal.LicenseInfo
import com.comtrade.vinciapp.R
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito

/**
 * About dependencies feature RecyclerView adapter test suite.
 * Assert valid data provided.
 * Assert correct dataset size.
 * Assert correct item data set.
 */
internal class AboutDependenciesAdapterTest {
    private lateinit var aboutDependenciesAdapter: AboutDependenciesAdapter
    private lateinit var mockItemClickCallback: AboutDependenciesAdapter.DependencyItemOnClickListener

    @Before
    fun setUp() {
        mockItemClickCallback =
            Mockito.mock(AboutDependenciesAdapter.DependencyItemOnClickListener::class.java)
        aboutDependenciesAdapter = AboutDependenciesAdapter(mockItemClickCallback)
    }

    @Test
    fun adapter_has_correct_item_count() {
        //Initial count is 0.
        assertEquals(0, aboutDependenciesAdapter.itemCount)
        aboutDependenciesAdapter.updateDataSet(getTestDependencyData(100))

        assertEquals(100, aboutDependenciesAdapter.itemCount)

        //After adding new data.
        aboutDependenciesAdapter.updateDataSet(getTestDependencyData(90))

        assertEquals(90, aboutDependenciesAdapter.itemCount)
    }

    @Test
    fun when_view_holder_bound_item_has_correct_text_and_click_event() {
        val testDataSet = getTestDependencyData(1)
        val testItem = testDataSet[0]
        aboutDependenciesAdapter.updateDataSet(testDataSet)
        assertEquals(1, aboutDependenciesAdapter.itemCount)


        val inflater = ApplicationProvider.getApplicationContext<Context>()
            .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val itemView = inflater.inflate(R.layout.about_dependencies_list_item, null, false)
        val holder = AboutDependenciesAdapter(mockItemClickCallback).ViewHolder(itemView)
        aboutDependenciesAdapter.onBindViewHolder(holder, 0)

        assertEquals(testItem.name, holder.dependencyName.text)
        assertEquals(testItem.description, holder.dependencyDescription.text)
        assertEquals(testItem.license[0].name, holder.dependencyLicense.text)
        assertEquals(testItem.url, holder.dependencyUrl.text)

    }

    /**
     * Fake test data.
     */
    private fun getTestDependencyData(itemCount: Int): Array<DependencyInfo> {
        val testData = mutableListOf<DependencyInfo>()

        for (i in 0 until itemCount) {
            testData.add(
                DependencyInfo(
                    "Test name $i",
                    "Test url $i",
                    "Test description $i",
                    listOf(
                        LicenseInfo(
                            "Test license information $i",
                            "Test license description $i",
                            "$i",
                            "Test license url $i",
                        )
                    )
                )
            )
        }

        return testData.toTypedArray()
    }
}