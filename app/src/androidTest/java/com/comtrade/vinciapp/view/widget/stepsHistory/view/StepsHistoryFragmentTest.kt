/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: StepsHistoryFragmentTest.kt
 * Author: sdelic
 *
 * History:  14.3.22. 11:14 created
 */

package com.comtrade.vinciapp.view.widget.stepsHistory.view

import android.content.Context
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withId
import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.entities.devices.CmdDevice
import com.comtrade.domain.entities.user.data.GetUserDevicesEntity
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.TestVinciApp
import com.comtrade.vinciapp.util.assertViewsDisplayed
import com.comtrade.vinciapp.util.isTabTitleAtPosition
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito

/**
 * StepsHistoryFragment screen UI tests.
 * Assert all of the view components are displayed.
 * Assert that all views are in their correct initial state.
 */
internal class StepsHistoryFragmentTest {
    // TODO: Mock dependencies when issue with Dagger sub components in testing environment resolved.
    private lateinit var prefs: IPrefs

    @Before
    fun setUp() {
        val application = ApplicationProvider.getApplicationContext<TestVinciApp>()
        prefs = application.appComponent.provideAppPrefs()
        Mockito.`when`(prefs.cmdDevice).thenReturn(provideTestCMDDevice())
        //AppCompat theme needed for the Tab Layout
        launchFragmentInContainer<StepsHistoryFragment>(themeResId = R.style.Theme_AppCompat_Light)
    }

    @Test
    fun steps_history_screen_is_displayed() {
        assertViewsDisplayed(
            R.id.history_graphs_tab_layout,
            R.id.history_graphs_view_pager,
        )
    }

    @Test
    fun tabs_at_positions_have_correct_titles() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        val titles = arrayOf(
            context.getString(R.string.history_graph_screen_7_days),
            context.getString(R.string.history_graph_screen_30_days),
            context.getString(R.string.history_graph_screen_6_months),
            context.getString(R.string.history_graph_screen_1_year),
        )
        for (titleIndex: Int in titles.indices) {
            onView(withId(R.id.history_graphs_tab_layout)).check(
                matches(
                    isTabTitleAtPosition(
                        titles[titleIndex],
                        titleIndex
                    )
                )
            )
        }
    }

    private fun provideTestCMDDevice(): CmdDevice = CmdDevice(
        GetUserDevicesEntity(
            deviceId = 1L,
            name = "testCMDDevice",
            description = "test CMD device description",
            uuid = "some random uuid",
            deviceType = "WRIST WATCH",
            active = true
        )
    )
}