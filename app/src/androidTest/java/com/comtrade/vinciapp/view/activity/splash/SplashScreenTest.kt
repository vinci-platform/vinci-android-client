package com.comtrade.vinciapp.view.activity.splash

import androidx.lifecycle.Lifecycle
import androidx.test.core.app.launchActivity
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.util.assertViewsDisplayed
import com.comtrade.vinciapp.view.activity.splash.view.SplashActivity
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Splash screen UI test.
 * Asserting the visibility of various views associated with this screen
 */
@RunWith(AndroidJUnit4::class)
internal class SplashScreenTest {

    @Test
    fun views_are_displayed() {
        val scenario = launchActivity<SplashActivity>()
        scenario.moveToState(Lifecycle.State.RESUMED)

        assertViewsDisplayed(
            R.id.splash_screen_aal_logo, R.id.splash_screen_vinci_logo,
        )
    }
}