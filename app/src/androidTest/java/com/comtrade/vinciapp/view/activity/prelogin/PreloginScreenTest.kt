package com.comtrade.vinciapp.view.activity.prelogin

import androidx.lifecycle.Lifecycle
import androidx.test.core.app.ApplicationProvider
import androidx.test.core.app.launchActivity
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.TestVinciApp
import com.comtrade.vinciapp.view.activity.prelogin.view.PreloginActivity
import org.hamcrest.CoreMatchers.not
import org.hamcrest.core.AllOf.allOf
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito

/**
 * Prelogin Screen UI test.
 * Asserting the acceptance of the terms and conditions checkbox and the navigation to the login and registration screens.
 */
internal class PreloginScreenTest {

    private lateinit var prefs: IPrefs


    @Before
    fun setUp() {
        val application = ApplicationProvider.getApplicationContext<TestVinciApp>()
        prefs = application.appComponent.provideAppPrefs()
        Mockito.`when`(prefs.isTermsAccepted).thenReturn(false)
    }


    @Test
    fun terms_and_conditions_not_checked_buttons_disabled() {
        val scenario = launchActivity<PreloginActivity>()
        scenario.moveToState(Lifecycle.State.RESUMED)

        val isTerms = prefs.isTermsAccepted
        assertFalse(isTerms)

        //Assert the terms and conditions checkbox is displayed and unchecked
        onView(withId(R.id.welcome_fragment_terms_checkbox)).check(
            matches(
                allOf(
                    isCompletelyDisplayed(),
                    isNotChecked()
                )
            )
        )

        //All buttons should be visible and in the disabled state and not clickable.
        val buttonIds = arrayOf(
            R.id.welcome_fragment_login_button, R.id.welcome_fragment_create_account_button,
            R.id.welcome_fragment_facebook_button, R.id.welcome_fragment_google_button
        )

        for (buttonId in buttonIds) {
            onView(withId(buttonId)).check(
                matches(
                    allOf(
                        isCompletelyDisplayed(),
                        not(isEnabled())
                    )
                )
            )
        }


    }

    @Test
    fun terms_and_conditions_checked_buttons_enabled() {
        Mockito.`when`(prefs.isTermsAccepted).thenReturn(true)

        val scenario = launchActivity<PreloginActivity>()
        scenario.moveToState(Lifecycle.State.RESUMED)

        val isTerms = prefs.isTermsAccepted
        assertTrue(isTerms)

        //Assert the terms and conditions checkbox is displayed and checked
        onView(withId(R.id.welcome_fragment_terms_checkbox)).check(
            matches(
                allOf(
                    isCompletelyDisplayed(),
                    isChecked()
                )
            )
        )

        //Login and create account buttons should be visible and in the enabled state.
        val buttonIds = arrayOf(
            R.id.welcome_fragment_login_button, R.id.welcome_fragment_create_account_button
        )

        for (buttonId in buttonIds) {
            onView(withId(buttonId)).check(
                matches(
                    allOf(
                        isCompletelyDisplayed(),
                        isEnabled()
                    )
                )
            )
        }
    }
}