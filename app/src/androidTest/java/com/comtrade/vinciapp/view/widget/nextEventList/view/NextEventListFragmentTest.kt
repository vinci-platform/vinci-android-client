/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: NextEventListFragmentTest.kt
 * Author: sdelic
 *
 * History:  3.3.22. 14:56 created
 */

package com.comtrade.vinciapp.view.widget.nextEventList.view

import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.contracts.usecases.events.IGetUserEventRecordsUseCase
import com.comtrade.domain.entities.event.EventEntity
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.util.assertViewsDisplayed
import kotlinx.coroutines.runBlocking
import org.hamcrest.core.AllOf.allOf
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.`when`

/**
 * NextEventListFragment screen UI tests.
 * Assert all of the view components are displayed.
 * Assert that all views are in their correct initial state.
 * Assert empty events display.
 */
internal class NextEventListFragmentTest {
    private val testUserId = 1L
    private val testTimeInterval = 1L

    private lateinit var prefs: IPrefs
    private lateinit var getUserEventRecordsUseCase: IGetUserEventRecordsUseCase

    private val ITEM_NUMBER = 1_000

    private lateinit var nextEventListFragment: NextEventListFragment

    @Before
    fun setUp() {
        prefs = Mockito.mock(IPrefs::class.java)
        getUserEventRecordsUseCase = Mockito.mock(IGetUserEventRecordsUseCase::class.java)

        launchFragmentInContainer<NextEventListFragment>().onFragment {
            nextEventListFragment = it
        }
    }

    @Test
    fun next_event_list_screen_is_displayed() {
        assertViewsDisplayed(
            R.id.next_event_list_view,
            R.id.next_event_view_fragment_buttons_container,
            R.id.next_event_view_fragment_add_activity_button,
        )
    }

    @Test
    fun add_new_activity_button_has_correct_text() {
        onView(
            allOf(
                withId(R.id.next_event_view_fragment_add_activity_button),
                withText(R.string.next_event_view_fragment_add_activity_button_text)
            )
        ).check(matches(isDisplayed()))
    }


    @Test
    fun test_event_records_have_correct_data() {
        assertNotNull(nextEventListFragment)

        runBlocking {
            `when`(prefs.userId).thenReturn(testUserId)
            `when`(
                getUserEventRecordsUseCase.getUserEventRecords(
                    prefs.userId,
                    testTimeInterval
                )
            ).thenReturn(
                testEventRecords(
                    ITEM_NUMBER
                )
            )

            val nextEventsList = getUserEventRecordsUseCase.getUserEventRecords(
                prefs.userId,
                testTimeInterval
            )
            Mockito.verify(prefs, Mockito.times(2))
                .userId
            Mockito.verify(getUserEventRecordsUseCase, Mockito.times(1))
                .getUserEventRecords(
                    prefs.userId,
                    testTimeInterval
                )

            assertEquals(ITEM_NUMBER, nextEventsList.size)

            //Verify objects
            assertNotNull(nextEventsList[ITEM_NUMBER - 1])
            assertNotNull(nextEventsList[0])
            assertNotNull(nextEventsList[789])

            //Verify ids
            assertEquals(0, nextEventsList[0].id)
            assertEquals(44, nextEventsList[44].id)
            assertEquals(ITEM_NUMBER - 1, nextEventsList[ITEM_NUMBER - 1].id)

            //Verify type
            assertEquals("Activity", nextEventsList[ITEM_NUMBER - 1].type)
            assertEquals("Activity", nextEventsList[0].type)
            assertEquals("Activity", nextEventsList[99].type)

            //Verify repeat
            assertEquals("", nextEventsList[ITEM_NUMBER - 1].repeat)
            assertEquals("", nextEventsList[0].repeat)
            assertEquals("", nextEventsList[234].repeat)
        }
    }

    /**
     * Fake EventRecord data.
     */
    private fun testEventRecords(itemNumber: Int): List<EventEntity> {
        val testData = mutableListOf<EventEntity>()

        for (i: Int in 0 until itemNumber) {
            testData.add(
                EventEntity(
                    i, i.toLong(),
                    System.currentTimeMillis(),
                    System.currentTimeMillis(),
                    -1,
                    "",
                    "outdoorActivity",
                    "Activity",
                    ""
                )
            )
        }

        return testData
    }
}