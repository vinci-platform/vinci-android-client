package com.comtrade.vinciapp.view.widget.createUser.view

import android.text.InputType
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.espresso.Espresso.onData
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import com.comtrade.domain.entities.user.UserRoles
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.util.assertEditFieldForHintAndInputType
import com.comtrade.vinciapp.util.assertViewsDisplayed
import com.comtrade.vinciapp.util.assertViewsNotDisplayed
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.instanceOf
import org.hamcrest.core.AllOf.allOf
import org.junit.Before
import org.junit.Test

/**
 * CreateUserFragment screen UI tests.
 * Assert all of the view components are displayed.
 * Assert that all views are in their correct initial state.
 * Assert input fields.
 * Assert input field error states.
 */
internal class CreateUserFragmentTest {

    @Before
    fun setUp() {
        launchFragmentInContainer<CreateUserFragment>()
    }

    @Test
    fun create_user_fragment_is_displayed() {
        assertViewsDisplayed(
            R.id.create_user_fragment_scroll,
            R.id.create_user_fragment_username_edit_text,
            R.id.create_user_fragment_email_edit_text,
            R.id.create_user_fragment_create_password_edit_text,
            R.id.create_user_fragment_repeat_password_edit_text,
            R.id.create_user_fragment_user_role_spinner,
            R.id.create_user_fragment_user_role_arrow_view,
            R.id.create_user_fragment_confirm_button
        )

        //Progress bar visible only on network calls.
        assertViewsNotDisplayed(R.id.progressBar)

        //Warning views not visible initially
        assertViewsNotDisplayed(
            R.id.create_user_fragment_username_warning,
            R.id.create_user_fragment_email_warning,
            R.id.create_user_fragment_create_password_warning,
            R.id.create_user_fragment_repeat_password_warning
        )
    }

    @Test
    fun input_fields_have_correct_hints_and_initial_state() {

        assertEditFieldForHintAndInputType(
            R.id.create_user_fragment_username_edit_text,
            R.string.create_user_screen_enter_username_hint,
            InputType.TYPE_CLASS_TEXT
        )

        assertEditFieldForHintAndInputType(
            R.id.create_user_fragment_email_edit_text,
            R.string.create_user_screen_enter_email_hint,
            InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS
        )

        assertEditFieldForHintAndInputType(
            R.id.create_user_fragment_create_password_edit_text,
            R.string.create_user_screen_create_password_hint,
            InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
        )

        assertEditFieldForHintAndInputType(
            R.id.create_user_fragment_repeat_password_edit_text,
            R.string.create_user_screen_repeat_password_hint,
            InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
        )
    }

    @Test
    fun create_account_button_has_correct_label() {
        onView(withId(R.id.create_user_fragment_confirm_button))
            .check(matches(withText(R.string.create_user_screen_submit_button_text)))
    }

    @Test
    fun role_selector_displays_first_role() {
        //Spinner view component selection text validation.
        onView(withId(R.id.create_user_fragment_user_role_spinner))
            .check(matches(withSpinnerText(UserRoles.ROLE_PACIENT.name)))
    }

    @Test
    fun role_selector_displays_selected_roles() {
        onView(withId(R.id.create_user_fragment_user_role_spinner))
            .check(matches(withSpinnerText(UserRoles.ROLE_PACIENT.name)))
        onView(withId(R.id.create_user_fragment_user_role_spinner)).perform(click())
        onData(
            allOf(
                `is`(instanceOf(String::class.java)),
                `is`(UserRoles.ROLE_FAMILY.name)
            )
        ).perform(
            click()
        )
        onView(withId(R.id.create_user_fragment_user_role_spinner))
            .check(matches(withSpinnerText(UserRoles.ROLE_FAMILY.name)))
        //Switch back to fist role
        onView(withId(R.id.create_user_fragment_user_role_spinner)).perform(click())
        onData(
            allOf(
                `is`(instanceOf(String::class.java)),
                `is`(UserRoles.ROLE_PACIENT.name)
            )
        ).perform(
            click()
        )
        onView(withId(R.id.create_user_fragment_user_role_spinner))
            .check(matches(withSpinnerText(UserRoles.ROLE_PACIENT.name)))

    }
}