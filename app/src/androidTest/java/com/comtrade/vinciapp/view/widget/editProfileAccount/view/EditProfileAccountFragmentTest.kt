package com.comtrade.vinciapp.view.widget.editProfileAccount.view

import android.content.Context
import android.text.InputType
import androidx.annotation.IdRes
import androidx.annotation.StringRes
import androidx.fragment.app.testing.FragmentScenario
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.lifecycle.Lifecycle
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso.onData
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.clearText
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.TestVinciApp
import com.comtrade.vinciapp.util.assertEditFieldForHintAndInputType
import com.comtrade.vinciapp.util.assertViewsDisplayed
import com.comtrade.vinciapp.view.widget.editProfileAccount.view.adapter.Gender
import org.hamcrest.CoreMatchers
import org.hamcrest.core.AllOf.allOf
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito

/**
 * EditProfileAccountFragment screen UI tests.
 * Assert all of the view components are displayed. Scroll behavior needed.
 * Assert that all views are in their correct initial state.
 * Assert input fields.
 */
internal class EditProfileAccountFragmentTest {

    private val inputFields = intArrayOf(
        R.id.edit_profile_account_first_name_text,
        R.id.edit_profile_account_last_name_text,
        R.id.edit_profile_account_address_text,
        R.id.edit_profile_account_phone_text,
        R.id.edit_profile_account_email_text
    )

    private lateinit var prefs: IPrefs
    private lateinit var fragmentScenario: FragmentScenario<EditProfileAccountFragment>

    @Before
    fun setUp() {
        val application = ApplicationProvider.getApplicationContext<TestVinciApp>()
        prefs = application.appComponent.provideAppPrefs()
        Mockito.`when`(prefs.userFirstName).thenReturn("")
        Mockito.`when`(prefs.userLastName).thenReturn("")
        Mockito.`when`(prefs.userAddress).thenReturn("")
        fragmentScenario = launchFragmentInContainer()
    }

    @After
    fun tearDown() {
        Mockito.reset(prefs)
    }

    /**
     * On smaller screens one scroll is needed to show the rest of the fields.
     */
    @Test
    fun edit_profile_fragment_is_displayed() {

        // Save button, static and always visible to the User
        assertViewsDisplayed(R.id.edit_profile_account_save_button)

        // Initial state views before scrolling down
        assertViewsDisplayed(
            R.id.edit_profile_account_fragment_scroll,
            R.id.edit_profile_account_first_name_text,
            R.id.edit_profile_account_last_name_text,
            R.id.edit_profile_account_address_text,
            R.id.edit_profile_account_phone_text,
            R.id.edit_profile_account_save_button
        )

        onView(withId(R.id.edit_profile_account_fragment_scroll)).perform(ViewActions.swipeUp())

        // Rest of the list views after scrolling down
        assertViewsDisplayed(
            R.id.edit_profile_account_email_text,
            R.id.edit_profile_account_gender_spinner,
            R.id.edit_profile_account_save_button
        )
    }

    @Test
    fun gender_selector_displays_selected_options() {
        // Default first male gender is shown.
        onView(withId(R.id.edit_profile_account_gender_spinner))
            .check(
                matches(
                    allOf(
                        withSpinnerText(Gender.Male.name),
                        isCompletelyDisplayed(),
                        withEffectiveVisibility(Visibility.VISIBLE)
                    )
                )
            )

        onView(withId(R.id.edit_profile_account_gender_spinner)).perform(click())
        onData(
            allOf(
                CoreMatchers.`is`(CoreMatchers.instanceOf(String::class.java)),
                CoreMatchers.`is`(Gender.Female.name)
            )
        ).perform(
            click()
        )
        onView(withId(R.id.edit_profile_account_gender_spinner))
            .check(matches(withSpinnerText(Gender.Female.name)))
        //Switch back to fist selection
        onView(withId(R.id.edit_profile_account_gender_spinner)).perform(click())
        onData(
            allOf(
                CoreMatchers.`is`(CoreMatchers.instanceOf(String::class.java)),
                CoreMatchers.`is`(Gender.Male.name)
            )
        ).perform(
            click()
        )
        onView(withId(R.id.edit_profile_account_gender_spinner))
            .check(matches(withSpinnerText(Gender.Male.name)))
    }

    @Test
    fun input_fields_have_correct_hints_and_initial_state() {
        assertEditFieldForHintAndInputType(
            R.id.edit_profile_account_first_name_text,
            R.string.edit_profile_account_screen_first_name_hint,
            InputType.TYPE_CLASS_TEXT
        )

        assertEditFieldForHintAndInputType(
            R.id.edit_profile_account_last_name_text,
            R.string.edit_profile_account_screen_last_name_hint,
            InputType.TYPE_CLASS_TEXT
        )

        assertEditFieldForHintAndInputType(
            R.id.edit_profile_account_address_text,
            R.string.edit_profile_account_screen_address_hint,
            InputType.TYPE_CLASS_TEXT
        )

        assertEditFieldForHintAndInputType(
            R.id.edit_profile_account_phone_text,
            R.string.edit_profile_account_screen_phone_hint,
            InputType.TYPE_CLASS_TEXT or InputType.TYPE_CLASS_PHONE
        )

        assertEditFieldForHintAndInputType(
            R.id.edit_profile_account_email_text,
            R.string.edit_profile_account_screen_email_hint,
            InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS
        )

        onView(withId(R.id.edit_profile_account_fragment_scroll)).perform(ViewActions.swipeUp())

    }

    @Test
    fun save_account_button_has_correct_label() {
        onView(withId(R.id.edit_profile_account_save_button))
            .check(matches(withText(R.string.edit_profile_account_screen_save_button)))
    }

    @Test
    fun save_button_click_focuses_on_missing_required_field() {

        populateInputFields(inputFields)

        for (id in inputFields) {
            onView(withId(id)).perform(clearText())
            onView(withId(R.id.edit_profile_account_save_button)).perform(click())
            onView(withId(id)).check(matches(hasFocus()))
            //Populate input with text since we already checked it
            onView(withId(id)).perform(ViewActions.typeText("done"))
        }
    }

    @Test
    fun save_button_click_focuses_on_first_name_and_shows_toast() {
        assertToastMessageDisplayed(
            R.id.edit_profile_account_first_name_text,
            R.string.edit_profile_account_screen_enter_first_name
        )
    }

    @Test
    fun save_button_click_focuses_on_last_name_and_shows_toast() {
        assertToastMessageDisplayed(
            R.id.edit_profile_account_last_name_text,
            R.string.edit_profile_account_screen_enter_last_name
        )
    }

    @Test
    fun save_button_click_focuses_on_address_and_shows_toast() {
        assertToastMessageDisplayed(
            R.id.edit_profile_account_address_text,
            R.string.edit_profile_account_screen_enter_address
        )
    }

    @Test
    fun save_button_click_focuses_on_email_and_shows_toast() {
        assertToastMessageDisplayed(
            R.id.edit_profile_account_email_text,
            R.string.edit_profile_account_screen_enter_email
        )
    }

    @Test
    fun save_button_click_focuses_on_phone_number_and_shows_toast() {
        assertToastMessageDisplayed(
            R.id.edit_profile_account_phone_text,
            R.string.edit_profile_account_screen_enter_phone_number
        )
    }

    /**
     * Populating all required fields and clearing the asserted field in order to verify that
     * the correct Toast message has been shown to Users when trying to save the profile.
     */
    private fun assertToastMessageDisplayed(@IdRes viewId: Int, @StringRes errorMessage: Int) {
        val context = ApplicationProvider.getApplicationContext<Context>()
        val error = context.getString(errorMessage)
        fragmentScenario.moveToState(Lifecycle.State.RESUMED)
        populateInputFields(inputFields)
        onView(withId(viewId)).perform(clearText())
        onView(withId(R.id.edit_profile_account_save_button)).perform(click())
        onView(withId(viewId)).check(matches(hasFocus()))
        onView(withId(viewId)).check(matches(hasErrorText(error)))
        onView(withId(viewId)).perform(clearText())
        // For avoiding toast messages stack,
        // access to the Toast object needs to be provided in order to cancel it before starting a new test series.
        fragmentScenario.moveToState(Lifecycle.State.DESTROYED)

    }

    /**
     * Populates the input fields with test text
     */
    private fun populateInputFields(inputFields: IntArray, text: String = "test") {
        for (id in inputFields) {
            onView(withId(id)).perform(ViewActions.typeText(text))
        }
    }
}