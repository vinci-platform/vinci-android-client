/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: RegisterMedicationFragmentTest.kt
 * Author: sdelic
 *
 * History:  28.3.22. 14:08 created
 */

package com.comtrade.vinciapp.view.widget.registerMedication.view

import android.text.InputType
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.util.*
import com.comtrade.vinciapp.view.widget.registerMedication.view.adapter.MedicationFrequency
import com.comtrade.vinciapp.view.widget.registerMedication.view.adapter.Patients
import org.hamcrest.CoreMatchers.not
import org.hamcrest.core.AllOf
import org.junit.Before
import org.junit.Test

/**
 * RegisterMedicationFragment screen UI tests.
 * Assert all of the view components are displayed.
 * Assert that all views are in their correct initial state.
 */
internal class RegisterMedicationFragmentTest {

    @Before
    fun setUp() {
        launchFragmentInContainer<RegisterMedicationFragment>()
    }

    @Test
    fun register_medication_screen_is_displayed() {
        assertViewsDisplayed(
            R.id.register_medication_fragment_scroll,
            R.id.register_medication_fragment_medication_edit_text,
            R.id.register_medication_fragment_start_date_spinner,
            R.id.register_medication_fragment_start_date_calendar_icon,
            R.id.register_medication_fragment_end_date_spinner,
            R.id.register_medication_fragment_end_date_calendar_icon,
            R.id.register_medication_fragment_frequency_first_text,
            R.id.register_medication_fragment_frequency_second_text,
            R.id.register_medication_fragment_frequency_spinner,
            R.id.register_medication_fragment_frequency_arrow_view,
            R.id.register_medication_fragment_instructions_edit_text,
            R.id.register_medication_fragment_patients_spinner,
            R.id.register_medication_fragment_patients_arrow_view,
            R.id.register_medication_fragment_add_medication_button,
        )
    }

    @Test
    fun medication_name_input_field_has_correct_hint_and_initial_state() {

        assertEditFieldForHintAndInputType(
            R.id.register_medication_fragment_medication_edit_text,
            R.string.register_medication_screen_medication_hint,
            InputType.TYPE_CLASS_TEXT
        )
    }

    @Test
    fun medication_instructions_input_field_has_correct_hint_and_initial_state() {

        assertEditFieldForHintAndInputType(
            R.id.register_medication_fragment_instructions_edit_text,
            R.string.register_medication_screen_instructions_hint,
            InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_FLAG_MULTI_LINE or InputType.TYPE_TEXT_FLAG_CAP_SENTENCES
        )

        //Assert max character count allowed
        val expectedLength = 2_000
        onView(withId(R.id.register_medication_fragment_instructions_edit_text)).check(
            matches(
                MaxLength(expectedLength)
            )
        )
    }

    @Test
    fun start_medication_date_input_has_correct_hint() {

        onView(withId(R.id.register_medication_fragment_start_date_spinner)).check(
            matches(
                withHint(R.string.register_medication_screen_start_date_hint)
            )
        )

    }

    @Test
    fun end_medication_date_input_has_correct_hint() {
        onView(withId(R.id.register_medication_fragment_end_date_spinner)).check(
            matches(
                withHint(R.string.register_medication_screen_end_date_hint)
            )
        )
    }

    @Test
    fun take_on_every_text_is_displayed() {
        onView(withText(R.string.register_medication_screen_frequency_first_part_text)).check(
            matches(
                isDisplayed()
            )
        )
    }

    @Test
    fun hours_text_is_displayed() {
        onView(withText(R.string.register_medication_screen_frequency_second_part_text)).check(
            matches(
                isDisplayed()
            )
        )
    }

    @Test
    fun frequency_selection_has_correct_options() {
        // Default first selection is shown.
        onView(withId(R.id.register_medication_fragment_frequency_spinner))
            .check(
                matches(
                    AllOf.allOf(
                        withSpinnerText(MedicationFrequency.ONE.number.toString()),
                        isCompletelyDisplayed(),
                        withEffectiveVisibility(Visibility.VISIBLE)
                    )
                )
            )

        assertSpinnerSelection(
            R.id.register_medication_fragment_frequency_spinner,
            MedicationFrequency.SIXTEEN.number.toString()
        )
        assertSpinnerSelection(
            R.id.register_medication_fragment_frequency_spinner,
            MedicationFrequency.TWENTY_FOUR.number.toString()
        )

        //Switch back to fist selection
        assertSpinnerSelection(
            R.id.register_medication_fragment_frequency_spinner,
            MedicationFrequency.ONE.number.toString()
        )
    }

    @Test
    fun select_patient_text_is_displayed() {
        onView(withText(R.string.register_medication_screen_select_patient_label)).check(
            matches(
                isDisplayed()
            )
        )
    }

    /**
     * Test data
     */
    @Test
    fun patient_selection_has_correct_options() {
        // Default first selection is shown.
        onView(withId(R.id.register_medication_fragment_patients_spinner))
            .check(
                matches(
                    AllOf.allOf(
                        withSpinnerText(Patients.JOHN_DOE.patientName),
                        isCompletelyDisplayed(),
                        withEffectiveVisibility(Visibility.VISIBLE)
                    )
                )
            )

        assertSpinnerSelection(
            R.id.register_medication_fragment_patients_spinner,
            Patients.JANE_DOE.patientName
        )
        assertSpinnerSelection(
            R.id.register_medication_fragment_patients_spinner,
            Patients.WALTER_WHITE.patientName
        )

        //Switch back to fist selection
        assertSpinnerSelection(
            R.id.register_medication_fragment_patients_spinner,
            Patients.JOHN_DOE.patientName
        )
    }

    @Test
    fun add_medication_button_is_disabled_at_start() {
        onView(withId(R.id.register_medication_fragment_add_medication_button)).check(
            matches(
                not(
                    isEnabled()
                )
            )
        )
    }

    @Test
    fun add_medication_button_has_correct_caption() {
        assertViewHasCaption(
            R.id.register_medication_fragment_add_medication_button,
            R.string.register_medication_screen_add_medication_button_text
        )
    }
}