/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: AboutViewFragmentTest.kt
 * Author: sdelic
 *
 * History:  22.3.22. 10:56 created
 */

package com.comtrade.vinciapp.view.widget.aboutView.view

import android.content.Context
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import com.comtrade.vinciapp.BuildConfig
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.util.assertViewHasCaption
import com.comtrade.vinciapp.util.assertViewsDisplayed
import org.junit.Before
import org.junit.Test

/**
 * AboutViewFragment screen UI tests.
 * Assert all of the view components are displayed.
 * Assert that all views are in their correct initial state.
 */
internal class AboutViewFragmentTest {

    @Before
    fun setUp() {
        launchFragmentInContainer<AboutViewFragment>()
    }

    @Test
    fun about_screen_is_displayed() {
        assertViewsDisplayed(
            R.id.image_background,
            R.id.about_screen_aal_logo_background,
            R.id.about_screen_aal_logo,
            R.id.about_screen_research_label,
            R.id.about_screen_vinci_logo,
            R.id.about_screen_version_label,
            R.id.about_screen_legal_button
        )
    }

    @Test
    fun research_project_label_has_correct_text() {
        assertViewHasCaption(
            R.id.about_screen_research_label,
            R.string.about_screen_research_label_text
        )
    }

    @Test
    fun legal_button_has_correct_caption() {
        assertViewHasCaption(R.id.about_screen_legal_button, R.string.about_dependencies_label)
    }

    @Test
    fun app_version_name_is_displayed() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        onView(withId(R.id.about_screen_version_label)).check(
            matches(
                withText(
                    context.getString(
                        R.string.about_screen_version_placeholder,
                        BuildConfig.VERSION_NAME
                    )
                )
            )
        )
    }
}