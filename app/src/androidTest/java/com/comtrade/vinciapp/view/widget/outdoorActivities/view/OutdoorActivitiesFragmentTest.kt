/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: OutdoorActivitiesFragmentTest.kt
 * Author: sdelic
 *
 * History:  14.3.22. 10:02 created
 */

package com.comtrade.vinciapp.view.widget.outdoorActivities.view

import android.content.Context
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.util.assertSpinnerSelection
import com.comtrade.vinciapp.util.assertViewsDisplayed
import org.hamcrest.CoreMatchers.not
import org.hamcrest.core.AllOf
import org.junit.Before
import org.junit.Test

/**
 * OutdoorActivitiesFragment screen UI tests.
 * Assert all of the view components are displayed.
 * Assert that all views are in their correct initial state.
 */
internal class OutdoorActivitiesFragmentTest {

    @Before
    fun setUp() {
        launchFragmentInContainer<OutdoorActivitiesFragment>()
    }

    @Test
    fun outdoor_activities_screen_is_displayed() {
        assertViewsDisplayed(
            R.id.outdoor_activities_fragment_scroll,
            R.id.outdoor_activities_fragment_text,
            R.id.outdoor_activities_fragment_spinner,
            R.id.outdoor_activities_fragment_arrow_view,
            R.id.outdoor_activities_fragment_datetime_spinner,
            R.id.outdoor_activities_fragment_datetime_calendar_icon,
            R.id.outdoor_activities_fragment_invite_friends_checkbox,
            R.id.outdoor_activities_save_button
        )
    }

    @Test
    fun user_question_is_displayed() {
        onView(withId(R.id.outdoor_activities_fragment_text)).check(matches(withText(R.string.outdoor_activities_screen_text)))
    }

    @Test
    fun date_time_picker_has_correct_hint() {
        onView(withId(R.id.outdoor_activities_fragment_datetime_spinner)).check(
            matches(
                withHint(R.string.outdoor_activities_screen_datetime)
            )
        )
    }

    @Test
    fun invite_friends_checkbox_has_correct_caption() {
        onView(withId(R.id.outdoor_activities_fragment_invite_friends_checkbox)).check(
            matches(
                withText(R.string.outdoor_activities_screen_invite_friends_checkbox_text)
            )
        )
    }

    @Test
    fun invite_friends_checkbox_is_unchecked_at_start() {
        onView(withId(R.id.outdoor_activities_fragment_invite_friends_checkbox)).check(
            matches(
                isNotChecked()
            )
        )
    }

    @Test
    fun save_button_has_correct_caption() {
        onView(withId(R.id.outdoor_activities_save_button)).check(
            matches(
                withText(R.string.outdoor_activities_screen_save_button_text)
            )
        )
    }

    @Test
    fun save_button_is_disabled_at_start() {
        onView(withId(R.id.outdoor_activities_save_button)).check(
            matches(
                not(isEnabled())
            )
        )
    }

    @Test
    fun outdoor_activities_selector_displays_selected_options() {
        val supportedActivities =
            ApplicationProvider.getApplicationContext<Context>().resources.getStringArray(R.array.supported_outdoor_activities)
        // Default first Walk is shown.
        onView(withId(R.id.outdoor_activities_fragment_spinner))
            .check(
                matches(
                    AllOf.allOf(
                        withSpinnerText(supportedActivities[0]),
                        isCompletelyDisplayed(),
                        withEffectiveVisibility(Visibility.VISIBLE)
                    )
                )
            )

        assertSpinnerSelection(
            R.id.outdoor_activities_fragment_spinner,
            supportedActivities[1]
        )

        assertSpinnerSelection(
            R.id.outdoor_activities_fragment_spinner,
            supportedActivities[2]
        )
        //Switch back to fist selection
        assertSpinnerSelection(
            R.id.outdoor_activities_fragment_spinner,
            supportedActivities[0]
        )
    }

}