/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: MainScreenFragmentTest.kt
 * Author: sdelic
 *
 * History:  15.3.22. 10:53 created
 */

package com.comtrade.vinciapp.view.widget.mainScreen.view

import android.Manifest
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.core.app.ApplicationProvider
import androidx.test.rule.GrantPermissionRule
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.TestVinciApp
import com.comtrade.vinciapp.util.assertViewsDisplayed
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito

/**
 * MainScreenFragment screen UI tests.
 * Assert all of the view components are displayed.
 * Assert that all views are in their correct initial state.
 */
internal class MainScreenFragmentTest {

    @Rule
    @JvmField
    val grantPermissionRule: GrantPermissionRule =
        GrantPermissionRule.grant(Manifest.permission.ACCESS_FINE_LOCATION)

    @Before
    fun setUp() {
        val application = ApplicationProvider.getApplicationContext<TestVinciApp>()
        val prefs = application.appComponent.provideAppPrefs()
        Mockito.`when`(prefs.userFirstName).thenReturn("Test Firstname")
        Mockito.`when`(prefs.userLastName).thenReturn("Test Lastname")
        Mockito.`when`(prefs.userEmail).thenReturn("test@test.com")
        Mockito.`when`(prefs.userProfilePicture).thenReturn("")
        launchFragmentInContainer<MainScreenFragment>()
    }

    @Test
    fun main_fragment_screen_is_displayed() {
        assertViewsDisplayed(
            R.id.main_screen_fragment_profile_image_backgound,
            R.id.main_screen_fragment_profile_image,
            R.id.main_screen_fragment_devices_background_border,
            R.id.main_screen_fragment_devices_background,
            R.id.main_screen_fragment_devices_icon,
            R.id.main_screen_fragment_profile_name,
            R.id.main_screen_fragment_points_label,
            R.id.main_screen_fragment_points,
            R.id.main_screen_fragment_event_background_image,
            R.id.main_screen_fragment_event_text_view,
            R.id.main_screen_fragment_quality_of_life_container,
            R.id.main_screen_fragment_friends_container,
            R.id.main_screen_fragment_emergency_call_container,
            R.id.main_screen_fragment_physical_activity_container,
            R.id.main_screen_fragment_cognitive_games_container,
            R.id.main_screen_fragment_today_feelings_container
        )
    }
}