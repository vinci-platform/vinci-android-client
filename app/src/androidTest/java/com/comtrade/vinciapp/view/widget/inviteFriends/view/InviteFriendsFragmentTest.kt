/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: InviteFriendsFragmentTest.kt
 * Author: sdelic
 *
 * History:  17.3.22. 12:07 created
 */

package com.comtrade.vinciapp.view.widget.inviteFriends.view

import androidx.fragment.app.testing.launchFragmentInContainer
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.util.assertButtonsEnabled
import com.comtrade.vinciapp.util.assertViewHasCaption
import com.comtrade.vinciapp.util.assertViewsDisplayed
import org.junit.Before
import org.junit.Test

/**
 * InviteFriendsFragment screen UI tests.
 * Assert all of the view components are displayed.
 * Assert that all views are in their correct initial state.
 */
internal class InviteFriendsFragmentTest {
    @Before
    fun setUp() {
        launchFragmentInContainer<InviteFriendsFragment>()
    }

    @Test
    fun invite_friends_screen_is_displayed() {
        assertViewsDisplayed(
            R.id.invite_friends_fragment_list_view,
            R.id.invite_friends_fragment_buttons_container,
            R.id.invite_friends_fragment_back_button,
            R.id.invite_friends_fragment_invite_button
        )
    }

    @Test
    fun invite_friends_buttons_are_enabled() {
        assertButtonsEnabled(
            R.id.invite_friends_fragment_back_button,
            R.id.invite_friends_fragment_invite_button
        )
    }

    @Test
    fun back_button_has_correct_caption() {
        assertViewHasCaption(
            R.id.invite_friends_fragment_back_button,
            R.string.invite_friends_screen_back_button_text
        )
    }

    @Test
    fun invite_friends_button_has_correct_caption() {
        assertViewHasCaption(
            R.id.invite_friends_fragment_invite_button,
            R.string.invite_friends_screen_invite_button_text
        )
    }
}