/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: QuestionnaireSurveyGradationQuestionTest.kt
 * Author: sdelic
 *
 * History:  30.3.22. 12:37 created
 */

package com.comtrade.vinciapp.view.widget.questionnaireSurvey.view.questionViews

import android.content.Context
import androidx.core.os.bundleOf
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.util.assertViewHasCaption
import com.comtrade.vinciapp.util.assertViewsDisplayed
import com.comtrade.vinciapp.view.widget.questionnaireSurvey.model.SurveyQuestionnaireItem
import com.comtrade.vinciapp.view.widget.questionnaireUserNeeds.view.questionViews.QuestionnaireCheckboxQuestion
import org.junit.Before
import org.junit.Test

/**
 * QuestionnaireSurveyGradationQuestion screen UI tests.
 * Assert all of the view components are displayed.
 * Assert that all views are in their correct initial state.
 */
internal class QuestionnaireSurveyGradationQuestionTest {

    private lateinit var context: Context

    @Before
    fun setUp() {
        context = ApplicationProvider.getApplicationContext()

        val testQuestionId =
            context.getString(R.string.user_needs_questionnaire_screen_question_a7_id)
        val testQuestion =
            context.getString(R.string.user_needs_questionnaire_screen_question_a7_text)
        val testAnswers =
            context.resources.getStringArray(R.array.user_needs_variants_communication)
        val questionItem = SurveyQuestionnaireItem(
            testQuestionId,
            testQuestionId,
            testQuestion,
            true,
            testAnswers
        )
        val arguments = bundleOf(
            QuestionnaireCheckboxQuestion.QUESTION_ID_KEY to questionItem.questionId,
            QuestionnaireCheckboxQuestion.QUESTION_NUMBER_KEY to questionItem.questionNumber,
            QuestionnaireCheckboxQuestion.QUESTION_KEY to questionItem.question,
            QuestionnaireCheckboxQuestion.ORDER_KEY to questionItem.order,
            QuestionnaireCheckboxQuestion.ANSWERS_KEY to questionItem.answers,
        )
        launchFragmentInContainer<QuestionnaireSurveyGradationQuestion>(arguments)
    }

    @Test
    fun questionnaire_gradation_question_screen_is_displayed() {
        assertViewsDisplayed(
            R.id.survey_questionnaire_screen_info_label,
            R.id.survey_questionnaire_screen_question_container,
            R.id.survey_questionnaire_screen_question_number,
            R.id.survey_questionnaire_screen_question_text,
            R.id.survey_questionnaire_screen_button_1,
            R.id.survey_questionnaire_screen_button_1_number_text,
            R.id.survey_questionnaire_screen_button_1_text,
            R.id.survey_questionnaire_screen_button_2,
            R.id.survey_questionnaire_screen_button_2_number_text,
            R.id.survey_questionnaire_screen_button_2_text,
            R.id.survey_questionnaire_screen_button_3,
            R.id.survey_questionnaire_screen_button_3_number_text,
            R.id.survey_questionnaire_screen_button_3_text,
            R.id.survey_questionnaire_screen_button_4,
            R.id.survey_questionnaire_screen_button_4_number_text,
            R.id.survey_questionnaire_screen_button_4_text,
            R.id.survey_questionnaire_screen_button_5,
            R.id.survey_questionnaire_screen_button_5_number_text,
            R.id.survey_questionnaire_screen_button_5_text
        )
    }

    @Test
    fun question_text_is_displayed() {
        assertViewHasCaption(
            R.id.survey_questionnaire_screen_question_text,
            R.string.user_needs_questionnaire_screen_question_a7_text
        )
    }

    @Test
    fun answers_have_correct_text() {
        val testAnswers =
            context.resources.getStringArray(R.array.user_needs_variants_communication)
        val answers =
            mapOf(
                R.id.survey_questionnaire_screen_button_1_text to testAnswers[0],
                R.id.survey_questionnaire_screen_button_2_text to testAnswers[1],
                R.id.survey_questionnaire_screen_button_3_text to testAnswers[2],
                R.id.survey_questionnaire_screen_button_4_text to testAnswers[3],
                R.id.survey_questionnaire_screen_button_5_text to testAnswers[4],
            )

        for (answer in answers) {
            Espresso.onView(ViewMatchers.withId(answer.key)).check(
                ViewAssertions.matches(
                    ViewMatchers.withText(
                        answer.value
                    )
                )
            )
        }
    }
}