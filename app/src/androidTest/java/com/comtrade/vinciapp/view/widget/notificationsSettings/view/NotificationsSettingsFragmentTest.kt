/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: NotificationsSettingsFragmentTest.kt
 * Author: sdelic
 *
 * History:  16.3.22. 16:18 created
 */

package com.comtrade.vinciapp.view.widget.notificationsSettings.view

import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.TestVinciApp
import com.comtrade.vinciapp.util.*
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import java.time.Instant

/**
 * NotificationsSettingsFragment screen UI tests.
 * Assert all of the view components are displayed.
 * Assert that all views are in their correct initial state.
 */
internal class NotificationsSettingsFragmentTest {
    private lateinit var prefs: IPrefs
    private lateinit var syncMessage: String

    @Before
    fun setUp() {
        val application = ApplicationProvider.getApplicationContext<TestVinciApp>()
        //Setting the default logging state to disabled
        prefs = application.appComponent.provideAppPrefs()
        `when`(prefs.isLogEnabled).thenReturn(false)
        //Setting sync message
        val formattedDateTime = getFormattedTimeFromInstant(Instant.now())
        `when`(prefs.lastSuccessfulSync).thenReturn(formattedDateTime)
        syncMessage = "Sync successful at ${prefs.lastSuccessfulSync}"
        `when`(prefs.connectionStatus).thenReturn(syncMessage)
        launchFragmentInContainer<NotificationsSettingsFragment>()
    }

    @After
    fun tearDown() {
        Mockito.reset(prefs)
    }

    @Test
    fun notification_settings_screen_is_displayed() {
        assertViewsDisplayed(
            R.id.notifications_settings_root_scroll,
            R.id.notifications_settings_outdoor_activity_layout,
            R.id.notifications_settings_outdoor_activity_switch,
            R.id.notifications_settings_quality_of_life_switch,
            R.id.notifications_settings_radio_group_text,
            R.id.notifications_settings_radio_group,
            R.id.notifications_settings_screen_quality_of_life_9_radio_button,
            R.id.notifications_settings_screen_quality_of_life_12_radio_button,
            R.id.notifications_settings_screen_quality_of_life_15_radio_button,
            R.id.notifications_settings_screen_connection_status,
            R.id.notifications_settings_logging_switch,
            R.id.notifications_settings_copy_log_button,
            R.id.notifications_settings_buttons_container,
            R.id.notifications_settings_cancel_button,
            R.id.notifications_settings_save_button
        )
    }

    @Test
    fun outdoor_activity_text_is_displayed() {
        onView(withText(R.string.notifications_settings_screen_outdoor_activity_title)).check(
            matches(
                isDisplayed()
            )
        )
    }

    @Test
    fun outdoor_activity_switch_has_correct_caption() {
        assertViewHasCaption(
            R.id.notifications_settings_outdoor_activity_switch,
            R.string.notifications_settings_screen_outdoor_activity_switch_enabled_text
        )
    }

    @Test
    fun quality_of_life_text_is_displayed() {
        onView(withText(R.string.notifications_settings_screen_quality_of_life_title)).check(
            matches(
                isDisplayed()
            )
        )
    }

    @Test
    fun quality_of_life_switch_has_correct_caption() {
        assertViewHasCaption(
            R.id.notifications_settings_quality_of_life_switch,
            R.string.notifications_settings_screen_outdoor_activity_switch_enabled_text
        )
    }

    @Test
    fun notification_settings_radio_group_has_correct_text() {
        assertViewHasCaption(
            R.id.notifications_settings_radio_group_text,
            R.string.notifications_settings_screen_quality_of_life_change_time_text
        )
    }

    @Test
    fun quality_of_life_radio_button_9_has_correct_caption() {
        assertViewHasCaption(
            R.id.notifications_settings_screen_quality_of_life_9_radio_button,
            R.string.notifications_settings_screen_quality_of_life_9_text
        )
    }

    @Test
    fun quality_of_life_radio_button_12_has_correct_caption() {
        assertViewHasCaption(
            R.id.notifications_settings_screen_quality_of_life_12_radio_button,
            R.string.notifications_settings_screen_quality_of_life_12_text
        )
    }

    @Test
    fun quality_of_life_radio_button_15_has_correct_caption() {
        assertViewHasCaption(
            R.id.notifications_settings_screen_quality_of_life_15_radio_button,
            R.string.notifications_settings_screen_quality_of_life_15_text
        )
    }

    @Test
    fun connection_status_text_is_displayed() {
        onView(withText(R.string.notifications_settings_screen_connection_status_text)).check(
            matches(
                isDisplayed()
            )
        )
    }

    @Test
    fun logging_text_is_displayed() {

        onView(withText(R.string.notifications_settings_screen_logging_title)).check(
            matches(
                isDisplayed()
            )
        )
    }

    @Test
    fun logging_switch_is_disabled_at_start() {
        onView(withId(R.id.notifications_settings_logging_switch)).check(matches(isNotChecked()))
    }

    @Test
    fun logging_switch_has_correct_caption() {
        assertViewHasCaption(
            R.id.notifications_settings_logging_switch,
            R.string.notifications_settings_screen_logging_switch_disabled_text
        )
    }

    @Test
    fun notification_settings_screen_buttons_are_enabled() {
        assertButtonsEnabled(
            R.id.notifications_settings_copy_log_button,
            R.id.notifications_settings_cancel_button,
            R.id.notifications_settings_save_button,
        )
    }

    @Test
    fun copy_log_button_has_correct_caption() {
        assertViewHasCaption(
            R.id.notifications_settings_copy_log_button,
            R.string.notifications_settings_copy_log_button_text
        )
    }

    @Test
    fun cancel_button_has_correct_caption() {
        assertViewHasCaption(
            R.id.notifications_settings_cancel_button,
            R.string.common_button_cancel
        )
    }

    @Test
    fun save_button_has_correct_caption() {
        assertViewHasCaption(
            R.id.notifications_settings_save_button,
            R.string.common_button_save
        )
    }

    @Test
    fun when_outdoor_activities_switch_off_disabled_message_is_displayed() {
        onView(withId(R.id.notifications_settings_outdoor_activity_switch)).perform(click())
        assertViewHasCaption(
            R.id.notifications_settings_outdoor_activity_switch,
            R.string.notifications_settings_screen_outdoor_activity_switch_disabled_text
        )
    }

    @Test
    fun when_quality_of_life_switch_off_disabled_message_is_displayed_radio_buttons_not_visible() {
        onView(withId(R.id.notifications_settings_quality_of_life_switch)).perform(click())
        assertViewHasCaption(
            R.id.notifications_settings_quality_of_life_switch,
            R.string.notifications_settings_screen_outdoor_activity_switch_disabled_text
        )

        assertViewsNotDisplayed(R.id.notifications_settings_radio_group)
    }

    @Test
    fun when_logging_switch_off_disabled_message_is_displayed() {
        onView(withId(R.id.notifications_settings_logging_switch)).check(matches(isNotChecked()))
            .perform(
                click()
            ).check(matches(isChecked()))

        assertViewHasCaption(
            R.id.notifications_settings_logging_switch,
            R.string.notifications_settings_screen_logging_switch_enabled_text
        )
    }

    @Test
    fun correct_last_sync_date_is_shown() {

        onView(withId(R.id.notifications_settings_screen_connection_status)).check(
            matches(
                withText(
                    syncMessage
                )
            )
        )
    }
}