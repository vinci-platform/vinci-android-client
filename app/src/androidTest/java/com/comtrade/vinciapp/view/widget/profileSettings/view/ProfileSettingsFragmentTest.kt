/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: ProfileSettingsFragmentTest.kt
 * Author: sdelic
 *
 * History:  28.3.22. 12:36 created
 */

package com.comtrade.vinciapp.view.widget.profileSettings.view

import androidx.fragment.app.testing.launchFragmentInContainer
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.util.assertButtonsEnabled
import com.comtrade.vinciapp.util.assertViewHasCaption
import com.comtrade.vinciapp.util.assertViewsDisplayed
import org.junit.Before
import org.junit.Test

/**
 * ProfileSettingsFragment screen UI tests.
 * Assert all of the view components are displayed.
 * Assert that all views are in their correct initial state.
 */
internal class ProfileSettingsFragmentTest {

    @Before
    fun setUp() {
        launchFragmentInContainer<ProfileSettingsFragment>()
    }

    @Test
    fun profile_settings_is_displayed() {
        assertViewsDisplayed(
            R.id.profile_settings_edit_personal_data_button,
            R.id.profile_settings_change_profile_picture_button,
            R.id.profile_settings_change_emergency_number_button,
            R.id.profile_settings_change_password_button,
        )
    }

    @Test
    fun profile_settings_buttons_are_enabled() {
        assertButtonsEnabled(
            R.id.profile_settings_edit_personal_data_button,
            R.id.profile_settings_change_profile_picture_button,
            R.id.profile_settings_change_emergency_number_button,
            R.id.profile_settings_change_password_button,
        )
    }

    @Test
    fun profile_edit_personal_data__button_has_correct_caption() {
        assertViewHasCaption(
            R.id.profile_settings_edit_personal_data_button,
            R.string.profile_settings_screen_edit_personal_data
        )
    }

    @Test
    fun change_profile_picture_button_has_correct_caption() {
        assertViewHasCaption(
            R.id.profile_settings_change_profile_picture_button,
            R.string.profile_settings_screen_change_profile_picture
        )
    }

    @Test
    fun change_emergency_number_button_has_correct_caption() {
        assertViewHasCaption(
            R.id.profile_settings_change_emergency_number_button,
            R.string.profile_settings_screen_change_emergency_number
        )
    }

    @Test
    fun change_password_button_has_correct_caption() {
        assertViewHasCaption(
            R.id.profile_settings_change_password_button,
            R.string.create_new_password_screen_create_button_text
        )
    }
}