/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: QuestionnaireSurveyEndScreenTest.kt
 * Author: sdelic
 *
 * History:  18.3.22. 10:32 created
 */

package com.comtrade.vinciapp.view.widget.questionnaireSurvey.view.questionViews

import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.util.assertViewHasCaption
import com.comtrade.vinciapp.util.assertViewsDisplayed
import org.junit.Before
import org.junit.Test

/**
 * QuestionnaireSurveyEndScreen screen UI tests.
 * Assert all of the view components are displayed.
 * Assert that all views are in their correct initial state.
 */
internal class QuestionnaireSurveyEndScreenTest {

    @Before
    fun setUp() {
        launchFragmentInContainer<QuestionnaireSurveyEndScreen>()
    }

    @Test
    fun questionnaire_survey_end_screen_is_displayed() {
        assertViewsDisplayed(
            R.id.survey_questionnaire_end_screen_scroll,
            R.id.survey_questionnaire_end_screen_text_first,
            R.id.survey_questionnaire_end_score_result_title,
            R.id.survey_questionnaire_end_score_result,
            R.id.survey_questionnaire_end_screen_text_second
        )
    }

    @Test
    fun congratulations_message_is_displayed() {
        assertViewHasCaption(
            R.id.survey_questionnaire_end_screen_text_first,
            R.string.survey_questionnaire_end_screen_text_first,
        )
    }

    @Test
    fun score_result_title_text_is_displayed() {
        assertViewHasCaption(
            R.id.survey_questionnaire_end_score_result_title,
            R.string.survey_questionnaire_end_score_results_title,
        )
    }

    @Test
    fun score_result_score_value_is_displayed() {
        onView(withId(R.id.survey_questionnaire_end_score_result)).check(matches(withText("0")))
    }

    @Test
    fun questionnaire_message_is_displayed() {
        assertViewHasCaption(
            R.id.survey_questionnaire_end_screen_text_second,
            R.string.survey_questionnaire_end_screen_text_second
        )
    }
}