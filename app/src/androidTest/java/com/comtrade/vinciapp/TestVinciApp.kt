package com.comtrade.vinciapp

import com.comtrade.feature_account.di.AccountComponent
import com.comtrade.vinciapp.di.AppComponent
import com.comtrade.vinciapp.di.DaggerTestAppComponent

/**
 * Test Application entry point.
 * Provides the testing dependency initializations instead of the production ones.
 * Using the for the custom Android jUnit runner component.
 */
internal class TestVinciApp : VinciApp() {

    private lateinit var accountComponent: AccountComponent


    override fun onCreate() {
        super.onCreate()

        accountComponent = appComponent.accountComponentFactory.create()

    }

    override val appComponent: AppComponent by lazy {
        DaggerTestAppComponent.factory().create(this@TestVinciApp)
    }

    override fun getAccountComponent(): AccountComponent {
        return accountComponent
    }
}