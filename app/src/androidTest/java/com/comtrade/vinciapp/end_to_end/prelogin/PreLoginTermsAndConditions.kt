package com.comtrade.vinciapp.end_to_end.prelogin

import androidx.lifecycle.Lifecycle
import androidx.test.core.app.ApplicationProvider
import androidx.test.core.app.launchActivity
import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.filters.LargeTest
import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.TestVinciApp
import com.comtrade.vinciapp.util.assertViewsDisplayed
import com.comtrade.vinciapp.view.activity.prelogin.view.PreloginActivity
import com.comtrade.vinciapp.view.widget.terms.view.TermsFragmentTest
import org.hamcrest.CoreMatchers
import org.hamcrest.core.AllOf
import org.junit.After
import org.junit.Assert.assertFalse
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito

/**
 * PreLogin screen navigation test.
 * Toggling the terms and conditions state and navigating to the login and account creation screens.
 */
@LargeTest
internal class PreLoginTermsAndConditions {

    private lateinit var prefs: IPrefs

    @Before
    fun setUp() {

        val application = ApplicationProvider.getApplicationContext<TestVinciApp>()
        prefs = application.appComponent.provideAppPrefs()
        Mockito.`when`(prefs.isTermsAccepted).thenReturn(false)
    }

    @After
    fun tearDown() {
        Mockito.reset(prefs)
    }

    @Test
    fun toggle_button_visibility_terms_and_conditions() {
        val scenario = launchActivity<PreloginActivity>()
        scenario.moveToState(Lifecycle.State.RESUMED)

        val isTerms = prefs.isTermsAccepted
        assertFalse(isTerms)

        //Assert the terms and conditions checkbox is displayed and unchecked
        Espresso.onView(ViewMatchers.withId(R.id.welcome_fragment_terms_checkbox)).check(
            ViewAssertions.matches(
                AllOf.allOf(
                    ViewMatchers.isCompletelyDisplayed(),
                    ViewMatchers.isNotChecked()
                )
            )
        )


        //All buttons should be visible and in the disabled state and not clickable.
        val buttonIds = arrayOf(
            R.id.welcome_fragment_login_button, R.id.welcome_fragment_create_account_button,
            R.id.welcome_fragment_facebook_button, R.id.welcome_fragment_google_button
        )

        for (buttonId in buttonIds) {
            Espresso.onView(ViewMatchers.withId(buttonId)).check(
                ViewAssertions.matches(
                    AllOf.allOf(
                        ViewMatchers.isCompletelyDisplayed(),
                        CoreMatchers.not(ViewMatchers.isEnabled())
                    )
                )
            )
        }

        //Set terms and conditions checkbox to the check state and assert that the terms and conditions screen is showing
        Espresso.onView(ViewMatchers.withId(R.id.welcome_fragment_terms_checkbox))
            .perform(ViewActions.click())
        assertViewsDisplayed(
            R.id.terms_fragment_title,
            R.id.terms_fragment_scroll_view,
        )

        //Scroll to the end of the Terms scroll in order to see the 'accept' button
        for (i in 0..TermsFragmentTest.NumberOfSwipesNeeded) {
            Espresso.onView(ViewMatchers.withId(R.id.terms_fragment_scroll_view))
                .perform(ViewActions.swipeUp())
        }

        //Click on the accept terms button and go back to the prelogin screen with the checkbox checked and the buttons enabled.
        Mockito.`when`(prefs.isTermsAccepted).thenReturn(true)
        Espresso.onView(ViewMatchers.withId(R.id.terms_fragment_accept_button))
            .perform(ViewActions.click())

        Espresso.onView(ViewMatchers.withId(R.id.welcome_fragment_terms_checkbox)).check(
            ViewAssertions.matches(
                AllOf.allOf(
                    ViewMatchers.isCompletelyDisplayed(),
                    ViewMatchers.isChecked()
                )
            )
        )


        //Login and create account buttons should be visible and in the enabled state and clickable.
        val loginCreateButtonIds = arrayOf(
            R.id.welcome_fragment_login_button, R.id.welcome_fragment_create_account_button
        )
        for (buttonId in loginCreateButtonIds) {
            Espresso.onView(ViewMatchers.withId(buttonId)).check(
                ViewAssertions.matches(
                    AllOf.allOf(
                        ViewMatchers.isCompletelyDisplayed(),
                        ViewMatchers.isEnabled()
                    )
                )
            )
        }
    }
}