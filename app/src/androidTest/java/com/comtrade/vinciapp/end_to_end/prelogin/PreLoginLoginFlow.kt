package com.comtrade.vinciapp.end_to_end.prelogin

import android.text.InputType
import androidx.lifecycle.Lifecycle
import androidx.test.core.app.ApplicationProvider
import androidx.test.core.app.launchActivity
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.TestVinciApp
import com.comtrade.vinciapp.util.assertEditFieldForHintAndInputType
import com.comtrade.vinciapp.util.assertViewsDisplayed
import com.comtrade.vinciapp.view.activity.prelogin.view.PreloginActivity
import com.comtrade.vinciapp.view.widget.terms.view.TermsFragmentTest
import org.hamcrest.CoreMatchers
import org.hamcrest.core.AllOf.allOf
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito

/**
 * PreLogin screen navigation test.
 * Accept terms and conditions and navigate to the Login screen.
 * Disable terms and conditions and make sure that the Login screen option is disabled.
 */
internal class PreLoginLoginFlow {

    private lateinit var prefs: IPrefs

    @Before
    fun setUp() {
        //Setting terms and conditions as not accepted.
        val application = ApplicationProvider.getApplicationContext<TestVinciApp>()
        prefs = application.appComponent.provideAppPrefs()
        Mockito.`when`(prefs.isTermsAccepted).thenReturn(false)
    }

    @After
    fun tearDown() {
        Mockito.reset(prefs)
    }


    @Test
    fun accept_terms_and_navigate_to_login() {
        accept_terms_and_conditions()

        //Click on the login button and assert that the login screen is displayed.
        onView(withId(R.id.welcome_fragment_login_button)).perform(click())

        assertViewsDisplayed(
            R.id.login_fragment_username_edit_text,
            R.id.login_fragment_password_edit_text,
            R.id.login_fragment_login_button,
            R.id.login_fragment_forgot_password_text_view
        )

        //Assert that the email and password field are empty and correct hints are displayed.
        assertEditFieldForHintAndInputType(
            R.id.login_fragment_username_edit_text,
            R.string.login_screen_enter_email_hint,
            InputType.TYPE_CLASS_TEXT
        )

        assertEditFieldForHintAndInputType(
            R.id.login_fragment_password_edit_text,
            R.string.login_screen_enter_password_hint,
            InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
        )
    }

    @Test
    fun accept_terms_and_navigate_to_create_account() {
        accept_terms_and_conditions()

        //Click on the create account button and assert that the create account screen is displayed.
        onView(withId(R.id.welcome_fragment_create_account_button)).perform(click())

        assertViewsDisplayed(
            R.id.create_user_fragment_scroll,
            R.id.create_user_fragment_username_edit_text,
            R.id.create_user_fragment_email_edit_text,
            R.id.create_user_fragment_create_password_edit_text,
            R.id.create_user_fragment_repeat_password_edit_text,
            R.id.create_user_fragment_user_role_spinner,
            R.id.create_user_fragment_user_role_arrow_view,
            R.id.create_user_fragment_confirm_button
        )

        //Assert that the input fields are empty and correct hints are displayed.
        assertEditFieldForHintAndInputType(
            R.id.create_user_fragment_username_edit_text,
            R.string.create_user_screen_enter_username_hint,
            InputType.TYPE_CLASS_TEXT
        )

        assertEditFieldForHintAndInputType(
            R.id.create_user_fragment_email_edit_text,
            R.string.create_user_screen_enter_email_hint,
            InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS
        )

        assertEditFieldForHintAndInputType(
            R.id.create_user_fragment_create_password_edit_text,
            R.string.create_user_screen_create_password_hint,
            InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
        )

        assertEditFieldForHintAndInputType(
            R.id.create_user_fragment_repeat_password_edit_text,
            R.string.create_user_screen_repeat_password_hint,
            InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
        )

    }

    /**
     * Espresso assertion of the navigation flow for accepting the terms and conditions and enabling
     * the login functionality for the whole app.
     */
    private fun accept_terms_and_conditions() {
        val scenario = launchActivity<PreloginActivity>()
        scenario.moveToState(Lifecycle.State.RESUMED)
        Mockito.`when`(prefs.isTermsAccepted).thenReturn(true)
        //Assert the terms and conditions checkbox is displayed and unchecked
        onView(withId(R.id.welcome_fragment_terms_checkbox)).check(
            matches(
                allOf(
                    isCompletelyDisplayed(),
                    isNotChecked()
                )
            )
        )


        //All buttons should be visible and in the disabled state and not clickable.
        val buttonIds = arrayOf(
            R.id.welcome_fragment_login_button, R.id.welcome_fragment_create_account_button,
            R.id.welcome_fragment_facebook_button, R.id.welcome_fragment_google_button
        )

        for (buttonId in buttonIds) {
            onView(withId(buttonId)).check(
                matches(
                    allOf(
                        isCompletelyDisplayed(),
                        CoreMatchers.not(isEnabled())
                    )
                )
            )
        }

        //Set terms and conditions checkbox to the check state and assert that the terms and conditions screen is showing
        onView(withId(R.id.welcome_fragment_terms_checkbox))
            .perform(click())
        assertViewsDisplayed(
            R.id.terms_fragment_title,
            R.id.terms_fragment_scroll_view,
        )

        //Scroll to the end of the Terms scroll in order to see the 'accept' button
        for (i in 0..TermsFragmentTest.NumberOfSwipesNeeded) {
            onView(withId(R.id.terms_fragment_scroll_view))
                .perform(ViewActions.swipeUp())
        }

        //Click on the accept terms button and go back to the prelogin screen with the checkbox checked and the buttons enabled.
        onView(withId(R.id.terms_fragment_accept_button))
            .perform(click())
        Mockito.`when`(prefs.isTermsAccepted).thenReturn(true)

        onView(withId(R.id.welcome_fragment_terms_checkbox)).check(
            matches(
                allOf(
                    isCompletelyDisplayed(),
                    isChecked()
                )
            )
        )


        //Login and create account buttons should be visible and in the enabled state and clickable.
        val loginCreateButtonIds = arrayOf(
            R.id.welcome_fragment_login_button, R.id.welcome_fragment_create_account_button
        )
        for (buttonId in loginCreateButtonIds) {
            onView(withId(buttonId)).check(
                matches(
                    allOf(
                        isCompletelyDisplayed(),
                        isEnabled()
                    )
                )
            )
        }
    }
}