/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: MainFragmentNavigateEndToEndTest.kt
 * Author: sdelic
 *
 * History:  15.3.22. 12:03 created
 */

package com.comtrade.vinciapp.end_to_end.main_screen.main_fragment_navigation

import androidx.annotation.IdRes
import androidx.lifecycle.Lifecycle
import androidx.test.core.app.ActivityScenario
import androidx.test.core.app.launchActivity
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.Espresso.pressBack
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.filters.LargeTest
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.end_to_end.main_screen.MainActivityAuthSetup
import com.comtrade.vinciapp.util.assertViewsDisplayed
import com.comtrade.vinciapp.view.activity.main.view.MainActivity
import org.junit.Test

/**
 * End to end test that covers the display of the user-flow for the navigation on the home/starting screen.
 * Asserting the individual screen displays and navigation.
 * Starting point: MainActivity.
 */
@LargeTest
internal class MainFragmentNavigateEndToEndTest : MainActivityAuthSetup() {

    private lateinit var activityScenario: ActivityScenario<MainActivity>

    override fun setUp() {
        super.setUp()
        activityScenario = launchActivity()
    }

    override fun tearDown() {
        super.tearDown()
        activityScenario.moveToState(Lifecycle.State.DESTROYED)
    }

    @Test
    fun user_can_navigate_through_main_screen_containers() {
        assertMainScreenDisplayed()

        // Go to events screen
        val eventScreenViews = arrayOf(
            R.id.next_event_list_view,
            R.id.next_event_view_fragment_buttons_container,
            R.id.next_event_view_fragment_add_activity_button,
        )
        assertScreen(R.id.main_screen_fragment_event_background_image, eventScreenViews)
        assertMainScreenDisplayed()

        // Go to quality of life
        val qualityOfLifeViews = arrayOf(
            R.id.survey_questionnaire_fragment_view_pager,
            R.id.survey_questionnaire_fragment_nav_buttons_container,
            R.id.survey_questionnaire_fragment_back_button,
            R.id.survey_questionnaire_fragment_next_button,
        )
        assertScreen(R.id.main_screen_fragment_quality_of_life_container, qualityOfLifeViews)
        assertMainScreenDisplayed()

        // Go to health and activity
        val healthAndActivityViews = arrayOf(
            R.id.physical_activity_fragment_step_count_container,
            R.id.physical_activity_fragment_outdoor_activities_container,
            R.id.physical_activity_fragment_heart_rate_container,
            R.id.physical_activity_fragment_calories_container,
            R.id.physical_activity_fragment_sleep_container,
            R.id.physical_activity_fragment_health_points_container,
            R.id.physical_activity_fragment_IPAQ_button,
        )
        assertScreen(R.id.main_screen_fragment_physical_activity_container, healthAndActivityViews)
        assertMainScreenDisplayed()

        // Go to friends screen
        val friendViews = arrayOf(
            R.id.friends_view_list,
            R.id.friends_view_fragment_buttons_container,
            R.id.friends_view_fragment_add_button
        )
        assertScreen(R.id.main_screen_fragment_friends_container, friendViews)
        assertMainScreenDisplayed()

        // Go to cognitive games
        val cognitiveGameViews = arrayOf(
            R.id.cognitive_games_fragment_scroll,
            R.id.cognitive_games_screen_text_first,
            R.id.cognitive_games_fragment_spinner,
            R.id.cognitive_games_fragment_arrow_view,
            R.id.cognitive_games_fragment_datetime_spinner,
            R.id.cognitive_games_fragment_datetime_calendar_icon,
            R.id.cognitive_games_fragment_invite_friends_checkbox
        )
        assertScreen(R.id.main_screen_fragment_cognitive_games_container, cognitiveGameViews)
        assertMainScreenDisplayed()

        // Go to emergency call
        val emergencyCallViews = arrayOf(
            R.id.setting_emergency_number_vertical_layout,
            R.id.setting_emergency_number_choose_contact_text,
            R.id.setting_emergency_number_choose_contact_button,
            R.id.setting_emergency_number_phone_number_layout,
            R.id.setting_emergency_number_phone_number,
            R.id.setting_emergency_number_button_layout,
            R.id.setting_emergency_number_cancel_button,
            R.id.setting_emergency_number_ok_button
        )
        assertScreen(R.id.main_screen_fragment_emergency_call_container, emergencyCallViews)
        assertMainScreenDisplayed()

        // Go to today's feelings
        val todayFeelingsViews = arrayOf(
            R.id.outdoor_activities_fragment_text,
            R.id.feeling_dialog_fragment_very_sad_image_button,
            R.id.feeling_dialog_fragment_sad_image_button,
            R.id.feeling_dialog_fragment_neutral_image_button,
            R.id.feeling_dialog_fragment_happy_image_button,
            R.id.feeling_dialog_fragment_very_happy_image_button,
            R.id.survey_questionnaire_fragment_nav_buttons_container,
            R.id.feeling_dialog_fragment_cancel_button,
            R.id.feeling_dialog_fragment_ok_button
        )
        assertScreen(R.id.main_screen_fragment_today_feelings_container, todayFeelingsViews)
        assertMainScreenDisplayed()

        // Go to profile settings
        val profileSettingsViews = arrayOf(
            R.id.profile_settings_edit_personal_data_button,
            R.id.profile_settings_change_profile_picture_button,
            R.id.profile_settings_change_emergency_number_button,
            R.id.profile_settings_change_password_button,
        )

        assertScreen(R.id.main_screen_fragment_profile_image, profileSettingsViews)
        assertMainScreenDisplayed()

    }

    /**
     * Navigate to and from a screen with display assertion
     * @param buttonId The click handler
     * @param viewIds The navigating screen view Id's
     */
    private fun assertScreen(@IdRes buttonId: Int, viewIds: Array<Int>) {
        onView(withId(buttonId)).perform(click())

        for (viewId in viewIds) {
            assertViewsDisplayed(viewId)
        }
        pressBack()
    }

    /**
     *  Main/Home screen view assertion.
     */
    private fun assertMainScreenDisplayed() {
        assertViewsDisplayed(
            R.id.main_screen_fragment_profile_image_backgound,
            R.id.main_screen_fragment_profile_image,
            R.id.main_screen_fragment_devices_background_border,
            R.id.main_screen_fragment_devices_background,
            R.id.main_screen_fragment_devices_icon,
            R.id.main_screen_fragment_profile_name,
            R.id.main_screen_fragment_points_label,
            R.id.main_screen_fragment_points,
            R.id.main_screen_fragment_event_background_image,
            R.id.main_screen_fragment_event_text_view,
            R.id.main_screen_fragment_quality_of_life_container,
            R.id.main_screen_fragment_friends_container,
            R.id.main_screen_fragment_emergency_call_container,
            R.id.main_screen_fragment_physical_activity_container,
            R.id.main_screen_fragment_cognitive_games_container,
            R.id.main_screen_fragment_today_feelings_container
        )
    }
}