/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: IPAQuestionnaireEndToEndTest.kt
 * Author: sdelic
 *
 * History:  9.3.22. 13:03 created
 */

package com.comtrade.vinciapp.end_to_end.main_screen.physical_activities

import android.text.InputType
import androidx.annotation.StringRes
import androidx.test.core.app.launchActivity
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.LargeTest
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.end_to_end.main_screen.MainActivityAuthSetup
import com.comtrade.vinciapp.util.assertEditFieldForHintAndInputType
import com.comtrade.vinciapp.util.assertTextDisplayed
import com.comtrade.vinciapp.util.assertViewsDisplayed
import com.comtrade.vinciapp.view.activity.main.view.MainActivity
import org.hamcrest.CoreMatchers.allOf
import org.hamcrest.CoreMatchers.not
import org.junit.Test

/**
 * End to end test that covers the display of the user-flow for the IPA Questionnaire physical activity feature.
 * Asserting the individual screen displays and navigation.
 * Starting point: MainActivity.
 */
@LargeTest
internal class IPAQuestionnaireEndToEndTest : MainActivityAuthSetup() {

    override fun setUp() {
        super.setUp()
        launchActivity<MainActivity>()
    }


    /**
     * Positive User flow.
     */
    @Test
    fun user_can_navigate_through_the_ips_questionnaire_by_answering_questions() {
        navigateToIPAQuestionnaireFromTheMainScreen()

        assertIntroductionParagraphs()

        //Advance to first question
        assertFirstQuestion()

        //Advance to second question
        assertSecondQuestion()

        //Advance to third question
        assertThirdQuestion()

        //Advance to fourth question
        assertFourthQuestion()

        //Advance to score
        assertScoreDisplay()
    }

    /**
     * Negative User flow.
     */
    @Test
    fun user_can_navigate_through_the_ips_questionnaire_by_selecting_none() {
        navigateToIPAQuestionnaireFromTheMainScreen()

        //Advance to intro paragraph
        assertIntroductionParagraphs()

        //Advance to first question
        assertFirstQuestion()

        //Advance to second question
        assertSecondQuestion(false)

        //Advance to third question
        assertThirdQuestion(false)

        //Advance to fourth question
        assertFourthQuestion(false)

        //Advance to score
        assertScoreDisplay()
    }

    /**
     * Bug fix with swiping on some answers.
     */
    @Test
    fun user_can_navigate_through_the_questionnaire_by_swiping_on_some_answers() {
        navigateToIPAQuestionnaireFromTheMainScreen()

        //Advance to intro paragraph
        assertIntroductionParagraphs()

        //Advance to first question
        assertFirstQuestion()

        //Skip to second question with swipe
        onView(withId(R.id.ipa_questionnaire_fragment_view_pager)).perform(swipeLeft())
        assertSecondQuestion(false)

        //Advance to third question
        assertThirdQuestion()

        //Advance to fourth question
        assertFourthQuestion(false)

        //Advance to score
        assertScoreDisplay()

    }

    private fun navigateToIPAQuestionnaireFromTheMainScreen() {
        assertViewsDisplayed(R.id.main_screen_fragment_physical_activity_container)
        assertTextDisplayed(R.string.main_screen_physical_activity_text)

        //Go to health and activity
        onView(withId(R.id.main_screen_fragment_physical_activity_container)).perform(click())
        assertViewsDisplayed(
            R.id.physical_activity_fragment_step_count_container,
            R.id.physical_activity_fragment_outdoor_activities_container,
            R.id.physical_activity_fragment_heart_rate_container,
            R.id.physical_activity_fragment_calories_container,
            R.id.physical_activity_fragment_sleep_container,
            R.id.physical_activity_fragment_health_points_container,
            R.id.physical_activity_fragment_IPAQ_button
        )

        //Go to IPA questionnaire
        onView(withId(R.id.physical_activity_fragment_IPAQ_button)).perform(click())
        assertViewsDisplayed(
            R.id.ipa_questionnaire_fragment_view_pager,
            R.id.ipa_questionnaire_fragment_nav_buttons_container,
            R.id.ipa_questionnaire_fragment_back_button,
            R.id.ipa_questionnaire_fragment_next_button
        )
    }

    private fun assertIntroductionParagraphs() {
        //Intro paragraphs are showing.
        assertViewsDisplayed(
            R.id.outdoor_activities_fragment_scroll,
            R.id.ipa_questionnaire_start_view_title,
            R.id.ipa_questionnaire_start_view_description_first_paragraph,
            R.id.ipa_questionnaire_start_view_description_second_paragraph,
            R.id.ipa_questionnaire_start_view_description_third_paragraph,
        )
        //Scroll up to see all the paragraphs
        onView(withId(R.id.outdoor_activities_fragment_scroll)).perform(swipeUp())

        assertViewsDisplayed(
            R.id.ipa_questionnaire_start_view_description_fourth_paragraph,
            R.id.ipa_questionnaire_start_view_description_fifth_paragraph,
            R.id.ipa_questionnaire_start_view_description_sixth_paragraph,
            R.id.ipa_questionnaire_start_view_description_seventh_paragraph
        )
    }

    private fun assertFirstQuestion() {
        onView(withId(R.id.ipa_questionnaire_fragment_next_button)).perform(click())

        assertViewsDisplayed(
            R.id.ipa_questionnaire_screen_input_question_container,
            R.id.ipa_questionnaire_screen_input_question_number,
            R.id.ipa_questionnaire_screen_input_question_description,
            R.id.ipa_questionnaire_screen_input_question_question,
            R.id.ipa_questionnaire_screen_input_question_edit_text_hours,
            R.id.ipa_questionnaire_screen_input_question_edit_text_minutes
        )

        assertEditFieldForHintAndInputType(
            R.id.ipa_questionnaire_screen_input_question_edit_text_hours,
            R.string.ipa_questionnaire_screen_input_hint_hours,
            InputType.TYPE_CLASS_NUMBER
        )

        assertEditFieldForHintAndInputType(
            R.id.ipa_questionnaire_screen_input_question_edit_text_minutes,
            R.string.ipa_questionnaire_screen_input_hint_minutes,
            InputType.TYPE_CLASS_NUMBER
        )

        //Assert not advancing without filling in all of the fields.
        onView(withId(R.id.ipa_questionnaire_fragment_next_button)).perform(click())

        assertViewsDisplayed(
            R.id.ipa_questionnaire_screen_input_question_container,
            R.id.ipa_questionnaire_screen_input_question_number,
            R.id.ipa_questionnaire_screen_input_question_description,
        )

        onView(withId(R.id.ipa_questionnaire_screen_input_question_edit_text_hours)).perform(
            typeText("1"), closeSoftKeyboard()
        )

        onView(withId(R.id.ipa_questionnaire_fragment_next_button)).perform(click())

        assertViewsDisplayed(
            R.id.ipa_questionnaire_screen_input_question_container,
            R.id.ipa_questionnaire_screen_input_question_number,
            R.id.ipa_questionnaire_screen_input_question_description,
        )

        onView(withId(R.id.ipa_questionnaire_screen_input_question_edit_text_minutes)).perform(
            typeText("59"), closeSoftKeyboard()
        )
    }

    private fun assertSecondQuestion(isPositiveFlow: Boolean = true) {

        val questionStrings = arrayOf(
            R.string.IPA_questionnaire_screen_question_2_number,
            R.string.IPA_questionnaire_screen_question_2_description,
            R.string.IPA_questionnaire_screen_question_2_question,
        )
        assertRadioButtonQuestionnaire(
            questionStrings,
            R.string.IPA_questionnaire_screen_question_2_hidden_question,
            isPositiveFlow
        )


    }

    private fun assertThirdQuestion(isPositiveFlow: Boolean = true) {
        val questionStrings = arrayOf(
            R.string.IPA_questionnaire_screen_question_3_number,
            R.string.IPA_questionnaire_screen_question_3_description,
            R.string.IPA_questionnaire_screen_question_3_question
        )
        assertRadioButtonQuestionnaire(
            questionStrings,
            R.string.IPA_questionnaire_screen_question_3_hidden_question,
            isPositiveFlow
        )
    }

    private fun assertFourthQuestion(isPositiveFlow: Boolean = true) {
        val questionStrings = arrayOf(
            R.string.IPA_questionnaire_screen_question_4_number,
            R.string.IPA_questionnaire_screen_question_4_description,
            R.string.IPA_questionnaire_screen_question_4_question
        )
        assertRadioButtonQuestionnaire(
            questionStrings,
            R.string.IPA_questionnaire_screen_question_4_hidden_question,
            isPositiveFlow
        )
    }

    private fun assertScoreDisplay() {
        onView(withId(R.id.ipa_questionnaire_fragment_next_button)).perform(click())

        assertViewsDisplayed(
            R.id.ipa_questionnaire_end_score_result_title,
            R.id.ipa_questionnaire_end_score_result,
            R.id.ipa_questionnaire_end_view_title,
            R.id.ipa_questionnaire_fragment_save_button
        )

        onView(withId(R.id.ipa_questionnaire_fragment_save_button)).check(matches(withText(R.string.common_button_save)))

        assertTextDisplayed(
            R.string.IPA_questionnaire_end_score_results_title,
            R.string.IPA_questionnaire_end_view_title
        )

        //Assert score not empty
        onView(withId(R.id.ipa_questionnaire_end_score_result)).check(matches(not(withText(""))))

    }

    private fun assertRadioButtonQuestionnaire(
        visibleQuestions: Array<Int>,
        @StringRes hiddenQuestion: Int,
        isPositiveFlow: Boolean = true
    ) {
        onView(withId(R.id.ipa_questionnaire_fragment_next_button)).perform(click())

        for (visibleQuestion in visibleQuestions) {
            assertTextDisplayed(visibleQuestion)
        }

        assertTextDisplayed(
            R.string.common_button_none,
            R.string.radio_button_days
        )

        if (isPositiveFlow) {
            //Click on days to show all fields.
            onView(allOf(withText(R.string.radio_button_days), isDisplayed())).perform(click())

            assertTextDisplayed(
                hiddenQuestion,
            )

            onView(
                allOf(
                    withId(R.id.ipa_questionnaire_screen_radio_button_question_edit_text_days),
                    isDisplayed()
                )
            ).check(matches(withHint(R.string.ipa_questionnaire_screen_input_hint_days)))

            onView(
                allOf(
                    withId(R.id.ipa_questionnaire_screen_radio_button_question_edit_text_hours),
                    isDisplayed()
                )
            ).check(matches(withHint(R.string.ipa_questionnaire_screen_input_hint_hours)))

            onView(
                allOf(
                    withId(R.id.ipa_questionnaire_screen_radio_button_question_edit_text_minutes),
                    isDisplayed()
                )
            ).check(matches(withHint(R.string.ipa_questionnaire_screen_input_hint_minutes)))

            //Not advancing until filling in all fields.
            onView(withId(R.id.ipa_questionnaire_fragment_next_button)).perform(click())

            for (visibleQuestion in visibleQuestions) {
                assertTextDisplayed(visibleQuestion)
            }

            //Days,hours and minutes
            onView(
                allOf(
                    withId(R.id.ipa_questionnaire_screen_radio_button_question_edit_text_days),
                    isDisplayed()
                )
            ).perform(
                typeText("1"), closeSoftKeyboard()
            )

            onView(
                allOf(
                    withId(R.id.ipa_questionnaire_screen_radio_button_question_edit_text_hours),
                    isDisplayed()
                )
            ).perform(
                typeText("1"), closeSoftKeyboard()
            )

            onView(
                allOf(
                    withId(R.id.ipa_questionnaire_screen_radio_button_question_edit_text_minutes),
                    isDisplayed()
                )
            ).perform(
                typeText("1"), closeSoftKeyboard()
            )
        } else {
            //Click on none to show all fields.
            onView(allOf(withText(R.string.common_button_none), isDisplayed())).perform(click())
        }
    }

}