package com.comtrade.vinciapp.end_to_end.login

import org.junit.Test

/**
 * Login screen navigation test.
 * Assert the input field validations.
 * Try a wrong username and password before trying a correct test username and password.
 */
internal class LoginFlow {

    @Test
    fun login_screen_is_displayed() {
    }
}