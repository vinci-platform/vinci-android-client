/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: GameSudokuEndToEndTest.kt
 * Author: sdelic
 *
 * History:  18.4.22. 11:14 created
 */

package com.comtrade.vinciapp.end_to_end.main_screen.cognitive_games

import android.Manifest
import android.content.Context
import androidx.annotation.StringRes
import androidx.test.core.app.ApplicationProvider
import androidx.test.core.app.launchActivity
import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.filters.LargeTest
import androidx.test.rule.GrantPermissionRule
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.end_to_end.main_screen.MainActivityAuthSetup
import com.comtrade.vinciapp.util.assertTextDisplayed
import com.comtrade.vinciapp.util.assertViewHasCaption
import com.comtrade.vinciapp.util.assertViewsDisplayed
import com.comtrade.vinciapp.view.activity.main.view.MainActivity
import org.hamcrest.CoreMatchers
import org.hamcrest.core.AllOf
import org.junit.Rule
import org.junit.Test

/**
 * End to end test that covers the display of the user-flow for the Cognitive games - sudoku activity feature.
 * Asserting the individual screen displays and navigation.
 * Starting point: MainActivity.
 */
@LargeTest
internal class GameSudokuEndToEndTest : MainActivityAuthSetup() {

    private lateinit var supportedGames: Array<String>

    @Rule
    @JvmField
    val grantContactPermissionRule: GrantPermissionRule =
        GrantPermissionRule.grant(Manifest.permission.READ_CONTACTS)

    override fun setUp() {
        super.setUp()
        supportedGames =
            ApplicationProvider.getApplicationContext<Context>().resources.getStringArray(R.array.supported_cognitive_games)

        launchActivity<MainActivity>()
    }

    @Test
    fun user_can_navigate_to_the_easy_sudoku_cognitive_game() {
        navigateToSudokuGameAndAssertState(
            supportedGames[0],
            R.string.cognitive_games_sudoku_easy
        )
    }

    @Test
    fun user_can_navigate_to_the_hard_sudoku_cognitive_game() {
        navigateToSudokuGameAndAssertState(
            supportedGames[1],
            R.string.cognitive_games_sudoku_hard
        )
    }

    /**
     * Navigates through the home menu to the cognitive game selection screen and asserts the view component states.
     * @param cognitiveGamesSelection Desired navigation endpoint
     * @param levelTextToBeDisplayed String resource for the level text to be asserted
     */
    private fun navigateToSudokuGameAndAssertState(
        cognitiveGamesSelection: String,
        @StringRes levelTextToBeDisplayed: Int
    ) {
        assertViewsDisplayed(R.id.main_screen_fragment_cognitive_games_container)
        assertTextDisplayed(R.string.main_screen_cognitive_games_text)

        // Go to the cognitive games screen
        Espresso.onView(ViewMatchers.withId(R.id.main_screen_fragment_cognitive_games_container))
            .perform(ViewActions.click())

        assertViewsDisplayed(
            R.id.cognitive_games_fragment_scroll,
            R.id.cognitive_games_screen_text_first,
            R.id.cognitive_games_fragment_spinner,
            R.id.cognitive_games_fragment_arrow_view,
            R.id.cognitive_games_fragment_datetime_spinner,
            R.id.cognitive_games_fragment_datetime_calendar_icon,
            R.id.cognitive_games_fragment_invite_friends_checkbox,
            R.id.cognitive_games_start_button,
            R.id.cognitive_games_save_button
        )

        // Select sudoku mode
        // Sudoku easy is the default selection
        Espresso.onView(ViewMatchers.withId(R.id.cognitive_games_fragment_spinner))
            .check(
                ViewAssertions.matches(
                    AllOf.allOf(
                        ViewMatchers.withSpinnerText(supportedGames[0]),
                        ViewMatchers.isCompletelyDisplayed(),
                        ViewMatchers.withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)
                    )
                )
            )

        Espresso.onView(ViewMatchers.withId(R.id.cognitive_games_fragment_spinner))
            .perform(ViewActions.click())
        Espresso.onData(
            AllOf.allOf(
                CoreMatchers.`is`(CoreMatchers.instanceOf(String::class.java)),
                CoreMatchers.`is`(cognitiveGamesSelection)
            )
        ).perform(
            ViewActions.click()
        )

        // Start the game
        Espresso.onView(ViewMatchers.withId(R.id.cognitive_games_start_button))
            .perform(ViewActions.click())

        // Sudoku displayed assertion
        assertViewsDisplayed(
            R.id.sudoku_title_layout,
            R.id.sudoku_fragment_level,
            R.id.sudoku_fragment_errors,
            R.id.sudoku_view,
            R.id.sudoku_grid,
            R.id.sudoku_button_1,
            R.id.sudoku_button_2,
            R.id.sudoku_button_3,
            R.id.sudoku_button_4,
            R.id.sudoku_button_5,
            R.id.sudoku_button_6,
            R.id.sudoku_button_7,
            R.id.sudoku_button_8,
            R.id.sudoku_button_9
        )

        assertViewHasCaption(R.id.sudoku_fragment_level, levelTextToBeDisplayed)
    }
}