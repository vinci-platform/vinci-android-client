/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: QualityOfLifeEndToEndTest.kt
 * Author: sdelic
 *
 * History:  7.3.22. 11:50 created
 */

package com.comtrade.vinciapp.end_to_end.main_screen.quality_of_life

import androidx.test.core.app.launchActivity
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.filters.LargeTest
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.end_to_end.main_screen.MainActivityAuthSetup
import com.comtrade.vinciapp.util.assertTextDisplayed
import com.comtrade.vinciapp.util.assertViewsDisplayed
import com.comtrade.vinciapp.view.activity.main.view.MainActivity
import org.hamcrest.CoreMatchers.allOf
import org.junit.Test
import kotlin.random.Random

/**
 * End to end test that covers the display of the user-flow for the Quality of Life feature.
 * Asserting the individual screen displays and navigation.
 * Starting with Quality of Life Questionnaire fragment.
 */
@LargeTest
internal class QualityOfLifeEndToEndTest : MainActivityAuthSetup() {

    override fun setUp() {
        super.setUp()
        launchActivity<MainActivity>()
    }

    @Test
    fun user_can_navigate_through_the_questionnaire_by_answering_questions() {
        assertViewsDisplayed(R.id.main_screen_fragment_quality_of_life_container)
        assertTextDisplayed(R.string.main_screen_quality_of_life_text)

        //Go to first questionnaire.
        onView(withId(R.id.main_screen_fragment_quality_of_life_container)).perform(click())
        assertViewsDisplayed(
            R.id.survey_questionnaire_fragment_nav_buttons_container,
            R.id.survey_questionnaire_fragment_back_button,
            R.id.survey_questionnaire_fragment_next_button
        )
        //Questions 1 to 26
        goThroughGradationQuestions(questions().size - 3)

        //Fill in questions 26-28 (26 is duplicated)
        assertTextDisplayed(questions()[25])
        assertTextDisplayed(questions()[26])
        assertTextDisplayed(questions()[27])

        val testInput = "end-to-end test data"
        val inputFields = arrayOf(
            R.id.survey_questionnaire_screen_question_27_answer,
            R.id.survey_questionnaire_screen_question_28_answer,
            R.id.survey_questionnaire_screen_question_29_answer
        )
        for (inputField in inputFields) {
            onView(allOf(withId(inputField), isDisplayed())).check(matches(isDisplayed()))
            onView(withId(inputField)).perform(typeText(testInput), closeSoftKeyboard())
        }

        //Go to final score screen
        onView(withId(R.id.survey_questionnaire_fragment_next_button)).perform(click())
        assertViewsDisplayed(
            R.id.survey_questionnaire_end_screen_scroll,
            R.id.survey_questionnaire_end_screen_text_first,
            R.id.survey_questionnaire_end_score_result_title,
            R.id.survey_questionnaire_end_score_result,
            R.id.survey_questionnaire_end_screen_text_second
        )

        assertTextDisplayed(
            R.string.survey_questionnaire_end_screen_text_first,
            R.string.survey_questionnaire_end_score_results_title
        )


    }

    private fun goThroughGradationQuestions(numQuestions: Int, possibleChoices: Int = 5) {
        for (i in 0 until numQuestions) {
            val randomAnswer = Random.nextInt(possibleChoices)
            assertTextDisplayed(questions()[i])
            onView(
                allOf(
                    withId(answerButtons()[randomAnswer]),
                    isDisplayed()
                )
            ).perform(click())

            //Go to next question
            onView(withId(R.id.survey_questionnaire_fragment_next_button)).perform(click())
        }
    }

    private fun questions(): Array<Int> {
        return arrayOf(
            R.string.survey_questionnaire_screen_question_1_text,
            R.string.survey_questionnaire_screen_question_2_text,
            R.string.survey_questionnaire_screen_question_3_text,
            R.string.survey_questionnaire_screen_question_4_text,
            R.string.survey_questionnaire_screen_question_5_text,
            R.string.survey_questionnaire_screen_question_6_text,
            R.string.survey_questionnaire_screen_question_7_text,
            R.string.survey_questionnaire_screen_question_8_text,
            R.string.survey_questionnaire_screen_question_9_text,
            R.string.survey_questionnaire_screen_question_10_text,
            R.string.survey_questionnaire_screen_question_11_text,
            R.string.survey_questionnaire_screen_question_12_text,
            R.string.survey_questionnaire_screen_question_13_text,
            R.string.survey_questionnaire_screen_question_14_text,
            R.string.survey_questionnaire_screen_question_15_text,
            R.string.survey_questionnaire_screen_question_16_text,
            R.string.survey_questionnaire_screen_question_17_text,
            R.string.survey_questionnaire_screen_question_18_text,
            R.string.survey_questionnaire_screen_question_19_text,
            R.string.survey_questionnaire_screen_question_20_text,
            R.string.survey_questionnaire_screen_question_21_text,
            R.string.survey_questionnaire_screen_question_22_text,
            R.string.survey_questionnaire_screen_question_23_text,
            R.string.survey_questionnaire_screen_question_24_text,
            R.string.survey_questionnaire_screen_question_25_text,
            R.string.survey_questionnaire_screen_question_26_text,
            R.string.survey_questionnaire_screen_question_27_text,
            R.string.survey_questionnaire_screen_question_28_text,
            R.string.survey_questionnaire_screen_question_29_text,
        )
    }

    private fun answerButtons(): Array<Int> {
        return arrayOf(
            R.id.survey_questionnaire_screen_button_1,
            R.id.survey_questionnaire_screen_button_2,
            R.id.survey_questionnaire_screen_button_3,
            R.id.survey_questionnaire_screen_button_4,
            R.id.survey_questionnaire_screen_button_5,
        )
    }
}