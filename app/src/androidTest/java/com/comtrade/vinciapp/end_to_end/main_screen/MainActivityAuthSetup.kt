/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: MainActivityAuthSetup.kt
 * Author: sdelic
 *
 * History:  4.5.22. 16:05 created
 */

package com.comtrade.vinciapp.end_to_end.main_screen

import android.Manifest
import androidx.test.core.app.ApplicationProvider
import androidx.test.filters.LargeTest
import androidx.test.rule.GrantPermissionRule
import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.domain.contracts.usecases.auth.IUserSignInUseCase
import com.comtrade.domain.entities.auth.UserAuthenticateResponseEntity
import com.comtrade.vinciapp.TestVinciApp
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.mockito.Mockito

/**
 * The MainActivity home screen authorization setup needed for all tests requiring the auth credentials to be granted.
 * Grants the location permission for advancing the test cases.
 */
@LargeTest
internal abstract class MainActivityAuthSetup {

    private val testToken = "testToken"
    private val testUsername = "testUsername"
    private val testPassword = "testPassword"
    private val testRememberMe = true

    protected lateinit var prefs: IPrefs
    private lateinit var userSignInUseCase: IUserSignInUseCase

    @Rule
    @JvmField
    val grantPermissionRule: GrantPermissionRule =
        GrantPermissionRule.grant(Manifest.permission.ACCESS_FINE_LOCATION)

    @Before
    open fun setUp() {
        val application = ApplicationProvider.getApplicationContext<TestVinciApp>()
        userSignInUseCase = application.getAccountComponent().provideUserSignInUseCase()
        prefs = application.appComponent.provideAppPrefs()
        Mockito.`when`(prefs.userAutologin).thenReturn(true)
        Mockito.`when`(prefs.userLogin).thenReturn(testUsername)
        Mockito.`when`(prefs.userPassword).thenReturn(testPassword)
        Mockito.`when`(prefs.userFirstName).thenReturn("Test Firstname")
        Mockito.`when`(prefs.userLastName).thenReturn("Test Lastname")
        Mockito.`when`(prefs.userEmail).thenReturn("test@test.com")
        Mockito.`when`(prefs.userProfilePicture).thenReturn("")
        runBlocking {
            Mockito.`when`(
                userSignInUseCase.authenticateUser(
                    testUsername,
                    testPassword,
                    testRememberMe
                )
            )
                .thenReturn(getTestUserAuthenticateResponseEntity(testToken))
        }
    }

    @After
    open fun tearDown() {
        Mockito.reset(prefs)
        // TODO: Needs to be reset when proper DI strategy implemented
     //   Mockito.reset(userSignInUseCase)
    }

    /**
     * Provide test response data.
     */
    private fun getTestUserAuthenticateResponseEntity(tokenId: String?): UserAuthenticateResponseEntity =
        UserAuthenticateResponseEntity(tokenId)

}