package com.comtrade.vinciapp.end_to_end

import androidx.lifecycle.Lifecycle
import androidx.test.core.app.launchActivity
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import com.comtrade.vinciapp.BuildConfig
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.end_to_end.main_screen.MainActivityAuthSetup
import com.comtrade.vinciapp.util.assertViewsDisplayed
import com.comtrade.vinciapp.view.activity.prelogin.view.PreloginActivity
import com.comtrade.vinciapp.view.activity.splash.view.SplashActivity
import org.hamcrest.core.AllOf.allOf
import org.junit.Test
import org.mockito.Mockito

/**
 * End to end test that covers the display of the Splash screen/Login screen,
 * entering the credentials and showing the home screen.
 */
internal class LoginToHomeScreenTest : MainActivityAuthSetup() {

    override fun setUp() {
        super.setUp()
        Mockito.`when`(prefs.isTermsAccepted).thenReturn(false)
    }

    @Test
    fun splash_screen_is_shown() {
        val scenario = launchActivity<SplashActivity>()
        scenario.moveToState(Lifecycle.State.RESUMED)

        assertViewsDisplayed(
            R.id.splash_screen_aal_logo, R.id.splash_screen_vinci_logo,
        )
    }

    @Test
    fun prelogin_activity_is_shown() {
        val scenario = launchActivity<PreloginActivity>()
        scenario.moveToState(Lifecycle.State.RESUMED)

        //Assert VINCI logo displayed
        onView(withId(R.id.includable_logo_image)).check(matches(isCompletelyDisplayed()))

        //Assert buttons displayed
        assertViewsDisplayed(
            R.id.welcome_fragment_login_button, R.id.welcome_fragment_create_account_button,
            R.id.welcome_fragment_facebook_button, R.id.welcome_fragment_google_button
        )

        //Assert the terms and conditions checkbox is displayed and unchecked
        onView(withId(R.id.welcome_fragment_terms_checkbox)).check(matches(isCompletelyDisplayed()))
    }


    @Test
    fun terms_and_conditions_not_checked_buttons_are_disabled() {
        Mockito.`when`(prefs.isTermsAccepted).thenReturn(false)

        val scenario = launchActivity<PreloginActivity>()
        scenario.moveToState(Lifecycle.State.RESUMED)

        //Assert the terms and conditions checkbox is displayed and unchecked
        onView(withId(R.id.welcome_fragment_terms_checkbox)).check(
            matches(
                allOf(
                    isCompletelyDisplayed(),
                    isNotChecked()
                )
            )
        )
    }

    @Test
    fun go_to_login_enter_credentials_home_screen_is_shown() {
        Mockito.`when`(prefs.isTermsAccepted).thenReturn(false)
        val scenario = launchActivity<PreloginActivity>()
        scenario.moveToState(Lifecycle.State.RESUMED)

        //Assert buttons displayed
        assertViewsDisplayed(
            R.id.welcome_fragment_login_button, R.id.welcome_fragment_create_account_button,
            R.id.welcome_fragment_facebook_button, R.id.welcome_fragment_google_button
        )
        //Assert the terms and conditions checkbox is displayed and unchecked
        onView(withId(R.id.welcome_fragment_terms_checkbox)).check(matches(isCompletelyDisplayed()))

        //Check terms and conditions
        onView(withId(R.id.welcome_fragment_terms_checkbox)).perform(click())

        Mockito.`when`(prefs.isTermsAccepted).thenReturn(true)
        //Click on accept
        onView(withId(R.id.terms_fragment_accept_button))
            .perform(click())

        onView(withId(R.id.welcome_fragment_terms_checkbox)).check(
            matches(
                allOf(
                    isCompletelyDisplayed(),
                    isChecked()
                )
            )
        )

        //Click on login
        onView(withId(R.id.welcome_fragment_login_button))
            .perform(click())

        //Enter username
        onView(withId(R.id.login_fragment_username_edit_text)).perform(typeText(BuildConfig.AUTH_TEST_USERNAME))

        //Enter password
        onView(withId(R.id.login_fragment_password_edit_text)).perform(typeText(BuildConfig.AUTH_TEST_PASSWORD))

        onView(withId(R.id.login_fragment_login_button)).perform(click())

        //Assert home screen shown.
        assertViewsDisplayed(R.id.navigation_view)

    }
}
