/*
 * Copyright (c) 2022, Comtrade
 * All rights reserved.
 *
 * Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project: VinciApplication
 * File: ElderlyQuestionnaireTest.kt
 * Author: sdelic
 *
 * History:  5.4.22. 11:54 created
 */

package com.comtrade.vinciapp.end_to_end.qustionnaire

import androidx.test.core.app.launchActivity
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.LargeTest
import com.comtrade.vinciapp.R
import com.comtrade.vinciapp.end_to_end.main_screen.MainActivityAuthSetup
import com.comtrade.vinciapp.util.assertViewsDisplayed
import com.comtrade.vinciapp.view.activity.main.view.MainActivity
import org.junit.Test
import kotlin.random.Random

/**
 * End to end test that covers the display of the user-flow for the Elderly Questionnaire feature.
 * Asserting the individual screen displays and navigation.
 * Starting with the home screen and navigating to the Elderly Questionnaire side-menu item.
 */
@LargeTest
internal class ElderlyQuestionnaireEndToEndTest : MainActivityAuthSetup() {


    override fun setUp() {
        super.setUp()
        launchActivity<MainActivity>()
    }

    @Test
    fun user_can_navigate_through_the_elderly_questionnaire_by_answering_questions() {
        navigate_to_questionnaire_from_main_menu()

        //First question
        assertFirstQuestion()

        //Second question
        assertSecondQuestion()

        //Third question
        assertThirdQuestion()

        //Fourth question
        assertFourthQuestion()

        //Fifth question
        assertFifthQuestion()

        //Sixth question
        assertSixthQuestion()

        //Seventh question
        assertSeventhQuestion()

        //End screen
        assertViewsDisplayed(R.id.ipa_questionnaire_end_view_title)

    }


    @Test
    fun user_can_navigate_back_to_questionnaire_menu_from_first_question() {
        navigate_to_questionnaire_from_main_menu()

        onView(withId(R.id.questionnaire_fragment_back_button)).perform(click())

        assertViewsDisplayed(
            R.id.questionnaire_menu_fragment_ipa_questionnaire_button,
            R.id.questionnaire_menu_fragment_whoqol_questionnaire_button,
            R.id.questionnaire_menu_fragment_user_needs_questionnaire_button,
            R.id.questionnaire_menu_fragment_elderly_questionnaire_button
        )
    }

    @Test
    fun user_can_navigate_back_to_questionnaire_menu_from_random_question() {
        //Select random question to navigate to and back from
        val randomQuestion = Random.nextInt(questions().size)

        navigate_to_questionnaire_from_main_menu()

        val questionAssertions = listOf(
            { assertFirstQuestion() },
            { assertSecondQuestion() },
            { assertThirdQuestion() },
            { assertFourthQuestion() },
            { assertFifthQuestion() },
            { assertSixthQuestion() },
            { assertSeventhQuestion() },
        )

        for (i in 0 until randomQuestion) {
            questionAssertions[i]()
        }

        //Navigate back
        for (i in randomQuestion downTo 0) {
            onView(withId(R.id.questionnaire_fragment_back_button)).perform(click())
        }

        assertViewsDisplayed(
            R.id.questionnaire_menu_fragment_ipa_questionnaire_button,
            R.id.questionnaire_menu_fragment_whoqol_questionnaire_button,
            R.id.questionnaire_menu_fragment_user_needs_questionnaire_button,
            R.id.questionnaire_menu_fragment_elderly_questionnaire_button
        )
    }

    private fun navigate_to_questionnaire_from_main_menu() {
        //Navigate to the Elderly questionnaire from the drawer menu
        assertViewsDisplayed(R.id.main_activity_menu_button)
        onView(withId(R.id.main_activity_menu_button)).perform(click())
        assertViewsDisplayed(R.id.navigation_questionnaire)
        onView(withText(R.string.navigation_screen_questionnaire)).perform(click())

        //Questionnaire selection is visible and go to the elderly questionnaire screen.
        assertViewsDisplayed(
            R.id.questionnaire_menu_fragment_ipa_questionnaire_button,
            R.id.questionnaire_menu_fragment_whoqol_questionnaire_button,
            R.id.questionnaire_menu_fragment_user_needs_questionnaire_button,
            R.id.questionnaire_menu_fragment_elderly_questionnaire_button
        )

        onView(withId(R.id.questionnaire_menu_fragment_elderly_questionnaire_button)).perform(click())

        //Elderly questionnaire.
        assertViewsDisplayed(
            R.id.questionnaire_fragment_elderly_survey_title,
            R.id.questionnaire_fragment_view_pager,
            R.id.questionnaire_fragment_nav_buttons_container,
            R.id.questionnaire_fragment_back_button,
            R.id.questionnaire_fragment_next_button
        )
    }

    private fun assertFirstQuestion() {
        val answers = arrayOf(
            R.id.elderly_questionnaire_answers_1_yes,
            R.id.elderly_questionnaire_answers_1_no
        )
        assertViewsDisplayed(
            R.id.elderly_questionnaire_question_1,
            R.id.elderly_questionnaire_answers_1,
            R.id.elderly_questionnaire_answers_1_yes,
            R.id.elderly_questionnaire_answers_1_no
        )
        selectRandomAnswer(questions()[0], answers)
    }

    private fun assertSecondQuestion() {
        val answers = arrayOf(
            R.id.elderly_questionnaire_question_2_answer_1,
            R.id.elderly_questionnaire_question_2_answer_2,
            R.id.elderly_questionnaire_question_2_answer_3,
            R.id.elderly_questionnaire_question_2_answer_4,
            R.id.elderly_questionnaire_question_2_answer_5,
            R.id.elderly_questionnaire_question_2_answer_6,
            R.id.elderly_questionnaire_question_2_answer_7
        )
        assertViewsDisplayed(
            R.id.elderly_questionnaire_question_2,
            R.id.elderly_questionnaire_question_2_answer_1,
            R.id.elderly_questionnaire_question_2_answer_2,
            R.id.elderly_questionnaire_question_2_answer_3,
            R.id.elderly_questionnaire_question_2_answer_4,
            R.id.elderly_questionnaire_question_2_answer_5,
            R.id.elderly_questionnaire_question_2_answer_6,
            R.id.elderly_questionnaire_question_2_answer_7
        )

        selectRandomAnswer(questions()[1], answers)
    }

    private fun assertThirdQuestion() {
        val answers = arrayOf(
            R.id.elderly_questionnaire_answers_3_yes,
            R.id.elderly_questionnaire_answers_3_no
        )
        assertViewsDisplayed(
            R.id.elderly_questionnaire_question_3,
            R.id.elderly_questionnaire_answers_3,
            R.id.elderly_questionnaire_answers_3_yes,
            R.id.elderly_questionnaire_answers_3_no
        )

        selectRandomAnswer(questions()[2], answers)
    }

    private fun assertFourthQuestion() {
        val answers = arrayOf(
            R.id.elderly_questionnaire_question_4_answer_1,
            R.id.elderly_questionnaire_question_4_answer_2,
            R.id.elderly_questionnaire_question_4_answer_3,
            R.id.elderly_questionnaire_question_4_answer_4,
            R.id.elderly_questionnaire_question_4_answer_5,
            R.id.elderly_questionnaire_question_4_answer_6,
            R.id.elderly_questionnaire_question_4_answer_7,
        )
        assertViewsDisplayed(
            R.id.elderly_questionnaire_question_4,
            R.id.elderly_questionnaire_question_4_answer_1,
            R.id.elderly_questionnaire_question_4_answer_2,
            R.id.elderly_questionnaire_question_4_answer_3,
            R.id.elderly_questionnaire_question_4_answer_4,
            R.id.elderly_questionnaire_question_4_answer_5,
            R.id.elderly_questionnaire_question_4_answer_6,
            R.id.elderly_questionnaire_question_4_answer_7,
            R.id.elderly_questionnaire_question_4_answer_8_label,
            R.id.elderly_questionnaire_question_4_answer_8
        )
        selectRandomAnswer(questions()[3], answers)
    }

    private fun assertFifthQuestion() {
        val answers = arrayOf(
            R.id.elderly_questionnaire_answers_5_yes,
            R.id.elderly_questionnaire_answers_5_no
        )

        assertViewsDisplayed(
            R.id.elderly_questionnaire_question_5,
            R.id.elderly_questionnaire_answers_5,
            R.id.elderly_questionnaire_answers_5_yes,
            R.id.elderly_questionnaire_answers_5_no
        )
        selectRandomAnswer(questions()[4], answers)
    }

    private fun assertSixthQuestion() {
        val testName = "Text Input Name"
        val testEmail = "someemail@testemail.com"
        val testPhone = "0123456789"

        assertViewsDisplayed(
            R.id.elderly_questionnaire_question_6,
            R.id.elderly_questionnaire_question_6_answer_1_label,
            R.id.elderly_questionnaire_question_6_answer_1,
            R.id.elderly_questionnaire_question_6_answer_2_label,
            R.id.elderly_questionnaire_question_6_answer_2,
            R.id.elderly_questionnaire_question_6_answer_3_label,
            R.id.elderly_questionnaire_question_6_answer_3
        )

        onView(withText(questions()[5])).check(matches(isDisplayed()))

        onView(withId(R.id.elderly_questionnaire_question_6_answer_1)).perform(
            ViewActions.typeText(testName), ViewActions.closeSoftKeyboard()
        )
        onView(withId(R.id.elderly_questionnaire_question_6_answer_2)).perform(
            ViewActions.typeText(testEmail), ViewActions.closeSoftKeyboard()
        )

        onView(withId(R.id.elderly_questionnaire_question_6_answer_3)).perform(
            ViewActions.typeText(testPhone), ViewActions.closeSoftKeyboard()
        )

        onView(withId(R.id.questionnaire_fragment_next_button)).perform(click())
    }

    private fun assertSeventhQuestion() {
        val answers = arrayOf(
            R.id.elderly_questionnaire_answers_7_yes,
            R.id.elderly_questionnaire_answers_7_no
        )

        assertViewsDisplayed(
            R.id.elderly_questionnaire_question_7,
            R.id.elderly_questionnaire_answers_7,
            R.id.elderly_questionnaire_answers_7_yes,
            R.id.elderly_questionnaire_answers_7_no
        )
        selectRandomAnswer(questions()[6], answers)
    }

    private fun selectRandomAnswer(questionId: Int, possibleAnswers: Array<Int>) {
        onView(withText(questionId)).check(matches(isDisplayed()))
        //Select random answer and move forward
        val randomAnswer = Random.nextInt(possibleAnswers.size)

        onView(withId(possibleAnswers[randomAnswer])).perform(click())
        onView(withId(possibleAnswers[randomAnswer])).check(matches(isChecked()))

        onView(withId(R.id.questionnaire_fragment_next_button)).perform(click())
    }

    private fun questions(): Array<Int> {
        return arrayOf(
            R.string.questionnaire_elderly_question_1_text,
            R.string.questionnaire_elderly_question_2_text,
            R.string.questionnaire_elderly_question_3_text,
            R.string.questionnaire_elderly_question_4_text,
            R.string.questionnaire_elderly_question_5_text,
            R.string.questionnaire_elderly_question_6_text,
            R.string.questionnaire_elderly_question_7_text,
        )
    }
}