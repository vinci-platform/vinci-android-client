package com.comtrade.vinciapp.util

import androidx.annotation.IdRes
import androidx.annotation.StringRes
import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers.*
import org.hamcrest.CoreMatchers
import org.hamcrest.CoreMatchers.allOf
import org.hamcrest.CoreMatchers.not
import org.hamcrest.core.AllOf

/**
 * Assert all views with the given resource Id's are displayed using the Espresso testing library view assertions.
 */
internal fun assertViewsDisplayed(@IdRes vararg viewIds: Int) {
    for (viewId in viewIds) {
        Espresso.onView(withId(viewId))
            .check(ViewAssertions.matches(isCompletelyDisplayed()))
    }
}

/**
 * Assert all views with the given resource Id's are not displayed using the Espresso testing library view assertions.
 */
internal fun assertViewsNotDisplayed(@IdRes vararg viewIds: Int) {
    for (viewId in viewIds) {
        Espresso.onView(withId(viewId))
            .check(ViewAssertions.matches(not(isDisplayed())))
    }
}

/**
 * Assert all views with the string resource Id's are displayed.
 */
internal fun assertTextDisplayed(@StringRes vararg stringRes: Int) {
    for (stringId in stringRes) {
        Espresso.onView(allOf(withText(stringId), isDisplayed())).check(
            ViewAssertions.matches(
                isCompletelyDisplayed()
            )
        )
    }
}

/**
 * Espresso assertion of input fields for the correct hint, empty state and input type.
 */
internal fun assertEditFieldForHintAndInputType(
    @IdRes viewId: Int,
    @StringRes hintId: Int,
    inputType: Int
) {
    Espresso.onView(withId(viewId)).check(
        ViewAssertions.matches(
            AllOf.allOf(
                withHint(hintId),
                withText(""),
                withInputType(inputType)
            )
        )
    )
}


/**
 * Verifies that the parent view component has matching descendant with the correct string displayed.
 */
internal fun assertDescendantWithText(@IdRes parentId: Int, stringIds: IntArray) {
    for (stringId in stringIds) {
        Espresso.onView(withId(parentId))
            .check(ViewAssertions.matches(hasDescendant(withText(stringId))))
    }
}

/**
 * Verifies that the Spinner component has the selection item and selects that item for selection visibility.
 * @param spinnerViewId The spinner view component Id
 * @param selectionItem The String value that will be picked and asserted for visibility
 */
internal fun assertSpinnerSelection(@IdRes spinnerViewId: Int, selectionItem: String) {
    Espresso.onView(withId(spinnerViewId)).perform(ViewActions.click())
    Espresso.onData(
        AllOf.allOf(
            CoreMatchers.`is`(CoreMatchers.instanceOf(String::class.java)),
            CoreMatchers.`is`(selectionItem)
        )
    ).perform(
        ViewActions.click()
    )
    Espresso.onView(withId(spinnerViewId))
        .check(ViewAssertions.matches(withSpinnerText(selectionItem)))
}

/**
 * Check view for matching text value
 * @param viewId assertion view Id
 * @param stringId String resource Id
 */
internal fun assertViewHasCaption(@IdRes viewId: Int, @StringRes stringId: Int) {
    Espresso.onView(withId(viewId)).check(ViewAssertions.matches(withText(stringId)))
}

/**
 *
 * Assert all views with the given resource Id's are enabled using the Espresso testing library view assertions.
 */
internal fun assertButtonsEnabled(@IdRes vararg viewIds: Int) {
    for (buttonId in viewIds) {
        Espresso.onView(withId(buttonId))
            .check(ViewAssertions.matches(isEnabled()))
    }
}