package com.comtrade.vinciapp.util

import android.view.WindowManager
import androidx.test.espresso.Root
import org.hamcrest.Description
import org.hamcrest.TypeSafeMatcher

/**
 * Custom toast UI matcher component.
 */
internal class ToastViewMatcher : TypeSafeMatcher<Root>() {
    override fun describeTo(description: Description?) {
        description?.apply {
            appendText("Root view should have")
        }
    }

    @Suppress("DEPRECATION")
    override fun matchesSafely(item: Root?): Boolean {
        val type = item?.windowLayoutParams?.get()?.type
        if (type == WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY || type == WindowManager.LayoutParams.TYPE_TOAST) {
            val windowToken = item.decorView.windowToken
            val appToken = item.decorView.applicationWindowToken
            if (windowToken == appToken) {
                return true
            }
        }
        return false
    }
}