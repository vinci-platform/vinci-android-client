/*
 *  Copyright (c) 2020, Comtrade
 *  All rights reserved.
 *
 *  Licence: BSD-3-Clause (GPL compatible)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of the <organization> nor the
 *        names of its contributors may be used to endorse or promote products
 *        derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *   DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  Package: com.comtrade.vinciapp.streamingServiceUtil
 *  File: AddDeviceFragment.kt
 *  Author: Kosta Eric <kosta.eric@comtrade.com>
 *
 *  Description: Shoe monitoring container
 *
 *  History: 8/21/20  Kosta  Initial code
 *
 *
 */


package com.comtrade.feature_shoe_monitoring

import android.content.Context
import android.util.Log
import com.comtrade.domain.contracts.repository.local.IPrefs
import com.comtrade.feature_sensoria.sensorialibrary.*
import com.comtrade.feature_sensoria.sensorialibrary.SAService.ServiceTypes
import com.comtrade.feature_shoe_monitoring.contracts.IShoeDataRefresh
import com.comtrade.feature_shoe_monitoring.contracts.IShoeDataSend
import com.comtrade.feature_shoe_monitoring.contracts.OperationState
import com.comtrade.feature_shoe_monitoring.data.models.GeneralUIInformation
import com.comtrade.feature_shoe_monitoring.data.models.ShoeDataUIInformation

/**
 * Launches the SA streaming service.
 * Stops, resumes the SA streaming service.
 * Dispatches shoe data information.
 * Updates shoe device data.
 */
class ShoeMonitoringContainer(
    private val context: Context,
    private val shoeDataSend: IShoeDataSend,
    private val streamingService: SAStreamingService,
    private val shoeDataRefresh: IShoeDataRefresh,
    private val prefs: IPrefs
) : SAServiceStreamingServiceInterface {
    private val deviceCode: String = streamingService.mDeviceCode
    private val deviceName: String = streamingService.mServiceName
    private val deviceMac: String = streamingService.mDeviceMac
    private var state = OperationState.INACTIVE
    private var s0Buffer: MutableList<Int> = ArrayList()
    private var s1Buffer: MutableList<Int> = ArrayList()
    private var s2Buffer: MutableList<Int> = ArrayList()
    private var sTBuffer: MutableList<Int> = ArrayList()
    private var prevCycleSteps = 0
    private var actualCycleSteps = 0
    private var lowThreshold = prefs.shoeDevice!!.lowThreshold
    private var highThreshold = prefs.shoeDevice!!.highThreshold
    private var waitSamples = 160
    private var initStandSamples = 64
    private var initWalkSamples = 320
    private var walkingSamples = 96
    private var avgStanding = -1.0
    private var maxWalking = -1
    private var minWalking = -1
    private var curInRange = false
    private var prevInRange = false

    private var userState = -1
    var isConnected: Boolean = false
    var steps = 0


    fun start(): Boolean {
        isConnected = true
        s0Buffer = ArrayList()
        s1Buffer = ArrayList()
        s2Buffer = ArrayList()
        sTBuffer = ArrayList()
        prevCycleSteps = 0
        actualCycleSteps = 0
        steps = 0
        initStandSamples = 64
        initWalkSamples = 320
        walkingSamples = 96
        avgStanding = -1.0
        maxWalking = -1
        minWalking = -1
        curInRange = false
        prevInRange = false
        try {
            if (state == OperationState.INACTIVE) {
                streamingService.start(this, context)
                streamingService.resume()
                Log.d(
                    TAG,
                    "Starting data acquisition..."
                )
                if (lowThreshold > 0 && highThreshold > 0)
                    state = OperationState.RECORDING
            }
        } catch (e: InterruptedException) {
            e.printStackTrace()
            return false
        }
        return true
    }

    fun startInit() {
        state = OperationState.WAIT_AT_START
        emptyBuffers()
        Log.d(
            TAG,
            String.format("Waiting to start initialization...")
        )
        shoeDataSend.sendAvgStanding(0.0, deviceMac)
        shoeDataSend.sendMinMaxWalking(0, 0, deviceMac)
    }

    fun stop() {
        streamingService.pause()
        streamingService.stop()
        state = OperationState.INACTIVE
    }

    fun resume() {
        streamingService.start(this, context)
        streamingService.resume()
    }

    private fun updateSteps(value: Int) {
        try {
            val avg = value.toDouble()
            if (avg < lowThreshold) {
                curInRange = false
            }
            if (avg in lowThreshold..highThreshold) {
                curInRange = true
            }
            if (!prevInRange && curInRange) {
                steps += 2
            }
            prevInRange = curInRange
        } catch (e: Exception) {
            Log.d(
                TAG,
                "Error updating steps"
            )
        }
    }

    private fun updateActivity(actualCycleSteps: Int, prevCycleSteps: Int, v: Int) {
        try {
            if (actualCycleSteps - prevCycleSteps > 0) {  //The step count has increased. Dispatch walking state  int(N/2)
                userState = 2
            } else if (actualCycleSteps == prevCycleSteps && v <= lowThreshold) {  //The step count has not increased nad the pressure sensor is reading low values. Dispatch not worn state int(N/2)
                userState = 1
            } else if (actualCycleSteps == prevCycleSteps && v > lowThreshold) {  ///The step count has not increased nad the pressure sensor is reading high values. Dispatch standing state int(N/2)
                userState = 0
            }
        } catch (e: Exception) {
            Log.e(
                TAG,
                "Error updating activity"
            )
        }
    }

    private fun isInRage(v: Double): Boolean {
        return v > lowThreshold && v < highThreshold
    }

    private fun addToBuffers(s0: Int, s1: Int, s2: Int) {
        s0Buffer.add(s0)
        s1Buffer.add(s1)
        s2Buffer.add(s2)
        sTBuffer.add(s0 + s1 + s2)
    }

    private fun emptyBuffers() {
        s0Buffer.clear()
        s1Buffer.clear()
        s2Buffer.clear()
        sTBuffer.clear()
    }


    private fun getBufferAverage(buffer: List<Int>): Double {
        var total = 0.0
        for (i in buffer.indices) {
            total += buffer[i]
        }
        return if (buffer.isNotEmpty()) total / buffer.size else 0.0
    }

    private fun getBufferMin(buffer: List<Int>): Int {
        var min = 1000000
        for (i in buffer.indices) {
            if (buffer[i] < min) {
                min = buffer[i]
            }
        }
        return min
    }

    private fun getBufferMax(buffer: List<Int>): Int {
        var max = 0
        for (i in buffer.indices) {
            if (buffer[i] > max) {
                max = buffer[i]
            }
        }
        return max
    }

    override fun didUpdateData(
        device: SADevice,
        serviceType: ServiceTypes,
        dataPoint: SADataPoint
    ) {
        val st = dataPoint.channels[0] + dataPoint.channels[1] + dataPoint.channels[2]
        val generalUIInformation = GeneralUIInformation(
            dataPoint.channels[0],
            dataPoint.channels[1],
            dataPoint.channels[2],
            dataPoint.accelerometers[0],
            dataPoint.accelerometers[1],
            dataPoint.accelerometers[2],
            dataPoint.nominalSamplingRate,
            dataPoint.actualSamplingRate,
            state.toString()
        )
        shoeDataSend.sendGeneralUIInformation(generalUIInformation, deviceMac)
        if (state == OperationState.INACTIVE) {
            Log.d(
                TAG,
                String.format(
                    "Received data from %s, tick: %d",
                    device.deviceName,
                    dataPoint.tickCount
                )
            )
            if (lowThreshold == -1.0 && highThreshold == -1.0)
                return
            Log.d(
                TAG,
                String.format("Starting standing threshold acquisition...")
            )
            state = OperationState.RECORDING
            addToBuffers(
                dataPoint.channels[0],
                dataPoint.channels[1],
                dataPoint.channels[2]
            )
        } else if (state == OperationState.WAIT_AT_START && s0Buffer.size < waitSamples) {
            addToBuffers(
                dataPoint.channels[0],
                dataPoint.channels[1],
                dataPoint.channels[2]
            )
        } else if (state == OperationState.WAIT_AT_START && s0Buffer.size == waitSamples) {
            emptyBuffers()
            state = OperationState.INIT_STAND
        } else if (state == OperationState.INIT_STAND && s0Buffer.size < initStandSamples) {
            addToBuffers(
                dataPoint.channels[0],
                dataPoint.channels[1],
                dataPoint.channels[2]
            )
        } else if (state == OperationState.INIT_STAND && s0Buffer.size == initStandSamples) {
            avgStanding = getBufferAverage(sTBuffer)
            shoeDataSend.sendAvgStanding(avgStanding, deviceMac)
            Log.d(
                TAG,
                String.format("Standing Treshold acquisition completed. Standing position avg: $avgStanding")
            )
            state = OperationState.INIT_WALK
            emptyBuffers()
        } else if (state == OperationState.INIT_WALK && s0Buffer.size < initWalkSamples) {
            addToBuffers(
                dataPoint.channels[0],
                dataPoint.channels[1],
                dataPoint.channels[2]
            )
        } else if (state == OperationState.INIT_WALK && s0Buffer.size == initWalkSamples) {
            addToBuffers(
                dataPoint.channels[0],
                dataPoint.channels[1],
                dataPoint.channels[2]
            )
            minWalking = getBufferMin(sTBuffer)
            maxWalking = getBufferMax(sTBuffer)
            shoeDataSend.sendMinMaxWalking(minWalking, maxWalking, deviceMac)
            Log.d(
                TAG,
                String.format("Walking acquisition completed. Min: $minWalking, Max:$maxWalking")
            )
            state = OperationState.RECORDING
            lowThreshold = minWalking + 0.8 * (maxWalking - minWalking)
            highThreshold = maxWalking - 0.1 * (maxWalking - minWalking)
            val shoeDevice = prefs.shoeDevice!!
            shoeDevice.lowThreshold = lowThreshold
            shoeDevice.highThreshold = highThreshold
            prefs.shoeDevice = shoeDevice
            prefs.addLog("Update shoe device: $shoeDevice")
            shoeDataRefresh.updateShoeDevice()
            emptyBuffers()
        } else if (state == OperationState.RECORDING && s0Buffer.size < walkingSamples) {
            addToBuffers(
                dataPoint.channels[0],
                dataPoint.channels[1],
                dataPoint.channels[2]
            )
            updateSteps(st)
        } else if (state == OperationState.RECORDING && s0Buffer.size == walkingSamples) {
            addToBuffers(
                dataPoint.channels[0],
                dataPoint.channels[1],
                dataPoint.channels[2]
            )
            updateSteps(st)
            actualCycleSteps = steps
            updateActivity(actualCycleSteps, prevCycleSteps, st)
            val shoeDataUIInformation = ShoeDataUIInformation(
                deviceMac, deviceName, deviceCode,
                actualCycleSteps - prevCycleSteps, userState
            )
            shoeDataSend.sendShoeDataInformation(shoeDataUIInformation, deviceMac)
            shoeDataRefresh.addSteps(actualCycleSteps - prevCycleSteps)
            Log.d(
                "Steps",
                "Current steps = " + (actualCycleSteps - prevCycleSteps) + " total steps: " + steps
            )
            prevCycleSteps = steps
            emptyBuffers()
        }
    }

    override fun didServiceError(
        p0: SADevice,
        p1: ServiceTypes,
        p2: String,
        p3: String,
        p4: SAErrors
    ) {
        isConnected = false
    }

    override fun didServiceError(
        p0: SADevice,
        p1: ServiceTypes,
        p2: String,
        p3: String,
        p4: SAErrors,
        p5: String
    ) {
        isConnected = false
    }

    override fun didServiceConnect(
        p0: SADevice,
        p1: ServiceTypes
    ) {
    }

    override fun didServiceDisconnect(
        p0: SADevice,
        p1: ServiceTypes
    ) {
    }

    override fun didServicePause(
        p0: SADevice,
        p1: ServiceTypes
    ) {
    }

    override fun didServiceResume(
        p0: SADevice,
        p1: ServiceTypes
    ) {
        isConnected = true
    }

    override fun didServiceReady(
        p0: SADevice,
        p1: ServiceTypes
    ) {
    }

    override fun didServiceReset(
        p0: SADevice,
        p1: ServiceTypes
    ) {
    }

    companion object {
        private const val TAG = "DataRecording"
    }

}